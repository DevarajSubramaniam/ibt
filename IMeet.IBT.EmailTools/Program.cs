﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

using IMeet.IBT.Common;
using IMeet.IBT.DAL;
using IMeet.IBT.Services;
using IMeet.IBT.Common.Caching;
using IMeet.IBT.Common.Logging;

using StructureMap;
using StructureMap.Configuration.DSL;
using log4net;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;


namespace IMeet.IBT.EmailTools
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                WriteLog("Start");
                RunStatus rs = ReadRuning();
                if (rs == null)
                {
                    SetRuning(false);
                    rs = new RunStatus() { Running = false, LastTime = DateTime.Now };
                }

                //if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length < 2        

                if (!rs.Running)
                {
                    SetRuning(true);
                    WriteLog("Running -True");
                    #region do something
                    WriteLog("Sending Email");
                    //IMeet.IBT.Controllers.StructureMapBootrstrapper.Configure();
                    //IAdminEmailService _adminEmailService = ObjectFactory.GetInstance<IAdminEmailService>();
                    //_adminEmailService.SendEmail();
                    Console.WriteLine("**********************************************************");
                    Console.WriteLine("Emails are going out, please DO NOT close this window");
                    Console.WriteLine("**********************************************************");

                    Task[] tasks = new Task[10];
                    tasks[0] = Task.Run(() => { sendEmails(0); });                    
                    tasks[1] = Task.Run(() => { sendEmails(1); });                    
                    tasks[2] = Task.Run(() => { sendEmails(2); });                    
                    tasks[3] = Task.Run(() => { sendEmails(3); });                    
                    tasks[4] = Task.Run(() => { sendEmails(4); });                    
                    tasks[5] = Task.Run(() => { sendEmails(5); });
                    tasks[6] = Task.Run(() => { sendEmails(6); });
                    tasks[7] = Task.Run(() => { sendEmails(7); });
                    tasks[8] = Task.Run(() => { sendEmails(8); });
                    tasks[9] = Task.Run(() => { sendEmails(9); });
                    Task.WaitAll(tasks);

                    //Console.WriteLine("End of sending emails");
                    //Thread[] threadsArray = new Thread[10];
                    //for (int i = 0; i < 10; i++)
                    //{
                    //    threadsArray[i] = new Thread(new ParameterizedThreadStart(sendEmails));
                    //    threadsArray[i].IsBackground = false;
                    //    threadsArray[i].Start(i);  
                    //}
                    //for (int i = 0; i < 10; i++)
                    //{
                    //    if (threadsArray[i].IsAlive)
                    //    {
                    //        threadsArray[i].Join();
                    //    }
                    //}


                    #endregion
                    SetRuning(false);
                    WriteLog("Running -False");
                }
                WriteLog("End");
                //   Console.ReadKey();
            }
            catch (Exception ex)
            {
                //SetRuning(false);
                WriteLog("Running -False" + ex.StackTrace.ToString());
            }
        }

        static void SetRuning(bool running)
        {
            try
            {
                string filePath = System.AppDomain.CurrentDomain.BaseDirectory + "RunStatus.ibt";
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                RunStatus rs = new RunStatus()
                {
                    Running = running,
                    LastTime = DateTime.Now
                };

                using (Stream fStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
                {
                    binaryFormatter.Serialize(fStream, rs);
                }
            }
            catch (Exception ex)
            {
                //SetRuning(false);
                WriteLog("Running -False" + ex.StackTrace.ToString());
            }
        }

        static RunStatus ReadRuning()
        {
            try
            {
                string filePath = System.AppDomain.CurrentDomain.BaseDirectory + "RunStatus.ibt";
                BinaryFormatter binaryFormatter = new BinaryFormatter();

                if (File.Exists(filePath))
                {
                    using (Stream fStream = File.OpenRead(filePath))
                    {
                        RunStatus rs = (RunStatus)binaryFormatter.Deserialize(fStream);
                        return rs;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
               // SetRuning(false);
                WriteLog("Running -False" + ex.StackTrace.ToString());
                return null;
            }
        }

        static void WriteLog(string message)
        {
            try
            {
                string logFileName = System.AppDomain.CurrentDomain.BaseDirectory + "log\\EmailToolLog(" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ").txt";

                if (!Directory.Exists("log"))
                {
                    Directory.CreateDirectory("log");
                }

                if (!File.Exists(logFileName))
                {
                    using (StreamWriter sw = File.CreateText(logFileName))
                    {
                        sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + message);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(logFileName))
                    {
                        sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + message);
                    }
                }
            }
            catch (Exception ex)
            {
               // SetRuning(false);
                WriteLog("Running -False" + ex.StackTrace.ToString());
            }
        }

        public static void sendEmails(object digit)
        {
            try
            {
                int varDigit = int.Parse(digit.ToString());
                IMeet.IBT.Controllers.StructureMapBootrstrapper.Configure();
                IAdminEmailService _adminEmailService = ObjectFactory.GetInstance<IAdminEmailService>();

                _adminEmailService.SendEmail(varDigit);
            }
            catch (Exception ex)
            {
                //SetRuning(false);
                WriteLog("Running -False" + ex.StackTrace.ToString());
            }
        }
    }

    [Serializable]
    public class RunStatus
    {
        public bool Running { get; set; }
        public DateTime LastTime { get; set; }
    }
}
