@Subject=Request of change of email on I've Been There
<html>
  <body>
    <div style="font-family: Verdana, Arial, Serif;"><br><div style="font-size: 11px; color:#6f7b7b; font-weight:bold;">
          Dear {firstname},
        </div><br>
      <div style="font-size: 11px; color:#6f7b7b;">
          We have been notified that your email has been changed to {NewEMail}.
          If you have not changed your email address, please contact customer service immediately at contact@i-meet.com.
          <br>
          <br><br>
          Thank you!<br>
          I've Been There team
        </div>
    </div>
  </body>
</html>