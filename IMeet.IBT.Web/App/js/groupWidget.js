﻿(function($) {

	$.fn.getGroupWidget = function(options) {
		var o = $.extend({}, $.fn.getGroupWidget.defaults, options);
	    //$(this).createStyleSheet("http://i-meet.com/include/css/widget.css");
		// hide container element
		//$(this).hide();
		//var flag = false;
		var obj = this;
		var objId = obj[0].id;
        //debugger;
		$.ajax({
		async: false, 
		url:'http://i-meet.com/AjaxServer/GroupMemberWidget.ashx?callback=?',
        type:"GET",
        data:{act:'key',gid:o.groupId,key:o.key},
        dataType: "jsonp",
        jsonp: "callback",
        success:function(json)
              {
                 if(json.msg=="true")
                 {
                    $.getWidgetHtml(objId,o.groupId,o.width,o.headingText,o.memberCount,o.topicCount,o.bgcolor);
                 }
              }
        });
		
	};

	// plugin defaults
	$.fn.getGroupWidget.defaults = {
		groupId: null,
		memberCount:6,
		topicCount:3,
		width: 180,
		key: "",
		headingText: "GroupName",
		bgcolor:"#3D7EBE"
	};
	
	$.extend({
        _fileloaded: ',',
        rootPath: '',
        include: function (file, rp) {
            if (rp) this.rootPath = rp;
            var files = typeof file == "string" ? [file] : file;
            for (var i = 0; i < files.length; i++) {
                var fn = files[i].toLowerCase();
                var array = fn.split('.');
                if (array.length == 0) continue;
                var ext = array[array.length - 1];
                var path = ($.rootPath + fn).toLowerCase();
                if (this._fileloaded.indexOf(path) == -1) {
                    if (ext == "css") {
                        $(document.createElement("link"))
                        .attr({ "rel": "stylesheet", "type": "text/css", "href": path })
                        .appendTo("head");
                    }
                    else {
                        $(document.createElement("script"))
                        .attr({ "type": "text/javascript", "src": path })
                        .appendTo("head");
                    }
                }
                this._fileloaded += path + ",";
            }
        },
        pageVeiw: function(id) { 
            //return a < b ? a : b; 
            var url = document.URL;
            var host = location.hostname;
            if(host.indexOf('i-meet.com')<0)
            {
                $.getJSON('http://i-meet.com/AjaxServer/GroupMemberWidget.ashx?callback=?',
                    {act:'view',gid:id,url:url},          
                    function(json)
                      {
                        
                      });
            }
          },
          pageClick: function(id) { 
            //return a > b ? a : b; 
            var url = document.URL;
            var host = location.hostname;
            if(host.indexOf('i-meet.com')<0)
            {
                $.getJSON('http://i-meet.com/AjaxServer/GroupMemberWidget.ashx?callback=?',
                    {act:'click',gid:id,url:url},
                    function(json)
                      {
                        
                      });
             }
          },
          checkKey: function(id,key){
                var flag = false;
		        $.ajax({
		        async: false, 
		        url:'http://i-meet.com/AjaxServer/GroupMemberWidget.ashx?callback=?',
                type:"GET",
                data:{act:'key',gid:id,key:key},
                dataType: "jsonp",
                jsonp: "callback",
                success:function(json)
                      {
                        //alert(json.msg);
                         if(json.msg=="true")
                         {
                            flag = true;
                         }
                      }
                });
                return flag;
          },
          
          getWidgetHtml: function(objid,groupId,width,headingText,memberCount,topicCount,color){
            // add heading to container element
		        $.include('widget.css','http://i-meet.com/widget/css/');
		        var whtm='<div class="cruise" style="width:' + width + 'px;background-color:'+color+';border:'+color+' 1px solid;"><div id="w_Top" class="top"></div><div class="info"><div id="gm_info" class="inline-block percent"></div><div id="widget_member_list" class="inline-block percent"></div><div id="gt_info" class="inline-block percent margintop5"></div><div id="widget_topic_list" class="margintop5"></div></div><div class="btm" style="width:' + (parseInt(width)-5) + 'px;"><div id="imLogo" style="text-align:center;"></div></div>';
		        $("#"+objid).append(whtm);
                
	            $.pageVeiw(groupId);
	            
		        $("#w_Top").append('<h2>'+headingText+'</h2>');
		        if(memberCount >-2){
	                $("#gm_info").append('<div class="left"><span class="n_bold">Members: </span><span id="spnAllMember" class="color615f9e font11"></span></div><div id="gmViewAll" class="right"></div>');
                }
                else{
                    $("#gm_info").hide();
                }
                
                if(memberCount> -1){
	                $("#gmViewAll").append('<a target="_blank" class="color615f9e font11" href="http://i-meet.com/pages/group/ptgroup_members.aspx?groupid='+groupId+'">View All</a>');
        		}
        		
                if(topicCount >-1){
	                $("#gt_info").append('<div class="left"><span class="n_bold">Discussion Topics: </span></div><div id="gtViewAll" class="right"></div>');
	                $("#gtViewAll").append('<a target="_blank" class="color615f9e font11" href="http://i-meet.com/pages/group/ptgroup.aspx?groupid='+groupId+'">View All</a>');
	            }
	            else{
	                $("#gt_info").hide();
	            }
	            // hide member list
	            $("#widget_member_list").hide();
                $("#widget_topic_list").hide();
	            // add preLoader to container element
        //		var pl = $('<p id="'+o.preloaderId+'">'+o.loaderText+'</p>');
        //		$(this).append(pl);

	            // add Twitter profile link to container element
		       // $("#profileLink").append('<a class="font11" target="_blank" href="http://i-meet.com/pages/group/ptgroup.aspx?groupid='+groupId+'" style="color:#ffffff;">Join us for more:<br> <span class="wordwrap">Photos,Videos,Blogs,Links and Specials.</span></a>');
	            $("#imLogo").append('<a href="http://www.i-meet.com" target="_blank" ><img style="border:0;width:66px;margin-top:10px;" src="http://i-meet.com/Images/new-add/widget_logo.png"></a>');
	            // show container element
	            $("#"+objid).show();
	            
	            var mcount = 1;
	            if(memberCount>0){
	                mcount = memberCount;
	            }
        		
	            $.getJSON(
                    'http://i-meet.com/AjaxServer/GroupMemberWidget.ashx?callback=?',
                    {act:'member',gid:groupId,mc:mcount},
                    function(json){
                        if(memberCount>-2){
                            if(memberCount> 0){
                                if(groupId !=542){
                                 $.each(json,function(i){
                                     var html = '<div class="memberbox">';
                                     html += '<div class="height_50"><a target="_blank" href="http://i-meet.com/pages/profiles/myprofile.aspx?userid='+json[i].UserId+'" title="'+json[i].Name+'"><img class="img" src="http://i-meet.com/' + json[i].Photo + '"></a></div>'; 
                                     html += '</div>'; 
                                     $('#widget_member_list').append(html);  
                                     $("#widget_member_list").show();
                                     });
                                 }else{
                                    $("#gmViewAll").html("");
                                 }
                             }
                             if(json.length>0)
                             {
                                $("#spnAllMember").html(json[0].TotalCount);
                             
                                $("#widget_member_list").find("a").bind("click",function() {
                                    $.pageClick(groupId);
                                 });
                             }
                             
                         }
                    });
        	        
                var tCount = 1;
                if(topicCount >0){
                    tCount = topicCount;
                }
                
                 $.getJSON(
                    'http://i-meet.com/AjaxServer/GroupMemberWidget.ashx?callback=?',
                    {act:'topic',gid:groupId,tc:tCount},
                    function(json){
                        if(topicCount>-1){
                            if(topicCount>0){
                                 $.each(json,function(i){
                                 var html ='';
                                 html += '<p><a target="_blank" href="http://i-meet.com/pages/group/ptgroup_viewtopic.aspx?groupid='+groupId+'&topicid='+ json[i].TopicId +'">' + json[i].TopicTitle + '</a></p>';  
                                 $('#widget_topic_list').append(html);  
                                 $("#widget_topic_list").show();
                                 });
                             }
                             if(json.length>0)
                             {
                                 $("#widget_topic_list").find("a").bind("click",function() {
                                        $.pageClick(groupId);
                                     });
                             }
                         }
                    });
                 $("#"+objid).find("a").bind("click",function() {
                        $.pageClick(groupId);
                     });

          }
     });

})(jQuery);