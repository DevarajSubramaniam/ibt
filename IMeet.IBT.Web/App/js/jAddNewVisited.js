﻿var AddNewVisited = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                AddNewVisited.scrolling = true;
                AddNewVisited.GetDataList(AddNewVisited.page);
                AddNewVisited.page++;
            }
        });
    },
    GetDataList: function (page) {
        //if ($('#talRecommend').children('li').length >= 100) {
        //    return false;
        //}
        if (this.itemTotal == 0) return;
        $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#talRecommend');
        var json_data = { page: page, pageSize: AddNewVisited.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val(), SearchKeyword: $("#SearchKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Admin/Home/VisitedListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    AddNewVisited.scrolling = false;
                    AddNewVisited.itemTotal = json.Item.itemTotal;
                    $("#AddNewRecommend_count").text(json.Item.total);
                    $('#talVisited').append(json.Message);
                    $("#count").html(json.Item.itemTotal);
                    AddNewVisited.txtSort();
                    $('.alignC').remove();
                    checkboxshow();
                    // Initialise the table
                    $("#talVisited").tableDnD({
                        onDrop: function (table, row) {
                            var rows = table.rows;
                            var debugStr = "";
                            var strId = "";
                            for (var i = 0; i < rows.length; i++) {
                                debugStr += rows[i].id + "-" + i.toString() + ",";
                                strId += rows[i].id + ",";
                            }
                            $("#hidArray").val(strId);
                        }
                    });
                    // Make a nice striped effect on the table
                    $("#talVisited tr:even").addClass("trBg");
                    //if ($('#talRecommend').children('tr').length == 0) {
                    //    $('<tr class="noData" style="text-align:center;"><td>no data!</td></tr>').appendTo('#talRecommend');
                    //}
                }
            }
        });
    },
    initData: function () {
        $("#talVisited").empty();
        AddNewVisited.GetDataList(1);
        AddNewVisited.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#talVisited").empty();//先删除以前的
        AddNewVisited.scrolling = false;
        AddNewVisited.itemTotal = -1;
        $('.noData').remove();
        AddNewVisited.GetDataList(1);
    },
    saveSort: function () {
        var json_data = { arrayData: $("#hidArray").val() };
        $.ajax({
            url: '/Admin/Home/PaiXuVisited', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    txtSort: function () {
        $(".cssTaskSn").click(function () {//给cssTaskSn加上click
            var objTd = $(this);//保存对象
            var oldText = $.trim(objTd.text());//保存原来的文本
            var input = $("<input id='txtorderby'  type='text' value='" + oldText + "' style='width:30px;text-align:center;' />");//定义input变量为一个文本框把原来文本写入
            objTd.html(input)//当前td变为文本框，把原来文本写入
            //设置文本框点击事件失效
            input.click(function () {
                return false;
            });
            //设置select全选文本事件,先触发焦点
            input.trigger("focus").trigger("select");

            //文本框失去焦点变回文本
            input.blur(
            function () {
                var newText = $(this).val();
                objTd.html(newText);
                AddNewVisited.SetVisitedSort($("#hidMVPId").val(), newText);  //设置排序数字
                var taskSnId = $.trim((objTd.prev()).prev().text());
                //alert(taskSnId);//这里显示的是空的，不知道怎么获取这个id,这个idq我是绑在一个隐藏的文本框上的见html代码和aspx代码。
            });
        });
    },
    //按字段排序
    sortbyTitle: function (obj) {
        $("#talVisited").empty();
        AddNewVisited.scrolling = false;
        AddNewVisited.itemTotal = -1;
        $('.noData').remove();
        AddNewVisited.GetDataList(1);
    },
    sortbyNum: function (obj) {
        $("#talVisited").empty();
        AddNewVisited.scrolling = false;
        AddNewVisited.itemTotal = -1;
        $('.noData').remove();
        AddNewVisited.GetDataList(1);
    },
    SetVisitedSort: function (objId, newText) {
        var json_data = { objId: objId, newText: newText };
        $.post("/Admin/Home/SetVisitedSort", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Modify SortNum" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Modify SortNum" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    DelVisited: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetVisitedIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del VISITED" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del VISITED" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false; }
    },
    AddVisitedItem: function (objId, objType, scroe) {
        if (confirm('Do you really want to add this search data?')) {
            var json_data = { objId: objId, objType:objType, scroe: scroe };
            $.post("/Admin/Home/AddVisitedItem", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add VISITED" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add VISITED" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }


    },
    save: function () {
        var json_data = { SupplyId: $("#SupplyId").val(), UserId: $("#UserId").val() };
        $.post("/Admin/Home/AddVisited", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Add New Visited" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Add New Visited" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
    }
};

