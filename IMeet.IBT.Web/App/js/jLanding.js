﻿var LandingManage = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (LandingManage.scrolling == true) {
                    LandingManage.GetDataList(LandingManage.page);
                    LandingManage.page++;
                }           
            }
        });
    },
    GetDataList: function (page) {
        LandingManage.scrolling = false;
        if (this.itemTotal == 0) { LandingManage.scrolling = false; return };
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#talSupply');
        var json_data = { page: page, pageSize: LandingManage.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val() };
        $.ajax({
            url: '/Admin/Home/LandingListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    LandingManage.scrolling = false;
                    LandingManage.itemTotal = json.Item.itemTotal;
                    $('#talSupply').append(json.Message);
                    $('.loading').remove();
                    LandingManage.scrolling = true;
                    // Initialise the table
                    $("#talSupply").tableDnD({
                        onDrop: function (table, row) {
                            var rows = table.rows;
                            var debugStr = "";
                            var strId = "";
                            for (var i = 0; i < rows.length; i++) {
                                debugStr += rows[i].id + "-" + i.toString() + ",";
                                strId += rows[i].id + ",";
                            }
                            $("#hidArray").val(strId);
                        }
                    });
                    // Make a nice striped effect on the table
                    $("#talSupply tr:even").addClass("trBg");
                }
            }
        });
    },
    initSupplyData: function () {
        $("#talSupply").empty();
        LandingManage.GetDataList(1);
        LandingManage.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#talSupply").empty();//先删除以前的
        LandingManage.scrolling = false;
        LandingManage.itemTotal = -1;
        $('.noData').remove();
        LandingManage.GetDataList(1);
    },
    DelIndexSupply: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetLandingIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del IndexTop10Supplies" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del IndexTop10Supplies" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false; }


    },
    AddIndexItem: function (objId, objType, scroe) {
        if (confirm('Do you really want to add this search data?')) {
            var json_data = { objId: objId, objType:objType, scroe: scroe };
            $.post("/Admin/Home/AddIndexItem", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add IndexTop10Supplies" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add IndexTop10Supplies" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }


    },
    saveSort: function () {
        var json_data = { arrayData: $("#hidArray").val() };
        $.ajax({
            url: '/Admin/Home/SetIndexSupplySort', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Set Sort" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Set Sort" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    Save: function (type) {
        var json_data = { IndexId: $("#hidIndexId").val(), imgSrc: $("#imgSave").val(), txtDesc: $("#txtDesc").val() };
        $.post("/Admin/Home/EditIndexSupply", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Edit IndexSupply" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Edit IndexSupply" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
    },
    uploadImg: function () {
        $('#uploadImage').uploadify({
            'buttonImage': '/App/images/button/btn_choose_file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 200,
            'height': 24,
            'uploader': '/Admin/Home/UploadImg?userId=' + $("#UserId").val() + '&SupplyId=' + $("#hidSupplyId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                $("#imgPreview").removeAttr("src");
                $("#imgPreview").attr("src", data.SaveName + "?" + Math.random(10000));
                $("#imgSave").attr("value", data.SaveName);
            }
        });
    }
};

