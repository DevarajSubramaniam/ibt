﻿var AddNewDestination = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                AddNewDestination.scrolling = true;
                AddNewDestination.GetDataList(AddNewDestination.page);
                AddNewDestination.page++;
            }
        });
    },
    GetDataList: function (page) {
        //if ($('#talDes').children('li').length >= 100) {
        //    return false;
        //}
        if (this.itemTotal == 0) return;
        $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#talDes');
        var json_data = { page: page, pageSize: AddNewDestination.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val(), SearchKeyword: $("#SearchKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Admin/Home/DestinationListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    AddNewDestination.scrolling = false;
                    AddNewDestination.itemTotal = json.Item.itemTotal;
                    $("#AddNewDestination_count").text(json.Item.total);
                    $('#talDes').append(json.Message);
                    $("#count").html(json.Item.itemTotal);
                    AddNewDestination.txtSort();
                    $('.alignC').remove();
                    checkboxshow();
                    // Initialise the table
                    $("#talDes").tableDnD({
                        onDrop: function (table, row) {
                            var rows = table.rows;
                            var debugStr = "";
                            var strId = "";
                            for (var i = 0; i < rows.length; i++) {
                                debugStr += rows[i].id + "-" + i.toString() + ",";
                                strId += rows[i].id + ",";
                            }
                            $("#hidArray").val(strId);
                        }
                    });
                    // Make a nice striped effect on the table
                    $("#talDes tr:even").addClass("trBg");
                    //if ($('#talDes').children('tr').length == 0) {
                    //    $('<tr class="noData" style="text-align:center;"><td>no data!</td></tr>').appendTo('#talDes');
                    //}
                }
            }
        });
    },
    initData: function () {
        $("#talDes").empty();
        AddNewDestination.GetDataList(1);
        AddNewDestination.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#talDes").empty();//先删除以前的
        AddNewDestination.scrolling = false;
        AddNewDestination.itemTotal = -1;
        $('.noData').remove();
        AddNewDestination.GetDataList(1);
    },
    saveSort: function () {
        var json_data = { arrayData: $("#hidArray").val() };
        $.ajax({
            url: '/Admin/Home/PaiXuDestination', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    txtSort: function () {
        $(".cssTaskSn").click(function () {//给cssTaskSn加上click
            var objTd = $(this);//保存对象
            var oldText = $.trim(objTd.text());//保存原来的文本
            var input = $("<input id='txtorderby'  type='text' value='" + oldText + "' style='width:30px;text-align:center;' />");//定义input变量为一个文本框把原来文本写入
            objTd.html(input)//当前td变为文本框，把原来文本写入
            //设置文本框点击事件失效
            input.click(function () {
                return false;
            });
            //设置select全选文本事件,先触发焦点
            input.trigger("focus").trigger("select");

            //文本框失去焦点变回文本
            input.blur(
            function () {
                var newText = $(this).val();
                objTd.html(newText);
                AddNewDestination.SetDestinationSort($("#hidDWeekId").val(), newText);  //设置排序数字
                var taskSnId = $.trim((objTd.prev()).prev().text());
                //alert(taskSnId);//这里显示的是空的，不知道怎么获取这个id,这个idq我是绑在一个隐藏的文本框上的见html代码和aspx代码。
            });
        });
    },
    //按字段排序
    sortbyTitle: function (obj) {
        $("#talDes").empty();
        AddNewDestination.scrolling = false;
        AddNewDestination.itemTotal = -1;
        $('.noData').remove();
        AddNewDestination.GetDataList(1);
    },
    sortbyNum: function (obj) {
        $("#talDes").empty();
        AddNewDestination.scrolling = false;
        AddNewDestination.itemTotal = -1;
        $('.noData').remove();
        AddNewDestination.GetDataList(1);
    },
    SetDestinationSort: function (objId, newText) {
        var json_data = { objId: objId, newText: newText };
        $.post("/Admin/Home/SetDestinationSort", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Modify SortNum" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Modify SortNum" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    DelDWeek: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetDestinationIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del Destination" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del Destination" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false; }
    },
    editInfo: function (objId) {
        var json_data = { dWeekId: objId };
        $.ajax({
            url: '/Admin/Home/GetItemTitle', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $('#SupplierName').attr("value", json.Message);
                }
            }
        });
    },
    save: function (type) {
        if (type == "") {
            var json_data = { Desp: $("#Desp").val(), imgSave: $("#imgSave").val(), objId: $("#ObjId").val(), objType: $("#ObjType").val(), UserId: $("#UserId").val() };
            $.post("/Admin/Home/AddNewDestinationItem", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add New Destination" });
                    //$(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add New Destination" });
                    //$(".boxyInner").addClass("popup");
                }
            }, "json");
        }
        else {
            var json_data = { dWeekId: $("#dWeekId").val(), sort: $("#hidSort").val(), Desp: $("#Desp").val(), imgSave: $("#imgSave").val(), objId: $("#ObjId").val(), UserId: $("#UserId").val() };
            $.post("/Admin/Home/EditDestination", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Edit Destination" });
                    //$(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Edit Destination" });
                    //$(".boxyInner").addClass("popup");
                }
            }, "json");
        }

    }
};

