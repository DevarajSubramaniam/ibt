﻿var AddNewRecommend = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                AddNewRecommend.scrolling = true;
                AddNewRecommend.GetDataList(AddNewRecommend.page);
                AddNewRecommend.page++;
            }
        });
    },
    GetDataList: function (page) {
        //if ($('#talRecommend').children('li').length >= 100) {
        //    return false;
        //}
        if (this.itemTotal == 0) return;
        $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#talRecommend');
        var json_data = { page: page, pageSize: AddNewRecommend.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val(), SearchKeyword: $("#SearchKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Admin/Home/RecommendListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    AddNewRecommend.scrolling = false;
                    AddNewRecommend.itemTotal = json.Item.itemTotal;
                    $("#AddNewRecommend_count").text(json.Item.total);
                    $('#talRecommend').append(json.Message);
                    $("#count").html(json.Item.itemTotal);
                    AddNewRecommend.txtSort();
                    $('.alignC').remove();
                    checkboxshow();
                    // Initialise the table
                    $("#talRecommend").tableDnD({
                        onDrop: function (table, row) {
                            var rows = table.rows;
                            var debugStr = "";
                            var strId = "";
                            for (var i = 0; i < rows.length; i++) {
                                debugStr += rows[i].id + "-" + i.toString() + ",";
                                strId += rows[i].id + ",";
                            }
                            $("#hidArray").val(strId);
                        }
                    });
                    // Make a nice striped effect on the table
                    $("#talRecommend tr:even").addClass("trBg");
                    //if ($('#talRecommend').children('tr').length == 0) {
                    //    $('<tr class="noData" style="text-align:center;"><td>no data!</td></tr>').appendTo('#talRecommend');
                    //}
                }
            }
        });
    },
    initData: function () {
        $("#talRecommend").empty();
        AddNewRecommend.GetDataList(1);
        AddNewRecommend.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#talRecommend").empty();//先删除以前的
        AddNewRecommend.scrolling = false;
        AddNewRecommend.itemTotal = -1;
        $('.noData').remove();
        AddNewRecommend.GetDataList(1);
    },
    saveSort: function () {
        var json_data = { arrayData: $("#hidArray").val() };
        $.ajax({
            url: '/Admin/Home/PaiXuRecommend', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    txtSort: function () {
        $(".cssTaskSn").click(function () {//给cssTaskSn加上click
            var objTd = $(this);//保存对象
            var oldText = $.trim(objTd.text());//保存原来的文本
            var input = $("<input id='txtorderby'  type='text' value='" + oldText + "' style='width:30px;text-align:center;' />");//定义input变量为一个文本框把原来文本写入
            objTd.html(input)//当前td变为文本框，把原来文本写入
            //设置文本框点击事件失效
            input.click(function () {
                return false;
            });
            //设置select全选文本事件,先触发焦点
            input.trigger("focus").trigger("select");

            //文本框失去焦点变回文本
            input.blur(
            function () {
                var newText = $(this).val();
                objTd.html(newText);
                AddNewRecommend.SetRecommendSort($("#hidMRPId").val(), newText);  //设置排序数字
                var taskSnId = $.trim((objTd.prev()).prev().text());
                //alert(taskSnId);//这里显示的是空的，不知道怎么获取这个id,这个idq我是绑在一个隐藏的文本框上的见html代码和aspx代码。
            });
        });
    },
    //按字段排序
    sortbyTitle: function (obj) {
        $("#talRecommend").empty();
        AddNewRecommend.scrolling = false;
        AddNewRecommend.itemTotal = -1;
        $('.noData').remove();
        AddNewRecommend.GetDataList(1);
    },
    sortbyNum: function (obj) {
        $("#talRecommend").empty();
        AddNewRecommend.scrolling = false;
        AddNewRecommend.itemTotal = -1;
        $('.noData').remove();
        AddNewRecommend.GetDataList(1);
    },
    SetRecommendSort: function (objId, newText) {
        var json_data = { objId: objId, newText: newText };
        $.post("/Admin/Home/SetRecommendSort", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Modify SortNum" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Modify SortNum" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    DelRecommend: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetRecommendIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del Recommend" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del Recommend" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false; }
    },
    AddRecommendItem: function (objId, objType, scroe) {
        if (confirm('Do you really want to add this search  data?')) {
            var json_data = { objId: objId, objType:objType, scroe: scroe };
            $.post("/Admin/Home/AddRecommendItem", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add IndexTop10Supplies" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add IndexTop10Supplies" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }


    },
    save: function () {
        var json_data = { SupplyId: $("#SupplyId").val(), UserId: $("#UserId").val() };
        $.post("/Admin/Home/AddNewRecommend", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Add New Recommend" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Add New Recommend" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
    }
};

