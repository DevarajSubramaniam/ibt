﻿var indexMaps = {
    idx: 0,
    map: null,
    source: null,
    markersArray: [],
    indexInfoBoxOptions: {
        content: ""
        , disableAutoPan: false
        , maxWidth: 0
        , pixelOffset: new google.maps.Size(-140, 0)
        , zIndex: null
        , boxStyle: {
            background: "url('/images/map_tips_t.png') no-repeat"
            , opacity: 0.85
            , width: "308px"
            , padding: "11px 0 0"
        }
        , closeBoxMargin: "0 8px -13px"
        , closeBoxURL: "/images/close.gif"
        , infoBoxClearance: new google.maps.Size(20, 20)
        , isHidden: false
        , pane: "floatPane"
        , enableEventPropagation: true
    },
    init: function (locations) {
        var myLatlng = new google.maps.LatLng(locations[0][1], locations[0][2]);
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        indexMaps.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        indexMaps.indexSetMarkers(indexMaps.map, locations);
    },
    indexSetMarkers:function(map, locations) { //locations:['tianhe', 23.1246550, 113.3612240, 4, 1681, '/images/icon/restaurant2.png'],
        for (var i = 0; i < locations.length; i++) { //从第一个开始，第一个单独处理
            var beach = locations[i];
            var image = new google.maps.MarkerImage(beach[6]);
            var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                title: beach[0],
                objId: beach[4],
                objType: beach[5],
                topN: beach[3]
            });

            //indexMaps.markersArray.push(marker);

            google.maps.event.addListener(marker, "click", function (e) {
                $(".infoBox:visible").detach();
                var boxText = document.createElement("div");
                boxText.setAttribute("id", "boxId" + this.objId);
                //boxText.className = 'mapTips';
                boxText.innerHTML = indexMaps.getIndexSupplyInfo(this.objId, this.objType, this.topN);
                indexMaps.indexInfoBoxOptions["content"] = boxText;
                var ib = new InfoBox(indexMaps.indexInfoBoxOptions);
                ib.open(map, this);

                indexMaps.idx = parseInt(this.topN) - 1;
            });

            if (i == 0) {
                indexMaps.idx = 0;
                var boxText = document.createElement("div");
                boxText.setAttribute("id", "boxId" + beach[4]);
                //boxText.className = 'mapTips';
                boxText.innerHTML = indexMaps.getIndexSupplyInfo(beach[4], beach[5], beach[3]);
                indexMaps.indexInfoBoxOptions["content"] = boxText;

                var ib = new InfoBox(indexMaps.indexInfoBoxOptions);
                ib.open(map, marker);
            }
        } //end:for

        /*
        http://stackoverflow.com/questions/5429444/google-maps-api-v3-event-mouseover-with-infobox-plugin
        google.maps.event.addListener(_point.popup, 'domready', function () {
            //Have to put this within the domready or else it can't find the div element (it's null until the InfoBox is opened)
            $(_point.popup.div_).hover(
                function () {
                    //This is called when the mouse enters the element
                },
                function () {
                    //This is called when the mouse leaves the element
                    _point.popup.close();
                }
            );
        });
        */
    },
    getIndexSupplyInfo: function (objId, objType, topN) {
        var json_data = { objId: objId, objType:objType, topN: topN };
        var res = "";
        $.ajax({
            url: "/Map/MapsIndexSupplyInfo", data: json_data, success: function (json) {
            if (json.Success) {
                res = json.Message;
            } else {
                alert("Error!");
                res = "no data";
            }
        },dataType:"json",async:false});

        return res;
    },
    boxNextPrev: function (isNext) {
        $(".infoBox:visible").detach();
        var ix = indexMaps.idx;
        ix = isNext ? ix + 1 : ix - 1;
        ix = ix < 0 ? indexMaps.source.length-1 : ix;
        ix = ix >= indexMaps.source.length ? 0 : ix;
        indexMaps.idx = ix;
        var beach = indexMaps.source[ix];
        var image = new google.maps.MarkerImage(beach[6]);
        var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: indexMaps.map,
            icon: image,
            title: beach[0],
            objId: beach[4],
            objType: beach[5],
            topN: beach[3]
        });
        //indexMaps.markersArray.push(marker);

        var boxText = document.createElement("div");
        boxText.setAttribute("id", "boxId" + beach[4]);
        //boxText.className = 'mapTips';
        boxText.innerHTML = indexMaps.getIndexSupplyInfo(beach[4], beach[5], beach[3]);
        indexMaps.indexInfoBoxOptions["content"] = boxText;

        google.maps.event.addListener(marker, "click", function (e) {
            $(".infoBox:visible").detach();
            var boxText = document.createElement("div");
            boxText.setAttribute("id", "boxId" + this.objId);
            //boxText.className = 'mapTips';
            boxText.innerHTML = indexMaps.getIndexSupplyInfo(this.objId, this.objType, this.topN);
            indexMaps.indexInfoBoxOptions["content"] = boxText;
            var ib = new InfoBox(indexMaps.indexInfoBoxOptions);
            ib.open(indexMaps.map, this);

            indexMaps.idx = parseInt(this.topN) - 1;
        });

        var ib = new InfoBox(indexMaps.indexInfoBoxOptions);
        ib.open(indexMaps.map, marker);

    },
    clearOverlays: function () {
        for (var i = 0; i < indexMaps.markersArray.length; i++) {
            indexMaps.markersArray[i].setMap(null);
        }
    }
};