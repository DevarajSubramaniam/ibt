// JavaScript Document

$(document).ready(function () {
   // PersonalPage.init();
});

var PersonalPage = {
    getData: function () {
        data = {
            //PPID
            PPID: $('#txtPPID').val(),
            //First Name
            FName: $('#pp_txtFname').val(),
            //Last Name
            LName: $('#pp_txtLname').val(),
            //DisplayLinks checkBox
            DisplayLinks: $('#giLinks').prop('checked'),
            //DisplayIndustryProfessional checkBox
            DisplayIndustryProfessional: $('#giIndustPrff').prop('checked'),
            //@PublishMeetingExperiences checkBox
            PublishMeetingExperiences: $('#meChkDisplay').prop('checked'),
            //@DisplayMeetingSiteManagement checkBox
            DisplayMeetingSiteManagement: $('#meOnsiteMngm').prop('checked'),
            //@MeetingSiteManagement
            MeetingSiteManagement: DSlider.getLevel('MeetingSiteManagement'),
            //@DisplayMeetingSiteInspection
            DisplayMeetingSiteInspection: $('#meSiteInspec').prop('checked'),
            //@MeetingSiteInspection
            MeetingSiteInspection: DSlider.getLevel('MeetingSiteInspection'),
            //@DisplayMeetingBusinessTrips
            DisplayMeetingBusinessTrips: $('#meBusinTrips').prop('checked'),
            //@MeetingBusinessTrips
            MeetingBusinessTrips: DSlider.getLevel('MeetingBusinessTrips'),
            //@DisplayMeetingDestinationFamiliarization
            DisplayMeetingDestinationFamiliarization: $('#meDestinFamilr').prop('checked'),
            //@MeetingDestinationFamiliarization
            MeetingDestinationFamiliarization: DSlider.getLevel('MeetingDestinationFamiliarization'),
            //@DisplayMeetingSpecialties
            DisplayMeetingSpecialties: $('#meSpecialties').prop('checked'),
            //@MeetingSpecialties
            MeetingSpecialties: $('#txtMeetingSpecialties').val(),
            //@DisplayMeetingChallenges
            DisplayMeetingChallenges: $('#meMeetingChllg').prop('checked'),
            //@IsMeetingPolitical
            IsMeetingPolitical: $('#mceCbPolitic').prop('checked'),
            //@IsMeetingNonHotelVenue
            IsMeetingNonHotelVenue: $('#mceCbNonHtVn').prop('checked'),
            //@IsMeetingStrike
            IsMeetingStrike: $('#mceCbStrike').prop('checked'),
            //@IsMeetingGroup500Attendees
            IsMeetingGroup500Attendees: $('#mceCbGrpAttnd').prop('checked'),
            //@IsMeetingNatural
            IsMeetingNatural: $('#mceCbNaturalDis').prop('checked'),
            //@IsMeetingVirtualComponent
            IsMeetingVirtualComponent: $('#mceCbVirtComp').prop('checked'),
            //@IsMeetingGlobalAudience
            IsMeetingGlobalAudience: $('#mceCbGlobAud').prop('checked'),
            //@IsMeetingCelebrity
            IsMeetingCelebrity: $('#mceCbCelbPolict').prop('checked'),
            //@PublishBookingContracting
            PublishBookingContracting: $('#bookCntrExp').prop('checked'),
            //@DisplayBookingEventsSources
            DisplayBookingEventsSources: $('#bkEventsSourced').prop('checked'),
            //@BookingEventsSources
            BookingEventsSources: DSlider.getLevel('BookingEventsSources'),
            //@BookingEventsSources
            BookingSpecialties: $('#txtBookingEventsSources').val(),
            //@DisplayBookingSpecialties
            DisplayBookingSpecialties: $('#bkSpecialities').prop('checked'),
            //@DisplayBookingVenuesContracted
            DisplayBookingVenuesContracted: $('#bkVenCntr').prop('checked')
        };
        return data;
    },
    init: function () {
    },
    save: function () {
        // alert('Save function');
        //var json_data = $(page).MySerialize();

        // upDateInfo = { pp: { name: 'AlirezA', inputemail: 'asasa@wqw.com' }};
        $.ajax({
            type: 'POST',
            url: '/PersonalProfile/update',
            data: this.getData(),
            //data: JSON.stringify({ user: { name: 'Rintu', email: 'Rintu@gmial.com' } }),
            datatype: "json",
            //contentType: 'application/json; charset=utf-8',
            //async: false,
            //processData: false,
            //cache: false,
            success: function (bm) {
                alert(bm.Message);
                //data, textStatus, xhr)
            },
            error: function (bm) {
                alert('There is an error');
                //alert(bm);
            }
        });


        //$.post("update", json_data, function (json) {
        //    if (json.Success) {
        //        //location.reload();
        //        //window.location = "RegisterSuccess";
        //        //location.href = '/account/welcome';
        //    } else {
        //        //$("#tipMsg").html(json.Message);
        //        //Boxy.alert(json.Message, null, { title: "SignUp" });
        //        //$(".boxyInner").addClass("popup");
        //    }
        //}, "json");
    }
};

var DSlider = {
    init: function () {
        $('.btn-pfadmScbrdPlatform').click(function () {
            DSlider.DragLevel(this);
        });
    },
    //set slider Level
    setLevel: function (SliderID, stepNo) {
        //parrent Node
        var PNode = $('#' + SliderID).children('.pfadmScbrdHolder').find('.pfadmScbrdBased');
        //FOR pfadmScbrdHghlghts
        var FNode = PNode.find('.pfadmScbrdHghlghtBase').find('.pfadmScbrdHghlghts');
        FNode.removeClass();
        FNode.addClass('pfadmScbrdHghlghts');
        FNode.addClass('pfadmScbrdHglgh0' + stepNo);
        //FOR  pfadmScbrdButton
        var LNode = PNode.find(">:nth-child(3)");
        LNode.removeClass();
        LNode.addClass('pfadmScbrdButton');
        LNode.addClass('pfadmScbrdBtn0' + stepNo);
        //steps
        var stepsList = PNode.find('.pfadmScbrdPltfrmBased');
        for (var i = 1; i <= 5; i++) {
            var stepPoint = stepsList.find(">:nth-child(" + i + ")").find('.btn-pfadmScbrdPlatform');
            stepPoint.removeClass();
            stepPoint.addClass('btn-pfadmScbrdPlatform');
            if (i <= stepNo) {
                stepPoint.addClass('pfadmScbrdPltformAct');
            }
        }
    },

    getLevel: function (SliderID) {
        var Level = $('#' + SliderID).find('.pfadmScbrdHghlghts').attr('class').substring(35);
        return Level;
    },
    DragLevel: function (levelObj) {
        var stepNo = $(levelObj).closest("span").attr('class').substring(4);
        //Find the current slider ID
        var SliderID = $(levelObj).closest('ul').parent().parent().parent().attr('id');
        this.setLevel(SliderID, stepNo);
    }



};