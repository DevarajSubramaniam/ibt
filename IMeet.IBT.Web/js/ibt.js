﻿var ibt = {
    IBTType_Country: 1,
    IBTType_City: 2,
    IBTType_Supply: 3,
    rorate_attid:0,
    mybox: null,
    isCall:false,
    canPost: true,
    popClose: function (obj) {
        //myboxClose.
        Boxy.get(obj).hideAndUnload();        
    },

    login: function () {
        if ($('#pop_login').data('bValidator').validate() == false) {
            return;
        }

        var json_data = { pop_username: $("#pop_username").val(), pop_password: $("#pop_password").val(), createPersistentCookie: $("#createPersistentCookie").val() } //$("#pop_login").MySerialize();
        $.post("/Account/LogOn", json_data, function (json) {
            if (json.Success) {
                /*如果是在注册页面登录，跳转到home页面。其他页面重新加载*/
                if (document.URL.toLowerCase().indexOf("/account/signup") > 1) {            /*注册页面*/
                    window.location = "/home";
                } else if (document.URL.toLocaleLowerCase().indexOf("/home/index") > 1) {        /*about页面*/
                    window.location = "/home";
                } else {
                    location.reload();
                }
            } else {
                $("#tipMsg").removeClass("error3");
                $("#tipMsg").addClass("error2");
                $("#tipMsg").html(json.Message);
            }
        }, "json");

    },
    IveBeenThere: function (url, type, json_data, funCallBack, title) {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }
        ibt.loadUrlPop(url, type, json_data, funCallBack, title);
    },
    loadUrlPop: function (url, type, json_data, funCallBack, title) {
       
        if (funCallBack == undefined) { funCallBack = null; }
        if (json_data == undefined) { json_data = ""; }
        if (type == undefined) { type = "Post"; }
        if (title == undefined) { title = ""; }
        //if (UserId == 0) {
        //    ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
        //    return;
        //}
        $.ajax({
            url: url, data: json_data, async: false, dataType: "text", type: type, cache: false, timeout: 8000,
            success: function (txt) {
                ibt.mybox = new Boxy(txt, {
                    title: title, modal: true, center: true, unloadOnHide: true, closeText: "X", afterHide: function (e) {
                        ibt.mybox = null; if (ibt.isCall) {
                            ibt.isCall = false;                            
                            if (typeof (funCallBack) == "function") { try { funCallBack("show_1"); } catch (e) { } }
                        }
                    }, afterShow: function (r) {
                        ibt.mybox = this;
                        if (typeof (funCallBack) == "function") { try { funCallBack(); } catch (e) { } } 
                        //if (isIE6 == "True") {
                        //    DD_belatedPNG.fix('.jscroll-c,.jscroll-e,.jscroll-h,.jscroll-hD,.jscroll-u,.jscroll-d,icon');
                        //}
                    }
                });
                $(".boxyInner").addClass("popup");
                var scorlltop = $(document).scrollTop();
                $(".boxyWrapper").css("top", scorlltop + 95 + "px")
                checkboxshow();
                selectSpan();                inputText();

                ibt.mybox.center();
                //var box_offset = $("#").offset();
                //mybox.moveTo(box_offset.left + box_left, box_offset.top + box_top);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统繁忙，请刷新重试！");
            }
        });
    }
    ,
    /* 页面的checkbox的取值 */
    getCheckboxVal: function (selector, dataId) {
        var vals = "";
        $(selector).each(function () {
            if ($(this).hasClass("uncheckbox")) {
                vals = vals + $(this).attr(dataId) + ",";
            }
        });
        if (vals.length > 0) {
            vals = vals.substring(0, vals.length - 1);
        }
        return vals;
    }
    ,
    /*
    * rrwbType 1.rating 2.Recommended 3.Want to Go/Go Back 4.Bucket List
    * ibtType: 1.Country 2.City 3.Supply
    */
    rrwb: function (postType, rrwbType, ibtType, objId, val) {
        var json_data = { postType: postType, ibtType: ibtType, rrwbType: rrwbType, objId: objId, val: val };
        $.post("/Supplier/RRWB", json_data, function (json) {
            if (json.Success) {
                if (postType == "1") {
                    location.reload();
                }
            }
        }, "json");
    },
    rrwb_rating: function (postType, ibtType, objId, val) {
        this.rrwb(postType, 1, ibtType, objId, val);
    },
    rrwb_rwb: function (obj) {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }

        if ($(obj).attr("data-readonly") == "true") {
            return;
        }
        var postType = $(obj).attr("data-postType"); //new
        var rrwbType = $(obj).attr("data-rrwbType");
        var ibtType = $(obj).attr("data-ibtType");
        var objId = $(obj).attr("data-objId");
        var d_val = $(obj).attr("data-val");
        if (d_val == "1") {
            $(obj).removeClass("uncheckbox");
            
            $(obj).attr("data-val", "0");
        }
        else {
            $(obj).addClass("uncheckbox");
            $(obj).attr("data-val", "1");
        }

        this.rrwb(postType, rrwbType, ibtType, objId, $(obj).attr("data-val"));
    },
    ibtRating: function () {
        $('.IBTRating').raty({
            readOnly: false,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                if (UserId == 0) {
                    ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                    return;
                }
                ibt.rrwb_rating(0, $(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
            }
        });
    },
    homeDwRating: function () {
        $('#DwSupplyRating').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-rating');
            }
        });
    },
    pvRating: function (objId) {
        if (objId == "0") {
            $('.IBTRating').raty({
                readOnly: false,
                score: function () {
                    return $(this).attr('data-rating');
                },
                click: function (score, evt) {
                    if (UserId == 0) {
                        ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                        return;
                    }
                    ibt.rrwb_rating(0, $(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
                }
            });
        }
        else {
            $('.IBTRating').raty({
                readOnly: true,
                score: function () {
                    return $(this).attr('data-rating');
                },
                click: function (score, evt) {
                    if (UserId == 0) {
                        ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                        return;
                    }
                    ibt.rrwb_rating(0, $(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
                }
            });
        }
    },
    PVselDate: function (obj, id) {
        $("#" + id).attr("data-item", $(obj).attr("data-item"));
        $("#selDate").val("true");
        $("#" + id).text($(obj).text());
    },
    shareOn: function () {
        var draggable = $("#postVisit .switch .draggable");
        draggable.click(function () {
            if ($(this).attr("data-status") == "off") {
                var name = $(this).attr("data-name");
                var name1 = name;
                if (name == "IMeet") {
                    name1 = "I-Meet";
                }
                //Boxy.confirm('You will need to connect your ' + name1 + ' account to start sharing! Do you want to connect to ' + name1 + ' ?', function () {
                //    //window.open("/sns/" + name, "target = 'blank'");
                //    var form = $("<form>").attr("method", "post").attr("target", "_blank").attr("action", "/sns/" + name);
                //    $(form).appendTo(document.body).submit();
                //    $(form).remove();
                //},
                //{ title: "Connect to " + name1 }
                //);
                //$(".boxyInner").addClass("popup");
                var boxContent = '<div id="alertBox" style=" display:none;"><div class="w300 popupMain "><div class="question">';
                boxContent += 'You will need to connect your ' + name1 + ' account to start sharing! Do you want to connect to ' + name1 + '?';
                boxContent += '</div><div class="answers"><a href="/sns/' + name + '" target="_blank">Connect</a></div></div></div>';
                var mybox = new Boxy(boxContent, {
                    title: "Connect to " + name1,
                    modal: true,
                    center: true,
                    unloadOnHide: true,
                    closeText: "X",
                    afterHide: function (e) { },
                    afterShow: function (e) { }
                });
                $(".boxyInner").addClass("popup");

                return false;
            } else {
                if ($(this).hasClass("off")) {
                    $(this).addClass("on").removeClass("off");
                }
                else {
                    $(this).addClass("off").removeClass("on");
                }
            }
        });
    },
    ShareOnCB: function () {
        var draggable = $("#postVisit .connect .checkboxBig");
        draggable.click(function () {
            if ($(this).attr("data-status") == "off") {
                var name = $(this).attr("data-name");
                var name1 = name;
                if (name == "IMeet") {
                    name1 = "I-Meet";
                }
                var boxContent = '<div id="alertBox" style=" display:none;"><div class="w300 popupMain "><div class="question">';
                boxContent += 'You will need to connect your ' + name1 + ' account to start sharing! Do you want to connect to ' + name1 + '?';
                boxContent += '</div><div class="answers"><a href="/sns/' + name + '" target="_blank">Connect</a></div></div></div>';
                var mybox = new Boxy(boxContent, {
                    title: "Connect to " + name1,
                    modal: true,
                    center: true,
                    unloadOnHide: true,
                    closeText: "X",
                    afterHide: function (e) { },
                    afterShow: function (e) { }
                });
                $(".boxyInner").addClass("popup");
                return false;
            } else {
                $(this).toggleClass("uncheckbox");
            }
        });
    },
    PvPopLoad: function () {
        if ($('#handleType').val() != "1") {
            ibt.ShareOnCB();
            ibt.shareOn();
            ibt.pvRating(0);
        }
        else {
            ibt.pvRating(1);
        }
        var IE = $.browser.msie;
        if (IE != undefined && IE) {
            $("#pv_fileList").remove();
            $("#upload_span").remove();
            $("#upload_span_flash").show();
            $('#file_upload_flash').uploadify({
                'formData': {
                    'guid': $("#pvGuid").val(),
                    'supplyId': $("#pvSupplyId").val()
                },
                'swf': '/js/uploadify/uploadify.swf',
                'uploader': '/Supplier/PostVisitUpload/',
                'buttonImage': '/images/blue_add_w85.gif',
                'fileDesc': '*.jpg;*.jpeg;*.png;*.gif',
                'fileExt': '*.jpg;*.jpeg;*.png;*.gif',
                'height': 85,
                'width': 85,
                onUploadSuccess: function (file, data, response) {
                    var json = eval('(' + data + ')');
                    var attId = json.Item;
                    $("#pv_fileList_flash").append("<span class=\"imgBox\" attId=\"" + attId + "\" ><img id='pvListImg_"+attId+"' onclick=\"ibt.select_rorate_img("+attId+",this);\" src=\"" + json.Message + "\" /><a href=\"javascript:void(0);\" onclick=\"pvRemoveImg('" + $("#pvGuid").val() + "','" + attId + "');\" class=\"delete\"><img src=\"/images/icon/delete.png\"  /></a></span>");
                    if ($("#pv_fileList>a").size() >= 4) {
                        $("#file_upload_flash").hide();
                        $("#file_upload-queue").hide();
                    }
                }
            });
        }
        else {
            $("#upload_span").show();
            $("#upload_span_flash").remove();
            $('#file_upload').fileupload({
                url: "/Supplier/PostVisitUpload/",
                formData: { "guid": $("#pvGuid").val(), 'supplyId': $("#pvSupplyId").val() },
                dataType: 'json',
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                        Boxy.alert('You must select an image(gif|jpg|jpeg|png) file only.');
                        goUpload = false;
                    }
                    if (uploadFile.size > 4000000) { // 4mb
                        Boxy.alert('Please upload a smaller image, max size is 4 MB.');
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        $("#file_progress").show();
                        data.submit();
                    }
                },
                done: function (e, data) {
                    var json = data.result;
                    var attId = json.Item;
                    $("#pv_fileList").append("<span class=\"imgBox\" attId=\"" + attId + "\" ><img id='pvListImg_" + attId + "' onclick=\"ibt.select_rorate_img(" + attId + ",this);\" src=\"" + json.Message + "\" /><a href=\"javascript:void(0);\" onclick=\"pvRemoveImg('" + $("#pvGuid").val() + "','" + attId + "');\" class=\"delete\"><img src=\"/images/icon/delete.png\"  /></a></span>");
                    if ($("#pv_fileList>a").size() >= 4) {
                        $("#file_upload").hide();
                        $("#file_upload-queue").hide();
                    }
                    $("#file_progress").hide();
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#file_progress .bar').css('width', progress + '%');
                }
            });
        }
    },
    PostVisitSave: function (obj) {
        //var year = $("#sel_year").attr("data-item");
        //var month = $("#sel_month").attr("data-item");
        //var day = $("#sel_day").attr("data-item");
        var newsfeedId = $("#newsfeedId").val();
        var activeType = $("#activeType").val();
        var year = $("#dd_year").val();
        var month = $("#dd_month").val();
        var day = $("#dd_day").val();
        var date = encodeURIComponent(year + "-" + month + "-" + day);
        var selDate = $("#selDate").val();
        var experiences = encodeURIComponent($("#experiences").val());
        var countryId = $("#countryId").val();
        var cityId = $("#cityId").val();
        var sharing = "";//todo:PostVisitSave sns list
        var guid = $("#pvGuid").val();
        var supplyId = $("#pvSupplyId").val();
        var ibtType = $("#pvIbtType").val();
        var postType = $("#postType").val();  //new
        var handleType = $("#handleType").val();  //new
        var share_facebook = 0;
        if ($("#pv_SnsList #share_on_facebook").hasClass("on")) {
            share_facebook = 1;
        }
        else if ($("#pv_SnsList #share_on_facebook").hasClass("uncheckbox")) {
            share_facebook = 1;
        }

        var share_linkedin = 0;
        if ($("#pv_SnsList #share_on_linkedin").hasClass("on")) {
            share_linkedin = 1;
        }
        else if ($("#pv_SnsList #share_on_linkedin").hasClass("uncheckbox")) {
            share_linkedin = 1;
        }

        var share_twitter = 0;
        if ($("#pv_SnsList #share_on_twitter").hasClass("on")) {
            share_twitter = 1;
        }
        else if ($("#pv_SnsList #share_on_twitter").hasClass("uncheckbox")) {
            share_twitter = 1;
        }

        var share_imeet = 0;
        if ($("#pv_SnsList #share_on_imeet").hasClass("on")) {
            share_imeet = 1;
        }
        else if ($("#pv_SnsList #share_on_imeet").hasClass("uncheckbox")) {
            share_imeet = 1;
        }
        var ibtTypeChooseCity = 0;
        if ($("#ibtTypeChooseCity").hasClass("uncheckbox")) {
            ibtTypeChooseCity = 1;
        }
        var ibtTypeChooseCountry = 0;
        if ($("#ibtTypeChooseCountry").hasClass("uncheckbox")) {
            ibtTypeChooseCountry = 1;
        }
        if ($("#includeDate").hasClass("uncheckbox")) {
            selDate = "true";
        }
        var share_title = $("#hidTitle").val() + ',' + $("#hidCity").val() + ',' + $("#hidState").val() + '-' + $("#hidCountry").val();
        var twitter_title = $("#hidTitle").val();
        var share_desp = encodeURIComponent($("#experiences").val() + ' Start/share your own travel map and travel history!');
        var share_comment = 'posted a visit on I\'ve Been There!';
        var share_url = $("#hidSupplierURL").val();
        var share_pic = "";
        if ($("#pv_fileList .imgBox img").length > 0) {
            share_pic = $("#pv_fileList .imgBox img").first().attr("src").replace("_tn.jpg", "_bn.jpg");
        }

        var json_data = {
            newsfeedId: newsfeedId, activeType: activeType, handleType: handleType, postType: postType, supplyId: supplyId, ibtType: ibtType, experience: experiences, date: date, selDate: selDate, sharing: sharing, guid: guid, countryId: countryId, cityId: cityId,
            share_facebook: share_facebook, share_linkedin: share_linkedin, share_twitter: share_twitter, share_imeet: share_imeet, share_title: share_title, share_desp: share_desp,
            share_comment: share_comment, share_url: share_url, share_pic: share_pic, twitter_title: twitter_title, isChooseCity: ibtTypeChooseCity, isChooseCountry: ibtTypeChooseCountry
        };
        if (ibt.canPost) {
            ibt.canPost = false;
            $.post("/Supplier/PostVistSave", json_data, function (json) {
                if (json.Success) {
                    Boxy.get(obj).hideAndUnload();
                    Boxy.alert(" You have successfully posted a visit!", function () {
                        location.reload();
                    }, { title: 'Post visit' });
                    $(".boxyInner").addClass("popup");
                } else { ibt.canPost = true; }
            }, "json");
        }
    },
    PostVisitNSave: function (obj) {
        var newsfeedId = $("#newsfeedId").val();
        var activeType = $("#activeType").val();
        var year = $("#dd_year").val();
        var month = $("#dd_month").val();
        var day = $("#dd_day").val();
        var date = encodeURIComponent(year + "-" + month + "-" + day);
        var selDate = $("#selDate").val();

        var experiences = $("#experiences").val();
        if (experiences == "Write a message...")
            experiences = "";
        else
            experiences = encodeURIComponent(experiences);

        var countryId = $("#countryId").val();
        var cityId = $("#cityId").val();
        var sharing = "";//todo:PostVisitSave sns list
        var guid = $("#pvGuid").val();
        var supplyId = $("#pvSupplyId").val();
        var ibtType = $("#pvIbtType").val();
        var postType = $("#postType").val();  //new
        var handleType = $("#handleType").val();  //new
        var share_facebook = 0;
        if ($("#pv_SnsList #share_on_facebook").hasClass("uncheckbox")) {
            share_facebook = 1;
        }
        var share_linkedin = 0;
        if ($("#pv_SnsList #share_on_linkedin").hasClass("uncheckbox")) {
            share_linkedin = 1;
        }
        var share_twitter = 0;
        if ($("#pv_SnsList #share_on_twitter").hasClass("uncheckbox")) {
            share_twitter = 1;
        }
        var share_imeet = 0;
        if ($("#pv_SnsList #share_on_imeet").hasClass("uncheckbox")) {
            share_imeet = 1;
        }
        var ibtTypeChooseCity = 0;
        if ($("#ibtTypeChooseCity").hasClass("uncheckbox")) {
            ibtTypeChooseCity = 1;
        }
        var ibtTypeChooseCountry = 0;
        if ($("#ibtTypeChooseCountry").hasClass("uncheckbox")) {
            ibtTypeChooseCountry = 1;
        }
        if ($("#includeDate").hasClass("uncheckbox")) {
            selDate = "true";
        }

        var isOSM = 0;
        var isSI = 0;
        var isGT = 0;
        if ($("#onSite").attr("checked") == "checked") {
            isOSM = 1;
        }
        if ($("#site").attr("checked") == "checked") {
            isSI = 1;
        }
        if ($("#general").attr("checked") == "checked") {
            isGT = 1;
        }
        var isExpand = $("#pvn_toggle").hasClass("expand");
        var isPSC = 0;
        var isStrike = 0;
        var isND = 0;
        var isGA = 0;
        var isNHV = 0;
        var isGroupA = 0;
        var isVC = 0;
        var isCPLP = 0;
        if (isOSM > 0) {
            if ($("#isPoloticalSocialCrisis").hasClass("uncheckbox")) {
                isPSC = 1;
            }
            if ($("#isStrike").hasClass("uncheckbox")) {
                isStrike = 1;
            }
            if ($("#isNaturalDisaster").hasClass("uncheckbox")) {
                isND = 1;
            }
            if ($("#isGobalAudience").hasClass("uncheckbox")) {
                isGA = 1;
            }
            if ($("#isNonHotelVenue").hasClass("uncheckbox")) {
                isNHV = 1;
            }
            if ($("#isGroupAttendees").hasClass("uncheckbox")) {
                isGroupA = 1;
            }
            if ($("#isVirtualComponent").hasClass("uncheckbox")) {
                isVC = 1;
            }
            if ($("#isCPLP").hasClass("uncheckbox")) {
                isCPLP = 1;
            }
        }

        var meetPlanComment = $("#meetPlanComment").val();
        if (meetPlanComment == "Write your meeting planning comments...")
            meetPlanComment = "";
        else
            meetPlanComment = encodeURIComponent(meetPlanComment);

        var share_title = $("#hidTitle").val() + ',' + $("#hidCity").val() + ',' + $("#hidState").val() + '-' + $("#hidCountry").val();
        var twitter_title = $("#hidTitle").val();
        var share_desp = experiences + encodeURIComponent(' Start/share your own travel map and travel history!');
        var share_comment = 'posted a visit on I\'ve Been There!';
        var share_url = $("#hidSupplierURL").val();
        var share_pic = "";

        var IE = $.browser.msie;
        if (IE != undefined && IE) {
            if ($("#pv_fileList_flash .imgBox img").length > 0) {
                share_pic = $("#pv_fileList_flash .imgBox img").first().attr("src").replace("_tn.jpg", "_570x430.jpg");
            }
        } else {
            if ($("#pv_fileList .imgBox img").length > 0) {
                share_pic = $("#pv_fileList .imgBox img").first().attr("src").replace("_tn.jpg", "_570x430.jpg");
            }
        }

        var json_data = {
            newsfeedId: newsfeedId, activeType: activeType, handleType: handleType, postType: postType, supplyId: supplyId, ibtType: ibtType, experience: experiences, date: date, selDate: selDate, sharing: sharing, guid: guid, countryId: countryId, cityId: cityId,
            share_facebook: share_facebook, share_linkedin: share_linkedin, share_twitter: share_twitter, share_imeet: share_imeet, share_title: share_title, share_desp: share_desp,
            share_comment: share_comment, share_url: share_url, share_pic: share_pic, twitter_title: twitter_title, isChooseCity: ibtTypeChooseCity, isChooseCountry: ibtTypeChooseCountry,
            isOSM: isOSM, isSI: isSI, isGT: isGT, isPSC: isPSC, isStrike: isStrike, isND: isND, isGA: isGA, isNHV: isNHV, isGroupA: isGroupA, isVC: isVC, isCPLP: isCPLP, meetPlanComment: meetPlanComment,isExpand:isExpand
        };
        if (ibt.canPost) {
            showPop();
            ibt.canPost = false;
            $.post("/Supplier/PostVisitNSave", json_data, function (json) {
                if (json.Success) {
                    myloadingbox.hide();
                    showPVNPop();
                    //Boxy.alert(" You have successfully posted a visit!", function () {
                    //    location.href = document.referrer;
                    //}, { title: 'Post visit' });
                    //ibt.canPost = true;
                } else { ibt.canPost = true; }
            }, "json");
        }
    },
    Share: function (ibtType, objId) {

        this.loadUrlPop("/Supplier/Share", "GET", { ibtType: ibtType, objId: objId }, function () { ibt.ibtRating(); }, "SHARE");
    },
    select_rorate_img: function (id,obj) {
        ibt.rorate_attid = id;

        $('#pv_rorate_img img').attr('src', $(obj).attr('src').replace("_tn","_bn"));
    },
    //rorate the post visit attachment
    rorate: function (type) {
        if (ibt.rorate_attid == 0) {
            return false;
        }

        $.ajax({
            url: '/supplier/pvrotate',
            data: { id: ibt.rorate_attid, rorate: type },
            type:'post',
            dataType: 'json',
            success: function (json) {
                if (json.Success) {
                    $('#pv_rorate_img img').attr('src', json.Message + '_bn.jpg');
                    $('#pvListImg_' + ibt.rorate_attid).attr('src', json.Message + '_tn.jpg');
                } else {
                    alert(json.Message);
                }
            },
            error: function () { }
        });
    }
};

var widget = {
    ibtSave: function () {
        var json_data = { ibt_ObjIds: $("#ibt_ObjIds").val(), ibt_ibtType: $("#ibt_ibtType").val() };
        $.post("/Widget/IBTSave", json_data, function (json) {
            if (json.Success) {
                Boxy.alert("Save Success!", function () {
                    location.href = "/Profile";
                }, { title: 'Post visit' });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
    },
    ibtAddMore: function () {
        if ($("#ibt_ObjIds").val() == '')
            window.location.href = "/Supplier/TravelsAdd";
        else
            window.location.href = "/Supplier/TravelsAdd?t=" + $("#ibt_ibtType").val() + "&ids=" + $("#ibt_ObjIds").val() + "&cids=" + $("#ibt_CitiesIds").val();
    },
    checkbox: function (obj) {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }
        var vals = $("#ibt_ObjIds").val();
        var cvals = $("#ibt_CitiesIds").val();
        var cid = $(obj).attr("data-objId");
        var cityid = $(obj).attr("data-cityId");

        if ($(obj).hasClass("uncheckbox")) {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != cid) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }

            vals = res;

            var tmp2 = cvals.split(',');
            var res2 = "";
            for (var i = 0; i < tmp2.length; i++) {
                if (tmp2[i] != cityid) {
                    res2 = res2 + tmp2[i] + ",";
                }
            }
            if (res2.length > 0) {
                res2 = res2.substring(0, res2.length - 1);
            }
            cvals = res2;

            $(obj).removeClass("uncheckbox");

        } else {
            if (vals.length > 0) {
                vals = vals + "," + cid;
            } else {
                vals = cid;
            }
            if (cvals.length > 0) {
                cvals = cvals + "," + cityid;
            } else {
                cvals = cityid;
            }
            $(obj).addClass("uncheckbox");
        }
        $("#ibt_ObjIds").val(vals);
        $("#ibt_CitiesIds").val(cvals);
    },
    Search: function (obj, letter) {
        if ($("#ibt_selLetter").val() == letter) {
            $("#ibt_selLetter").val("");
            $(obj).removeClass("active");
        }
        else {
            $("#ibt_selLetter").val(letter);
            $("#wg_ibt_letterChoose li a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
        }

        var json_data = { letter: $("#ibt_selLetter").val(), selObjIds: $("#ibt_ObjIds").val() };
        $.post("/Widget/IBTCountryList", json_data, function (json) {
            if (json.Success) {
                $("#wg_ibt_nameList").html(json.Message);
                if ($('#wg_ibt_nameList').children('li').length == 0) {
                    $('<div class="noData">no data!</div>').appendTo('#wg_ibt_nameList');
                }
                checkboxshow();

                $(".jscroll-c").css("top", "0px");
                $(".jscroll-e").show();
                scroll_init();
            }
        }, "json");
    }
}


$.fn.MySerialize = function () {
    var s = [];
    $('input[name], select[name], textarea[name]', this).each(function () {
        if (this.disabled || (this.type == 'checkbox' || this.type == 'radio') && !this.checked)
            return;
        if (this.type == 'select-multiple') {
            var name = this.name;
            $('option:selected', this).each(function () {
                s.push(name + '=' + encodeURIComponent(this.value));
            });
        }
        else
            s.push(this.name + '=' + encodeURIComponent(this.value));
    });
    return s.join('&');
};

$(document).ready(function () {
    ibt.ibtRating();
});


