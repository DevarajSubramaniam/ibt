﻿var signup = {
    signup: function () {
        //CheckType();
        if ($('#signupForm').data('bValidator').validate() == false) {
            return;
        }

        var json_data = $("#signupForm").MySerialize();
        $.post("/Account/Signup", json_data, function (json) {
            if (json.Success) {
                //location.reload();
                //window.location = "RegisterSuccess";
                location.href = '/account/welcome';
            } else {
                //$("#tipMsg").html(json.Message);
                Boxy.alert(json.Message, null, { title: "SignUp" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
       
    }
};

