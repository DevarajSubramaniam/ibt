/*Checkbox*/
var checkboxshow = function () {
    var readEmail = $(".readEmail");
    /*20130221裕冲修改*/
    //$(".checkbox,.checkboxSmall,.checkboxBig,.checkboxTxt,.checkboxMTxt,.checkboxMedium,.uncheckboxSmall,.checkboxBigTxt").each(
    $(".checkboxSmall,.checkboxBig,.checkboxTxt,.checkboxMTxt,.checkboxMedium,.uncheckboxSmall,.checkboxBigTxt").each(
        function () {
            //add readonly
            if ($(this).attr("data-readonly") == "true") {
                $(this).addClass("cursordefault");
                return;
            } else {
                $(this).removeClass("cursordefault");
            }

            //end:add readonly
            $(this).click(function () {
                if ($(this).hasClass("disable") || $(this).hasClass("custom")) {
                    return false;
                } else {
                    if ($(this).parents().parents().parents().parents().hasClass("messageBox")) {
                        if ($(this).hasClass("uncheckbox")) {
                            $(this).removeClass("uncheckbox");
                            $(this).parents("li").removeClass("selected");
                        } else {
                            $(this).addClass("uncheckbox").parents("li").addClass("selected")
                        };
                    } else {
                        if ($(this).hasClass("uncheckbox")) {
                            $(this).removeClass("uncheckbox");
                        } else {
                            $(this).addClass("uncheckbox");
                        }
                    }
                }
            })
        })
    readEmail.click(function () { if ($(this).hasClass("unreadEmail")) { $(this).removeClass("unreadEmail") } else { $(this).addClass("unreadEmail") } });
}

/*Search*/
var searchTab = function () {
    var a = $(".searchChosse a:not('.btnGray)");
    var main = $(".searchBar").children("div");
    a.click(function () {
        a.each(function () {
            var aClass = $(this).attr("class");
            $(this).attr('class', aClass.replace("Active", ""))
        });
        var aClass2 = $(this).attr("class");
        $(this).attr("class", aClass2 + "Active");

        var t = $(this).index();
        main.eq(t).show().siblings().hide();
        return false;
    })
    var dropDownList = $(".one").find(".dropDownList");
    var btn = $(".one .btnSearch");
    btn.click(function () { $(this).siblings(".dropDownList").show(); return false; })
    dropDownList.hover(function () { $(this).show() }, function () { $(this).hide() });
    $(document).click(function () { dropDownList.hide(); $(".select").children(".list").hide(); $(".select").removeClass("active"); })
}
/*Select*/
var selectSpan = function () {
    var list = $(".select").children(".list");
    var listH = $(".header .list");
    var arrow = $(".select").children(".arrow");
    var arrow2 = $(".select").children(".arrow2");
    var downArrow = $(".select").find(".downArrow");
    var view = $(".view");
    var my = $(".my");
    var li = $(".select .list ul li");
    var textarea = $(" .shareTravel .shareBox .txtMain textarea");
    var menuList = $(".menuList");
    arrow.click(function (event) { event.stopPropagation(); $(this).siblings(".list").show(); $(".jscroll-e ").show(); $(this).parent(".select ").css("z-index", "5"); $(this).parents("span.rightCol").css("z-index", "5"); var w = $(this).parent(".select ").outerWidth(); $(this).siblings(".list").width(w - 2); scroll_init(scroll_blue); return false; })
    arrow2.click(function (event) { event.stopPropagation(); $(this).siblings(".list").show(); $(this).parent(".select ").css("z-index", "5"); $(this).parents("span.rightCol").css("z-index", "5"); var w = $(this).parent(".select ").outerWidth(); $(this).siblings(".list").width(w - 2); return false; })
    downArrow.click(function () { $(this).parent(".title").siblings(".list").show(); $(this).parents(".select").addClass("active"); return false; })
    $(".selecBtnList").find(".list").click(function () { })
    my.click(function () { $(this).parent("map").siblings(".select").addClass("active").show(); })
    list.hover(function () { $(this).show() }, function () { $(this).hide(); $(".select").removeClass("active") });
    list.find("li").hover(function () { $(this).addClass("hover") }, function () { $(this).removeClass("hover") })
    $(".view").find(".arrow").click(function () { $(this).parent(".select").addClass("active"); $(this).siblings(".list").show(); $(this).siblings(".list").css("width", "auto") })
    textarea.click(function (event) { event.stopPropagation(); $(this).addClass("maxHeight") })
    $(document).click(function () { $(".select").children(".list").hide(); $(".select").css("z-index", "0").removeClass("active"); $("span.rightCol").css("z-index", "0"); $(" .shareTravel .shareBox .txtMain textarea").removeClass("maxHeight"); menuList.children(".dropDownList").hide(); })
    li.click(function () { var txt = $(this).text(); $(this).addClass("selected").siblings("li").removeClass("selected"); $(this).parent("ul").parents(".list").siblings(".arrow").text(txt); })
    menuList.click(function () { $(this).children(".dropDownList").show(); return false; })
    //li.click(function(){var txt =$(this).text();$(this).addClass("selected").siblings("li").removeClass("selected");$(this).parent("ul").parent(".list").siblings(".arrow").text(txt); })
}
/*Scroll*/
function scroll_init(conf) {
    conf = conf || scroll_blue;
    $(".scrollBox").jscroll(conf);
    //	 $(" .popup2 .commentBox li dl .replyBox li").addClass("IERound");
}

/*Map tips slide*/
var mapSlide = function () {
    var cur = 1;
    var slideUl = $(".slideMain ul");
    var slidTag = $(".slideBox ul li");
    var slideLi = $(".slideBox ul li").size();
    var liNext = $(".slideBox .next");
    var liPrev = $(".slideBox .prev");
    var i = 1;

    liPrev.eq(0).css({ "opacity": "0.5", "cursor": "default" });

    liPrev.click(function () {
        if (cur > 1 && cur <= slideLi) {

            $(this).css({ "opacity": "1", "cursor": "pointer" });
            liNext.css({ "opacity": "1", "cursor": "pointer" });
            slideUl.animate({ left: "-=" + (-slidTag.width()) }, 500);
            cur--;
            if (cur <= 1) {
                $(this).css({ "opacity": "0.5", "cursor": "default" });
            }
        }
    })
    liNext.click(function () {
        if (cur >= 1 && cur < slideLi) {
            slideUl.animate({ left: "-=" + slidTag.width() }, 500);
            liPrev.css({ "opacity": "1", "cursor": "pointer" });
            cur++;

            if (cur >= slideLi) {
                $(this).css({ "opacity": "0.5", "cursor": "default" });
            }
        }
    })
}
/*comment*/
var comment = function () {
    var show = $(".show");
    show.click(function () {
        if ($(".img").is(":visible")) {
            $(this).parent("map").siblings("img").show();
            $(".img").hide();

        } else {
            $(this).parent("map").siblings("img").hide();
            $(".img").show();
        }
    })
}
var hover = function () {
    var hoverBox = $(".hoverBox");
    hoverBox.hover(function () { $(".img").show() }, function () { $(".img").hide(); })
    $(".img").hover(function () { $(this).show() }, function () { $(this).hide(); })
}
/*Fade click*/
$.fn.extend({
    fadeClick: function (options) {
        var defaults = {
            fadeSlide: $(this).find("ul").children("li"),
            first: $(this).find("ul").children("li:first"),
            last: $(this).find("ul").children("li").last(),
            size: $(this).find("ul").children("li").size(),
            showNum: $(this).find(".num").children("span"),
            showNumF: $(this).find(".num").children("span:first"),
            showNumL: $(this).find(".num").children("span:last"),
            next: $(this).find(".next"),
            prev: $(this).find(".prev"),
            text: $(".fadeText .textBox dl")
        }
        var o = options;
        var o = $.extend(defaults, o);
        var i = 0;
        o.fadeSlide.hide();
        o.text.hide();
        o.first.show();
        o.text.first().show();

        function showtime() {
            if (o.last.is(":visible")) {
                o.first.fadeIn(500);
                o.text.first().fadeIn(500);
                o.last.hide();
                o.text.last().hide();
                o.showNumF.addClass("active").siblings().removeClass("active");
                i = 0;
            } else {
                o.fadeSlide.hide().next().eq(i).fadeIn(500);
                o.text.hide().next().eq(i).fadeIn(500);
                o.showNum.next().eq(i).addClass("active").siblings().removeClass("active");
                i++;
            }
        }
        var auto = setInterval(showtime, 3000);

        o.fadeSlide.hover(function () { clearInterval(auto); }, function () { auto = setInterval(showtime, 3000) })

        o.showNum.click(function () {
            clearInterval(auto);
            var t = $(this).index();
            o.fadeSlide.eq(t).fadeIn(500).siblings("li").stop(false, true).hide();
            var t = $(this).index();
            o.text.eq(t).fadeIn(500).siblings("dl").stop(false, true).hide();
            $(this).addClass("active").siblings().removeClass("active");
            i = t;
        })

        o.showNum.hover(function () { clearInterval(auto); }, function () { auto = setInterval(showtime, 3000); })

        o.next.click(function () { showtime(); })

        o.prev.click(function () {
            if (o.first.is(":visible")) {
                o.last.fadeIn(500);
                o.text.last().fadeIn(500);
                o.first.hide();
                o.text.first().hide();
                o.showNumL.addClass("active").siblings().removeClass("active");
                i = o.size - 1;
            } else {
                i--;
                o.fadeSlide.hide().eq(i).fadeIn(500);
                o.text.hide().next().eq(i).fadeIn(500);
                o.showNum.eq(i).addClass("active").siblings().removeClass("active");
            }
        })

        o.prev.hover(function () { clearInterval(auto); }, function () { auto = setInterval(showtime, 3000); })
        o.next.hover(function () { clearInterval(auto); }, function () { auto = setInterval(showtime, 3000); })
    }
});
/*Fade click end*/
/*add travel step tab*/
var stepTab = function () {
    //var a=$(".sideColumn .country").find("a");
    var s = $(".listMain .scrollBox");
    $(".sideColumn .country .scrollBox ul li,.step3 .countriesBox .listMain .scrollBox dl dd").click(function () {
        //var t= $(this).parent("li").index();
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
        // s.eq(t).css("height","500px").siblings(".scrollBox").css("height","0");
        //scroll_init(scroll_blue);
    })
}
/*Input Focus*/
var inputText = function () {

    $(".popupLoging  input:text,.signupBox input:text").each(function () {
        var val = $(this).val();
        $(this).focus(function () {
            if (val === $(this).val()) { $(this).val("") }
        }).blur(function () {
            if ($(this).val() == "") { $(this).val(val) }
        });
    })
    $(".inputPassword").blur(function () {
        var val = $(this).val();
        if (val === "") {
            $(this).siblings(".value").show();
        }
    })
    $(".value").click(function () {
        $(this).hide();
        $(this).siblings("input:password").focus();
    })
}
/*Switch*/
var switchEffect = function () {
    var span = $(".switch span");
    $(document).mousemove(function (e) {
        x = e.clientX;
        y = e.clientY;
        span.css({ "left": x, "top": y });
    });
    $(document).mouseup(function () {

        if (span.hasClass("off")) {
            span.removeClass("off").addClass("on")
        } else {
            span.removeClass("on").addClass("off")
        }
        $(document).unbind('mousemove');
        $(document).unbind('mouseup');
    });
}
/*	Connections Box*/
//var connectionsBox =function(){
//	var li=$(".connectionsBox li");
//	var dl=$(".connectionsBox li dl");
//	li.hover(function(){$(this).find(dl).hide();$(this).find(dl).eq(1).show();},function(){$(this).find(dl).hide();$(this).find(dl).eq(0).show()})	
//}

/*Draggable*/

var drag = function () {
    var draggable = $(".switch .draggable");
    if ($.browser.msie && ($.browser.version == '6.0' || $.browser.version == '7.0')) {

        draggable.click(function () {
            if ($(this).hasClass("on")) {
                $(this).addClass("off").removeClass("on");
            } else {
                $(this).addClass("on").removeClass("off")
            }
        })
    } else {
        var draggable = $(".switch .draggable");
        $(".switch .draggable").draggable({
            axis: "x",
            containment: "parent",
            snap: 'span',
            addClasses: false,
            revert: false
        })

        /*			
            $(".switch .draggable").draggable({
                    stop: function() {
                    if(parseInt($(this).css("left"))==parseInt("0")){
                            $(this).addClass("off").removeClass("on")
                        }else{
                            $(this).addClass("on").removeClass("off")
                        }
                    }
            })
            
        $(".fillForm .formLi .switch .draggable ").click(function(){
                    if($(this).hasClass("on")){
                        $(this).addClass("off").removeClass("on").css("left","0px");	
                    }else{
                        $(this).addClass("on").removeClass("off").css("left","27px");
                    }
            })*/

        /*	$(".layourForm .formLi .switch .draggable ").click(function(){
                    if($(this).hasClass("on")){
                        alert(0)
                        $(this).addClass("off").removeClass("on").css("left","0px");	
                    }else{
                        $(this).addClass("on").removeClass("off").css("left","45px");
                    }
            })	
        */	draggable.mousedown(function () {
    $(this).addClass("drag");

}).mouseup(function () {
    var right = Math.ceil($(".switch").width() / 2);
    $(this).removeClass("drag");

    if ($(this).css("left") <= 23 + "px" || $(this).css("left") > 1 + "px") {
        if ($(this).hasClass("off")) {
            $(this).addClass("on").removeClass("off").css("left", right);
        } else {
            $(this).addClass("off").removeClass("on").css("left", "0px");
        }

    } else {
        $(this).addClass("on").removeClass("off").css("left", right);
    }
})
        draggable.mouseup(function () {
            $(this).addClass("drag");
        }).mouseup(function () {
            $(this).removeClass("drag");
        })
    }
}

/*Scorll Slide*/
var scorllSlide = function () {
    var next = $(".fadeList .next");
    var prev = $(".fadeList .prev");
    var ul = $(".fadeList .screen ul");
    var size = $(".fadeList .screen ul li").size();
    var liw = $(".fadeList .screen ul li").outerWidth();
    var cur = Math.ceil(size / 5);
    var w = 5 * liw;
    var i = 1;
    if (cur == 1) {
        next.css("cursor", "default")
        prev.css("cursor", "default")
    } else {
        next.css("cursor", "pointer");
        prev.css("cursor", "pointer")
        next.click(function () {
            if (i < cur) {
                ul.stop(true, true).animate({ "left": -w * i }, 2000)
                i++;
            } else {
                ul.stop(true, true).animate({ "left": 0 }, 2000);
                i = 1;
            }
        })
        prev.click(function () {
            if (i == 1) {
                cur = Math.ceil(size / 5);
                ul.stop(true, true).animate({ "left": -w * (cur - 1) }, 2000);
                i = cur;
            } else if (cur == 2) {
                ul.stop(true, true).animate({ "left": 0 }, 2000)
                i = 1;
            } else {
                --i;
                ul.stop(true, true).animate({ "left": -w * (i - 1) }, 2000)
            }
        })
    }
}
/*Window Reload*/
var windowReload = function () {
    $(window).resize(function () {
        window.location.reload()
    });
}
/*Message List*/
/*var messageList =function(){
	var li =$(".messageBox .listMain li");
	var checkbox=$(".messageBox .listMain .checkboxMedium");
	checkbox.each(function(){
		checkbox.click(function(){
			$(this).click(function(){if($(this).hasClass("uncheckbox")){$(this).removeClass("uncheckbox"); $(this).parents("li").removeClass("selected");}else{$(this).addClass("uncheckbox").parents("li").addClass("selected")}});
		})
	})	
}*/

/*Travel History List*/
/*
var historyList =function(){
	var li =$(".historyList li.row").not($(".frist"));
	var txt = $(".historyList li.row").not($(".frist")).children(".rowMain").find(".txt")
	$(".subList").hide();
//	var ulClose = $(".historyList li").find(".open");
		txt.click(function(){
			var t =$(this);
			li.find(".subList").stop(true,true).slideUp(500);
			li.removeClass("active");
			if(t.hasClass("close")){
				$(this).parents(".row").addClass("active");
				$(this).parent("div").siblings(".subList").stop(true,true).slideDown(500);
				txt.addClass("close").removeClass("open");
				$(this).addClass("open").removeClass("close");
				
			}else{
				txt.addClass("close").removeClass("open");
				
			}
		})
}
*/
/*Edit Profile Button*/
var editPorfile = function () {
    var pic = $(".profileBar .userPic");
    pic.hover(
		function () { if ($(this).find(".btn").is(":animated")) { } else { $(this).find(".btn").show(); } },
		function () { $(this).find(".btn").hide(); }
	)
}
/*CountryListing*/
var photosList = function () {
    var li = $(".photosList").find("li");
    li.hover(function () {
        var p = $(this).find(".photosInfo");
        if (p.is(":animated")) { } else {
            p.slideDown();
        }
    }, function () {
        $(this).find(".photosInfo").slideUp();
    })
}
/*Popup*/
var popup2 = function () {
    var wh = function () {
        var h = $(document).height();
        var w = $(window).width();
        var pw = $(".popup2").width();
        var ph = $(".popup2").height();
        var l = (w - pw) / 2;
        var t = ($(window).height() - ph) / 2 + $(document).scrollTop();
        var c = $(".close");
        $(".overLayout").css({ "width": w, "height": h })
        $(".popup2").css({ "left": l, "top": t })
        c.click(function () {
            $(".overLayout,.popup2").hide();
        })
    }
    wh();
    $(window).resize(function () {
        wh();
    })
}
/*banner*/
var banner = function () {
    var cur = 0;
    var slideUl = $(".banner ul");
    var slidTag = $(".banner ul li");
    var slideLi = $(".banner ul li").size();
    var liNext = $(".banner .next");
    var liPrev = $(".banner .prev");
    var i = 0;
    slidTag.eq(0).css("left", "0");
    liPrev.click(function () {
        show();
    })
    liNext.click(function () {
        if (slidTag.is(":animated")) {
        } else {
            slidTag.not($(".active")).css("left", "748px")
            if (cur > 0 && cur <= slideLi) {
                i--;
                slidTag.eq(i).addClass("active").siblings().animate({ left: "748px" }, 500, function () {
                    slidTag.not($(".active")).css("left", "-748px");
                    slidTag.eq(i).siblings().removeClass("active");
                })
                slidTag.eq(i).animate({ left: "0" }, 500);
                cur--;
            } else {
                cur = slideLi;
                i = slideLi;
                i--;
                slidTag.eq(i).addClass("active").siblings().animate({ left: "748px" }, 500, function () {
                    slidTag.not($(".active")).css("left", "-748px");
                    slidTag.eq(i).siblings().removeClass("active");
                })
                slidTag.eq(i).animate({ left: "0" }, 500);
                cur--;
            }
        }
    })
    function show() {
        if (slidTag.is(":animated")) {
        } else {
            slidTag.not($(".active")).css("left", "748px")
            if (cur >= 0 && cur < slideLi) {
                i++;
                slidTag.eq(i).addClass("active").siblings().animate({ left: "-748px" }, 500, function () {
                    slidTag.not($(".active")).css("left", "748px");
                    slidTag.eq(i).siblings().removeClass("active");
                })
                slidTag.eq(i).animate({ left: "0" }, 500);
                cur++;

                if (cur >= slideLi) {
                    cur = 0;
                    i = 0;
                    slidTag.eq(i).addClass("active").siblings().animate({ left: "-748px" }, 500, function () {
                        slidTag.not($(".active")).css("left", "748px");
                        slidTag.eq(i).siblings().removeClass("active");
                    })
                    slidTag.eq(i).animate({ left: "0" }, 500);
                }
            }
        }

    }
    var auto = setInterval(show, 5000);
    $(".banner ul li,.banner .next,.banner .prev").hover(function () {
        liNext.show(); liPrev.show();
        clearInterval(auto);
    }, function () {
        liNext.hide(); liPrev.hide();
        auto = setInterval(show, 5000);
    }
    )
}
/*Feed*/
var feedSlider = function () {
    var sliderA = $(".feed .history .sliderD");
    sliderA.click(function () {
        var txt = "close history";
        var txt2 = "view history";

        if ($(this).hasClass("sliderU")) {
            if (!$(".feed .historyList").is(":animated")) { $(this).removeClass("sliderU").text(txt).siblings(".historyList").slideDown(); }
        } else {
            $(this).addClass("sliderU").text(txt2).siblings(".historyList").slideUp();
        }
    })
}
/*Post visit*/
var postRadio = function () {
    var radio = $(".challenges ").find("input:radio");
    radio.click(function () {
        if ($(this).hasClass("mL20")) {
            $(this).parents(".challenges").siblings(".challenges,.txtMain").slideUp();
        } else {
            $(this).parents(".challenges").siblings(".challenges,.txtMain").slideDown();
        }
    })
}
/*Toggle*/
var toggle = function () {
    var t = $(".toggle");
    t.click(function () {
        if (!$(this).parent().siblings(".toggleMain").is(":animated")) {
            if ($(this).hasClass("expand")) {
                $(this).parent().siblings(".toggleMain").slideUp();
                $(this).removeClass("expand");
            } else {
                $(this).addClass("expand");
                $(this).parent().siblings(".toggleMain").slideDown();
            }
        }
    })
}