﻿var searchListing = {
    posting: false,
    searchBy: "hotels",
    filter: "",
    keyWord:"",
    pageIndex: 1,
    pageSize: 10,
    isNav: false,
    scrolling: false,
    isLastPage:false,
    init: function () {
        searchListing.getListing();
        $(window).scroll(function () {
            if (searchListing.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (searchListing.isLastPage == false)
                    searchListing.nextPage();
            }
        });
    },
    search: function () {
        searchListing.pageIndex = 1;
        searchListing.isLastPage = false;
        searchListing.filter = "";
        $(".letterBar .bg .main a").each(function () {
            $(this).removeClass("active");
        });
        searchListing.getListing();
    },
    searchByN: function (searchKey) {
        searchListing.searchBy = searchKey;
        searchListing.filter = "";
        searchListing.keyWord = "";
        searchListing.pageIndex = 1;
        searchListing.isLastPage = false;
        $("#keyWord").val("Key in Keyword");
        $('#nav_search_keywork').val("Key in Keyword");
        $(".letterBar .bg .main a").each(function () {
            $(this).removeClass("active");
        });
        var searchByName = 'hotels';
        switch (searchListing.searchBy) {
            case "countries":
                searchByName = 'Countries';
                break;
            case 'cities':
                searchByName = 'Cities';
                break;
            case 'other':
                searchByName = 'Other Venues';
                break;
            case 'all':
                searchByName = 'All Suppliers';
                break;
            case 'member':
                searchByName = 'People';
                break;
            default:
                searchByName = "Hotels";
                break;
        }
        $('.searchBar2 .select .arrow').html(searchByName);
        searchListing.getListing();
    },
    filterSearch: function (obj) {
        if ($(obj).hasClass("active")) {
            //$(obj).removeClass("active");
            $('.filter_id_' + $(obj).html()).removeClass("active");
            searchListing.filter = "";
        } else {
            if (searchListing.filter != $(obj).html()) {
                $(".letterBar .bg .main a").each(function () {
                    $(this).removeClass("active");
                });
            }
            //$(obj).addClass("active");
            $('.filter_id_' + $(obj).html()).addClass('active');
            searchListing.filter = $(obj).html();
        }
        searchListing.pageIndex = 1;
        searchListing.isLastPage = false;
        searchListing.getListing();
    },
    nextPage: function () {
        searchListing.pageIndex++;
        searchListing.getListing();
    },
    getListing: function () {
        if (searchListing.scrolling) {
            return ;
        }
        searchListing.scrolling = true;
        if (!searchListing.isNav) {
            searchListing.keyWord = $("#keyWord").val();
        } else {
            searchListing.keyWord = $("#nav_search_keywork").val();
        }
        if (searchListing.keyWord == "Key in Keyword")
            searchListing.keyWord = "";
        var data = { pageIndex: searchListing.pageIndex, pageSize: searchListing.pageSize, filter: searchListing.filter, keyWord: searchListing.keyWord, searchBy: searchListing.searchBy };
        if (!searchListing.posting) {
            $("#loading").show();
            if (searchListing.pageIndex == 1) {
                $("#listUl").html('');
            }
            searchListing.posting = true;
            $.ajax({
                'url': '/search/getlisting',
                'data': data,
                'type': 'post',
                'dataType': 'json',
                'success': function (json) {
                    if (json.Success) {
                        if (searchListing.pageIndex == 1) {
                            if (json.Message != '')
                                $("#listUl").html(json.Message);
                            else
                                $("#listUl").append('<div style="font-weight: bold;color: #666666;font-weight: bold;margin-top: 50px;margin-bottom:50px;text-align: center;">There are no results found</div>');
                        }
                        else
                            $("#listUl").append(json.Message);
                        ibt.ibtRating();
                        if (searchListing.pageIndex * searchListing.pageSize >= Number(json.Item.total)) {
                            //$("#clickloadmore_searchList").hide();
                            searchListing.isLastPage = true;
                        } else {
                            //$("#clickloadmore_searchList").show();
                            searchListing.isLastPage = false;
                        }
                        $("#searchResultTxt").html(json.Item.msg);
                    } else {
                        alert(json.Message);
                    }
                    $("#loading").hide();
                    searchListing.posting = false;
                    searchListing.scrolling = false;
                },
                'error': function () { searchListing.posting = false; searchListing.scrolling = false }
            });
        }
    }
}