﻿var Connection = {
    remove: function (friendId) {
        var json_data = {friendId: friendId };
        $.post("/Connection/RemoveConnection", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Connection Remove" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Connection Remove" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
       
    },
    request: function (friendId) {
        var json_data = { friendId: friendId };
        $.post("/Connection/SendRequest", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "CONNECTION REQUEST" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "CONNECTION REQUEST" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    create: function (friendId) {
        var json_data = { friendId: friendId };
        $.post("/Connection/CreateConnection", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Connection Request" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Connection Request" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    ignore: function (friendId) {
        var json_data = { friendId: friendId };
        $.post("/Connection/IgnoreConnection", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Connection Request" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Connection Request" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    }
};
