﻿var ibt_country = {
    attType_City: 1,
    attType_NewsFeed: 2,
    attType_Supply: 3,
    attType_Country: 4,
    pageIndex:1,
    rrwb: function (obj)
    {
        var rrwbType = $(obj).attr("data-rrwbType");
        var ibtType = $(obj).attr("data-ibtType");
        var d_val = $(obj).attr("data-val");
        if (d_val == "1") {
            $(obj).attr("data-val", "0");
        }
        else {
            $(obj).attr("data-val", "1");
        }
        var jasonData = { countryId: $("#ibt_countryId").val(), rrwbType: rrwbType, ibtType: ibtType, val: $(obj).attr("data-val") };
        $.post("/IBT/RRWB", jasonData, function (json) {
            if (json.Success) {
                location.reload();
            }
        }, "json");
    },
    showPop: function () {
        $.ajax({
            url: "/Supplier/PostVisit", data: { ibtType: $("#country_ibtType").val(), objId: $("#ibt_countryId").val() },
            async: false, dataType: "text", type: "Post", cache: false, timeout: 8000,
            success: function (txt) {
                var mybox = null;
                mybox = new Boxy(txt, {
                    title: "POst visit",
                    modal: true,
                    center: true,
                    unloadOnHide: true,
                    closeText: "",
                    afterHide: function (e) { },
                    afterShow: function (e) { }
                });
                $(".boxyInner").addClass("popup");
                checkboxshow();
                selectSpan();
                mybox.center();
                ibt.PvPopLoad();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统繁忙，请刷新重试！");
            }
        });
    },
    rating: function () {
        $("#ibtrating").raty({
            readOnly: false,
            score: function () { return $(this).attr("data-rating"); },
            click: function (score, evt) {
                $.post("/IBT/RRWBRating", {countryId:$("#ibt_countryId").val(),score:score}, function () { }, "json");
            }
        });
    },
    uploadify: function () {
        $("#ibt_uploadify").uploadify({
            "formData": { "guid": $("#ibt_guid").val(), "attType": $("#ibt_attType").val() },
            "swf": "/js/uploadify/uploadify.swf",
            "uploader": "/IBT/AttachmentUpload",
            "buttonImage": "/images/blue_add_w85.gif",
            "fileDesc": "*.jpg;*.jpeg;*.png;*.gif",
            "fileExt": "*.jpg;*.jpeg;*.png;*.gif",
            "height": 85,
            "width": 85,
            onUploadSuccess: function (file, data, response) {
                var json = eval('(' + data + ')');
                $("#ibt_fileList").append("<span class=\'img\'><img data-attid=\'" + json.Item + "\' src=\'" + json.Message + "\' alt=\'\'/></span>");
            }
        });
    },
    newsFeedPost: function () {
        var json_data = { "guid": $("#ibt_guid").val(), "attType": $("#ibt_attType").val(), "val": $("#ibt_val").val(), "countryId": $("#ibt_countryId").val(), "ibtType": $("#country_ibtType").val() };
        $.ajax({
            url: "/IBT/NewsFeedSave",
            data: json_data,
            type: "Post",
            dataType: "json",
            success: function (success) {
                if (success)
                { location.reload(); }
            }
        });
    },
    getFeedComment: function (id) {
        var box = $("#replyBox_" + id);
        if (box.hasClass("show")) {
            box.removeClass("show");
            box.hide();
        }
        else {
            box.addClass("show");
            box.show();
            $.ajax({
                url: "/IBT/FeedCommentList",
                data: { newsFeedId: id },
                type: "Post",
                dataType: "json",
                success: function (data) {
                    $("#commentsCount_" + id).html("Comments (" + data.Item + ")");
                    box.html(data.Message);
                    $("textarea").focus(function () {
                        var val = "Write a message...";
                        if (val === $(this).val()) { $(this).val("") }
                    }).blur(function () {
                        if ($(this).val() == "") { $(this).val("Write a message...") }
                    });
                },
                errer: function () { alert("系统繁忙，请刷新重试！"); }
            });
        }
    },
    saveFeedComment: function (id) {
        if ($("#commnetTxt_" + id).val() == "Write a message...") {
            Boxy.alert("The text field is empty. Please write in your comment before posting!", null, { title: "" });
            $(".boxyInner").addClass("popup");
            return;
        }
        $.ajax({
            url: "/IBT/SaveFeedComment",
            data: { newsFeedId: id, val: $("#commnetTxt_" + id).val() },
            type: "Post",
            dataType: "json",
            success: function (data) {
                var count = parseInt($("#commentsCount_" + id).attr("data-comments"));
                count = count + 1;
                $("#commentsCount_" + id).html("Comments (" + count + ")");
                $("#commentsCount_" + id).attr("data-comments", count);
                $("#commentList_" + id).prepend(data.Message);
                $("#commnetTxt_" + id).val("Write a message...")
            }
        });
    },
    getCountryFeedList: function () {
        $("#country_loadMore").hide();
        $.ajax({
            url: "/IBT/CountryFeedList",
            data: { countryId: $("#ibt_countryId").val(), pageIndex: (this.pageIndex+1), searchTime: $("#country_searchTime").val() },
            type:"Post",
            dataType: "html",
            success: function (txt) {
                    $("#country_fixbox").append(txt);
                    $("#country_loadMore").show();
                    ibt_country.pageIndex++;
            },
            error: function () {
                $("#country_loadMore").show();
            }
        });
    },
};

$(document).ready(function () {
    ibt_country.rating();
    ibt_country.uploadify();
    $("textarea").focus(function () {
        var val = "Write a message...";
        if (val === $(this).val()) { $(this).val("") }
    }).blur(function () {
        if ($(this).val() == "") { $(this).val("Write a message...") }
    });
});