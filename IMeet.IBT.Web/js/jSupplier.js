﻿$(function () {
    //加载上传控件
    supplyUploadify();
    var request = new Object();
    request = GetRequest();
    var tb = request["p"];
    if (tb == '' || tb == "undefined" || tb == undefined) {
        tb = 'newsfeeds';
    }

    $(".fold").removeClass("unfold");
    $("#_about").hide();
    $("#_newsfeeds").hide();
    $("#_photoalbum").hide();
    $("#_specials").hide();
    $("#_requestinfomation").hide();

    if (tb == "about") {
        $("#tb_about").addClass("unfold");
        $("#_about").show();
        //加载地图
        initialize();
    } else if (tb == "newsfeeds") {
        $("#tb_newsfeeds").addClass("unfold");
        $("#_newsfeeds").show();
        $("#SupplyFeedbackLists").empty();
        //加载评论
        jFeedbacks.init();
    } else if (tb == "photo") {
        //加载相册的时候，清空newfeeds的list，一面与相册的详细层的list内的id冲突
        $("#SupplyFeedbackLists").empty();
        $("#tb_photo").addClass("unfold");
        $("#_photoalbum").show();
        //加载相册
        PhotoManage.init();
        photosList();
    } else if (tb == "specials") {
        $("#tb_specials").addClass("unfold");
        $("#_specials").show();
    } else if (tb == "request") {
        $("#tb_request").addClass("unfold");
        $("#_requestinfomation").show();
    }

    $("textarea").focus(function () {
        var val = "Write a message...";
        if ($(this).val() == val) { $(this).val("") }
    }).blur(function () {
        if ($(this).val() == "") { $(this).val("Write a message...") }
    });
})

function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串 
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}

function SupplyV(obj, type) {
    return;
    //以下为无用代码
    $(".fold").removeClass("unfold");
    $(obj).addClass("unfold");
    $("#_about").hide();
    $("#_newsfeeds").hide();
    $("#_photoalbum").hide();
    $("#_specials").hide();
    $("#_requestinfomation").hide();

    if (type == "about") {
        $("#_about").show();
    } else if (type == "newsfeeds") {
        $("#_newsfeeds").show();
        $("#SupplyFeedbackLists").empty();
        jFeedbacks.init();
    } else if (type == "photo") {
        //加载相册的时候，清空newfeeds的list，一面与相册的详细层的list内的id冲突
        $("#SupplyFeedbackLists").empty();
        $("#_photoalbum").show();
        PhotoManage.init();
        photosList();
    } else if (type == "specials") {
        $("#_specials").show();
    } else if (type == "request") {
        $("#_requestinfomation").show();
    }
    //加载supply页面的图片上传控件
    supplyUploadify();
    $.setCookie('tabnum', type);

}

function supplyUploadify() {
    var IE = $.browser.msie;
    if (IE != undefined && IE) {
        $("#fileList").remove();
        $("#fileListFlash").show();
        $("#supply_uploadify_flash").uploadify({
            "formData": { "guid": $("#att_guid").val(), "attType": $("#att_type").val() },
            "swf": "/js/uploadify/uploadify.swf",
            "uploader": "/IBT/AttachmentUpload",
            "buttonImage": "/images/add_w100.gif",
            "fileDesc": "*.jpg;*.jpeg;*.png;*.gif",
            "fileExt": "*.jpg;*.jpeg;*.png;*.gif",
            "height": 100,
            "width": 100,
            onUploadSuccess: function (file, data, response) {
                eval("data=" + data);
                if (data.Success) {
                    $("#fileListFlash").prepend("<span class=\"img\" style=\"height: 80px; width: 116px;\"><span id=\"data-attid" + data.Item + "\" onclick=\"jFeedbacks.cbkImages(this);\" data-attid=\"" + data.Item + "\" class=\"checkboxBig mL5 mR5 uncheckbox\"></span><img src=\"" + data.Message + "\" alt=\"\"/></span>");
                    var vals = $("#imageids").val();
                    if (vals.length > 0) {
                        vals = vals + "," + data.Item;
                    } else {
                        vals = data.Item;
                    }
                    $("#imageids").val(vals);
                    //checkboxshow();
                    $("#data-attid" + data.Item).click(function () {
                        if ($(this).parents().parents().parents().parents().hasClass("messageBox")) {
                            if ($(this).hasClass("uncheckbox")) { $(this).removeClass("uncheckbox"); $(this).parents("li").removeClass("selected"); } else { $(this).addClass("uncheckbox").parents("li").addClass("selected") };
                        } else {
                            if ($(this).hasClass("uncheckbox")) {
                                $(this).removeClass("uncheckbox")
                            } else {
                                $(this).addClass("uncheckbox")
                            }
                        }
                    })
                } else {
                    if (UserId == 0) {
                        ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                        return;
                    }
                }
            }
        });
    }
    else {
        $("#fileList").show();
        $("#fileListFlash").remove();
        $("#supply_uploadify").fileupload({
            url: "/IBT/AttachmentUpload",
            formData: { "guid": $("#att_guid").val(), "attType": $("#att_type").val() },
            dataType: 'json',
            add: function (e, data) {
                var goUpload = true;
                var uploadFile = data.files[0];
                if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                    Boxy.alert('You must select an image(gif|jpg|jpeg|png) file only.');
                    goUpload = false;
                }
                if (uploadFile.size > 4000000) { // 4mb
                    Boxy.alert('Please upload a smaller image, max size is 4 MB.');
                    goUpload = false;
                }
                if (goUpload == true) {
                    $("#progress").show();
                    data.submit();
                }
            },
            done: function (e, data) {
                var json = data.result;
                if (json.Success) {
                    $("#fileList").prepend("<span class=\"img\" style=\"height: 80px; width: 116px;\"><span id=\"data-attid" + json.Item + "\" onclick=\"jFeedbacks.cbkImages(this);\" data-attid=\"" + json.Item + "\" class=\"checkboxBig mL5 mR5 uncheckbox\"></span><img src=\"" + json.Message + "\" alt=\"\"/></span>");
                    var vals = $("#imageids").val();
                    if (vals.length > 0) {
                        vals = vals + "," + json.Item;
                    } else {
                        vals = json.Item;
                    }
                    $("#imageids").val(vals);
                    //checkboxshow();
                    $("#data-attid" + json.Item).click(function () {
                        if ($(this).parents().parents().parents().parents().hasClass("messageBox")) {
                            if ($(this).hasClass("uncheckbox")) { $(this).removeClass("uncheckbox"); $(this).parents("li").removeClass("selected"); } else { $(this).addClass("uncheckbox").parents("li").addClass("selected") };
                        } else {
                            if ($(this).hasClass("uncheckbox")) {
                                $(this).removeClass("uncheckbox")
                            } else {
                                $(this).addClass("uncheckbox")
                            }
                        }
                    })
                } else {
                    if (UserId == 0) {
                        ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                        return;
                    }
                }
                $("#progress").hide();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css('width', progress + '%');
            }
        });
    }
}

(function ($) {
    if (!$.setCookie) {
        $.extend({
            setCookie: function (c_name, value, exdays) {
                try {
                    if (!c_name) return false;
                    var exdate = new Date();
                    exdate.setDate(exdate.getDate() + exdays);
                    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
                    document.cookie = c_name + "=" + c_value;
                }
                catch (err) {
                    return '';
                };
                return '';
            }
        });
    };
    if (!$.getCookie) {
        $.extend({
            getCookie: function (c_name) {
                try {
                    var i, x, y,
                        ARRcookies = document.cookie.split(";");
                    for (i = 0; i < ARRcookies.length; i++) {
                        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                        x = x.replace(/^\s+|\s+$/g, "");
                        if (x == c_name) return unescape(y);
                    };
                }
                catch (err) {
                    return '';
                };
                return '';
            }
        });
    };
})(jQuery);