﻿var AccountSetting = {
    setting: function () {
        var json_data = {
            username: $("#username").val(), firstname: $("#firstname").val(), lastname: $("#lastname").val(), email: $("#email").val(), newemail: $("#newemail").val(), altemail: $("#altemail").val(),
            hidfldDay: $("#hidfldDay").val(), hidfldMonth: $("#hidfldMonth").val(), hidfldYear: $("#hidfldYear").val(),
            statis: $("#statis").val(), statusUpdate: $("#statusUpdate").val(), hidfldCountry: $("#hidfldCountry").val(), hidfldCity: $("#hidfldCity").val(),
            createUserType: $("#createUserType").val(), createIsPle: $("#createIsPle").val(), createIsBus: $("#createIsBus").val()
        };
        $.post("/Account/AccountSetting", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    window.location = "/Profile";
                }, { title: "Edit Profile" });
                $(".boxyInner").addClass("popup");

            }
            else {
                Boxy.alert(json.Message, null, { title: "Edit Profile" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    uploadAvator: function () {
        $('#upfile').uploadify({
            'buttonImage': '/images/icon/upload_imageIcon.jpg',
            'swf': '/js/uploadify/uploadify.swf',
            'cancelImg': '/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 282,
            'height': 282,
            'uploader': '/Account/Upload?UserId=' + $("#UserId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                //alert('文件 ' + file.name + ' 已经上传成功，并返回 ' + response + ' 保存文件名称为 ' + data.SaveName);
                document.getElementById('imgshow').style.display = "block";
                document.getElementById('divfile').style.display = "none";
                $("#imgAvatar").removeAttr("src");
                //修改显示-处理缓存问题 /*20130206裕冲修改*/
                $("#imgAvatar").attr("src", "/Staticfile/Avatar/tmp/" + "ibt_" + data.SaveName + "?" + Math.random(10000));
                $("#imgAvatar_temp").removeAttr("src");
                //需要保存至数据的值 /*20130206裕冲修改*/
                $("#imgAvatar_temp").attr("src", "/Staticfile/Avatar/tmp/" + "ibt_" + data.SaveName)
                //UpdateAvartar(response);
                //documentReady();
                picLoad();
            }
        });
    }
};
