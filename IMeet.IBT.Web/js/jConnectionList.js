﻿var connectionList = {
    page:1,
    scrolling:false,
    pageSize:10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (connectionList.scrolling == true) {
                    connectionList.GetDataList(connectionList.page);
                    connectionList.page++;
                }
            }
        });
    },
    GetDataList: function (page) {
        connectionList.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#feedList');
        var json_data = { userId:$("#userId").val(),  page: page, pageSize: connectionList.pageSize, filterLetter: $("#filterLetter").val(), filterKeyword: $("#filterKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Connection/ConnectionListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    connectionList.scrolling = false;
                    connectionList.itemTotal = json.Item.itemTotal;
                    $("#connectionList_count").text(json.Item.total);
                    $('#feedList').append(json.Message);
                    $('.loading').remove();
                    checkboxshow();
                    if ($('#feedList').children('li').length == 0) {
                        $('<div class="noData">No results found</div>').appendTo('#feedList');
                    }
                    connectionList.scrolling = true;
                }
            }
        });
    },
    initData: function () {
        $("#feedList").empty();
        connectionList.GetDataList(1);
        connectionList.page = 2;
        $("#filterLetter").val("");
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#feedList").empty();//先删除以前的
        connectionList.scrolling = false;
        connectionList.itemTotal = -1;
        $("#filterLetter").val("");
        $('.noData').remove();
        connectionList.GetDataList(1);
    },
    //快速搜索
    quickSearch: function (obj, letter) {
        $("#feedList").empty();
        $('.noData').remove();
        connectionList.scrolling = false;
        connectionList.itemTotal = -1;
        connectionList.page = 2;
        if ($("#filterLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#filterLetter").val("");
            connectionList.GetDataList(1);
        } else {
            $("#quickSearch a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#filterLetter").val(letter);
            connectionList.GetDataList(1);
        }
    }
}