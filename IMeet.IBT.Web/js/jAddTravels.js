﻿var jAddTravels = {
    selectLetter: "",
    selectCountryId: 0,
    selectCityId: 0,
    selectVenuesId: 0,
    currentStepN: 1,
    venuesPage: 1,
    sortName: "",
    CountryFilter: function (obj, letter, stepN) { //1bug: 在2，3再点前面的字母过滤国家没处理（save）
        if (this.selectLetter == letter) {
            this.selectLetter = "";
        } else {
            this.selectLetter = letter;
            $(obj).addClass("active");
        }

        this.Step1();
    },
    Step1: function (num) {
        //如果从第二步退回第一步，先保存第二步选择的城市
        if (num != undefined && num == 2) {
            this._Step2Save();
        }
        this.goStepN(num);
        this.currentStepN = 1;
        var json_Data = { selectLetter: this.selectLetter, countryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val() };
        $.getJSON("/Supplier/AddTravels_Step1", json_Data, function (json) {
            if (json.Success) {
                $("#countriesBox").html(json.Message);
                $("#page_content").removeClass();
                $("#page_content").addClass("content");
                checkboxshow();
                scroll_init(scroll_blue);
            }
        });
    },
    Step2: function (num) {
        if (num != undefined && num == 1) {
            this._Step1Save();
        }
        if (num != undefined && num == 3) {
            this._Step3Save();
        }
        this.goStepN(num);
        this.currentStepN = 2;
        var json_Data = { countryIds: $("#iptCountryIds").val(), selectCountryId: this.selectCountryId, epoch: $("#iptEpoch").val() };
        $.getJSON("/Supplier/AddTravels_Step2", json_Data, function (json) {
            if (json.Success) {
                $("#countriesBox").html(json.Message);
                $("#page_content").removeClass();
                $("#page_content").addClass("content step2");
                jAddTravels.selectCountryId = json.Item;
                if ($("#CityLists dd").size() == 0) {
                    $("#step3CityTxt").text("You have not selected a city!");
                } 
                checkboxshow();
                scroll_init(scroll_blue);
            }
        });
    },
    Step2_1: function (countryId) {


        //iptCountryCity: 4_3,4,5-7_5,1,9
        var json_Data = { countryId: countryId, selCityId: "" };

    },
    Step3: function (num) {
        if (num != undefined && num == 2) {
            this._Step2Save();
        }
        if (num != undefined && num == 4) {
        }

        this.goStepN(num);
        this.currentStepN = 3;
        var json_Data = { countryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val(), selectCityId: this.selectCityId };
        $.getJSON("/Supplier/AddTravels_Step3", json_Data, function (json) {
            if (json.Success) {
                $("#countriesBox").html(json.Message);
                $("#page_content").removeClass();
                $("#page_content").addClass("content step3");
                jAddTravels.selectCityId = json.Item.cityId;
                if (json.Item.item == 10) {
                    $("#loadSeeMore").show();
                } else {
                    $("#loadSeeMore").hide();
                }
                checkboxshow();
                selectSpan();
                $(".jscroll-e").show();
                scroll_init(scroll_blue);
                jAddTravels._Step3VenuesRating();
            }
        });
    },
    Step4: function (num) {
        if (num != undefined && num == 1) {
            this._Step1Save();
        }
        if (num != undefined && num == 2) {
            this._Step2Save();
        }
        if (num != undefined && num == 3) {
            this._Step3Save();
        }
        this.goStepN(num);
        this.currentStepN = 4;
        var json_Data = { counryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val() };
        $.ajax({
            url: "/Supplier/AddTravels_Step4", data: json_Data, success: function (json) {
                if (json.Success) {
                    $("#countriesBox").html(json.Message);
                    $("#page_content").removeClass();
                    $("#page_content").addClass("content step4");
                    checkboxshow();
                    scroll_init(scroll_blue);
                }
            }, dataType: "json", async: false
        });

        //maps
        var maps_data = { mapType: 1, userId: addTravelsMaps.UserId, lat: $config["lat"], lng: $config["lng"], iptEpoch: $("#iptEpoch").val() };
        $.ajax({
            url: "/map/GetaddTravelsMapsList", data: maps_data, success: function (json) {
                if (json.Success) {
                    addTravelsMaps.source = json.Item;
                    addTravelsMaps.init(json.Item);
                }
            }, dataType: "json", async: false
        });
    },
    goStepN: function (num) {
        if (num != undefined && num == 0) {
            if (this.currentStepN == 1) {
                this._Step1Save();
            }
            if (this.currentStepN == 2) {
                this._Step2Save();
            }
            if (this.currentStepN == 3) {
                this._Step3Save();
            }
        }
    },
    btnConfrim: function () {
        var json_Data = { epoch: $("#iptEpoch").val() };
        $.getJSON("/Supplier/AddTravels_Confirm", json_Data, function (json) {
            if (json.Success) {
                Boxy.alert("You have successfully added your travels!", function () {
                    location.href = "/Profile";
                }, { title: "Add Travels" });
                $(".boxyInner").addClass("popup");
            }
        });
    },
    ckbCountry: function (obj) {
        var vals = $("#iptCountryIds").val();
        var cid = $(obj).attr("data-countryid");
        if ($(obj).hasClass("uncheckbox")) {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != cid) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }

            vals = res;
        } else {
            if (vals.length > 0)
                vals = vals + "," + cid;
            else
                vals = cid;
        }
        $("#iptCountryIds").val(vals);
    },
    GetCityList: function (obj, countryId, stepNum) {
        stepNum = stepNum == undefined ? 0 : stepNum;
        //todo:1 save 当前的选择
        var selCityIds = ibt.getCheckboxVal("#CityLists dl>dd>span", "data-cityId");
        //get
        var json_Data = { countryId: countryId, saveCountryId: this.selectCountryId, selCityIds: selCityIds, epoch: $("#iptEpoch").val(), stepNum: stepNum };
        $.post("/Supplier/AddTravels_Step2_1", json_Data, function (json) {
            if (json.Success) {
                $("#CityLists").html(json.Message);
                jAddTravels.selectCountryId = json.Item;
                $(obj).parent().parent().children().each(function () {
                    $(this).children().removeClass("active");
                });
                //选中当前的
                $(obj).addClass("active");
                checkboxshow();
                scroll_init(scroll_blue);
                if ($("#CityLists dd").size() == 0) {
                    $("#step3CityTxt").text("You have not selected a city!");
                } 
            }
        }, "json");
    },
    GetVenuesList: function (obj, cityId, stepNum) {
        stepNum = stepNum == undefined ? 0 : stepNum;

        var selVenueIds = this._Step3GetVenuesIds();
        var json_data = { cityId: cityId, selCityId: jAddTravels.selectCityId, selVenueIds: selVenueIds, epoch: $("#iptEpoch").val() };
        $("#step3Loading").show();
        $.post("/Supplier/AddTravels_Step3_1", json_data, function (json) {
            if (json.Success) {
                $("#step3Loading").hide();
                $("#feedBox").html(json.Message);
                jAddTravels.selectCityId = cityId;
                $(obj).parent().children().each(function () {
                    $(this).removeClass("active");
                });
                if (json.Item.item == 10) {
                    $("#loadSeeMore").show();
                } else {
                    $("#loadSeeMore").hide();
                }
                //选中当前的
                $(obj).addClass("active");
                checkboxshow();
                selectSpan();
                $(".jscroll-e").show();
                scroll_init(scroll_blue);
                jAddTravels._Step3VenuesRating();
            }
        }, "json");
    },
    _Step1Save: function () {
        var json_Data = { selCountryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val() };
        $.post("/Supplier/AddTravels_Step1_Save", json_Data, function (json) {
            if (json.Success) {
                //only save
            }
        }, "json");
    },
    _Step2Save: function () {
        var selCityIds = ibt.getCheckboxVal("#CityLists dl>dd>span", "data-cityId");
        var json_Data = { countryId: this.selectCountryId, saveCountryId: this.selectCountryId, selCityIds: selCityIds, epoch: $("#iptEpoch").val() };
        $.post("/Supplier/AddTravels_Step2_Save", json_Data, function (json) {
            if (json.Success) {
                //only save
                if ($("#CityLists dd").size() == 0) {
                    $("#step3CityTxt").text("You have not selected a city!");
                }
            }
        }, "json");
    },
    _Step3Save: function () {
        var selVenueIds = this._Step3GetVenuesIds();
        var json_data = { selCityId: jAddTravels.selectCityId, selVenueIds: selVenueIds, epoch: $("#iptEpoch").val() };
        $.post("/Supplier/AddTravels_Step3_Save", json_data, function (json) {
            if (json.Success) {
            }
        }, "json");
    },
    _Step3VenuesRating: function () {
        $('.StepRating').raty({
            readOnly: false,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                ibt.rrwb_rating(ibt.IBTType_Supply, $(this).attr('data-objId'), score);
            }
        });
    },
    _Step3GetVenuesIds: function () {
        var vals = "";
        $("dd a").each(function () {
            if ($(this).attr("data-sel") == "1") {
                vals = vals + $(this).attr("data-id") + ",";
            }
        });
        if (vals.length > 0) {
            vals = vals.substring(0, vals.length - 1);
        }
        return vals;
    },
    Step3IBT: function (obj, id) {
        if ($(obj).attr("data-sel") == "0") {
            var ibtCount = parseInt($(obj).attr("data-ibtnum")) + 1;
            isAdd = true;
            //选择
            $(obj).attr("data-sel", 1);
            $(obj).attr("data-ibtnum", ibtCount);
            $(obj).removeClass();
            $(obj).addClass("btnBlueOkH27");
            $(obj).html("<span class=\"btnBlueNumH27\"><span class=\"btnBlueAddH27\">I've Been There</span>" + ibtCount + "</span>");

        } else { //取消选择
            var ibtCount = parseInt($(obj).attr("data-ibtnum")) - 1;
            ibtCount = ibtCount < 0 ? 0 : ibtCount;
            $(obj).attr("data-sel", 0);
            $(obj).attr("data-ibtnum", ibtCount);
            $(obj).removeClass();
            if (ibtCount < 1) {
                $(obj).addClass("feedBox_IBT btnGrayDotH29");
                $(obj).html("<span class=\"bg\">I've Been There</span>");
            } else {
                $(obj).addClass("btnGrayOkH29");
                $(obj).html("<span class=\"btnGrayNumH29\"><span class=\"btnGrayAddH29\">I've Been There</span>" + ibtCount + "</span>");
            }
        }
    },
    Step3VenuesSort: function (sortName, page) {
        jAddTravels.sortName = sortName;
        if (jAddTravels.sortName != "All") {
            $("#SortByName").text(sortName);
        } else {
            $("#SortByName").text("Sort By");
        }
        var selVenueIds = this._Step3GetVenuesIds();
        var json_data = { cityId: jAddTravels.selectCityId, page: page, sortName: sortName, epoch: $("#iptEpoch").val() };
        $("#step3Loading").show();
        $.post("/Supplier/AddTravels_Step3_1", json_data, function (json) {
            if (json.Success) {
                $("#step3Loading").hide();
                $("#feedBox").html(json.Message);
                if (json.Item.item == 10) {
                    $("#loadSeeMore").show();
                } else {
                    $("#loadSeeMore").hide();
                }
                //选中当前的
                checkboxshow();
                selectSpan();
                $(".jscroll-e").show();
                scroll_init(scroll_blue);
                jAddTravels._Step3VenuesRating();
            }
        }, "json");
    },
    Step3LoadMore: function () {
        jAddTravels.Step3VenuesSort(jAddTravels.sortName, jAddTravels.venuesPage + 1);
    }
};
