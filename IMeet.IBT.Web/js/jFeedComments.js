﻿var feedComments = {
    ShowMessageBoxPage: function (feedId, page) {
        //ajax load list
        var json_data = { feedId: feedId, page: page, pageSize: 3 };
        $.post("/Supplier/FeedCommentList", json_data, function (json) {
            $("#operating_" + feedId).html(json.Message);
            $("#replyBox_" + feedId).show();
            $("#replyBox_" + feedId).addClass("show");

            $("#commnetTxt_" + feedId).focus(function () {
                var val = "Write a message...";
                if (val == $("#commnetTxt_" + feedId).val()) { $("#commnetTxt_" + feedId).val("") }
            }).blur(function () {
                if ($("#commnetTxt_" + feedId).val() == "") { $("#commnetTxt_" + feedId).val("Write a message...") }
            });
        }, "json");

    },
    ShowMessageBox: function (feedId) {
        if ($("#replyBox_" + feedId).hasClass("show")) {
            $("#replyBox_" + feedId).removeClass("show");
            $("#replyBox_" + feedId).hide();
            $("#supply_common_paging_" + feedId).hide();
            return;
        }

        feedComments.ShowMessageBoxPage(feedId, 1);
    },
    ShowMessageBoxPopPage: function (feedId, page) {
        //ajax load list
        var json_data = { feedId: feedId, page: page, pageSize: 3 };
        $.post("/Supplier/FeedCommentListPop", json_data, function (json) {
            $("#operatingPop_" + feedId).html(json.Message);
            setTimeout(function () {
                scroll_init(scroll_blue);
                if (parseInt($("#replyBoxPop_" + feedId).height()) > 300) {
                    $(".jscroll-e").show();
                }
            }, 300);
            $("#commnetTxtPop_" + feedId).focus(function () {
                var val = "Write a message...";
                if (val === $("#commnetTxtPop_" + feedId).val()) { $("#commnetTxtPop_" + feedId).val("") }
            }).blur(function () {
                if ($("#commnetTxtPop_" + feedId).val() == "") { $("#commnetTxtPop_" + feedId).val("Write a message...") }
            });
        }, "json");
    },
    ShowMessageBoxPop: function (feedId) {
        if ($("#replyBoxPop_" + feedId).hasClass("show")) {
            $("#replyBoxPop_" + feedId).removeClass("show");
            $("#replyBoxPop_" + feedId).hide();
            $("#supply_common_paging_pop_" + feedId).hide();
            return;
        }

        feedComments.ShowMessageBoxPopPage(feedId, 1);
    },
    SaveComment: function (feedId) {
        if ($("#commnetTxt_" + feedId).val() == "Write a message...") {
            Boxy.alert("The text field is empty. Please write in your comment before posting!", null, { title: "" });
            $(".boxyInner").addClass("popup");
            return;
        }
        var json_data = { feedId: feedId, commentTxt: encodeURIComponent($("#commnetTxt_" + feedId).val()) };
        $.post("/Supplier/FeedCommentSave", json_data, function (json) {
            if (json.Success) {
                feedComments.ShowMessageBoxPage(feedId, 1);
            } else {
                Boxy.alert(json.Message, null, { title: "Message" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
    },
    SaveCommentPop: function (feedId) {
        if ($("#commnetTxtPop_" + feedId).val() == "Write a message...") {
            Boxy.alert("The text field is empty. Please write in your comment before posting!", null, { title: "" });
            $(".boxyInner").addClass("popup");
            return;
        }
        var json_data = { feedId: feedId, commentTxt: encodeURIComponent($("#commnetTxtPop_" + feedId).val()) };
        $.post("/Supplier/FeedCommentSavePop", json_data, function (json) {
            if (json.Success) {
                feedComments.ShowMessageBoxPopPage(feedId, 1);
            } else {
                Boxy.alert(json.Message, null, { title: "Message" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
    }
}