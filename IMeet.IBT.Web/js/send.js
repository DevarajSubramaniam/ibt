﻿var sendPwd = {
    send: function () {
        if ($('#forgotForm').data('bValidator').validate() == false) {
            return;
        }
        var json_data = $("#forgotForm").MySerialize();
        $.post("/Account/SendPassword", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Forgot Password" });
                $(".boxyInner").addClass("popup");

            } else {
                Boxy.alert(json.Message, null, { title: "Forgot Password" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
       
    }
};

