﻿var myListMaps = {
    prevZoom: 2,
    map: null,
    source: null,// ['Manly Beach', -33.80010128657071, 151.28747820854187,1,1683, '/images/icon/restaurant2.png'], title,lan,lon,ibttype,id,icon
    userZoom: true,
    markersArray: [],
    UserId: 0,
    infoBoxOptions: {
        content: ""
        , disableAutoPan: false
        , maxWidth: 0
        , pixelOffset: new google.maps.Size(-140, 0)
        , zIndex: null
        , boxStyle: {
            background: "url('/images/map_tips_t.png') no-repeat"
            , opacity: 0.85
            , width: "308px"
            , padding: "11px 0 0"
        }
        , closeBoxMargin: "0 8px -13px"
        , closeBoxURL: "/images/close.gif"
        , infoBoxClearance: new google.maps.Size(20, 20)
        , isHidden: false
        , pane: "floatPane"
        , enableEventPropagation: false
    },
    init: function (locations) {
        if (locations != null && locations.length > 0) {
            var initLatlng = new google.maps.LatLng(locations[0].lat, locations[0].lng);
            var mapOptions = {
                zoom: 2,
                minZoom: 2,
                maxZoom: 19,
                center: initLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            myListMaps.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

            myListMaps.setMarkers(myListMaps.map, locations);

            //zoom
            google.maps.event.addListener(myListMaps.map, 'zoom_changed', function () {
                if (myListMaps.userZoom) {
                    myListMaps.userZoom = false;

                    if (myListMaps.map.getZoom() >= 8) {
                        //profileMaps.prevZoom = profileMaps.map.getZoom();
                        if (myListMaps.prevZoom < 8) {

                            var _lat = myListMaps.map.getCenter().lat();
                            var _lng = myListMaps.map.getCenter().lng();
                            setTimeout(function () {
                                var json_data = { userId: myListMaps.UserId, lat: _lat, lng: _lng, typ: 3 };
                                $.ajax({
                                    url: "/map/GetMyListMapsList", data: json_data, success: function (json) {
                                        myListMaps.clearOverlays();
                                        myListMaps.setMarkers(myListMaps.map, json);
                                    }, dataType: "json", async: false, cache: true
                                });
                            }, 5000);
                        }
                    }
                    if (myListMaps.map.getZoom() <= 4) {
                        if (myListMaps.prevZoom > 4) {

                            var _lat = myListMaps.map.getCenter().lat();
                            var _lng = myListMaps.map.getCenter().lng();

                            var json_data = { userId: myListMaps.UserId, lat: _lat, lng: _lng, typ: 1 };
                            $.ajax({
                                url: "/map/GetMyListMapsList", data: json_data, success: function (json) {
                                    myListMaps.clearOverlays();
                                    myListMaps.setMarkers(myListMaps.map, json);
                                }, dataType: "json", async: false, cache: true
                            });

                        }
                    }

                    if (myListMaps.map.getZoom() > 4 && myListMaps.map.getZoom() < 8) {
                        if (myListMaps.prevZoom <= 4 || myListMaps.prevZoom >= 8) {

                            var _lat = myListMaps.map.getCenter().lat();
                            var _lng = myListMaps.map.getCenter().lng();
                            var json_data = { userId: myListMaps.UserId, lat: _lat, lng: _lng, typ: 2 };
                            $.ajax({
                                url: "/map/GetMyListMapsList", data: json_data, success: function (json) {
                                    myListMaps.clearOverlays();
                                    myListMaps.setMarkers(myListMaps.map, json);
                                }, dataType: "json", async: false, cache: true
                            });
                        }
                    }
                    myListMaps.prevZoom = myListMaps.map.getZoom();
                    myListMaps.userZoom = true;
                }
            });
        } else {
            var mapOptions = {
                zoom: 2,
                minZoom: 2,
                maxZoom: 19,
                center: new google.maps.LatLng(-34.397, 150.644),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map_canvas'),
                mapOptions);
        }
        //end:zoom
    },
    setMarkers: function (map, locations) {
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var image = new google.maps.MarkerImage(beach.icon);
            var myLatLng = new google.maps.LatLng(beach.lat, beach.lng);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                title: beach.Title,
                zIndex: null,
                customId: beach.Id,
                typ: beach.typ
            });
            myListMaps.markersArray.push(marker);
            myListMaps.setMarkerClick(map, marker, beach);
            
        } //end:for
    },
    setMarkerClick: function (map, marker, beach) {
        google.maps.event.addListener(marker, "click", function (e) {
            if (map.getZoom() < 9 && marker.typ != 3) {
                var _lat = myListMaps.map.getCenter().lat();
                var _lng = myListMaps.map.getCenter().lng();
                myListMaps.clearOverlays();
                if (map.getZoom() <= 4) {
                    map.setCenter(marker.getPosition());
                    map.setZoom(5);
                    var json_data = { userId: myListMaps.UserId, lat: _lat, lng: _lng, typ: 2 };
                    $.ajax({
                        url: "/map/GetMyListMapsList", data: json_data, success: function (json) {
                            myListMaps.setMarkers(myListMaps.map, json);
                        }, dataType: "json", async: false, cache: true
                    });
                } else {
                    map.setCenter(marker.getPosition());
                    map.setZoom(9);
                    var json_data = { userId: myListMaps.UserId, lat: _lat, lng: _lng, typ: 3 };
                    $.ajax({
                        url: "/map/GetMyListMapsList", data: json_data, success: function (json) {
                            myListMaps.setMarkers(myListMaps.map, json);
                        }, dataType: "json", async: false, cache: true
                    });
                }

            }
            else {
                $(".infoBox:visible").detach();

                var boxText = document.createElement("div");
                boxText.setAttribute("id", "boxId" + beach[4]);
                //boxText.className = 'mapInfoBox';
                boxText.innerHTML = myListMaps.getSupplyInfo(this.customId);
                myListMaps.infoBoxOptions["content"] = boxText;
                var ib = new InfoBox(myListMaps.infoBoxOptions);
                ib.open(map, this);
            }
        });
    },
    clearOverlays: function () {
        for (var i = 0; i < myListMaps.markersArray.length; i++) {
            myListMaps.markersArray[i].setMap(null);
        }
    }
    , getData: function (typ, lat, lng) {
        var json_data = {userId: myListMaps.UserId, lat: lat, lng: lng };
        $.ajax({
            url: "/map/GetMyListMapsList", data: json_data, success: function (json) {
                return json;
            }, dataType: "json", async: false
        });
    },
    getSupplyInfo: function (supplyId) {
        var json_data = { supplyId: supplyId, userId: myListMaps.UserId };
        var res = "";
        $.ajax({
            url: "/Map/GetMyListSupplyInfo", data: json_data, success: function (json) {
                if (json.Success) {
                    res = json.Message;
                } else {
                    alert("Error!");
                    res = "no data";
                }
            }, dataType: "json", async: false
        });

        return res;
    }
};