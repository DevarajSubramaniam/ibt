﻿var jTravelsAdd = {
    selectCountryId: 0,
    selectCityId: 0,
    selectVenuesId: 0,
    currentStepN: 1,
    beforeStepN:1,
    venuesPage: 1,
    sortName: "All",
    posting: true,
    searching: false,
    Step1: function (num) {
        //如果从第二步退回第一步，先保存第二步选择的城市
        if (num != undefined && num == 2) {
            this._Step2Save();
            //$("#iptCityIds").val('');
        }
        this.goStepN(num);
        this.currentStepN = 1;
        var json_Data = { Keyword: $("#filterKeyword").val(), countryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val() };
        $.getJSON("/Supplier/TravelsAdd_Step1", json_Data, function (json) {
            if (json.Success) {
                $("#countriesBox").html(json.Message);
                stepTab();
                //jTravelsAdd.SearchCountries();

                //$("#countriesBox").before("<div>hello</div>");
                //$("#countriesBox").after("<div class=\"outBtn fixbox mT10 mB20\" id=\"step1\"><span class=\"mL10 txt left\"><p><span class=\"checkbox disable\"></span> Check the place that I've Been</p><p><span class=\"smallHistiory\"></span> The place that I've posted</p></span><span class=\"right\"><a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step2(1);\"><span class=\"bg\">NEXT</span></a><a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step4(1);\"><span class=\"bg\">FINISH</span></a></span></div>");

                $("#step1").attr("style", "display:block;");
                $("#TopStep1").attr("style", "display:block;");
                $("#step1").show();
                $("#TopStep1").show();
                $('#stepTitle_1').show();
                $("#step2").hide();
                $("#TopStep2").hide();
                $('#stepTitle_2').hide();
                $("#step3").hide();
                $("#TopStep3").hide();
                $('#stepTitle_3').hide();
                $("#step4").hide();
                $("#TopStep4").hide();
                $('#stepTitle_4').hide();
                $("#page_content").removeClass();
                $("#page_content").addClass("content step1");
                $('#topBar_warp').removeClass();
                $('#topBar_warp').addClass('wrap step1');
                checkboxshow();
                $("#filterKeyword").val('Search By Country');
                //scroll_init(scroll_blue);
            }
        });
    },
    Step2: function (num) {
        if (num != undefined && num == 1) {
            this._Step1Save();
        }
        if (num != undefined && num == 3) {
            this._Step3Save();
        }
        this.goStepN(num);
        this.currentStepN = 2;
        var countryIds = $("#iptCountryIds").val();
        var cityids = $("#iptCountryCity").val();

        if (countryIds == "") {
            //jTravelsAdd.Step1(0);
            Boxy.alert("Please select a country before proceeding!", null, { title: 'Select Countries' });
            $(".boxyInner").addClass("popup");
            return;
        }

        var json_Data = { keyWord: $("#cityKeyword").val(), countryIds: countryIds, cityids: cityids, selectCountryId: this.selectCountryId, epoch: $("#iptEpoch").val() };
        $("#step2Loading").show();
        $.getJSON("/Supplier/TravelsAdd_Step2", json_Data, function (json) {
            if (json.Success) {
                $("#countriesBox").html(json.Message);
                $("#step2Loading").hide();
                stepTab();
                //jTravelsAdd.SearchCities();
                //$("#countriesBox").after("<div class=\"outBtn fixbox mT10 mB20\" id=\"step2\"><span class=\"mL10 txt left\"><p><span class=\"checkbox disable\"></span> Check the place that I've Been</p><p><span class=\"smallHistiory\"></span> The place that I've posted</p></span><span class=\"right\"> <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step1(2);\" class=\"btnBlueH33\"><span class=\"bg\">BACK</span></a><a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step3(2);\"><span class=\"bg\">NEXT</span></a><a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step4(2);\"><span class=\"bg\">FINISH</span></a></span></div>");

                $("#step2").show();
                $("#TopStep2").show();
                $("#step1").hide();
                $("#TopStep1").hide();
                $("#step3").hide();
                $("#TopStep3").hide();
                $("#step4").hide();
                $("#TopStep4").hide();
                $('#stepTitle_2').show();
                $('#stepTitle_1').hide();
                $('#stepTitle_3').hide();
                $('#stepTitle_4').hide();
                $("#page_content").removeClass();
                $("#page_content").addClass("content step2");
                $('#topBar_warp').removeClass();
                $('#topBar_warp').addClass('wrap step2');
                jTravelsAdd.selectCountryId = json.Item.ParentId;
                $("#iptCityIds").val(json.Item.Ids);
                checkboxshow();
                //scroll_init(scroll_blue);
                $("#filterKeyword").val('');
                if ($("#CityLists dd").size() == 0) {
                    $("#step3CityTxt").text("You have not selected a city!");
                }
            }
        });
    },
    Step2_1: function (countryId) {


        //iptCountryCity: 4_3,4,5-7_5,1,9
        var json_Data = { countryId: countryId, selCityId: "" };

    },
    Step3: function (num) {
        if (num != undefined && num == 2) {
            this._Step2Save();
        }
        if (num != undefined && num == 4) {
        }
        $('#filterLetter').val('');
        var cityids = $("#iptCityIds").val();
        if (cityids == "") {
            //jTravelsAdd.Step1(0);
            Boxy.alert("Please select a city before proceeding!", null, { title: 'Check in Cities' });
            $(".boxyInner").addClass("popup");
            return;
        }

        this.goStepN(num);
        this.currentStepN = 3;
        var json_Data = { keyWord: $("#supplyKeyword").val(), filterLetter: $("#filterLetter").val(), countryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val(), selectCityId: this.selectCityId, selVenueIds: $("#iptVenuesIds").val() };
        $.getJSON("/Supplier/TravelsAdd_Step3", json_Data, function (json) {
            if (json.Success) {
                $("#countriesBox").html(json.Message);
                stepTab();
                //$("#countriesBox").after("<div class=\"outBtn fixbox mT10 mB20\" id=\"step3\"><span class=\"mL10 txt left\"><p><span class=\"checkbox disable\"></span> Check the place that I've Been</p><p><span class=\"smallHistiory\"></span> The place that I've posted</p></span><span class=\"right\"> <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step2(3);\" class=\"btnBlueH33\"><span class=\"bg\">BACK</span></a><a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step4(3);\"><span class=\"bg\">FINISH</span></a></span></div>");
                initKeyWord();
                $("#step3").show();
                $("#TopStep3").show();
                $("#step1").hide();
                $("#TopStep1").hide();
                $("#step2").hide();
                $("#TopStep2").hide();
                $("#step4").hide();
                $("#TopStep4").hide();
                $('#stepTitle_3').show();
                $('#stepTitle_1').hide();
                $('#stepTitle_2').hide();
                $('#stepTitle_4').hide();
                $("#page_content").removeClass();
                $("#page_content").addClass("content step3");
                $('#topBar_warp').removeClass();
                $('#topBar_warp').addClass('wrap step3');
                jTravelsAdd.selectCityId = json.Item.cityId;
                $("#iptVenuesIds").val(json.Item.Ids);
                if (json.Item.item == 10) {
                    $("#loadSeeMore").show();
                } else {
                    $("#loadSeeMore").hide();
                }
                if (jTravelsAdd.venuesPage <= 1) {
                    $("#loadSeeMorePrev").hide();
                } else {
                    $("#loadSeeMorePrev").show();
                }
                checkboxshow();
                selectSpan();
                $("#filterKeyword").val('');
                if ($('#CityLists dl').html() == '')
                    $("#CityLists dl").html("You have not selected a city. Click <a href='javascript:void(0);' onclick='jTravelsAdd.Step2(3);'>here</a> to make your selection.");
                $(".jscroll-e").show();
                //scroll_init(scroll_blue);
                jTravelsAdd._Step3VenuesRating();
            }
        });
    },
    Step4: function (num) {
        if ($('#iptCountryIds').val() == '') {
            Boxy.alert("Please select a country before proceeding!", null, { title: 'Select Countries' });
            $(".boxyInner").addClass("popup");
            return;
        }
        if (num != undefined && num == 1) {
            this._Step1Save();
        }
        if (num != undefined && num == 2) {
            this._Step2Save();
        }
        if (num != undefined && num == 3) {
            this._Step3Save();
        }
        jTravelsAdd.beforeStepN = num;
        this.goStepN(num);
        this.currentStepN = 4;
        var json_Data = { counryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val() };
        $.ajax({
            url: "/Supplier/TravelsAdd_Step4", data: json_Data, success: function (json) {
                if (json.Success) {
                    $("#countriesBox").html(json.Message);
                    stepTab();
                    //$("#countriesBox").append("<div class=\"outBtn fixbox mT10 mB20\" id=\"step4\"><span class=\"mL10 txt left\"><p><span class=\"checkbox disable\"></span> Check the place that I've Been</p><p><span class=\"smallHistiory\"></span> The place that I've posted</p></span><span class=\"right\"><a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step1(4);\" class=\"btnBlueH33\"><span class=\"bg\">EDIT</span></a><a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.btnConfrim();\"><span class=\"bg\">FINISH</span></a></span></div>");

                    $("#step4").show();
                    $("#TopStep4").show();
                    $("#step1").hide();
                    $("#TopStep1").hide();
                    $("#step2").hide();
                    $("#TopStep2").hide();
                    $("#step3").hide();
                    $("#TopStep3").hide();
                    $('#stepTitle_4').show();
                    $('#stepTitle_1').hide();
                    $('#stepTitle_2').hide();
                    $('#stepTitle_3').hide();
                    $("#page_content").removeClass();
                    $("#page_content").addClass("content step4");
                    $('#topBar_warp').removeClass();
                    $('#topBar_warp').addClass('wrap step4');
                    checkboxshow();
                    //scroll_init(scroll_blue);
                }
            }, dataType: "json", async: false
        });

        //maps
        //var maps_data = { mapType: 1, userId: UserId, lat: $config["lat"], lng: $config["lng"], iptEpoch: $("#iptEpoch").val() };
        //$.ajax({
        //    url: "/map/GetAddTravelsMapsList", data: maps_data, success: function (json) {
        //        if (json.Success) {
        //            addTravelsMaps.source = json.Item;
        //            addTravelsMaps.init(json.Item);
        //        }
        //    }, dataType: "json", async: false
        //});
    },
    goStepN: function (num) {
        if (num != undefined && num == 0) {
            if (this.currentStepN == 1) {
                this._Step1Save();
            }
            if (this.currentStepN == 2) {
                this._Step2Save();
            }
            if (this.currentStepN == 3) {
                this._Step3Save();
            }
        }
    },
    btnConfrim: function () {
        var json_Data = { epoch: $("#iptEpoch").val() };
        showPop();
        $.getJSON("/Supplier/TravelsAdd_Confirm", json_Data, function (json) {
            myloadingbox.hide();
            if (json.Success) {
                Boxy.alert("You have successfully added your travels!", function () {
                    location.href = "/Profile";
                }, { title: "Add Travels" });
                $(".boxyInner").addClass("popup");
            }
        });
    },
    ckbCountry: function (obj) {
        var vals = $("#iptCountryIds").val();
        var cid = $(obj).attr("data-countryid");
        if ($(obj).hasClass("uncheckbox")) {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != cid) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }

            vals = res;
        } else {
            if (vals.length > 0)
                vals = vals + "," + cid;
            else
                vals = cid;
        }
        $("#iptCountryIds").val(vals);
    },
    ckbCity: function (obj) {
        var vals = $("#iptCityIds").val();
        var cid = $(obj).attr("data-cityid");
        if ($(obj).hasClass("uncheckbox")) {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != cid) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }

            vals = res;
        } else {
            if (vals.length > 0)
                vals = vals + "," + cid;
            else
                vals = cid;
        }
        $("#iptCityIds").val(vals);
    },
    GetCityList: function (obj, countryId, stepNum) {
        //$("#iptCountryCity").attr("value", "");
        stepNum = stepNum == undefined ? 0 : stepNum;
        //todo:1 save 当前的选择
        var selCityIds = $("#iptCityIds").val();//ibt.getCheckboxVal("#CityLists dl>dd>span", "data-cityId");
        //get
        var json_Data = { countryId: countryId, saveCountryId: jTravelsAdd.selectCountryId/*this.selectCountryId*/, selCityIds: selCityIds, epoch: $("#iptEpoch").val(), stepNum: stepNum };
        $("#step2Loading").show();
        $.post("/Supplier/TravelsAdd_Step2_1", json_Data, function (json) {
            if (json.Success) {
                $("#CityLists").html(json.Message);
                $("#step2Loading").hide();
                $("#iptCityIds").val(json.Item);
                stepTab();
                jTravelsAdd.selectCountryId = countryId;
                $(obj).parent().parent().children().each(function () {
                    $(this).children().removeClass("active");
                });

                if ($("#CityLists dd").size() == 0) {
                    $("#step3CityTxt").text("You have not selected a city!");
                }
                //选中当前的
                $(obj).addClass("active");
                checkboxshow();
                //jTravelsAdd.SearchCities();
                //scroll_init(scroll_blue);
                if (stepNum == 3) {
                    $("#feedBox").html("");
                    $("#loadSeeMore").hide();
                    if ($('#iptCityIds').val() == '') {
                        $("#CityLists dl").html("You have not selected a city. Click <a href='javascript:void(0);' onclick='jTravelsAdd.Step2(3);'>here</a> to make your selection.");
                    } else {
                        $("#CityLists dl dd:first").click();
                    }
                }
                $('#filterLetter').val('');
                $('.letterBarVertical').children().each(function () {
                    $(this).removeClass('active');
                });
            }
        }, "json");
    },
    GetVenuesList: function (obj, cityId, stepNum) {
        var thisTagName = $(obj)[0].tagName;
        if (thisTagName == "DD") {
            $("#filterLetter").val("");
            $("#supplyKeyword").val("Search Supplier Name");
            jTravelsAdd.sortName = "All";
            $("#SortByName").html("Sort By");
            $(".letterBarVertical").children().each(function () {
                $(this).removeClass("active");
            });
            $(obj).parent().parent().children().each(function () {
                $(this).children().removeClass("active");
            });
            $(obj).addClass("active");
        }

        this.venuesPage = 1;
        stepNum = stepNum == undefined ? 0 : stepNum;
        //jTravelsAdd.selectCityId = cityId;
        var selVenueIds = $("#iptVenuesIds").val();//this._Step3GetVenuesIds();
        var json_data = { keyWord: $("#supplyKeyword").val(), filterLetter: $("#filterLetter").val(), sortName: jTravelsAdd.sortName, cityId: cityId, selCityId: jTravelsAdd.selectCityId, selVenueIds: selVenueIds, epoch: $("#iptEpoch").val() };

        $("#step3Loading").show();
        $.post("/Supplier/TravelsAdd_Step3_1", json_data, function (json) {
            if (json.Success) {
                $("#step3Loading").hide();
                $("#feedBox").html(json.Message);
                initKeyWord();
                jTravelsAdd.selectCityId = cityId;
                $("#iptVenuesIds").val(json.Item.Ids);
                //if (thisTagName == "DD") {
                //    $(obj).parent().parent().children().each(function () {
                //        $(this).children().removeClass("active");
                //    });
                //}
                if (json.Item.item == 10) {
                    $("#loadSeeMore").show();
                } else {
                    $("#loadSeeMore").hide();
                }
                //if (jTravelsAdd.venuesPage <= 1) {
                //    $("#loadSeeMorePrev").hide();
                //} else {
                //    $("#loadSeeMorePrev").show();
                //}
                //选中当前的
                //if (thisTagName == "DD")
                //    $(obj).addClass("active");

                checkboxshow();
                stepTab();
                selectSpan();
                $(".jscroll-e").show();
                //scroll_init(scroll_blue);
                jTravelsAdd._Step3VenuesRating();
                $(".jscroll-c").css("top", "0");
            }
        }, "json");
    },
    _Step1Save: function () {
        var json_Data = { selCountryIds: $("#iptCountryIds").val(), epoch: $("#iptEpoch").val() };
        $.ajax({
            url: "/Supplier/TravelsAdd_Step1_Save", data: json_Data, async: false, dataType: "json", type: "post", cache: false, timeout: 8000,
            success: function (json) {
                //only save
            }
        })
        //$.post("/Supplier/TravelsAdd_Step1_Save", json_Data, function (json) {
        //    if (json.Success) {
        //        //only save
        //    }
        //}, "json");
    },
    _Step2Save: function () {
        var selCityIds = $("#iptCityIds").val();//ibt.getCheckboxVal("#CityLists dl>dd>span", "data-cityId");
        var json_Data = { countryId: this.selectCountryId, saveCountryId: this.selectCountryId, selCityIds: selCityIds, epoch: $("#iptEpoch").val() };
        $.ajax({
            url: "/Supplier/TravelsAdd_Step2_Save", data: json_Data, async: false, dataType: "json", type: "post", cache: false, timeout: 8000,
            success: function (json) {
                if ($("#CityLists dd").size() == 0) {
                    $("#step3CityTxt").text("You have not selected a city!");
                }
            }
        })
        //$.post("/Supplier/TravelsAdd_Step2_Save", json_Data, function (json) {
        //    if (json.Success) {
        //        //only save
        //        if ($("#CityLists dd").size() == 0) {
        //            $("#step3CityTxt").text("You have not selected a city!");
        //        }
        //    }
        //}, "json");
    },
    _Step3Save: function () {
        var selVenueIds = $("#iptVenuesIds").val();//this._Step3GetVenuesIds();
        var json_Data = { selCityId: jTravelsAdd.selectCityId, selVenueIds: selVenueIds, epoch: $("#iptEpoch").val() };
        $.ajax({
            url: "/Supplier/TravelsAdd_Step3_Save", data: json_Data, async: false, dataType: "json", type: "post", cache: false, timeout: 8000,
            success: function (json) {
                //only save
            }
        })
        //$.post("/Supplier/TravelsAdd_Step3_Save", json_Data, function (json) {
        //    if (json.Success) {
        //    }
        //}, "json");
    },
    _Step3VenuesRating: function () {
        $('.StepRating').raty({
            readOnly: false,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                /*ibt.rrwb_rating(ibt.IBTType_Supply, $(this).attr('data-objId'), score); 20130222裕冲修改*/
                ibt.rrwb_rating(0, ibt.IBTType_Supply, $(this).attr('data-objId'), score);
            }
        });
    },
    _Step3GetVenuesIds: function () {
        var vals = "";
        $("dd a").each(function () {
            if ($(this).attr("data-sel") == "1") {
                vals = vals + $(this).attr("data-id") + ",";
            }
        });
        if (vals.length > 0) {
            vals = vals.substring(0, vals.length - 1);
        }
        return vals;
    },
    Step3IBT: function (obj, id) {
        if ($(obj).attr("data-sel") == "0") {
            var ibtCount = parseInt($(obj).attr("data-ibtnum")) + 1;
            isAdd = true;
            //选择
            $(obj).attr("data-sel", 1);
            $(obj).attr("data-ibtnum", ibtCount);
            $(obj).removeClass();
            $(obj).addClass("btnBlueOkH27");
            $(obj).html("<span class=\"btnBlueNumH27\"><span class=\"btnBlueAddH27\">I've Been There</span>" + ibtCount + "</span>");

        } else { //取消选择
            var ibtCount = parseInt($(obj).attr("data-ibtnum")) - 1;
            ibtCount = ibtCount < 0 ? 0 : ibtCount;
            $(obj).attr("data-sel", 0);
            $(obj).attr("data-ibtnum", ibtCount);
            $(obj).removeClass();
            if (ibtCount < 1) {
                $(obj).addClass("feedBox_IBT btnGrayDotH29");
                $(obj).html("<span class=\"bg\">I've Been There</span>");
            } else {
                $(obj).addClass("btnGrayOkH29");
                $(obj).html("<span class=\"btnGrayNumH29\"><span class=\"btnGrayAddH29\">I've Been There</span>" + ibtCount + "</span>");
            }
        }

        var vals = $("#iptVenuesIds").val();
        var did = $(obj).attr("data-id");
        if ($(obj).attr("data-sel") == "0") {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != did) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }

            vals = res;
        } else {
            if (vals.length > 0)
                vals = vals + "," + did;
            else
                vals = did;
        }
        $("#iptVenuesIds").val(vals);
    },
    Step3VenuesSort: function (sortName, page) {
        jTravelsAdd.sortName = sortName;
        if (jTravelsAdd.sortName != "All") {
            $("#SortByName").text(sortName);
        } else {
            $("#SortByName").text("Sort By");
        }
        if (page==1) {
            $("#filterLetter").val("");
            $(".letterBarVertical").children().each(function () {
                $(this).removeClass("active");
            });
        }
        var selVenueIds = $("#iptVenuesIds").val();//this._Step3GetVenuesIds();
        var json_data = { keyWord: $("#supplyKeyword").val(), cityId: jTravelsAdd.selectCityId, filterLetter: $("#filterLetter").val(), page: page, sortName: sortName, selVenueIds: selVenueIds, epoch: $("#iptEpoch").val() };
        $("#step3Loading").show();
        $.post("/Supplier/TravelsAdd_Step3_1", json_data, function (json) {
            if (json.Success) {
                $("#step3Loading").hide();
                //$("#feedBox").html(json.Message);
                if (page == 1)
                    $("#feedBox").empty();
                $("#feedBox").append(json.Message);
                jTravelsAdd.venuesPage = page;
                if (json.Item.item == 10) {
                    $("#loadSeeMore").show();
                } else {
                    $("#loadSeeMore").hide();
                }
                //if (jTravelsAdd.venuesPage <= 1) {
                //    $("#loadSeeMorePrev").hide();
                //} else {
                //    $("#loadSeeMorePrev").show();
                //}
                //选中当前的
                checkboxshow();
                selectSpan();
                $(".jscroll-e").show();
                //scroll_init(scroll_blue);
                jTravelsAdd._Step3VenuesRating();

                $(".jscroll-c").css("top", "0");
            }
        }, "json");
    },
    Step3LoadMore: function () {
        /*20130225裕冲修改*/
        //jTravelsAdd.Step3VenuesSort(jTravelsAdd.sortName, jTravelsAdd.venuesPage + 1);
        jTravelsAdd.venuesPage++;
        //if (jTravelsAdd.venuesPage >= 1) {
        //    $("#loadSeeMorePrev").show();
        //}
        jTravelsAdd.Step3VenuesSort(jTravelsAdd.sortName, jTravelsAdd.venuesPage);
        $(".jscroll-c").css("top", "0");
    },
    /*20130225裕冲修改*/
    Step3LoadMorePrev: function () {
        jTravelsAdd.venuesPage--;
        if (jTravelsAdd.venuesPage < 1) {
            $("#loadSeeMorePrev").hide();
        } else {
            $("#loadSeeMorePrev").show();
            jTravelsAdd.Step3VenuesSort(jTravelsAdd.sortName, jTravelsAdd.venuesPage);
        }
        $(".jscroll-c").css("top", "0");
    },
    /*修改结束*/
    SearchCountries: function () {
        $("#filterKeyword").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: jTravelsAdd.Step1(),
                    data: {
                        name: request.term
                    },
                    dataType: "json"
                    //success: function (data, textStatus, jqXHR) {
                    //    response($.map(data, function (item, index) {
                    //        return item["FirstName"] + " " + item["LastName"] + " (" + item["FriendId"] + ")";
                    //    }));

                    //}
                });
            },
            minLength: 3
        });
    },
    SearchCities: function () {
        $("#cityKeyword").autocomplete({
            source: function (request, response) {
                $.ajax({
                    //url: jTravelsAdd.Step2(),
                    url: jTravelsAdd.citySearch(),
                    data: {
                        name: request.term
                    },
                    dataType: "json"
                });
            },
            minLength: 3
        });
        this._Step1Save();
    },
    SearchSupplier: function () {
        $("#supplyKeyword").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: jTravelsAdd.Step3(),
                    data: {
                        name: request.term
                    },
                    dataType: "json"
                });
            },
            minLength: 3
        });
        this._Step2Save();
    },
    goTab: function (obj) {
        $("#feedBox").empty();//先删除以前的
        $("#filterLetter").val("");
        $(".letterBarVertical").children().each(function () {
            $(this).removeClass("active");
        });
        //jTravelsAdd.Step3(0);
        jTravelsAdd.Step3VenuesSort(jTravelsAdd.sortName, 1);
    },
    //快速搜索
    quickSearch: function (obj, letter, objId) {
        //jTravelsAdd.GetVenuesList(this, " + item.CityId.ToString() + ", 3);
        objId = jTravelsAdd.selectCityId;
        $("#feedBox").empty();
        if ($("#filterLetter").val() == letter) {
            if (letter != '')
                $(obj).removeClass("active");
            else
                $(obj).toggleClass("active");
            $("#filterLetter").val("");
            jTravelsAdd.GetVenuesList(obj, objId, 3);
        } else {
            $(obj).parent().children().each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#filterLetter").val(letter);
            jTravelsAdd.GetVenuesList(obj, objId, 3);
        }
    },
    citySearch: function (e) {
        var e = e || window.event;
        var keyCode = e.keyCode || e.which;
        var oTarget = e.srcElement || e.target;
        if ((keyCode == 13 || $("#cityKeyword").val().length >= 3) && !jTravelsAdd.searching) {
            jTravelsAdd.getCityListSearch();
        }
    },
    getCityListSearch: function () {
        var json_Data = { keyWord: $("#cityKeyword").val(), cityids: $("#iptCityIds").val(), selectCountryId: jTravelsAdd.selectCountryId, filter: $('#filterLetter').val() };
        if (jTravelsAdd.posting) {
            $("#step2Loading").show();
            jTravelsAdd.posting = false;
            $.post("/Supplier/TravelsAdd_Step2Search", json_Data, function (json) {
                if (json.Success) {
                    $("#CityListsScrollBox").html(json.Message);
                    checkboxshow();
                    $("#step2Loading").hide();
                    jTravelsAdd.searching = false;
                    jTravelsAdd.posting = true;
                }
            });
        }
    },
    cityFilterSearch: function (obj, letter) {
        if ($('#filterLetter').val() == letter) {
            if (letter != '')
                $(obj).removeClass("active");
            else
                $(obj).toggleClass("active");
            $('#filterLetter').val('');
        } else {
            $(obj).parent().children().each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#filterLetter").val(letter);
        }
        jTravelsAdd.getCityListSearch();
    },
    getCountryListSearch: function () {
        var json_Data = { countryIds: $('#iptCountryIds').val(), keyWord: $('#filterKeyword').val(), filter: $('#filterLetter').val() };
        if (jTravelsAdd.posting) {
            jTravelsAdd.posting = false;
            $('#step1Loading').show();
            $.post("/Supplier/TravelsAdd_Step1Search", json_Data, function (json) {
                if (json.Success) {
                    $('#countriesList').html(json.Message);
                    checkboxshow();
                    jTravelsAdd.posting = true;
                }
                $('#step1Loading').hide();
            });
        }
    },
    countryFilterSearch: function (obj, letter) {
        if ($('#filterLetter').val() == letter) {
            if (letter != '')
                $(obj).removeClass("active");
            else
                $(obj).toggleClass("active");
            $('#filterLetter').val('');
        } else {
            $(obj).parent().children().each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#filterLetter").val(letter);
        }
        jTravelsAdd.getCountryListSearch();
    },
    countrySearch: function (e) {
        var e = e || window.event;
        var keyCode = e.keyCode || e.which;
        var oTarget = e.srcElement || e.target;
        if ((keyCode == 13 || $("#filterKeyword").val().length >= 3) && jTravelsAdd.posting) {
            jTravelsAdd.getCountryListSearch();
            //jTravelsAdd.countrySearch();
            //            jTravelsAdd.countryFilterSearch();
        }
    },
    back: function () {
        if (jTravelsAdd.beforeStepN == 1)
            jTravelsAdd.Step1(0);
        else if (jTravelsAdd.beforeStepN == 2)
            jTravelsAdd.Step2(0);
        else if (jTravelsAdd.beforeStepN == 3)
            jTravelsAdd.Step3(0);
    },
    clickVenues: function (url) {
        jTravelsAdd._Step3Save();
        Boxy.ask("<p class='italic'>Would you like to save your selections to your travel history before leaving ADD+HISTORY? Leaving this section will clear out your selections unless you save.</p>"
            , ["YES", "CLEAR OUT", "CANCEL"]
            , function (val) {
                if (val == "YES") {
                    var json_Data = { epoch: $("#iptEpoch").val() };
                    showPop();
                    $.getJSON("/Supplier/TravelsAdd_Confirm", json_Data, function (json) {
                        myloadingbox.hide();
                        if (json.Success) {
                            Boxy.alert("You have successfully added your travels!", function () {
                                location.href = url;
                            }, { title: "Add Travels" });
                            $(".boxyInner").addClass("popup");
                        }
                    });
                } else if (val == "CLEAR OUT") {
                    location.href = url;
                }
            }
            , { title: "Add Travels" });
        $(".boxyInner").addClass("popup");
    }
};
