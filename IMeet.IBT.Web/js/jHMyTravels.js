﻿var TravelTravelhistoryList = {
    page:1,
    scrolling:false,
    pageSize:10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (TravelhistoryList.scrolling == true) {
                    TravelhistoryList.GetCitiesListByCountryId(TravelhistoryList.page);
                    TravelhistoryList.page++;
                }
            }
        });
    },
    GetCitiesListByCountryId: function (page) {
        TravelhistoryList.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#historyList');
        var json_data = { listCountryId: $("#listCountryId").val(), page: page, pageSize: TravelhistoryList.pageSize, filterKeyword: $("#filterKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Supplier/MyHistoryListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    TravelhistoryList.scrolling = false;
                    TravelhistoryList.itemTotal = json.Item.itemTotal;
                    $("#historyList_count").text(json.Item.total);
                    $('#historyList').append(json.Message);
                    $('.loading').remove();
                    checkboxshow();
                    if ($('#historyList').children('li').length == 0) {
                        $('<div class="noData">Currently, no data!</div>').appendTo('#historyList');
                    }
                    TravelhistoryList.scrolling = true;
                }
            }
        });
    },
    initData: function () {
        $("#totalCount").html("<span class=\"f10\">Total</span>3000");
        $("#historyList").empty();
        TravelhistoryList.GetCitiesListByCountryId(1);
        TravelhistoryList.page = 2;
        $('.noData').detach();
    },
    goTab: function (obj, id) {
        $("#historyList").empty();//先删除以前的
        TravelhistoryList.scrolling = false;
        TravelhistoryList.itemTotal = -1;
        $('.noData').detach();
        $(".listNav a").each(function() {
            $(this).removeClass("btnActive");
            });
        $(obj).addClass("btnActive");
        $("#listCountryId").val(id);
        $("#totalCount").html("<span class=\"f10\">Total</span>3000");
        TravelhistoryList.GetCitiesListByCountryId(1);
    }
}