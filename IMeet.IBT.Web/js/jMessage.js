﻿var messageList = {
    page:1,
    scrolling:true,
    pageSize:10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (messageList.scrolling == true) {
                    messageList.GetDataList(messageList.page);
                    messageList.page++;
                }
            }
        });
    },
    GetDataList: function (page) {
        if (messageList.scrolling) {
            messageList.scrolling = false;
            $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#feedList');
            var json_data = { type: $("#IsShowMyMsg").val(), page: page, pageSize: messageList.pageSize, filterKeyword: $("#filterKeyword").val(), filterField: $("#filterField").val() };
            $.ajax({
                url: '/Connection/MessageListAjx',
                async: false,
                dataType: "json",
                data: json_data,
                type: 'post',
                success: function (json) {
                    if (json.Success) {
                        messageList.scrolling = false;
                        messageList.itemTotal = json.Item.itemTotal;
                        $("#messageList_count").text(json.Item.total);
                        $('#feedList').append(json.Message);
                        $('.alignC').remove();
                        checkboxshow();
                        if ($('#feedList').children('li').length == 0) {
                            $('#feedList').html('<div class="noData">no data!</div>');
                        }
                        messageList.scrolling = true;
                    }
                },
                error: function () { messageList.scrolling = true;}
            });
        }
    },
    initMsgData: function () {
        $("#feedList").empty();
        messageList.GetDataList(1);
        messageList.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#feedList").empty();//先删除以前的
        messageList.scrolling = false;
        messageList.itemTotal = -1;
        $('.noData').remove();
        messageList.GetDataList(1);
    },
    SendMessage: function (objId,msgComent) {  //xg
        var json_data = { objId: objId, msgComent: msgComent };
        $.post("/Connection/SendMessage", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    window.location = "/Connection/Message";
                }, { title: "Message" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Message" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    changeTab: function (id) {
        $("#IsShowMyMsg").val(id);
        messageList.initMsgData();  //xg
    },
    SelAllHandle: function (tabId) {
        var json_data = { selmsgId: $("#hidSelMsgId").val() };
        if (tabId == 1) {
            $.ajax({
                url: '/Connection/SetAllRead', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                    if (json.Success) {
                        Boxy.alert(json.Message, function () {
                            location.reload();
                        }, { title: "Message List" });
                        $(".boxyInner").addClass("popup");
                    }
                    else {
                        Boxy.alert(json.Message, null, { title: "Message List" });
                        $(".boxyInner").addClass("popup");
                    }
                }
            });
        }
        else {
            $.ajax({
                url: '/Connection/SetAllDel', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                    if (json.Success) {
                        Boxy.alert(json.Message, function () {
                            location.reload();
                        }, { title: "Message List" });
                        $(".boxyInner").addClass("popup");
                    }
                    else {
                        Boxy.alert(json.Message, null, { title: "Message List" });
                        $(".boxyInner").addClass("popup");
                    }
                }
            });
        }
    },
    IsRead: function (objId) {
        var json_data = { objId: objId };
        $.post("/Connection/SetIsRead", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Message" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Message" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    Del: function (objId) {
        var json_data = { objId: objId };
        $.post("/Connection/SetIsDel", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Message" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Message" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    GetReplyList: function () {
        if (this.itemTotal == 0) return;
        $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#replyList');
        var json_data = {msgId: $("#hidMsgId").val(), friendId: $("#hidFriendId").val()};
        $.ajax({
            url: '/Connection/ReplyListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    messageList.scrolling = false;
                    messageList.itemTotal = json.Item.itemTotal;
                    $('#replyList').append(json.Message);
                    $('.alignC').remove();
                }
            }
        });
    },
    initReplyData: function () {
        $("#replyList").empty();
        messageList.GetReplyList();//无需分页
        $('.noData').remove();
    },
    SendReply: function () {  //xg
        var json_data = { msgId: $("#hidMsgId").val(), friendId: $("#hidFriendId").val(), replyContent: $("#txtMsgContent").val() };
        $.post("/Connection/SendReply", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Message" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Message" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    SetMsgRead: function (objId) {
        var json_data = { objId: objId };
        $.post("/Connection/SetMsgRead", json_data, function (json) {
            if (json.Success) {
            } 
        }, "json");

    },
};
