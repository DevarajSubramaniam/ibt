﻿var ibt_common = {
    attType_City: 1,
    attType_NewsFeed: 2,
    attType_Supply: 3,
    attType_Country: 4,
    pageIndex:1,
    pageSize:10,
    bo: $.browser,
    rrwb: function (obj) {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }
        var rrwbType = $(obj).attr("data-rrwbType");
        var ibtType = $(obj).attr("data-ibtType");
        var d_val = $(obj).attr("data-val");
        if (d_val == "1") {
            $(obj).attr("data-val", "0");
        }
        else {
            $(obj).attr("data-val", "1");
        }
        var jasonData = { ibtObjId: $("#ibtcommon_ibtObjId").val(), rrwbType: rrwbType, ibtType: $("#ibcommon_ibtType").val(), val: $(obj).attr("data-val") };
        $.post("/IBT/RRWB", jasonData, function (json) {
            if (json.Success) {
                location.reload();
                //$("#ibtcommon_rrwb").html(json.Message);
            } 
        }, "json");
    },
    rating: function () {
        $("#ibtrating").raty({
            readOnly: false,
            score: function () { return $(this).attr("data-rating"); },
            click: function (score, evt) {
                if (UserId == 0) {
                    ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                    return;
                }
                $.post("/IBT/RRWBRating", { ibtObjId: $("#ibtcommon_ibtObjId").val(), score: score, ibtType: $("#ibcommon_ibtType").val() }, function (json) {
                    if (json.Success) {
                        location.reload();
                    }
                }, "json");
            }
        });
    },
    feedlistRating: function () {
        $(".ibt_rating").raty({
            readOnly: true,
            score: function () { return $(this).attr("data-rating"); }
        });
    },
    showPop: function () {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }
        $.ajax({
            url: "/Supplier/PostVisit", data: { ibtType: $("#ibcommon_ibtType").val(), objId: $("#ibtcommon_ibtObjId").val() },
            async: false, dataType: "text", type: "Post", cache: false, timeout: 8000,
            success: function (txt) {
                var mybox = null;
                mybox = new Boxy(txt, {
                    title: "POst visit",
                    modal: true,
                    center: true,
                    unloadOnHide: true,
                    closeText: "",
                    afterHide: function (e) { },
                    afterShow: function (e) { }
                });
                $(".boxyInner").addClass("popup");
                checkboxshow();
                selectSpan();
                mybox.center();
                ibt.PvPopLoad();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统繁忙，请刷新重试！");
            }
        });
    },
    feedListShowPop: function (ibtType, objId) {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }
        $.ajax({
            url: "/Supplier/PostVisit", data: { ibtType:ibtType, objId:objId },
            async: false, dataType: "text", type: "Post", cache: false, timeout: 8000,
            success: function (txt) {
                var mybox = null;
                mybox = new Boxy(txt, {
                    title: "POst visit",
                    modal: true,
                    center: true,
                    unloadOnHide: true,
                    closeText: "",
                    afterHide: function (e) { },
                    afterShow: function (e) { }
                });
                $(".boxyInner").addClass("popup");
                checkboxshow();
                selectSpan();
                mybox.center();
                ibt.PvPopLoad();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统繁忙，请刷新重试！");
            }
        });
    },
    uploadify: function () {
        var IE=$.browser.msie;
        if (IE != undefined && IE) {//使用uploadify falsh控件上传
            $("#ibtcommon_fixbox").remove();
            $("#ibtcommon_fixbox_flash").show();
            $("#ibtcommon_uploadify_flash").uploadify({
                "formData": { "guid": $("#ibtcommon_guid").val(), "attType": this.attType_NewsFeed },
                "swf": "/js/uploadify/uploadify.swf",
                "uploader": "/IBT/AttachmentUpload",
                "buttonImage": "/images/add_w100.gif",
                "fileTypeDesc": "Image Files",
                "fileTypeExts": "*.jpg;*.jpeg;*.png;*.gif",
                "height": 100,
                "width": 100,
                onUploadSuccess: function (file, data, response) {
                    var json = eval('(' + data + ')');
                    if (json.Success) {
                        $("#ibtcommon_fixbox_flash").prepend("<span class=\"img\" style=\"height: 80px; width: 116px;\"><span id=\"data-attid" + json.Item + "\" onclick=\"ibt_common.clickImage(this);\" data-attid=\"" + json.Item + "\" class=\"checkboxBig mL5 mR5 uncheckbox\"></span><img src=\"" + json.Message + "\" alt=\"\"/></span>");
                    } else {
                        if (UserId == 0) {
                            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ 
                                singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                            return;
                        }
                    }
                }
            });
        }
        else {//使用jquery file upload控件上传
            $("#ibtcommon_fixbox_flash").remove();
            $("#ibtcommon_fixbox").show();
            $("#ibtcommon_uploadify").fileupload({
                url: "/IBT/AttachmentUpload",
                formData: { "guid": $("#ibtcommon_guid").val(), "attType": this.attType_NewsFeed },
                dataType: 'json',
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    if (!(/\.(gif|jpg|jpeg|png)$/i).test(uploadFile.name)) {
                        Boxy.alert('You must select an image(gif|jpg|jpeg|png) file only.');
                        goUpload = false;
                    }
                    if (uploadFile.size > 4000000) { // 4mb
                        Boxy.alert('Please upload a smaller image, max size is 4 MB.');
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        $("#progress").show();
                        data.submit();
                    }
                },
                done: function (e, data) {
                    var json = data.result;
                    if (json.Success) {
                        $("#ibtcommon_fixbox").prepend("<span class=\"img\" style=\"height: 80px; width: 116px;\"><span id=\"data-attid" + json.Item + "\" onclick=\"ibt_common.clickImage(this);\" data-attid=\"" + json.Item + "\" class=\"checkboxBig mL5 mR5 uncheckbox\"></span><img src=\"" + json.Message + "\" alt=\"\"/></span>");
                    } else {
                        if (UserId == 0) {
                            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
                            return;
                        } else { Boxy.alert("You must upload an image file only."); }
                    }
                    $("#progress").hide();
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .bar').css('width', progress + '%');
                }
            });
        }
    },
    clickImage: function (obj) {
        if ($(obj).hasClass("uncheckbox")) {
            $.post("/IBT/AttachmentChange", { attId: $(obj).attr("data-attid"), status: 0 }, function () { $(obj).removeClass("uncheckbox") }, "json");
        }
        else {
            $.post("/IBT/AttachmentChange", { attId: $(obj).attr("data-attid"), status: 1 }, function () { $(obj).addClass("uncheckbox") }, "json");
        }
    },
    saveNewsFeed: function () {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }
        if ($("#ibtcommon_val").val() == "Write a message...") {
            Boxy.alert("The text field is empty. Please write in your comment before posting!", null, { title: " " });
            //$(".boxyInner").addClass("popup");
            return;
        }
        var json_data = { "guid": $("#ibtcommon_guid").val(), "attType": this.attType_NewsFeed, "val": $("#ibtcommon_val").val(), "countryId": $("#ibtcommon_ibtObjId").val(), "ibtType": $("#ibcommon_ibtType").val() };
        $.ajax({
            url: "/IBT/NewsFeedSave",
            data: json_data,
            type: "Post",
            dataType: "json",
            success: function (json) {
                if (json.Success)
                { location.reload(); }
            }
        });
    },
    getFeedComment: function (id,pageIndex) {
        var box = $("#replyBox_" + id);
        if (box.hasClass("show")) {
            box.removeClass("show");
            box.hide();
            $("#ibt_common_paging_"+id).removeClass("show");
            $("#ibt_common_paging_" + id).hide();
        }
        else {
            $(".replyBox").removeClass("show");
            $(".replyBox").hide();
            $(".paging").removeClass("show");
            $(".paging").hide();
            box.addClass("show");
            box.show();
            $("#ibt_common_paging_" + id).addClass("show");
            $("#ibt_common_paging_" + id).show();
            this.getFeedCommentPage(id, pageIndex);
        }
    },
    getFeedCommentPage: function (id, pageIndex) {
        $.ajax({
            url: "/IBT/GetFeedCommentList",
            data: { newsFeedId: id, pageIndex: pageIndex },
            type: "Post",
            dataType: "json",
            success: function (data) {
                if (data.Success) {
                    //$("#ibtcommon_comments_" + id).html("Comments (" + data.Item + ")");
                    $("#ibtcommon_operating_" + id).html(data.Message);
                    //box.html(data.Message);
                    ibt_common.textFocus();
                }
            },
            errer: function () { alert("系统繁忙，请刷新重试！"); }
        });
    },
    textFocus: function () {
        $("textarea").focus(function () {
            var val = "Write a message...";
            if (val === $(this).val()) { $(this).val(""); }
        }).blur(function () {
            if ($(this).val() == "") { $(this).val("Write a message..."); }
        });
    },
    saveFeedComment: function (id) {
        if (UserId == 0) {
            ibt.loadUrlPop('/account/login', 'Get', '', function () { $('#pop_login').bValidator({ singleError: true }); $('.boxyInner').addClass('popup'); inputText(); }, 'Login With');
            return;
        }
        if ($("#commnetTxt_" + id).val() == "Write a message...") {
            Boxy.alert("The text field is empty. Please write in your comment before posting!", null, { title: " " });
            //$(".boxyInner").addClass("popup");
            return;
        }
        $.ajax({
            url: "/IBT/SaveFeedComment",
            data: { newsFeedId: id, val: $("#commnetTxt_" + id).val() },
            type: "Post",
            dataType: "json",
            success: function (data) {
                ibt_common.getFeedCommentPage(id, 1);
            }
        });
    },
    getFeedList: function () {
        $("#ibtcommon_loadMore").hide();
        $(".loading").show();
        $.ajax({
            url: "/IBT/GetFeedList",
            data: { ibtType: $("#ibcommon_ibtType").val(), ibtObjId: $("#ibtcommon_ibtObjId").val(), pageIndex: (this.pageIndex + 1), searchTime: $("#ibtcommon_searchTime").val() },
            type: "Post",
            dataType: "html",
            success: function (txt) {
                if (txt.replace(/^\s+,""/).replace(/^\s+$/, "") != '') {
                    $("#ibtcommon_fixlist").append(txt);
                    ibt_common.feedlistRating();
                    selectSpan();
                    if ($("#ibtcommon_fixlist").children("li").length % ibt_common.pageSize == 0) {
                        $("#ibtcommon_loadMore").show();
                    }
                    ibt_common.pageIndex++;
                }
                $(".loading").hide();
            },
            error: function () {
                $(".loading").hide();
                $("#ibtcommon_loadMore").show();
            }
        });
    },
    initData: function () {
        $(".loading").hide();
        ibt_common.rating();
        ibt_common.uploadify();
        ibt_common.getFeedComment();
        ibt_common.textFocus();
        if ($("#ibtcommon_fixlist")!="undefined")
        {
            if ($("#ibtcommon_fixlist").children("li").length == 0) {
                $('<li class="noData">There are no posts in this listing. Start sharing your trips by posting on the newsfeed!</li>').appendTo('#ibtcommon_fixlist');
                $("#ibtcommon_loadMore").hide();
            } else if ($("#ibtcommon_fixlist").children("li").length % ibt_common.pageSize != 0) { $("#ibtcommon_loadMore").hide(); }
        }
        $("#shareTextarea").focus(function () {
            $("#shareBox1").hide();
            $("#shareBox2").show();
        });
    }
}


var specialsOfCountry = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                specialsOfCountry.scrolling = true;
                specialsOfCountry.GetDataList(specialsOfCountry.page);
                specialsOfCountry.page++;
            }
        });
    },
    GetDataList: function (page) {
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#offerList');
        var json_data = { page: page, pageSize: specialsOfCountry.pageSize, ibtObjId: $("#ibtcommon_ibtObjId").val(), ibtType: $("#ibcommon_ibtType").val() };
        $.ajax({
            url: '/IBT/GetSpecialsList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    specialsOfCountry.scrolling = false;
                    specialsOfCountry.itemTotal = json.Item.itemTotal;
                    $('#offerList').append(json.Message);
                    $('.loading').remove();
                    if ($('#offerList').children('li').length == 0) {
                        $('<div class="noData">no data!</div>').appendTo('#offerList');
                    }
                }
            }
        });
    },
    initData: function () {
        $("#offerList").empty();
        specialsOfCountry.GetDataList(1);
        specialsOfCountry.page = 2;
        $('.noData').remove();
    }
}