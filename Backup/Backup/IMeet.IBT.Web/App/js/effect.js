/*Checkbox*/
var checkboxshow = function () {
    var readEmail = $(".readEmail");
    $(".checkbox,.checkboxSmall,.checkboxBig,.checkboxTxt,.checkboxMTxt,.checkboxMedium,.uncheckboxSmall,.checkboxBigTxt").each(function () {
        $(this).click(function () {
            if ($(this).parents().parents().parents().parents().hasClass("messageBox")) {
                if ($(this).hasClass("uncheckbox")) { $(this).removeClass("uncheckbox"); $(this).parents("li").removeClass("selected"); } else { $(this).addClass("uncheckbox").parents("li").addClass("selected") };
            } else {
                if ($(this).hasClass("uncheckbox")) {
                    $(this).removeClass("uncheckbox")
                } else {
                    $(this).addClass("uncheckbox")
                }
            }
        });
    })
    readEmail.click(function () { if ($(this).hasClass("unreadEmail")) { $(this).removeClass("unreadEmail") } else { $(this).addClass("unreadEmail") } });
    $(".inputBg,.select,.popupMain textarea,.input").addClass("IEbg");
}
/*Select*/
var selectSpan = function () {
    var list = $(".select").children(".list");
    var listH = $(".header .list");
    var arrow = $(".select").children(".arrow");
    var downArrow = $(".select").find(".downArrow");
    var view = $(".view");
    var my = $(".my");
    var li = $(".select .list ul li");

    arrow.click(function (event) { event.stopPropagation(); $(this).siblings(".list").show(); $(this).parent(".select ").css("z-index", "5"); $(this).parents("span.rightCol").css("z-index", "5"); var w = $(this).parent(".select ").outerWidth(); $(this).siblings(".list").width(w - 2); $(".jscroll-e ").show(); scroll_init(scroll_blue); return false; })
    downArrow.click(function () { $(this).parent(".title").siblings(".list").show(); $(this).parents(".select").css("z-index", "5").addClass("active"); checkboxshow(); return false; })
    $(".selecBtnList").find(".list").click(function () { })
    my.click(function () { $(this).parent("map").siblings(".select").addClass("active").show(); })
    list.hover(function () { $(this).show() }, function () { $(this).hide(); $(".select").removeClass("active") });
    list.find("li").hover(function () { $(this).addClass("hover") }, function () { $(this).removeClass("hover") })
    $(".view").find(".arrow").click(function () { $(this).parent(".select").addClass("active"); $(this).siblings(".list").show(); $(this).siblings(".list").css("width", "auto") })
    $(document).click(function () { $(".select").children(".list").hide(); $(".select").css("z-index", "0").removeClass("active"); $("span.rightCol").css("z-index", "0"); })
    li.click(function () {
        var txt = $(this).text();
        $(this).addClass("selected").siblings("li").removeClass("selected");
        $(this).parent("ul").parents(".list").siblings(".arrow").text(txt);
        $(this).parents("dd").addClass("NBorderB").siblings(".sponsorSupplie").hide();
    })
    var txtSponsor = $(".select .list .txtSponsor");
    txtSponsor.click(function () {
        $(this).parents("dd").removeClass("NBorderB").siblings(".sponsorSupplie").show();

    })
    //li.click(function(){
    //	var txt =$(this).text();
    //	$(this).addClass("selected").siblings("li").removeClass("selected");
    //	$(this).parent("ul").parent(".list").siblings(".arrow").text(txt);
    //})
}
/*Scroll*/
function scroll_init(conf) {
    conf = conf || scroll_blue;
    $(".scrollBox").jscroll(conf);
}
/*Input Focus*/
var inputText = function () {

    $(".login input:text").each(function () {
        var val = $(this).val();
        $(this).focus(function () {
            if (val === $(this).val()) { $(this).val("") }
        }).blur(function () {
            if ($(this).val() == "") { $(this).val(val) }
        });
    })
    $(".inputPassword").blur(function () {
        var val = $(this).val();
        if (val === "") {
            $(this).siblings(".value").show();
        }
    })
    $(".login .value").click(function () {
        $(this).hide();
        $(this).siblings("input:password").focus();
    })
    $(".inputBg,.select,.popupMain textarea").addClass("IEbg");

}
/*Window Reload*/
var windowReload = function () {
    $(window).resize(function () {
        window.location.reload()
    });
}
/*Table */
var tableBox = function () {
    $(".table").find("tr").not("tr:first").hover(function () {
        $(this).addClass("hove");
    }, function () {
        $(this).removeClass("hove");
    })
}
/* Table Control*/
var control = function () {
    var a = $(".controlBg .bg a ");
    a.click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active")
        } else {
            $(this).addClass("active")
        }
    })
}
/*search */
var searchLi = function () {
    var input = $(".searchLi").find("input");
    input.focus(function () {
        $(this).parent("li").removeClass("NBorderB");
        $(this).parents("ul").siblings().slideDown();
        $(this).parents(".W600").find(".resultsBox").children(".scrollBox").height(200) + "px";
        scroll_init(scroll_blue);
        $(".W600 .botBox").children("a").hide().siblings().show();
    })
    $(".filterBox .btnBlueH45,.resultsBox .scrollBox,.W600 .botBox ").click(function () {
        input.parent("li").addClass("NBorderB");
        input.parents("ul").siblings().slideUp();
        $(".W600").find(".resultsBox").children(".scrollBox").height(406) + "px";
        scroll_init(scroll_blue);
        $(".W600 .botBox").children(".txt").hide().siblings().show();
    })

}
