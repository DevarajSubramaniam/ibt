﻿var j_email_list = {
    id:0,
    bseId:0,
    posting: false,
    page: 1,
    pagesize: 10,
    orderby: 'name',
    instance: null,
    get: function () {
        if (!j_email_list.posting) {
            $.ajax({
                url: '/admin/home/getemaillist',
                data: { id: j_email_list.id, page: j_email_list.page, pagesize: j_email_list.pagesize, orderby: j_email_list.orderby },
                type: 'post',
                dataType: 'json',
                success: function (json) {
                    var emailListTable = $('#emailListTable');
                    emailListTable.find('.emailListRow').remove();
                    emailListTable.append(json.Item.List);
                    $('#emailListTitle').html("<p>Total Number of Sign-ups: " + json.Message+"</p>");
                    $('#emailListPages').html(json.Item.Page);
                    j_email_list.posting = false;
                },
                error: function () { j_email_list.posting = false; }
            });
        }
    },
    goPage: function (index, orderby) {
        j_email_list.page = index;
        j_email_list.orderby = orderby;
        j_email_list.get();
    },
    orderbyPage: function (orderby) {
        j_email_list.goPage(1, orderby);
    },
    searchPage: function (index) {
        j_email_list.goPage(index, j_email_list.orderby);
    },
    sendForTest: function () {
        if (!j_email_list.check() || $('#testEmail').val() == '') {
            Boxy.alert("Please input From mail and title and content and test emial.", null, { title: "SAVE" });
            $(".boxyInner").addClass("popup");
            return false;
        }

        $.ajax({
            url: '/admin/home/badgespecialemailsendfortest',
            data: { fromEmail: $('#fromMail').val(), title: $('#emailTitle').val(), msg: j_email_list.instance.getData(), testEmail: $('#testEmail').val() },
            type: 'post',
            dataType: 'json',
            success: function (json) {
                Boxy.alert("SUCCESS", null, { title: "Send For Test" });
                $(".boxyInner").addClass("popup");
            },
            error: function () { }
        });
    },
    send: function () {
        if (!j_email_list.check()) {
            Boxy.alert("Please input From mail and title and content.", null, { title: "SAVE" });
            $(".boxyInner").addClass("popup");
            return false;
        }

        $.ajax({
            url: '/admin/home/badgespecialemailsend',
            data: { id: j_email_list.id , bseid: j_email_list.bseId, fromEmail: $('#fromMail').val(), title: $('#emailTitle').val(), msg: j_email_list.instance.getData()},
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (json.Success) {
                    Boxy.alert("SUCCESS", null, { title: "Send Email" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Send Email" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    save: function () {
        if (!j_email_list.check()) {
            Boxy.alert("Please input From mail and title and content.", null, { title: "SAVE" });
            $(".boxyInner").addClass("popup");
            return false;
        }

        $.ajax({
            url: '/admin/home/badgespecialemailsave',
            data: { id: j_email_list.id, bseid: j_email_list.bseId, fromEmail: $('#fromMail').val(), title: $('#emailTitle').val(), msg: j_email_list.instance.getData() },
            type: 'post',
            dataType: 'json',
            success: function (json) {
                j_email_list.bseId = parseInt(json.Message);
                Boxy.alert("SUCCESS", null, { title: "SAVE" });
                $(".boxyInner").addClass("popup");
            },
            error: function () { }
        });
    },
    check: function () {
        if ($('#fromMail').val() == "" || $('#emailTitle').val() == "" || j_email_list.instance.getData() == "") {
            return false;
        }
        return true;
    }
};