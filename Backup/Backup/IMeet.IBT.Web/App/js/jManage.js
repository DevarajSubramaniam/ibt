﻿var memberManager = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (memberManager.scrolling == true) {
                    memberManager.GetDataList(memberManager.page);
                    memberManager.page++;
                }          
            }
        });
        if (memberManager.itemTotal < 10) {
            $("#clickloadmore").hide();
        }
    },
    GetMemberListOnClick: function () {
        memberManager.GetDataList(memberManager.page);
        memberManager.page++;
    },
    GetDataList: function (page) {
        //if ($('#tabMember').children('li').length >= 100) {
        //    return false;
        //}
        memberManager.scrolling = false;
        if (this.itemTotal == 0) { $("#clickloadmore").hide(); memberManager.scrolling = false; return };
        $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#tabMember');
        var json_data = { page: page, pageSize: memberManager.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val(), SearchKeyword: $("#SearchKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Admin/Home/MemberListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    memberManager.scrolling = false;
                    memberManager.itemTotal = json.Item.itemTotal;
                    $("#memberManager_count").text(json.Item.total);
                    $('#tabMember').append(json.Message);
                    $("#count").html(json.Item.itemTotal);
                    memberManager.txtSort();
                    $('.alignC').remove();
                    checkboxshow();
                    memberManager.scrolling = true;
                    //if ($('#tabMember').children('tr').length == 0) {                     
                    //    $('<tr class="noData" style="text-align:center;"><td>no data!</td></tr>').appendTo('#tabMember');
                    //}
                }
            }
        });
    },
    txtSort: function () {
        $(".cssTaskSn").click(function () {//给cssTaskSn加上click
            var objTd = $(this);//保存对象
            var oldText = $.trim(objTd.text());//保存原来的文本
            var input = $("<input id='txtorderby'  type='text' value='" + oldText + "' style='width:30px;text-align:center;' />");//定义input变量为一个文本框把原来文本写入
            objTd.html(input)//当前td变为文本框，把原来文本写入
            //设置文本框点击事件失效
            input.click(function () {
                return false;
            });
            //设置select全选文本事件,先触发焦点
            input.trigger("focus").trigger("select");

            //文本框失去焦点变回文本
            input.blur(
            function () {
                var newText = $(this).val();
                objTd.html(newText);
                memberManager.SetMemberSort($("#hidUserId").val(), newText);  //设置排序数字
                var taskSnId = $.trim((objTd.prev()).prev().text());
                //alert(taskSnId);//这里显示的是空的，不知道怎么获取这个id,这个idq我是绑在一个隐藏的文本框上的见html代码和aspx代码。
            });
        });
    },
    initData: function () {
        $("#tabMember").empty();
        memberManager.GetDataList(1);
        memberManager.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#tabMember").empty();//先删除以前的
        memberManager.scrolling = false;
        memberManager.itemTotal = -1;
        $('.noData').remove();
        memberManager.GetDataList(1);
    },
    sortbyName: function (obj) {
        $("#tabMember").empty();//FirstName Sort
        memberManager.scrolling = false;
        memberManager.itemTotal = -1;
        $('.noData').remove();
        memberManager.GetDataList(1);
    },
    sortbyNum: function (obj) {
        $("#tabMember").empty();//Num Sort
        memberManager.scrolling = false;
        memberManager.itemTotal = -1;
        $('.noData').remove();
        memberManager.GetDataList(1);
    },
    SetMemberSort: function (objId, newText) {
        var json_data = { objId: objId, newText: newText };
        $.post("/Admin/Home/SetMemberSort", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Member Search Score" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Member Search Score" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    SetIsApproved: function (objId) {
        var json_data = { objId: objId };
        $.post("/Admin/Home/SetIsApproved", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Member Activation" });
                $(".boxyInner").addClass("popup");
            } else {
                //$("#tipMsg").html(json.Message);
                Boxy.alert(json.Message, null, { title: "Member Activation" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    SetIsFeatured: function (objId) {
        var json_data = { objId: objId };
        $.post("/Admin/Home/SetIsFeatured", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Member Featured" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Member Featured" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    Del: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Member Remove" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Member Remove" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false; }
    }
};
