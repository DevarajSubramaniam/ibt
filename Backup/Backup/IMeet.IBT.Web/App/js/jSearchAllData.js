var SearchData = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function (tabId) {
        if (this.scrolling == false) {
            SearchData.scrolling = true;
            return;
        }
        if (tabId == "1") {
            SearchData.GetCountryDataList(SearchData.page);
        }
        else {
            SearchData.GetCityDataList(SearchData.page);
        }
        SearchData.page++;
    },
    //获取国家list
    GetCountryDataList: function (page) {
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#ulData');
        var json_data = { page: page, pageSize: SearchData.pageSize, keyword: $("#txtValue").val() };
        $.ajax({
            url: '/Admin/Home/SearchCountryList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    SearchData.scrolling = false;
                    SearchData.itemTotal = json.Item.itemTotal;
                    $('#ulData').append(json.Message);
                    $('.loading').remove();
                    $(".jscroll-e").show();
                    scroll_init(scroll_blueSearch2);//初始化scroll.js(old)
                }
            }
        });
    },
    GetCountryDataListByClick: function () {
        SearchData.GetCountryDataList(SearchData.page);
        SearchData.page++;
    },
    //获取城市list
    GetCityDataList: function (page) {
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#ulData');
        var json_data = { page: page, pageSize: SearchData.pageSize, keyword: $("#txtValue").val(), countryId: $("#hidfldCountry").val(), cityId: $("#hidfldCity").val() };
        $.ajax({
            url: '/Admin/Home/SearchCityList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    SearchData.scrolling = false;
                    SearchData.itemTotal = json.Item.itemTotal;
                    $('#ulData').append(json.Message);
                    $('.loading').remove();
                    $(".jscroll-e").show();
                    scroll_init(scroll_blueSearch3);//初始化scroll.js(old)
                }
            }
        });
    },
    GetCityDataListByClick: function () {
        SearchData.GetCityDataList(SearchData.page);
        SearchData.page++;
    },
    initData: function (tabId) {
        $("#ulData").empty();
        if (tabId == "1") {
            SearchData.GetCountryDataList(1);
        }
        else {
            SearchData.GetCityDataList(1);
            $("#ddlCountry").change(function () {
                GetCity();
                var value = $("#ddlCountry").val();
                $("#hidfldCountry").attr("value", value);  //隐藏控件赋值
            });
            $("#ddlCity").change(function () {
                var value = $("#ddlCity").val();
                $("#hidfldCity").attr("value", value);
            });
        }
        SearchData.page = 2;
        $('.noData').remove();
    },
    goTab: function (tabId) {
        $("#ulData").empty();//先删除以前的
        SearchData.scrolling = false;
        SearchData.itemTotal = -1;
        $('.noData').remove();
        if (tabId == "1") {
            SearchData.GetCountryDataList(1);
        }
        else {
            SearchData.GetCityDataList(1);
        }
    }
};