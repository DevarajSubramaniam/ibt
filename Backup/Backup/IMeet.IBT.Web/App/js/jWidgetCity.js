﻿var WidgetCityManage = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (WidgetCityManage.scrolling == true) {
                    WidgetCityManage.GetCityList(WidgetCityManage.page);
                    WidgetCityManage.page++;
                }
            }
        });
    },
    GetCityList: function (page) {
        WidgetCityManage.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#talCity');
        var json_data = { page: page, pageSize: WidgetCityManage.pageSize };
        $.ajax({
            url: '/Admin/Home/WidgetCityListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    WidgetCityManage.scrolling = false;
                    WidgetCityManage.itemTotal = json.Item.itemTotal;
                    $("#talCity").prepend(json.Message);
                    $('.loading').remove();
                    WidgetCityManage.scrolling = true;
                }
            }
        });
    },
    initData: function () {
        $("#talCity").empty();
        WidgetCityManage.GetCityList(1);
        WidgetCityManage.page = 2;
        //$('#talCity').append("<tr><td class=\"alignR\" colspan=\"9\"><div class=\"controlBg control1\"><span class=\"bg\"><a href=\"javascript:void(0);\" class=\"add NborderR\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddWidgetCity','Get','',function(){$('.jscroll-e').show();scroll_init(scroll_blueSearch3);SearchData.initData(2);SearchData.init(2);},'Search');\" ></a> </span></div></td><tr>");
        $('.noData').remove();
    },
    AddWidgetCity: function (objId, scroe) {
        if (confirm('Do you really want to add this city data?')) {
            var json_data = { objId: objId, scroe: scroe };
            $.post("/Admin/Home/AddWidgetCityAjax", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add WidgetIBT" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add WidgetIBT" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }


    },
    saveSort: function () {
        var json_data = { arrayData: $("#hidArray").val() };
        $.ajax({
            url: '/Admin/Home/PaiXuCity', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    DelWidgetCity: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/WidgetCityManegeDelete", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del City" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del City" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }
    }
};

