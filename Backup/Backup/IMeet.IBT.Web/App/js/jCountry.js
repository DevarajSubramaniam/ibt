﻿var CountryManage = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (CountryManage.scrolling == true) {
                    CountryManage.GetCountryList(CountryManage.page);
                    CountryManage.page++;
                }
                //CountryManage.scrolling = true;
            }
        });
        if (CountryManage.itemTotal < 10) {
            $("#clickloadmore").hide();
        }
    },
    GetCountryListOnClick: function () {
        if (CountryManage.scrolling == true) {
            CountryManage.GetCountryList(CountryManage.page);
            CountryManage.page++;
        }
    },
    GetCountryList: function (page) {
        CountryManage.scrolling = false;
        if (this.itemTotal == 0) { $("#clickloadmore").hide(); CountryManage.scrolling = false; return };
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#talCountry');
        var json_data = { page: page, pageSize: CountryManage.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val(), SearchKeyword: $("#SearchKeyword").val() };
        $.ajax({
            url: '/Admin/Home/CountryListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {                    
                    CountryManage.itemTotal = json.Item.itemTotal;
                    $('#talCountry').append(json.Message);
                    CountryManage.txtSort();
                    $('.loading').remove();
                    CountryManage.scrolling = true;
                }
            }
        });
    },
    initCountryData: function () {
        $("#talCountry").empty();
        CountryManage.GetCountryList(1);
        CountryManage.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#talCountry").empty();//先删除以前的
        CountryManage.scrolling = false;
        CountryManage.itemTotal = -1;
        $('.noData').remove();
        CountryManage.page = 1;
        CountryManage.GetCountryList(CountryManage.page);
        CountryManage.page++;
        //CountryManage.GetCountryList(1);
        $("#clickloadmore").show();
    },
    txtSort: function () {
        $(".cssTaskSn").click(function () {//给cssTaskSn加上click
            var objTd = $(this);//保存对象
            var oldText = $.trim(objTd.text());//保存原来的文本
            var input = $("<input id='txtorderby'  type='text' value='" + oldText + "' style='width:30px;text-align:center;' />");//定义input变量为一个文本框把原来文本写入
            objTd.html(input)//当前td变为文本框，把原来文本写入
            //设置文本框点击事件失效
            input.click(function () {
                return false;
            });
            //设置select全选文本事件,先触发焦点
            input.trigger("focus").trigger("select");

            //文本框失去焦点变回文本
            input.blur(
            function () {
                var newText = $(this).val();
                objTd.html(newText);
                CountryManage.SetCountrySort($("#hidcountryId").val(), newText);  //设置排序数字
                var taskSnId = $.trim((objTd.prev()).prev().text());
                //alert(taskSnId);//这里显示的是空的，不知道怎么获取这个id,这个idq我是绑在一个隐藏的文本框上的见html代码和aspx代码。
            });
        });
    },
    //按字段排序
    sortbyTitle: function (obj) {
        $("#talCountry").empty();
        CountryManage.scrolling = false;
        CountryManage.itemTotal = -1;
        $('.noData').remove();
        CountryManage.GetCountryList(1);
    },
    sortbyNum: function (obj) {
        $("#talCountry").empty();
        CountryManage.scrolling = false;
        CountryManage.itemTotal = -1;
        $('.noData').remove();
        CountryManage.GetCountryList(1);
    },
    SetCountrySort: function (objId, newText) {
        var json_data = { objId: objId, newText: newText };
        $.post("/Admin/Home/SetCountrySort", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Modify Score" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Modify Score" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    DelCountry: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetCountryIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del Country" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del Country" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }


    },
    editInfo: function (objId) {
        var json_data = { objId: objId };
        $.ajax({
            url: '/Admin/Home/GetCountryName', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $('#CountryName').attr("value", json.Message);
                }
            }
        });
    },
    save: function (type) {
        if (type == "") {
            var json_data = { countryName: $("#CountryName").val(), flag: $("#imgSave").val(), score: $("#score").val(), UserId: $("#UserId").val() };
            $.post("/Admin/Home/AddCountry", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add New Country" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add New Country" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }
        else {
            var json_data = { countryId: $("#countryId").val(), countryName: $("#CountryName").val(), score: $("#score").val(), flag: $("#imgSave").val() };
            $.post("/Admin/Home/EditCountry", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Edit Country" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Edit Country" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }

    },
    uploadFlag: function () {
        $('#uploadFlag').uploadify({
            'buttonImage': '/App/images/button/btn_choose_file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 180,
            'height': 16,
            'auto': false,
            'uploader': '/Admin/Home/UploadFlag?userId=' + $("#UserId").val() + '&countryId=' + $("#countryId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                $("#imgPreview").removeAttr("src");
                $("#imgPreview").attr("src", "/Staticfile/flag/" + data.SaveName);
                $("#imgSave").attr("value", "/Staticfile/flag/" + data.SaveName);
            }
        });
    }
};

