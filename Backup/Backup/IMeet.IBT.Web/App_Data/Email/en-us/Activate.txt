﻿@Subject={SiteName}: Account Activate.
<html>
<body>
  <div style="font-family: Verdana, Arial, Serif;"><br />
    <div style="font-size: 12px;  font-weight:bold;">
        <div style="color:red;">New Member Welcome Email</div><br>
          Thanks for joining I’ve Been There!  We’re thrilled to have you join our global community, <br>
		  and we hope you’ll visit us often!  There a lot to do here – and we encourage you to use IBT <br>
		  to consistently records your travel experiences.  It’s fun to do, and fun to share!<br>

        </div><br />
   <div style="font-size: 12px; font-weight: bold; margin-left:30px;">	
        RECORD YOUR TRAVELS<br>
        Countries.Cities.Hotels.Venues.  Post Photos.  Comments.<br>
        See your travel history – all in one place!<br>
		<br>
		SHARE YOUR EXPERIENCES<br>
        Create Your Travel Map.  Share Your Recommendations.  See Your Friends’ Travels.<br>
        See Who’s Been Where!<br>
		<br>
		WISHES!  PLOT YOUR FUTURE TRAVEL MAP<br>
        Travel Ideas.  Special Offers.  Where You Want To Go or Go Back.<br>
        Explore The World Virtually and Post Where You Want To Go!<br>
</div>
    <div style="font-size: 12px;font-weight: bold;"><br />
          Let us know your comments, or if there is anything we can do to help.The world brings us <br>
		  all together, and I’ve Been There helps you record and share experiences that everyone <br>
		  wants to know about!
          <br /><br />
             Happy Travels!<br /><br />
             The I’ve Been There Team
    </div>
  </div>
</body>
</html>