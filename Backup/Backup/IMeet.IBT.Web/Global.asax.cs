﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using StructureMap;
using StructureMap.Configuration.DSL;
using IMeet.IBT.Controllers;

namespace IMeet.IBT.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            RouteTable.Routes.RouteExistingFiles = false;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            RouteTable.Routes.IgnoreRoute("{resource}.css/{*pathInfo}");
            RouteTable.Routes.IgnoreRoute("{folder}/{*pathInfo}", new { folder = "images" });
            routes.IgnoreRoute("js/{*pathInfo}");

            //home
            routes.MapRoute("Home", "home", new { controller = "Home", action = "Home" }, new string[] { "IMeet.IBT.Controllers" });

            //static
            routes.MapRoute("Terms", "TermsOfUse", new { controller = "Static", action = "Terms" }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("Privacy", "PrivacyPolicy", new { controller = "Static", action = "Privacy" }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("Contact", "contact_us", new { controller = "Static", action = "Contact" }, new string[] { "IMeet.IBT.Controllers" });

            //PostVisit
            routes.MapRoute("PostVisit", "PostVisit/{ibtType}/{objId}", new { controller = "Supplier", action = "PostVisit", ibtType = 3, objId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("PostVisitCountry", "PostVisitN/Country/{objId}", new { controller = "Supplier", action = "PostVisitN", ibtType = 1, objId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("PostVisitCity", "PostVisitN/City/{objId}", new { controller = "Supplier", action = "PostVisitN", ibtType = 2, objId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("PostVisitSupply", "PostVisitN/Supply/{objId}", new { controller = "Supplier", action = "PostVisitN", ibtType = 3, objId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //ShowFriend
            routes.MapRoute("ShowFriend", "ShowFriend/{ibtType}/{objId}", new { controller = "Supplier", action = "ShowFriend", ibtType = 3, objId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //supply detail
            routes.MapRoute("supply-detail", "Supplier/v/{supplyId}", new { controller = "Supplier", action = "Detail", SupplyId = "-" }, new string[] { "IMeet.IBT.Controllers" });
            //Profile
            routes.MapRoute("Profile", "Profile/{userId}", new { controller = "Supplier", action = "Profile", userId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //My Travel 
            routes.MapRoute("supply-MyTravel", "MyTravel/{userId}/{categoryId}", new { controller = "Supplier", action = "MyTravel", userId = 0, categoryId = 3 }, new string[] { "IMeet.IBT.Controllers" });
            //My Countries 
            routes.MapRoute("supply-MyCountries", "Supplier/Countries/{userId}", new { controller = "Supplier", action = "MyCountries", userId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //My Cities 
            routes.MapRoute("supply-MyCities", "Supplier/Cities/{userId}", new { controller = "Supplier", action = "MyCities", userId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //My List
            routes.MapRoute("supply-MyList", "MyList", new { controller = "Supplier", action = "MyList" }, new string[] { "IMeet.IBT.Controllers" });
            //Services 
            routes.MapRoute("supply-Services", "Services/{userId}/{categoryId}", new { controller = "Supplier", action = "Services", userId = 0, categoryId = 43 }, new string[] { "IMeet.IBT.Controllers" });
            //FriendRequest
            routes.MapRoute("FriendRequest", "Account/FriendRequest/{userId}", new { controller = "Account", action = "FriendRequest", userId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //Message
            routes.MapRoute("Message", "Connection/Message/{userId}", new { controller = "Connection", action = "Message", userId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //InviteFriend
            routes.MapRoute("InviteFriend", "Connection/InviteFriend/{userId}", new { controller = "Connection", action = "InviteFriend", userId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //Search
            routes.MapRoute("Search", "Search/Index/{keyword}", new { controller = "Search", action = "Index", keyword = "" }, new string[] { "IMeet.IBT.Controllers" });
            //SpecialAds
            routes.MapRoute("SpecialAds", "Supplier/s/{supplyId}", new { controller = "Supplier", action = "SpecialAds", supplyId = "-" }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("SpecialsListing", "SpecialsListing", new { controller = "Supplier", action = "SpecialsListing" }, new string[] { "IMeet.IBT.Controllers" });

            //IBT
            routes.MapRoute("IBT-NewsFeed", "IBT/NewsFeed/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "NewsFeed", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("IBT-PhotoAlbum", "IBT/PhotoAlbum/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "PhotoAlbum", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("IBT-About", "IBT/About/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "About", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("IBT-Specials", "IBT/Specials/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "Specials", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });

            routes.MapRoute("Country-NewsFeed", "Country/NewsFeed/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "NewsFeed", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("Country-PhotoAlbum", "Country/PhotoAlbum/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "PhotoAlbum", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("Country-About", "Country/About/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "About", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("Country-Specials", "Country/Specials/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "Specials", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });

            routes.MapRoute("City-NewsFeed", "City/NewsFeed/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "NewsFeed", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("City-PhotoAlbum", "City/PhotoAlbum/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "PhotoAlbum", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("City-About", "City/About/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "About", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            routes.MapRoute("City-Specials", "City/Specials/{ibtType}/{ibtObjId}", new { controller = "IBT", action = "Specials", ibtType = 0, ibtObjId = 0 }, new string[] { "IMeet.IBT.Controllers" });
            //MeetingProfessionalBadge overview
            routes.MapRoute("BadgeOverView", "MeetingProfessionalBadge/Overview", new { controller = "TravelBadge", action = "OverView" }, new string[] { "IMeet.IBT.Controllers" });

            //Verification
            routes.MapRoute("Verifications", "Verification/{token}", new { controller = "Account", action = "Verification", token = "" }, new string[] { "IMeet.IBT.Controllers" });

            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional }, new string[] { "IMeet.IBT.Controllers" });

        }

        /// <summary>
        /// Authenticate the request.
        /// Using Custom Forms Authentication since I'm not using the "RolesProvider".
        /// Roles are manually stored / encrypted in a cookie in <see cref="FormsAuthenticationService.SignIn"/>
        /// This takes out the roles from the cookie and rebuilds the Principal w/ the decrypted roles.
        /// http://msdn.microsoft.com/en-us/library/aa302397.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            //SecurityHelper.RebuildUserFromCookies();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            StructureMapBootrstrapper.Configure();
            ControllerBuilder.Current.SetControllerFactory(new IoCControllerFactory());            

        }
    }
}