﻿var createUser = {
    create: function () {
        if ($('#fillForm').data('bValidator').validate() == false) {
            return;
        }

        var json_data = $("#fillForm").MySerialize();
        $.post("/Account/BCreateUser", json_data, function (json) {
            if (json.Success) {
                window.location = "/home";
            } else {
                Boxy.alert(json.Message, null, { title: "Create User" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
       
    }
};

