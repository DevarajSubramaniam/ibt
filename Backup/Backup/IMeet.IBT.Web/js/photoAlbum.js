﻿var PhotoManage = {
    page: 1,
    scrolling: false,
    pageSize: 12,
    itemTotal: -1,
    total: -1,
    rotateType:0,
    GetPhotoListOnClick: function () {
        PhotoManage.page++;
        PhotoManage.GetPhotoList(PhotoManage.page);
    },
    GetPhotoList: function (page) {
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#photolist');
        var json_data = {
            page: page, pageSize: PhotoManage.pageSize, type: $("#att_type").val(), objid: $("#att_objid").val(), objtype: $("#obj_type").val()
        };
        $.ajax({
            url: '/IBT/AttachementList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $('#photolist').append(json.Message);
                    try {
                        //重新加载上传控件
                        supplyUploadify();
                    } catch (ex) { }
                    //重新加载相册的隐藏层
                    photosList();
                    if (json.Item >= PhotoManage.pageSize) {
                        $("#clickloadmore_photo").show();
                    } else {
                        $("#clickloadmore_photo").hide();
                    }
                    if (PhotoManage.page == 1 && json.Item == 0) {
                        $('<p class="noData">There are currently no photos on this listing.</p>').appendTo('#photolist');
                    } else {
                        $('.noData').remove();
                    }
                    $('.loading').remove();
                }
            }
        });
    },
    init: function () {
        $("#photolist").empty();
        PhotoManage.page = 1;
        PhotoManage.GetPhotoList(PhotoManage.page);
    },
    GetPhotoDetail: function (attid, objid) {
        this.rotateType = 0;
        var json_data = { attid: attid };
        $.ajax({
            url: '/supplier/AttachmentPopup', async: false, dataType: 'json', data: json_data, type: 'post', cache: false, timeout: 8000, success: function (json) {
                if (json.Success) {
                    $("#_photoalbumDetail").html(json.Message);
                } else {
                    Boxy.alert(json.Message, function () { }, "Message");
                }
            }
        });
        $(".overLayout").show();
        feedComments.ShowMessageBoxPop(objid);
        popup2();
        //setTimeout(function () { scroll_init(scroll_blue); }, 300);
        //var isshowscroll = $("#feedcommentsCountPop_" + objid).attr("data-comments");
        //if (isshowscroll > 3) {
        //    $(".jscroll-e").show();
        //} else {
        //    $(".jscroll-e").hide();
        //}

        $("textarea").focus(function () {
            var val = "Write a message...";
            if ($(this).val() == val) { $(this).val("") }
        }).blur(function () {
            if ($(this).val() == "") { $(this).val("Write a message...") }
        });
    },
    DeletePhotoPopup: function () {
        $(".overLayout").hide();
        $("#_photoalbumDetail").empty();
    },
    RightRotate: function () {
        if (this.rotateType < 3) {
            this.rotateType++;
        } else {
            this.rotateType = 0;
        }
        $("#_rotate").rotate(this.rotateType * 90);
    },
    LeftRotate: function () {
        if (this.rotateType == 0) {
            this.rotateType = 3;
        } else {
            this.rotateType--;
        }
        $("#_rotate").rotate(this.rotateType * 90);
    },
    UpdateRotate: function (obj) {
        _this = this;
        if (this.rotateType == 0) { Boxy.alert("You have successfully saved the orientation of the image!"); } else {
            $.ajax({
                url: "/Supplier/AttachmentRotateUpdate", data: { att: $(obj).attr("data-att"), rotate: this.rotateType }, type: "post", dataType: "json", success: function (json) {
                    if (json.Success) {
                        Boxy.alert("You have successfully saved the orientation of the image!", function () {
                            _this.rotateType = 0;
                        });
                    } else { Boxy.alert(json.Message); }
                }
            });
        }
    }
};
