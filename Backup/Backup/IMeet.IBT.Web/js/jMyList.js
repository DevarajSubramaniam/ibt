﻿var myList = {
    page:1,
    scrolling:false,
    pageSize:10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (myList.scrolling == true) {
                    myList.GetDataList(myList.page);
                    myList.page++;
                }
            }
        });
    },
    GetDataList:function (page) {
        myList.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#feedList');
        var json_data = { listType: $("#listType").val(), page: page, pageSize: myList.pageSize, filterLetter: $("#filterLetter").val(), filterKeyword: $("#filterKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Supplier/MyListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    myList.scrolling = false;
                    myList.itemTotal = json.Item.itemTotal;
                    $("#mylist_count").text(json.Item.total);
                    $('#feedList').append(json.Message);
                    $('.loading').remove();
                    ibt.ibtRating();
                    checkboxshow();
                    //console.info($('#feedList').children('li').length);
                    myList.scrolling = true;
                }
            }
        });
      
    },
    initData: function () {
        $("#myList_title").html("List of Recommended <span class=\"txt\">You've Recommended about <span id=\"mylist_count\">-</span> places</span>");
        $("#feedList").empty();
        myList.GetDataList(1);
        myList.page = 2;
        $("#filterLetter").val("");
        $('.noData').detach();
    },
    goTab: function (obj, id) {
        $("#feedList").empty();//先删除以前的
        myList.scrolling = false;
        myList.itemTotal = -1;
        $("#quickSearch a").each(function () {
            $(this).removeClass("active");
        });
        $("#filterLetter").val("");
        $('.noData').detach();
        $(".listNav a").each(function() {
            $(this).removeClass("btnActive");
            });
        $(obj).addClass("btnActive");

        switch (id) {
            case 3:
                $("#listType").val("WanttoGo");
                $("#myList_title").html("LIST OF WANT TO GO / GO BACK <span class=\"txt\">You've about <span id=\"mylist_count\">-</span> places in your Want to Go / Go Back</span>");

                break;
            case 4:
                $("#listType").val("BucketList");
                $("#myList_title").html("LIST OF BUCKET LIST <span class=\"txt\">You've about <span id=\"mylist_count\">-</span> places in your Bucket List</span>");
                break;
            default: //default 2
                $("#listType").val("Recommended");
                $("#myList_title").html("List of Recommended <span class=\"txt\">You've Recommended about <span id=\"mylist_count\">-</span> places</span>");
                break;
        }
        myList.GetDataList(1);
        if ($('#feedList').children('li').length == 0) {
            $('<div class="noData">Currently, you have not added any places to this list! Add them to your list by clicking on Fast Feedback!</div>').appendTo('#feedList');
        }
    },
    quickSearch: function (obj, letter) {
        $("#feedList").empty();
        $('.noData').detach();
        myList.scrolling = false;
        myList.itemTotal = -1;
        myList.page = 2;
        if ($("#filterLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#filterLetter").val("");

            myList.GetDataList(1);
        } else {
            $("#quickSearch a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");

            $("#filterLetter").val(letter);
            myList.GetDataList(1);
        }
    }
}