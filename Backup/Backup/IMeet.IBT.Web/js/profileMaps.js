﻿var profileMaps = {
    prevZoom: 2,
    map: null,
    source: null,// ['Manly Beach', -33.80010128657071, 151.28747820854187,1,1683, '/images/icon/restaurant2.png'], title,lan,lon,ibttype,id,icon
    userZoom: true,
    markersArray: [],
    UserId:0,
    infoBoxOptions: {
        content: ""
        , disableAutoPan: false
        , maxWidth: 0
        , pixelOffset: new google.maps.Size(-140, 0)
        , zIndex: null
        , boxStyle: {
            background: "url('/images/map_tips_t.png') no-repeat"
            , opacity: 0.85
            , width: "308px"
            , padding: "11px 0 0"
        }
        , closeBoxMargin: "0 8px -13px"
        , closeBoxURL: "/images/close.gif"
        , infoBoxClearance: new google.maps.Size(20, 20)
        , isHidden: false
        , pane: "floatPane"
        , enableEventPropagation: false
    },
    init: function (locations) {
        var initLatlng;
        if (locations[0] != undefined)
            initLatlng = new google.maps.LatLng(locations[0].lat, locations[0].lng);
        else
            initLatlng = new google.maps.LatLng(1, 1);
        var mapOptions = {
            zoom: profileMaps.prevZoom,
            minZoom: 2,
            maxZoom: 19,
            center: initLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        profileMaps.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);


        profileMaps.setMarkers(profileMaps.map, locations);

        //zoom
        google.maps.event.addListener(profileMaps.map, 'zoom_changed', function () {
            if (profileMaps.userZoom) {
                profileMaps.userZoom = false;
                
                //console.info(profileMaps.prevZoom + "," + profileMaps.map.getZoom());
                if (profileMaps.map.getZoom()>=8) {
                    //profileMaps.prevZoom = profileMaps.map.getZoom();
                    if (profileMaps.prevZoom < 8) {
                        var _lat = profileMaps.map.getCenter().lat();
                        var _lng = profileMaps.map.getCenter().lng();
                        setTimeout(function () {
                            var json_data = { typ: 3, userId: profileMaps.UserId, lat: _lat, lng: _lng };
                            $.ajax({
                                url: "/map/GetProfileMapsList", data: json_data, success: function (json) {
                                    profileMaps.clearOverlays();
                                    profileMaps.setMarkers(profileMaps.map, json);
                                }, dataType: "json", async: false, cache: true
                            });
                        }, 3000);
                    }
                }

                //country
                if (profileMaps.map.getZoom() <= 4) {
                    if (profileMaps.prevZoom > 4) {
                        var _lat = profileMaps.map.getCenter().lat();
                        var _lng = profileMaps.map.getCenter().lng();
                        var json_data = { typ: 1, userId: profileMaps.UserId, lat: _lat, lng: _lng };
                        $.ajax({
                            url: "/map/GetProfileMapsList", data: json_data, success: function (json) {
                                profileMaps.clearOverlays();
                                profileMaps.setMarkers(profileMaps.map, json);
                            }, dataType: "json", async: false
                        });
                    }
                }

                //city
                if (profileMaps.map.getZoom() > 4 && profileMaps.map.getZoom() < 8) {
                    if (profileMaps.prevZoom <= 4 || profileMaps.prevZoom >= 8) {
                        var _lat = profileMaps.map.getCenter().lat();
                        var _lng = profileMaps.map.getCenter().lng();
                        var json_data = { typ: 2, userId: profileMaps.UserId, lat: _lat, lng: _lng };
                        $.ajax({
                            url: "/map/GetProfileMapsList", data: json_data, success: function (json) {
                                profileMaps.clearOverlays();
                                profileMaps.setMarkers(profileMaps.map, json);
                            }, dataType: "json", async: false
                        });
                    }
                }

                profileMaps.userZoom = true;
                profileMaps.prevZoom = profileMaps.map.getZoom();
            }
        });
        //end:zoom
    },
    setMarkers: function (map, locations) {
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var image = new google.maps.MarkerImage(beach.icon);
            var myLatLng = new google.maps.LatLng(beach.lat, beach.lng);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                title: beach.Title,
                zIndex: null,
                customId: beach.Id,
                typ:beach.typ
            });
            profileMaps.markersArray.push(marker);
            profileMaps.setMarkerClick(map, marker, beach);
            
        } //end:for
    },
    setMarkerClick: function (map, marker, beach) {
        google.maps.event.addListener(marker, "click", function (e) {

            if (map.getZoom() < 9 && marker.typ != 3) {

                var _lat = marker.getPosition().lat();
                var _lng = marker.getPosition().lng();
                profileMaps.clearOverlays();

                if (map.getZoom() <= 4) {
                    map.setCenter(marker.getPosition());
                    map.setZoom(5);
                    setTimeout(function () {
                        var locals = null;
                        var json_data = { typ: 2, userId: profileMaps.UserId, lat: _lat, lng: _lng };
                        $.ajax({
                            url: "/map/GetProfileMapsList", data: json_data, success: function (json) {
                                profileMaps.setMarkers(map, json);
                            }, dataType: "json", async: false
                        });
                    }, 2000);
                }
                else {
                    map.setCenter(marker.getPosition());
                    map.setZoom(9);

                    setTimeout(function () {
                        var locals = null;
                        var json_data = { typ: 3, userId: profileMaps.UserId, lat: _lat, lng: _lng };
                        $.ajax({
                            url: "/map/GetProfileMapsList", data: json_data, success: function (json) {
                                profileMaps.setMarkers(map, json);
                            }, dataType: "json", async: false
                        });
                    }, 2000);
                }

            } else {
                $(".infoBox:visible").detach();

                var boxText = document.createElement("div");
                boxText.setAttribute("id", "boxId" + beach[4]);
                //boxText.className = 'mapInfoBox';
                boxText.innerHTML = profileMaps.getSupplyInfo(this.customId);
                profileMaps.infoBoxOptions["content"] = boxText;
                var ib = new InfoBox(profileMaps.infoBoxOptions);
                ib.open(map, this);
            }
        });
    },
    clearOverlays:function() {
        for (var i = 0; i < profileMaps.markersArray.length; i++) {
            profileMaps.markersArray[i].setMap(null);
        }
    }
    ,getData: function (typ,lat, lng) {
        var json_data = { typ: typ, userId: profileMaps.UserId, lat: lat, lng: lng };
        $.ajax({
            url: "/map/GetProfileMapsList", data: json_data, success: function (json) {
                return json;
            }, dataType: "json", async: false
        });
    },
    getSupplyInfo: function (supplyId) {
        var json_data = { supplyId: supplyId, userId: profileMaps.UserId };
        var res = "";
        $.ajax({
            url: "/Map/MapsProfileSupplyInfo", data: json_data, success: function (json) {
                if (json.Success) {
                    res = json.Message;
                } else {
                    alert("Error!");
                    res = "no data";
                }
            }, dataType: "json", async: false
        });

        return res;
    }
};