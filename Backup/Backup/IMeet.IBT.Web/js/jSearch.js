﻿var baseSearch = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (baseSearch.scrolling == true) {
                    if ($("#listType").val() == "Country") {
                        baseSearch.GetCountryList(baseSearch.page);
                    }
                    else if ($("#listType").val() == "City") {
                        baseSearch.GetCityList(baseSearch.page);
                    }
                    else if ($("#listType").val() == "Supplier") {
                        baseSearch.GetSupplyList(baseSearch.page);
                    }
                    else {
                        baseSearch.GetMemberList(baseSearch.page);
                    }
                    baseSearch.page++;
                }
            }
        });
    },
    GetSearchListOnClick: function () {
        if (baseSearch.scrolling == true) {
            if ($("#listType").val() == "Country") {
                baseSearch.GetCountryList(baseSearch.page);
            }
            else if ($("#listType").val() == "City") {
                baseSearch.GetCityList(baseSearch.page);
            }
            else if ($("#listType").val() == "Supplier") {
                baseSearch.GetSupplyList(baseSearch.page);
            }
            else {
                baseSearch.GetMemberList(baseSearch.page);
            }
            baseSearch.page++;
        }
    },
    CountryResultList: function () {
        //baseSearch.scrolling = false;
        var json_data = { baseKeyword: $("#baseKeyword").val() };
        $.post("/Search/CountryResultList", json_data, function (json) {
            if (json.Success) {
                $('#countryDiv').empty();
                $('#countryDiv').append(json.Message);
                if (json.Item.itemTotal == 0) {
                    document.getElementById("countryList").style.display = "none";
                }
                else {
                    document.getElementById("countryList").style.display = "block";
                }
                //baseSearch.scrolling = true;
            }
        }, "json");

    },
    CityResultList: function () {
        var json_data = { baseKeyword: $("#baseKeyword").val() };
        $.post("/Search/CityResultList", json_data, function (json) {
            if (json.Success) {
                $('#cityDiv').empty();
                $('#cityDiv').append(json.Message);
                if (json.Item.itemTotal == 0) {
                    document.getElementById("cityList").style.display = "none";
                }
                else {
                    document.getElementById("cityList").style.display = "block";
                }
            }
        }, "json");

    },
    SupplierResultList: function () {
        var json_data = { baseKeyword: $("#baseKeyword").val() };
        $.post("/Search/SupplierResultList", json_data, function (json) {
            if (json.Success) {
                $('#supplyDiv').empty();
                $('#supplyDiv').append(json.Message);
                if (json.Item.itemTotal == 0) {
                    document.getElementById("supplyList").style.display = "none";
                }
                else {
                    document.getElementById("supplyList").style.display = "block";
                }
            }
        }, "json");

    },
    SupplyResultCount: function (page) {
        var json_data = { page: page, pageSize: baseSearch.pageSize, filterLetter: $("#filterLetter").val(), baseKeyword: $("#hidkeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Search/SupplyAllList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {

                    $("#SearchSupplier_count").empty();
                    $("#SearchSupplier_count").text(json.Item.total);

                    $("#hidSupplycount").attr("value", json.Item.total);
                    ibt.ibtRating();
                }
            }
        });
    },
    MemberResultList: function () {
        var json_data = { baseKeyword: $("#baseKeyword").val() };
        $.post("/Search/MemberResultList", json_data, function (json) {
            if (json.Success) {
                $('#memberDiv').empty();
                $('#memberDiv').append(json.Message);
                if (json.Item.itemTotal == 0) {
                    document.getElementById("memberList").style.display = "none";
                }
                else {
                    document.getElementById("memberList").style.display = "block";
                }
            }
        }, "json");

    },
    MemberResultCount: function (page) {
        var json_data = { page: page, pageSize: baseSearch.pageSize, filterLetter: $("#filterLetter").val(), baseKeyword: $("#hidkeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Search/MemberAllList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {

                    $("#SearchMember_count").empty();
                    $("#SearchMember_count").text(json.Item.total);

                    $("#hidMembercount").attr("value", json.Item.total);
                }
            }
        });
    },
    IBTRating: function () {
        $('.IBTRating').raty({
            readOnly: false,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                //ibt.rrwb_rating($(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
            }
        });
    },
    GetCountryList: function (page) {
        baseSearch.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#filterList');
        var json_data = { page: page, pageSize: baseSearch.pageSize, filterKeyword: $("#baseKeyword").val(), saveLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/BrowseListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    baseSearch.scrolling = false;
                    baseSearch.itemTotal = json.Item.itemTotal;
                    document.getElementById("feedDiv").style.display = "block";
                    document.getElementById("searchDiv").style.display = "none";
                    if (json.Item.total == 0) {
                        document.getElementById("feedDiv").style.display = "none";
                    }
                    $("#hidcount").attr("value", json.Item.total);
                    $("#baseSearch_count").text(json.Item.total);

                    $('#filterList').append(json.Message);
                    if (json.Item.itemTotal >= baseSearch.pageSize) {
                        $("#clickloadmore_filterList").show();
                    } else {
                        $("#clickloadmore_filterList").hide();
                    }
                    $('.loading').remove();
                    ibt.ibtRating();
                    checkboxshow();
                    baseSearch.scrolling = true;
                }
            }
        });
    },
    GetCityList: function (page) {
        baseSearch.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#filterList');
        var json_data = { page: page, pageSize: baseSearch.pageSize, filterKeyword: $("#baseKeyword").val(), filterLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/CityListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    baseSearch.scrolling = false;
                    baseSearch.itemTotal = json.Item.itemTotal;
                    if (json.Item.total == 0) {
                        document.getElementById("feedDiv").style.display = "none";
                    }
                    else {
                        document.getElementById("feedDiv").style.display = "block";
                    }
                    document.getElementById("searchDiv").style.display = "none";
                    $("#hidcount").attr("value", json.Item.total);
                    $("#baseSearch_count").text($("#hidcount").val());
                    //$("#SearchCity_count").text(json.Item.total);
                    $('#filterList').append(json.Message);
                    if (json.Item.itemTotal >= baseSearch.pageSize) {
                        $("#clickloadmore_filterList").show();
                    } else {
                        $("#clickloadmore_filterList").hide();
                    }
                    $('.loading').remove();
                    ibt.ibtRating();
                    checkboxshow();
                    baseSearch.scrolling = true;
                }
            }
        });
    },
    CityInitCount: function (page) {
        var json_data = { page: page, pageSize: baseSearch.pageSize, filterLetter: $("#filterLetter").val(), filterKeyword: $("#hidkeyword").val() };
        $.ajax({
            url: '/Search/CityListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $("#SearchCity_count").empty();
                    $("#SearchCity_count").text(json.Item.total);
                    $("#hidtab2count").attr("value", json.Item.total);  //初始化保存数据
                    if (json.Item.total == 0) {
                        document.getElementById("feedDiv").style.display = "none";
                    }
                    ibt.ibtRating();
                }
            }
        });
    },
    GetSupplyList: function (page) {
        baseSearch.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchList');
        var json_data = { page: page, pageSize: baseSearch.pageSize, filterLetter: $("#filterLetter").val(), baseKeyword: $("#baseKeyword").val() };
        $.ajax({
            url: '/Search/SupplyAllList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    if (json.Item.total == 0 && $("#filterLetter").val()=="") {
                        document.getElementById("member").click();
                    }
                    baseSearch.scrolling = false;
                    baseSearch.itemTotal = json.Item.itemTotal;
                    $("#hidcount").attr("value", json.Item.total);
                    $("#baseSearch_count").text(json.Item.total);

                    $("#hidkeyword").attr("value", $("#baseKeyword").val());
                    $("#baseSearch_keyword").text($("#baseKeyword").val());
                    $('#searchList').append(json.Message);
                    if (json.Item.itemTotal >= baseSearch.pageSize) {
                        $("#clickloadmore_searchList").show();
                    } else {
                        $("#clickloadmore_searchList").hide();
                    }
                    $('.loading').remove();
                    ibt.ibtRating();
                    checkboxshow();
                    baseSearch.scrolling = true;
                }
            }
        });
    },
    SupplyInitCount: function () {
        var json_data = { filterKeyword: $("#hidkeyword").val() };
        $.ajax({
            url: '/Search/GetAllSupplierCount', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $("#SearchSupplier_count").empty();
                    $("#SearchSupplier_count").text(json.Message);
                }
            }
        });
    },
    GetMemberList: function (page) {
        baseSearch.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchList');
        var json_data = { page: page, pageSize: baseSearch.pageSize, filterLetter: $("#filterLetter").val(), baseKeyword: $("#baseKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Search/MemberAllList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    baseSearch.scrolling = false;
                    baseSearch.itemTotal = json.Item.itemTotal;
                    $("#hidcount").attr("value", json.Item.total);
                    $("#baseSearch_count").text(json.Item.total);
                    //alert($("#hidSupplycount").val());
                    //$("#SearchMember_count").text(json.Item.total);


                    $("#hidkeyword").attr("value", $("#baseKeyword").val());
                    $("#baseSearch_keyword").text($("#baseKeyword").val());
                    $('#searchList').append(json.Message);
                    if (json.Item.itemTotal >= baseSearch.pageSize) {
                        $("#clickloadmore_searchList").show();
                    } else {
                        $("#clickloadmore_searchList").hide();
                    }
                    $('.loading').remove();
                    checkboxshow();
                    //if ($('#searchList').children('li').length == 0) {
                    //    $('<div class="noData">Currently, the search have not result!</div>').appendTo('#searchList');
                    //}
                    baseSearch.scrolling = true;
                }
            }
        });
    },
    MemberInitCount: function () {
        var json_data = {filterKeyword: $("#hidkeyword").val() };
        $.ajax({
            url: '/Search/GetAllMemberCount', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $("#SearchMember_count").empty();
                    $("#SearchMember_count").text(json.Message);
                }
            }
        });
    },
    initData: function () {
        $("#searchList").empty();
        document.getElementById("quickSearch").style.display = "block";
        document.getElementById("feedDiv").style.display = "block";
        document.getElementById("searchDiv").style.display = "none";
        baseSearch.GetCountryList(1);
        $("#hidtabcount").attr("value", $("#hidcount").val());  //初始化保存数据
        $("#SearchCountry_count").text($("#hidtabcount").val());
        baseSearch.CityInitCount(1);
        baseSearch.SupplyInitCount();
        baseSearch.MemberInitCount();
        baseSearch.page = 2;
        //判断tab
        if ($("#hidtabcount").val() == "0") {
            if ($("#hidtab2count").val() == "0") {
                document.getElementById("supply").click();
            }
            else {
                document.getElementById("city").click();
            }
        }
        else {
            var selectCount = parseInt($("#hidcount").val());
            if (selectCount == 1) {
                $("#baseSearch_title").html("Search Results <span class=\"txt\">1 Country Found</span>");
            }
            else if (selectCount > 1) {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Countries Found</span>");
            }
            else {
                document.getElementById("feedDiv").style.display = "none";
                $("#baseSearch_title").html("Search Results <span class=\"txt\">There are no countries matching <span id=\"baseSearch_keyword\">your search keyword</span></span>");
            }
            $("#baseSearch_count").text($("#hidcount").val());
        }
        $("#filterLetter").val("");
        $('.noData').detach();
        /*20130221 裕冲修改*/
        ibt.ibtRating();
    },
    goSearch: function () {
        $("#countryDiv").empty();//先删除以前的
        $("#cityDiv").empty();
        $("#supplyDiv").empty();
        $("#memberDiv").empty();
        $("#listType").val("All");
        baseSearch.CountryResultList();
        baseSearch.CityResultList();
        baseSearch.SupplierResultList();
        baseSearch.MemberResultList();
        $("#resultALL").html("<a href=\"/Search/Index/" + $("#baseKeyword").val() + "\" >See more results for " + $("#baseKeyword").val() + "</a>");//<span id=\"search_allcount\">0</span>  onclick=\"baseSearch.goTab(this,1);\"
    },
    goTab: function (obj, id) {
        baseSearch.page = 2;
        baseSearch.scrolling = false;
        baseSearch.itemTotal = -1;
        $("#quickSearch a").each(function () {
            $(this).removeClass("active");
        });
        $("#filterLetter").val("");
        $('.noData').detach();
        switch (id) {
            case 1:
                $("#listType").val("Member");
                $("#searchList").empty();
                document.getElementById("quickSearch").style.display = "block";
                document.getElementById("feedDiv").style.display = "none";
                document.getElementById("searchDiv").style.display = "block";
                baseSearch.GetMemberList(1);

                var searchCount = parseInt($("#hidcount").val());
                if (searchCount <= 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Member  Found</span>");
                }
                else if (searchCount > 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Members  Found</span>");
                }
                else {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\">There are no members matching <span id=\"baseSearch_keyword\">your search keyword</span></span>");
                    $("#baseSearch_keyword").text($("#baseKeyword").val());
                }
                $("#baseSearch_count").text($("#hidcount").val());
                break;
            case 2:
                $("#listType").val("City");
                $("#filterList").empty();
                document.getElementById("quickSearch").style.display = "block";
                document.getElementById("feedDiv").style.display = "block";
                document.getElementById("searchDiv").style.display = "none";
                baseSearch.GetCityList(1);
                var searchCount = parseInt($("#hidcount").val());
                if (searchCount <= 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> City Found</span>");
                }
                else if (searchCount > 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Cities  Found</span>");
                }
                else {
                    document.getElementById("feedDiv").style.display = "none";
                    $("#baseSearch_title").html("Search Results <span class=\"txt\">There are no cities matching your search <span id=\"baseSearch_keyword\">keyword</span></span>");
                    $("#baseSearch_keyword").text($("#baseKeyword").val());
                }
                $("#baseSearch_count").text($("#hidcount").val());
                break;
            case 3:
                $("#listType").val("Venues");
                document.getElementById("feedDiv").style.display = "none";
                document.getElementById("searchDiv").style.display = "block";
                $("#baseSearch_title").html("Search Results <span class=\"txt\">About <span id=\"baseSearch_count\">-</span> Venues in  <span id=\"baseSearch_keyword\">-</span></span>");
                break;
            case 4:
                $("#listType").val("Supplier");
                $("#searchList").empty();
                document.getElementById("quickSearch").style.display = "block";
                document.getElementById("feedDiv").style.display = "none";
                document.getElementById("searchDiv").style.display = "block";
                baseSearch.GetSupplyList(1);

                //$("#SearchSupplier_count").empty();
                //$("#SearchSupplier_count").text($("#hidcount").val());  

                var searchCount = parseInt($("#hidcount").val());
                if (searchCount <= 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Supplier Found</span>");
                }
                else if (searchCount > 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Suppliers Found</span>");
                }
                else {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\">There are no suppliers matching <span id=\"baseSearch_keyword\">your search keyword</span></span>");
                    $("#baseSearch_keyword").text($("#baseKeyword").val());
                }
                $("#baseSearch_count").text($("#hidcount").val());
                break;
            default: //default 0 
                $("#listType").val("Country");
                $("#filterList").empty();
                document.getElementById("quickSearch").style.display = "block";
                document.getElementById("feedDiv").style.display = "block";
                document.getElementById("searchDiv").style.display = "none";
                baseSearch.GetCountryList(1);
                var searchCount = parseInt($("#hidcount").val());
                if (searchCount <= 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Country Found</span>");
                }
                else if (searchCount > 1) {
                    $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Countries Found</span>");
                }
                else {
                    document.getElementById("feedDiv").style.display = "none";
                    $("#baseSearch_title").html("Search Results <span class=\"txt\">There are no countries matching <span id=\"baseSearch_keyword\">your search keyword</span></span>");
                    $("#baseSearch_keyword").text($("#baseKeyword").val());
                }
                $("#baseSearch_count").text($("#hidcount").val());
                break;


        }

    },
    quickSearch: function (obj, letter) {
        $("#filterList").empty();
        $("#searchList").empty();
        $('.noData').detach();
        baseSearch.scrolling = false;
        baseSearch.itemTotal = -1;
        baseSearch.page = 2;
        if ($("#filterLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#filterLetter").val("");
            if ($("#listType").val() == "Country") {
                baseSearch.GetCountryList(1);
            }
            else if ($("#listType").val() == "City") {
                baseSearch.GetCityList(1);
            }
            else if ($("#listType").val() == "Supplier") {
                baseSearch.GetSupplyList(1);
            }
            else {
                baseSearch.GetMemberList(1);
            }
        } else {
            $("#quickSearch a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");

            $("#filterLetter").val(letter);
            if ($("#listType").val() == "Country") {
                baseSearch.GetCountryList(1);
            }
            else if ($("#listType").val() == "City") {
                baseSearch.GetCityList(1);
            }
            else if ($("#listType").val() == "Supplier") {
                baseSearch.GetSupplyList(1);
            }
            else {
                baseSearch.GetMemberList(1);
            }
        }
        var searchCount = parseInt($("#hidcount").val());
        if ($("#listType").val() == "Country") {
            if (searchCount > 1) {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Countries Found</span>");
            }
            else {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Country Found</span>");
            }
        }
        else if ($("#listType").val() == "City") {
            if (searchCount <= 1) {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> City Found</span>");
            }
            else {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Cities Found</span>");
            }
        }
        else if ($("#listType").val() == "Supplier") {
            if (searchCount <= 1) {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Supplier Found</span>");
            }
            else {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Suppliers Found</span>");
            }
        }
        else {
            if (searchCount <= 1) {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Member Found</span>");
            }
            else {
                $("#baseSearch_title").html("Search Results <span class=\"txt\"><span id=\"baseSearch_count\">-</span> Members Found</span>");
            }
        }
        $("#baseSearch_count").text($("#hidcount").val());
    }
}