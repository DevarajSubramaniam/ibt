﻿var invite = {
    invitefriend: function () {
        if ($('#emailform').data('bValidator').validate() == false) {
            return;
        }

        var json_data = $("#emailform").MySerialize();
        $.post("/Connection/InviteFriend", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Invite Friend" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Invite Friend" });
                $(".boxyInner").addClass("popup");
                //$("#tipMsg").html(json.Message);
            }
        }, "json");
       
    }
};

