﻿var Calendar={
    Year:[1935,1936,1937,1938,1939,1940,1941,1942,1943,1944,1945,1946,1947,1948,1949,1950,1951,1952,1953,1954,1955,1956,1957,1958,1959,1960,1961,1962,1963,1964,1965,1966,1967,1968,1969,1970,1971,1972,1973,1974,1975,1976,1977,1978,1979,1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992],
    Month:[{Key:"01",Value:1},{Key:"02",Value:2},{Key:"03",Value:3}, {Key:"04",Value:4},{Key:"05",Value:5},{Key:"06",Value:6},{Key:"07",Value:7},{Key:"08",Value:8},{Key:"09",Value:9},{Key:"10",Value:10},{Key:"11",Value:11},{Key:"12",Value:12}],
    Day:[{Key:"01",Value:1},{Key:"02",Value:2},{Key:"03",Value:3},{Key:"04",Value:4},{Key:"05",Value:5},{Key:"06",Value:6},{Key:"07",Value:7},{Key:"08",Value:8},{Key:"09",Value:9},{Key:"10",Value:10},{Key:"11",Value:11},{Key:"12",Value:12},{Key:"13",Value:13}, {Key:"14",Value:14},{Key:"15",Value:15},{Key:"16",Value:16},{Key:"17",Value:17}, {Key:"18",Value:18},{Key:"19",Value:19},{Key:"20",Value:20},{Key:"21",Value:21},{Key:"22",Value:22},{Key:"23",Value:23},{Key:"24",Value:24},{Key:"25",Value:25},{Key:"26",Value:26},{Key:"27",Value:27},{Key:"28",Value:28},{Key:"29",Value:29},{Key:"30",Value:30},{Key:"31",Value:31}]
}

function ChangeDateTime(selectYearID,selectMonthID,selectDayID,changeID)
{
    var objSelectYear=document.getElementById(selectYearID);
    var objSelectMonth=document.getElementById(selectMonthID);
    var objSelectDay=document.getElementById(selectDayID);
    
    var curYearIndex=objSelectYear.selectedIndex;
    var curMonthIndex=objSelectMonth.selectedIndex;
    var curDayIndex=objSelectDay.selectedIndex;
    var curYearValue=Number(objSelectYear.options[curYearIndex].value);
    var curMonthValue=Number(objSelectMonth.options[curMonthIndex].value);
    var curDayValue=Number(objSelectDay.options[curDayIndex].value);
    
    if(changeID==1)
    {
        if(curMonthValue==2 && !IsLeapYear(curYearValue))
        {
            if(curDayValue>28)
            {
                objSelectDay.options[28].selected = true; 
                
            }
        }
    }
    else if(changeID==2)
    {
        if(curMonthValue==2 && (IsLeapYear(curYearValue) || curYearValue==-1))
        {
            if(curDayValue>29)
            {
                objSelectDay.options[29].selected = true; 
                
            }
        }
        else if(curMonthValue==2 && IsLeapYear(curYearValue)==false)
        {
            if(curDayValue>28)
            {
                objSelectDay.options[28].selected = true; 
            }
        }
        else if(curMonthValue==4 || curMonthValue==6 || curMonthValue==9 || curMonthValue==11)
        {
            if(curDayValue>30)
            {
                objSelectDay.options[30].selected = true; 
            }
        }
    }
    else if(changeID==3)
    {
        if((curDayValue==31 && (curMonthValue==4 || curMonthValue==6 || curMonthValue==9 || curMonthValue==11)) || (curMonthValue==2 && (IsLeapYear(curYearValue) || curYearValue==-1) && curDayValue>29) || (curMonthValue==2 && IsLeapYear(curYearValue)==false && curDayValue>28))
        {
            objSelectMonth.options[0].selected = true; 
        }
    }
}

//初始化年月日
function InitBirthDay(selectYearID,selectMonthID,selectDayID)
{
    var objSelectYear=document.getElementById(selectYearID);
    var objSelectMonth=document.getElementById(selectMonthID);
    var objSelectDay=document.getElementById(selectDayID);
    InitYear(objSelectYear);
    InitMonth(objSelectMonth);
    InitDay(objSelectDay);
}

//初始化年
function InitYear(objSelectYear)
{
    objSelectYear.options.length=0;
    objSelectYear.options.add(new Option("Year","-1"));
    for(var i=0;i<Calendar.Year.length;i++)
    {
        var objYear=Calendar.Year[i];
        objSelectYear.options.add(new Option(objYear,objYear));
    }
}

//初始化月
function InitMonth(objSelectMonth)
{
    objSelectMonth.options.length=0;
    objSelectMonth.options.add(new Option("Month","-1"));
    for(var i=0;i<Calendar.Month.length;i++)
    {
        var objMonth=Calendar.Month[i];
        objSelectMonth.options.add(new Option(objMonth.Key,objMonth.Value));
    }
    
}

//初始化天
function InitDay(objSelectDay)
{
    objSelectDay.options.add(new Option("Day","-1"));
    for(var i=0;i<Calendar.Day.length;i++)
    {
        var objDay=Calendar.Day[i];
        objSelectDay.options.add(new Option(objDay.Key,objDay.Value));
    }
}

//判断是否是闰年
function IsLeapYear(year)
{
    if((year%4==0 && year%100!=0) || (year%400==0))
    {
        return true;
    }
    else
    {
        return false;
    }
}

