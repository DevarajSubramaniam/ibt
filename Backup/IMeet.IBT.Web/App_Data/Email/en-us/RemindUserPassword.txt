﻿@Subject=Password Notification
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>I've Been There</title>
</head>

<body>
<table cellpadding="0" cellspacing="0" width="675px" border="0" style=" font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; margin:0 auto; border:1px solid #ccc;">
	<tr valign="top">
    	<td style="padding:10px 20px 20px; "><a href="{AlphaSiteUrl}" title="I've Been There"  style="width:168px; height:44px; display:block;"><img style="border:0; display:block;" src="{AlphaSiteUrl}/pictures/logo.gif" alt="I've Been There" /></a></td>
    </tr>
	<tr valign="top">
        <td style="font-size:17px; text-transform:uppercase; color:#31aee3; line-height:22px;  padding:10px 20px;">Posted Visits By Our Members</td>
	</tr>
	<tr valign="top">
    	<td background="{AlphaSiteUrl}/pictures/gray_bg01.gif" height="40px" style=" background-position:left top; background-repeat:repeat-x; padding:0 20px; background-color:#dadada; border-top:1px solid #dadada; border-bottom:1px solid #d8d8d8;">
        	<table  border="0" cellpadding="0" width="100%" cellspacing="0" background="{AlphaSiteUrl}/pictures/gray_bg02.gif" style="border-right:1px solid #d2d2d2; border-left:1px solid #d2d2d2; background-position: left top; background-repeat:repeat-x; background-color:#cfcfcf; height:40px; line-height:34px;" align="center">
            	<tr>
                	<td style="padding:5px 0px 0px 15px;color:#0070a9;"  align="center" valign="top"  height="38px">
                    	<span style="float:left; border:1px solid #a3c6d4;line-height:16px; margin-top:4px; padding:5px; background-color:#fff;  font-size:15px; margin-right:2px;">{StatisticsCountry}</span> Countries<img src="http://alpha.ibt.i-meet.com/pictures/line.gif" style=" vertical-align:middle; margin-left:8px; margin-right:8px;" />
                    </td>
                	<td style="padding:5px 0px 0px 0px;color:#0070a9;"  align="center" valign="top"  height="38px">
                    	<span style="float:left; border:1px solid #a3c6d4; line-height:16px; margin-top:4px; padding:5px; background-color:#fff;  font-size:15px; margin-right:2px;">{StatisticsCity}</span> Cities<img src="http://alpha.ibt.i-meet.com/pictures/line.gif" style=" vertical-align:middle; margin-left:8px; margin-right:8px;" />
                    </td>
                	<td style="padding:5px 0px 0px 0px;color:#0070a9;"  align="center" valign="top"  height="38px">
                    	<span style="float:left; border:1px solid #a3c6d4; line-height:16px; margin-top:4px; padding:5px; background-color:#fff;  font-size:15px; margin-right:2px;">{StatisticsHotel}</span> Hotels<img src="http://alpha.ibt.i-meet.com/pictures/line.gif" style=" vertical-align:middle; margin-left:8px; margin-right:8px;" />
                    </td>
                	<td style="padding:5px 0px 0px 0px;color:#0070a9;"  align="center" valign="top"  height="38px">
                    	<span style="float:left; border:1px solid #a3c6d4; line-height:16px; margin-top:4px; padding:5px; background-color:#fff;  font-size:15px; margin-right:2px;">{StatisticsVenue}</span> Lifestyle<img src="http://alpha.ibt.i-meet.com/pictures/line.gif" style=" vertical-align:middle; margin-left:8px; margin-right:8px;" />
                    </td>
                	<td style="padding:5px 0px 0px 0px;color:#0070a9;"  align="center" valign="top"  height="38px">
                    	<span style="float:left; border:1px solid #a3c6d4; line-height:16px; margin-top:4px; padding:5px; background-color:#fff;  font-size:15px; margin-right:2px;">{StatisticsOther}</span> Attractions
                    </td>
              </tr>
            </table>
        </td>
    </tr>
	<tr valign="top">
    	<td style="background-color:#f2f2f2; padding-top:5px;">
                                  

        	<table  border="0"  style="background-color:#fff; color:#9d9d9d; font-size:12px; line-height:18px;" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td style="padding:50px 20px 30px;">This is the email that is generated AFTER an unsuccessful login – where the member clicks a link called <br />"<a href="#" style="color:#9d9d9d; text-decoration:none;">Forgot your password</a>?" </td>
                  </tr>
                  <tr>
                    <td style="padding:20px 20px 0px; color:#31aee3">
                    
                     	PASSWORD NOTIFICATION
                    </td>
                  </tr>
                  <tr>
                    <td style="padding:20px 20px 0px;">
                   Thank you for contacting I've Been There.  Here is your password based on this email address:
                    </td>
                   </tr>
                   <tr>
                    <td style="padding:20px 20px 50px;">
                          {password}
                    </td>
                    </tr>
            </table>
        </td>
    </tr>
	
	
	
    
	<tr valign="top">
    	<td style="background-color:#f8f8f8; padding: 20px; color:#9d9d9d; font-size:11px;">
        	<table  border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td valign="middle" style=" border-right:1px solid #ededed; padding:10px;">
                    	Your World Travel Notebook, Rating and Sharing Tool. 
                   </td>
                   <td valign="middle"  style=" padding:10px 20px;"><img src="{AlphaSiteUrl}/pictures/complete.gif" alt="" style="margin-right:10px; vertical-align:middle;" />Add more travels at <a style="color:#31aee3;" href="www.Ive-Been-There.com" target="_blank">www.Ive-Been-There.com</a></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr valign="top">
    	<td style="padding:20px; color:#9d9d9d; font-size:10px;">
        Important Note About Privacy: No personal contact information is visible within I've Been There so your privacy is always protected. 
We always respect your wishes.  If for any reason you do not wish to be included in our online community, choose Update 
Settings/Profile to change your preferences or de-activate your account.
        </td>
    </tr>
	<tr valign="top">
    	<td style="padding:0 20px 20px; color:#9d9d9d; font-size:10px;">
       Too Many Email Notifications?  l 
       <a href="#" style="color:#9d9d9d; text-decoration:none;">Unsubscribe Email Notifications</a>  |  
        <a href="#" style="color:#9d9d9d; text-decoration:none;">Unsubscribe Weekly Member Update</a>
		<a href="www. Ive-Been-There.com" target="_blank" style="color:#9d9d9d; text-decoration:none;">Visit www. Ive-Been-There.com</a>  |  
         <a href="#" style="color:#9d9d9d; text-decoration:none;">Privacy Statement</a>  |  
          <a href="#" style="color:#9d9d9d; text-decoration:none;">Update Settings/Profile</a>
        </td>
    </tr>
</table>
</body> 
</html>
