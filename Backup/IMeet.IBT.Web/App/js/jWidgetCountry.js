﻿var WidgetCountryManage = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (WidgetCountryManage.scrolling == true) {
                    WidgetCountryManage.GetCountryList(WidgetCountryManage.page);
                    WidgetCountryManage.page++;
                }       
            }
        });
    },
    GetCountryList: function (page) {
        WidgetCountryManage.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#talCountry');
        var json_data = { page: page, pageSize: WidgetCountryManage.pageSize };
        $.ajax({
            url: '/Admin/Home/WidgetCountryListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    WidgetCountryManage.scrolling = false;
                    WidgetCountryManage.itemTotal = json.Item.itemTotal;
                    $("#talCountry").append(json.Message);
                    //$('#talCountry').append("<tr><td class=\"alignR\" colspan=\"9\"><div class=\"controlBg control1\"><span class=\"bg\"><a href=\"javascript:void(0);\" class=\"add NborderR\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddWidgetCountry','Get','',function(){$('.jscroll-e').show();scroll_init(scroll_blueSearch2);SearchData.initData(1);SearchData.init(1);},'Search');\" ></a> </span></div></td><tr>");
                    //$("#talCountry").prepend(json.Message);
                    $('.loading').remove();
                    WidgetCountryManage.scrolling = true;
                }
            }
        });
    },
    initData: function () {
        $("#talCountry").empty();
        WidgetCountryManage.GetCountryList(1);
        WidgetCountryManage.page = 2;
        $('.noData').remove();
    },
    AddWidgetCountry: function (objId, scroe) {
        if (confirm('Do you really want to add this country data?')) {
            var json_data = { objId: objId, scroe: scroe };
            $.post("/Admin/Home/AddWidgetCountryAjax", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add WidgetIBT" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add WidgetIBT" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }


    },
    saveSort: function () {
        var json_data = { arrayData: $("#hidArray").val() };
        $.ajax({
            url: '/Admin/Home/PaiXuCountry', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Modify Score" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    DelWidgetCountry: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/WidgetCountryManegeDelete", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del Country" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del Country" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }
    }
};

