﻿var WidgetCode = {
    previewWidget: function () {
        $('#divWidget_Preview').html('');
        var reg = /^-?\d+$/;
        var mc = $("#txtMember").val();
        if (!reg.test(mc)) {
            $("#txtMember").focus();
            return false;
        }
        var tc = $("#txtTopic").val();
        if (!reg.test(tc)) {
            $("#txtTopic").focus();
            return false;
        }
        var areg = /^[0-9]*[1-9][0-9]*$/;
        var wd = $("#txtWidth").val();
        if (!areg.test(wd)) {
            $("#txtWidth").focus();
            return false;
        }
        var bc = '#' + $("#txtBgColor").val();
        $('#divWidget_Preview').getGroupWidget({
            groupId: '214',
            memberCount: mc,
            topicCount: tc,
            width: wd,
            key: '56EEACAA865394B8',
            headingText: 'Argentina Community @ i-Meet',
            bgcolor: bc
        });
        WidgetCode.createWidgetCode(mc, tc, wd, bc);
    },
    createWidgetCode: function (memberCount, topicCount, width, color) {
        var html = '<script type="text/javascript" src="http://alpha.i-meet.com/widget/js/jquery.js"><\/script>\r\n';
        html += '<script type="text/javascript" src="http://alpha.i-meet.com/widget/js/groupWidget.js"><\/script>\r\n';
        html += '<script type="text/javascript">\r\n';
        html += '$(document).ready(function() {\r\n';
        html += '$("#divWidget_214").getGroupWidget({\r\n';
        html += 'groupId: "214",\r\n';
        html += 'memberCount:' + memberCount + ',\r\n';
        html += 'topicCount:' + topicCount + ',\r\n';
        html += 'width: ' + width + ',\r\n';
        html += 'key: "56EEACAA865394B8",\r\n';
        html += 'headingText: "Argentina Community @ i-Meet",\r\n';
        html += 'bgcolor:"' + color + '"\r\n';
        html += '});\r\n';
        html += '});\r\n<\/script>\r\n';
        html += '<div id="divWidget_214"></div>';

        $('#txtGroupWidgetCode').val(html);
        var hreg = /^[0-9]*[1-9][0-9]*$/;
        var h = $("#txtHeight").val();
        if (!hreg.test(h)) {
            h = "300";
        }
        WidgetCode.createWidgetKey('214', '56EEACAA865394B8', 'Argentina Community @ i-Meet', memberCount, topicCount, width, h, color);
        return false;
    },
    createWidgetKey: function (g, k, n, m, t, w, h, c) {
        var ifHmtl = '<iframe src="http://alpha.i-meet.com/widget/GetGroupWidget.aspx'
     + '?groupId=' + g + '&key=' + k + '&headingText=' + encodeURIComponent(n) + '&memberCount=' + m + '&topicCount=' + t + '&width=' + w + '&bgcolor=' + encodeURIComponent(c) + '"'
     + ' width="' + (parseInt(w) + 20) + '" height="' + h + '" scrolling="no" frameborder="0" style="border:none;"></iframe>';
        $('#txtIframeCode').val(ifHmtl);
    }
};

