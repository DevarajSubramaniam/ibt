﻿var SearchManage = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        if (SearchManage.scrolling == false) {
            SearchManage.scrolling = true;
            return;
        }
        SearchManage.GetDataList(SearchManage.page);
        SearchManage.page++;
    },
    GetDataList: function (page) {
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchSupplyList');
        var json_data = { page: page, pageSize: SearchManage.pageSize, pageType: $("#hidpageType").val(), keyWord: $("#keyWord").val(), countryId: $("#hidfldCountry").val(), cityId: $("#hidfldCity").val(), cateIds: $("#hidcateIds").val(), sortBy: $("#IsShowSearch").val(), tab: $("#hidfldTab").val() };
        $.ajax({
            url: '/Admin/Home/SupplySearchAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    SearchManage.scrolling = false;
                    SearchManage.itemTotal = json.Item.itemTotal;
                    $("#resultCount").text(json.Item.total);
                    $('#searchSupplyList').append(json.Message);
                    $('.loading').remove();
                    $(".botBox").hide();
                    if (json.Item.total > 6) {
                        $(".botBox").show();
                        $(".jscroll-e").show();
                        scroll_init(scroll_blue);//jSearchSupply.js中初始化scroll.js
                    }
                }
            }
        });
    },
    GetDataListByClick: function () {
        SearchManage.GetDataList(SearchManage.page);
        SearchManage.page++;
    },
    initData: function () {
        $("#searchSupplyList").empty();
        SearchManage.GetDataList(1);
        SearchManage.page = 2;
        $('.noData').remove();
        $("#ddlCountry").change(function () {
            GetCity();
            var value = $("#ddlCountry").val();
            $("#hidfldCountry").attr("value", value);  //隐藏控件赋值
        });
        $("#ddlCity").change(function () {
            var value = $("#ddlCity").val();
            $("#hidfldCity").attr("value", value);
        });
        $("#keyWord").focus(function () {
            var val = "Type in something to search";
            if ($(this).val() == val) {
                $(this).val("");
            }
        }).blur(function () {
            if ($(this).val() == "") {
                $(this).val("Type in something to search");
            }
        });
    },
    goTab: function (obj) {
        $("#searchSupplyList").empty();//先删除以前的
        SearchManage.scrolling = false;
        SearchManage.itemTotal = -1;
        $('.noData').remove();
        SearchManage.GetDataList(1);
    },
    changeSort: function (id) {
        $("#IsShowSearch").val(id);
    },
    Save: function (type) {      
        if (type == "") {
            var json_data = { SupplyName: $("#SupplyName").val(), flag: $("#imgSave").val(), score: $("#score").val(), UserId: $("#UserId").val() };
            $.post("/Admin/Home/AddSupply", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add New Supply" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add New Supply" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }
        else {
            var json_data = { SupplyId: $("#SupplyId").val(),SupplyName: $("#SupplyName").val(), score: $("#score").val(), flag: $("#imgSave").val() };
            $.post("/Admin/Home/EditSupply", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Edit Supply" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Edit Supply" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }
       
    }
};


var SearchManage_Visited = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        if (SearchManage_Visited.scrolling == false) {
            SearchManage_Visited.scrolling = true;
            return;
        }
        SearchManage_Visited.GetDataList(SearchManage_Visited.page);
        SearchManage_Visited.page++;
    },
    GetDataList: function (page) {
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchSupplyList');
        var json_data = { page: page, pageSize: SearchManage_Visited.pageSize, pageType: $("#hidpageType").val(), keyWord: $("#keyWord").val(), countryId: $("#hidfldCountry").val(), cityId: $("#hidfldCity").val(), cateIds: $("#hidcateIds").val(), sortBy: $("#IsShowSearch").val(), tab: $("#hidfldTab").val() };
        $.ajax({
            url: '/Admin/Home/SupplySearchAjx_Visited', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    SearchManage_Visited.scrolling = false;
                    SearchManage_Visited.itemTotal = json.Item.itemTotal;
                    $("#resultCount").text(json.Item.total);
                    $('#searchSupplyList').append(json.Message);
                    $('.loading').remove();
                    $(".botBox").hide();
                    if (json.Item.total > 6) {
                        $(".botBox").show();
                        $(".jscroll-e").show();
                        scroll_init(scroll_blue);//jSearchSupply.js中初始化scroll.js
                    }
                }
            }
        });
    },
    GetDataListByClick: function () {
        SearchManage_Visited.GetDataList(SearchManage_Visited.page);
        SearchManage_Visited.page++;
    },
    initData: function () {
        $("#searchSupplyList").empty();
        SearchManage_Visited.GetDataList(1);
        SearchManage_Visited.page = 2;
        $('.noData').remove();
        $("#ddlCountry").change(function () {
            GetCity();
            var value = $("#ddlCountry").val();
            $("#hidfldCountry").attr("value", value);  //隐藏控件赋值
        });
        $("#ddlCity").change(function () {
            var value = $("#ddlCity").val();
            $("#hidfldCity").attr("value", value);
        });
        $("#keyWord").focus(function () {
            var val = "Type in something to search";
            if ($(this).val() == val) {
                $(this).val("");
            }
        }).blur(function () {
            if ($(this).val() == "") {
                $(this).val("Type in something to search");
            }
        });
    },
    goTab: function (obj) {
        $("#searchSupplyList").empty();//先删除以前的
        SearchManage_Visited.scrolling = false;
        SearchManage_Visited.itemTotal = -1;
        $('.noData').remove();
        SearchManage_Visited.GetDataList(1);
    },
    changeSort: function (id) {
        $("#IsShowSearch").val(id);
    },
    Save: function (type) {
        if (type == "") {
            var json_data = { SupplyName: $("#SupplyName").val(), flag: $("#imgSave").val(), score: $("#score").val(), UserId: $("#UserId").val() };
            $.post("/Admin/Home/AddSupply", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add New Supply" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add New Supply" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }
        else {
            var json_data = { SupplyId: $("#SupplyId").val(), SupplyName: $("#SupplyName").val(), score: $("#score").val(), flag: $("#imgSave").val() };
            $.post("/Admin/Home/EditSupply", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Edit Supply" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Edit Supply" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }

    }
};

