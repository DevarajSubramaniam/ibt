﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	 //config.toolbar = 'MyToolbar';

    config.toolbar_MyToolbar =
    [
    
        ['Bold','Italic','Underline','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','NumberedList','BulletedList','Link','Unlink','Image', 'TextColor','BGColor']
      
        
    ]; 
    
      config.toolbar_pToolbar =
    [
    
        ['Bold','Italic','Underline','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','NumberedList','BulletedList','Link','Unlink', 'TextColor','BGColor']
      
        
    ]; 
       config.toolbar_SourceToolbar =
    [
    
        ['Source','Bold','Italic','Underline','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','NumberedList','BulletedList','Link','Unlink','Image', 'TextColor','BGColor']
      
        
    ]; 
    config.toolbar_eToolbar = [     ]; 
    
 config.toolbar_myFull =
[
    ['Source','Save','NewPage','Preview','Templates'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord','Print', 'SpellChecker', 'Scayt'],
    ['Undo','Redo','Find','Replace','SelectAll','RemoveFormat'],
    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
    
    ['Bold','Italic','Underline','Strike','Subscript','Superscript'],
    ['NumberedList','BulletedList','Outdent','Indent','Blockquote','CreateDiv'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['BidiLtr', 'BidiRtl' ],
    ['Link','Unlink','Anchor'],
    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'],
  
    ['Styles','Format','Font','FontSize'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','About']
];
    
    //去掉底部一行的元素路径显示,右键菜单,toolbar.
    config.removePlugins='elementspath,contextmenu';
    
    //配置邮件菜单。
    // config.menu_groups = '';
    //config.filebrowserUploadUrl = '/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
    //config.filebrowserImageUploadUrl = '/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images';
    //config.filebrowserFlashUploadUrl = '/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash';

    //focus
    config.startupFocus = false;
    
   
};


