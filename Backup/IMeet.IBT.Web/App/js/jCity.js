﻿var CityManage = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (CityManage.scrolling == true) {
                    CityManage.GetCityList(CityManage.page);
                    CityManage.page++;
                }
            }
        });
        if (CityManage.itemTotal < 10) {
            $("#clickloadmore").hide();
        }
    },
    GetCityListOnClick: function () {
        if (CityManage.scrolling == true) {
            CityManage.GetCityList(CityManage.page);
            CityManage.page++;
        }
    },
    GetCityList: function (page) {
        CityManage.scrolling = false;
        if (this.itemTotal == 0) { $("#clickloadmore").hide(); CityManage.scrolling = false; return };
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#talCity');
        var json_data = { page: page, pageSize: CityManage.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val(), SearchKeyword: $("#SearchKeyword").val() };
        $.ajax({
            url: '/Admin/Home/CityListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    CityManage.itemTotal = json.Item.itemTotal;
                    $('#talCity').append(json.Message);
                    CityManage.txtSort();
                    $('.loading').remove();
                    CityManage.scrolling = true;
                }
            }
        });
    },
    initCityData: function () {
        $("#talCity").empty();
        CityManage.GetCityList(1);
        CityManage.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#talCity").empty();//先删除以前的
        CityManage.scrolling = false;
        CityManage.itemTotal = -1;
        $('.noData').remove();
        CityManage.page = 1;
        CityManage.GetCityList(CityManage.page);
        CityManage.page++;
        //CityManage.GetCityListOnClick();
        //CityManage.GetCityList(1);
        $("#clickloadmore").show();
    },
    txtSort: function () {
        $(".cssTaskSn").click(function () {//给cssTaskSn加上click
            var objTd = $(this);//保存对象
            var oldText = $.trim(objTd.text());//保存原来的文本
            var input = $("<input id='txtorderby'  type='text' value='" + oldText + "' style='width:30px;text-align:center;' />");//定义input变量为一个文本框把原来文本写入
            objTd.html(input)//当前td变为文本框，把原来文本写入
            //设置文本框点击事件失效
            input.click(function () {
                return false;
            });
            //设置select全选文本事件,先触发焦点
            input.trigger("focus").trigger("select");

            //文本框失去焦点变回文本
            input.blur(
            function () {
                var newText = $(this).val();
                objTd.html(newText);
                CityManage.SetCitySort($("#hidcityId").val(), newText);  //设置排序数字
                var taskSnId = $.trim((objTd.prev()).prev().text());
                //alert(taskSnId);//这里显示的是空的，不知道怎么获取这个id,这个idq我是绑在一个隐藏的文本框上的见html代码和aspx代码。
            });
        });
    },
    //按字段排序
    sortbyTitle: function (obj) {
        $("#talCity").empty();
        CityManage.scrolling = false;
        CityManage.itemTotal = -1;
        $('.noData').remove();
        CityManage.GetCityList(1);
    },
    sortbyNum: function (obj) {
        $("#talCity").empty();
        CityManage.scrolling = false;
        CityManage.itemTotal = -1;
        $('.noData').remove();
        CityManage.GetCityList(1);
    },
    SetCitySort: function (objId, newText) {
        var json_data = { objId: objId, newText: newText };
        $.post("/Admin/Home/SetCitySort", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Modify Score" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Modify Score" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    DelCity: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetCityIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del City" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del City" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }
    },
    editInfo: function (cityId) {
        //$("#ddlCountry li").each(function () {
        //    $("li[data-countryId=" + $("#countryId").val() + "]").addClass("selected");
        //});
        setTimeout("initCountry()", 1000);
        var json_data = { cityId: cityId };
        $.ajax({
            url: '/Admin/Home/GetCityName', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $('#CityName').attr("value", json.Message);
                }
            }
        });
    },
    save: function (type) {
        if (type == "") {
            var json_data = { direction: $("#direction").val(), cityName: $("#CityName").val(), countryId: $("#countryId").val(), score: $("#score").val() };
            $.post("/Admin/Home/AddNewCity", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add New City" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add New City" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }
        else {
            var json_data = { cityId: $("#cityId").val(), direction: $("#direction").val(), cityName: $("#CityName").val(), countryId: $("#countryId").val(), score: $("#score").val() };
            $.post("/Admin/Home/EditCity", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Edit City" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Edit City" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }

    }
};

