﻿var ibtAdmin = {
    mybox: null,
    popClose: function (obj) {
        //myboxClose.
        Boxy.get(obj).hideAndUnload();
    },
    login: function () {
        if ($('#admin_login').data('bValidator').validate() == false) {
            return;
        }

        var json_data = $("#admin_login").MySerialize();//{ pop_username: $("#txt_username").val(), pop_password: $("#txt_password").val() }
        $.post("/Admin/Home/LogOn", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    window.location = "/admin/home/AdminLoginCompleted";
                }, { title: "Admin Login" });
                $(".boxyInner").addClass("popup");             
                //location.reload();
            } else {
                Boxy.alert(json.Message, null, { title: "Admin Login" });
                $(".boxyInner").addClass("popup");
                //$("#tipMsg").html(json.Message);
            }
        }, "json");

    },
    loadUrlPop: function (url,type,json_data,funCallBack,title) {
        if (funCallBack == undefined) { funCallBack = null; }
        if (json_data == undefined) { json_data = ""; }
        if (type == undefined) { type = "Post"; }
        if (title == undefined) { title = ""; }

        $.ajax({
            url: url, data: json_data, async: false, dataType: "text", type: type, cache: false, timeout: 8000,
            success: function (txt) {
                ibtAdmin.mybox = new Boxy(txt, {
                    title: title, modal: true, center: true, unloadOnHide: true, closeText: "X", afterHide: function (e) { }, afterShow: function (r) {
                        if (typeof (funCallBack) == "function") { try { funCallBack(); } catch (e) { } }
                        //if (isIE6 == "True") {
                        //    DD_belatedPNG.fix('.jscroll-c,.jscroll-e,.jscroll-h,.jscroll-hD,.jscroll-u,.jscroll-d,icon');
                        //}
                    }
                });
                $(".boxyInner").addClass("popup");
                checkboxshow();
                selectSpan();                inputText();
		        ibtAdmin.mybox.center();
                //var box_offset = $("#").offset();
                //mybox.moveTo(box_offset.left + box_left, box_offset.top + box_top);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统繁忙，请刷新重试！");
            }
        });
    }
    ,
    /* 页面的checkbox的取值 */
    getCheckboxVal: function (selector, dataId) {
        var vals = "";
        $(selector).each(function () {
            if ($(this).hasClass("uncheckbox")) {
                vals = vals + $(this).attr(dataId) + ",";
            }
        });
        if (vals.length > 0) {
            vals = vals.substring(0, vals.length - 1);
        }
        return vals;
    }
}
$.fn.MySerialize = function () {
    var s = [];
    $('input[name], select[name], textarea[name]', this).each(function () {
        if (this.disabled || (this.type == 'checkbox' || this.type == 'radio') && !this.checked)
            return;
        if (this.type == 'select-multiple') {
            var name = this.name;
            $('option:selected', this).each(function () {
                s.push(name + '=' + encodeURIComponent(this.value));
            });
        }
        else
            s.push(this.name + '=' + encodeURIComponent(this.value));
    });
    return s.join('&');
};


