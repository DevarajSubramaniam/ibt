// JavaScript Document
$(document).ready(function () {

    // Admin - Create New Badge Special
    // Select > Option > Selected
    $('.formTypeSelection').change(function () {
        var formTypeValue = $(".formTypeSelection option:selected").val();
        //alert(formTypeValue);

        /*if(formTypeValue == 'uniqueIBT'){
            $('#expandUniqueIBT').slideDown();
            $('#expandUniqueIBT').removeClass('formTypeUnselected');
        } else {
            $('#expandUniqueIBT').slideUp();
            $('#expandUniqueIBT').addClass('formTypeUnselected');
        }*/
        $('#badgeSpecial_link').show();
        $('#badgeSpecial_point').show();
        if (formTypeValue == '2') {
            $('#badgeSpecial_link').hide();
        } 

        if (formTypeValue == 'uniqueIBT' || formTypeValue=='3') {
            if ($('expandUnqSpecificAct').hasClass('formTypeUnselected') != true) {
                $('#expandUnqSpecificAct').addClass('formTypeUnselected').slideUp(function () {
                    $('#expandUniqueIBT').slideDown();
                    $('#expandUniqueIBT').removeClass('formTypeUnselected');
                });
            } else {
                $('#expandUniqueIBT').slideDown();
                $('#expandUniqueIBT').removeClass('formTypeUnselected');
            }
            $('#badgeSpecial_link').hide();
            $('#badgeSpecial_point').hide();
        } else {
            $('#expandUniqueIBT').slideUp();
            $('#expandUniqueIBT').addClass('formTypeUnselected');
        }

        if (formTypeValue == 'uniqueSpecificAct' || formTypeValue=='4') {
            if ($('expandUniqueIBT').hasClass('formTypeUnselected') != true) {
                $('#expandUniqueIBT').addClass('formTypeUnselected').slideUp(function () {
                    $('#expandUnqSpecificAct').slideDown();
                    $('#expandUnqSpecificAct').removeClass('formTypeUnselected');
                });
            } else {
                $('#expandUnqSpecificAct').slideDown();
                $('#expandUnqSpecificAct').removeClass('formTypeUnselected');
            }
            $('#badgeSpecial_link').hide();
            $('#badgeSpecial_point').hide();
        } else {
            $('#expandUnqSpecificAct').slideUp();
            $('#expandUnqSpecificAct').addClass('formTypeUnselected');
        }

    });

    // Characters Counter
    //Short Description Counter
    var shortDescriptMax = 200;
    //$('#shortDescripLength').html(shortDescriptMax);
    $('#shortDescripTxtArea').keyup(function () {
        var longDescriptionLength = $('#shortDescripTxtArea').val().length;
        var longDescripRemaining = shortDescriptMax - longDescriptionLength;

        $('#shortDescripLength').html(longDescripRemaining);
    });

    // Long Description Counter
    var longDescriptMax = 300;
    //$('#longDescripLength').html(longDescriptMax);
    $('#longDescripTxtArea').keyup(function () {
        var longDescriptionLength = $('#longDescripTxtArea').val().length;
        var longDescripRemaining = longDescriptMax - longDescriptionLength;

        $('#longDescripLength').html(longDescripRemaining);
    });
    // Admin - Email Communications - Show List Talbe Slide
    $(".btnShowListMailContent").click(function () {
        if ($('.emailListMainContainer').hasClass('hideListTable') == true) {
            $(".emailListMainContainer").slideDown();
            $('.emailListMainContainer').addClass('showListTable');
            $('.emailListMainContainer').removeClass('hideListTable');
            $('.showListArrowBlue').addClass('showListBtnHit');
        } else if ($('.emailListMainContainer').hasClass('showListTable') == true) {
            $(".emailListMainContainer").slideUp();
            $('.emailListMainContainer').addClass('hideListTable');
            $('.emailListMainContainer').removeClass('showListTable');
            $('.showListArrowBlue').removeClass('showListBtnHit');
        }
    });

    $(".emlListNameArrowWhite").click(function () {
        if ($('.emlListNameArrowWhite').hasClass('listSortSelected') == false) {
            $('.emlListNameArrowWhite').addClass('listSortSelected');
            $('.emlListEmlAddArrowWhite').removeClass('listSortSelected');
            $('.emlListDateArrowWhite').removeClass('listSortSelected');
            $('.emlListPointsArrowWhite').removeClass('listSortSelected');
        }
    });
    $(".emlListEmlAddArrowWhite").click(function () {
        if ($('.emlListEmlAddArrowWhite').hasClass('listSortSelected') == false) {
            $('.emlListNameArrowWhite').removeClass('listSortSelected');
            $('.emlListEmlAddArrowWhite').addClass('listSortSelected');
            $('.emlListDateArrowWhite').removeClass('listSortSelected');
            $('.emlListPointsArrowWhite').removeClass('listSortSelected');
        }
    });
    $(".emlListDateArrowWhite").click(function () {
        if ($('.emlListDateArrowWhite').hasClass('listSortSelected') == false) {
            $('.emlListNameArrowWhite').removeClass('listSortSelected');
            $('.emlListEmlAddArrowWhite').removeClass('listSortSelected');
            $('.emlListDateArrowWhite').addClass('listSortSelected');
            $('.emlListPointsArrowWhite').removeClass('listSortSelected');
        }
    });
    $(".emlListPointsArrowWhite").click(function () {
        if ($('.emlListPointsArrowWhite').hasClass('listSortSelected') == false) {
            $('.emlListNameArrowWhite').removeClass('listSortSelected');
            $('.emlListEmlAddArrowWhite').removeClass('listSortSelected');
            $('.emlListDateArrowWhite').removeClass('listSortSelected');
            $('.emlListPointsArrowWhite').addClass('listSortSelected');
        }
    });

});

