﻿var supplyManager = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (supplyManager.scrolling == true) {
                    supplyManager.GetDataList(supplyManager.page);
                    supplyManager.page++;
                }
            }
        });
        if (supplyManager.itemTotal < 10) {
            $("#clickloadmore").hide();
        }
    },
    GetDateListOnClick: function () {
        if (supplyManager.scrolling == true) {
            supplyManager.GetDataList(supplyManager.page);
            supplyManager.page++;
        }
    },
    GetDataList: function (page) {
        supplyManager.scrolling = false;
        if (this.itemTotal == 0) { $("#clickloadmore").hide(); return };
        $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#tabSupply');
        var json_data = { page: page, pageSize: supplyManager.pageSize, OrderBy: $("#hidOrderTypeName").val(), DescOrAsc: $("#hidOrderValue").val(), SearchKeyword: $("#SearchKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Admin/Home/SupplierListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    supplyManager.scrolling = false;
                    supplyManager.itemTotal = json.Item.itemTotal;
                    $("#supplyManager_count").text(json.Item.total);
                    //alert(json.Message);
                    $('#tabSupply').append(json.Message);
                    $("#count").html(json.Item.itemTotal);
                    supplyManager.txtSort();
                    $('.alignC').remove();
                    checkboxshow();
                    supplyManager.scrolling = true;
                    //if ($('#tabSupply').children('tr').length == 0) {                     
                    //    $('<tr class="noData" style="text-align:center;"><td>no data!</td></tr>').appendTo('#tabSupply');
                    //}
                }
            }
        });
    },
    initData: function () {
        $("#tabSupply").empty();
        supplyManager.GetDataList(1);
        supplyManager.page = 2;
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#tabSupply").empty();//先删除以前的
        supplyManager.scrolling = false;
        supplyManager.itemTotal = -1;
        $('.noData').remove();
        supplyManager.GetDataList(1);
    },
    txtSort: function () {
        $(".cssTaskSn").click(function () {//给cssTaskSn加上click
            var objTd = $(this);//保存对象
            var oldText = $.trim(objTd.text());//保存原来的文本
            var input = $("<input id='txtorderby'  type='text' value='" + oldText + "' style='width:30px;text-align:center;' />");//定义input变量为一个文本框把原来文本写入
            objTd.html(input)//当前td变为文本框，把原来文本写入
            //设置文本框点击事件失效
            input.click(function () {
                return false;
            });
            //设置select全选文本事件,先触发焦点
            input.trigger("focus").trigger("select");

            //文本框失去焦点变回文本
            input.blur(
            function () {
                var newText = $(this).val();
                objTd.html(newText);
                supplyManager.SetSortby($("#hidSupplierId").val(), newText);  //设置排序数字
                var taskSnId = $.trim((objTd.prev()).prev().text());
                //alert(taskSnId);//这里显示的是空的，不知道怎么获取这个id,这个idq我是绑在一个隐藏的文本框上的见html代码和aspx代码。
            });
        });
    },
    //按字段排序
    sortbyTitle: function (obj) {
        $("#tabSupply").empty();
        supplyManager.scrolling = false;
        supplyManager.itemTotal = -1;
        $('.noData').remove();
        supplyManager.GetDataList(1);
    },
    sortbyScore: function (obj) {
        $("#tabSupply").empty();
        supplyManager.scrolling = false;
        supplyManager.itemTotal = -1;
        $('.noData').remove();
        supplyManager.GetDataList(1);
    },
    SetSortby: function (objId, newText) {
        var json_data = { objId: objId, newText: newText };
        $.post("/Admin/Home/SetSortby", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    location.reload();
                }, { title: "Supplier Search Score" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Supplier Search Score" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");

    },
    Del: function (objId) {
        Boxy.confirm('Do you really want to delete this article data?', function () {
            var json_data = { objId: objId };
            $.post("/Admin/Home/SetSupplierIsDel", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Supplier Remove" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Supplier Remove" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        });
        return false;
    },
    uploadAvator: function () {
        $('#uploadImage').uploadify({
            'buttonImage': '/App/images/button/file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 280,
            'uploader': '/Admin/Home/PostImgUpload?UserId=' + $("#UserId").val() + '&ObjId=' + $("#ObjId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                //alert('文件 ' + file.name + ' 已经上传成功，并返回 ' + response + ' 保存文件名称为 ' + data.SaveName);
                $("#imgPreview").removeAttr("src");
                $("#imgPreview").attr("src", "/Staticfile/Supplier/DWeek/" + "Cut_" + data.SaveName + "?" + Math.random(10000));
                $("#imgSave").attr("value", "/Staticfile/Supplier/DWeek/Des_" + data.SaveName);
            }
        });
    },
    autoComplete: function () {
        $("#SupplierName").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/Admin/Home/GetSearchName",
                    data: {
                        name: request.term
                    },
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        response($.map(data, function (item, index) {
                            //return { label: item + "(" + index + ")", id: index };
                            return { label: item["Title"] + "(" + item["ObjId"] + "-" + item["DWType"] + ")", id: item["ObjId"], type: item["DWType"], desp: item["Desp"] };
                            //return item["Title"] + "(" + item["ObjId"] + ")";

                        }));

                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#ObjId").val(ui.item.id);
                $.ajax({
                    url: "/Admin/Home/GetItemDespOrType",
                    data: { id: ui.item.id},
                    success: function (message) {
                        $("#ObjType").val(ui.item.type);
                        if (message != " " && ui.item.type == 3) {
                            $("#Desp").val(message);
                        }
                        else if (ui.item.type == 1) {
                            $("#Desp").val(ui.item.desp);
                        }
                        else if (ui.item.type == 2) {
                            $("#Desp").val(ui.item.desp);
                        }
                        else {
                            $("#Desp").val("");
                        }
                    }
                });
            }
        });
    },
    /*editsuppliermanage checkbox*/
    ckbCountry: function (obj) {
        var vals = $("#iptCategoriesIds").val();
        var attrdataid = $(obj).attr("data-attrdataid");
        if ($(obj).hasClass("uncheckbox")) {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != attrdataid) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }

            vals = res;
        } else {
            if (vals.length > 0)
                vals = vals + "," + attrdataid;
            else
                vals = attrdataid;
        }
        $("#iptCategoriesIds").val(vals);
    },
    cbkImages: function (obj) {
        var vals = $("#txtimageguids").val();
        var imgguid = $(obj).attr("data-imgguid");
        if ($(obj).hasClass("uncheckbox")) {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != imgguid) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }
            vals = res;
        } else {
            if (vals.length > 0)
                vals = vals + "," + imgguid;
            else
                vals = imgguid;
        }
        $("#txtimageguids").val(vals);
    },
    submitEditData: function () {
        // $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#tabSupply');
        var json_data = {
            supplyId: $("#txtSupplyId").val(),
            /*A&W Restaurants*/
            supplierName: encodeURI($("#txtSupplierName").val()),
            typeOfSupplier: encodeURI($("#ddlSupplieType").val()),
            parentSupplierId: $("#SponsorSupplieId").val(),
            /*Location*/
            country: $("#hidfldCountry").val(),
            city: $("#hidfldCity").val(),
            stateOrProvince: encodeURI($("#txtStateOrProvince").val()),
            address: encodeURI($("#txtAddress").val()),
            zipcode: encodeURI($("#txtInfoZip").val()),
            /*info*/
            description: encodeURI($("#txtDescription").val()),
            emailAddress: encodeURI($("#txtEmailAddress").val()),
            phone: encodeURI($("#txtPhone").val()),
            fax: encodeURI($("#txtFax").val()),
            webSite: encodeURI($("#txtWebSite").val()),
            coordsLongitude: encodeURI($("#txtCoordsLongitude").val()),
            coordsLatitude: encodeURI($("#txtCoordsLatitude").val()),
            /*categories*/
            categoriesIds: $("#iptCategoriesIds").val(),
            /*logo*/
            logoImage: $("#logo_save").val(),
            logoX1: $("#x1").val(),
            logoX2: $("#x2").val(),
            logoY1: $("#y1").val(),
            logoY2: $("#y2").val(),
            /*images*/
            images: $("#txtimageguids").val()
        };
        $.ajax({
            url: '/Admin/Home/SubmitEditDataAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert("You have successfully saved the information for this supplier!", function () {
                        location.href = "/admin/home/SupplierManage";
                        //location.reload();
                    }, { title: "Edit Supplier" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Edit Supplier" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    uploadly_logo: function () {
        $('#upload_logo').uploadify({
            'buttonImage': '/App/images/button/btn_choose_file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 190,
            'height': 22,
            'auto': true,
            'multi': false,
            'onInit': function () {
                //$("#upload_logo-queue").hide();
            },
            'uploader': '/Admin/Home/SupplierImgUpload',
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                if (data.Success) {
                    $("#preview_logo").attr("src", "/Staticfile/Supplier/SuppliesAttachment/" + data.SaveName + "_bn.jpg");
                    $("#logo_save").attr("value", data.SaveName);
                    $("#preview_logo").imgAreaSelect({
                        handles: true,
                        aspectRatio: '1:1',
                        onSelectEnd: function (img, selection) {
                            $('input[name="x1"]').val(selection.x1);
                            $('input[name="y1"]').val(selection.y1);
                            $('input[name="x2"]').val(selection.x2);
                            $('input[name="y2"]').val(selection.y2);
                        }
                    });
                } else {
                    Boxy.alert(data.Message);
                }
            }
        });
    },
    uploadly_images: function () {
        $('#upload_images').uploadify({
            'buttonImage': '/App/images/button/btn_choose_file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 190,
            'height': 22,
            'auto': true,
            'multi': true,
            'uploader': '/Admin/Home/SupplierImgagesUpload',
            'onInit': function () {
                //$("#upload_images-queue").hide();
            },
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                if (data.Success) {
                    var html = "<div style=\"float: right; border: 1px solid #E2E2E2;margin:3px 3px 3px 3px; \"><span id=\"data-imgguid" + data.SaveName + "\" onclick=\"supplyManager.cbkImages(this);\" data-imgguid=\"" + data.SaveName + "\" class=\"checkboxBig mL5 mR5 uncheckbox\"></span><img src=\"/Staticfile/Supplier/SuppliesAttachment/" + data.SaveName + "_mn.jpg\" /></div>";
                    $("#upload_end_images").append(html);
                    var vals = $("#txtimageguids").val();
                    if (vals.length > 0) {
                        vals = vals + "," + data.SaveName;
                    } else {
                        vals = data.SaveName;
                    }
                    $("#txtimageguids").val(vals);
                    //checkboxshow();
                    $("#data-imgguid" + data.SaveName).click(function () {
                        if ($(this).parents().parents().parents().parents().hasClass("messageBox")) {
                            if ($(this).hasClass("uncheckbox")) { $(this).removeClass("uncheckbox"); $(this).parents("li").removeClass("selected"); } else { $(this).addClass("uncheckbox").parents("li").addClass("selected") };
                        } else {
                            if ($(this).hasClass("uncheckbox")) {
                                $(this).removeClass("uncheckbox")
                            } else {
                                $(this).addClass("uncheckbox")
                            }
                        }
                    })
                } else {
                    Boxy.alert(data.Success);
                }
            }
        });
    },
    //batchImport: function (objId, newText) {
    //    var json_data = { objId: objId, newText: newText };
    //    $.post("/Admin/Home/GetFile", json_data, function (json) {
    //        if (json.Success) {
    //            alert(json.Message);
    //            location.reload();
    //        } else {
    //            //$("#tipMsg").html(json.Message);
    //            alert(json.Message);
    //        }
    //    }, "json");

    //},

};
var supplyEdit = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling == false) {
                supplyEdit.scrolling = true;
                return;
            }
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                supplyEdit.scrolling = true;
                supplyEdit.GetDataList(supplyEdit.page);
                supplyEdit.page++;
            }
        });
        if (supplyEdit.itemTotal < 10) {
            $("#clickloadmore").hide();
        }
    },
    GetDateListOnClick: function () {
        supplyEdit.GetDataList(supplyEdit.page);
        supplyEdit.page++;
    },
    GetDataList: function (page) {
        if (this.itemTotal == 0) { $("#clickloadmore").hide(); return };
        $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#tabSupply');
        var json_data = { page: page, pageSize: supplyEdit.pageSize, SearchKeyword: $("#SearchKeyword").val(), pageType: $("#hidpageType").val(), countryId: $("#hidfldCountry_pop").val(), cityId: $("#hidfldCity_pop").val(), cateIds: $("#hidcateIds").val(), sortBy: $("#IsShowSearch").val(), tab: $("#hidfldTab").val() };
        $.ajax({
            url: '/Admin/Home/SponsorSupplieDataAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    supplyEdit.itemTotal = json.Item.itemTotal;
                    //alert(json.Message);
                    $('#sponsorsupplielist').append(json.Message);
                    $("#count").html(json.Item.itemTotal);
                    $('.alignC').remove();
                    checkboxshow();

                    scroll_init(scroll_blue);
                    $(".jscroll-e").show();
                    //if ($('#tabSupply').children('tr').length == 0) {                     
                    //    $('<tr class="noData" style="text-align:center;"><td>no data!</td></tr>').appendTo('#tabSupply');
                    //}
                }
            }
        });
    },
    initData: function () {
        $("#tabSupply").empty();
        supplyEdit.GetDataList(1);
        supplyEdit.page = 2;
        $('.noData').remove();
        GetDataByJquery();
        initCountryCityList();
    },
    Choose: function (sid, stitle, simg) {
        //    $("#SupplieSponsor").val(sid + "," + stitle);
        $("#previewid").attr("value", sid);
        $("#previewtitle").html(stitle);
        $("#previewimgsrc").attr("src", simg);

        showPop2();
    },
    goTab: function (obj) {
        $("#sponsorsupplielist").empty();//先删除以前的
        supplyEdit.scrolling = false;
        supplyEdit.itemTotal = -1;
        $('.noData').remove();
        supplyEdit.GetDataList(1);
    },
    changeSort: function (id) {
        $("#IsShowSearch").val(id);
    }
};