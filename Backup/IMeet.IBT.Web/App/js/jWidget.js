﻿var WidgetManage = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (WidgetManage.scrolling == true) {
                    WidgetManage.GetWidgetList(WidgetManage.page);
                    WidgetManage.page++;
                }           
            }
        });
        if (WidgetManage.itemTotal < 10) {
            $("#clickloadmore").hide();
        }
    },
    GetWidgetListOnClick: function () {
        WidgetManage.GetWidgetList(WidgetManage.page);
        WidgetManage.page++;
    },
    GetWidgetList: function (page) {
        WidgetManage.scrolling = false;
        if (this.itemTotal == 0) { $("#clickloadmore").hide(); return };
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#talWidget');
        var json_data = { page: page, pageSize: WidgetManage.pageSize };
        $.ajax({
            url: '/Admin/Home/GetWidgetListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    WidgetManage.scrolling = false;
                    WidgetManage.itemTotal = json.Item.itemTotal;
                    $("#talWidget").append(json.Message);
                    //$('#talWidget').append("<tr><td class=\"alignR\" colspan=\"9\"><div class=\"controlBg control1\"><span class=\"bg\"><a href=\"javascript:void(0);\" class=\"add NborderR\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddWidgetWidget','Get','',function(){$('.jscroll-e').show();scroll_init(scroll_blueSearch2);SearchData.initData(1);SearchData.init(1);},'Search');\" ></a> </span></div></td><tr>");
                    //$("#talWidget").prepend(json.Message);
                    $('.loading').remove();
                    WidgetManage.scrolling = true;
                }
            }
        });
    },
    initData: function () {
        $("#talWidget").empty();
        WidgetManage.GetWidgetList(1);
        WidgetManage.page = 2;
        $('.noData').remove();
    },
    DelWidgetMedia: function (objId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId };
            $.post("/Admin/Home/DelWidgetMedia", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Del WidgetMedia" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del WidgetMedia" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false }
    },
    Save: function (objId) {
        if (objId == "0") {
            var json_data = { ibtType: $("#hidIbtType").val(), inputType: $("#hidInputType").val(), SupplyId: $("#hidSupplyId").val(), CountryId: $("#hidfldCountry").val(), CityId: $("#hidfldCity").val(), position: $("#hidTabId").val(), mediaType: $("#hidTypeId").val(), title: $("#txtMediaName").val(), desc: $("#txtDesc").val(), startDate: $("#txtStartDate").val(), endTime: $("#txtEndTime").val(), imgUrl: $("#txtImgUrl").val(), htmlCode: $("#txtCode").val(), htmlCode2: $("#htmlCode").val(), isLink: $("#hidIsCheck").val(), targetUrl: $("#txtTargetUrl").val() };
            $.post("/Admin/Home/AddWidgetMediaAjax", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Add New Media" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Add New Media" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }
        else {
            var json_data = { ibtType: $("#hidIbtType").val(), inputType: $("#hidInputType").val(), MediaId: $("#hidMediaId").val(), SupplyId: $("#hidSupplyId").val(), CountryId: $("#hidfldCountry").val(), CityId: $("#hidfldCity").val(), position: $("#hidTabId").val(), mediaType: $("#hidTypeId").val(), title: $("#txtMediaName").val(), desc: $("#txtDesc").val(), startDate: $("#txtStartDate").val(), endTime: $("#txtEndTime").val(), imgUrl: $("#txtImgUrl").val(), txtCode: $("#txtCode").val(), htmlCode2: $("#htmlCode").val(), isLink: $("#hidIsCheck").val(), targetUrl: $("#txtTargetUrl").val() };
            $.post("/Admin/Home/EditWidgetMediaAjax", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Edit Media" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Edit Media" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        }

    },
    uploadImg: function () {
        $('#uploadImg').uploadify({
            'buttonImage': '/App/images/button/btn_choose_file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 180,
            'height': 16,
            'auto': true,
            //'uploadLimit': 1,
            'uploader': '/Admin/Home/UploadMediaImg?supplyId=' + $("#hidsupplyId").val() + '&mediaTab=' + $('#hidTabId').val() + '&mediaType=' + $("#hidTypeId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                $("#imgPreview").removeAttr("src");
                $("#imgPreview").attr("src", "/Staticfile/Media/" + data.SaveName);
                $("#txtImgUrl").attr("value", "/Staticfile/Media/" + data.FileName);
            }
        });
    },
    autoComplete: function () {
        
        $("#SupplierName").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/Admin/Home/SearchSupplier",
                    data: {
                        name: request.term
                    },
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        response($.map(data, function (item, index) {
                            return item + "(" + index + ")";
                        }));
                    }
                });
            },
            minLength: 3
        });
    }
};

