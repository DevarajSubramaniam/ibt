﻿var supplyTypeManager = {
    itemTotal: -1,
    initData: function (typeId) {
        supplyTypeManager.uploadIcon();
        supplyTypeManager.uploadMap();
        supplyTypeManager.uploadMap2();
        var json_data = { typeId: typeId };
        $.ajax({
            url: '/Admin/Home/SupplyTypeListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $('#SupplyTypeList').append(json.Message);
                }
            }
        });
    },
    saveType: function (handleType) {   
        if (handleType == "Add") {
            var json_data = { typeName: $("#typeName").val(), attrDataId: $("#hidattrDataId").val(), attrCateId: $("#hidSaveId").val(), imgSave: $("#imgSave").val(), imgMapSave: $("#imgMapSave").val(), imgMap2Save: $("#imgMap2Save").val() };
            $.ajax({
                url: '/Admin/Home/AddNewSupplyType', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                    if (json.Success) {
                        Boxy.alert(json.Message, function () {
                            location.reload();
                        }, { title: "Add SupplyType" });
                        $(".boxyInner").addClass("popup");
                    }
                    else {
                        Boxy.alert(json.Message, null, { title: "Add SupplyType" });
                        $(".boxyInner").addClass("popup");
                    }
                }
            });
        }
        else {
            var json_data = { typeName: $("#typeName").val(), attrDataId: $("#hidattrDataId").val(), attrCateId: $("#hidSaveId").val(), imgSave: $("#imgSave").val(), imgMapSave: $("#imgMapSave").val(), imgMap2Save: $("#imgMap2Save").val() };
            $.ajax({
                url: '/Admin/Home/UpdateSupplyType', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                    if (json.Success) {
                        Boxy.alert(json.Message, function () {
                            location.reload();
                        }, { title: "Edit SupplyType" });
                        $(".boxyInner").addClass("popup");
                    }
                    else {
                        Boxy.alert(json.Message, null, { title: "Edit SupplyType" });
                        $(".boxyInner").addClass("popup");
                    }
                }
            });
        }
    },
    saveSort: function () {
        var json_data = { arrayData: $("#hidArray").val() };
        $.ajax({
            url: '/Admin/Home/SetSupplyTypeSort', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Set Sort" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Set Sort" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    saveSelType: function () {
        var json_data = { supplyId: $("#hidEditsupplyId").val(), selattrId: $("#hidSelAttrId").val() };
        $.ajax({
            url: '/Admin/Home/EditSupplyType', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        location.reload();
                    }, { title: "Select Type" });
                    $(".boxyInner").addClass("popup");
                }
                else {
                    Boxy.alert(json.Message, null, { title: "Select Type" });
                    $(".boxyInner").addClass("popup");
                }
            }
        });
    },
    editType: function (attrId, cateId) {
        $("#type_Title").html("Edit Type");
        $("#handleType").attr("value", "Edit");
        $("#hidattrDataId").attr("value", attrId);
        $("#hidSaveId").attr("value", cateId);
        $("#ddlAttrData option[value='" + $("#hidSaveId").val() + "']").attr("selected", "selected");  //1,3,4
        //alert($("#hidSaveId").val());
        var json_data = { attrId: attrId };
        $.ajax({
            url: '/Admin/Home/GetSupplyTypeAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $('#typeName').attr("value", json.Message);
                    GetIcon();
                }
            }
        });
        supplyTypeManager.uploadIcon();
        supplyTypeManager.uploadMap();
        supplyTypeManager.uploadMap2();
    },
    viewTitle: function (supplyId) {
        var json_data = { supplyId: supplyId };
        $.ajax({
            url: '/Admin/Home/GetSupplyName', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $("#EditSortTitle").html(json.Message);
                    $("#EditSupplyName").html(json.Message);
                }
            }
        });
    },
    GetPopType: function (objId, id) {
        $("#hidsupplyId").attr("value", objId);
     
        switch (id) {
            case 2:
                var json_data = { objId: objId };
                $.ajax({
                    url: '/Admin/Home/SupplyTypeListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                        if (json.Success) {
                            $('#TypeList').append(json.Message);
                            $("#hidEditsupplyId").attr("value", objId);
                            //alert($("#hidEditsupplyId").val());
                        }
                    }
                });
                break;
            default: //default 1
                var json_data = { objId: objId };
                $.ajax({
                    url: '/Admin/Home/SupplySortListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                        if (json.Success) {
                            $('#tableSort').append(json.Message);
                            supplyManager.itemTotal = json.Item.itemTotal;
                            if (supplyManager.itemTotal == 0) {
                                $('<tr class="noData"><td>There are currently no category assigned to this supplier</td><td>0</td></tr>').appendTo('#tableSort');
                            }
                        }
                    }
                });
                break;
        }

    },
    uploadIcon: function () {
        $('#uploadTypeIcon').uploadify({
            'buttonImage': '/App/images/button/file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 280,
            'uploader': '/Admin/Home/PostIconUpload?UserId=' + $("#UserId").val() + '&AttrDataId=' + $("#hidattrDataId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                //alert('文件 ' + file.name + ' 已经上传成功，并返回 ' + response + ' 保存文件名称为 ' + data.SaveName);
                $("#imgIconPreview").removeAttr("src");
                $("#imgIconPreview").attr("src", "/Staticfile/Supplier/Icon/" + "Cut_" + data.SaveName);
                $("#imgSave").attr("value", "/Staticfile/Supplier/Icon/" + data.SaveName);
            }
        });
    },
    uploadMap: function () {
        $('#uploadTypeMap').uploadify({
            'buttonImage': '/App/images/button/file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 280,
            'uploader': '/Admin/Home/PostMapUpload?UserId=' + $("#UserId").val() + '&AttrDataId=' + $("#hidattrDataId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                //alert('文件 ' + file.name + ' 已经上传成功，并返回 ' + response + ' 保存文件名称为 ' + data.SaveName);
                $("#imgMapPreview").removeAttr("src");
                $("#imgMapPreview").attr("src", "/Staticfile/Supplier/Icon/" + "Cut_" + data.SaveName);
                $("#imgMapSave").attr("value", "/Staticfile/Supplier/Icon/" + data.SaveName);
            }
        });
    },
    uploadMap2: function () {
        $('#uploadTypeMap2').uploadify({
            'buttonImage': '/App/images/button/file.gif',
            'swf': '/App/js/uploadify/uploadify.swf',
            'cancelImg': '/App/js/uploadify/uploadify-cancel.png',
            'fileTypeDesc': '图片文件',
            'fileTypeExts': '*.jpg;*.jpeg;*.png;*.gif',
            'width': 280,
            'uploader': '/Admin/Home/PostMap2Upload?UserId=' + $("#UserId").val() + '&AttrDataId=' + $("#hidattrDataId").val() + "&d=" + (new Date()).getTime(),
            'onUploadSuccess': function (file, data, response) {
                eval("data=" + data);
                //alert('文件 ' + file.name + ' 已经上传成功，并返回 ' + response + ' 保存文件名称为 ' + data.SaveName);
                $("#imgMap2Preview").removeAttr("src");
                $("#imgMap2Preview").attr("src", "/Staticfile/Supplier/Icon/" + "Cut_" + data.SaveName);
                $("#imgMap2Save").attr("value", "/Staticfile/Supplier/Icon/" + data.SaveName);
            }
        });
    }
};
