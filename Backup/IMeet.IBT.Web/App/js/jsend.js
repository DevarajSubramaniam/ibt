﻿var adminSendPwd = {
    send: function () {
        if ($('#forgotForm').data('bValidator').validate() == false) {
            return;
        }
        var json_data = $("#forgotForm").MySerialize();
        $.post("/Account/SendPassword", json_data, function (json) {
            if (json.Success) {
                Boxy.alert(json.Message, function () {
                    window.location = "/admin/home/AdminLogin";
                }, { title: "Forgot Password" });
                $(".boxyInner").addClass("popup");

            } else {
                Boxy.alert(json.Message, null, { title: "Forgot Password" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
       
    }
};

