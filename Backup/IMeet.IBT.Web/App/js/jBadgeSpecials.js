﻿var jBadgeSpecials = {
    posting: false,
    canScrolling: true,
    page: 1,
    pagesize: 20,
    orderby: "",
    sortord:"asc",
    total: 0,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (jBadgeSpecials.posting == false && jBadgeSpecials.canScrolling) {
                    jBadgeSpecials.nextpage();
                }
            }
        });
    },
    list: function () {
        if (!jBadgeSpecials.posting) {
            jBadgeSpecials.posting = true;
            $('<div class="alignC"><img src="/App/images/icon/loading.gif" alt="Loading" /></div>').appendTo('#adminContent');
            $.ajax({
                url: '/admin/home/badgespecialslist',
                data: { page: jBadgeSpecials.page, pagesize: jBadgeSpecials.pagesize, orderby: jBadgeSpecials.orderby,sortord:jBadgeSpecials.sortord },
                type: 'POST',
                dataType: 'json',
                success: function (json) {
                    $('#adminContent').append(json.Message);
                    jBadgeSpecials.posting = false;
                    if (json.Item <= (jBadgeSpecials.page * jBadgeSpecials.pagesize)) {
                        jBadgeSpecials.canScrolling = false;
                    }
                    $('.alignC').remove();
                },
                error: function () {
                    jBadgeSpecials.posting = false;
                    alert("server error");
                }
            });
        }
    },
    nextpage: function () {
        if (!jBadgeSpecials.posting) {
            jBadgeSpecials.page++;
            jBadgeSpecials.list();
        }
    },
    orderbyPage: function (jb_orderby) {
        if (jBadgeSpecials.posting)
            return false;
        jBadgeSpecials.page = 1;
        jBadgeSpecials.orderby = jb_orderby;
        $("#adminContent .adnCntRow").remove();
        jBadgeSpecials.list();
    },
    del: function (id) {
        Boxy.ask("Are you sure you want to permanently delete this Special?", ["YES", "NO"], function (val) {
            if (val == "YES") {
                $.ajax({
                    url: '/admin/home/badgespecialdel',
                    data: { id: id },
                    type: 'post',
                    dataType: 'json',
                    success: function (json) {
                        location.reload();
                    },
                    error: function () {
                        Boxy.alert("server error");
                    }
                });
            }
        }, { title: "Delete Badge Special" });
        $(".boxyInner").addClass("popup");
    },
    sortby: function (obj) {
        $.ajax({
            url: '/admin/home/badgespecialsortby',
            data: { pid: $(obj).attr('data-pid'), id: $(obj).attr('data-id') },
            type: 'post',
            dataType: 'json',
            success: function (json) {
                location.reload();
            }
        });
    }
}