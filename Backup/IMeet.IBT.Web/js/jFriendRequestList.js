﻿var requestList = {
    page:1,
    scrolling:false,
    pageSize:10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (requestList.scrolling == true) {
                    requestList.GetDataList(requestList.page);
                    requestList.page++;
                }
            }
        });
    },
    GetDataList: function (page) {
        requestList.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#requestList');
        var json_data = { userId:$("#userId").val(),  page: page, pageSize: requestList.pageSize, filterLetter: $("#filterLetter").val(), filterKeyword: $("#filterKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Account/RequestListAjx/', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    requestList.scrolling = false;
                    requestList.itemTotal = json.Item.itemTotal;
                    $("#requestList_count").text(json.Item.total);
                    $('#requestList').append(json.Message);
                    $('.loading').remove();
                    checkboxshow();
                    if ($('#requestList').children('li').length == 0) {
                        $('<div class="noData">no data!</div>').appendTo('#requestList');
                    }
                    requestList.scrolling = true;
                }
            }
        });
    },
    initData: function () {
        $("#requestList").empty();
        requestList.GetDataList(1);
        $("#filterLetter").val("");
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#requestList").empty();//先删除以前的
        requestList.scrolling = false;
        requestList.itemTotal = -1;
        $("#filterLetter").val("");
        $('.noData').remove();
        requestList.GetDataList(1);
    },
    //快速搜索
    quickSearch: function (obj, letter) {
        $("#requestList").empty();
        $('.noData').remove();
        requestList.scrolling = false;
        requestList.itemTotal = -1;

        if ($("#filterLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#filterLetter").val("");
            requestList.GetDataList(1);
        } else {
            $("#quickSearch a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#filterLetter").val(letter);
            requestList.GetDataList(1);
        }
    }
}