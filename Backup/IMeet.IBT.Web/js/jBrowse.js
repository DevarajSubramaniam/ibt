﻿var jbrowse = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (jbrowse.scrolling == true) {
                    jbrowse.myBrowse(jbrowse.page);
                    jbrowse.page++;
                }
            }
        });
    },
    GetCountryListOnClick: function () {
        if (jbrowse.scrolling == true) {
            jbrowse.myBrowse(jbrowse.page);
            jbrowse.page++;
        }
    },
    initData: function () {
        $('#browseList').empty();
        jbrowse.myBrowse(1);
        jbrowse.page = 2;
    },
    IBTRating: function () {
        $('.IBTRating').raty({
            readOnly: false,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                //ibt.rrwb_rating($(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
            }
        });
    },
    myBrowse: function (page) {
        jbrowse.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#browseList');
        var json_data = { page: page, pageSize: jbrowse.pageSize, filterKeyword: $("#hidkeyword").val(), saveLetter: $("#saveLetter").val() };
        $.ajax({
            url: '/Search/BrowseListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    jbrowse.scrolling = false;
                    jbrowse.itemTotal = json.Item.itemTotal;


                    $("#browseTitle").html("Browse by country<span class=\"txt\">About <span id=\"country_count\">-</span> Country in the world</span>");
                    $("#country_count").text(json.Item.total);
                    $('#browseList').append(json.Message);
                    if (json.Item.itemTotal >= jbrowse.pageSize) {
                        $("#clickloadmore_browse").show();
                    } else {
                        $("#clickloadmore_browse").hide();
                    }
                    $('.loading').remove();

                    ibt.ibtRating();
                    checkboxshow();
                    if ($('#browseList').children('li').length == 0) {
                        $('<div class="noData">Currently, no Data!</div>').appendTo('#browseList');
                    }
                    jbrowse.scrolling = true;
                }
            }
        });
    },
    letterSearch: function (obj, letter) {
        $("#browseList").empty();
        $('.noData').detach();
        jbrowse.scrolling = false;
        jbrowse.itemTotal = -1;
        jbrowse.page = 2;
        if ($("#saveLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#saveLetter").val("");
            jbrowse.myBrowse(1);
        } else {
            $("#letterFilter a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#saveLetter").val(letter);
            jbrowse.myBrowse(1);
        }
    }
}