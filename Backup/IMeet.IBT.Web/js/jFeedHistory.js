﻿var feedHistory = {
    ShowFeedHistory: function (tabId, feedId, ibtType, objId, userId) {
        var txt = "close history";
        var txt2 = "view history";

        if ($("#feedHistoryList_" + feedId).hasClass("show")) {
            //$(".feed .history .sliderD").text(txt2);
            $(".feed .history #feedHistoryTxt_" + feedId).text(txt2);
            $("#feedHistoryList_" + feedId).removeClass("show");
            $("#feedHistoryList_" + feedId).hide();
            $("#feedHistoryList_" + feedId).empty();
            return;
        }
        //ajax load list
        var json_data = { tabId: tabId, feedId: feedId, ibtType: ibtType, objId: objId, userId: userId };
        $.post("/Supplier/getFeedPostVisitHistoryList", json_data, function (json) {
            $("#feedHistoryList_" + feedId).html(json.Message);
            selectSpan();
        }, "json");
        //$(".feed .history .sliderD").text(txt);
        $(".feed .history #feedHistoryTxt_" + feedId).text(txt);
        $("#feedHistoryList_" + feedId).show();
        $("#feedHistoryList_" + feedId).addClass("show");
    },
    ShowFeedDate: function (ibtType) {
        document.getElementById("letterBar").style.display = "none ";
        document.getElementById("tabMain").style.display = "block ";
        var json_data = { userId: $("#feed_UserId").val(), ibtType: ibtType };
        $.post("/Supplier/getFeedDateList", json_data, function (json) {
            $("#tabMain").html(json.Message);
        }, "json");
    }
}
var jTravelFeed = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (jTravelFeed.scrolling == true) {
                    jTravelFeed.myFeed(jTravelFeed.page);
                    jTravelFeed.page++;
                }
            }
        });
    },
    myFeed: function (page) {
        //与数据库的分类对应
        var iptCatId = $("#categoryId").val();
        var categoryId = 2;
        if (iptCatId == 3) {
            categoryId = 1;
        } else if (iptCatId == 4) {
            categoryId = 3;
        } else if (iptCatId == -1) {
            categoryId = -1;
        } else if (iptCatId == -2) {
            categoryId = -2;
        }else if (iptCatId == 43) {
            categoryId = 43;
        }else {
            categoryId = 4;
        }
        jTravelFeed.scrolling = false;
        //end:与数据库的分类对应
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#feedList');
        var json_data = { userId: $("#feed_UserId").val(), yearMonth: $("#yearMonth").val(), categoryId: categoryId, type: $("#IsShowMyProfile").val(), page: page, pageSize: this.pageSize, filterLetter: $("#filterLetter").val(), sortByRating: $("#IsSortRating").val() };
        $.ajax({
            url: '/Supplier/getFeedDataList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {

                    jTravelFeed.scrolling = false;
                    jTravelFeed.itemTotal = json.Item.itemTotal;
                    $('#feedList').append(json.Message);
                    $("textarea").focus(function () {
                        var val = $(this).val();
                        if (val === $(this).val()) { $(this).val("") }
                    }).blur(function () {
                        if ($(this).val() == "") { $(this).val("Write a message...") }
                    });
                    $('.loading').remove();
                    jTravelFeed.FeedRating();
                    //feedSlider();
                    selectSpan();
                    if (page == 1 && json.Item.itemTotal == 0) {
                        $('<div class="noData">Currently, you have not added any places to this list! Add them to your list by clicking on Fast Feedback!</div>').appendTo('#feedList');
                    }
                    jTravelFeed.scrolling = true;
                }
            }
        });
    },
    initData: function (tabId) {
        //if (tabId == 1) {
        $("#feedList").empty();
        this.myFeed(1);
        jTravelFeed.tabId = tabId;
        this.page = 2;
        //}
    },
    FeedRating: function () {
        $('.FeedRating').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                //ibt.rrwb_rating($(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
            }
        });
    },
    DelPVNewsfeed: function (feedId, objId, ibtId) {
        if (confirm('Do you really want to delete this article data?')) {
            var json_data = { objId: objId, ibtId: ibtId };
            $.post("/Supplier/DelPVNewsfeed", json_data, function (json) {
                if (json.Success) {
                    Boxy.alert(json.Message, function () {
                        jTravelFeed.ShowIBTCount(feedId, objId);
                        if (feedId == objId) { location.reload(); }
                    }, { title: "Del Newsfeed" });
                    $(".boxyInner").addClass("popup");
                } else {
                    Boxy.alert(json.Message, null, { title: "Del Newsfeed" });
                    $(".boxyInner").addClass("popup");
                }
            }, "json");
        } else { return false; }
    },
    ShowIBTCount: function (feedId, objId) {
        var num = $("#feedIBTCount_" + feedId).attr("data-ibtcount"); //ibtcount
        var n = parseInt(num);
        var result = n - 1;
        $("#feedIBTCount_" + feedId).html(result);
        $("#HistoryItem_" + objId).empty();
    },
    //快速搜索
    quickSearch: function (obj, letter) {
        $("#feedList").empty();
        $('.noData').remove();
        jTravelFeed.scrolling = false;
        jTravelFeed.itemTotal = -1;
        jTravelFeed.page = 2;
        if ($("#filterLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#filterLetter").val("");
            jTravelFeed.myFeed(1);
        } else {
            $("#quickSearch a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#filterLetter").val(letter);
            jTravelFeed.myFeed(1);
        }
    },
    dateSearch: function (obj, yearMonth) {
        $("#feedList").empty();
        $('.noData').remove();
        jTravelFeed.scrolling = false;
        jTravelFeed.itemTotal = -1;
        jTravelFeed.page = 2;
        if ($("#yearMonth").val() == yearMonth) {
            $(obj).removeClass("btnColorB IERound");
            $(obj).addClass("btnColorG IERound");
            $("#yearMonth").val("");
            jTravelFeed.myFeed(1);
        } else {
            $("#tabMain a").each(function () {
                $(this).removeClass("btnColorB IERound");
                $(this).addClass("btnColorG IERound");
            });
            $(obj).removeClass("btnColorG IERound");
            $(obj).addClass("btnColorB IERound");
            $("#yearMonth").val(yearMonth);
            jTravelFeed.myFeed(1);
        }
    }
}