﻿var addTravelsMaps = {
    prevZoom: 2,
    map: null,
    source: null,// ['Manly Beach', -33.80010128657071, 151.28747820854187,1,1683, '/images/icon/restaurant2.png'], title,lan,lon,ibttype,id,icon
    userZoom: true,
    markersArray: [],
    UserId: 0,
    init: function (locations) {
        var initLatlng = new google.maps.LatLng(locations[0].lat, locations[0].lng);
        var mapOptions = {
            zoom: 2,
            minZoom: 2,
            maxZoom: 19,
            center: initLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        addTravelsMaps.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        addTravelsMaps.setMarkers(addTravelsMaps.map, locations);

        //zoom
        google.maps.event.addListener(addTravelsMaps.map, 'zoom_changed', function () {
            if (addTravelsMaps.userZoom) {
                addTravelsMaps.userZoom = false;
                var _lat = addTravelsMaps.map.getCenter().lat();
                var _lng = addTravelsMaps.map.getCenter().lng();

                var mapType = 1; //default country, >4 is city,>8 is supply
                if (addTravelsMaps.map.getZoom() <= 4) {
                    if (addTravelsMaps.prevZoom > 4) {
                        mapType = 1;
                        var json_data = { mapType: mapType, userId: addTravelsMaps.UserId, lat: _lat, lng: _lng, iptEpoch: $("#iptEpoch").val() };
                        $.ajax({
                            url: "/map/GetaddTravelsMapsList", data: json_data, success: function (json) {
                                addTravelsMaps.clearOverlays();
                                addTravelsMaps.setMarkers(addTravelsMaps.map, json.Item);
                            }, dataType: "json", async: false, cache: true
                        });
                    }
                }
                if (addTravelsMaps.map.getZoom() >= 8) {
                    if (addTravelsMaps.prevZoom < 8) {
                        mapType = 3;
                        var json_data = { mapType: mapType, userId: addTravelsMaps.UserId, lat: _lat, lng: _lng, iptEpoch: $("#iptEpoch").val() };
                        $.ajax({
                            url: "/map/GetaddTravelsMapsList", data: json_data, success: function (json) {
                                addTravelsMaps.clearOverlays();
                                addTravelsMaps.setMarkers(addTravelsMaps.map, json.Item);
                            }, dataType: "json", async: false, cache: true
                        });
                    }
                } else if (addTravelsMaps.map.getZoom() > 4 && addTravelsMaps.map.getZoom() < 8) {
                    if (addTravelsMaps.prevZoom <= 4 || addTravelsMaps.prevZoom >= 8) {
                        mapType = 2;
                        var json_data = { mapType: mapType, userId: addTravelsMaps.UserId, lat: _lat, lng: _lng, iptEpoch: $("#iptEpoch").val() };
                        $.ajax({
                            url: "/map/GetaddTravelsMapsList", data: json_data, success: function (json) {
                                addTravelsMaps.clearOverlays();
                                addTravelsMaps.setMarkers(addTravelsMaps.map, json.Item);
                            }, dataType: "json", async: false, cache: true
                        });
                    }
                }
            }
            addTravelsMaps.prevZoom = addTravelsMaps.map.getZoom();
            addTravelsMaps.userZoom = true;
        });
        //end:zoom
    },
    setMarkers: function (map, locations) {
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var image = new google.maps.MarkerImage(beach.icon);//beach[4]
            var myLatLng = new google.maps.LatLng(beach.lat, beach.lng);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                title: beach.Title,
                zIndex: null,
                customId: beach.Id,
                typ: beach.typ
            });
            addTravelsMaps.markersArray.push(marker);
            addTravelsMaps.setMarkerClick(map, marker);
            
        } //end:for
    },
    setMarkerClick: function (map, marker) {
        google.maps.event.addListener(marker, "click", function (e) {

            var _lat = marker.getPosition().lat();
            var _lng = marker.getPosition().lng();
            addTravelsMaps.clearOverlays();

            if (map.getZoom() < 9 && marker.typ != 3) {
                if (map.getZoom() <= 4) {
                    mapType = 2;
                    map.setZoom(5);
                }
                else {
                    mapType = 3;
                    map.setZoom(9);
                }
            } else {
                mapType = 1;
            }
            
            map.setCenter(marker.getPosition());
                var json_data = { mapType: mapType, userId: addTravelsMaps.UserId, lat: _lat, lng: _lng, iptEpoch: $("#iptEpoch").val() };
                $.ajax({
                    url: "/map/GetaddTravelsMapsList", data: json_data, success: function (json) {
                        if (json.Success) {
                            addTravelsMaps.setMarkers(map, json.Item);
                        }
                    }, dataType: "json", async: false
                });
        });
    },
    clearOverlays: function () {
        for (var i = 0; i < addTravelsMaps.markersArray.length; i++) {
            addTravelsMaps.markersArray[i].setMap(null);
        }
    }
    , getData: function (typ, lat, lng) {
        var json_data = {userId: addTravelsMaps.UserId, lat: lat, lng: lng, iptEpoch: $("#iptEpoch").val() };
        $.ajax({
            url: "/map/GetaddTravelsMapsList", data: json_data, success: function (json) {
                return json;
            }, dataType: "json", async: false
        });
    }
};