﻿var jServices = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    tabId: 1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (jServices.scrolling == true) {
                    jServices.myFeed(jServices.page);
                    jServices.page++;
                }
            }
        });
    },
    ShowFeedDate: function (ibtType) {
        document.getElementById("letterBar").style.display = "none ";
        document.getElementById("tabMain").style.display = "block ";
        var json_data = { userId: $("#feed_UserId").val(), ibtType: ibtType };
        $.post("/Supplier/getServicesDate", json_data, function (json) {
            $("#tabMain").html(json.Message);
        }, "json");
    },
    getFeedList: function () {
        if (jServices.scrolling == true) {
            jServices.myFeed(jServices.page);
            jServices.page++;
        }
    },
    myFeed: function (page) {
        //与数据库的分类对应
        var iptCatId = $("#categoryId").val();
        var categoryId = 2;
        if (iptCatId == 3) {
            categoryId = 1;
        } else if (iptCatId == 4) {
            categoryId = 3;
        } else if (iptCatId == -1) {
            categoryId = -1;
        } else if (iptCatId == -2) {
            categoryId = -2;
        }else if (iptCatId == 43) {
            categoryId = 43;
        }else {
            categoryId = 4;
        }
        jServices.scrolling = false;
        //end:与数据库的分类对应
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#feedList');
        var json_data = { userId: $("#feed_UserId").val(), tabId: 3, categoryId: categoryId, type: $("#IsShowMyProfile").val(), page: page, pageSize: this.pageSize, filterLetter: $("#filterLetter").val(), sortByRating: $("#IsSortRating").val() };
        $.ajax({
            url: '/Supplier/ServicesList', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {

                    jServices.scrolling = false;
                    jServices.itemTotal = json.Item.itemTotal;
                    $('#feedList').append(json.Message);
                    $("textarea").focus(function () {
                        var val = $(this).val();
                        if (val === $(this).val()) { $(this).val("") }
                    }).blur(function () {
                        if ($(this).val() == "") { $(this).val("Write a message...") }
                    });
                    if (json.Item.itemTotal >= jServices.pageSize) {
                        $("#clickloadmore_travels").show();
                    } else {
                        $("#clickloadmore_travels").hide();
                    }
                    $('.loading').remove();
                    jServices.FeedRating();
                    selectSpan();
                    if (page == 1 && json.Item.itemTotal == 0) {
                        $('<div class="noData">Currently, you have not rated any services yet.</div>').appendTo('#feedList');
                    }
                    jServices.scrolling = true;
                }
            }
        });
    },
    initData: function (tabId) {
        //if (tabId == 1) {
        $("#feedList").empty();
        this.myFeed(1);
        jServices.tabId = tabId;
        this.page = 2;
        //}
    },
    FeedRating: function () {
        $('.FeedRating').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                //ibt.rrwb_rating($(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
            }
        });
    },
   
    //快速搜索
    quickSearch: function (obj, letter) {
        $("#feedList").empty();
        $('.noData').remove();
        jServices.scrolling = false;
        jServices.itemTotal = -1;
        jServices.page = 2;
        if ($("#filterLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#filterLetter").val("");
            jServices.myFeed(1);
        } else {
            $("#quickSearch a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");
            $("#filterLetter").val(letter);
            jServices.myFeed(1);
        }
    }
}