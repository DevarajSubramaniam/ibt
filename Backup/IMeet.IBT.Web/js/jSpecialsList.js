﻿var specialsList = {
    page:1,
    scrolling:false,
    pageSize:10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (specialsList.scrolling == true) {
                    specialsList.GetDataList(specialsList.page);
                    specialsList.page++;
                }
            }
        });
    },
    GetSpecListOnClick: function () {
        if (specialsList.scrolling == true) {
            specialsList.GetDataList(specialsList.page);
            specialsList.page++;
        }
    },
    GetDataList: function (page) {
        specialsList.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#specialsList');
        var json_data = { page: page, pageSize: specialsList.pageSize, filterSort: $("#filterSort").val(), filterKeyword: $("#filterKeyword").val(), filterField: $("#filterField").val() };
        $.ajax({
            url: '/Supplier/specialsListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    specialsList.scrolling = false;
                    specialsList.itemTotal = json.Item.itemTotal;
                    $("#specialsList_count").text(json.Item.total);
                    $('#specialsList').append(json.Message);
                    if (json.Item.itemTotal >= specialsList.pageSize) {
                        $("#clickloadmore_spec").show();
                    } else {
                        $("#clickloadmore_spec").hide();
                    }
                    $('.loading').remove();
                    checkboxshow();
                    if ($('#specialsList').children('li').length == 0) {
                        $('<div class="noData">no data!</div>').appendTo('#specialsList');
                    }
                    specialsList.scrolling = true;
                }
            }
        });
    },
    SortByItem: function (tabId) {
        if (tabId.toString() == "0") {
            $("#filterSort").val("Date");
        }  else {
            $("#filterSort").val("Alphabet");
        }
        this.initData();
    },
    initData: function () {
        $("#specialsList").empty();
        specialsList.GetDataList(1);
        specialsList.page = 2;
        $("#filterLetter").val("");
        $('.noData').remove();
    },
    goTab: function (obj) {
        $("#specialsList").empty();//先删除以前的
        specialsList.scrolling = false;
        specialsList.itemTotal = -1;
        $('.noData').remove();
        specialsList.GetDataList(1);
    }
}