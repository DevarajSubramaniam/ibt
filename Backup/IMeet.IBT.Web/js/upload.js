﻿var upload = {
    PvPopLoad: function () {
        $('#upfile').uploadify({
            'formData': {
                'guid': $("#pvGuid").val(),
                'supplyId': $("#pvSupplyId").val()
            },
            'swf': '/js/uploadify/uploadify.swf',
            'uploader': '/Account/Upload',
            'buttonImage': '/images/icon/upload_imageIcon.jpg',
            'fileDesc': '*.jpg;*.jpeg;*.png;*.gif',
            'fileExt': '*.jpg;*.jpeg;*.png;*.gif',
            'height': 282,
            'width': 282,
            onUploadSuccess: function (file, data, response) {
                var json = eval('(' + data + ')');
                $("#imgAvatar").attr("src", "/Staticfile/Avatar/tmp/" + data.SaveName + "?" + Math.random(10000));
                // $("#imgAvatar").append("<a href=\"javascript:void(0);\" ><img src=\"" + json.Message + "\" /></a>");
            }
        });
    }
};

