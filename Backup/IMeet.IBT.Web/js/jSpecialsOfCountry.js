﻿var specialsOfCountry = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (specialsOfCountry.scrolling == true) {
                    specialsOfCountry.GetDataList(specialsOfCountry.page);
                    specialsOfCountry.page++;
                }
            }
        });
    },
    GetDataList: function (page) {
        specialsOfCountry.scrolling = false;
        if (this.itemTotal == 0) return;
        $('<div class="loading"><img src="/images/icon_waiting.gif"/>Wait a moment... it\'s loading!</div>').appendTo('#offerList');
        var json_data = { page: page, pageSize: specialsOfCountry.pageSize, countryId: $("#countryId").val() };
        $.ajax({
            url: '/IBT/specialsListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    specialsOfCountry.scrolling = false;
                    specialsOfCountry.itemTotal = json.Item.itemTotal;
                    $('#offerList').append(json.Message);
                    $('.loading').remove();
                    if ($('#offerList').children('li').length == 0) {
                        $('<div class="noData">no data!</div>').appendTo('#offerList');
                    }
                    specialsOfCountry.scrolling = true;
                }
            }
        });
    },
    initData: function () {
        $("#offerList").empty();
        specialsOfCountry.GetDataList(1);
        specialsOfCountry.page = 2;
        $('.noData').remove();
    }
}