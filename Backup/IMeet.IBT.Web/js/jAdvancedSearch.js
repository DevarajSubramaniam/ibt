﻿var advancedSearch = {
    page: 1,
    scrolling: false,
    isLastPage:false,
    pageSize: 10,
    itemTotal: -1,
    total: -1,
    init: function () {
        $("#clickloadmore_filterList").hide();
        $('#clickloadmore_searchList').hide();
        $(window).scroll(function () {
            if (this.scrolling) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (!advancedSearch.scrolling  && !advancedSearch.isLastPage) {
                    if ($("#listType").val() == "Country") {
                        advancedSearch.GetCountryList(advancedSearch.page);
                    }
                    else if ($("#listType").val() == "City") {
                        advancedSearch.GetCityList(advancedSearch.page);
                    }
                    else if ($("#listType").val() == "Supplier") {
                        advancedSearch.GetSupplyList(advancedSearch.page);

                    }
                    else {
                        advancedSearch.GetMemberList(advancedSearch.page);
                    }
                    advancedSearch.page++;
                }
            }
        });
    },
    GetSearchListOnClick: function () {
        if (advancedSearch.scrolling == false) {
            if ($("#listType").val() == "Country") {
                advancedSearch.GetCountryList(advancedSearch.page);
            }
            else if ($("#listType").val() == "City") {
                advancedSearch.GetCityList(advancedSearch.page);
            }
            else if ($("#listType").val() == "Supplier") {
                advancedSearch.GetSupplyList(advancedSearch.page);
            }
            else {
                advancedSearch.GetMemberList(advancedSearch.page);
            }
            advancedSearch.page++;
        }
    },
    IBTRating: function () {
        $('.IBTRating').raty({
            readOnly: false,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                //ibt.rrwb_rating($(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
            }
        });
    },
    GetCountryList: function (page) {
        if (this.itemTotal == 0) return;
        if (advancedSearch.scrolling) return;
        advancedSearch.scrolling = true;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#filterList');
        var json_data = { page: page, pageSize: advancedSearch.pageSize, countryId: $("#rs_hidcountryId").val(), saveLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/CoutryListAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    advancedSearch.itemTotal = json.Item.itemTotal;
                    document.getElementById("feedDiv").style.display = "block";
                    document.getElementById("searchDiv").style.display = "none";
                    if (json.Item.total == 0) {
                        document.getElementById("feedDiv").style.display = "none";
                    }
                    $("#hidcount").attr("value", json.Item.total);
                    $("#advancedSearch_count").text(json.Item.total);

                    $('#filterList').append(json.Message);
                    if (json.Item.itemTotal >= advancedSearch.pageSize) {
                        //$("#clickloadmore_filterList").show();
                    } else {
                        $("#clickloadmore_filterList").hide();
                        advancedSearch.isLastPage = true;
                    }
                    $('.loading').remove();
                    ibt.ibtRating();
                    checkboxshow();
                }
                advancedSearch.scrolling = false;
            }, error: function () { advancedSearch.scrolling = false; }
        });
    },
    GetCityList: function (page) {
        if (this.itemTotal == 0) return;
        if (advancedSearch.scrolling) return;
        advancedSearch.scrolling = true;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#filterList');
        var json_data = { page: page, pageSize: advancedSearch.pageSize, countryId: $("#rs_hidcountryId").val(), cityId: $("#rs_hidcityId").val(), filterLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/CitySearchAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    advancedSearch.itemTotal = json.Item.itemTotal;
                    if (json.Item.total == 0) {
                        document.getElementById("feedDiv").style.display = "none";
                    }
                    else {
                        document.getElementById("feedDiv").style.display = "block";
                    }
                    document.getElementById("searchDiv").style.display = "none";
                    $("#hidcount").attr("value", json.Item.total);
                    $("#advancedSearch_count").text($("#hidcount").val());
                    //$("#SearchCity_count").text(json.Item.total);
                    $('#filterList').append(json.Message);
                    if (json.Item.itemTotal >= advancedSearch.pageSize) {
                        //$("#clickloadmore_filterList").show();
                    } else {
                        //$("#clickloadmore_filterList").hide();
                        advancedSearch.isLastPage = true;
                    }
                    $('.loading').remove();
                    advancedSearch.IBTRating();
                    checkboxshow();
                }
                advancedSearch.scrolling = false;
            },error: function () { advancedSearch.scrolling = false; }
        });
    },
    CityInitCount: function (page) {
        var json_data = { page: page, pageSize: advancedSearch.pageSize, countryId: $("#rs_hidcountryId").val(), cityId: $("#rs_hidcityId").val(), filterLetter: $("#filterLetter").val(), filterKeyword: $("#hidkeyword").val() };
        $.ajax({
            url: '/Search/CitySearchAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $("#SearchCity_count").empty();
                    $("#SearchCity_count").text(json.Item.total);
                    $("#hidtab2count").attr("value", json.Item.total);  //初始化保存数据
                    if (json.Item.total == 0) {
                        document.getElementById("feedDiv").style.display = "none";
                    }
                }
            }
        });
    },
    GetSupplyList: function (page) {
        if (this.itemTotal == 0) return;
        if (advancedSearch.scrolling) return;
        advancedSearch.scrolling = true;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchList');
        var json_data = { page: page, pageSize: advancedSearch.pageSize, countryId: $("#rs_hidcountryId").val(), cityId: $("#rs_hidcityId").val(), cateIds: $("#hidcateIds").val(), sortBy: $("#hidsortby").val(), filterLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/SupplySearchAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    advancedSearch.itemTotal = json.Item.itemTotal;
                    $("#hidcount").attr("value", json.Item.total);
                    $("#advancedSearch_count").text(json.Item.total);
                    $("#SearchSupplier_count").text(json.Item.total);
                    $('#searchList').append(json.Message);
                    if (json.Item.itemTotal >= advancedSearch.pageSize) {
                        //$("#clickloadmore_searchList").show();
                    } else {
                        //$("#clickloadmore_searchList").hide();
                        advancedSearch.isLastPage = true;
                    }
                    $('.loading').remove();
                    advancedSearch.IBTRating();
                    //checkboxshow();
                }
                advancedSearch.scrolling = false;
            }, error: function () { advancedSearch.scrolling = false; }
        });
    },
    SupplyInitCount: function (page) {
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchList');
        var json_data = { page: page, pageSize: advancedSearch.pageSize, countryId: $("#rs_hidcountryId").val(), cityId: $("#rs_hidcityId").val(), cateIds: $("#hidcateIds").val(), sortBy: $("#hidsortby").val(), filterLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/SupplySearchAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $("#SearchSupplier_count").empty();
                    $("#SearchSupplier_count").text(json.Item.total);
                    $('.loading').remove();
                    checkboxshow();
                }
            }
        });
    },
    GetMemberList: function (page) {
        if (this.itemTotal == 0) return;
        if (advancedSearch.scrolling) return;
        advancedSearch.scrolling = true;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchList');
        var json_data = { page: page, pageSize: advancedSearch.pageSize, countryId: $("#rs_hidcountryId").val(), cityId: $("#rs_hidcityId").val(), filterLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/MemberSearchAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    advancedSearch.itemTotal = json.Item.itemTotal;
                    $("#hidcount").attr("value", json.Item.total);
                    $("#advancedSearch_count").text(json.Item.total);
                    $('#searchList').append(json.Message);
                    if (json.Item.itemTotal >= advancedSearch.pageSize) {
                        //$("#clickloadmore_searchList").show();
                    } else {
                        //$("#clickloadmore_searchList").hide();
                        advancedSearch.isLastPage = true;
                    }
                    $('.loading').remove();
                    checkboxshow();
                }
                advancedSearch.scrolling = false;
            }, error: function () { advancedSearch.scrolling = false; }
        });
    },
    MemberInitCount: function (page) {
        if (this.itemTotal == 0) return;
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#searchList');
        var json_data = { page: page, pageSize: advancedSearch.pageSize, countryId: $("#rs_hidcountryId").val(), cityId: $("#rs_hidcityId").val(), filterLetter: $("#filterLetter").val() };
        $.ajax({
            url: '/Search/MemberSearchAjx', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $("#SearchMember_count").empty();
                    $("#SearchMember_count").text(json.Item.total);
                    $('.loading').remove();
                    checkboxshow();
                }
            }
        });
    },
    initData: function () {
        $("#searchList").empty();
        document.getElementById("quickSearch").style.display = "block";
        document.getElementById("feedDiv").style.display = "block";
        document.getElementById("searchDiv").style.display = "none";
        advancedSearch.GetCountryList(1);
        $("#hidtabcount").attr("value", $("#hidcount").val());  //初始化保存数据
        $("#SearchCountry_count").text($("#hidtabcount").val());
        advancedSearch.CityInitCount(1);
        advancedSearch.SupplyInitCount(1);
        advancedSearch.MemberInitCount(1);
        advancedSearch.page = 2;
        //判断tab
        if ($("#hidtabcount").val() == "0") {
            if ($("#hidtab2count").val() == "0") {
                document.getElementById("supply").click();
            }
            else {
                document.getElementById("city").click();
            }
        }
        else {
            var selectCount = parseInt($("#hidcount").val());
            if (selectCount == 1) {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\">1 Country Found</span>");
            }
            else if (selectCount > 1) {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Countries Found</span>");
            }
            else {
                document.getElementById("feedDiv").style.display = "none";
                $("#advancedSearch_title").html("Search Results <span class=\"txt\">There are no countries matching <span id=\"advancedSearch_keyword\">your search keyword</span></span>");
            }
            $("#advancedSearch_count").text($("#hidcount").val());
        }
        $("#filterLetter").val("");
        $('.noData').detach();
        if ($("#listType").val() == "City") { $("#city").click(); }
        else if ($("#listType").val() == "Supplier") { $("#supply").click(); }
        else if ($("#listType").val() == "Member") { $("#member").click(); }
    },
    changeSort: function (id) {
        $("#IsShowMySearch").val(id);
        $("#hidsortby").val(id);
    },
    goTab: function (obj, id) {
        advancedSearch.page = 2;
        advancedSearch.isLastPage = false;
        $("#searchList").empty();//先删除以前的
        //advancedSearch.scrolling = false;
        advancedSearch.itemTotal = -1;
        $("#quickSearch a").each(function () {
            $(this).removeClass("active");
        });
        $("#filterLetter").val("");
        $('.noData').detach();
        switch (id) {
            case 1:
                $("#listType").val("City");
                $("#filterList").empty();
                document.getElementById("quickSearch").style.display = "block";
                document.getElementById("feedDiv").style.display = "block";
                document.getElementById("searchDiv").style.display = "none";
                advancedSearch.GetCityList(1);
                var searchCount = parseInt($("#hidcount").val());
                if (searchCount == 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> City Found</span>");
                }
                else if (searchCount > 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Cities  Found</span>");
                }
                else {
                    document.getElementById("feedDiv").style.display = "none";
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\">There are no cities matching your search</span>");
                }
                $("#advancedSearch_count").text($("#hidcount").val());
                break;
            case 2:
                $("#listType").val("Supplier");
                $("#searchList").empty();
                document.getElementById("quickSearch").style.display = "block";//xg
                document.getElementById("feedDiv").style.display = "none";
                document.getElementById("searchDiv").style.display = "block";
                advancedSearch.GetSupplyList(1);

                //$("#SearchSupplier_count").empty();
                //$("#SearchSupplier_count").text($("#hidcount").val());  

                var searchCount = parseInt($("#hidcount").val());
                if (searchCount == 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Supplier Found</span>");
                }
                else if (searchCount > 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Suppliers Found</span>");
                }
                else {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\">There are no suppliers matching <span id=\"advancedSearch_keyword\">your search keyword</span></span>");
                }
                $("#advancedSearch_count").text($("#hidcount").val());
                break;
            case 3:
                $("#listType").val("Member");
                $("#searchList").empty();
                document.getElementById("quickSearch").style.display = "block";
                document.getElementById("feedDiv").style.display = "none";
                document.getElementById("searchDiv").style.display = "block";
                advancedSearch.GetMemberList(1);

                var searchCount = parseInt($("#hidcount").val());
                if (searchCount == 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Member  Found</span>");
                }
                else if (searchCount > 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Members  Found</span>");
                }
                else {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\">There are no members matching <span id=\"advancedSearch_keyword\">your search keyword</span></span>");
                }
                $("#advancedSearch_count").text($("#hidcount").val());
                break;
            default: //default 0 
                $("#listType").val("Country");
                $("#filterList").empty();
                document.getElementById("quickSearch").style.display = "block";
                document.getElementById("feedDiv").style.display = "block";
                document.getElementById("searchDiv").style.display = "none";
                advancedSearch.GetCountryList(1);
                var searchCount = parseInt($("#hidcount").val());
                if (searchCount == 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Country Found</span>");
                }
                else if (searchCount > 1) {
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Countries Found</span>");
                }
                else {
                    document.getElementById("feedDiv").style.display = "none";
                    $("#advancedSearch_title").html("Search Results <span class=\"txt\">There are no countries matching <span id=\"advancedSearch_keyword\">your search keyword</span></span>");
                }
                $("#advancedSearch_count").text($("#hidcount").val());
                break;
        }
    },
    quickSearch: function (obj, letter) {
        $("#filterList").empty();
        $("#searchList").empty();
        $('.noData').detach();
        advancedSearch.isLastPage = false;
        advancedSearch.itemTotal = -1;
        advancedSearch.page = 2;
        if ($("#filterLetter").val() == letter) {
            $(obj).removeClass("active");
            $("#filterLetter").val("");
            if ($("#listType").val() == "Country") {
                advancedSearch.GetCountryList(1);
            }
            else if ($("#listType").val() == "City") {
                advancedSearch.GetCityList(1);
            }
            else if ($("#listType").val() == "Supplier") {
                $("#searchList").empty();
                advancedSearch.GetSupplyList(1);
            }
            else {
                $("#searchList").empty();
                advancedSearch.GetMemberList(1);
            }
        } else {
            $("#quickSearch a").each(function () {
                $(this).removeClass("active");
            });
            $(obj).addClass("active");

            $("#filterLetter").val(letter);
            if ($("#listType").val() == "Country") {
                advancedSearch.GetCountryList(1);
            }
            else if ($("#listType").val() == "City") {
                advancedSearch.GetCityList(1);
            }
            else if ($("#listType").val() == "Supplier") {
                advancedSearch.GetSupplyList(1);
            }
            else {
                advancedSearch.GetMemberList(1);
            }
        }
        var searchCount = parseInt($("#hidcount").val());
        if ($("#listType").val() == "Country") {
            if (searchCount <= 1) {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Country Found</span>");
            }
            else {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Countries Found</span>");
            }
        }
        else if ($("#listType").val() == "City") {
            if (searchCount <= 1) {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> City Found</span>");
            }
            else {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Cities Found</span>");
            }
        }
        else if ($("#listType").val() == "Supplier") {
            if (searchCount <= 1) {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Supplier Found</span>");
            }
            else {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Suppliers Found</span>");
            }
        }
        else {
            if (searchCount <= 1) {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Member Found</span>");
            }
            else {
                $("#advancedSearch_title").html("Search Results <span class=\"txt\"><span id=\"advancedSearch_count\">-</span> Members Found</span>");
            }
        }
        $("#advancedSearch_count").text($("#hidcount").val());
    }
}