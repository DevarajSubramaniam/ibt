﻿var jFeedbacks = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    commentLists: function (page) {
        $('<div class="mT20 alignC loading"><img src="/images/icon/loading.gif" /></div>').appendTo('#SupplyFeedbackLists');
        var json_data = { supplyId: $("#supplyId").val(), page: this.page, pageSize: this.pageSize };
        $.ajax({
            url: '/Supplier/FeedbackLists', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    $('#SupplyFeedbackLists').append(json.Message);
                    $('.loading').remove();
                    jFeedbacks.page++;
                    if (json.Item.itemTotal >= jFeedbacks.pageSize) {
                        $("#clickloadmore").show();
                    } else {
                        $("#clickloadmore").hide();
                    }
                    ibt.ibtRating();
                    jfeed.FeedRating();
                    if ($('#SupplyFeedbackLists').children('li').length == 0) {
                        $('<li class="noData">There are no posts in this listing. Start sharing your trips by posting on the newsfeed!</li>').appendTo('#SupplyFeedbackLists');
                    }

                    $("textarea").focus(function () {
                        var val = "Write a message...";
                        if ($(this).val() == val) { $(this).val("") }
                    }).blur(function () {
                        if ($(this).val() == "") { $(this).val("Write a message...") }
                    });
                }
            }
        });
    },
    GetFeedListOnClick: function () {
        jFeedbacks.commentLists(jFeedbacks.page);
        jFeedbacks.page++;
    },
    init: function () {
        jFeedbacks.page = 1;
        jFeedbacks.commentLists(jFeedbacks.page);
    },
    Save: function () {
        if ($("#supplyCommentTxt").val() == "Write a message...") {
            Boxy.alert("The text field is empty. Please write in your comment before posting!", null, { title: "" });
            $(".boxyInner").addClass("popup");
            return;
        }
        var json_data = { supplyId: $("#supplyId").val(), att_ids: $("#imageids").val(), comments: encodeURIComponent($("#supplyCommentTxt").val()) };
        $.post("/Supplier/SupplyFeedbacks", json_data, function (json) {
            if (json.Success) {
                $("#SupplyFeedbackLists").prepend(json.Message);
                $("#supplyCommentTxt").val("Write a message...");

                $("textarea").focus(function () {
                    var val = "Write a message...";
                    if (val === $(this).val()) { $(this).val("") }
                }).blur(function () {
                    if ($(this).val() == "") { $(this).val("Write a message...") }
                });
                //清除图片列表
                $("#fileList").empty();
                //清除图片ids
                $("#imageids").attr("value", "");
                try {
                    //重新加载上传控件
                    supplyUploadify();
                } catch (ex) { }
                //重新加载相册
                PhotoManage.init();
                //重新加载相册的隐藏层
                photosList();
                location.reload();

            } else {
                Boxy.alert(json.Message, null, { title: "Message" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
    },
    LoadMore: function () {
        jFeedbacks.commentLists(jFeedbacks.page + 1);
    },
    cbkImages: function (obj) {
        var vals = $("#imageids").val();
        var attrdataid = $(obj).attr("data-attid");
        if ($(obj).hasClass("uncheckbox")) {
            //del
            var tmp = vals.split(',');
            var res = "";
            for (var i = 0; i < tmp.length; i++) {
                if (tmp[i] != attrdataid) {
                    res = res + tmp[i] + ",";
                }
            }
            if (res.length > 0) {
                res = res.substring(0, res.length - 1);
            }

            vals = res;
        } else {
            if (vals.length > 0)
                vals = vals + "," + attrdataid;
            else
                vals = attrdataid;
        }
        $("#imageids").val(vals);
    }
};