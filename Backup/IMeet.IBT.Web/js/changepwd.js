﻿var ChangePwd = {
    change: function () {
        if ($('#changeForm').data('bValidator').validate() == false) {
            return;
        }
        var json_data = $("#changeForm").MySerialize();
        $.post("/Account/ChangePassword", json_data, function (json) {
            if (json.Success) {
                Boxy.alert("You have successfully changed your password!", function () {
                    location.href = "/Account/AccountSetting";
                }, { title: "Change Password" });
                $(".boxyInner").addClass("popup");
            } else {
                Boxy.alert(json.Message, null, { title: "Change Password" });
                $(".boxyInner").addClass("popup");
            }
        }, "json");
       
    }
};

