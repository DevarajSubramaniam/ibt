﻿var jfeed = {
    page: 1,
    scrolling: false,
    pageSize: 10,
    itemTotal: -1,
    tabId: 1,
    init: function () {
        $(window).scroll(function () {
            if (this.scrolling==false) return;
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (jfeed.scrolling == true) {
                    jfeed.myFeed(jfeed.page);
                    jfeed.page++;
                }
            }
        });
    },
    GetFeedListOnClick: function () {
        if (jfeed.scrolling == true) {
            jfeed.myFeed(jfeed.page);
            jfeed.page++;
        }
    },
    myFeed: function (page) {
        jfeed.scrolling = false;
        var json_data = { userId: $("#feed_UserId").val(), tabId: 1, type: $("#IsShowMyProfile").val(), page: page, pageSize: this.pageSize };
        $.ajax({
            url: '/Supplier/Feedlist', async: false, dataType: "json", data: json_data, type: 'post', success: function (json) {
                if (json.Success) {
                    jfeed.itemTotal = json.Item.itemTotal;
                    $('#feedList').append(json.Message);
                    jfeed.scrolling = false;
                    if (json.Item.itemTotal >= jfeed.pageSize) {
                        $("#clickloadmore_feed").show();
                    } else {
                        $("#clickloadmore_feed").hide();
                    }
                    $('.loading').remove();
                    // ibt.ibtRating();
                    jfeed.FeedRating();
                    selectSpan();
                    //if ($('#feedList').children('li').length == 0) {
                    if (page == 1 && json.Item.itemTotal == 0) {
                        $('<li class="noData">Currently, you have not added any places to this list! Add them to your list by clicking on Fast Feedback!</li>').appendTo('#feedList');
                    }
                    jfeed.scrolling = true;
                }
            }
        });
    },
    initData: function (tabId) {
        $('#feedList').empty();
        if (tabId == 1) {
            this.myFeed(1);
            jfeed.tabId = tabId;
            this.page = 2;
        }
    },
    changeActiveity: function (id) {
        $("#IsShowMyProfile").val(id);
        if (id.toString() == "0") {
            $("#listTypeTitle").text("Friends' Activities");
        } else if (id.toString() == "1") {
            $("#listTypeTitle").text("My Activity");
        } else {
            $("#listTypeTitle").text("All Activities");
        }
        this.initData(1);
    },
    FeedRating: function () {
        $('.FeedRating').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-rating');
            },
            click: function (score, evt) {
                //ibt.rrwb_rating($(this).attr('data-ibtType'), $(this).attr('data-objId'), score);
            }
        });
    }
}