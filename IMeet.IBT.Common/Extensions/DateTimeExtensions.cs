﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.Common.Extensions
{
   public static class DateTimeExtensions
    {
        /// <summary>
        /// 把datetime转换成unixdate
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        ///<remarks>url: http://www.epochconverter.com/ </remarks>
       public static long Epoch(this DateTime time)
       {
           return (long)(time - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds;
       } 
    }
}
