﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace IMeet.IBT.Common.Utilities
{
    public class ThumbnailImage
    {
        /// <summary>
        /// 压缩比例，决定图片大小的重要因素。调整清晰度的LONG型参数，一般50-90 就很清晰了。
        /// </summary>
        private static int _PhotoQuality = 100;


        private static int[] GetResizeWidthAndHeight(int intOldWidth, int intOldHeight, int intMaxlength)
        {
            if (intOldWidth > intOldHeight)
            {
                if (intOldWidth <= intMaxlength)
                {
                    return null;
                }
                return new int[] { intMaxlength, ((int)(intMaxlength * (((float)intOldHeight) / ((float)intOldWidth)))) };
            }
            if (intOldHeight <= intMaxlength)
            {
                return null;
            }
            int[] numArray = new int[2];
            numArray[1] = intMaxlength;
            numArray[0] = (int)(intMaxlength * (((float)intOldWidth) / ((float)intOldHeight)));
            return numArray;
        }

        /// <summary>
        /// string[] strsSavePath = new string[] { path + fileName + "_mn.jpg", path + fileName + "_tn.jpg", path + fileName + "_xtn.jpg", path + fileName + "_xstn.jpg" };
        /// int[] intMaxLength = new int[] { 180, 120, 80, 50 };
        /// ResizeFromPostedFile(hfile, strsSavePath, intMaxLength);
        /// </summary>
        /// <param name="hpf"></param>
        /// <param name="strSavePath"></param>
        /// <param name="intMaxLength"></param>
        /// <returns></returns>
        public static bool ResizeFromPostedFile(HttpPostedFile hpf, string[] strSavePath, int[] intMaxLength)
        {
            int[] intX = new int[strSavePath.Length];
            int[] intY = new int[strSavePath.Length];
            for (int i = 0; i < intX.Length; i++)
            {
                intX[i] = 0;
                intY[i] = 0;
            }
            return ResizeFromPostedFile(hpf, strSavePath, intMaxLength, intX, intY, true);
        }

        public static bool ResizeFromPostedFile(HttpPostedFile hpf, string[] strSavePath, int[] intMaxLength, int[] intX, int[] intY, bool blOverWrite)
        {
            if (((strSavePath.Length != intMaxLength.Length) || (strSavePath.Length != intX.Length)) || (strSavePath.Length != intY.Length))
            {
                throw new Exception("strSavePath,intMaxLength,intX,intY 数组长度不一致！");
            }
            using (Image image = Image.FromStream(hpf.InputStream))
            {
                ImageFormat rawFormat = image.RawFormat;
                int height = (int)image.PhysicalDimension.Height;
                int width = (int)image.PhysicalDimension.Width;
                Image image2 = null;
                Graphics graphics = null;
                EncoderParameters encoderParams = null;
                try
                {
                    try
                    {
                        for (int i = 0; i < intMaxLength.Length; i++)
                        {
                            if (!blOverWrite && File.Exists(strSavePath[i]))
                            {
                                continue;
                            }
                            int[] numArray = GetResizeWidthAndHeight(width, height, intMaxLength[i]);
                            if (numArray == null)
                            {
                                hpf.SaveAs(strSavePath[i]);
                                continue;
                            }
                            image2 = new Bitmap(numArray[0], numArray[1]);
                            graphics = Graphics.FromImage(image2);
                            graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
                            graphics.SmoothingMode = SmoothingMode.AntiAlias;
                            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            graphics.Clear(Color.White);
                            Rectangle destRect = new Rectangle(0, 0, image2.Width, image2.Height);
                            int num4 = 0;
                            Rectangle srcRect = new Rectangle(intX[i], intY[i], image.Width, image.Height + num4);
                            graphics.DrawImage(image, destRect, srcRect, GraphicsUnit.Pixel);
                            encoderParams = new EncoderParameters();
                            long[] numArray2 = new long[] { (long)_PhotoQuality };
                            EncoderParameter parameter = new EncoderParameter(Encoder.Quality, numArray2);
                            encoderParams.Param[0] = parameter;
                            ImageCodecInfo[] imageEncoders = ImageCodecInfo.GetImageEncoders();
                            ImageCodecInfo encoder = null;
                            for (int j = 0; j < imageEncoders.Length; j++)
                            {
                                if (imageEncoders[j].FormatDescription.Equals("JPEG"))
                                {
                                    encoder = imageEncoders[j];
                                    break;
                                }
                            }
                            if (encoder != null)
                            {
                                image2.Save(strSavePath[i], encoder, encoderParams);
                            }
                            else
                            {
                                image2.Save(strSavePath[i], rawFormat);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                }
                finally
                {
                    if (graphics != null)
                    {
                        graphics.Dispose();
                    }
                    if (image2 != null)
                    {
                        image2.Dispose();
                    }
                    if (encoderParams != null)
                    {
                        encoderParams.Dispose();
                    }
                }
                return true;
            }
        }

        #region resize photo
        /// <summary>
        /// resize HttpPostedFile or File, make it any side no longer than longer_side_size or shorter than shorter_side_size
        /// </summary>
        /// <param name="objPostedFile"></param>
        /// <param name="strOriginalImagePath"></param>
        /// <param name="strImageResizedPath"></param>
        /// <param name="intResizedWidth"></param>
        /// <param name="intResizedHeight"></param>
        /// <param name="strResizeStander"></param>
        public static bool ResizePhoto_O(HttpPostedFile objPostedFile, string strOriginalImagePath, String strImageResizedPath, int intResizedWidth, int intResizedHeight, string strResizeStander)
        {
            int intNewWidth = 0;
            int intNewHeight = 0;
            System.Drawing.Image oImg = null;
            try
            {
                if (objPostedFile != null && strOriginalImagePath == "")
                {
                    oImg = System.Drawing.Image.FromStream(objPostedFile.InputStream);
                }
                else if (objPostedFile == null && strOriginalImagePath != "")
                {
                    oImg = System.Drawing.Image.FromFile(strOriginalImagePath);
                }
            }
            catch
            {
                return false;
            }

            //get image original width and height
            int intOldWidth = oImg.Width;
            int intOldHeight = oImg.Height;
            double dblCoef = 1.0;
            if (intOldWidth > intResizedWidth || intOldHeight > intResizedHeight)  // 判断是否有至少有一边比重新设定的边大
            {
                switch (strResizeStander)
                {
                    case "intMaxSideSize":
                        //determine if landscape or portrait
                        int intMaxSide;
                        int intMaxSideSize;
                        if (intOldWidth / (double)intOldHeight > intResizedWidth / (double)intResizedHeight)
                        {
                            intMaxSide = intOldWidth;
                            intMaxSideSize = intResizedWidth;
                        }
                        else
                        {
                            intMaxSide = intOldHeight;
                            intMaxSideSize = intResizedHeight;
                        }
                        dblCoef = intMaxSideSize / (double)intMaxSide;
                        //intNewWidth = Convert.ToInt32(dblCoef * intOldWidth);
                        //intNewHeight = Convert.ToInt32(dblCoef * intOldHeight);
                        break;

                    case "intMinSideSize":
                        //determine if landscape or portrait
                        int intMinSide;
                        int intMinSideSize;
                        if (intOldWidth / (double)intOldHeight > intResizedWidth / (double)intResizedHeight)
                        {
                            intMinSide = intOldHeight;
                            intMinSideSize = intResizedHeight;
                        }
                        else
                        {
                            intMinSide = intOldWidth;
                            intMinSideSize = intResizedWidth;
                        }
                        dblCoef = intMinSideSize / (double)intMinSide;
                        //intNewWidth = Convert.ToInt32(dblCoef * intOldWidth);
                        //intNewHeight = Convert.ToInt32(dblCoef * intOldHeight);
                        break;
                }
            }

            intNewWidth = Convert.ToInt32(dblCoef * intOldWidth);
            intNewHeight = Convert.ToInt32(dblCoef * intOldHeight);

            try
            {
                if (dblCoef < 1.0)
                {
                    System.Drawing.Image oThumbNail = new Bitmap(intNewWidth, intNewHeight);//, oImg.PixelFormat);
                    Graphics oGraphic = Graphics.FromImage(oThumbNail);
                    oGraphic.CompositingQuality = CompositingQuality.HighQuality;
                    oGraphic.SmoothingMode = SmoothingMode.HighQuality;
                    oGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    oGraphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    oGraphic.Clear(Color.White);
                    Rectangle oRectangle = new Rectangle(0, 0, intNewWidth, intNewHeight);
                    oGraphic.DrawImage(oImg, oRectangle);
                    oImg.Dispose();

                    ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
                    EncoderParameters encoderParameters;
                    encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                    oThumbNail.Save(strImageResizedPath, info[1], encoderParameters);
                    oThumbNail.Dispose();
                }
                else
                {
                    objPostedFile.SaveAs(strImageResizedPath);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// resize HttpPostedFile, make it any side no longer than longer_side_size
        /// </summary>
        /// <param name="objPostedFile"></param>
        /// <param name="strImageResizedPath"></param>
        /// <param name="intResizedWidth"></param>
        /// <param name="intResizedHeight"></param>
        public static bool ResizePhotoByMaxSideSize(HttpPostedFile objPostedFile, String strImageResizedPath, int intResizedWidth, int intResizedHeight)
        {
            return ResizePhoto_O(objPostedFile, "", strImageResizedPath, intResizedWidth, intResizedHeight, "intMaxSideSize");
        }


        /// <summary>
        /// resize File, make it any side no longer than longer_side_size
        /// </summary>
        /// <param name="strOriginalImagePath"></param>
        /// <param name="strImageResizedPath"></param>
        /// <param name="intResizedWidth"></param>
        /// <param name="intResizedHeight"></param>
        public static bool ResizePhotoByMaxSideSize(string strOriginalImagePath, String strImageResizedPath, int intResizedWidth, int intResizedHeight)
        {
            return ResizePhoto_O(null, strOriginalImagePath, strImageResizedPath, intResizedWidth, intResizedHeight, "intMaxSideSize");
        }


        /// <summary>
        /// resize HttpPostedFile, make it any side shorter than shorter_side_size
        /// </summary>
        /// <param name="objPostedFile"></param>
        /// <param name="strImageResizedPath"></param>
        /// <param name="intResizedWidth"></param>
        /// <param name="intResizedHeight"></param>
        public static bool ResizePhotoByMinSideSize(HttpPostedFile objPostedFile, String strImageResizedPath, int intResizedWidth, int intResizedHeight)
        {
            return ResizePhoto_O(objPostedFile, "", strImageResizedPath, intResizedWidth, intResizedHeight, "intMinSideSize");
        }


        /// <summary>
        /// resize File, make it any side shorter than shorter_side_size
        /// </summary>
        /// <param name="strOriginalImagePath"></param>
        /// <param name="strImageResizedPath"></param>
        /// <param name="intResizedWidth"></param>
        /// <param name="intResizedHeight"></param>
        public static bool ResizePhotoByMinSideSize(string strOriginalImagePath, String strImageResizedPath, int intResizedWidth, int intResizedHeight)
        {
            return ResizePhoto_O(null, strOriginalImagePath, strImageResizedPath, intResizedWidth, intResizedHeight, "intMinSideSize");
        }

        #endregion
    }
}
