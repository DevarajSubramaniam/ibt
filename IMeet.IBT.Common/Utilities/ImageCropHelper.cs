﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace IMeet.IBT.Common.Utilities
{
    /// <summary>
    ///  1.上传到图片服务器返回mn图片
    ///  2.在mn图片切割，返回坐标到图片服务器然后减切成小图
    /// </summary>
    public class ImageCropHelper
    {
        public static void CropImage(System.Drawing.Image imgSrc, string destPath, int destImgWidth, int destImgHeight, int srcStartPointX, int srcStartPointY, int destStartPointX, int destStartPointY)
        {
            if (imgSrc == null)
            {
                return;
            }
            Bitmap bitmapDest = new Bitmap(destImgWidth, destImgHeight);
            Graphics graphics = Graphics.FromImage(bitmapDest);
            Rectangle rectSrc = new Rectangle(new Point(srcStartPointX, srcStartPointY), new Size(destImgWidth, destImgHeight));
            Rectangle rectDest = new Rectangle(new Point(destStartPointX, destStartPointY), new Size(destImgWidth, destImgHeight));
            graphics.DrawImage(imgSrc, rectDest, rectSrc, GraphicsUnit.Pixel);
            bitmapDest.Save(destPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        }



        public static void CropImage(string imgUrl, string destPath, int destImgWidth, int destImgHeight, int srcStartPointX, int srcStartPointY, int destStartPointX, int destStartPointY)
        {
            System.Drawing.Image imgSrc = GetWebImage(imgUrl);
            if (imgSrc == null)
            {
                return;
            }
            string destDirection = Directory.GetParent(destPath).ToString();
            if (!Directory.Exists(destDirection))
                Directory.CreateDirectory(destDirection);
            Bitmap bitmapDest = new Bitmap(destImgWidth, destImgHeight);
            Graphics graphics = Graphics.FromImage(bitmapDest);
            Rectangle rectSrc = new Rectangle(new Point(srcStartPointX, srcStartPointY), new Size(destImgWidth, destImgHeight));
            Rectangle rectDest = new Rectangle(new Point(destStartPointX, destStartPointY), new Size(destImgWidth, destImgHeight));
            graphics.DrawImage(imgSrc, rectDest, rectSrc, GraphicsUnit.Pixel);
            bitmapDest.Save(destPath, System.Drawing.Imaging.ImageFormat.Jpeg);
            imgSrc.Dispose();
            bitmapDest.Dispose();
            graphics.Dispose();

        }

        #region 没用
        //public static void UploadImageToServer(string imgUrl, string ftpPath, string ftpFileName, string ftpConfigKey)
        //{
        //    try
        //    {
        //        FileStream stream = new FileStream(imgUrl, FileMode.Open);
        //        string strBackMsg = "";
        //        Techsailor.Ftp.UpLoadFile.FileManage.UploadFile(stream, ftpFileName, ftpConfigKey, ftpPath, ref strBackMsg);
        //        stream.Close();
        //        stream.Dispose();
        //    }
        //    catch
        //    {

        //    }
        //}
#endregion

        /// <summary>
        ///  上转头像到文件服务器
        /// </summary>
        /// <param name="imgUrl">服务器上图片的物理地址</param>
        /// <param name="networkID">networkID</param>
        /// <param name="strSign">运行版填0</param>
        /// <param name="strToServerUrl">http://site2.test.com/doajaxfileupload.ashx</param>
        public static bool UploadAvatarToServer(string imgUrl, int networkID, string strSign, string strToServerUrl)
        {
            try
            {
                string url = strToServerUrl + "?networkID=" + networkID.ToString() + "&Sign=" + strSign;
                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    wc.UploadFile(url, "post", imgUrl);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// 获取网络图片--没用
        /// </summary>
        /// <param name="imgUrl">网络图片的URL</param>
        /// <returns></returns>
        public static System.Drawing.Image GetWebImage(string imgUrl)
        {
            try
            {
                System.Net.WebRequest request = System.Net.WebRequest.Create(imgUrl);
                System.Net.WebResponse respose = request.GetResponse();
                System.IO.Stream resposeStream = respose.GetResponseStream();
                System.Drawing.Image img = System.Drawing.Image.FromStream(resposeStream);
                resposeStream.Close();
                resposeStream.Dispose();
                return img;
            }
            catch
            {
                return null;
            }
        }

        #region 新的图片剪裁方法
        /// <summary>
        /// 
        /// </summary>
        /// <param name="src">图片源地址</param>
        /// <param name="zoom">zoom比率</param>
        /// <param name="x">默认 x</param>
        /// <param name="y">默认 y</param>
        /// <param name="width">select width</param>
        /// <param name="height">select height</param>
        /// <returns></returns>
        public static void GenerateBitmap(string src, int x, int y, int width, int height, string destPath, int imgNowWidth, int imgNowHeight)
        {
            try
            {
                if (string.IsNullOrEmpty(src))
                {
                    return;
                }
                string destDirection = Directory.GetParent(destPath).ToString();
                if (!Directory.Exists(destDirection))
                    Directory.CreateDirectory(destDirection);

                //Image.GetThumbnailImageAbort abort = null;
                //11-17
                //System.Drawing.Image oldBitmap = GetWebImage(src);
                //new 11-17
                System.Drawing.Image oldBitmap = System.Drawing.Image.FromFile(src);

                Cutter cut = new Cutter(
                    x,
                    y,
                    width,
                    height,
                    imgNowWidth,
                    imgNowHeight);

                Bitmap bmp = GenerateBitmap(oldBitmap, cut, destPath);
                oldBitmap.Dispose();

                bmp.Save(destPath, ImageFormat.Jpeg);
                bmp.Dispose();

            }
            catch (Exception ex)
            {
            }
        }
        /// <summary>
        /// 这个方法就是 通过用户drag的比率 然后放大图片 截取定义的长宽图片
        /// </summary>
        /// <param name="oldImage">A  source</param>
        /// <param name="cut"> model</param>
        /// <returns></returns>
        public static Bitmap GenerateBitmap(Image oldImage, Cutter cut, string desctPATH)
        {
            Image newBitmap = null;
            if (oldImage == null)
                throw new ArgumentNullException("oldImage");
            if (newBitmap == null)
            {
                newBitmap = new Bitmap(cut.Width, cut.Height);
            }
            //Re-paint the oldImage
            using (Graphics g = Graphics.FromImage(newBitmap))
            {
                g.InterpolationMode = InterpolationMode.High;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.DrawImage(oldImage, 0, 0, new Rectangle(cut.X, cut.Y, cut.Width, cut.Height), GraphicsUnit.Pixel);
                g.Save();
            }
            //别忘记注释
            newBitmap.Save(desctPATH, ImageFormat.Jpeg);
            Bitmap bmp = new Bitmap(cut.CutterWidth, cut.CutterHeight);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = InterpolationMode.High;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.DrawImage(newBitmap, new Rectangle(0, 0, cut.CutterWidth, cut.CutterHeight), new Rectangle(0, 0, cut.Width, cut.Height), GraphicsUnit.Pixel);
                g.Save();
                newBitmap.Dispose();
            }
            return bmp;
        }


        #region 实体
        /// <summary>
        /// Cutter Model
        /// </summary>
        public class Cutter
        {
            #region -Constructor-
            /// <summary>
            /// Create a new Cutter instance
            /// </summary>
            public Cutter(int _X, int _Y, int _CutterWidth, int _CutterHeight, int _Width, int _Height)
            {

                this._X = _X;
                this._Y = _Y;
                this._CutterHeight = _CutterHeight;
                this._CutterWidth = _CutterWidth;
                this._Width = _Width;
                this._Height = _Height;

            }
            #endregion

            #region -Properties-
            private int _SaveWidth;
            /// <summary>
            /// Resize bitmap(Width)
            /// </summary>
            public int SaveWidth
            {
                get { return _SaveWidth; }
                set { _SaveWidth = value; }
            }
            private int _SaveHeight;
            /// <summary>
            /// Resize bitmap(Height)
            /// </summary>
            public int SaveHeight
            {
                get { return _SaveHeight; }
                set { _SaveHeight = value; }
            }
            private int _X;
            /// <summary>
            /// X coordinates(from left-top corner)
            /// </summary>
            public int X
            {
                get { return _X; }
                set { _X = value; }
            }
            private int _Y;
            /// <summary>
            /// Y coordinates(from left-top corner)
            /// </summary>
            public int Y
            {
                get { return _Y; }
                set { _Y = value; }
            }
            private int _CutterWidth;
            /// <summary>
            /// Width of cutter
            /// </summary>
            public int CutterWidth
            {
                get { return _CutterWidth; }
                set { _CutterWidth = value; }
            }
            private int _CutterHeight;
            /// <summary>
            /// Height of cutter
            /// </summary>
            public int CutterHeight
            {
                get { return _CutterHeight; }
                set { _CutterHeight = value; }
            }
            private int _Width;
            /// <summary>
            /// Width of original size
            /// </summary>
            public int Width
            {
                get { return _Width; }
                set { _Width = value; }
            }
            private int _Height;
            /// <summary>
            /// Height of original size
            /// </summary>
            public int Height
            {
                get { return _Height; }
                set { _Height = value; }
            }

            private int _NowWidth;
            /// <summary>
            /// Width of select size
            /// </summary>
            public int NowWidth
            {
                get { return _NowWidth; }
                set { _NowWidth = value; }
            }
            private int _NowHeight;
            /// <summary>
            /// Height of select size
            /// </summary>
            public int NowHeight
            {
                get { return _NowHeight; }
                set { _NowHeight = value; }
            }
            #endregion
        }
        #endregion


        #endregion

       
    }
}
