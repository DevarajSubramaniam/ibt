﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

namespace IMeet.IBT.Common.Caching
{
    /// <summary>
    /// 暂时不用这
    /// </summary>
    public class TsCache
    {
        public static T Get<T>(string key, string cacheManagerName = "")
        {
            ICacheManager _cacher = Init(cacheManagerName);

            object obj = _cacher.GetData(key.ToString());
            if (obj == null) { return default(T); }

            return (T)obj;
        }

        public static T Get<T>(string key, int totalSeconds, Func<T> fetcher, string cacheManagerName = "", bool absoluteTime = false, Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority cacheItemPriority = Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority.Normal,ICacheItemRefreshAction refreshAction = null)
        {
            ICacheManager _cacher = Init(cacheManagerName);

            object obj = _cacher.GetData(key);
            if (obj == null)
            {
                T result = fetcher();
                if (absoluteTime)
                    _cacher.Add(key, result, cacheItemPriority, refreshAction, new AbsoluteTime(TimeSpan.FromSeconds(totalSeconds)));
                else
                    _cacher.Add(key, result, cacheItemPriority, refreshAction, new SlidingTime(TimeSpan.FromSeconds(totalSeconds)));
                return result;
            }

            return (T)obj;
        }

        public static bool Contains(string key, string cacheManagerName = "")
        {
            ICacheManager _cacher = Init(cacheManagerName);
           
            return _cacher.Contains(key);
        }

        /// <summary>
        /// 从缓存移除数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cacheManagerName"></param>
        public static void Remove(string key, string cacheManagerName = "")
        {
            ICacheManager _cacher = Init(cacheManagerName);
            _cacher.Remove(key.ToString());
        }

        /// <summary>
        /// 清除所有数据
        /// </summary>
        /// <param name="cacheManagerName"></param>
        public static void Flush(string cacheManagerName = "")
        {
            ICacheManager _cacher = Init(cacheManagerName);
            _cacher.Flush();
        }


        private static ICacheManager Init(string cacheManagerName)
        {
            ICacheManager _cacher;
            if (string.IsNullOrWhiteSpace(cacheManagerName))
                _cacher = CacheFactory.GetCacheManager();
            else
                _cacher = CacheFactory.GetCacheManager(cacheManagerName);

            return _cacher;
        }

    }
}
