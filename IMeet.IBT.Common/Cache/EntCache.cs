﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

namespace IMeet.IBT.Common.Caching
{
    public class EntCache : ICache
    {
        ICacheManager _cacher;
        private CacheSettings _settings = new CacheSettings();

        public EntCache()
        {
            _cacher = CacheFactory.GetCacheManager();
            _settings = new CacheSettings();
        }

        public EntCache(string cacheManagerName)
        {
            _cacher = CacheFactory.GetCacheManager(cacheManagerName);
            _settings = new CacheSettings();
        }

        /// <summary>
        /// Get the cache settings.
        /// </summary>
        public CacheSettings Settings
        {
            get { return _settings; }
        }

        public int Count
        {
            get {
                return _cacher.Count;
            }
        }

        public System.Collections.ICollection Keys
        {
            get { throw new NotImplementedException(); }
        }

        public bool Contains(string key)
        {
            return _cacher.Contains(key);
        }

        public object Get(object key)
        {
            return _cacher.GetData(key.ToString());
        }

        public T Get<T>(object key)
        {
            object obj = _cacher.GetData(key.ToString());
            if (obj == null) { return default(T); }

            return (T)obj;
        }

        public T GetOrInsert<T>(object key, int timeToLiveInSeconds, bool slidingExpiration, Func<T> fetcher)
        {
            object obj = Get(key);
            if (obj == null)
            {
                T result = fetcher();
                Insert(key, result, timeToLiveInSeconds, slidingExpiration);
                return result;
            }

            return (T)obj;
        }

        public void Remove(object key)
        {
            _cacher.Remove(key.ToString());
        }

        public void RemoveAll(System.Collections.ICollection keys)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            _cacher.Flush();
        }

        public void Insert(object key, object value)
        {
            Insert(key, value, _settings.DefaultTimeToLive, _settings.DefaultSlidingExpirationEnabled, _settings.DefaultCachePriority);
        }

        public void Insert(object key, object value, int timeToLive, bool slidingExpiration)
        {
            Insert(key, value, timeToLive, slidingExpiration, _settings.DefaultCachePriority);
        }

        public void Insert(object key, object value, int timeToLive, bool slidingExpiration, Caching.CacheItemPriority priority)
        {
            Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority EntPriority = ConvertToAspNetPriority(priority);
            _cacher.Add(key.ToString(), value, EntPriority, null, new SlidingTime(TimeSpan.FromSeconds(timeToLive)));
        }

        public IList<CacheItemDescriptor> GetDescriptors()
        {
            throw new NotImplementedException();
        }


        private Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority ConvertToAspNetPriority(CacheItemPriority priority)
        {
            if (priority == CacheItemPriority.Default) { return Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority.None; }
            if (priority == CacheItemPriority.High) { return Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority.High; }
            if (priority == CacheItemPriority.Low) { return Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority.Low; }
            if (priority == CacheItemPriority.Normal) { return Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority.Normal; }

            return Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority.NotRemovable;
        }
    }
}
