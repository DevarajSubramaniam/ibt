﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace IMeet.IBT.Common.Email
{
    public class MailMessageTemplate
    {
        private readonly FileInfo _template;
        private readonly Dictionary<string, string> _variables = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
        private const string Pattern = "{(.|\n)+?}";
        private MailMessage _message;

        /// <summary>
        /// 联系我们
        /// </summary>
        public const string Contact = "Contact";
        /// <summary>
        /// 忘记用户名
        /// </summary>
        public const string ForgotUsername = "ForgotUsername";
        /// <summary>
        /// 忘记密码
        /// </summary>
        public const string PasswordReset = "PasswordReset";
        /// <summary>
        /// 注册
        /// </summary>
        public const string Registered = "Register";
        /// <summary>
        /// 新用户通知
        /// </summary>
        public const string NewAccountNotification = "NewAccountNotification";

        public MailMessageTemplate(FileInfo template)
        {
            _template = template;
            Parse();
        }

        private void AddVariable(string key, string value)
        {
            if (_variables.ContainsKey(key))
                _variables[key] = value;
            else
                _variables.Add(key, value);
        }

        public void AssignVariable(string key, string value)
        {
            var realKey = "{" + key + "}";
            if (_variables.ContainsKey(realKey))
                _variables[realKey] = value;
        }

        private void Parse()
        {
            _message = new MailMessage();
            _message.IsBodyHtml = true;

            string[] lines = File.ReadAllLines(_template.FullName);
            var sb = new StringBuilder();

            foreach (var line in lines)
            {
                bool append = true;
                if (line.ToUpper().StartsWith("@"))
                {
                    append = false;
                    string[] tokens = line.Split('=');
                    if (tokens.Length == 2)
                    {
                        AddVariable(tokens[0].Trim(), tokens[1]);
                    }
                }

                var matches = Regex.Matches(line, Pattern);
                foreach (Match match in matches)
                {
                    AddVariable(match.Groups[0].Value, String.Empty);
                }

                if (append)
                    sb.AppendLine(line);
            }

            _message.Body = sb.ToString();
        }

        public MailMessage Execute()
        {
            ReplaceVariables();
            return _message;
        }

        private void ReplaceVariables()
        {
            if (_variables.ContainsKey("@Subject"))
                _message.Subject = _variables["@Subject"];

            var keys = _variables.Where(x => x.Key.StartsWith("{") && x.Key.EndsWith("}")).Select(x => x.Key);
            foreach (var key in keys)
            {
                _message.Body = _message.Body.Replace(key, _variables[key]);
                _message.Subject = _message.Subject.Replace(key, _variables[key]);
            }
        }
    }
}
