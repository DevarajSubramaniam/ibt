﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Globalization;

using IMeet.IBT.Common.Configurations;

namespace IMeet.IBT.Common.Email
{
    /// <summary>
    /// 发送邮件
    /// </summary>
    /// <remarks>
    /// 注意：web.config需要设置如下，如果需要密码发送把network的注释去掉设置好相关的
    /// <code>
    /// <system.net>
    /// <mailSettings>
    ///  <smtp deliveryMethod="SpecifiedPickupDirectory">
    ///    <specifiedPickupDirectory pickupDirectoryLocation="c:\temp\ts" />
    ///    <!--<network host="smtpserver" port="25" userName="username" password="password" defaultCredentials="true" />-->
    ///  </smtp>
    /// </mailSettings>
    /// </system.net>
    /// </code>
    /// </remarks>
    public partial class EmailService
    {
        public DirectoryInfo _rootDirectory;
        public readonly SiteConfiguration _configuration;

        public EmailService()
        {
            _configuration = new SiteConfiguration();
            _rootDirectory = FileHelper.GetDirectory(_configuration.EmailTemplateDirectory);
        }

        /// <summary>
        /// App_Data目录下的邮件模板文件夹名称/路径：Email\\zh-cn
        /// </summary>
        /// <param name="emailPath"></param>
        public EmailService(string emailPath)
        {
            _rootDirectory = FileHelper.GetDirectory(emailPath);
            _configuration = new SiteConfiguration();
        }

        public DirectoryInfo GetLocalizedDirectory()
        {
            DirectoryInfo info = _rootDirectory;

            if (CultureInfo.CurrentCulture != CultureInfo.GetCultureInfo("en-US"))
            {
                string path = Path.Combine(_rootDirectory.FullName, CultureInfo.CurrentCulture.Name);
                if (Directory.Exists(path))
                    info = new DirectoryInfo(path);
            }

            return info;
        }

        public MailMessageTemplate GetMailTemplate(string name)
        {
            var file = new FileInfo(Path.Combine(GetLocalizedDirectory().FullName, name + ".txt"));

            if (!file.Exists)
                throw new FileNotFoundException("The mail template could not be found.", file.FullName);

            return new MailMessageTemplate(file);
        }

        /// <summary>
        /// 邀请用户注册发送的邮件
        /// </summary>
        public void SendInviteMail(string email, string msg, string firstname, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("Invite"); //Invite是App_Data下Email模板文件名称     
            //设置变量
            //template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("Message", msg);
            template.AssignVariable("InviteName", firstname);
            template.AssignVariable("StatisticsCountry", statisticsCountry);
            template.AssignVariable("StatisticsCity", statisticsCity);
            template.AssignVariable("StatisticsHotel", statisticsHotel);
            template.AssignVariable("StatisticsVenue", statisticsVenue);
            template.AssignVariable("StatisticsOther", statisticsOther);
            MailMessage message = template.Execute();

            //添加收信人地址
            message.To.Add(new MailAddress(email));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);


        }

        /// <summary>
        /// 用户注册时发送帐号激活的邮件
        /// </summary>
        public void SendActivateMail(string email)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("Activate"); //Activate是App_Data下Email模板文件名称
            //设置变量
            template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("SiteUrl", _configuration.SiteUrl);
            //template.AssignVariable("firstname", firstname==""?userName:firstname);
            //template.AssignVariable("userName", userName);
            //template.AssignVariable("password", password);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(email));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        /// <summary>
        /// 用户邮件(测试)
        /// </summary>
        /// <param name="email"></param>
        public void SendTemplateMail(string email)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("EmailTemplate"); //EmailTemplate是App_Data下Email模板文件名称
            //设置变量
            template.AssignVariable("SiteName", _configuration.SiteName);
            //template.AssignVariable("SiteUrl", _configuration.SiteUrl);
            //template.AssignVariable("firstname", firstname==""?userName:firstname);
            //template.AssignVariable("userName", userName);
            //template.AssignVariable("password", password);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(email));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        /// <summary>
        /// 用户注册成功时发送的邮件
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="firstname"></param>
        public void SendRegistrationMail(string email, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("Register"); //Register是App_Data下Email模板文件名称
            //设置变量
            //template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            //template.AssignVariable("firstname", firstname==""?userName:firstname);
            //template.AssignVariable("userName", userName);
            //template.AssignVariable("password", password);
            template.AssignVariable("StatisticsCountry", statisticsCountry);
            template.AssignVariable("StatisticsCity", statisticsCity);
            template.AssignVariable("StatisticsHotel", statisticsHotel);
            template.AssignVariable("StatisticsVenue", statisticsVenue);
            template.AssignVariable("StatisticsOther", statisticsOther);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(email));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        /// <summary>
        /// 忘记密码发送密码（是直接发送密码给用户的），这可以把密码修改成验证码，要输入这验证码才可以修改密码
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="firstname"></param>
        public void RemindUserPassword(string toEmail, string username, string password, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("RemindUserPassword");
            //设置变量
            //template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            //template.AssignVariable("firstname", firstname == "" ? username : firstname);
            //template.AssignVariable("userName", username);
            template.AssignVariable("password", password);
            template.AssignVariable("StatisticsCountry", statisticsCountry);
            template.AssignVariable("StatisticsCity", statisticsCity);
            template.AssignVariable("StatisticsHotel", statisticsHotel);
            template.AssignVariable("StatisticsVenue", statisticsVenue);
            template.AssignVariable("StatisticsOther", statisticsOther);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(toEmail));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        public void SendReSetPasswordLink(string toEmail, string username, string validateCode, string firstname = "")
        {
            //获取发送的模板文件
            var template = GetMailTemplate("SendReSetPasswordLink");
            //设置变量
            template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("SiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("firstname", firstname == "" ? username : firstname);
            template.AssignVariable("userName", username);
            template.AssignVariable("validateCode", validateCode);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(toEmail));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        /// <summary>
        /// 激活TravelBadge时发送邮件
        /// </summary>
        /// <param name="toEmail"></param>
        public void SendActivedTeavelBadge(string toEmail)
        {
            var template = GetMailTemplate("ActiveTravelBadge");
            template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);

            MailMessage message = template.Execute();
            message.To.Add(new MailAddress(toEmail));
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, "Meeting Professional Badge");

            SendEmail(message);
        }

        /// <summary>
        /// badge special 给用户发送email
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="fromEmail"></param>
        /// <param name="title"></param>
        /// <param name="msg"></param>
        public void SendBadgeSpecial(string toEmail, string fromEmail, string title, string msg, bool isBodyHtml = false)
        {
            MailMessage message = new MailMessage();
            message.Body = msg;
            message.IsBodyHtml = isBodyHtml;
            message.To.Add(toEmail);
            message.From = new MailAddress(fromEmail);
            message.Subject = title;

            SendEmail(message);
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="fromEmail"></param>
        /// <param name="title"></param>
        /// <param name="msg"></param>
        /// <param name="isBodyHtml"></param>
        public void SendEmailTools(string toEmail, string fromEmail, string title, string msg, bool isBodyHtml = false)
        {
            MailMessage message = new MailMessage();
            message.Body = msg;
            message.IsBodyHtml = isBodyHtml;
            message.To.Add(toEmail);
            message.From = new MailAddress(fromEmail);
            message.Subject = title;

            SendEmail(message);
        }

        /// <summary>
        /// 发送邮件给已激活用户
        /// </summary>
        /// <param name="fromEmail"></param>
        /// <param name="toEmail"></param>
        /// <param name="userName"></param>
        /// <param name="profilePic"></param>
        /// <param name="countryNumber"></param>
        /// <param name="cityNumber"></param>
        /// <param name="placesNumber"></param>
        /// <param name="content"></param>
        public void SendActiveMemberEmail(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string content, string key, string totalPoints, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                logo = @"/images/activate/logo_ibt.jpg";
            }
            var template = GetMailTemplate("ActivateMember");

            if (ShowMPB == 1)
                template = GetMailTemplate("ActivateMember");
            else
                template = GetMailTemplate("ActivateMember_NOMPB");

            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);
            template.AssignVariable("Content", content);
            template.AssignVariable("Key", key);
            template.AssignVariable("TotalPoints", totalPoints);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            message.To.Add(new MailAddress(toEmail));
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }
        /// <summary>
        /// New Send Active member email
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="fromEmail"></param>
        /// <param name="toEmail"></param>
        /// <param name="userName"></param>
        /// <param name="subject"></param>
        /// <param name="profilePic"></param>
        /// <param name="countryNumber"></param>
        /// <param name="cityNumber"></param>
        /// <param name="placesNumber"></param>
        /// <param name="content"></param>
        /// <param name="key"></param>
        /// <param name="totalPoints"></param>
        /// <param name="emailId"></param>
        /// <param name="title"></param>
        /// <param name="logo"></param>
        /// <param name="ShowMPB"></param>
        public void NewSendActiveMemberEmail(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string content, string key, string totalPoints, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                logo = @"/images/activate/logo_ibt.jpg";
            }
            var template = GetMailTemplate("NewActivateMember");

            if (ShowMPB == 1)
                template = GetMailTemplate("NewActivateMember");
            else
                template = GetMailTemplate("NewActivateMember_NOMPB");

            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);
            template.AssignVariable("Content", content);
            template.AssignVariable("Key", key);
            template.AssignVariable("TotalPoints", totalPoints);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            message.To.Add(new MailAddress(toEmail));
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }
        /// <summary>
        /// 发送邮件给未激活用户
        /// </summary>
        /// <param name="fromEmail"></param>
        /// <param name="toEmail"></param>
        /// <param name="userName"></param>
        /// <param name="subject"></param>
        /// <param name="profilePic"></param>
        /// <param name="countryNumber"></param>
        /// <param name="cityNumber"></param>
        /// <param name="placesNumber"></param>
        /// <param name="content"></param>
        public void SendNotActiveMemberEmail(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string content, string key, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                logo = @"/images/notactivate/logo_ibt.jpg";
            }
            var template = GetMailTemplate("NotActivateMember");

            if (ShowMPB == 1)
                template = GetMailTemplate("NotActivateMember");
            else
                template = GetMailTemplate("NotActivateMember_NOMPB");

            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);
            template.AssignVariable("Content", content);
            template.AssignVariable("Key", key);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            message.To.Add(new MailAddress(toEmail));
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }
        /// <summary>
        /// New Send Not Activate member email
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="fromEmail"></param>
        /// <param name="toEmail"></param>
        /// <param name="userName"></param>
        /// <param name="subject"></param>
        /// <param name="profilePic"></param>
        /// <param name="countryNumber"></param>
        /// <param name="cityNumber"></param>
        /// <param name="placesNumber"></param>
        /// <param name="content"></param>
        /// <param name="key"></param>
        /// <param name="emailId"></param>
        /// <param name="title"></param>
        /// <param name="logo"></param>
        /// <param name="ShowMPB"></param>
        public void NewSendNotActiveMemberEmail(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string content, string key, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                logo = @"/images/notactivate/logo_ibt.jpg";
            }
            var template = GetMailTemplate("NewNotActivateMember");

            if (ShowMPB == 1)
                template = GetMailTemplate("NewNotActivateMember");
            else
                template = GetMailTemplate("NewNotActivateMember_NOMPB");

            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);
            template.AssignVariable("Content", content);
            template.AssignVariable("Key", key);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            message.To.Add(new MailAddress(toEmail));
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }
        public void NewEmailAddressAdded(string email, string newEmailId, string userName, string firstname)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("NewEmailAddressAdded"); //Activate是App_Data下Email模板文件名称
            //设置变量
            template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("SiteUrl", _configuration.SiteUrl);
            template.AssignVariable("firstname", firstname == "" ? userName : firstname);
            template.AssignVariable("NewEMail", newEmailId);
            //template.AssignVariable("firstname", firstname==""?userName:firstname);

            //template.AssignVariable("password", password);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(email));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        public void NewEmailVerification(string email, string userName, string firstname, string verificationtoken)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("NewEmailVerification"); //Activate是App_Data下Email模板文件名称
            //设置变量
            template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("SiteUrl", _configuration.SiteUrl);
            template.AssignVariable("firstname", firstname == "" ? userName : firstname);
            template.AssignVariable("verificationtoken", verificationtoken);
            template.AssignVariable("NewEMail", email);
            //template.AssignVariable("firstname", firstname==""?userName:firstname);
            //template.AssignVariable("userName", userName);
            //template.AssignVariable("password", password);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(email));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        public void NewEmailConfirmed(string email, string userName, string firstname)
        {
            //获取发送的模板文件
            var template = GetMailTemplate("NewEmailConfirmed"); //Activate是App_Data下Email模板文件名称
            //设置变量
            template.AssignVariable("SiteName", _configuration.SiteName);
            template.AssignVariable("SiteUrl", _configuration.SiteUrl);
            template.AssignVariable("firstname", firstname == "" ? userName : firstname);
            //template.AssignVariable("firstname", firstname==""?userName:firstname);
            //template.AssignVariable("userName", userName);
            //template.AssignVariable("password", password);

            MailMessage message = template.Execute();
            //添加收信人地址
            message.To.Add(new MailAddress(email));
            //添加发信人及地址
            message.From = new MailAddress(_configuration.NoReplyEmailAddress, _configuration.SiteName);
            //发送邮件
            SendEmail(message);
        }

        /// <summary>
        /// 发送邮件，需要web.config配置
        /// </summary>
        /// <param name="message"></param>
        public static void SendEmail(MailMessage message)
        {
            try
            {
                var client = new SmtpClient();
                //client.Host = "smtp.mandrillapp.com";
                //client.Port = 587;
                //client.UseDefaultCredentials = false;
                //System.Net.NetworkCredential MyCredentials = new System.Net.NetworkCredential("email@i-meet.com", "e5M26WGxhJyKrgOvCqQlNg");
                //client.Credentials = MyCredentials;
                //client.EnableSsl = true;
                client.Send(message);
            }
            catch (Exception)
            { }
        }

        #region Email Survey

        public void SendActiveMemberSurvey(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string contentTop, string contentDown, string suggestedList, string key, string totalPoints, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                //logo = @"/images/activate/logo_ibt.jpg";
                logo = @"/images/activate/logo_ibt_survey.jpg";
            }
            var template = GetMailTemplate("ActivateMemberSurvey");

            if (ShowMPB == 1)
                template = GetMailTemplate("ActivateMemberSurvey");
            else
                template = GetMailTemplate("ActivateMemberSurvey_NOMPB");

            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);

            template.AssignVariable("Content-1", contentTop);
            template.AssignVariable("Content-2", contentDown);
            template.AssignVariable("SuggestedList", suggestedList);

            template.AssignVariable("Key", key);
            template.AssignVariable("TotalPoints", totalPoints);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            message.To.Add(new MailAddress(toEmail));
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }

        /// <summary>
        /// New send activate member survey
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="fromEmail"></param>
        /// <param name="toEmail"></param>
        /// <param name="userName"></param>
        /// <param name="subject"></param>
        /// <param name="profilePic"></param>
        /// <param name="countryNumber"></param>
        /// <param name="cityNumber"></param>
        /// <param name="placesNumber"></param>
        /// <param name="contentTop"></param>
        /// <param name="contentDown"></param>
        /// <param name="suggestedList"></param>
        /// <param name="key"></param>
        /// <param name="totalPoints"></param>
        /// <param name="emailId"></param>
        /// <param name="title"></param>
        /// <param name="logo"></param>
        /// <param name="ShowMPB"></param>
        public void NewSendActiveMemberSurvey(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string contentTop, string contentDown, string suggestedList, string key, string totalPoints, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                //logo = @"/images/activate/logo_ibt.jpg";
                logo = @"/images/activate/logo_ibt_survey.jpg";
            }
            var template = GetMailTemplate("NewActivateMemberSurvey");

            if (ShowMPB == 1)
                template = GetMailTemplate("NewActivateMemberSurvey");
            else
                template = GetMailTemplate("NewActivateMemberSurvey_NOMPB");

            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);

            template.AssignVariable("Content-1", contentTop);
            template.AssignVariable("Content-2", contentDown);
            template.AssignVariable("SuggestedList", suggestedList);

            template.AssignVariable("Key", key);
            template.AssignVariable("TotalPoints", totalPoints);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            message.To.Add(new MailAddress(toEmail));
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }
        public void SendNotActiveMemberSurvey(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string contentTop, string contentDown, string suggestedList, string key, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                //logo = @"/images/notactivate/logo_ibt.jpg";
                logo = @"/images/activate/logo_ibt_survey.jpg";
            }
            var template = GetMailTemplate("NotActivateMemberSurvey");

            if (ShowMPB == 1)
                template = GetMailTemplate("NotActivateMemberSurvey");
            else
                template = GetMailTemplate("NotActivateMemberSurvey_NOMPB");


            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);

            template.AssignVariable("Content-1", contentTop);
            template.AssignVariable("Content-2", contentDown);
            template.AssignVariable("SuggestedList", suggestedList);

            template.AssignVariable("Key", key);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            var dd = toEmail;
            MailAddress _mailAddress = new MailAddress(toEmail);
            message.To.Add(_mailAddress);
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }

        /// <summary>
        /// New send not activate member survey
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="fromEmail"></param>
        /// <param name="toEmail"></param>
        /// <param name="userName"></param>
        /// <param name="subject"></param>
        /// <param name="profilePic"></param>
        /// <param name="countryNumber"></param>
        /// <param name="cityNumber"></param>
        /// <param name="placesNumber"></param>
        /// <param name="contentTop"></param>
        /// <param name="contentDown"></param>
        /// <param name="suggestedList"></param>
        /// <param name="key"></param>
        /// <param name="emailId"></param>
        /// <param name="title"></param>
        /// <param name="logo"></param>
        /// <param name="ShowMPB"></param>
        public void NewSendNotActiveMemberSurvey(string senderName, string fromEmail, string toEmail, string userName, string subject, string profilePic, string countryNumber, string cityNumber, string placesNumber, string contentTop, string contentDown, string suggestedList, string key, string emailId, string title, string logo, int ShowMPB)
        {
            string width = "auto";
            if (string.IsNullOrEmpty(logo))
            {
                width = "0px";
                //logo = @"/images/notactivate/logo_ibt.jpg";
                logo = @"/images/activate/logo_ibt_survey.jpg";
            }
            var template = GetMailTemplate("NewNotActivateMemberSurvey");

            if (ShowMPB == 1)
                template = GetMailTemplate("NewNotActivateMemberSurvey");
            else
                template = GetMailTemplate("NewNotActivateMemberSurvey_NOMPB");


            template.AssignVariable("AlphaSiteUrl", _configuration.AlphaSiteUrl);
            template.AssignVariable("UserName", userName);
            template.AssignVariable("ProfilePic", profilePic);
            template.AssignVariable("Subject", subject);
            template.AssignVariable("CountryNumber", countryNumber);
            template.AssignVariable("CityNumber", cityNumber);
            template.AssignVariable("PlacesNumber", placesNumber);

            template.AssignVariable("Content-1", contentTop);
            template.AssignVariable("Content-2", contentDown);
            template.AssignVariable("SuggestedList", suggestedList);

            template.AssignVariable("Key", key);
            template.AssignVariable("EmailId", emailId);
            template.AssignVariable("Logo", logo);
            template.AssignVariable("Title", title);
            template.AssignVariable("Width", width);

            MailMessage message = template.Execute();
            message.IsBodyHtml = true;
            var dd = toEmail;
            MailAddress _mailAddress = new MailAddress(toEmail);
            message.To.Add(_mailAddress);
            message.From = new MailAddress(fromEmail, senderName);

            SendEmail(message);
        }


        #endregion

    }
}
