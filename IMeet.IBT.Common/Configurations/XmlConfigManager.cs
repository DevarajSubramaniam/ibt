﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using System.Web;
using System.Web.Caching;

namespace IMeet.IBT.Common.Configurations
{
    /// <summary>
    /// 
    /// </summary>
    public class XmlConfigManager
    {
        private Dictionary<string, Dictionary<string, string>> _settings;
        private const string DefaultGroup = "default";

        public XmlConfigManager()
        {
            Refresh();
        }

        public XmlConfigManager(string fileName)
        {
            FileName = fileName;
            Refresh();
        }

        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }

        public void Refresh()
        {
            string cacheKey = "cache_" + FileName;
            HttpContext context = HttpContext.Current;
            if (context.Cache[cacheKey] == null)
            {
                _settings = new Dictionary<string, Dictionary<string, string>>();
                string path = Path.Combine(FileHelper.DataDirectory.FullName, FileName+".xml");
                if (File.Exists(path) == false) return;

                XDocument xmlDoc = XDocument.Load(path);
                _settings = xmlDoc.Descendants("Group")
                    .Select(x => new
                    {
                        SiteName = x.Attribute("name").Value,
                        Settings = x.Descendants("Setting").ToDictionary(s => s.Attribute("name").Value, s => s.Value)
                    })
                    .ToDictionary(x => x.SiteName, x => x.Settings);

                CacheDependency dp = new CacheDependency(path);
                context.Cache.Insert(cacheKey, _settings, dp, DateTime.MaxValue, TimeSpan.Zero);
            }
            _settings = (Dictionary<string, Dictionary<string, string>>)context.Cache[cacheKey];
        }

        private String FindSetting(string name, string groupName = null)
        {

            if (_settings.ContainsKey(groupName ?? DefaultGroup))
            {
                var siteSettings = _settings[groupName ?? DefaultGroup];
                if (siteSettings.ContainsKey(name))
                {
                    return siteSettings[name];
                }
            }

            return null;
        }

        public dynamic GetSetting(string name, dynamic defaultValue, string groupName = null)
        {
            var setting = FindSetting(name, groupName);

            if (typeof(String) == defaultValue.GetType())
                return setting ?? defaultValue;

            if (typeof(DateTime) == defaultValue.GetType())
                return setting == null ? defaultValue : Convert.ToDateTime(setting);

            if (typeof(Int32) == defaultValue.GetType())
                return setting == null ? defaultValue : Convert.ToInt32(setting);

            if (typeof(Boolean) == defaultValue.GetType())
                return setting == null ? defaultValue : Convert.ToBoolean(setting);

            throw new NotSupportedException();
        }

        public void RemoveSetting(string name, string groupName = null)
        {
            if (_settings.ContainsKey(groupName ?? DefaultGroup) == false)
                return;

            var siteSettings = _settings[groupName ?? DefaultGroup];
            if (siteSettings.ContainsKey(name))
            {
                siteSettings.Remove(name);
            }
        }

        /// <summary>
        /// 删除组
        /// </summary>
        /// <param name="groupName"></param>
        public void RemoveGroup(string groupName)
        {
            if (_settings.ContainsKey(groupName) == true)
            {
                _settings.Remove(groupName);
            }
        }

        public void Save()
        {
            string path = Path.Combine(FileHelper.DataDirectory.FullName, FileName +".xml");
            var root = new XElement(FileName);
            foreach (var key in _settings.Keys)
            {
                var siteNode = new XElement("Group", new XAttribute("name", key));
                foreach (var item in _settings[key])
                {
                    siteNode.Add(new XElement("Setting", new XAttribute("name", item.Key), item.Value));
                }
                root.Add(siteNode);
            }
            var xmlDoc = new XDocument(root);
            xmlDoc.Save(path);
        }
    }
}
