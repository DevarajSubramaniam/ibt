﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using System.Web;
using System.Web.Caching;

namespace IMeet.IBT.Common.Configurations
{
    /// <summary>
    /// 站点的基本配置管理
    /// </summary>
    public class SiteConfiguration
    {
        private Dictionary<string, Dictionary<string, string>> _settings;
        private const string DefaultGroup = "default";
        private const string cacheKey = "SiteConfiguration";

        public SiteConfiguration()
        {
            Refresh();
        }

        public void Refresh()
        {
            if (HttpContext.Current != null)
            {
                HttpContext context = HttpContext.Current;
                if (context.Cache[cacheKey] == null)
                {
                    _settings = new Dictionary<string, Dictionary<string, string>>();
                    string path = Path.Combine(FileHelper.DataDirectory.FullName, "SiteConfiguration.xml");
                    if (File.Exists(path) == false) return;

                    XDocument xmlDoc = XDocument.Load(path);
                    _settings = xmlDoc.Descendants("Group")
                        .Select(x => new
                        {
                            SiteName = x.Attribute("name").Value,
                            Settings = x.Descendants("Setting").ToDictionary(s => s.Attribute("name").Value, s => s.Value)
                        })
                        .ToDictionary(x => x.SiteName, x => x.Settings);

                    CacheDependency dp = new CacheDependency(path);
                    context.Cache.Insert(cacheKey, _settings, dp, DateTime.MaxValue, TimeSpan.Zero);
                }
                _settings = (Dictionary<string, Dictionary<string, string>>)context.Cache[cacheKey];
            }
            else
            {
                _settings = new Dictionary<string, Dictionary<string, string>>();
                string path = @"SiteConfiguration.xml";
                FileInfo info = new FileInfo(path);
                if (!File.Exists(path)) return;

                XDocument xmlDoc = XDocument.Load(path);
                _settings = xmlDoc.Descendants("Group")
                    .Select(x => new
                    {
                        SiteName = x.Attribute("name").Value,
                        Settings = x.Descendants("Setting").ToDictionary(s => s.Attribute("name").Value, s => s.Value)
                    })
                    .ToDictionary(x => x.SiteName, x => x.Settings);
            }
        }

        private String FindSetting(string name, string groupName = null)
        {
            //if (HttpContext.Current == null)
            //    return null;

            if (_settings.ContainsKey(groupName ?? DefaultGroup))
            {
                var siteSettings = _settings[groupName ?? DefaultGroup];
                if (siteSettings.ContainsKey(name))
                {
                    return siteSettings[name];
                }
            }

            return null;
        }

        public dynamic GetSetting(string name, dynamic defaultValue, string groupName = null)
        {
            var setting = FindSetting(name, groupName);

            if (typeof(String) == defaultValue.GetType())
                return setting ?? defaultValue;

            if (typeof(DateTime) == defaultValue.GetType())
                return setting == null ? defaultValue : Convert.ToDateTime(setting);

            if (typeof(Int32) == defaultValue.GetType())
                return setting == null ? defaultValue : Convert.ToInt32(setting);

            if (typeof(Boolean) == defaultValue.GetType())
                return setting == null ? defaultValue : Convert.ToBoolean(setting);

            throw new NotSupportedException();
        }

        public void SetSetting(string name, dynamic value, string groupName = null)
        {
            if (_settings.ContainsKey(groupName ?? DefaultGroup) == false)
                _settings.Add(groupName ?? DefaultGroup, new Dictionary<string, string>());

            var siteSettings = _settings[groupName ?? DefaultGroup];
            if (siteSettings.ContainsKey(name))
            {
                siteSettings[name] = Convert.ToString(value);
            }
            else
            {
                siteSettings.Add(name, Convert.ToString(value));
            }
        }

        public void RemoveSetting(string name, string groupName = null)
        {
            if (_settings.ContainsKey(groupName ?? DefaultGroup) == false)
                return;

            var siteSettings = _settings[groupName ?? DefaultGroup];
            if (siteSettings.ContainsKey(name))
            {
                siteSettings.Remove(name);
            }
        }

        /// <summary>
        /// 删除组
        /// </summary>
        /// <param name="groupName"></param>
        public void RemoveGroup(string groupName)
        {
            if (_settings.ContainsKey(groupName) == true)
            {
                _settings.Remove(groupName);
            }
        }

        /// <summary>
        /// 网站名称
        /// </summary>
        public string InviteName
        {
            get { return GetSetting("InviteName", "Ts Site"); }
            set { SetSetting("InviteName", value); }
        }

        /// <summary>
        /// 网站名称
        /// </summary>
        public string SiteName
        {
            get { return GetSetting("SiteName", "Ts Site"); }
            set { SetSetting("SiteName", value); }
        }



        /// <summary>
        /// 网站地址
        /// </summary>
        public string SiteUrl
        {
            get { return GetSetting("SiteUrl", "http://www.ive-been-there.com"); }
           // get { return GetSetting("SiteUrl", "http://alpha.ibt.i-meet.com/"); }
            set { SetSetting("SiteUrl", value.ToString()); }
        }
        public string AlphaSiteUrl
        {
            get { return GetSetting("AlphaSiteUrl", "http://www.ive-been-there.com"); }
            //get { return GetSetting("AlphaSiteUrl", "http://alpha.ibt.i-meet.com/"); }
            set { SetSetting("AlphaSiteUrl", value.ToString()); }
        }

        /// <summary>
        /// 网站MetaKeywords
        /// </summary>
        public string SiteMetaKeywords
        {
            get { return GetSetting("SiteMetaKeywords", String.Empty); }
            set { SetSetting("SiteMetaKeywords", value); }
        }

        /// <summary>
        /// 网站MetaDescription
        /// </summary>
        public string SiteMetaDescription
        {
            get { return GetSetting("SiteMetaDescription", String.Empty); }
            set { SetSetting("SiteMetaDescription", value); }
        }

        /// <summary>
        /// 注册用户是否必须填邮箱
        /// </summary>
        public bool RegisterEmailRequired
        {
            get { return GetSetting("RegisterEmailRequired", true); }
            set { SetSetting("RegisterEmailRequired", value); }
        }

        /// <summary>
        /// 新注册是否发送邮件给用户
        /// </summary>
        public string EnableSendRegistrationMail
        {
            get { return GetSetting("EnableSendRegistrationMail", false); }
            set { SetSetting("EnableSendRegistrationMail", value); }
        }
        /// <summary>
        /// 不需回复的邮箱地址
        /// </summary>
        public string NoReplyEmailAddress
        {
            get { return GetSetting("NoReplyEmailAddress", "noreply@ts.com"); }
            set { SetSetting("NoReplyEmailAddress", value); }
        }

        /// <summary>
        /// 网站支持的email,多个邮箱用分号隔开
        /// </summary>
        public string SupportEmailAddress
        {
            get { return GetSetting("SupportEmailAddress", "support@ts.com"); }
            set { SetSetting("SupportEmailAddress", value); }
        }

        /// <summary>
        /// 网站支持的email(发送cc的),多个邮箱用分号隔开
        /// </summary>
        public string SupportCCEmailAddress
        {
            get { return GetSetting("SupportCCEmailAddress", ""); }
            set { SetSetting("SupportCCEmailAddress", value); }
        }

        /// <summary>
        /// Email 模板在AppData里的路径
        /// </summary>
        public string EmailTemplateDirectory
        {
            get { return GetSetting("EmailTemplateDirectory", "Email\\en-us"); }
            set { SetSetting("EmailTemplateDirectory", value); }
        }

        /// <summary>
        /// 日期格式
        /// </summary>
        public string DateFormat
        {
            get
            {
                try
                {
                    string format = GetSetting("DateFormat", "MM/dd/yyyy");
                    DateTime.Now.ToString(format);
                    return format;
                }
                catch (FormatException)
                {
                    return "MM/dd/yyyy";
                }
            }
            set { SetSetting("DateFormat", value); }
        }


        public void Save()
        {
            string path = Path.Combine(FileHelper.DataDirectory.FullName, "SiteConfiguration.xml");
            var root = new XElement("SiteConfiguration");
            foreach (var key in _settings.Keys)
            {
                var siteNode = new XElement("Group", new XAttribute("name", key));
                foreach (var item in _settings[key])
                {
                    siteNode.Add(new XElement("Setting", new XAttribute("name", item.Key), item.Value));
                }
                root.Add(siteNode);
            }
            var xmlDoc = new XDocument(root);
            xmlDoc.Save(path);
        }
    }
}
