﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web;

using IMeet.IBT.Common;

namespace IMeet.IBT.Common.Helpers
{
    public class HttpHelper
    {
        public enum Method { GET, POST, PUT, DELETE };

        public static BoolMessage WebRequest(Method method, string url, Dictionary<string, string> postData)
        {
            string pData = "";
            if (postData.Count > 0)
            {
                foreach (KeyValuePair<string, string> kvp in postData)
                {
                    if (pData.Length > 0)
                    {
                        pData += "&";
                    }

                    pData += (kvp.Key + "=" + kvp.Value);
                }
            }
            return WebRequest(method, url, pData);
        }

        public static BoolMessage WebRequest(Method method, string url, string postData)
        {
            HttpWebRequest webRequest = null;
            StreamWriter requestWriter = null;

            if (method == Method.GET && postData.Length > 0)
            {
                if (url.IndexOf("?") > 0)
                {
                    url += "&";
                }
                else
                {
                    url += "?";
                }
                url = url + postData;
            }

            webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
            webRequest.Method = method.ToString();
            webRequest.ServicePoint.Expect100Continue = false;

            webRequest.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.2; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3";
            webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            webRequest.Headers.Add("Accept-Encoding", "gzip");
            //webRequest.Headers.Add("Accept-Language", "zh-cn,zh;q=0.5");
            webRequest.Timeout = 5000;

            if (method == Method.POST)
            {
                webRequest.ContentType = "application/x-www-form-urlencoded";
                requestWriter = new StreamWriter(webRequest.GetRequestStream());
                try
                {
                    requestWriter.Write(postData);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    requestWriter.Close();
                    requestWriter = null;
                }
            }

            var bm = WebRequest(webRequest);

            webRequest = null;

            return bm;

        }

        public static BoolMessage WebRequest(HttpWebRequest webRequest)
        {
            StreamReader responseReader = null;// new StreamReader(response.GetResponseStream(), System.Text.Encoding.GetEncoding("gb2312"));
            var bm = new BoolMessage(false, "unknow Error!");
            try
            {
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (response.ContentEncoding != null && response.ContentEncoding.Equals("gzip", StringComparison.InvariantCultureIgnoreCase))
                        responseReader = new StreamReader(new GZipStream(response.GetResponseStream(), CompressionMode.Decompress), System.Text.Encoding.GetEncoding("utf-8"));
                    else
                        responseReader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.GetEncoding("utf-8"));

                    bm = new BoolMessage(true, responseReader.ReadToEnd());
                }

                response.Close();
                response = null;
                webRequest.GetResponse().GetResponseStream().Close();
                responseReader.Close();
            }
            catch (WebException e)
            {
                bm = new BoolMessage(false, e.Status.ToString());
            }
            catch(Exception e)
            {
                bm = new BoolMessage(false, e.Message.ToString());
            }
            finally
            {
                responseReader = null;
            }

            return bm;
        }

        public static BoolMessage WebRequestStatusCode(HttpWebRequest webRequest)
        {
            var bm = new BoolMessage(false, "unknow Error!");
            try
            {
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
                bm = new BoolMessage(true, response.StatusCode.ToString());
                response.Close();
                response = null;
                webRequest.GetResponse().GetResponseStream().Close();
            }
            catch (WebException e)
            {
                bm = new BoolMessage(false, e.Status.ToString());
            }
            catch (Exception e)
            {
                bm = new BoolMessage(false, e.Message.ToString());
            }

            return bm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fileData">//想要发送的XML文件</param>
        /// <returns></returns>
        public static BoolMessage WebPostXML(string url,string xmlData)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);

            foreach (string headerKey in System.Web.HttpContext.Current.Request.Headers.AllKeys)
                httpWebRequest.Headers.Add(headerKey, System.Web.HttpContext.Current.Request.Headers[headerKey]);

            httpWebRequest.ContentType = "application/xml";
            httpWebRequest.Method = "POST";

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xmlData);
                requestStream.Write(bytes, 0, bytes.Length);
            }

            return WebRequest(httpWebRequest);
        }
    }
}
