﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace IMeet.IBT.Common.Helpers
{
    public static class JsonHelper
    {
        /// <summary>
        /// DataTable返回ArrayList,然后可以return Json(new{data=GetJSonArrayList(dt)})
        /// </summary>
        /// <param name="_dt"></param>
        /// <returns></returns>
        public static ArrayList GetJSonArrayList(DataTable _dt)
        {
            ArrayList m_R = new ArrayList();
            foreach (DataRow m_Row in _dt.Rows)
            {
                Dictionary<string, object> m_Di = new Dictionary<string, object>();
                foreach (DataColumn m_Col in _dt.Columns)
                    m_Di.Add(m_Col.ColumnName, m_Row[m_Col]);
                m_R.Add(m_Di);
            }
            return m_R;
        }
    }
}
