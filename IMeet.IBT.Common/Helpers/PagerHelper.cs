﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace IMeet.IBT.Common.Helpers
{
    public static class PagerHelper
    {
        /// <summary>
        /// 后台的分页
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="curPage">当前页数</param>
        /// <param name="total"></param>
        /// <param name="pageSize"></param>
        /// <param name="url">超级链接地址 /group/thread/new/[page]?abc=2</param>
        /// <param name="extendPage">周边页码显示个数上限</param>
        /// <returns></returns>
        public static string AdminPager(this HtmlHelper helper, int curPage, int total, int pageSize, string url, int extendPage = 3)
        {
            int startPage = 1;
            int endPage = 1;
            int pageCount = 1;

            if (Convert.ToBoolean((total % pageSize) == 0))
                pageCount = total / pageSize;
            else
                pageCount = Convert.ToInt32(total / pageSize + 1);

            //if (url.IndexOf("?") >= 0)
            //    url = url + "&";
            //else
            //    url = url + "?";

            if (pageCount < 1) pageCount = 1;
            if (extendPage < 3) extendPage = 2;
            if (curPage < 1) curPage = 1;
            if (curPage > pageCount) curPage = pageCount;
            StringBuilder sb = new StringBuilder();
            sb.Append("<span class=\"AdminPager right\">");
            //<span>« Prev</span> <span>...</span> Next »  class=\"current\"
            if (curPage > 1)
                //    sb.Append("<a href=\"" + url.Replace("[page]", "1") + "\">« Prev</a>");//sb.Append("<span>« Prev</span>");
                //else
                sb.Append("<a href=\"" + url.Replace("[page]", ((curPage - 1) * 1).ToString()) + "\"><<上一页</a>");
            if (curPage - extendPage > 2)
                startPage = curPage - extendPage;
            if (curPage + extendPage <= pageCount)
                endPage = curPage + extendPage;
            else if (curPage + extendPage > pageCount)
                endPage = pageCount;
            if (extendPage * 2 > endPage)
                endPage = extendPage * 2 + 2;
            if (startPage < 1) startPage = 1;
            if (endPage > pageCount) endPage = pageCount;

            if (startPage > 2)
            {
                sb.Append("<a href=\"" + url.Replace("[page]", "1") + "\">1</a>");
                sb.Append("<a href=\"" + url.Replace("[page]", "2") + "\">2</a>");
                if (startPage > 3)
                    sb.Append("<span>...</span>");
            }
            for (int i = startPage; i < endPage; i++)
            {
                if (i == curPage)
                    sb.Append("<a class=\"active\" href=\"" + url.Replace("[page]", i.ToString()) + "\">" + i.ToString() + "</a>");
                else
                    sb.Append("<a href=\"" + url.Replace("[page]", i.ToString()) + "\">" + i.ToString() + "</a>");
            }
            if (endPage < pageCount)
                sb.Append("<span>...</span>");
            if (curPage == pageCount)
                sb.Append("<a class=\"active\" href=\"" + url.Replace("[page]", curPage.ToString()) + "\">" + curPage.ToString() + "</a>");
            else
                sb.Append("<a href=\"" + url.Replace("[page]", pageCount.ToString()) + "\">" + pageCount.ToString() + "</a>");
            if (curPage < pageCount)
                //sb.Append("<span>Next »</span>");
                //else
                sb.Append("<a href=\"" + url.Replace("[page]", ((curPage + 1) * 1).ToString()) + "\">下一页>></a>");

            sb.Append("</span>");
            return sb.ToString();
        }

        /// <summary>
        /// 分页(格式：上一页 下一页 1/13)
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="curPage">当前页数</param>
        /// <param name="total"></param>
        /// <param name="pageSize"></param>
        /// <param name="url">超级链接地址 /group/thread/new/[page]?abc=2</param>
        /// <returns></returns>
        public static string Pager(this HtmlHelper helper, int curPage, int total, int pageSize, string url)
        {
            int endPage = 1;
            int pageCount = 1;

            if (Convert.ToBoolean((total % pageSize) == 0))
                pageCount = total / pageSize;
            else
                pageCount = Convert.ToInt32(total / pageSize + 1);

            if (pageCount < 1) pageCount = 1;
            if (curPage < 1) curPage = 1;
            if (curPage > pageCount) curPage = pageCount;
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"pagination\">");
            if (curPage > 1)
                sb.Append("<a href=\"" + url.Replace("[page]", ((curPage - 1) * 1).ToString()) + "\">上一页</a>&nbsp;");

            if (endPage > pageCount) endPage = pageCount;
            if (curPage < pageCount)
                sb.Append("<a href=\"" + url.Replace("[page]", ((curPage + 1) * 1).ToString()) + "\">下一页</a>&nbsp;");

            sb.Append(curPage.ToString() + "/" + pageCount.ToString());
            sb.Append("</div>");
            if (pageCount > 1)
                return sb.ToString();
            else
                return "";
        }

        /// <summary>
        /// 分页(格式：上一幅 下一幅)
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="curNum">当前页数</param>
        /// <param name="total"></param>
        /// <param name="pageSize"></param>
        /// <param name="url">超级链接地址 /group/thread/new/[page]?abc=2</param>
        /// <returns></returns>
        public static string PicturePager(this HtmlHelper helper, int curNum, int total, int pageSize, PicturePagerBtn btn, string url)
        {
            int endPage = 1;
            int pageCount = 1;

            if (Convert.ToBoolean((total % pageSize) == 0))
                pageCount = total / pageSize;
            else
                pageCount = Convert.ToInt32(total / pageSize + 1);

            if (pageCount < 1) pageCount = 1;
            if (curNum < 1) curNum = 1;
            if (curNum > pageCount) curNum = pageCount;
            StringBuilder sb = new StringBuilder();

            if (btn == PicturePagerBtn.Up)
            {
                if (curNum > 1)
                    sb.Append("<span class=\"inlineBlock height100\"><a href=\"" + url.Replace("[pnum]", ((curNum - 1) * 1).ToString()) + "\" class=\"a_light_btn\"><img src=\"/images/prev.gif\" /></a></span>");
                else
                    sb.Append("<span class=\"inlineBlock height100\"><a href=\"" + url.Replace("[pnum]", ((pageCount) * 1).ToString()) + "\" class=\"a_light_btn\"><img src=\"/images/prev.gif\" /></a></span>");
            }
            if (endPage > pageCount) endPage = pageCount;
            if (btn == PicturePagerBtn.Down)
            {
                if (curNum < pageCount)
                    sb.Append("<span class=\"inlineBlock height100\"><a href=\"" + url.Replace("[pnum]", ((curNum + 1) * 1).ToString()) + "\" class=\"a_light_btn\"><img src=\"/images/next.gif\" /></a></span>");
                else
                    sb.Append("<span class=\"inlineBlock height100\"><a href=\"" + url.Replace("[pnum]", "1") + "\" class=\"a_light_btn\"><img src=\"/images/next.gif\" /></a></span>");
            }
            if (pageCount > 1)
                return sb.ToString();
            else
                return "";
        }
    }

    public enum PicturePagerBtn
    {
        /// <summary>
        /// 上一幅
        /// </summary>
        Up,
        /// <summary>
        /// 下一幅
        /// </summary>
        Down
    }
}
