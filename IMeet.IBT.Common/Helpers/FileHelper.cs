﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;

namespace IMeet.IBT.Common
{
    public class FileHelper
    {
        /// <summary>
        /// 保存的路径/年月/日/会员id/
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="saveDateTime">加这时间主要是前后一致</param>
        /// <returns>saveDateTime.ToString("yyyyMM") + "/" + saveDateTime.ToString("dd") + "/" + userId + "/"</returns>
        public static string SaveDbPaht(string userId = "0", DateTime? saveDateTime = null)
        {
            var dt = saveDateTime == null ? DateTime.Now : Convert.ToDateTime(saveDateTime);
            return dt.ToString("yyyyMM") + "/" + dt.ToString("dd") + "/" + userId + "/";
        }

        /// <summary>
        /// 保存的物理路径/年月/日/会员id/
        /// </summary>
        /// <param name="physicsPath">物理路径~/Static/</param>
        /// <param name="userId">userid</param>
        /// <param name="saveDateTime">加这时间主要是前后一致</param>
        /// <returns></returns>
        public static string SaveMapPath(string physicsPath = "~/Static/", string userId = "0", DateTime? saveDateTime = null)
        {
            var dt = saveDateTime == null ? DateTime.Now : Convert.ToDateTime(saveDateTime);
            string path = HttpContext.Current.Server.MapPath(physicsPath) + dt.ToString("yyyyMM") + "\\" + dt.ToString("dd") + "\\" + userId + "\\";
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);
            return path;
        }

        /// <summary>
        /// 根据path删除保存在电脑里的文件
        /// </summary>
        /// <param name="dbPath"></param>
        public static void DeleteFile(string path)
        {
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
        }

        /// <summary>
        /// 获取app_Data目录地址
        /// </summary>
        public static DirectoryInfo DataDirectory
        {
            get
            {
                var appDataPath = (string)AppDomain.CurrentDomain.GetData("DataDirectory") ??
                                   AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                return new DirectoryInfo(appDataPath);
            }
        }
        /// <summary>
        /// 获取app_Data下的文件夹目录地址
        /// </summary>
        public static DirectoryInfo GetDirectory(string relativePath)
        {
            string path = Path.Combine(DataDirectory.FullName, relativePath);
            return new DirectoryInfo(path);
        }
    }
}
