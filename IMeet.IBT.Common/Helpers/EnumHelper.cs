﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.Common.Helpers
{
   public class EnumHelper
    {
        public Dictionary<string, int> GetEnumItems<T>()
        {
            Dictionary<string, int> dicResult = new Dictionary<string, int>();
            T obj = default(T);
            Type type = obj.GetType();
            foreach (string s in type.GetEnumNames())
            {
                dicResult.Add(s, Convert.ToInt16((T)Enum.Parse(typeof(T), s, true)));
            }
            return dicResult;
        }

        public T GetEnumByEnumName<T>(string enumName)
        {
            T result = default(T);
            if (Enum.IsDefined(typeof(T), enumName))
            {
                result = (T)Enum.Parse(typeof(T), enumName, true);
            }
            else
            {

            }
            return result;
        }

        public T GetEnumByEnumIndex<T>(int ienum)
        {
            T result = default(T);
            if (Enum.IsDefined(typeof(T), ienum))
            {
                result = (T)Enum.Parse(typeof(T), ienum.ToString(), true);
            }
            else
            {

            }
            return result;
        }
    }
}
