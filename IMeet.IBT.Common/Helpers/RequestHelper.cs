﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;

namespace IMeet.IBT.Common.Helpers
{
    public static class RequestHelper
    {
        /// <summary>
        /// 获取Post/Get的值
        /// </summary>
        /// <param name="form"></param>
        /// <param name="getKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetVal(FormCollection form, string getKey, string defaultValue = "")
        {
            foreach (var key in form.AllKeys)
            {
                if (string.Equals(key, getKey, StringComparison.OrdinalIgnoreCase))
                {
                    return form.Get(getKey);
                }
            }

            return defaultValue.Trim();
        }

        /// <summary>
        /// 获取Form提交的值（数字）
        /// </summary>
        /// <param name="form"></param>
        /// <param name="getKey"></param>
        /// <param name="defaultValue">默认是0</param>
        /// <returns></returns>
        public static int GetValInt(FormCollection form, string getKey, int defaultValue = 0)
        {
            int val = int.TryParse(RequestHelper.GetVal(form, getKey, defaultValue.ToString()), out val) ? val : defaultValue;
            return val;
        }
        /// <summary>
        /// 返回UrlDecode(xx)，当提交的时候用了encodeURIComponent需要用此
        /// </summary>
        /// <param name="form"></param>
        /// <param name="getKey"></param>
        /// <param name="defaultValue">默认是空</param>
        /// <returns></returns>
        public static string GetUrlDecodeVal(FormCollection form, string getKey, string defaultValue = "")
        {
            return HttpContext.Current.Server.UrlDecode(GetVal(form, getKey, defaultValue: defaultValue));
        }

        /// <summary>
        /// 获取Post/Get的值
        /// </summary>
        /// <param name="getKey"></param>
        /// <param name="defaultValue">默认是空</param>
        /// <param name="blQueryString">是否必须用QueryString取值</param>
        /// <returns></returns>
        public static string GetVal(string getKey, string defaultValue = "", bool blQueryString = false)
        {
            if (blQueryString)
                return HttpContext.Current.Request.QueryString[getKey] ?? defaultValue;
            else
                return HttpContext.Current.Request.Params[getKey] ?? defaultValue;
        }

        /// <summary>
        /// 返回UrlDecode(xx)，当提交的时候用了encodeURIComponent需要用此
        /// </summary>
        /// <param name="getKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetUrlDecodeVal(string getKey, string defaultValue = "")
        {
            return HttpContext.Current.Server.UrlDecode(GetVal(getKey, defaultValue: defaultValue));
        }

        /// <summary>
        /// 获取Post/Get的值(数字)
        /// </summary>
        /// <param name="getKey"></param>
        /// <param name="defaultValue">默认是0</param>
        /// <returns></returns>
        public static int GetValInt(string getKey, int defaultValue = 0)
        {
            //int val = int.TryParse(GetVal(getKey, defaultValue: defaultValue.ToString()), out val) ? val : defaultValue;
            string val = GetVal(getKey, defaultValue: defaultValue.ToString());
            //if (IsNumeric(val))
            try
            {
                return Convert.ToInt32(val);
                //            else
            }
            catch { }
            return defaultValue;
        }

        /// <summary>
        /// 获取Post/Get的值(数字)
        /// </summary>
        /// <param name="getKey"></param>
        /// <param name="defaultValue">默认是0</param>
        /// <returns></returns>
        public static long GetValLong(string getKey, long defaultValue = 0)
        {
            //int val = int.TryParse(GetVal(getKey, defaultValue: defaultValue.ToString()), out val) ? val : defaultValue;
            string val = GetVal(getKey, defaultValue: defaultValue.ToString());
            if (IsNumeric(val))
                return Convert.ToInt64(val);
            else
                return defaultValue;
        }


        /// <summary>
        /// 获取Post/Get的值(decimal)
        /// </summary>
        /// <param name="getKey"></param>
        /// <param name="defaultValue">默认是0</param>
        /// <returns></returns>
        public static decimal GetValDecimal(string getKey, decimal defaultValue = 0)
        {
            decimal val = Decimal.TryParse(GetVal(getKey, defaultValue: defaultValue.ToString()), out val) ? val : defaultValue;
            return val;
        }

        /// <summary>
        /// 获取Post/Get的值(double)
        /// </summary>
        /// <param name="getKey"></param>
        /// <param name="defaultValue">默认是0</param>
        /// <returns></returns>
        public static double GetValDouble(string getKey, double defaultValue = 0)
        {
            double val = Double.TryParse(GetVal(getKey, defaultValue: defaultValue.ToString()), out val) ? val : defaultValue;
            return val;
        }

        /// <summary>
        /// 如果没值就默认值是1900-1-1 (datetime 数据类型存储从 1753 年 1 月 1 日至 9999 年 12 月 31 日的日期,smalldatetime 数据类型存储从 1900 年 1 月 1 日至 2079 年 6 月 6 日的日期。)
        /// </summary>
        /// <param name="getKey"></param>
        /// <param name="defaultDate"></param>
        /// <param name="isLess1900">是否允许日期小于1900年</param>
        /// <returns></returns>
        public static DateTime GetValDateTime(string getKey, DateTime? defaultDate = null, bool isLess1900 = false)
        {
            DateTime dt = new DateTime(1900, 1, 1);
            if (defaultDate.HasValue)
                dt = Convert.ToDateTime(defaultDate);
            DateTime getDt = DateTime.TryParse(GetVal(getKey, defaultValue: defaultDate.ToString()), out getDt) ? getDt : dt;

            if (isLess1900)
                return getDt;
            else
                return getDt.Year <= 1900 ? dt : getDt;
        }

        /// <summary>
        /// 是否为数字
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        private static bool IsNumeric(string strInput)
        {
            if (strInput == "")
                return false;
            char[] ca = strInput.ToCharArray();
            bool found = true;
            for (int i = 0; i < ca.Length; i++)
            {
                if ((ca[i] < '0' || ca[i] > '9') && ca[i] != '.')
                {
                    found = false;
                    break;
                };
            };
            return found;
        }
    }
}
