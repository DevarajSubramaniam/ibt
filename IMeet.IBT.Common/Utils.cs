﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;

namespace IMeet.IBT.Common
{
   public class Utils
    {
        /// <summary>
        /// 获得当前页面客户端的IP
        /// </summary>
        /// <returns>当前页面客户端的IP</returns>
        public static string GetIP()
        {
            string result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(result))
                result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(result))
                result = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(result) || !IsIP(result))
                return "127.0.0.1";

            return result;
        }

        /// <summary>
        /// 是否为ip
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }

        /// <summary>
        /// 是否为数字
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public static bool IsNumeric(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput))
                return false;
            char[] ca = strInput.ToCharArray();
            bool found = true;
            for (int i = 0; i < ca.Length; i++)
            {
                if ((ca[i] < '0' || ca[i] > '9') && ca[i] != '.' && ca[i] !='-')
                {
                    found = false;
                    break;
                };
            };
            return found;
        }

        /// <summary>
        /// 把datetime转换成unixdate
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        ///<remarks>url: http://www.epochconverter.com/ </remarks>
        public static long Epoch(DateTime time)
        {
            return (long)(time - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds;
        }


        /// <summary>
        /// int to byte
        /// </summary>
        /// <param name="val">tinyint类型</param>
        /// <returns></returns>
        public static byte IntToByte(int val)
        {
            //因为数据库中是tinyint类型，数据的长度不会超过1个byte。
            return BitConverter.GetBytes(val)[0];
        }


    }
}
