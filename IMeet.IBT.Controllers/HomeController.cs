﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using IMeet.IBT.Services;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Helpers;

namespace IMeet.IBT.Controllers
{
    public class HomeController : BaseController
    {
        private ISupplierService _supplierService;
        private IWidgetService _widgetService;
        private IMapsService _mapsService;
        public HomeController(IWidgetService widgetService, ISupplierService supplierService, IMapsService mapsService)
        {
            _supplierService = supplierService;
            _widgetService = widgetService;
            _mapsService = mapsService;
        }

        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            this.Title = "home";
            this.Keywords = "keywords";
            this.Description = "description";

            int membersNum = _profileService.GetAllMemberCount();
            ViewBag.MembersNum = membersNum.ToString().PadLeft(6, '0');
            int postedVisitsNum = _supplierService.GetPostedVisitsCount();
            ViewBag.PostedVisits = postedVisitsNum.ToString().PadLeft(6, '0');
            var list = _mapsService.GetIndexTopList();
            System.Text.StringBuilder sb = new StringBuilder();
            sb.Append("[");
            int idx = 1;
            foreach (var item in list)
            {
                sb.Append("['" + item.Title.Replace("'", "\'") + "'," + item.lat.ToString() + "," + item.lng.ToString() + "," + idx + "," + item.Id.ToString() + "," + item.typ.ToString() + ",'" + PageHelper.GetSupplyMapsIcon(item.Id) + "'],\n");
                idx++;
            }
            sb.Append("];");

            ViewBag.MapsTopList = sb.ToString();

            return View();
        }

        /// <summary>
        /// 登录后的首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            ViewBag.DestinationWeekList = _widgetService.GetWidgetDestinationWeekList(UserId, 4);
            ViewBag.RecentPlacesList = _supplierService.GetHomeRecentPlacesList(15);
            ViewBag.MostRecommendedPlaces = _widgetService.GetMostRecommendedPlaces(UserId, 5);
            //ViewBag.MostRecommendedPlaces = _supplierService.GetMostRecommendedPlaces(UserId, 5);
            ViewBag.MostVisitedPlaces = _supplierService.GetMostVisitedPlaces(UserId, 5);

            return View();
        }

    }
}
