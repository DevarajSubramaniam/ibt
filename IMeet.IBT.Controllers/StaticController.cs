﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using IMeet.IBT.Services;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Controllers
{
    public class StaticController:BaseController
    {
        private IWidgetService _widgetService;
        public StaticController(IWidgetService widgetService)
        {
            _widgetService = widgetService;
        }

        /// <summary>
        /// Terms
        /// </summary>
        /// <returns></returns>
        public ActionResult Terms()
        {
            this.Title = "TERMS OF USE";
            this.Keywords = "keywords";
            this.Description = "description";

            return View();
        }


        /// <summary>
        /// Privacy
        /// </summary>
        /// <returns></returns>
        public ActionResult Privacy()
        {
            this.Title = "Privacy";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }

        /// <summary>
        /// Contact
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            this.Title = "Contact";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }

        public ActionResult MPBVideoLanding1()
        {
            return View();
        }
        
        public ActionResult PersonalPageStart()
        {
            return View();
        }

        public ActionResult SpainMICE()
        {
            return View();
        }

        public ActionResult GlobalCorrespondents()
        {
            return View();
        }
        public ActionResult VirtualPharmaEurope2015FAQ()
        {
            return File("/App/pdf/VirtualPharmaEurope2015/FAQ.pdf", "application/pdf");
        }
        public ActionResult VirtualPharmaEurope2015() {
            return View();
        }

    }
}
