﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
namespace IMeet.IBT.Controllers.SNSHelper
{
    public class IMeetBaseInfo
    {
        private static DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(IMeetBaseInfo));

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Email { get; set; }

        public static IMeetBaseInfo Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                throw new ArgumentNullException("json");
            }

            return Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(json)));
        }

        public static IMeetBaseInfo Deserialize(Stream jsonStream)
        {
            if (jsonStream == null)
            {
                throw new ArgumentNullException("jsonStream");
            }

            return (IMeetBaseInfo)jsonSerializer.ReadObject(jsonStream);
        }
    }
}
