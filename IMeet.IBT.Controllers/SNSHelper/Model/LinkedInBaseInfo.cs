﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.Controllers.SNSHelper
{
    public class LinkedInBaseInfo
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string country { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
    }
}
