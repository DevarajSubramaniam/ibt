﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMeet.IBT.Common.Helpers;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

using StructureMap;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Logging;
using IMeet.IBT.Common.Caching;
using IMeet.IBT.Services;

namespace IMeet.IBT.Controllers.SNSHelper
{
    public class FriendList
    {
        public static List<friendData> GetFriendList(string token)
        {
            var _cache = ObjectFactory.GetInstance<ICache>();
            return _cache.GetOrInsert<List<friendData>>(token, 1200, true, () =>
            {
                string firstUrl = "https://graph.facebook.com/me/friends?fields=id,name,installed,picture.type(small)&offset=0&limit=5000&access_token=" + token;
                List<friendData> list = new List<friendData>();
                string nextUrl = string.Empty;
                list = GetFriendList(token, firstUrl, out nextUrl);

                //todo:暂时就取第一页 5000个好友
                return list;
            });
        }

        private static List<friendData> GetFriendList(string token, string url,out string nextUrl)
        {
            List<friendData> list = new List<friendData>();
            nextUrl = string.Empty;
            try 
	        {	
        
		        Dictionary<string, string> dic = new Dictionary<string, string>();
                //dic.Add("access_token", token);
                var bm = HttpHelper.WebRequest(HttpHelper.Method.GET, url, "");
                if (bm.Success)
                {
                    string jsonData = bm.Message;

                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(FacebookFriend));
                    FacebookFriend friends = (FacebookFriend)jsonSerializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(jsonData)));
                    nextUrl = friends.paging.next;
                    list = friends.data;
                }
	        }
	        catch (Exception)
	        {
	        }

            return list;
        }
    }


    public class FacebookFriend
    {
        public List<friendData> data { get; set; }
        public paging paging { get; set; }
    }

    public class friendData
    {
        public string id { get; set; }
        public int? UserId { get; set; }
        public string name { get; set; }
        public picture picture { get; set; }
        public bool? installed { get; set; }
    }

    public class picture
    {
        public picturedata data { get; set; }
    }

    public class picturedata
    {
        public string url { get; set; }
        public bool? is_silhouette { get; set; }
    }

    public class paging {
        public string next { get; set; }
    }

}

