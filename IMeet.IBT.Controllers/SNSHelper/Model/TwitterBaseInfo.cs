﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.Controllers.SNSHelper
{
    public class TwitterBaseInfo
    {
        public string Id { get; set; }
        /// <summary>
        /// name max 30
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// screen_name max 30
        /// </summary>
        public string LastName { get; set; }
        public string country { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
    }
}
