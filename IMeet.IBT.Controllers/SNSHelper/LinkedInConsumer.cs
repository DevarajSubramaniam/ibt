﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.OAuth.ChannelElements;

using StructureMap;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Logging;
using IMeet.IBT.Services;
using IMeet.IBT.Common.Helpers;

namespace IMeet.IBT.Controllers.SNSHelper
{
    public static class LinkedInConsumer
    {
        /// <summary>
        /// The description of LinkedIn's OAuth protocol URIs for use with actually reading/writing
        /// a user's private LinkedIn data.
        /// </summary>
        public static readonly ServiceProviderDescription ServiceDescription = new ServiceProviderDescription
        {
            AccessTokenEndpoint = new MessageReceivingEndpoint("https://api.linkedin.com/uas/oauth/accessToken", HttpDeliveryMethods.PostRequest),
            RequestTokenEndpoint = new MessageReceivingEndpoint("https://api.linkedin.com/uas/oauth/requestToken", HttpDeliveryMethods.PostRequest),
            UserAuthorizationEndpoint = new MessageReceivingEndpoint("https://www.linkedin.com/uas/oauth/authorize", HttpDeliveryMethods.PostRequest),
            TamperProtectionElements = new ITamperProtectionChannelBindingElement[] { new HmacSha1SigningBindingElement() },
            ProtocolVersion = ProtocolVersion.V10a
        };

        //https://api.linkedin.com/uas/oauth/requestToken?scope=r_basicprofile+r_emailaddress
        //http://developer.linkedin.com/documents/authentication
        /// <summary>
        /// The description of LinkedIn's OAuth protocol URIs for use with their "Sign in with LinkedIn" feature.
        /// </summary>
        public static readonly ServiceProviderDescription SignInWithLinkedInServiceDescription = new ServiceProviderDescription
        {
            RequestTokenEndpoint = new MessageReceivingEndpoint("https://api.linkedin.com/uas/oauth/requestToken?scope=r_fullprofile+r_emailaddress+rw_nus", HttpDeliveryMethods.PostRequest),
            UserAuthorizationEndpoint = new MessageReceivingEndpoint("https://www.linkedin.com/uas/oauth/authenticate", HttpDeliveryMethods.PostRequest),
            AccessTokenEndpoint = new MessageReceivingEndpoint("https://api.linkedin.com/uas/oauth/accessToken", HttpDeliveryMethods.PostRequest),
            TamperProtectionElements = new ITamperProtectionChannelBindingElement[] { new HmacSha1SigningBindingElement() },
        };

        private static readonly MessageReceivingEndpoint BaseInfoEndpoint = new MessageReceivingEndpoint("http://api.linkedin.com/v1/people/~:(id,first-name,last-name,picture-url,industry,email-address,location)", HttpDeliveryMethods.GetRequest);

        /// <summary>
        /// The consumer used for the Sign in to LinkedIn feature.
        /// </summary>
        private static WebConsumer signInConsumer;

        /// <summary>
        /// The lock acquired to initialize the <see cref="signInConsumer"/> field.
        /// </summary>
        private static object signInConsumerInitLock = new object();

        /// <summary>
        /// Initializes static members of the <see cref="LinkedInConsumer"/> class.
        /// </summary>
        static LinkedInConsumer()
        {
            // LinkedIn can't handle the Expect 100 Continue HTTP header. 
            //ServicePointManager.FindServicePoint(GetFavoritesEndpoint.Location).Expect100Continue = false;
        }

        /// <summary>
        /// Gets a value indicating whether the LinkedIn consumer key and secret are set in the web.config file.
        /// </summary>
        public static bool IsLinkedInConsumerConfigured
        {
            get
            {
                return !string.IsNullOrEmpty(ConfigurationManager.AppSettings["LinkedInConsumerKey"]) &&
                    !string.IsNullOrEmpty(ConfigurationManager.AppSettings["LinkedInConsumerSecret"]);
            }
        }

        /// <summary>
        /// Gets the consumer to use for the Sign in to LinkedIn feature.
        /// </summary>
        /// <value>The LinkedIn sign in.</value>
        public static WebConsumer LinkedInSignIn
        {
            get
            {
                if (signInConsumer == null)
                {
                    lock (signInConsumerInitLock)
                    {
                        if (signInConsumer == null)
                        {
                            signInConsumer = new WebConsumer(SignInWithLinkedInServiceDescription, ShortTermUserSessionTokenManager);
                        }
                    }
                }

                return signInConsumer;
            }
        }

        public static LinkedInTokenManager ShortTermUserSessionTokenManager
        {
            get
            {
                var store = HttpContext.Current.Session;
                var tokenManager = (LinkedInTokenManager)store["LinkedInShortTermUserSessionTokenManager"];
                if (tokenManager == null)
                {
                    string consumerKey = ConfigurationManager.AppSettings["LinkedInConsumerKey"];
                    string consumerSecret = ConfigurationManager.AppSettings["LinkedInConsumerSecret"];
                    if (IsLinkedInConsumerConfigured)
                    {
                        tokenManager = new LinkedInTokenManager(consumerKey, consumerSecret);
                        store["LinkedInShortTermUserSessionTokenManager"] = tokenManager;
                    }
                    else
                    {
                        throw new InvalidOperationException("No LinkedIn OAuth consumer key and secret could be found in web.config AppSettings.");
                    }
                }

                return tokenManager;
            }
        }

        public static XDocument GetBaseInfo(ConsumerBase linkedIn, string accessToken)
        {
            IncomingWebResponse response = linkedIn.PrepareAuthorizedRequestAndSend(BaseInfoEndpoint, accessToken);
            return XDocument.Load(XmlReader.Create(response.GetResponseReader()));
        }

        public static OutgoingWebResponse StartSignInWithhLinkedIn()
        {
            //http://developer.linkedin.com/documents/authentication
            //https://api.linkedin.com/uas/oauth/requestToken?scope=r_fullprofile+r_emailaddress
            //http://developer.linkedin.com/documents/profile-api  //r_basicprofile
            var redirectParameters = new Dictionary<string, string>();
            //redirectParameters["scope"] = "r_emailaddress";
            //Uri callback = MessagingUtilities.GetRequestUrlFromContext().StripQueryArgumentsWithPrefix("oauth_");
            Uri callback = new Uri(HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/sns/LinkedInCallback"));
            var request = LinkedInSignIn.PrepareRequestUserAuthorization(callback, null, redirectParameters);
            return LinkedInSignIn.Channel.PrepareResponse(request);
        }

        /// <summary>
        /// 返回用户的基本资料
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static LinkedInBaseInfo GetUserBaseInfo(string accessToken)
        {
            LinkedInBaseInfo baseInfo = new LinkedInBaseInfo();
            try
            {
                var linkedIn = new WebConsumer(LinkedInConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
                IncomingWebResponse response = linkedIn.PrepareAuthorizedRequestAndSend(BaseInfoEndpoint, accessToken);
                XDocument xdoc = XDocument.Load(XmlReader.Create(response.GetResponseReader()));

                XElement xe_id = xdoc.XPathSelectElement("/person/id");
                baseInfo.Id = xe_id == null ? "" : xe_id.Value;
                XElement xe_fristName = xdoc.XPathSelectElement("/person/first-name");
                baseInfo.FirstName = xe_fristName == null ? "" : xe_fristName.Value;
                XElement xe_lastName = xdoc.XPathSelectElement("/person/last-name");
                baseInfo.LastName = xe_lastName == null ? "" : xe_lastName.Value;
                XElement xe_email = xdoc.XPathSelectElement("/person/email-address");
                baseInfo.Email = xe_email == null ? "" : xe_email.Value;
                XElement xe_localtion = xdoc.XPathSelectElement("/person/location/name");
                string localtion = xe_localtion == null ? "," : xe_localtion.Value;
                if (localtion.Split(',').Length == 2)
                {
                    baseInfo.City = localtion.Split(',')[0].Replace(" City", "");
                    baseInfo.country = localtion.Split(',')[1];
                }
            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("LinkedIn GetUserBaseInfo Error", ex);
            }
            return baseInfo;
        }

        public static BoolMessage Share4(string accessToken, string xml)
        {
            BoolMessage bm = new BoolMessage(false, "");
            try
            {
                MessageReceivingEndpoint shareEndpoint = new MessageReceivingEndpoint("http://api.linkedin.com/v1/people/~/shares", HttpDeliveryMethods.PostRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);
                var linkedIn = new WebConsumer(LinkedInConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
                var parts = new[] {
				    MultipartPostPart.CreateFormFilePart("share_xml","share_xml","text/xml",new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml)))
                    //MultipartPostPart.CreateFormPart("comment","Check out the LinkedIn Share API!"),
                    //MultipartPostPart.CreateFormPart("title","LinkedIn Developers Documentation On Using the Share API"),
                    //MultipartPostPart.CreateFormPart("description","description description API!"),
                    //MultipartPostPart.CreateFormPart("submitted-url","https://developer.linkedin.com/documents/share-api!"),
                    //MultipartPostPart.CreateFormPart("submitted-image-url","http://m3.licdn.com/media/p/3/000/124/1a6/089a29a.png API!"),
                    //MultipartPostPart.CreateFormPart("code","anyone")
                };
                HttpWebRequest request = linkedIn.PrepareAuthorizedRequest(shareEndpoint, accessToken, parts);
                IncomingWebResponse response = linkedIn.Channel.WebRequestHandler.GetResponse(request);
                XDocument xdoc = XDocument.Load(XmlReader.Create(response.GetResponseReader()));

                if (xdoc != null)
                {
                    bm = new BoolMessage(true, xdoc.ToString());
                }
            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("LinkedIn Share Error", ex);
            }

            return bm;

        }

        public static BoolMessage Share(string token,string xml)
        {
            //http://stackoverflow.com/questions/6192644/dotnetopenauth-post-sample-with-json-body
            MessageReceivingEndpoint endpoint = new MessageReceivingEndpoint("http://api.linkedin.com/v1/people/~/shares", HttpDeliveryMethods.PostRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);
            var consumer = new WebConsumer(LinkedInConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
            HttpWebRequest httpRequest = consumer.PrepareAuthorizedRequest(endpoint, token);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://api.linkedin.com/v1/people/~/shares");

            foreach (string headerKey in httpRequest.Headers.AllKeys)
                httpWebRequest.Headers.Add(headerKey, httpRequest.Headers[headerKey]);

            httpWebRequest.ContentType = "application/xml";
            httpWebRequest.Method = "POST";

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xml);
                requestStream.Write(bytes, 0, bytes.Length);
            }
            //注意：这shares返回的是201(Created)状态
            var bm = HttpHelper.WebRequestStatusCode(httpWebRequest);
            return bm; 

        }

        /// <summary>
        /// 获取用户的大头像
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static BoolMessage GetUserOriginalPictureUrl(string accessToken)
        {
            BoolMessage bm = new BoolMessage(false, "");
            try
            {
                MessageReceivingEndpoint PictureUrlEndpoint = new MessageReceivingEndpoint("http://api.linkedin.com/v1/people/~/picture-urls::(original)", HttpDeliveryMethods.GetRequest);
                var linkedIn = new WebConsumer(LinkedInConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
                IncomingWebResponse response = linkedIn.PrepareAuthorizedRequestAndSend(PictureUrlEndpoint, accessToken);
                XDocument xdoc = XDocument.Load(XmlReader.Create(response.GetResponseReader()));

                //<picture-urls total="1">
                //  <picture-url key="original">http://m3.licdn.com/mpr/mprx/0_PKGMavx1_xMAAIroAiSBSqYgCmLl9spoAPyqHqCAuUk-AUmkP8_vEtnrpBV</picture-url>
                //</picture-urls>
                XElement xeUrl = xdoc.XPathSelectElement("/picture-urls/picture-url[@key='original']");
                if (xeUrl != null)
                {
                    bm = new BoolMessage(true, xeUrl.Value);
                }
            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("LinkedIn GetUserOriginalPictureUrl Error", ex);
            }

            return bm;
        }

        public static bool TryFinishSignInWithLinkedIn(out string screenName, out int userId)
        {
            screenName = null;
            userId = 0;
            var response = LinkedInSignIn.ProcessUserAuthorization();
            if (response == null)
            {
                return false;
            }

            HttpContext.Current.Response.Write("token:" + response.AccessToken + "<br />");
            HttpContext.Current.Response.Write("Screct:" + ShortTermUserSessionTokenManager.GetTokenSecret(response.AccessToken) + "<br />");

            /*
            screenName = response.ExtraData["screen_name"];
            userId = int.Parse(response.ExtraData["user_id"]);

            var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
            string name = TwitterConsumer.GetUsername(twitter, response.AccessToken);
            HttpContext.Current.Response.Write("name:" + name);

            // If we were going to make this LOOK like OpenID even though it isn't,
            // this seems like a reasonable, secure claimed id to allow the user to assume.
            OpenId.Identifier fake_claimed_id = string.Format(CultureInfo.InvariantCulture, "http://twitter.com/{0}#{1}", screenName, userId);
            */
            return true;
        }
    }
}
