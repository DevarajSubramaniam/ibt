﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.OAuth.ChannelElements;

using StructureMap;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Logging;
using IMeet.IBT.Services;

using System.Web;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace IMeet.IBT.Controllers.SNSHelper
{
    /// <summary>
    /// A consumer capable of communicating with Twitter.
    /// </summary>
    public static class TwitterConsumer
    {
        /// <summary>
        /// The description of Twitter's OAuth protocol URIs for use with actually reading/writing
        /// a user's private Twitter data.
        /// </summary>
        public static readonly ServiceProviderDescription ServiceDescription = new ServiceProviderDescription
        {
            RequestTokenEndpoint = new MessageReceivingEndpoint("https://twitter.com/oauth/request_token", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest),
            UserAuthorizationEndpoint = new MessageReceivingEndpoint("https://twitter.com/oauth/authorize", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest),
            AccessTokenEndpoint = new MessageReceivingEndpoint("https://twitter.com/oauth/access_token", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest),
            TamperProtectionElements = new ITamperProtectionChannelBindingElement[] { new HmacSha1SigningBindingElement() },
        };

        /// <summary>
        /// The description of Twitter's OAuth protocol URIs for use with their "Sign in with Twitter" feature.
        /// </summary>
        public static readonly ServiceProviderDescription SignInWithTwitterServiceDescription = new ServiceProviderDescription
        {
            RequestTokenEndpoint = new MessageReceivingEndpoint("https://twitter.com/oauth/request_token", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest),
            UserAuthorizationEndpoint = new MessageReceivingEndpoint("https://twitter.com/oauth/authenticate", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest),
            AccessTokenEndpoint = new MessageReceivingEndpoint("https://twitter.com/oauth/access_token", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest),
            TamperProtectionElements = new ITamperProtectionChannelBindingElement[] { new HmacSha1SigningBindingElement() },
        };

        /// <summary>
        /// The URI to get a user's favorites.
        /// </summary>
        private static readonly MessageReceivingEndpoint GetFavoritesEndpoint = new MessageReceivingEndpoint("http://twitter.com/favorites.xml", HttpDeliveryMethods.GetRequest);

        /// <summary>
        /// The URI to get the data on the user's home page.
        /// </summary>
        private static readonly MessageReceivingEndpoint GetFriendTimelineStatusEndpoint = new MessageReceivingEndpoint("http://twitter.com/statuses/friends_timeline.xml", HttpDeliveryMethods.GetRequest);

        private static readonly MessageReceivingEndpoint UpdateProfileBackgroundImageEndpoint = new MessageReceivingEndpoint("http://twitter.com/account/update_profile_background_image.xml", HttpDeliveryMethods.PostRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);

        private static readonly MessageReceivingEndpoint UpdateProfileImageEndpoint = new MessageReceivingEndpoint("http://twitter.com/account/update_profile_image.xml", HttpDeliveryMethods.PostRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);

        private static readonly MessageReceivingEndpoint VerifyCredentialsEndpoint = new MessageReceivingEndpoint("http://api.twitter.com/1/account/verify_credentials.xml", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);

        /// <summary>
        /// The consumer used for the Sign in to Twitter feature.
        /// </summary>
        private static WebConsumer signInConsumer;

        /// <summary>
        /// The lock acquired to initialize the <see cref="signInConsumer"/> field.
        /// </summary>
        private static object signInConsumerInitLock = new object();

        /// <summary>
        /// Initializes static members of the <see cref="TwitterConsumer"/> class.
        /// </summary>
        static TwitterConsumer()
        {
            // Twitter can't handle the Expect 100 Continue HTTP header. 
            ServicePointManager.FindServicePoint(GetFavoritesEndpoint.Location).Expect100Continue = false;
        }

        /// <summary>
        /// Gets a value indicating whether the Twitter consumer key and secret are set in the web.config file.
        /// </summary>
        public static bool IsTwitterConsumerConfigured
        {
            get
            {
                return !string.IsNullOrEmpty(ConfigurationManager.AppSettings["twitterConsumerKey"]) &&
                    !string.IsNullOrEmpty(ConfigurationManager.AppSettings["twitterConsumerSecret"]);
            }
        }

        /// <summary>
        /// Gets the consumer to use for the Sign in to Twitter feature.
        /// </summary>
        /// <value>The twitter sign in.</value>
        public static WebConsumer TwitterSignIn
        {
            get
            {
                if (signInConsumer == null)
                {
                    lock (signInConsumerInitLock)
                    {
                        if (signInConsumer == null)
                        {
                            signInConsumer = new WebConsumer(SignInWithTwitterServiceDescription, ShortTermUserSessionTokenManager);
                        }
                    }
                }

                return signInConsumer;
            }
        }

        public static TwitterTokenManager ShortTermUserSessionTokenManager
        {
            get
            {
                var store = HttpContext.Current.Session;
                var tokenManager = (TwitterTokenManager)store["TwitterShortTermUserSessionTokenManager"];
                if (tokenManager == null)
                {
                    string consumerKey = ConfigurationManager.AppSettings["twitterConsumerKey"];
                    string consumerSecret = ConfigurationManager.AppSettings["twitterConsumerSecret"];
                    if (IsTwitterConsumerConfigured)
                    {
                        tokenManager = new TwitterTokenManager(consumerKey, consumerSecret);
                        store["TwitterShortTermUserSessionTokenManager"] = tokenManager;
                    }
                    else
                    {
                        throw new InvalidOperationException("No Twitter OAuth consumer key and secret could be found in web.config AppSettings.");
                    }
                }

                return tokenManager;
            }
        }

        public static XDocument GetUpdates(ConsumerBase twitter, string accessToken)
        {
            IncomingWebResponse response = twitter.PrepareAuthorizedRequestAndSend(GetFriendTimelineStatusEndpoint, accessToken);
            return XDocument.Load(XmlReader.Create(response.GetResponseReader()));
        }

        public static XDocument GetFavorites(ConsumerBase twitter, string accessToken)
        {
            IncomingWebResponse response = twitter.PrepareAuthorizedRequestAndSend(GetFavoritesEndpoint, accessToken);
            return XDocument.Load(XmlReader.Create(response.GetResponseReader()));
        }

        public static XDocument UpdateProfileBackgroundImage(ConsumerBase twitter, string accessToken, string image, bool tile)
        {
            var parts = new[] {
				MultipartPostPart.CreateFormFilePart("image", image, "image/" + Path.GetExtension(image).Substring(1).ToLowerInvariant()),
				MultipartPostPart.CreateFormPart("tile", tile.ToString().ToLowerInvariant()),
			};
            HttpWebRequest request = twitter.PrepareAuthorizedRequest(UpdateProfileBackgroundImageEndpoint, accessToken, parts);
            request.ServicePoint.Expect100Continue = false;
            IncomingWebResponse response = twitter.Channel.WebRequestHandler.GetResponse(request);
            string responseString = response.GetResponseReader().ReadToEnd();
            return XDocument.Parse(responseString);
        }

        public static XDocument UpdateProfileImage(ConsumerBase twitter, string accessToken, string pathToImage)
        {
            string contentType = "image/" + Path.GetExtension(pathToImage).Substring(1).ToLowerInvariant();
            return UpdateProfileImage(twitter, accessToken, File.OpenRead(pathToImage), contentType);
        }

        public static XDocument UpdateProfileImage(ConsumerBase twitter, string accessToken, Stream image, string contentType)
        {
            var parts = new[] {
				MultipartPostPart.CreateFormFilePart("image", "twitterPhoto", contentType, image),
			};
            HttpWebRequest request = twitter.PrepareAuthorizedRequest(UpdateProfileImageEndpoint, accessToken, parts);
            IncomingWebResponse response = twitter.Channel.WebRequestHandler.GetResponse(request);
            string responseString = response.GetResponseReader().ReadToEnd();
            return XDocument.Parse(responseString);
        }

        /// <summary>
        /// 发布微博
        /// </summary>
        /// <param name="twitter"></param>
        /// <param name="accessToken"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static string StatusesUpdate(string accessToken, string status)
        {
            string responseString = "";
            try
            {
                if (status.Length > 140)
                    throw new Exception("twitter 发布内容长度不可超过140");
                var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
                string url = "https://api.twitter.com/1.1/statuses/update.json";
                MessageReceivingEndpoint UpdateEndpoint = new MessageReceivingEndpoint(url, HttpDeliveryMethods.PostRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);
                var parts = new[] { 
                MultipartPostPart.CreateFormPart("status",status)
            };

                HttpWebRequest request = twitter.PrepareAuthorizedRequest(UpdateEndpoint, accessToken, parts);
                IncomingWebResponse response = twitter.Channel.WebRequestHandler.GetResponse(request);
                responseString = response.GetResponseReader().ReadToEnd();
            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("Twitter Share Error", ex);
            }

            return responseString;
        }

        /// <summary>
        /// 发布带图片的微博
        /// </summary>
        /// <param name="twitter"></param>
        /// <param name="accessToken"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static string StatusesUpdate(string accessToken, string status, string imagePath)
        {
            string responseString = "";
            try
            {
                if (status.Length > 140)
                    throw new Exception("twitter 发布内容长度不可超过140");
                var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
                string url = "https://api.twitter.com/1.1/statuses/update_with_media.json";
                MessageReceivingEndpoint UpdateEndpoint = new MessageReceivingEndpoint(url, HttpDeliveryMethods.PostRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);
                var parts = new[] { 
                    MultipartPostPart.CreateFormPart("status",status),
                    MultipartPostPart.CreateFormFilePart("media[]", imagePath, "image/" + Path.GetExtension(imagePath).Substring(1).ToLowerInvariant()),
                };

                HttpWebRequest request = twitter.PrepareAuthorizedRequest(UpdateEndpoint, accessToken, parts);
                IncomingWebResponse response = twitter.Channel.WebRequestHandler.GetResponse(request);
                responseString = response.GetResponseReader().ReadToEnd();
            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("Twitter Share Error", ex);
            }
            return responseString;
        }

        public static XDocument VerifyCredentials(ConsumerBase twitter, string accessToken)
        {
            IncomingWebResponse response = twitter.PrepareAuthorizedRequestAndSend(VerifyCredentialsEndpoint, accessToken);
            return XDocument.Load(XmlReader.Create(response.GetResponseReader()));
        }

        public static string GetUsername(ConsumerBase twitter, string accessToken)
        {
            XDocument xml = VerifyCredentials(twitter, accessToken);
            XPathNavigator nav = xml.CreateNavigator();
            return nav.SelectSingleNode("/user/screen_name").Value;
        }

        /// <summary>
        /// Prepares a redirect that will send the user to Twitter to sign in.
        /// </summary>
        /// <param name="forceNewLogin">if set to <c>true</c> the user will be required to re-enter their Twitter credentials even if already logged in to Twitter.</param>
        /// <returns>The redirect message.</returns>
        /// <remarks>
        /// Call <see cref="OutgoingWebResponse.Send"/> or
        /// <c>return StartSignInWithTwitter().<see cref="MessagingUtilities.AsActionResult">AsActionResult()</see></c>
        /// to actually perform the redirect.
        /// </remarks>
        public static OutgoingWebResponse StartSignInWithTwitter(bool forceNewLogin)
        {
            var redirectParameters = new Dictionary<string, string>();
            if (forceNewLogin)
            {
                redirectParameters["force_login"] = "true";
            }
            //Uri callback = MessagingUtilities.GetRequestUrlFromContext().StripQueryArgumentsWithPrefix("oauth_");
            Uri callback = new Uri(HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/sns/TwitterCallback"));
            var request = TwitterSignIn.PrepareRequestUserAuthorization(callback, null, redirectParameters);
            return TwitterSignIn.Channel.PrepareResponse(request);
        }

        /// <summary>
        /// 返回用户的基本资料
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static TwitterBaseInfo GetUserBaseInfo(string accessToken)
        {
            TwitterBaseInfo baseInfo = new TwitterBaseInfo();
            try
            {
                //API v1.1
                //https://dev.twitter.com/docs/api/1.1/get/account/verify_credentials
                //https://api.twitter.com/1.1/account/verify_credentials.json
                string url = "https://api.twitter.com/1.1/account/verify_credentials.json?include_entities=false";
                MessageReceivingEndpoint verify_credentilsEndpoint = new MessageReceivingEndpoint(url, HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);
                var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
                IncomingWebResponse response = twitter.PrepareAuthorizedRequestAndSend(verify_credentilsEndpoint, accessToken);
                string json = response.GetResponseReader().ReadToEnd();
                JavaScriptSerializer jss = new JavaScriptSerializer();

                TwitterUserInfo user = jss.Deserialize<TwitterUserInfo>(json);

                baseInfo.Id = user.id;
                baseInfo.FirstName = StringHelper.Truncate(user.screen_name, 30);
                baseInfo.LastName = StringHelper.Truncate(user.name, 30);

                //API v1.0
                //https://dev.twitter.com/docs/api/1/get/account/verify_credentials
                //MessageReceivingEndpoint verify_credentialsEndpoint = new MessageReceivingEndpoint("http://api.twitter.com/1/account/verify_credentials.xml?include_entities=false", HttpDeliveryMethods.GetRequest | HttpDeliveryMethods.AuthorizationHeaderRequest);
                //var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
                //IncomingWebResponse response = twitter.PrepareAuthorizedRequestAndSend(verify_credentialsEndpoint, accessToken);
                //XDocument xdoc = XDocument.Load(XmlReader.Create(response.GetResponseReader()));

                ///*
                //      <id>123596341</id>
                //      <name>Randy</name>
                //      <screen_name>fxsyn</screen_name>
                // */
                //XElement xe_id = xdoc.XPathSelectElement("/user/id");
                //baseInfo.Id = xe_id == null ? "" : xe_id.Value;
                //XElement xe_fristName = xdoc.XPathSelectElement("/user/screen_name");
                //baseInfo.FirstName = xe_fristName == null ? "" : StringHelper.Truncate(xe_fristName.Value, 30);
                //XElement xe_lastName = xdoc.XPathSelectElement("/user/name");
                //baseInfo.LastName = xe_lastName == null ? "" : StringHelper.Truncate(xe_lastName.Value, 30);
            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("Twitter GetUserBaseInfo Error", ex);
            }
            return baseInfo;
        }

        /// <summary>
        /// Checks the incoming web request to see if it carries a Twitter authentication response,
        /// and provides the user's Twitter screen name and unique id if available.
        /// </summary>
        /// <param name="screenName">The user's Twitter screen name.</param>
        /// <param name="userId">The user's Twitter unique user ID.</param>
        /// <returns>
        /// A value indicating whether Twitter authentication was successful;
        /// otherwise <c>false</c> to indicate that no Twitter response was present.
        /// </returns>
        public static bool TryFinishSignInWithTwitter(out string screenName, out int userId)
        {
            screenName = null;
            userId = 0;
            var response = TwitterSignIn.ProcessUserAuthorization();
            if (response == null)
            {
                return false;
            }

            HttpContext.Current.Response.Write("token:" + response.AccessToken + "<br />");
            HttpContext.Current.Response.Write("Screct:" + ShortTermUserSessionTokenManager.GetTokenSecret(response.AccessToken) + "<br />");


            screenName = response.ExtraData["screen_name"];
            userId = int.Parse(response.ExtraData["user_id"]);

            var twitter = new WebConsumer(TwitterConsumer.ServiceDescription, ShortTermUserSessionTokenManager);
            string name = TwitterConsumer.GetUsername(twitter, response.AccessToken);
            HttpContext.Current.Response.Write("name:" + name);

            return true;
        }
    }

    public class TwitterUserInfo
    {
        public string id { get; set; }
        public string name { get; set; }
        public string screen_name { get; set; }
    }
}
