﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DotNetOpenAuth.OAuth.ChannelElements;
using DotNetOpenAuth.OAuth.Messages;
using DotNetOpenAuth.OpenId.Extensions.OAuth;

using IMeet.IBT.DAL;
using StructureMap;
using IMeet.IBT.Services;
using IMeet.IBT.Common;

namespace IMeet.IBT.Controllers.SNSHelper
{
    public class LinkedInTokenManager : IConsumerTokenManager, IOpenIdOAuthTokenManager
    {
        private Dictionary<string, string> tokensAndSecrets = new Dictionary<string, string>();
        //private TokenType _tokenType;
        /// <summary>
        /// Initializes a new instance of the <see cref="LinkedInTokenManager"/> class.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        public LinkedInTokenManager(string consumerKey, string consumerSecret)
        {
            if (string.IsNullOrEmpty(consumerKey))
            {
                throw new ArgumentNullException("consumerKey");
            }

            this.ConsumerKey = consumerKey;
            this.ConsumerSecret = consumerSecret;
        }

        public string ConsumerKey { get; private set; }

        public string ConsumerSecret { get; private set; }

        #region ITokenManager Members

        public void ExpireRequestTokenAndStoreNewAccessToken(string consumerKey, string requestToken, string accessToken, string accessTokenSecret)
        {
            this.tokensAndSecrets.Remove(requestToken);
            this.tokensAndSecrets[accessToken] = accessTokenSecret;
            System.Web.HttpContext.Current.Cache.Remove(requestToken);
            System.Web.HttpContext.Current.Cache.Insert(accessToken, accessTokenSecret, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero);

            //this._tokenType = TokenType.AccessToken;
        }

        public string GetTokenSecret(string token)
        {
            //return this.tokensAndSecrets[token];
            //3ee9e09e-93ba-46b5-a3fe-0b1f8ea765ee
            //return "d52f90e6-17cc-4d38-8380-9ce11272b2b2";
            string secretToken = "";
            object secrets = System.Web.HttpContext.Current.Cache.Get(token);
            if (secrets != null && !string.IsNullOrWhiteSpace(secrets.ToString()))
            {
                secretToken = secrets.ToString(); 
            }else{
                var snsService = ObjectFactory.GetInstance<ISNSConnectService>();
                var item = snsService.GetSNSConnectByAccessTokenToken(token, Identifier.SnsType.LinkedIn);
                secretToken = item.OauthTokenSecret;
                System.Web.HttpContext.Current.Cache.Insert(token, secretToken, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero);
            }

            return secretToken;
        }

        public TokenType GetTokenType(string token)
        {
            throw new NotImplementedException();
            //return this._tokenType;
        }

        public void StoreNewRequestToken(UnauthorizedTokenRequest request, ITokenSecretContainingMessage response)
        {
            this.tokensAndSecrets[response.Token] = response.TokenSecret;
            //this._tokenType = TokenType.RequestToken;
            string key = response.Token;
            System.Web.HttpContext.Current.Cache.Insert(key, response.TokenSecret, null, DateTime.Now.AddMinutes(30),TimeSpan.Zero);
        }
        #endregion


        #region IOpenIdOAuthTokenManager Members
        public void StoreOpenIdAuthorizedRequestToken(string consumerKey, AuthorizationApprovedResponse authorization)
        {
            this.tokensAndSecrets[authorization.RequestToken] = string.Empty;
            System.Web.HttpContext.Current.Cache.Remove(authorization.RequestToken);
        }
        #endregion
    }

}
