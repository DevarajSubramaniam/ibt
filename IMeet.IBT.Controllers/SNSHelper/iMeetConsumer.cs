﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using IMeet.IBT.Common.Helpers;
using StructureMap;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Logging;
namespace IMeet.IBT.Controllers.SNSHelper
{
    public static class iMeetConsumer
    {
        public static void SignUpWithiMeet()
        {
            //return Redirect("http://imeet.com/uas/Authorize.aspx?oauth_token=GRIfzLEduiilWKRH83ethjX92zI=");

            string RequestUrl = ConfigurationManager.AppSettings["imeetSiteURL"] + "ibt/IBTService.asmx/RequestToken";

            string oauth_signature = "";

            //构造请求获取未授权的RequestToken
            Dictionary<string, string> Paras = new Dictionary<string, string>();
            //Paras.Add("oauth_signature_method", "HMAC-SHA1");
            //Paras.Add("oauth_consumer_key", "");
            Uri callback = new Uri(HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/sns/iMeetCallback"));
            Paras.Add("oauth_callback", callback.ToString());
            Paras.Add("time", DateTime.Now.ToFileTimeUtc().ToString());
            oauth_signature = iMeetTokenManager.CreateOauthSignature(Paras, RequestUrl, "GET", "", string.Empty);

            Paras.Add("oauth_token", iMeetTokenManager.RFC3986_UrlEncode(oauth_signature));

            string strRedirect = ConfigurationManager.AppSettings["imeetSiteURL"] + "ibt/Authorize.aspx?" + Paras.ToQueryString();
            //result = iMeetTokenManager.WebServiceGet(RequestUrl, Paras.ToQueryString());
            HttpContext.Current.Response.Redirect(strRedirect);
        }

        public static IMeetBaseInfo GetUserBaseInfo(string token, string email)
        {
            Dictionary<string, string> Paras = new Dictionary<string, string>();
            Paras.Add("token", HttpContext.Current.Server.UrlEncode(token));
            Paras.Add("email", HttpContext.Current.Server.UrlEncode(email));
            string RequestUrl = ConfigurationManager.AppSettings["imeetSiteURL"] + "ibt/IBTService.asmx/GetUserInfo";
            string result = iMeetTokenManager.WebServiceGet(RequestUrl, Paras.ToQueryString());

            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(result);
            XmlNode node = xdoc.LastChild;
            string json = node.InnerText.TrimStart('[').TrimEnd(']');

            IMeetBaseInfo user = new IMeetBaseInfo();
            if (!string.IsNullOrWhiteSpace(json))
            {
                user = IMeetBaseInfo.Deserialize(json);
            }
            return user;
        }

        /// <summary>
        /// 发布微博
        /// </summary>
        /// <param name="token"></param>
        /// <param name="email"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static string StatusesUpdate(string token, string email, string status)
        {
            string responseString = "";
            try
            {
                Dictionary<string, string> Paras = new Dictionary<string, string>();
                Paras.Add("token", HttpContext.Current.Server.UrlEncode(token));
                Paras.Add("email", HttpContext.Current.Server.UrlEncode(email));
                Paras.Add("status", HttpContext.Current.Server.UrlEncode(status));
                string RequestUrl = ConfigurationManager.AppSettings["imeetSiteURL"] + "ibt/IBTService.asmx/StatusUpdate";
                string result = iMeetTokenManager.WebServiceGet(RequestUrl, Paras.ToQueryString());

                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(result);
                XmlNode node = xdoc.LastChild;
                responseString = node.InnerText;


            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("i-Meet Share Error", ex);
            }

            return responseString;
        }

        /// <summary>
        /// 带图片的Status
        /// </summary>
        /// <param name="token"></param>
        /// <param name="email"></param>
        /// <param name="status"></param>
        /// <param name="link"></param>
        /// <param name="picture"></param>
        /// <returns></returns>
        public static string StatusesUpdate(string token, string email, string status, string link, string picture)
        {
            string responseString = "";
            try
            {
                Dictionary<string, string> Paras = new Dictionary<string, string>();
                Paras.Add("token", HttpContext.Current.Server.UrlEncode(token));
                Paras.Add("email", HttpContext.Current.Server.UrlEncode(email));
                Paras.Add("status", HttpContext.Current.Server.UrlEncode(status));
                Paras.Add("link", HttpContext.Current.Server.UrlEncode(link));
                Paras.Add("picture", HttpContext.Current.Server.UrlEncode(picture));
                string RequestUrl = ConfigurationManager.AppSettings["imeetSiteURL"] + "ibt/IBTService.asmx/StatusUpdateHasImage";
                string result = iMeetTokenManager.WebServiceGet(RequestUrl, Paras.ToQueryString());

                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(result);
                XmlNode node = xdoc.LastChild;
                responseString = node.InnerText;


            }
            catch (Exception ex)
            {
                ILogger _logger = ObjectFactory.GetInstance<ILogger>();
                _logger.Error("i-Meet Share Error", ex);
            }

            return responseString;
        }
    }
}
