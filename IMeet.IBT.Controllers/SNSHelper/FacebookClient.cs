﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using IMeet.IBT.Common.Helpers;
using IMeet.IBT.Common;
using System.IO;

namespace IMeet.IBT.Controllers.SNSHelper
{
    public class FacebookClient : WebServerClient
    {
        private static readonly AuthorizationServerDescription FacebookDescription = new AuthorizationServerDescription
        {
            TokenEndpoint = new Uri("https://graph.facebook.com/oauth/access_token"),
            AuthorizationEndpoint = new Uri("https://graph.facebook.com/oauth/authorize?scope=email,publish_stream"),
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookClient"/> class.
        /// </summary>
        public FacebookClient(): base(FacebookDescription)
        {
        }

        /// <summary>
        /// 检查token是否可以正常获取数据
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static BoolMessage CheckTokenHasExpired(string token)
        {
            string url = "https://graph.facebook.com/me?access_token=" + HttpUtility.UrlEncode(token);
            var bm = HttpHelper.WebRequest(HttpHelper.Method.GET, url, "");

            return bm;
        }

        /// <summary>
        /// 分享到facebook
        /// </summary>
        /// <param name="token"></param>
        /// <param name="name">标题</param>
        /// <param name="link">点击了的连接地址</param>
        /// <param name="caption">caption（一行）</param>
        /// <param name="description">description</param>
        /// <param name="source">显示的图片地址150x150大小,需要以http开头的哦</param>
        /// <param name="message">评论内容</param>
        /// <returns></returns>
        public static BoolMessage Share(string token, string name, string link, string caption, string description, string source, string message)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("name", name);
            dic.Add("link", link);
            dic.Add("caption", caption);
            dic.Add("description", description);
            dic.Add("source", source);
            dic.Add("privacy", "{\"value\": \"EVERYONE\"}");
            dic.Add("message", message);
            dic.Add("access_token",token);
            string url = "https://graph.facebook.com/me/feed";
            return HttpHelper.WebRequest(HttpHelper.Method.POST, url,dic);
        }
    }

    public class FacebookRequestError
    {
        public FacebookErrorMsg error { get; set; }
    }

    public class FacebookErrorMsg
    {
        public string message { get; set; }
        public string type { get; set; }
        public int code { get; set; }
        public int? error_subcode { get; set; }
    }
}
