﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Text;
using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Extensions;
using IMeet.IBT.Common.Helpers;

using IMeet.IBT.DAL;

namespace IMeet.IBT.Controllers
{
    public class MapController : BaseController
    {
        private IMapsService _mapsService;
        private ISupplierService _supplierService;

        public const string ICON_BLUECOUNTRY = "/images/sign/blue/country.png";
        public const string ICON_BLUECITY = "/images/sign/blue/cities.png";
        public const string ICON_ORANGECOUNTRY = "/images/sign/orange/country.png";
        public const string ICON_ORANGECITY = "/images/sign/orange/cities.png";

        public MapController(IMapsService mapsService,ISupplierService supplierService)
        {
            _mapsService = mapsService;
            _supplierService = supplierService;
        }

        #region 坐标
        public JsonResult GeoCountry()
        {
            int countryId = RequestHelper.GetValInt("countryId");
            _mapsService.GeoCountry(countryId);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GeoCity()
        {
            int cityId = RequestHelper.GetValInt("cityId");
            _mapsService.GeoCity(cityId);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GeoSupply()
        {
            int supplyId = RequestHelper.GetValInt("supplyId");
            _mapsService.GeoSupply(supplyId);

            return Json("OK", JsonRequestBehavior.AllowGet);

        }
        #endregion


        public JsonResult MapsIndexSupplyInfo()
        {
            int objId = RequestHelper.GetValInt("objId");
            int objType = RequestHelper.GetValInt("objType");
            var item = _mapsService.GetIndexSupplyInfo(objId, objType);

            var vtype = _supplierService.GetAttributeData(item.ObjId);

            System.Text.StringBuilder sb = new StringBuilder();

            sb.Append("  <div class=\"slideBox mapTips\">");
            sb.Append("  <div class=\"mapMain\">");
            sb.Append("      <div class=\"slideMain\">");
            sb.Append("          <ul>");
            sb.Append("                  <li>");
            sb.Append("                  <dl>");
            sb.Append("                      <dt>");
            //sb.Append("                          <h3>Recommended " + vtype.AttrDataName + " IN " + item.CityName + "," + item.CountryName + "</h3>");
            sb.Append("                          <h3>Featured</h3>");
            sb.Append("                      <a class=\"more\" href=\""+ item.TitleUrl +"\">more</a>");
            sb.Append("                          <div class=\"info\">");
            sb.Append("                              <h4><a href=\"" + item.TitleUrl + "\">" + item.SubTitle + "</a></h4>");
            sb.Append("                              <p class=\"link\"><a href=\"javascript:void(0);\">"+ item.InfoAddress +"</a></p>");
            sb.Append("                              <p class=\"rating\"><span class=\"title\">rating</span>");
            sb.Append(PageHelper.GetRatingString(item.Rating));
            sb.Append("                              </p>");
            sb.Append("                          </div>");
            sb.Append("                      </dt>");
            sb.Append("                          <dd><img src=\""+ item.ImgSrc +"\"></dd>");
            sb.Append("                          <dd>"+ StringHelper.TruncateWithText(item.Desp,300,"...") +" </dd>");
            sb.Append("                      </dl>");
            sb.Append("                  </li>");
            sb.Append("          </ul>");
            sb.Append("      </div>");
            sb.Append("      <span class=\"next\" onclick=\"indexMaps.boxNextPrev(false);\"><img id=\"info_next\"  src=\"/images/icon/arrow_left.png\"></span>");
            sb.Append("      <span class=\"prev\" onclick=\"indexMaps.boxNextPrev(true);\" id=\"info_prev\" style=\"opacity: 1; \"><img src=\"/images/icon/arrow_right.png\"></span>");
            sb.Append("  </div>");
            sb.Append("  </div>");
            sb.Append("<div class=\"bot\"></div>");

            return Json(new BoolMessage(true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        #region Maps for Profile

        /// <summary>
        /// Maps list data （profile 的页面数据）
        /// </summary>
        /// <returns></returns>
        public JsonResult GetProfileMapsList()
        {
            int userId = RequestHelper.GetValInt("userId");
            decimal lat = RequestHelper.GetValDecimal("lat");
            decimal lng = RequestHelper.GetValDecimal("lng");
            Identifier.IBTType ibtTyp = Identifier.IBTType.Country;
            int typ = RequestHelper.GetValInt("typ");
            switch (typ)
            {
                case 2:
                    ibtTyp = Identifier.IBTType.City;
                    break;
                case 3:
                    ibtTyp = Identifier.IBTType.Supply;
                    break;
                default:
                    ibtTyp = Identifier.IBTType.Country;
                    break;
            }

            var list = _mapsService.GetProfileMapsList(ibtTyp, userId, lat, lng);
            foreach (var item in list)
            {
                switch (typ)
                {
                    case 2:
                        item.icon = ICON_BLUECITY;
                        break;
                    case 3:
                        item.icon = PageHelper.GetSupplyMapsIcon(item.Id);
                        break;
                    default:
                        item.icon = ICON_BLUECOUNTRY;
                        break;
                }
               
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MapsProfileSupplyInfo()
        {
            int supplyId = RequestHelper.GetValInt("supplyId");
            int userId = RequestHelper.GetValInt("userId");

            var item = _mapsService.GetProfileSupplyInfo(userId, supplyId);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(" <div  class=\"mapTips\" >  ");
            sb.Append("  <div class=\"mapMain\">");
            sb.Append("    <div class=\"feedInfo\">");
            sb.Append("        <ul>                ");
            sb.Append("	        <li>               ");
            sb.Append("		        <a class=\"img\" href=\""+ item.TitleUrl +"\"><img alt=\"\" style=\"width:70px;\" src=\""+ PageHelper.GetSupplyPhoto(item.SupplyId,item.CoverPhotoSrc,Identifier.SuppilyPhotoSize._tn)+"\"></a>               ");
            sb.Append("		        <dl>                                                                                             ");
            sb.Append("				        <dt>                                                                                     ");
            sb.Append("					        <div class=\"info\">                                                                 ");
            sb.Append("						        <h4><a href=\"" + item.TitleUrl + "\">" + item.Firstname + " posted a visit to " + item.Title + "</a></h4>          ");
            //sb.Append("						        <p class=\"city\">United States · Nov,21,2012</p>   href=\"javascript:void(0);\"                                       ");
            sb.Append("						        <p class=\"link\"><a >"+ item.InfoAddress +"· "+ item.CreateDate.ToString("MMM,dd,yyyy")+"</a></p> ");
            sb.Append("						        <p class=\"rating\"><span class=\"title\">rating</span>                          ");
            sb.Append(PageHelper.GetRatingString(Convert.ToDecimal(item.Rating)));
            sb.Append("						        </p>                                  ");
            sb.Append("					        </div>                                    ");
            sb.Append("			          </dt>                                           ");
            sb.Append("			          <dd>                                            ");
            sb.Append("			          \"" + StringHelper.TruncateWithText(item.Experiences, 300, "...") + "\"  ");
            sb.Append("			          </dd>                    ");
            //sb.Append("			          <dd class=\"showImg\">   ");
            //sb.Append("					        <h5>Photos</h5>    ");
            //sb.Append("					        <div class=\"box\">");
            //sb.Append("						        <a href=\"#\"><img src=\"/images/photos/imgW42_01.jpg\" alt=\"\" /></a> ");
            //sb.Append("						        <a href=\"#\"><img src=\"/images/photos/imgW42_01.jpg\" alt=\"\" /></a> ");
            //sb.Append("						        <a href=\"#\"><img src=\"/images/photos/imgW42_01.jpg\" alt=\"\" /></a> ");
            //sb.Append("						        <a href=\"#\"><img src=\"/images/photos/imgW42_01.jpg\" alt=\"\" /></a> ");
            //sb.Append("					        </div>                                                                     ");
            //sb.Append("					        <div class=\"alignR\">                                                     ");
            //sb.Append("						        <a href=\"#\" class=\"more\">more photos</a>                           ");
            //sb.Append("					        </div>");
            //sb.Append("			          </dd>       ");
            sb.Append("	          </dl>");
            sb.Append("	        </li>  ");
            sb.Append("        </ul>   ");
            sb.Append("    </div>      ");
            sb.Append("</div>          ");
            sb.Append("</div>          ");
            return Json(new BoolMessage(true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Maps for Mylist

        public JsonResult GetMyListMapsList()
        {
            int userId = RequestHelper.GetValInt("userId");
            decimal lat = RequestHelper.GetValDecimal("lat");
            decimal lng = RequestHelper.GetValDecimal("lng");

            int ibttype = RequestHelper.GetValInt("typ", 0);



            IList<MapsItem> list;
            if (ibttype > 0)
            {
                list = _mapsService.GetMyListMapsList(userId, lat, lng, (Identifier.IBTType)ibttype);
            }
            else
            {
                list = _mapsService.GetMyListMapsList(userId, lat, lng);
            }
            foreach (var item in list)
            {
                switch (item.typ)
                {
                    case 2:
                        item.icon = ICON_BLUECITY;
                        break;
                    case 3:
                        item.icon = PageHelper.GetSupplyMapsIcon(item.Id);
                        break;
                    default:
                        item.icon = ICON_BLUECOUNTRY;
                        break;
                }

            }
            return Json(list, JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetMyListSupplyInfo()
        {
            int supplyId = RequestHelper.GetValInt("supplyId");
            int userId = RequestHelper.GetValInt("userId");

            var item = _mapsService.GetMyListSupplyInfo(supplyId, userId);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(" <div  class=\"mapTips\" >  ");
            sb.Append("  <div class=\"mapMain\">");
            sb.Append("    <div class=\"feedInfo\">");
            sb.Append("        <ul>                ");
            sb.Append("	        <li>               ");
            sb.Append("		        <a class=\"img\" href=\"" + item.TitleUrl + "\"><img alt=\"\" style=\"width:70px;\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, Identifier.SuppilyPhotoSize._tn) + "\"></a>               ");
            sb.Append("		        <dl>                                                                                             ");
            sb.Append("				        <dt>                                                                                     ");
            sb.Append("					        <div class=\"info\">                                                                 ");
            sb.Append("						        <h4><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a></h4>          ");
            sb.Append("						        <p class=\"link\"><a >" + item.InfoAddress + "</a></p> ");
            sb.Append("						        <p class=\"rating\"><span class=\"title\">rating</span>                          ");
            sb.Append(PageHelper.GetRatingString(Convert.ToDecimal(item.Rating)));
            sb.Append("						        </p>                                  ");
            sb.Append("					        </div>                                    ");
            sb.Append("			          </dt>                                           ");
            sb.Append("			          <dd>                                            ");
            sb.Append("			          \"" + StringHelper.TruncateWithText(item.Desp,300,"...") + "\"  ");
            sb.Append("			          </dd>                    ");
            sb.Append("	          </dl>");
            sb.Append("	        </li>  ");
            sb.Append("        </ul>   ");
            sb.Append("    </div>      ");
            sb.Append("</div>          ");
            sb.Append("</div>          ");
            return Json(new BoolMessage(true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region add Travels

        public JsonResult GetaddTravelsMapsList()
        {
            int userId = RequestHelper.GetValInt("userId");
            decimal lat = RequestHelper.GetValDecimal("lat");
            decimal lng = RequestHelper.GetValDecimal("lng");
            long iptEpoch = RequestHelper.GetValLong("iptEpoch");
            int mapType = RequestHelper.GetValInt("mapType"); //==ibtType

            //已经IBT的
            List<MapsItem> list = new List<MapsItem>();

            if (mapType == (int)Identifier.IBTType.City)
            {
                //city
                var list_city = _mapsService.GetAddTravelsMapsList(Identifier.IBTType.City, userId, lat, lng);
                foreach (var item in list_city)
                {
                    item.icon = ICON_BLUECITY;
                    list.Add(item);
                }

                #region add new city
                IList<int> add_cityIds = _supplierService.AddTravelsGetSelectIds(userId, iptEpoch, Identifier.AddTravelsType.City);
                //new ids
                List<int> new_cityIds = new List<int>();
                foreach (var item in add_cityIds)
                {
                    if (list_city.Count(c => c.Id.Equals(item)) == 0)
                    {
                        new_cityIds.Add(item);
                    }
                }

                if (new_cityIds.Count > 0)
                {
                    string cityIds = "";
                    foreach (var item in new_cityIds)
                    {
                        cityIds = cityIds + item + ",";
                    }
                    cityIds = cityIds.Length > 0 ? cityIds.Substring(0, cityIds.Length - 1) : "";
                    var newListCountry = _mapsService.GetAddTravelsNewList(Identifier.IBTType.City, cityIds);
                    foreach (var item in newListCountry)
                    {
                        item.icon = ICON_ORANGECITY;
                        list.Add(item);
                    }
                }
                #endregion

            }
            else if (mapType == (int)Identifier.IBTType.Supply)
            {
                //Supply
                var list_supply = _mapsService.GetAddTravelsMapsList(Identifier.IBTType.Supply, userId, lat, lng);
                foreach (var item in list_supply)
                {
                    item.icon = PageHelper.GetSupplyMapsIcon(item.Id);
                    list.Add(item);
                }

                #region add new Supply
                IList<int> add_supplyIds = _supplierService.AddTravelsGetSelectIds(userId, iptEpoch, Identifier.AddTravelsType.Venue);
                //new ids
                List<int> new_supplyIds = new List<int>();
                foreach (var item in add_supplyIds)
                {
                    if (list_supply.Count(c => c.Id.Equals(item)) == 0)
                    {
                        new_supplyIds.Add(item);
                    }
                }

                if (new_supplyIds.Count > 0)
                {
                    string supplyIds = "";
                    foreach (var item in new_supplyIds)
                    {
                        supplyIds = supplyIds + item + ",";
                    }
                    supplyIds = supplyIds.Length > 0 ? supplyIds.Substring(0, supplyIds.Length - 1) : "";
                    var newListCountry = _mapsService.GetAddTravelsNewList(Identifier.IBTType.Supply, supplyIds);
                    foreach (var item in newListCountry)
                    {
                        item.icon = PageHelper.GetSupplyMapsIcon(item.Id);
                        list.Add(item);
                    }
                }
                #endregion
            }
            else
            {
                var list_country = _mapsService.GetAddTravelsMapsList(Identifier.IBTType.Country, userId, lat, lng);
                foreach (var item in list_country)
                {
                    item.icon = ICON_BLUECOUNTRY;
                    list.Add(item);
                }

                #region country
                IList<int> add_countryIds = _supplierService.AddTravelsGetSelectIds(userId, iptEpoch, Identifier.AddTravelsType.Country);
                //new ids
                List<int> new_countryIds = new List<int>();
                foreach (var item in add_countryIds)
                {
                    if (list_country.Count(c => c.Id.Equals(item)) == 0)
                    {
                        new_countryIds.Add(item);
                    }
                }

                if (new_countryIds.Count > 0)
                {
                    string countryIds = "";
                    foreach (var item in new_countryIds)
                    {
                        countryIds = countryIds + item + ",";
                    }
                    countryIds = countryIds.Length > 0 ? countryIds.Substring(0, countryIds.Length - 1) : "";

                    var newListCountry = _mapsService.GetAddTravelsNewList(Identifier.IBTType.Country, countryIds);

                    foreach (var item in newListCountry)
                    {
                        item.icon = ICON_ORANGECOUNTRY;
                        list.Add(item);
                    }
                }
                #endregion
            }

            //新的需要使用黄色的图标
            //todo:这里应该改到直接去数据对比数据更准确，上面的只是取了一部分的。下次更改这问题
            //country

            return Json(new BoolMessageItem(list, list.Count >0, ""), JsonRequestBehavior.AllowGet);
        }
        #endregion


    }
}
