﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Helpers;

using IMeet.IBT.DAL;
using IMeet.IBT.Controllers.Helper;

namespace IMeet.IBT.Controllers
{
   public class ConnectionController:BaseController
    {
        private IConnectionService _connectionService;
        private ISupplierService _supplierService;
        public ConnectionController(IConnectionService connectionService, ISupplierService supplierService)
       {
           _connectionService = connectionService;
           _supplierService = supplierService;
       }

     

       #region My Connection / Friend's Connection
        [HttpGet]
        public ActionResult Connection(int? id)  //判断用户ID区别好友ID
        {
            int userId = id.HasValue ? Convert.ToInt32(id) : _profileService.GetUserId();
            if (userId == UserId)
            {
                _profileService.GetUserProfile(UserId);
            }
            else
            {
                _profileService.GetUserProfile(userId);
            }
            ViewBag.userId = userId;
            this.Title = "Connection";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();          
        }
        /// <summary>
        /// Connection data
        /// </summary>
        /// <returns></returns>
        public JsonResult ConnectionListAjx()  //用户ID(参数)有问题
        {
            int userId = RequestHelper.GetValInt("userId");
            //int userId =Convert.ToInt32( ViewData["userId"]);
            int statusId = 1;
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = ConnectionListData(userId,statusId, page, pageSize, filterLetter, filterKeyword, filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回ConnectionList的数据
        /// </summary>
        /// <param name="listType">Recommended</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string ConnectionListData(int userId,int statusId, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _connectionService.GetConnectionList(userId, statusId, page, pageSize, filterLetter, filterKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string addr_city = "";
            int FromStatusId = 0;
            int ToStatusId = 0;
            int Fromcount = 0;
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append(" <div class=\"bgStripe\">");
                sb.Append("      <a href=\"/Profile/" + item.FriendId + "\" class=\"right into\">");
                sb.Append("         <img class=\"rightArrowBlack\" src=\"/images/icon/img.gif\" alt=\"Into\" /></a> ");
                sb.Append("     <a href=\"/Profile/" + item.FriendId + "\" class=\"userPic\">");
                sb.Append("     <img src=\"" + _profileService.GetUserAvatar(item.FriendId, (int)Identifier.AccountSettingAvatarSize._lw) + "\" alt=\"" + item.UserName + "\" /></a>");
                sb.Append("     <dl>");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                sb.Append("        <dt>" +  item.FirstName + " " + item.LastName  +"<span class=\"txt\">" + addr_city + item.CountryName + "</span></dt>");
                sb.Append("         <dd>");
                FromStatusId = _profileService.GetUserConnection(UserId, item.FriendId).StatusId;  //判断好友
                ToStatusId = _profileService.GetUserConnection(item.FriendId, UserId).StatusId;
                Fromcount = _profileService.GetFriendCount(UserId,item.FriendId);
                if (ToStatusId == 1 && FromStatusId == 1)
                {
                    sb.Append("       <span class=\"btnBg IEPng \">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.FriendId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.remove(" + item.FriendId + ");\" class=\"last\">");
                    sb.Append("         <img alt=\"" + item.UserName + "\" src=\"/images/icon/friend.gif\" class=\"mr5\">Friends</a></span>");

                    // sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.UserId + ");\"><img alt=\"\" src=\"/images/icon/img.gif\" class=\"addFriend2\"></a>");
                }
                else if (Fromcount != 0 && (FromStatusId == 0 || ToStatusId == 0))
                {
                    sb.Append("       <span class=\"btnBg IEPng btnBgGray \">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.FriendId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" class=\"last\"><img alt=\"" + item.UserName + "\" src=\"/images/icon/pending.gif\" class=\"mR5\"> Pending</a></span>");

                }
                else if (UserId == item.FriendId)
                {

                }
                else
                {
                    sb.Append("       <span class=\"btnBg IEPng \">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.FriendId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.FriendId + ");\" class=\"last\"><img alt=\"" + item.UserName + "\" src=\"/images/icon/friend_blue_small.gif\" class=\"mR10\"> Add</a></span>");
                }
                //sb.Append("        <dd><span class=\"btnBg IEPng\">");
                //sb.Append("         <a href=\"/Connection/Compose/" + item.FriendId + "\"><img class=\"readEmailSmall\" src=\"/images/icon/img.gif\" alt=\"Email\" /></a>");
                //int StatusId = _profileService.GetUserConnection(UserId, item.FriendId).StatusId;  //判断好友
                //if (StatusId == 1)
                //{
                //    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.remove(" + item.FriendId + ");\" class=\"last\">");
                //    sb.Append("         <img class=\"mr5\" src=\"/images/icon/friend.gif\" alt=\"\" />Friends</a>");
                //    // sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.UserId + ");\"><img alt=\"\" src=\"/images/icon/img.gif\" class=\"addFriend2\"></a>");
                //}
                //else
                //{

                //    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.FriendId + ");\" class=\"last\"><img alt=\"\" src=\"/images/icon/friend_blue_small.gif\" class=\"mR10\">Add</a>");
                //}
                //sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.remove(" + item.FriendId + ");\">");
                //sb.Append("         <img class=\"addFriendSmall\" src=\"/images/icon/img.gif\" alt=\"\" /></a>");
                //sb.Append("         </span>");
                sb.Append("         </dd>");
                sb.Append("     </dl>");
                sb.Append("        <div class=\"fixbox mT10\"><span class=\"C31AEE3\">LAST POSTED VISIT: </span>" + _supplierService.LastPostedVistPlaceName(item.FriendId) + "</div>");
                sb.Append(" </div>");
                sb.Append(" <div class=\"bot\">");
                sb.Append("   <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"NborderL\" width=\"32%\">");
                sb.Append("       <tr><td>" + Widget.IbtStatistics(1, item.FriendId) + "</td><td>Countrles</td></tr>");
                sb.Append("       <tr><td>" + Widget.IbtStatistics(2, item.FriendId) + "</td><td>Citles</td></tr>");
                sb.Append("   </table>");
                sb.Append("   <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"32%\">");
                sb.Append("       <tr><td>" + Widget.IbtStatistics(3, item.FriendId) + "</td><td>Hotels</td></tr>");
                sb.Append("       <tr><td>" + Widget.IbtStatistics(4, item.FriendId) + "</td><td>Lifestyle</td></tr>");
                sb.Append("   </table>");
                sb.Append("   <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"NborderR\" width=\"35%\">");
                sb.Append("       <tr><td>" + Widget.IbtStatistics(5, item.FriendId) + "</td><td>Attractions</td></tr>");
                sb.Append("       <tr><td>" + _profileService.GetConnectionCount(item.FriendId) + "</td><td>Connections</td></tr>");
                sb.Append("   </table>");
                sb.Append(" </div>");
                sb.Append("</li>");
            }

            return sb.ToString();
        }

        /// <summary>
        /// 列出所有好友 (需要传入用户ID)
        /// </summary>
        /// <returns></returns>
        private ActionResult GetAllConnectionList()
        {
            var connectionList = _connectionService.GetAllConnection(UserId);
            return View();
        }


        public JsonResult RemoveConnection()
        {
            int friendId = RequestHelper.GetValInt("friendId");
            var bmi = _connectionService.RemoveConnection(UserId, friendId);
            if (bmi.Success)
            {

            }
            return Json(bmi);
         
        }
       //发送好友请求
        public JsonResult SendRequest()
        {
            int friendId = RequestHelper.GetValInt("friendId");
            if (friendId == UserId)
            {
            }
            var bmi = _connectionService.SendRequest(UserId, friendId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
       //创建好友关系
        public JsonResult CreateConnection()
        {
            int friendId = RequestHelper.GetValInt("friendId");
            var bmi = _connectionService.CreateConnection(UserId, friendId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        public JsonResult IgnoreConnection()
        {
            int friendId = RequestHelper.GetValInt("friendId");
            var bmi = _connectionService.IgnoreConnection(UserId, friendId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        #endregion

       #region Invite Friend
        [HttpGet]
       public ActionResult InviteFriend()
       {
           return View();
       }

       [HttpPost]
       public JsonResult InviteFriend(string returnUrl)  //xg
       {
           string email = RequestHelper.GetVal("inputemail");
           string email2 = RequestHelper.GetVal("inputemail2");
           string email3 = RequestHelper.GetVal("inputemail3");
           string email4 = RequestHelper.GetVal("inputemail4");
           string str = email + "," + email2 + "," + email3 + "," + email4;//要拆分的字符串  
           List<string> emailList = new List<string>() { email, email2, email3, email4 };
           string[] arr = str.Split(',');//拆分后的字符数组  

           for (int i = 0; i < arr.Length; i++)
           {
               if (arr[i] == "")//判断   
               {
                   emailList.Remove(arr[i]);//存在则从ArrayList中删除  
               }
           }  
           string message = RequestHelper.GetVal("txtmsg");
           //List<string> emailList = new List<string>() { email, email2, email3, email4 };
           string firstname = _profileService.GetUserProfile(UserId).Firstname;
           string statisticsCountry = Widget.IbtStatistics(1, 0).ToString();
           string statisticsCity = Widget.IbtStatistics(2, 0).ToString();
           string statisticsHotel = Widget.IbtStatistics(3, 0).ToString();
           string statisticsVenue = Widget.IbtStatistics(4, 0).ToString();
           string statisticsOther = Widget.IbtStatistics(5, 0).ToString();
           var bmi = _connectionService.InviteFriend(emailList, message, firstname, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther);
           if (bmi.Success)
           {
           }
           return Json(bmi);
       }
        #endregion
  
       #region My Message And Reply
       [HttpGet]
       public ActionResult Message()
       {
           this.Title = "Message";
           this.Keywords = "keywords";
           this.Description = "description";
           return View();
       }
       [HttpGet]
       public ActionResult Compose(int? id)
       {
           int userId = id.HasValue ? Convert.ToInt32(id) : _profileService.GetUserId();
           ViewBag.userId = userId;
           this.Title = "Compose";
           this.Keywords = "keywords";
           this.Description = "description";
           return View();
       }
       public JsonResult GetUserName()  //好友与非好友信息(xg)
       {
           string name = RequestHelper.GetVal("name");
           return Json(_connectionService.GetAllConnectionProfile(UserId, name).ToList(), JsonRequestBehavior.AllowGet);
       }
       /// <summary>
       /// Message data
       /// </summary>
       /// <returns></returns>
       public JsonResult MessageListAjx()
       {
           int page = RequestHelper.GetValInt("page", 1);
           int pageSize = RequestHelper.GetValInt("pageSize", 10);
           string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
           string filterField = RequestHelper.GetUrlDecodeVal("filterField");
           int type = RequestHelper.GetValInt("type"); //0: All  1: Read  2:Unread

           pageSize = pageSize > 50 ? 10 : pageSize;
           pageSize = pageSize < 1 ? 10 : pageSize;

           int total = 0;
           int itemTotal = 0;
           var list = MessageListData(type, page, pageSize, filterKeyword, filterField, out total, out itemTotal);

           Dictionary<string, int> dic = new Dictionary<string, int>();
           dic.Add("total", total);
           dic.Add("itemTotal", itemTotal);
           //如果返回的item=0 则不可以再下拉
           return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// 返回MessageList的数据
       /// </summary>
       /// <param name="type"></param>
       /// <param name="page"></param>
       /// <param name="pageSize"></param>
       /// <param name="filterKeyword"></param>
       /// <param name="filterField"></param>
       /// <param name="total"></param>
       /// <returns></returns>
       private string MessageListData(int type, int page, int pageSize,string filterKeyword, string filterField, out int total, out int itemTotal)
       {
           string strWhere = "";
           if (type == 1)
           {
               strWhere = " AND dbo.Messges.IsRead=1";
           }
           else if (type == 2)
           {
               strWhere = " AND dbo.Messges.IsRead=0";
           }
           else
           {
               strWhere = "";
           }
           var list = _connectionService.GetMessageList(UserId, false, strWhere, page, pageSize, filterKeyword, filterField, out total);
           itemTotal = list.Count;
           System.Text.StringBuilder sb = new System.Text.StringBuilder();
           int userId = 0;
           int msgcount = 0;
           string notify = "";
           foreach (var item in list)
           {
               if (item.FormUserId == UserId)
               {
                   userId = item.ToUserId;
               }
               else
               {
                   userId = item.FormUserId;
               }
               sb.Append(" <li>");
               //sb.Append("   <input name=\"chkItem\" type=\"checkbox\" class=\"checkboxMedium uncheckbox\" id=\"chkItem\" value=\"" + item.MsgId + "\">");
               sb.Append(" <span name=\"chkItem\" class=\"checkboxMedium\" id=\"chkItem\" data-msgId=\"" + item.MsgId + "\" onclick=\"changeCheck();\"></span>");
               sb.Append("       <div class=\"main\">");
               sb.Append("            <div class=\"fixbox\">");
               sb.Append("              <span class=\"btnBg right IEPng\">");
               sb.Append("                   <a href='javascript:messageList.IsRead(\"" + item.MsgId + "\")' class=\"readEmail " + (item.IsRead.Equals(true) ? "" : "unreadEmail") + "\">");
               sb.Append("                   <img src=\"/images/img.gif\" alt=\"Email\" /></a>");
               sb.Append("                   <a href='javascript:messageList.Del(\"" + item.MsgId + "\")' class=\"iconDeleteB\">");
               sb.Append("                   <img src=\"/images/img.gif\" alt=\"Delete\" /></a>");
               sb.Append("               </span>");
               sb.Append("          <a href=\"/Profile/" + userId + "\" class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(userId, (int)Identifier.AccountSettingAvatarSize._at) + "\" alt=\"User Name\"  /></a>");
               sb.Append("          <dl>");
               msgcount = _profileService.GetReplyCount(item.MsgId, UserId);
               notify = msgcount == 0 ? "" : "(" + msgcount.ToString() + ")";
               sb.Append("             <dt><a href=\"/Profile/" + userId + "\">" + _profileService.GetUserProfile(userId).Firstname + " " + _profileService.GetUserProfile(userId).Lastname + "</a>" + " " + notify + "</dt>");
               sb.Append("             <dd><a href=\"/Connection/Reply?msgId=" + item.MsgId + "&friendId=" + userId + "\" onclick=\"messageList.SetMsgRead(" + item.MsgId + "," + userId + ");\" >" + item.Content + "</a></dd>");
               sb.Append("          </dl>");
               sb.Append("         </div>");
               sb.Append("     </div>");
               sb.Append(" </li>");
           }

           return sb.ToString();
       }

       //发送消息(Message)
       public JsonResult SendMessage()
       {
           int formUserId = _profileService.GetUserId();
           int toUserId = RequestHelper.GetValInt("objId");
           string comment = RequestHelper.GetVal("msgComent");
           var bmi = _connectionService.SendMessage(formUserId, toUserId, comment);
           if (bmi.Success)
           {

           }
           return Json(bmi);

       }
       //设置已读状态(单击)
       public JsonResult SetMsgRead()
       {
           int msgId = RequestHelper.GetValInt("objId");
           var bmi = _connectionService.SetMsgRead(msgId);
           if (bmi.Success)
           {

           }
           return Json(bmi);

       }
       //设置是否已读状态(单选)
       public JsonResult SetIsRead()
       {
           int msgId = RequestHelper.GetValInt("objId");
           var bmi = _connectionService.SetIsRead(msgId);
           if (bmi.Success)
           {

           }
           return Json(bmi);

       }
      
       public JsonResult SetAllRead()
       {
           string SelMsgId = RequestHelper.GetVal("selmsgId");
           List<string> TypeList = new List<string>() { SelMsgId };
           List<int> MsgIdList = new List<int>() { };
           string[] arr = SelMsgId.Split(',');//拆分后的字符数组  
           int msgId = 0;
           for (int i = 0; i < arr.Length; i++)
           {
               if (arr[i] == "0" || arr[i] == "")//判断   
               {
                   TypeList.Remove(arr[i]);//存在则从ArrayList中删除  
               }
               else
               {
                   int.TryParse(arr[i], out msgId);
                   MsgIdList.Add(msgId);
               }
           }

           var bmi = _connectionService.SetAllRead(MsgIdList);
           if (bmi.Success)
           {

           }
           return Json(bmi);

       }
       //设置是否删除状态（单选）
       public JsonResult SetIsDel()
       {       
           int msgId = RequestHelper.GetValInt("objId");
           var bmi = _connectionService.SetIsDel(msgId);
           if (bmi.Success)
           {

           }
           return Json(bmi);

       }

       public JsonResult SetAllDel()
       {
           string SelMsgId = RequestHelper.GetVal("selmsgId");
           List<string> TypeList = new List<string>() { SelMsgId };
           List<int> MsgIdList = new List<int>() { };
           string[] arr = SelMsgId.Split(',');//拆分后的字符数组  
           int msgId = 0;
           for (int i = 0; i < arr.Length; i++)
           {
               if (arr[i] == "0" || arr[i] == "")//判断   
               {
                   TypeList.Remove(arr[i]);//存在则从ArrayList中删除  
               }
               else
               {
                   int.TryParse(arr[i], out msgId);
                   MsgIdList.Add(msgId);
               }
           }

           var bmi = _connectionService.SetAllDel(MsgIdList);
           if (bmi.Success)
           {

           }
           return Json(bmi);

       }

       [HttpGet]
       public ActionResult Reply()
       {
           this.Title = "Reply";
           this.Keywords = "keywords";
           this.Description = "description";
           return View();
       }

       /// <summary>
       /// Reply data
       /// </summary>
       /// <returns></returns>
       public JsonResult ReplyListAjx()
       {       
           int msgId = RequestHelper.GetValInt("msgId");
           int friendId = RequestHelper.GetValInt("friendId");

           int total = 0;
           int itemTotal = 0;
           var list = ReplyListData(msgId, friendId,out total, out itemTotal);

           Dictionary<string, int> dic = new Dictionary<string, int>();
           dic.Add("total", total);
           dic.Add("itemTotal", itemTotal);
           //如果返回的item=0 则不可以再下拉
           return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
       }

       /// <summary>
       /// 返回ReplyList的数据
       /// </summary>
       /// <param name="page"></param>
       /// <param name="pageSize"></param>
       /// <param name="total"></param>
       /// <returns></returns>
       private string ReplyListData(int msgId, int friendId, out int total, out int itemTotal)
       {
           var msgList = _connectionService.GetMsgList(msgId, UserId, friendId);
           var list = _connectionService.GetReplyList(msgId, false, out total);
           itemTotal = list.Count;
           System.Text.StringBuilder sb = new System.Text.StringBuilder();
           string date = "";
           string time = "";
           foreach (var msgItem in msgList)
           {
               sb.Append(" <li class=\"\">");
               sb.Append("       <div class=\"main\">");
               sb.Append("            <div class=\"fixbox\">");
               date = DateTime.Parse(msgItem.CreateDate.ToString()).ToString("d MMMM yyyy");
               time = string.Format(msgItem.CreateDate.ToShortTimeString(), "HH:nn:ss AM/PM");
               sb.Append("              <span class=\"time right\">" + date + "  |  <span class=\"inlineBox\">" + time + "</span></span>");
               sb.Append("          <a href=\"/Profile/" + msgItem.FormUserId + "\" class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(msgItem.FormUserId, (int)Identifier.AccountSettingAvatarSize._at) + "\" alt=\"" + _profileService.GetUserProfile(msgItem.FormUserId).Username + "\"  /></a>");
               sb.Append("          <dl>");
               sb.Append("             <dt><a href=\"/Profile/" + msgItem.FormUserId + "\">" + _profileService.GetUserProfile(msgItem.FormUserId).Firstname + " " + _profileService.GetUserProfile(msgItem.FormUserId).Lastname + "</a></dt>");
               sb.Append("             <dd>" + msgItem.Content + "</dd>");
               sb.Append("          </dl>");
               sb.Append("         </div>");
               sb.Append("     </div>");
               sb.Append(" </li>");
           }
           string date2 = "";
           string time2 = "";
           foreach (var item in list)
           {
               sb.Append(" <li class=\"\">");
               sb.Append("       <div class=\"main\">");
               sb.Append("            <div class=\"fixbox\">");
               date2 = DateTime.Parse(item.CreateDate.ToString()).ToString("d MMMM yyyy");
               time2 = string.Format(item.CreateDate.ToShortTimeString(), "HH:nn:ss AM/PM");
               sb.Append("              <span class=\"time right\">" + date + "  |  <span class=\"inlineBox\">" + time + "</span></span>");
               sb.Append("          <a href=\"/Profile/" + item.FormUserId + "\" class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(item.FormUserId, (int)Identifier.AccountSettingAvatarSize._at) + "\" alt=\"" + _profileService.GetUserProfile(item.FormUserId).Username + "\"  /></a>");
               sb.Append("          <dl>");
               sb.Append("             <dt><a href=\"/Profile/" + item.FormUserId + "\">" + _profileService.GetUserProfile(item.FormUserId).Firstname + " " + _profileService.GetUserProfile(item.FormUserId).Lastname + "</a></dt>");
               sb.Append("             <dd>" + item.Content + "</dd>");
               sb.Append("          </dl>");
               sb.Append("         </div>");
               sb.Append("     </div>");
               sb.Append(" </li>");
           }
           sb.Append(" <li class=\"\">");
           sb.Append("       <div class=\"main\">");
           sb.Append("          <div class=\"fixbox\">");
           sb.Append("               <a href=\"/Profile/" + UserId.ToString() + "\" class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(UserId, (int)Identifier.AccountSettingAvatarSize._at) + "\" alt=\"" + _profileService.GetUserProfile(UserId).Username + "\"  /></a>");
           sb.Append("            <form>");
           sb.Append("                 <textarea id=\"txtMsgContent\" name=\"txtMsgContent\">Type your mesages here</textarea>");
           sb.Append("               <div class=\"alignR mT10\">");
           sb.Append("                  <a href=\"javascript:void(0);\" class=\"btnBlueH33\" onclick=\"messageList.SendReply();\"><span class=\"bg\">Reply</span></a>");
           sb.Append("               </div>");
           sb.Append("            </form>");
           sb.Append("        </div>");
           sb.Append("     </div>");
           sb.Append(" </li>");
           return sb.ToString();
       }

       //回复消息(Reply)
       public JsonResult SendReply()
       {
           int msgId = RequestHelper.GetValInt("msgId");
           int formUserId = _profileService.GetUserId();
           int toUserId = RequestHelper.GetValInt("friendId");
           string replyContent = RequestHelper.GetVal("replyContent");
           var bmi = _connectionService.SendReply(msgId, formUserId, toUserId, replyContent);
           if (bmi.Success)
           {

           }
           return Json(bmi);

       }
       
        #endregion
    }
}
