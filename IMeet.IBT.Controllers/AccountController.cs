﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Helpers;
using IMeet.IBT.Common.Email;
using System.IO;
using System.Text;
using IMeet.IBT.Common.Utilities;
using System.Drawing;
using IMeet.IBT.Controllers.Helper;
using IMeet.IBT.Controllers.SNSHelper;
using System.Configuration;
using DotNetOpenAuth.OAuth2;
using System.Net;

namespace IMeet.IBT.Controllers
{
    public class AccountController : BaseController
    {
        private IAccountService _accountService;
        private ISupplierService _supplierService;
        private IConnectionService _connectionService;
        private ISNSConnectService _snsConnectService;
        private ITravelBadgeService _travelBadegService;
        //private IAccountLogService _accountLogService;
        
        public AccountController(IAccountService accountService, ISupplierService supplierService,
            IConnectionService connectionService, ISNSConnectService snsConnectService, ITravelBadgeService travelBadgeService)
        {
            _accountService = accountService;
            _supplierService = supplierService;
            _connectionService = connectionService;
            _snsConnectService = snsConnectService;
            _travelBadegService = travelBadgeService;
        }

        #region 登录
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOn()
        {
            return View();
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LogOn(string returnUrl)
        {
            string username = RequestHelper.GetUrlDecodeVal("pop_username");
            string password = RequestHelper.GetUrlDecodeVal("pop_password");
            string createPersistentCookie = RequestHelper.GetVal("createPersistentCookie");
            var bmi = _accountService.ValidateUser(username, password);
            if (bmi.Success)
            {
                _accountService.SignIn(bmi.Item, createPersistentCookie.ToLower() == "true" ? true : false);
            }
            AccountLogService accountLogService = new AccountLogService();
            accountLogService.Write(username, password, bmi.Success, bmi.Item);
            return Json(bmi);
        }

        public ActionResult LogOff()
        {
            _accountService.SignOut();
            Session["logoffValue"] = "LogOut";//TestCode
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region 注册
        [HttpGet]
        public ActionResult Signup()
        {
            this.Title = "Signup";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }


        [HttpPost]
        public JsonResult Signup(string returnUrl)
        {
            string firstname = RequestHelper.GetUrlDecodeVal("firstname");
            string lastname = RequestHelper.GetUrlDecodeVal("lastname");
            string password = RequestHelper.GetVal("password");
            string email = RequestHelper.GetUrlDecodeVal("email");
            int countryId = RequestHelper.GetValInt("hidfldCountry");
            int cityId = RequestHelper.GetValInt("hidfldCity");
            int type = RequestHelper.GetValInt("createUserType");
            string IsPleasure = RequestHelper.GetVal("createIsPle");
            string IsBusiness = RequestHelper.GetVal("createIsBus");
            bool isPleasure = IsPleasure == "1" ? true : false;
            bool isBusiness = IsBusiness == "1" ? true : false;
            string statisticsCountry = Widget.IbtStatistics(1, 0).ToString();
            string statisticsCity = Widget.IbtStatistics(2, 0).ToString();
            string statisticsHotel = Widget.IbtStatistics(3, 0).ToString();
            string statisticsVenue = Widget.IbtStatistics(4, 0).ToString();
            string statisticsOther = Widget.IbtStatistics(5, 0).ToString();
            var bmi = _accountService.CreateUser(firstname, lastname, password, email, countryId, cityId, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther, type, "0", isPleasure, isBusiness);

            if (bmi.Success)
            {

                //登录
                _accountService.SignIn(bmi.Item, false);


            }

            return Json(bmi);
        }
        #endregion

        #region 创建用户绑定
        [HttpGet]
        public ActionResult BCreateUser(string accessToken, string tokenSecret, string snsType, string snsId, string firstName, string lastName, string email, string city, string country)
        {
            this.Title = "Create User";
            this.Keywords = "keywords";
            this.Description = "description";
            ViewBag.accessToken = accessToken;
            ViewBag.tokenSecret = tokenSecret;
            ViewBag.snsType = snsType;
            ViewBag.snsId = snsId;
            ViewBag.firstName = firstName;
            ViewBag.lastName = lastName;
            ViewBag.email = email;
            //todo:这里需要把城市名称转换成int 
            if (country.ToString() == "")
            {
                country = "United States";
                city = "New York, NY";
            }

            DAL.Country _country = _supplierService.GetAllCountryInfo(country);
            DAL.City _city = _supplierService.GetAllCityInfo(city);

            ViewBag.countryId = _country == null ? "3" : _country.CountryId.ToString();
            ViewBag.cityId = _city == null ? "-1" : _city.CityId.ToString();
            //ViewBag.countryId = string.IsNullOrWhiteSpace(country) ? "-1" : _supplierService.GetAllCountryInfo(country).CountryId.ToString();
            //ViewBag.cityId = string.IsNullOrWhiteSpace(city) ? "-1" : _supplierService.GetAllCityInfo(city).CityId.ToString();

            return View();
        }
        [HttpPost]
        public JsonResult BCreateUser(string returnUrl)
        {
            string firstName = RequestHelper.GetUrlDecodeVal("txtFirstname");
            string lastName = RequestHelper.GetUrlDecodeVal("txtLastname");
            string password = RequestHelper.GetVal("txtPassword");
            string email = RequestHelper.GetUrlDecodeVal("txtEmail");
            int countryId = RequestHelper.GetValInt("hidfldCountry");
            int cityId = RequestHelper.GetValInt("hidfldCity");
            int type = RequestHelper.GetValInt("createUserType");
            string IsPleasure = RequestHelper.GetVal("createIsPle");
            string IsBusiness = RequestHelper.GetVal("createIsBus");
            bool isPleasure = IsPleasure == "1" ? true : false;
            bool isBusiness = IsBusiness == "1" ? true : false;
            string strsnsType = RequestHelper.GetVal("hidSnsType");
            int snsType = 0;
            if (strsnsType == Identifier.SnsType.Facebook.ToString())
            {
                snsType = 1;
            }
            else if (strsnsType == Identifier.SnsType.Twitter.ToString())
            {
                snsType = 2;
            }
            else if (strsnsType == Identifier.SnsType.LinkedIn.ToString())
            {
                snsType = 3;
            }
            else
            {
                snsType = 4;
            }
            string snsId = RequestHelper.GetVal("hidSnsId");
            string accessToken = RequestHelper.GetVal("hidAccessToken");
            string tokenSecret = RequestHelper.GetVal("hidTokenSecret");

            //没有记录就先添加用户
            string statisticsCountry = Widget.IbtStatistics(1, 0).ToString();
            string statisticsCity = Widget.IbtStatistics(2, 0).ToString();
            string statisticsHotel = Widget.IbtStatistics(3, 0).ToString();
            string statisticsVenue = Widget.IbtStatistics(4, 0).ToString();
            string statisticsOther = Widget.IbtStatistics(5, 0).ToString();

            var bmi = new BoolMessageItem<int>(0, false, "");
            if (_profileService.GetUserCount(email) > 0)
            {
                bmi = _accountService.JudgeUser(email, password);
                if (bmi.Success)
                {
                    //直接绑定
                    _snsConnectService.AddSNSConnect(bmi.Item, snsId, (int)snsType, accessToken, tokenSecret);
                    //最后登录
                    _accountService.SignIn(bmi.Item, false);
                }
            }
            else
            {
                bmi = _accountService.CreateUser(firstName, lastName, password, email, countryId, cityId, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther, type, source: snsType.ToString(), isPleasure: isPleasure, isBusiness: isBusiness);
                if (bmi.Success)
                {
                    //todo:SNS头像生成
                    System.Net.WebClient wc = new System.Net.WebClient();
                    string fileName = "";
                    string url = "";
                    if (snsType == (int)Identifier.SnsType.Facebook)
                    {
                        url = "http://graph.facebook.com/" + snsId + "/picture?type=large";  //暂时
                        fileName = snsId + "_facebook.jpg";
                    }
                    else if (snsType == (int)Identifier.SnsType.Twitter)
                    {
                        var item = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Twitter);
                        if (item != null && !string.IsNullOrWhiteSpace(item.OauthToken))
                        {
                            url = "https://api.twitter.com/1/users/profile_image?screen_name=" + TwitterConsumer.GetUserBaseInfo(item.OauthToken).FirstName + "&size=original"; //暂时              
                        }
                        fileName = snsId + "_twitter.jpg";
                    }
                    else if (snsType == (int)Identifier.SnsType.LinkedIn)
                    {
                        var item = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.LinkedIn);
                        if (item != null && !string.IsNullOrWhiteSpace(item.OauthToken))
                        {
                            var lbm = LinkedInConsumer.GetUserOriginalPictureUrl(item.OauthToken);
                            if (lbm.Success)
                            {
                                url = lbm.Message;
                            }
                        }
                        fileName = snsId + "_linkedIn.jpg";
                    }
                    else
                    {
                        fileName = snsId + "_imeet.jpg";
                    }

                    string filePath = Server.MapPath("~/Staticfile/Avatar/tmp/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    if (!string.IsNullOrWhiteSpace(url))
                    {
                        wc.DownloadFile(url, filePath + fileName);
                        string destPath = "/Staticfile/Avatar/" + snsId.ToString() + "/" + DateTime.Now.ToString("yyyyMM") + "/";
                        destPath = Server.MapPath("~" + destPath);
                        if (!Directory.Exists(destPath))
                            Directory.CreateDirectory(destPath);
                        //原图处理(获取原图的Width、Height)
                        System.Drawing.Image image = System.Drawing.Image.FromFile(filePath + fileName);
                        //生成小图
                        ImageCropHelper.GenerateBitmap(filePath + fileName, 0, 0, 26, 26, destPath + snsId.ToString() + "_26.jpg", image.Width, image.Height);
                        ImageCropHelper.GenerateBitmap(filePath + fileName, 0, 0, 36, 36, destPath + snsId.ToString() + "_36.jpg", image.Width, image.Height);
                        ImageCropHelper.GenerateBitmap(filePath + fileName, 0, 0, 50, 50, destPath + snsId.ToString() + "_50.jpg", image.Width, image.Height); //生成小图
                        ImageCropHelper.GenerateBitmap(filePath + fileName, 0, 0, 55, 55, destPath + snsId.ToString() + "_55.jpg", image.Width, image.Height);
                        ImageCropHelper.GenerateBitmap(filePath + fileName, 0, 0, 75, 75, destPath + snsId.ToString() + "_75.jpg", image.Width, image.Height);
                        ImageCropHelper.GenerateBitmap(filePath + fileName, 0, 0, 100, 100, destPath + snsId.ToString() + "_100.jpg", image.Width, image.Height);
                    }
                    //然后绑定
                    _snsConnectService.AddSNSConnect(bmi.Item, snsId, (int)snsType, accessToken, tokenSecret);

                    //然后登录
                    _accountService.SignIn(bmi.Item, false);

                    //return Redirect("/home");
                    //bm = new BoolMessage(true, "/home");
                }
            }

            return Json(bmi);
        }
        #endregion

        #region Account Setting
        public string CountryID;
        [HttpGet]
        public ActionResult AccountSetting()
        {
            //国家城市初始化
            if (!string.IsNullOrEmpty(_profileService.GetUserProfile(UserId).CountryId.ToString()) && !string.IsNullOrEmpty(_profileService.GetUserProfile(UserId).CityId.ToString()))
            {
                CountryID = _profileService.GetUserProfile(UserId).CountryId.ToString();

                //选中值
                this.ViewData["CountryID"] = _profileService.GetUserProfile(UserId).CountryId.ToString();
                this.ViewData["CityID"] = _profileService.GetUserProfile(UserId).CityId.ToString();
            }
            else
            {
                this.ViewData["selectCountry"] = -1;
                this.ViewData["selectCity"] = -1;
            }


            //绑定月
            List<SelectListItem> monthitems = new List<SelectListItem>();
            monthitems.Add(new SelectListItem { Text = "Month", Value = "-1" });
            monthitems.Add(new SelectListItem { Text = "01", Value = "1" });
            monthitems.Add(new SelectListItem { Text = "02", Value = "2" });
            monthitems.Add(new SelectListItem { Text = "03", Value = "3" });
            monthitems.Add(new SelectListItem { Text = "04", Value = "4" });
            monthitems.Add(new SelectListItem { Text = "05", Value = "5" });
            monthitems.Add(new SelectListItem { Text = "06", Value = "6" });
            monthitems.Add(new SelectListItem { Text = "07", Value = "7" });
            monthitems.Add(new SelectListItem { Text = "08", Value = "8" });
            monthitems.Add(new SelectListItem { Text = "09", Value = "9" });
            monthitems.Add(new SelectListItem { Text = "10", Value = "10" });
            monthitems.Add(new SelectListItem { Text = "11", Value = "11" });
            monthitems.Add(new SelectListItem { Text = "12", Value = "12" });
            this.ViewData["monthList"] = monthitems;


            //绑定日
            List<SelectListItem> dayitems = new List<SelectListItem>();
            dayitems.Add(new SelectListItem { Text = "Day", Value = "-1" });
            for (int i = 1; i <= 31; i++)
            {
                string itemText = i.ToString();
                if (i < 10)
                {
                    itemText = "0" + i.ToString();
                }
                dayitems.Add(new SelectListItem { Text = itemText, Value = i.ToString() });
            }
            this.ViewData["dayList"] = dayitems;


            //绑定年
            List<SelectListItem> yearitems = new List<SelectListItem>();
            yearitems.Add(new SelectListItem { Text = "Year", Value = "-1" });

            for (int i = DateTime.Now.Year; i >= 1950; i--)
            {
                yearitems.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            this.ViewData["yearList"] = yearitems;

            //初始化
            ProfileService profile = new ProfileService();
            DateTime birth = Convert.ToDateTime(profile.GetUserProfile(UserId).Birth);
            if (!string.IsNullOrEmpty(profile.GetUserProfile(UserId).Birth.ToString()))
            {
                int day = int.Parse(birth.Day.ToString());
                int month = int.Parse(birth.Month.ToString());
                int year = int.Parse(birth.Year.ToString());
                //选中值
                this.ViewData["selectDay"] = day;
                this.ViewData["selectMonth"] = month;
                this.ViewData["selectYear"] = year;
            }
            else
            {
                this.ViewData["selectDay"] = -1;
                this.ViewData["selectMonth"] = -1;
                this.ViewData["selectYear"] = -1;
            }

            //sns
            string facebook_css = "off";
            string twitter_css = "off";
            string linkedin_css = "off";
            string imeet_css = "off";
            IList<DAL.SnsConnect> snsList = _snsConnectService.GetSNSConnectList(UserId);
            foreach (DAL.SnsConnect sns in snsList)
            {
                if (sns.SnsTypeId == (int)Identifier.SnsType.Facebook)
                {
                    facebook_css = "on";
                }
                if (sns.SnsTypeId == (int)Identifier.SnsType.Twitter)
                {
                    twitter_css = "on";
                }
                if (sns.SnsTypeId == (int)Identifier.SnsType.LinkedIn)
                {
                    linkedin_css = "on";
                }
                if (sns.SnsTypeId == (int)Identifier.SnsType.IMeet)
                {
                    imeet_css = "on";
                }
            }

            ViewBag.Facebook = facebook_css;
            ViewBag.Twitter = twitter_css;
            ViewBag.LinkedIn = linkedin_css;
            ViewBag.IMeet = imeet_css;

            this.Title = "AccountSetting";
            this.Keywords = "keywords";
            this.Description = "description";



            return View();
        }

        [HttpPost]
        public JsonResult AccountSetting(string returnUrl)
        {

            string username = RequestHelper.GetUrlDecodeVal("username");
            string firstname = RequestHelper.GetUrlDecodeVal("firstname");
            string lastname = RequestHelper.GetUrlDecodeVal("lastname");
            string email = RequestHelper.GetUrlDecodeVal("email");
            string newemail = RequestHelper.GetUrlDecodeVal("newemail");
            string altemail = RequestHelper.GetUrlDecodeVal("altemail");
            int intDay = RequestHelper.GetValInt("hidfldDay");
            int intMonth = RequestHelper.GetValInt("hidfldMonth");
            int intYear = RequestHelper.GetValInt("hidfldYear");

            int type = RequestHelper.GetValInt("createUserType");
            string IsPleasure = RequestHelper.GetVal("createIsPle");
            string IsBusiness = RequestHelper.GetVal("createIsBus");
            string IsEmailSubscribe = RequestHelper.GetVal("emailSubscribe");
            bool isPleasure = IsPleasure == "1" ? true : false;
            bool isBusiness = IsBusiness == "1" ? true : false;
            bool isEmailSubscribe = IsEmailSubscribe == "1" ? true : false;
            //DateTime dtBirthday = getDateTime(strDay, strMonth, strYear);
            DateTime dtBirthday;
            if (intDay > 0 && intMonth > 0 && intYear > 0)
            {
                dtBirthday = new DateTime(intYear, intMonth, intDay);
            }
            else
            {
                dtBirthday = new DateTime(1900, 01, 01);
            }

            int gender = RequestHelper.GetValInt("statis");
            string statusUpdate = RequestHelper.GetUrlDecodeVal("statusUpdate");
            int countryId = RequestHelper.GetValInt("hidfldCountry");
            int cityId = RequestHelper.GetValInt("hidfldCity");

            var bmi = _accountService.UpdateProfile(UserId, username, firstname, lastname, dtBirthday, email, newemail, altemail, gender, statusUpdate, countryId, cityId, type, isPleasure, isBusiness, isEmailSubscribe);

            if (bmi.Success)
            {

            }
            return Json(bmi);
        }

        /// <summary>
        /// sns绑定取消
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult SNSCancelConnect(int id)
        {
            _snsConnectService.DeleteSNSConnect(UserId, id);
            return Json(new BoolMessage(true, ""), JsonRequestBehavior.AllowGet);
        }

        private DateTime getDateTime(string year, string month, string day)
        {
            DateTime dt = DateTime.Now;
            string strBirthday = string.Empty;
            if (!string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(day))
            {
                strBirthday = year + "-" + month + "-" + day;
            }
            if (!string.IsNullOrEmpty(strBirthday))
            {
                DateTime.TryParse(strBirthday, out dt);
            }
            else
            {
                dt = new DateTime(1900, 01, 01);
            }
            return dt;

        }
    
        [HttpPost]
        public JsonResult ActivateTravelBadge(string id)
        {
            int Uid = UserId;
            if (!string.IsNullOrEmpty(id))
            {
                int varID = 0;
                var strKey = Common.Encrypt.EncodeHelper.AES_Decrypt(id);
                if (int.TryParse(strKey, out varID) && varID > 0)
                {
                    Uid = varID;
                }
            }

            var bm = _profileService.SetUserActivateTravelBadge(Uid);
            var sds = Json(bm);
            return Json(bm);
        }
        #endregion

        #region Post avatars
        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }
        /// <summary>
        /// 用户上传头像
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Upload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    int userId = RequestHelper.GetValInt("UserId");
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Avatar/tmp/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    string saveName = userId.ToString() + "_square" + fileExtension; // 保存文件名称
                    fileData.SaveAs(filePath + saveName);
                    //原图处理
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + "ibt_" + saveName, 282, 282, MakeThumbImage.ThumbPhotoModel.Cut);


                    //string newfilePath = filePath + "ibt" + saveName;
                    //newfilePath = Server.MapPath("~" + newfilePath);

                    string url = "";
                    bool isUpload = false;
                    HttpPostedFile hfile = System.Web.HttpContext.Current.Request.Files[0];
                    if (hfile != null && hfile.ContentLength > 0)
                    {
                        int dir = userId / 1000;
                        string staticPath = "/Staticfile/Avatar/" + dir.ToString() + "/" + userId.ToString() + "/";
                        string path = System.Web.HttpContext.Current.Server.MapPath("~" + staticPath);
                        if (!System.IO.Directory.Exists(path))
                            System.IO.Directory.CreateDirectory(path);

                        //string fileName = Request.Params["fileName"];
                        //if (string.IsNullOrEmpty(fileName))
                        string fileName2 = System.Guid.NewGuid().ToString().Substring(0, 9).Replace("-", "");

                        //MakeThumbImage.MakeThumbPhoto(picpath, destPath, 120, 120, IMeet.IBT.Common.Utilities.MakeThumbImage.ThumbPhotoModel.Cut);

                        //生成小图
                        //string[] strsSavePath = new string[] { path + fileName2 + "_100.jpg", path + fileName2 + "_75.jpg", path + fileName2 + "_55.jpg", path + fileName2 + "_50.jpg", path + fileName2 + "_36.jpg", path + fileName2 + "_26.jpg" };
                        //int[] intMaxLength = new int[] { (int)Identifier.AccountSettingAvatarSize._mn, 75, (int)Identifier.AccountSettingAvatarSize._at, (int)Identifier.AccountSettingAvatarSize._xat, (int)Identifier.AccountSettingAvatarSize._xlat, (int)Identifier.AccountSettingAvatarSize._xxlat };
                        bool blSucResize = false;
                        try
                        {
                            //blSucResize = IMeet.IBT.Common.Utilities.ThumbnailImage.ResizeFromPostedFile(hfile, strsSavePath, intMaxLength);
                            Image image = Image.FromFile(path + fileName2 + "_100x100.jpg");
                            //context.Response.Write("{\"src\":\"" + Ran.Core.Common.Default.GetAvatarsPath(userId, fileName, AvatarSize._mn) + "?id=" + System.Guid.NewGuid().ToString().Substring(0, 9).Replace("-", "") + "\",\"w\":\"" + image.PhysicalDimension.Width.ToString() + "\",\"h\":\"" + image.PhysicalDimension.Height.ToString() + "\"}");
                            image.Dispose();
                            isUpload = true;
                            url = staticPath + fileName2 + "_55x55.jpg";
                        }
                        catch (Exception ex)
                        {
                            isUpload = false;
                            _logger.Error("用户上传头像！（Post avatar）", ex);
                        }
                    }

                    return Json(new { Success = true, FileName = fileName, SaveName = saveName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose files to upload !" }, JsonRequestBehavior.AllowGet);
            }


            //return Json(new BoolMessageItem<string>("", isUpload, url), JsonRequestBehavior.AllowGet);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CropImg(FormCollection collection)
        {
            try
            {

                int userId = RequestHelper.GetValInt("UserId");
                int x1 = RequestHelper.GetValInt("x1");
                int y1 = RequestHelper.GetValInt("y1");
                //int x2 = RequestHelper.GetValInt("x2");
                //int y2 = RequestHelper.GetValInt("y2");
                int width = RequestHelper.GetValInt("width");
                int height = RequestHelper.GetValInt("height");
                //int x1 = int.Parse(collection["x1"]);
                //int y1 = int.Parse(collection["y1"]);
                //int x2 = int.Parse(collection["x2"]);
                //int y2 = int.Parse(collection["y2"]);
                //int width = int.Parse(collection["width"]);
                //int height = int.Parse(collection["height"]);
                string picpath = collection["picpath"];
                picpath = Server.MapPath("~" + picpath);
                //string destPath = Server.MapPath("~/Staticfile/Avatar/") + (userId / 1000) + "/" + userId.ToString() + "/";
                string destPath = "/Staticfile/Avatar/" + (userId / 1000) + "/" + userId.ToString() + "/";
                destPath = Server.MapPath("~" + destPath);
                if (!Directory.Exists(destPath))
                    Directory.CreateDirectory(destPath);
                //string warning = "";  //剪辑警告信息
                ImageCropHelper.GenerateBitmap(picpath, x1, y1, width, height, destPath + userId.ToString() + "_cut.jpg", width, height); //截图(截剪)
                //生成小图
                ImageCropHelper.GenerateBitmap(destPath + userId.ToString() + "_cut.jpg", 0, 0, 50, 50, destPath + userId.ToString() + "_50.jpg", width, height); //生成小图
                ImageCropHelper.GenerateBitmap(destPath + userId.ToString() + "_cut.jpg", 0, 0, 55, 55, destPath + userId.ToString() + "_55.jpg", width, height);
                ImageCropHelper.GenerateBitmap(destPath + userId.ToString() + "_cut.jpg", 0, 0, 75, 75, destPath + userId.ToString() + "_75.jpg", width, height);
                ImageCropHelper.GenerateBitmap(destPath + userId.ToString() + "_cut.jpg", 0, 0, 26, 26, destPath + userId.ToString() + "_26.jpg", width, height);
                ImageCropHelper.GenerateBitmap(destPath + userId.ToString() + "_cut.jpg", 0, 0, 36, 36, destPath + userId.ToString() + "_36.jpg", width, height);
                ImageCropHelper.GenerateBitmap(destPath + userId.ToString() + "_cut.jpg", 0, 0, 100, 100, destPath + userId.ToString() + "_100.jpg", width, height);
                ImageCropHelper.GenerateBitmap(destPath + userId.ToString() + "_cut.jpg", 0, 0, 130, 130, destPath + userId.ToString() + "_130.jpg", width, height);

                //数据库操作
                //更新用户上传的头像
                string sourceFile = destPath + userId.ToString() + "_130.jpg";
                string feedPath = "/Staticfile/Newsfeed/" + (userId / 1000) + "/" + userId.ToString() + "/";
                feedPath = Server.MapPath("~" + feedPath);
                if (!Directory.Exists(feedPath))
                    Directory.CreateDirectory(feedPath);
                string filePName = userId.ToString() + "_" + DateTime.Now.ToString("yyyyMMddmmss") + ".jpg";
                string destinationFile = feedPath + filePName;
                var bmi = _accountService.UpdateAvatar(UserId, userId.ToString() + "_cut.jpg", sourceFile, destinationFile, filePName);
                if (bmi.Success)
                {
                }
                return Json(new { Success = true, Message = "You have successfully updated your profile picture!" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }


        }

        //删除原图
        private void deleteTempAvatar(int userId)
        {
            string strVPath = "";
            string strPhotoPath = Server.MapPath(strVPath + "/" + userId);

            if (System.IO.Directory.Exists(strPhotoPath))
            {
                Directory.Delete(strPhotoPath, true);
            }
        }
        #endregion

        #region Forgot Password
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            this.Title = "ForgotPassword";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }
        [HttpPost]
        public JsonResult SendPassword(string returnUrl)
        {
            string email = RequestHelper.GetUrlDecodeVal("email");
            string statisticsCountry = Widget.IbtStatistics(1, 0).ToString();
            string statisticsCity = Widget.IbtStatistics(2, 0).ToString();
            string statisticsHotel = Widget.IbtStatistics(3, 0).ToString();
            string statisticsVenue = Widget.IbtStatistics(4, 0).ToString();
            string statisticsOther = Widget.IbtStatistics(5, 0).ToString();
            var bmi = _accountService.SendPassword(email, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther);

            if (bmi.Success)
            {

            }

            return Json(bmi);
        }
        [HttpGet]
        public ActionResult ForgotPwdFinish()
        {
            this.Title = "ForgotPwdFinish";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }
        #endregion

        #region Register Success
        [HttpGet]
        public ActionResult RegisterSuccess()
        {
            this.Title = "RegisterSuccess";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }
        #endregion

        #region Change Password
        [HttpGet]
        public ActionResult ChangePassword()
        {
            this.Title = "ChangePassword";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }

        [HttpPost]
        public JsonResult ChangePassword(string returnUrl)
        {
            string oldpwd = RequestHelper.GetUrlDecodeVal("oldpwd");
            string newpwd = RequestHelper.GetUrlDecodeVal("newpwd");
            //string confirmpwd = RequestHelper.GetUrlDecodeVal("email");
            var bmi = _accountService.ChangePassword(UserId, oldpwd, newpwd);
            if (bmi.Success)
            {

            }
            return Json(bmi);
        }
        #endregion

        #region Friend Request
        [HttpGet]
        public ActionResult FriendRequest(int userId)
        {
            ViewBag.userId = userId;
            this.Title = "FriendRequest";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }
        /// <summary>
        /// Request data
        /// </summary>
        /// <returns></returns>
        public JsonResult RequestListAjx()  //用户ID(参数)有问题
        {
            int userId = RequestHelper.GetValInt("userId");
            //int userId =Convert.ToInt32( ViewData["userId"]);
            int statusId = 0;
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = RequestListData(userId, statusId, page, pageSize, filterLetter, filterKeyword, filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回ConnectionList的数据
        /// </summary>
        /// <param name="listType">Recommended</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string RequestListData(int userId, int statusId, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _connectionService.GetRequestList(userId, statusId, page, pageSize, filterLetter, filterKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string addr_city = "";
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append(" <div class=\"bgStripe\">");
                sb.Append("      <span class=\"right mT15\">");
                sb.Append("         <a href=\"javascript:void(0);\" class=\"btnBlueH45 mR10\" onclick=\"Connection.create(" + item.UserId + ");\"><span class=\"bg\">CONFIRM</span></a>");
                sb.Append("         <a href=\"javascript:void(0);\" class=\"btnGrayH45\" onclick=\"Connection.ignore(" + item.UserId + ");\"><span class=\"bg\">IGNORE</span></a>");
                sb.Append("     </span>");
                sb.Append("     <a href=\"/Profile/" + item.UserId + "\" class=\"userPic\">");
                sb.Append("      <img src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._lw) + "\" alt=\"" + item.FirstName + " " + item.LastName + "\" /></a>");
                sb.Append("        <dl>");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                sb.Append("          <dt>" + item.FirstName + " " + item.LastName + "<span class=\"txt\">" + addr_city + item.CountryName + "</span></dt>");
                sb.Append("          <dd><a href=\"/Connection/Compose/" + item.UserId + "\" class=\"underline\">Send a Message</a></dd>");
                sb.Append("          <dd class=\"mutalFriends\">" + _connectionService.GetConList(item.UserId, 1).Count() + " mutal friends</dd>");
                sb.Append("        </dl>");
                sb.Append(" </div>");
                sb.Append(" </li>");
            }

            return sb.ToString();
        }
        #endregion

        public JsonResult GetCountry()
        {
            var list = _supplierService.GetAllCountryItem();
            return Json(list, JsonRequestBehavior.AllowGet);
            //return Json(_supplierService.GetAllCountry().ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCity(int id)
        {
            var list = _supplierService.GetAllCityItem(id);
            return Json(list, JsonRequestBehavior.AllowGet);
            //return Json(_supplierService.GetCityList(id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFacebookProfileImage(int uid)
        {
            IMeet.IBT.DAL.SnsConnect sc = _snsConnectService.GetSNSConnect(uid, Identifier.SnsType.Facebook);
            if (sc.ConnectId == 0)
            {
                return Json(new { scuess = "no", message = "Please connect to your Facebook account before refreshing." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //数据库读取用户的token
                string snsid = sc.Sns_UId;
                string picurl = string.Empty;
                //头像的size
                int _mm = (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._mm;          //130*130
                int _mn = (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._mn;          //100*100
                int _lw = (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._lw;          //75*75
                int _at = (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._at;          //55*55
                int _xat = (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._xat;         //50*50
                int _xlat = (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._xlat;        //36*36
                int _xxlat = (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._xxlat;       //26*26

                string destPath = "/Staticfile/Avatar/" + (uid / 1000) + "/" + uid.ToString() + "/";
                destPath = Server.MapPath("~" + destPath);
                if (!Directory.Exists(destPath))
                    Directory.CreateDirectory(destPath);

                string p_cut = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, 282, 282);
                string p_mm = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, _mm, _mm);
                string p_mn = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, _mn, _mn);
                string p_lw = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, _lw, _lw);
                string p_at = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, _at, _at);
                string p_xat = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, _xat, _xat);
                string p_xlat = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, _xlat, _xlat);
                string p_xxlat = string.Format("http://graph.facebook.com/{0}/picture?width={1}&height={2}", snsid, _xxlat, _xxlat);

                picurl = "/Staticfile/Avatar/" + (uid / 1000) + "/" + uid.ToString() + "/" + uid.ToString() + "_100.jpg";
                WebClient wc = new WebClient();
                try
                {
                    wc.DownloadFile(p_mn, destPath + uid.ToString() + "_test.jpg");
                    wc.DownloadFile(p_cut, destPath + uid.ToString() + "_cut.jpg");
                    wc.DownloadFile(p_mm, destPath + uid.ToString() + "_130.jpg");
                    wc.DownloadFile(p_mn, destPath + uid.ToString() + "_100.jpg");
                    wc.DownloadFile(p_lw, destPath + uid.ToString() + "_75.jpg");
                    wc.DownloadFile(p_at, destPath + uid.ToString() + "_55.jpg");
                    wc.DownloadFile(p_xat, destPath + uid.ToString() + "_50.jpg");
                    wc.DownloadFile(p_xlat, destPath + uid.ToString() + "_36.jpg");
                    wc.DownloadFile(p_xxlat, destPath + uid.ToString() + "_26.jpg");
                }
                catch (Exception ex)
                {
                    return Json(new { scuess = "error", message = ex.Message, url = picurl }, JsonRequestBehavior.AllowGet);
                }
                string test_pic = destPath + uid.ToString() + "_test.jpg";
                System.IO.File.Delete(test_pic);

                string sourceFile = destPath + uid.ToString() + "_130.jpg";
                string feedPath = "/Staticfile/Newsfeed/" + (uid / 1000) + "/" + uid.ToString() + "/";
                feedPath = Server.MapPath("~" + feedPath);
                if (!Directory.Exists(feedPath))
                    Directory.CreateDirectory(feedPath);
                string destinationFile = feedPath + uid.ToString() + "_" + DateTime.Now.ToString("yyyyMMddmmss") + ".jpg";
                //数据库操作
                var bmi = _accountService.UpdateAvatar(uid, uid.ToString() + "_cut.jpg", sourceFile, destinationFile, uid.ToString() + "_" + DateTime.Now.ToString("yyyyMMddmmss") + ".jpg");

                //然后查看数据是否有记录。如果有就获取用户ID然后直接登录graph.Email, "", "");
                return Json(new { scuess = "ok", message = "ok", url = picurl }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ProPhotos(int? id)
        {
            int userId = id.HasValue ? Convert.ToInt32(id) : _profileService.GetUserId();
            if (userId == UserId)
            {
                _profileService.GetUserProfile(UserId);
            }
            else
            {
                _profileService.GetUserProfile(userId);
            }
            ViewBag.userId = userId;
            return View();
        }

        public ActionResult Welcome() 
        {
            //sns
            string facebook_css = "off";
            string twitter_css = "off";
            string linkedin_css = "off";
            string imeet_css = "off";
            IList<DAL.SnsConnect> snsList = _snsConnectService.GetSNSConnectList(UserId);
            foreach (DAL.SnsConnect sns in snsList)
            {
                if (sns.SnsTypeId == (int)Identifier.SnsType.Facebook)
                {
                    facebook_css = "on";
                }
                if (sns.SnsTypeId == (int)Identifier.SnsType.Twitter)
                {
                    twitter_css = "on";
                }
                if (sns.SnsTypeId == (int)Identifier.SnsType.LinkedIn)
                {
                    linkedin_css = "on";
                }
                if (sns.SnsTypeId == (int)Identifier.SnsType.IMeet)
                {
                    imeet_css = "on";
                }
            }

            ViewBag.Facebook = facebook_css;
            ViewBag.Twitter = twitter_css;
            ViewBag.LinkedIn = linkedin_css;
            ViewBag.IMeet = imeet_css;

            this.Title = "Welcome";
            this.Keywords = "keywords";
            this.Description = "description";

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Unsubscribe([Bind(Include = "email")] String email)
        {
            ViewBag.message = "";
            var key = RequestHelper.GetVal("key");
            //ViewBag.emailSubscribe = false;
            int id = -1;
            var dsds = new ValidateAntiForgeryTokenAttribute();

            if (!string.IsNullOrEmpty(key) &&
                int.TryParse(Common.Encrypt.EncodeHelper.AES_Decrypt(key), out id) && id > 0 && !string.IsNullOrEmpty(email))
            {
                int keyUser = _profileService.GetUserInfo(id).UserId;
                int emailUser = _accountService.GetUserByEmail(email).UserId;

                if (keyUser == emailUser)
                {
                    ViewBag.emailSubscribe = (bool)_accountService.EmailUnsubscribe(keyUser).Success;
                    ViewBag.emailSubscribe = false;
                }
                else
                {
                    ViewBag.emailSubscribe = true;
                    ViewBag.message = "Please enter correct email address";
                }
            }
            return View();
        }

        public ActionResult Unsubscribe()
        {
            ViewBag.message = "";
            var key = RequestHelper.GetVal("key");
            //ViewBag.emailSubscribe = false;
            int id = -1;
            if (!string.IsNullOrEmpty(key) && int.TryParse(Common.Encrypt.EncodeHelper.AES_Decrypt(key), out id) && id > 0)
            {
                //find user by email 
                var uKey = _profileService.GetUserInfo(id);

                if (uKey.IsEmailSubscribe == true)
                {
                    ViewBag.emailSubscribe = true;
                }
                else
                {
                    ViewBag.emailSubscribe = false;
                }
            }
            else
            {
                //  return Redirect("signup");
            }

            return View();
        }

        public ActionResult Verification(string token)
        {
            DAL.User userObj = _profileService.GetUserNewEMailVerification(token);
            if (userObj != null && userObj.UserId > 0)
            {
                var bmi = _accountService.ChangeUserAndProfileEmail(userObj.UserId, userObj.NewEmailId);
            }

            this.Title = "Verification";
            this.Keywords = "keywords";
            this.Description = "description";

            return View();
        }

        /// <summary>
        /// update account see world for welcome
        /// </summary>
        /// <returns></returns>
        public JsonResult UpdateAccountSeeWorld()
        {
            int type = RequestHelper.GetValInt("type");
            int value = RequestHelper.GetValInt("value");

            BoolMessageItem<int> bm = _accountService.UpdateAccountSeeWorld(UserId, type, value);

            return Json(bm);
        }

        #region Travel Badge
        /// <summary>
        /// return point summary page
        /// </summary>
        /// <returns></returns>
        public ActionResult PointSummary()
        {
            string key = RequestHelper.GetVal("key");
            string tnx = RequestHelper.GetVal("tnx");
            ViewBag.textContent = "";
            if (!string.IsNullOrEmpty(key))
            {
                if (!User.Identity.IsAuthenticated)
                {
                    int id = 0;
                    string strKey = Common.Encrypt.EncodeHelper.AES_Decrypt(key);

                    //return Content("This is strKey values: " + strKey + " and key value:" + key);
                    //string strKey = key;

                    if (int.TryParse(strKey, out id) && id > 0)
                    {
                        //to show thank you message
                        ViewBag.tnxMsg = tnx;
                        
                        //_accountService.SignIn(id, true);
                        //if (_profileService.GetUserInfo(id).IsActivate == 0){
                        DAL.User userObj = _profileService.GetUserInfo(id);
                        ViewBag.CurrentUser = userObj;
                        
                        ViewBag.UserTravelbadge = _travelBadegService.GetUserTravelbadge(userObj);
                        ViewBag.UserPointSummaryByYear = _travelBadegService.GetUserPointSummaryByYear(userObj);
                        
                        ViewBag.EncryptedID = Common.Encrypt.EncodeHelper.AES_Encrypt(id.ToString());

                        return View("PointSummary2");
                        //}else {ViewBag.textContent = "showBadgeMsg";return View("PointSummary2");}
                    }
                }
                return Redirect("/account/pointsummary");
            }

            if (UserId > 0)
            {
                DAL.User userObj = _profileService.GetUserInfo(UserId);

                ViewBag.CurrentUser = userObj;
                ViewBag.UserTravelbadge = _travelBadegService.GetUserTravelbadge(userObj);
                ViewBag.UserPointSummaryByYear = _travelBadegService.GetUserPointSummaryByYear(userObj);
            }
            else
            {
                ViewBag.CurrentUser = null;
                ViewBag.UserTravelbadge = null;
                ViewBag.UserPointSummaryByYear = null;
                ViewBag.textContent = "showLogin";
            }
            return View();
        }
        #endregion

        //TestCode
        public ActionResult Login_popup()
        {
            this.Title = "";//Login_popup is replaced by ""
            this.Keywords = "keywords";
            this.Description = "description";

            return View();
        }
    }
}
