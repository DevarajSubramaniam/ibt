﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using IMeet.IBT.Common.Helpers;

using IMeet.IBT.Services;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using System.Web;
using System.IO;
using IMeet.IBT.Common.Utilities;
using System.Threading.Tasks;

namespace IMeet.IBT.Controllers
{
    public class PersonalProfileController : BaseController
    {
        private ISupplierService _supplierService;
        private IWidgetService _widgetService;
        private IMapsService _mapsService;
        private IAccountService _accountService;
        public PersonalProfileController(IAccountService accountService, IWidgetService widgetService, ISupplierService supplierService, IMapsService mapsService)
        {
            _accountService = accountService;
            _supplierService = supplierService;
            _widgetService = widgetService;
            _mapsService = mapsService;
        }


        #region PublicProfile

        class newMemeber
        {
            public string fname { get; set; }
            public string lname { get; set; }
            public string email { get; set; }
            public string password { get; set; }
        }

        public ActionResult RegisterUser1()
        {
            IBTEntities ibt = new IBTEntities();
            string strMessage = "No content";
            var memeber = ibt.Database.SqlQuery<newMemeber>(@"SELECT [fname],[lname],[email],[Password] FROM [ibt_alpha].[dbo].[imeetMembers]").ToList();

            foreach (var item in memeber)
            {
                var strResult = _accountService.CreateUser(item.fname, item.lname, item.password, item.email, 3, 0, "196", "1302", "187", "41", "13", 0, "0", false, false);

                DAL.User user = ibt.Users.Where(u => u.Email == item.email).First();

                if (user.IsActivate != (int)Identifier.IsActivate.True)
                {
                    user.IsActivate = (int)Identifier.IsActivate.True;
                    user.ActivateDate = DateTime.Now;
                    var rere = ibt.SaveChanges();
                }

                
            }

            return Content(strMessage);
        }

        public ActionResult Profile(string id)
        {
            PersonalProfileService pps = new PersonalProfileService();
            int _userId = 0;

            var vv = String.IsNullOrEmpty(id);
            //var sdsds = int.TryParse(id, out _userId);
            if (!String.IsNullOrEmpty(id) && int.TryParse(id, out _userId))
            {
                try
                {
                    //var userObj = _profileService.GetUserInfo(_userId);
                    var pp = pps.getPersonalProfile(_userId);
                    ViewBag.PersonalProfile = pp;

                    //check for not existing userID
                    if (pp == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    var ppp = pp.PersonalProfilePhotos.ToArray();
                    ViewBag.PersonalProfilePhotos = ppp;

                    // set the user Country Name
                    int? countryID = pp.User.Profile.CountryId;
                    if (countryID >= 1)
                    {
                        ViewBag.UserCountryName = new IBTEntities().Countries.Where(cou => cou.CountryId == countryID).First().CountryName;
                    }

                    //set user visited countries list
                    var CitiesAll = pps.getCities(_userId);
                    ViewBag.CitiesList = CitiesAll;

                    ViewBag.TravelHistory = getHtmlCountriesListForProfile(_userId);

                    var CountriesList = pps.getCountries(_userId);
                    ViewBag.CountriesList = CountriesList;

                    var photos = pp.PersonalProfilePhotos.ToList();
                    // ViewBag.Links2 = pp.PersonalProfileLinks.ToList();

                    var ppl = pps.getLinks(pp.PPID);
                    ViewBag.Links = ppl;
                }
                catch (NullReferenceException)
                {
                    //in case redirect to current page
                    return RedirectToAction("Profile", "PersonalProfile");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            // var dd = id;
            return View("Profile");
        }

        public ActionResult EditProfile()
        {
            PersonalProfileService pps = new PersonalProfileService();
            int _userID = UserId;

            if (!User.Identity.IsAuthenticated)
            {
                //Redirect to main page
                return RedirectToAction("Index", "Home");
            }

            try
            {
                var pp = pps.getPersonalProfile(_userID);
                ViewBag.PersonalProfile = pp;


                //pp.PersonalProfileLinks.Count

                // set the user Country Name
                int? countryID = pp.User.Profile.CountryId;
                if (countryID >= 1)
                {
                    ViewBag.UserCountryName = new IBTEntities().Countries.Where(cou => cou.CountryId == countryID).First().CountryName;
                }

                // set the user City Name
                int? cityID = pp.User.Profile.CityId;
                if (cityID >= 1)
                {
                    ViewBag.UserCityName = new IBTEntities().Cities.Where(cit => cit.CityId == cityID).First().CityName;
                }

                //set all the cities list
                var allCities = pps.getCities(_userID);
                ViewBag.CitiesList = allCities;

                var ProfileImage = _profileService.GetUserProfile(UserId).ProfileImage;
                var ProfessionalHeadline = _profileService.GetUserProfile(_userID).JobTitle;
                var CountryId = _profileService.GetUserProfile(_userID).CountryId;

                var CityId = _profileService.GetUserProfile(_userID).CityId;
                ViewBag.CityId = CityId;

                var Company = _profileService.GetUserProfile(_userID).Company;
                ViewBag.Company = Company;

                TravelBadgeService _TravelBadgeService = new TravelBadgeService(_profileService);

                var CountriesList = pps.getCountries(_userID);
                ViewBag.CountriesList = CountriesList;

                var CountriesListHtml = getHtmlCountriesListForEditProfile();
                ViewBag.CountriesListHtml = CountriesListHtml;

            }
            catch (NullReferenceException)
            {
                //new Task("").Wait(20);
                return RedirectToAction("editProfile", "PersonalProfile");
            }
            return View();
        }

        /// <summary>
        /// get HTML string of countries list for EditProfile
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>String: HTML string</returns>
        public string getHtmlCountriesListForEditProfile()
        {
            int userID = UserId;

            PersonalProfileService pps = new PersonalProfileService();
            //int countryID = 0;
            //String countryName = "";
            //String flagPhotoPath = "";
            //String CitiesList = "";

            //---------------------------------------------------------------------
            //var Cities = pps.getCities(userID);
            //String strHtmlCities = "<!-- Cities Content -->";
            //strHtmlCities += "<div class='ihhCitiesDisplay hide'>";
            //strHtmlCities += "<ul class='ihhCitiesHolder'>";
            //strHtmlCities += "<li class='ihhCitiesCol'><p><span class='ihhCitiesName'>Maryland</span> / City</li>";
            //strHtmlCities += "<li class='ihhCitiesCol'><p><span class='ihhCitiesName'>New York</span> / City</li>";
            //strHtmlCities += "<li class='ihhCitiesCol'><p><span class='ihhCitiesName'>Dallas</span> / City</li>";
            //strHtmlCities += "</ul><div class='ihhCitiesDivider'></div></div>";

            //---------------------------------------------------------------------
            var Countries = pps.getCountriesByOrder(userID);
            String strHtmlCountries = "";

            foreach (var _country in Countries)
            {
                var Cities = pps.getCities(userID, int.Parse(_country[0]));
                //Html.Raw("document.write(TravelHistory.addCountry());");
                strHtmlCountries += "<!-- A Country -->";
                strHtmlCountries += "<li class='ihhCountriesHolder'>";
                strHtmlCountries += "<div class='ihhCountriesDisplay' data-cid='" + _country[0].ToString() + "'>";
                strHtmlCountries += "<div class='ihhCountriesIcons'><img src='" + _country[2].ToString() + "' alt='Canada' /></div>";
                strHtmlCountries += "<div class='ihhCountriesName'><p>" + _country[1].ToString() + "</p></div>";
                strHtmlCountries += "<div class='ihhDashedLines'><svg><g fill='none' stroke='rgba(102,102,102,0.23)' stroke-width='1'><path stroke-dasharray='5,5' d='M5 13 660 13' /></g></svg></div>";
                strHtmlCountries += "<div class='ihhCitiesFigure'><p>" + Cities.Count.ToString() + "</p></div>";
                strHtmlCountries += "<div class='ihhCntMoreButtons'><a class='btn-ihhCitiesInfo'>&#5167;</a></div></div>";
                //strHtmlCountries += strHtmlCities;

                //------------------------------------------
                //add cities list for each country
                String strHtmlCities = "<!-- Cities Content -->";
                strHtmlCities += "<div class='ihhCitiesDisplay hide'>";
                strHtmlCities += "<ul class='ihhCitiesHolder'>";
                foreach (var item in Cities)
                {
                    strHtmlCities += "<li class='ihhCitiesCol'><p><span class='ihhCitiesName'>" + item.CityName + " </span> / City</li>";
                }
                //strHtmlCities += "<li class='ihhCitiesCol'><p><span class='ihhCitiesName'>Maryland</span> / City</li>";
                //strHtmlCities += "<li class='ihhCitiesCol'><p><span class='ihhCitiesName'>New York</span> / City</li>";
                //strHtmlCities += "<li class='ihhCitiesCol'><p><span class='ihhCitiesName'>Dallas</span> / City</li>";
                strHtmlCities += "</ul><div class='ihhCitiesDivider'></div></div>";
                //--------------------------------------
                strHtmlCountries += strHtmlCities;
                strHtmlCountries += "</li><!-- end of A Country -->";
            }

            //BoolMessage bm = new BoolMessage(true, strHtmlCountries);
            //return Json(bm);
            return strHtmlCountries;
        }

        /// <summary>
        /// get HTML string of countries list for Profile
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>String: HTML string</returns>
        public string getHtmlCountriesListForProfile(int userID)
        {
            PersonalProfileService pps = new PersonalProfileService();
            //---------------------------------------------------------------------
            var Countries = pps.getCountriesByOrder(userID);
            String strHtml = "";

            // strHtml += "<div class='trvhstCntsec01'><ul class='trvhstListWrapper'>";
            //All countries
            for (int i = 0; i < Countries.Count; i += 4)
            {
                strHtml += @"<!-- Travel List row 01 --><li class='trvhstListRow'>
                                 <dl class='trvhstListCont'><dt class='trvhstListCol'>";
                // 4 countries for each row
                for (int j = 0; ((i + j) < Countries.Count && j < 4); j++)
                {
                    var CID = Countries[(j + i)][0].ToString();
                    string countryName = Countries[(j + i)][1].ToString();

                    //check for country with long name effect the design
                    if (countryName.Length > 17)
                    {
                        countryName = countryName.Substring(0, 14) + "...";
                    }

                    strHtml += string.Format(@"<div class='trvhstCntContainer' id='country{0}'>
            <span class='trvhstContCountry'>{1}</span><span class='trvhstContFigure'>{2}</span></div>"
                        , CID, countryName, pps.getCities(userID, int.Parse(CID)).Count.ToString());
                }
                strHtml += @"</dt> <dt class='trvhstListCol'><div class='trvhstListCtsWrapper hide'>
                                	<div class='trvhstListCtsHolder'>";
                strHtml += "<div class='trvhstListCtsArrow thLstCtsArw01 hide'></div>";
                for (int j = 0; ((i + j) < Countries.Count && j < 4); j++)
                {
                    var CID = Countries[(j + i)][0].ToString();
                    var cities = pps.getCities(userID, int.Parse(CID));
                    strHtml += "<!-- col 0" + j.ToString() + " -->";
                    strHtml += @"<ul class='trvhstListCtsContainer hide' id='cities" + CID + "'>";
                    foreach (var city in cities)
                    {
                        strHtml += string.Format("<li class='trvhstListCtsCol'><p><span>{0}</span>&nbsp;/&nbsp;City</p></li>"
                            , city.CityName);
                    }
                    strHtml += " </ul>";
                }
                strHtml += @" </div></div></dt></dl></li><!-- end of Travel List Row 01 -->";
            }
            //strHtml += "</ul>";
            //if (true)
            //{
            //    strHtml += "<div class='trvhstButtons'><a class='btn-travelMoreHistory' onclick='TravelHistory.showCountriesProfile();'><span>more</span></a></div>";
            //}
            //strHtml += "</div><!-- end of .trvhstCntsec01 -->";
            return strHtml;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        //[ValidateAntiForgeryToken]
        public JsonResult update(object ppObj)
        {
            PersonalProfileService pps = new PersonalProfileService();
            IBTEntities ibt = new IBTEntities();
            PersonalProfile pp = new PersonalProfile();

            var PPID = int.Parse(RequestHelper.GetVal("PPID"));
            var ppUserProfileFirstname = RequestHelper.GetVal("FName");
            var ppUserProfileLastname = RequestHelper.GetVal("LName");

            var ppUserProfileJobTitle = RequestHelper.GetVal("JobTitle");
            var ppCompany = RequestHelper.GetVal("CompanyName");

            var ppDisplayLinks = bool.Parse(RequestHelper.GetVal("DisplayLinks"));
            var ppPersonalProfileLinks = RequestHelper.GetVal("PersonalProfileLinks");
            var ppDisplayIndustryProfessional = bool.Parse(RequestHelper.GetVal("DisplayIndustryProfessional"));
            var ppPersonalProfileProfessionals = RequestHelper.GetVal("PersonalProfileProfessionals");
            var PersonalProfileCountriesOrdered = RequestHelper.GetVal("PersonalProfileCountriesOrdered");
            var ppPublishMeetingExperiences = bool.Parse(RequestHelper.GetVal("PublishMeetingExperiences"));
            var ppDisplayMeetingSiteManagement = bool.Parse(RequestHelper.GetVal("DisplayMeetingSiteManagement"));
            var ppMeetingSiteManagement = int.Parse(RequestHelper.GetVal("MeetingSiteManagement"));
            var ppDisplayMeetingSiteInspection = bool.Parse(RequestHelper.GetVal("DisplayMeetingSiteInspection"));
            var ppMeetingSiteInspection = int.Parse(RequestHelper.GetVal("MeetingSiteInspection"));
            var ppDisplayMeetingBusinessTrips = bool.Parse(RequestHelper.GetVal("DisplayMeetingBusinessTrips"));
            var ppMeetingBusinessTrips = int.Parse(RequestHelper.GetVal("MeetingBusinessTrips"));
            var ppDisplayMeetingDestinationFamiliarization = bool.Parse(RequestHelper.GetVal("DisplayMeetingDestinationFamiliarization"));
            var ppMeetingDestinationFamiliarization = int.Parse(RequestHelper.GetVal("MeetingDestinationFamiliarization"));
            var ppDisplayMeetingSpecialties = bool.Parse(RequestHelper.GetVal("DisplayMeetingSpecialties"));
            var ppMeetingSpecialties = RequestHelper.GetVal("MeetingSpecialties");
            var ppDisplayMeetingChallenges = bool.Parse(RequestHelper.GetVal("DisplayMeetingChallenges"));
            var ppIsMeetingPolitical = bool.Parse(RequestHelper.GetVal("IsMeetingPolitical"));
            var ppIsMeetingNonHotelVenue = bool.Parse(RequestHelper.GetVal("IsMeetingNonHotelVenue"));
            var ppIsMeetingStrike = bool.Parse(RequestHelper.GetVal("IsMeetingStrike"));
            var ppIsMeetingGroup500Attendees = bool.Parse(RequestHelper.GetVal("IsMeetingGroup500Attendees"));
            var ppIsMeetingNatural = bool.Parse(RequestHelper.GetVal("IsMeetingNatural"));
            var ppIsMeetingVirtualComponent = bool.Parse(RequestHelper.GetVal("IsMeetingVirtualComponent"));
            var ppIsMeetingGlobalAudience = bool.Parse(RequestHelper.GetVal("IsMeetingGlobalAudience"));
            var ppIsMeetingCelebrity = bool.Parse(RequestHelper.GetVal("IsMeetingCelebrity"));
            var ppPublishBookingContracting = bool.Parse(RequestHelper.GetVal("PublishBookingContracting"));
            var ppDisplayBookingEventsSources = bool.Parse(RequestHelper.GetVal("DisplayBookingEventsSources"));
            var ppBookingEventsSources = int.Parse(RequestHelper.GetVal("BookingEventsSources"));
            var ppDisplayBookingSpecialties = bool.Parse(RequestHelper.GetVal("DisplayBookingSpecialties"));
            var ppBookingSpecialties = RequestHelper.GetVal("BookingSpecialties");
            var ppDisplayBookingVenuesContracted = bool.Parse(RequestHelper.GetVal("DisplayBookingVenuesContracted"));


            int tempPPID = pps.getPersonalProfileID(UserId);
            if (tempPPID == PPID)
            {
                pps.updatePersonalProfile(PPID, ppUserProfileFirstname, ppUserProfileLastname, ppUserProfileJobTitle, ppCompany, ppDisplayLinks, ppDisplayIndustryProfessional,
                    ppPublishMeetingExperiences, ppDisplayMeetingSiteManagement, ppMeetingSiteManagement,
                    ppDisplayMeetingSiteInspection, ppMeetingSiteInspection, ppDisplayMeetingBusinessTrips,
                    ppMeetingBusinessTrips, ppDisplayMeetingDestinationFamiliarization, ppMeetingDestinationFamiliarization,
                    ppDisplayMeetingSpecialties, ppMeetingSpecialties, ppDisplayMeetingChallenges, ppIsMeetingPolitical,
                    ppIsMeetingStrike, ppIsMeetingNatural, ppIsMeetingGlobalAudience, ppIsMeetingNonHotelVenue,
                    ppIsMeetingGroup500Attendees, ppIsMeetingVirtualComponent, ppIsMeetingCelebrity,
                    ppPublishBookingContracting, ppDisplayBookingEventsSources, ppBookingEventsSources,
                    ppDisplayBookingSpecialties, ppBookingSpecialties, ppDisplayBookingVenuesContracted);

                //updating the links 
                pps.addLinksList(PPID, ppPersonalProfileLinks);

                //PersonalProfileCountriesOrdered
                pps.addPersonalProfileCountriesOrdered(PPID, PersonalProfileCountriesOrdered);

                pps.addProfessionalsList(PPID, ppPersonalProfileProfessionals);

                //JsonResult jr = new JsonResult();
                //jr.Data = ppObj;
                // jr.ContentEncoding[]

                // return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
                //Boxy.alert(json.Message, null, { title: "SignUp" });
                // return Json("jSon result", JsonRequestBehavior.AllowGet);

                BoolMessage bm = new BoolMessage(true, "Your profile has been successfully updated");
                return Json(bm);
            }
            //BoolMessage bm = new BoolMessage(true, "Your profile has been successfully updated");
            return Json(new BoolMessage(false, "Something goes wrong, please try again later"));
        }

        public ActionResult getVenues(string str)
        {
            PersonalProfileService pps = new PersonalProfileService();
            var query = RequestHelper.GetVal("q");

            var result = pps.suggestVenues("edede");
            var ddde = result.ToArray();

            return Json(ddde, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadToGallery(HttpPostedFileBase fileData)
        {
            PersonalProfileService pps = new PersonalProfileService();
            var VirtualPath = "";
            if (fileData != null)
            {
                try
                {
                    //int userId = RequestHelper.GetValInt("UserId");
                    VirtualPath = "/Staticfile/PersonalProfile/" + UserId + "/";
                    String filePath = Server.MapPath(VirtualPath);
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);
                    string fileExtension = Path.GetExtension(fileName);
                    string _guid = System.Guid.NewGuid().ToString().Substring(0, 12).Replace("-", "");
                    string saveName = "Gal_" + _guid + fileExtension;

                    VirtualPath += saveName;
                    fileData.SaveAs(filePath + saveName);
                    pps.addPhotoToGallery(UserId, VirtualPath);
                }
                catch (Exception)
                { }
            }
            return Json(VirtualPath, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        //[ValidateAntiForgeryToken]
        public JsonResult removeFromGallery()
        {
            PersonalProfileService pps = new PersonalProfileService();
            var filePath = RequestHelper.GetVal("filePath");
            var fileName = Path.GetFileName(filePath);

            if (!fileName.IsNullOrEmpty())
            {
                try
                {
                    //check for shared photos in gallery
                    if (!fileName.Contains("Gal_default"))
                    {
                        filePath = "/Staticfile/PersonalProfile/" + UserId + "/";
                        filePath += fileName;

                        //get server phisical path
                        String filePhisicalPath = Server.MapPath(filePath);

                        if (System.IO.File.Exists(filePhisicalPath))
                        {
                            System.IO.File.Delete(filePhisicalPath);
                        }
                    }
                    pps.removePhotoFromGallery(UserId, filePath);
                }
                catch (Exception)
                {
                    return Json(new BoolMessage(false, ""));
                }
            }

            return Json(new BoolMessage(true, ""));
            //return Json("{success : true}", JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadPersonalPhoto(HttpPostedFileBase fileData)
        {
            PersonalProfileService pps = new PersonalProfileService();
            var VirtualPath = "";
            if (fileData != null)
            {
                try
                {
                    //int userId = RequestHelper.GetValInt("UserId");
                    VirtualPath = "/Staticfile/PersonalProfile/" + UserId + "/";
                    String filePath = Server.MapPath(VirtualPath);
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);
                    string fileExtension = Path.GetExtension(fileName);
                    string _guid = System.Guid.NewGuid().ToString().Substring(0, 12).Replace("-", "");
                    string saveName = "Profile_" + _guid + fileExtension;

                    VirtualPath += saveName;
                    fileData.SaveAs(filePath + saveName);
                    pps.addPersonalPhoto(UserId, VirtualPath);
                }
                catch (Exception)
                { }
            }
            return Json(VirtualPath, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadPersonalCoverPhoto(HttpPostedFileBase fileData)
        {
            PersonalProfileService pps = new PersonalProfileService();
            var VirtualPath = "";
            if (fileData != null)
            {
                try
                {
                    //int userId = RequestHelper.GetValInt("UserId");
                    VirtualPath = "/Staticfile/PersonalProfile/" + UserId + "/";
                    String filePath = Server.MapPath(VirtualPath);
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);
                    string fileExtension = Path.GetExtension(fileName);
                    string _guid = System.Guid.NewGuid().ToString().Substring(0, 12).Replace("-", "");
                    string saveName = "Cover_" + _guid + fileExtension;

                    VirtualPath += saveName;
                    fileData.SaveAs(filePath + saveName);
                    pps.updatePersonalCoverPhoto(UserId, VirtualPath);
                }
                catch (Exception)
                { }
            }
            return Json(VirtualPath, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult setDefaultCoverPhoto()
        {
            PersonalProfileService pps = new PersonalProfileService();
            string newCoverUrl = "/Staticfile/PersonalProfile/SharedPhotos/Cover_default1.jpg";
            try
            {
                PersonalProfile pp = pps._db.PersonalProfiles.Where(p => p.UserID == UserId).FirstOrDefault();
                //"/Staticfile/PersonalProfile/SharedPhotos/Cover_default1.jpg";

                string strCoverPageURL = pp.CoverPageURL;
                if (strCoverPageURL.Contains("Cover_default"))
                {
                    int indexOf = strCoverPageURL.IndexOf("Cover_default") + 13;
                    int defNo = int.Parse(strCoverPageURL.Substring(indexOf, 1));

                    defNo++;
                    if (defNo > 3)
                    {
                        defNo = 1;
                    }

                    newCoverUrl = "/Staticfile/PersonalProfile/SharedPhotos/Cover_default";
                    newCoverUrl += defNo.ToString() + ".jpg";
                }

                pps.updatePersonalCoverPhoto(UserId, newCoverUrl);
            }
            catch (Exception)
            {
                pps.updatePersonalCoverPhoto(UserId, newCoverUrl);
                return Json(new BoolMessage(false, ""));
            }

            return Json(new BoolMessage(true, newCoverUrl));
        }

    }
        #endregion
}
