﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using IMeet.IBT.Services;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Helpers;
using System.Web;
using IMeet.IBT.Common.Utilities;
using System.IO;
using System.Drawing;
using System.Data.OleDb;
using System.Data;
using IMeet.IBT.DAL.Models;
using IMeet.IBT.Controllers.Helper;

using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using System.Web.Security;
using StructureMap;
using System.Collections.Specialized;
using IMeet.IBT.Common.Configurations;

namespace IMeet.IBT.Controllers.Areas.Admin.Controllers
{
    public class HomeController : AdminBaseController
    {
        private IAccountService _accountService;
        private ISupplierService _supplierService;
        private IWidgetService _widgetService;
        private ISearchService _searchService;
        private IMapsService _mapsService;
        private IIBTService _ibtService;
        private ITravelBadgeService _travelBadgeService;
        private IAdminEmailService _adminEmailService;
        public HomeController(IAccountService accountService, ISupplierService supplierService, IWidgetService widgetService, ISearchService searchService, IMapsService mapsService, IIBTService ibtService, ITravelBadgeService trableBadgeService, IAdminEmailService adminEmailService)
        {
            _accountService = accountService;
            _supplierService = supplierService;
            _widgetService = widgetService;
            _searchService = searchService;
            _mapsService = mapsService;
            _ibtService = ibtService;
            _travelBadgeService = trableBadgeService;
            _adminEmailService = adminEmailService;
        }

        #region Admin Login
        public ActionResult AdminLogin()
        {
            this.Title = "AdminLogin";
            this.PageTitle = "Home Management";
            return View();
        }
        /// <summary>
        /// 管理员登录
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LogOn(string returnUrl)
        {
            string username = RequestHelper.GetUrlDecodeVal("txt_username");
            string password = RequestHelper.GetUrlDecodeVal("txt_password");
            string createPersistentCookie = RequestHelper.GetVal("createPersistentCookie");
            var bmi = _accountService.ValidateAdminUser(username, password);
            if (bmi.Success)
            {
                _accountService.SignIn(bmi.Item, createPersistentCookie.ToLower() == "true" ? true : false);
            }

            return Json(bmi);
        }
        public ActionResult AdminLoginCompleted()
        {
            this.Title = "AdminLoginCompleted";
            this.PageTitle = "Home Management";
            return View();
        }
        
        #region Forgot Password
        public ActionResult AdminLoginForgot()
        {
            this.Title = "AdminLoginForgot";
            this.PageTitle = "Home Management";
            return View();
        }
        [HttpPost]
        public JsonResult SendPassword(string returnUrl)
        {
            string email = RequestHelper.GetUrlDecodeVal("txtFemail");
            string statisticsCountry = Widget.IbtStatistics(1, 0).ToString();
            string statisticsCity = Widget.IbtStatistics(2, 0).ToString();
            string statisticsHotel = Widget.IbtStatistics(3, 0).ToString();
            string statisticsVenue = Widget.IbtStatistics(4, 0).ToString();
            string statisticsOther = Widget.IbtStatistics(5, 0).ToString();
            var bmi = _accountService.SendPassword(email, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther);

            if (bmi.Success)
            {

            }

            return Json(bmi);
        }

        #endregion

        #endregion

        #region Destination
        public ActionResult Destination()
        {
            this.Title = "Home page Management";
            this.PageTitle = "Home Page Management";
            return View();
        }
        /// <summary>
        /// Supplier Detail(SupplyName)返回Supplier Title
        /// </summary>
        /// <returns></returns>
        public JsonResult GetItemTitle()
        {
            int dWeekId = RequestHelper.GetValInt("dWeekId");
            int objId = _widgetService.GetWidgetDWeekInfo(dWeekId).ObjId;
            int dwType = _widgetService.GetWidgetDWeekInfo(dWeekId).DWType;
            string itemName = "";
            if (dwType == (int)Identifier.IBTType.Supply)
            {
                itemName = _supplierService.GetSupplyDetail(objId).Title;
            }
            else if (dwType == (int)Identifier.IBTType.Country)
            {
                itemName = _supplierService.GetCountryInfo(objId).CountryName;
            }
            else
            {
                itemName = _supplierService.GetCityInfo(objId).CityName;
            }
            return Json(new BoolMessageItem<string>("", true, itemName + "(" + objId.ToString() + "-" + dwType.ToString() + ")"), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Add New Destination
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddNewDestination()
        {
            return View();
        }
        //todo:Edit Destination

        [HttpPost]
        public JsonResult AddNewDestinationItem(string returnUrl)
        {
            string desp = RequestHelper.GetUrlDecodeVal("Desp");
            string imgSrc = RequestHelper.GetVal("imgSave");
            int objId = RequestHelper.GetValInt("objId");
            int objType = RequestHelper.GetValInt("objType");
            int userId = RequestHelper.GetValInt("UserId");

            var bmi = _widgetService.AddNewDestination(objId, objType, userId, desp, imgSrc);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        [HttpPost]
        public JsonResult EditDestination(string returnUrl)
        {
            string desp = RequestHelper.GetUrlDecodeVal("Desp");
            string imgSrc = RequestHelper.GetVal("imgSave");
            int dWeekId = RequestHelper.GetValInt("dWeekId");
            int ObjId = RequestHelper.GetValInt("objId");
            int userId = RequestHelper.GetValInt("UserId");
            int sort = RequestHelper.GetValInt("sort");

            var bmi = _widgetService.EditDestination(dWeekId, sort, ObjId, userId, desp, imgSrc);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostImgUpload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    int userId = RequestHelper.GetValInt("UserId");
                    int objId = RequestHelper.GetValInt("ObjId");
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Supplier/DWeek/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    //string saveName = userId.ToString() + "_" + objId.ToString() + "_supply.jpg";// +fileExtension; // 保存文件名称
                    string saveName = Guid.NewGuid().ToString("N") + ".jpg";
                    fileData.SaveAs(filePath + saveName);
                    //原图处理
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + "Cut_" + saveName, 270, 103, MakeThumbImage.ThumbPhotoModel.Cut);

                    //生成430X282图片 todo(xg)                   
                    //int dir = objId / 500;
                    string staticPath = "/Staticfile/Supplier/DWeek/";
                    string path = System.Web.HttpContext.Current.Server.MapPath("~" + staticPath);
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, path + "Des_" + saveName, 430, 282, MakeThumbImage.ThumbPhotoModel.Cut);
                    return Json(new { Success = true, FileName = fileName, SaveName = saveName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose files to upload !" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Destination data
        /// </summary>
        /// <returns></returns>
        public JsonResult DestinationListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            string strOrderType = RequestHelper.GetVal("OrderBy");
            //string strOrderType = RequestHelper.GetVal("hidOrderTypeName");  //Todo:赋值给JS（hidOrderTypeName）
            Identifier.SupplyDestinationOrderBy emunOrderType;
            switch (strOrderType)
            {
                default:
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Sort;
                    break;
                case "Title":
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Title;
                    break;
            }
            Identifier.OrderBy emunOrderValue;
            //string OrderValue = GetOrderValue();
            string OrderValue = RequestHelper.GetVal("DescOrAsc");
            if (OrderValue == "Desc")
            {
                emunOrderValue = Identifier.OrderBy.DESC;
            }
            else
            {
                emunOrderValue = Identifier.OrderBy.ASC;
            }
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;


            int total = 0;
            int itemTotal = 0;
            int OrderBy = (int)emunOrderType;  //排序
            string DescOrAsc = emunOrderValue.ToString();
            //IBT_IMeetGetAllDestinationList 0,'ASC','',-1,1,10,0,1,-1
            var list = DestinationListData(OrderBy, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 返回DestinationList的数据
        /// </summary>
        /// <param name="OrderType"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string DestinationListData(int OrderBy, string DescOrAsc, int page, int pageSize, string SearchKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _widgetService.GetDestinationList(OrderBy, UserId, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            string TypeName = "";
            foreach (var item in list)
            {
                sb.Append(" <tr id=\"" + item.DWeekId + "\">");
                sb.Append("     <td>" + item.row_number + ".</td>");
                sb.Append("     <td><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a></td>");
                sb.Append("     <td>" + item.InfoAddress.ToString() + "</td>");
                sb.Append("     <td>" + item.CountryName + "</td>");
                sb.Append("     <td>" + item.CityName + "</td>");
                sb.Append("     <td>" + item.InfoState.ToString() + "</td>");
                sb.Append("     <td>" + IMeet.IBT.Common.StringHelper.Truncate(item.Desp, 25, "...") + "</td>");
                if (item.DWType == (int)Identifier.IBTType.Supply)
                {
                    TypeName = "Supplier";
                }
                else if (item.DWType == (int)Identifier.IBTType.Country)
                {
                    TypeName = "Country";
                }
                else
                {
                    TypeName = "City";
                }
                sb.Append("     <td>" + TypeName + "</td>");
                //sb.Append("     <td class=\"cssTaskSn\"><a href=\"javascript:void(0);\" onclick=\"GetDWeekId(" + item.DWeekId + ");\">" + item.Sort.ToString() + "</a></td>");
                sb.Append("     <td class=\" alignR\">");
                sb.Append("       <div class=\"controlBg control2\">");
                sb.Append("          <span class=\"bg\">");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddNewDestination?dWeekId=" + item.DWeekId + "','Get','',function(){$('#dWeekId').val(" + item.DWeekId + "); AddNewDestination.editInfo($('#dWeekId').val());supplyManager.uploadAvator();supplyManager.autoComplete();$('#addNewDesform').bValidator({singleError:true}); },'Edit destination');\" class=\"edit NborderL\"  ></a>");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"AddNewDestination.DelDWeek(" + item.DWeekId + ");\" class=\"delete NborderR\" ></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }

            return sb.ToString();
        }

        public JsonResult SetDestinationIsDel()
        {
            int dweekId = RequestHelper.GetValInt("objId");
            var bmi = _widgetService.SetDestinationIsDel(dweekId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        /// <summary>
        /// PaiXuDestination
        /// </summary>
        /// <returns></returns>
        public JsonResult PaiXuDestination()
        {//将最新数据保存到数据库
            string arrList = RequestHelper.GetVal("arrayData");

            List<string> SortList = new List<string>() { arrList };
            List<int> ObjIds = new List<int>() { };
            string[] arr = arrList.Split(',');//拆分后的字符数组  
            int objId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    SortList.Remove(arr[i]);//存在则从SortList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out objId);
                    ObjIds.Add(objId);
                }
            }
            var bmi = _widgetService.SetWidgetDestinationSort(ObjIds);
            if (bmi.Success)
            {

            }
            return Json(bmi);
        }
        #endregion

        #region Recommend
        public ActionResult Recommend()
        {
            this.Title = "Home Page Management";
            this.PageTitle = "Home Page Management";
            return View();

        }
        /// <summary>
        /// Recommend data
        /// </summary>
        /// <returns></returns>
        public JsonResult RecommendListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            string strOrderType = RequestHelper.GetVal("OrderBy");
            //string strOrderType = RequestHelper.GetVal("hidOrderTypeName");  //Todo:赋值给JS（hidOrderTypeName）
            Identifier.SupplyDestinationOrderBy emunOrderType;
            switch (strOrderType)
            {
                default:
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Sort;
                    break;
                case "Title":
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Title;
                    break;
            }
            Identifier.OrderBy emunOrderValue;
            //string OrderValue = GetOrderValue();
            string OrderValue = RequestHelper.GetVal("DescOrAsc");
            if (OrderValue == "Desc")
            {
                emunOrderValue = Identifier.OrderBy.DESC;
            }
            else
            {
                emunOrderValue = Identifier.OrderBy.ASC;
            }
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;


            int total = 0;
            int itemTotal = 0;
            int OrderBy = (int)emunOrderType;  //排序
            string DescOrAsc = emunOrderValue.ToString();
            //IBT_IMeetGetAllRecommendList 0,'ASC','',-1,1,10,0,1,-1
            var list = RecommendListData(OrderBy, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// 返回RecommendList的数据
        /// </summary>
        /// <param name="OrderType">UserId</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string RecommendListData(int OrderBy, string DescOrAsc, int page, int pageSize, string SearchKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _widgetService.GetRecommendList(OrderBy, UserId, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            string TypeName = "";
            foreach (var item in list)
            {
                sb.Append(" <tr id=\"" + item.MRPId + "\">");
                sb.Append("     <td>" + item.row_number + ".</td>");
                sb.Append("     <td><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a></td>");
                sb.Append("     <td>" + item.InfoAddress.ToString() + "</td>");
                sb.Append("     <td>" + item.CountryName + "</td>");
                sb.Append("     <td>" + item.CityName + "</td>");
                sb.Append("     <td>" + item.InfoState.ToString() + "</td>");
                sb.Append("     <td>" + IMeet.IBT.Common.StringHelper.Truncate(item.Desp, 25, "...") + "</td>");
                if (item.MRPType == (int)Identifier.IBTType.Supply)
                {
                    TypeName = "Supplier";
                }
                else if (item.MRPType == (int)Identifier.IBTType.Country)
                {
                    TypeName = "Country";
                }
                else
                {
                    TypeName = "City";
                }
                sb.Append("     <td>" + TypeName + "</td>");
                //sb.Append("     <td class=\"cssTaskSn\"><a href=\"javascript:void(0);\" onclick=\"GetMRPId(" + item.MRPId + ");\">" + item.Sort.ToString() + "</a></td>");
                sb.Append("     <td class=\" alignR\">");
                sb.Append("       <div class=\"controlBg control1\">");
                sb.Append("          <span class=\"bg\">");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"AddNewRecommend.DelRecommend(" + item.MRPId + ");\" class=\"delete NborderR\" ></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }

            return sb.ToString();
        }
        public JsonResult SetRecommendIsDel()
        {
            int mrpId = RequestHelper.GetValInt("objId");
            var bmi = _widgetService.SetRecommendIsDel(mrpId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        /// <summary>
        /// PaiXuRecommend
        /// </summary>
        /// <returns></returns>
        public JsonResult PaiXuRecommend()
        {//将最新数据保存到数据库
            string arrList = RequestHelper.GetVal("arrayData");

            List<string> SortList = new List<string>() { arrList };
            List<int> ObjIds = new List<int>() { };
            string[] arr = arrList.Split(',');//拆分后的字符数组  
            int objId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    SortList.Remove(arr[i]);//存在则从SortList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out objId);
                    ObjIds.Add(objId);
                }
            }
            var bmi = _widgetService.SetWidgetRecommendSort(ObjIds);
            if (bmi.Success)
            {

            }
            return Json(bmi);
        }

        #region Add Recommend (Search Supplier)
        /// <summary>
        /// Add New Recommend
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddRecommend()
        {
            return View();
        }
        public JsonResult AddRecommendItem()
        {
            int objId = RequestHelper.GetValInt("objId");
            int objType = RequestHelper.GetValInt("objType");
            int scroe = RequestHelper.GetValInt("scroe");
            var bmi = _widgetService.AddNewRecommend(objId, objType, scroe, UserId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        //to do Landing Management(GoSearch)
        #endregion

        #endregion

        #region Visited
        public ActionResult Visited()
        {
            this.Title = "Home Page Management";
            this.PageTitle = "Home Page Management";
            return View();
        }

        public JsonResult VisitedListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            string strOrderType = RequestHelper.GetVal("OrderBy");
            //string strOrderType = RequestHelper.GetVal("hidOrderTypeName");  //Todo:赋值给JS（hidOrderTypeName）
            Identifier.SupplyDestinationOrderBy emunOrderType;
            switch (strOrderType)
            {
                default:
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Sort;
                    break;
                case "Title":
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Title;
                    break;
            }
            Identifier.OrderBy emunOrderValue;
            //string OrderValue = GetOrderValue();
            string OrderValue = RequestHelper.GetVal("DescOrAsc");
            if (OrderValue == "Desc")
            {
                emunOrderValue = Identifier.OrderBy.DESC;
            }
            else
            {
                emunOrderValue = Identifier.OrderBy.ASC;
            }
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;


            int total = 0;
            int itemTotal = 0;
            int OrderBy = (int)emunOrderType;  //排序
            string DescOrAsc = emunOrderValue.ToString();
            //IBT_IMeetGetAllRecommendList 0,'ASC','',-1,1,10,0,1,-1
            var list = VisitedListData(OrderBy, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);

        }

        private string VisitedListData(int OrderBy, string DescOrAsc, int page, int pageSize, string SearchKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _widgetService.GetVisitedList(OrderBy, UserId, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            string TypeName = "";
            foreach (var item in list)
            {
                sb.Append(" <tr id=\"" + item.MVPId + "\">");
                sb.Append("     <td>" + item.row_number + ".</td>");
                sb.Append("     <td><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a></td>");
                sb.Append("     <td>" + item.InfoAddress.ToString() + "</td>");
                sb.Append("     <td>" + item.CountryName + "</td>");
                sb.Append("     <td>" + item.CityName + "</td>");
                sb.Append("     <td>" + item.InfoState.ToString() + "</td>");
                sb.Append("     <td>" + IMeet.IBT.Common.StringHelper.Truncate(item.Desp, 25, "...") + "</td>");
                if (item.MVPType == (int)Identifier.IBTType.Supply)
                {
                    TypeName = "Supplier";
                }
                else if (item.MVPType == (int)Identifier.IBTType.Country)
                {
                    TypeName = "Country";
                }
                else
                {
                    TypeName = "City";
                }
                sb.Append("     <td>" + TypeName + "</td>");
                //sb.Append("     <td class=\"cssTaskSn\"><a href=\"javascript:void(0);\" onclick=\"GetMRPId(" + item.MRPId + ");\">" + item.Sort.ToString() + "</a></td>");
                sb.Append("     <td class=\" alignR\">");
                sb.Append("       <div class=\"controlBg control1\">");
                sb.Append("          <span class=\"bg\">");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"AddNewVisited.DelVisited(" + item.MVPId + ");\" class=\"delete NborderR\" ></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }

            return sb.ToString();
        }
        public JsonResult SetVisitedIsDel()
        {
            int mvpId = RequestHelper.GetValInt("objId");
            var bmi = _widgetService.SetVisitedIsDel(mvpId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }

        public JsonResult PaiXuVisited()
        {//将最新数据保存到数据库
            string arrList = RequestHelper.GetVal("arrayData");

            List<string> SortList = new List<string>() { arrList };
            List<int> ObjIds = new List<int>() { };
            string[] arr = arrList.Split(',');//拆分后的字符数组  
            int objId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    SortList.Remove(arr[i]);//存在则从SortList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out objId);
                    ObjIds.Add(objId);
                }
            }
            var bmi = _widgetService.SetWidgetVisitedSort(ObjIds);
            if (bmi.Success)
            {

            }
            return Json(bmi);
        }

        #region Add Visited (Search Supplier)
        /// <summary>
        /// Add New Recommend
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddVisited()
        {
            return View();
        }
        public JsonResult AddVisitedItem()
        {
            int objId = RequestHelper.GetValInt("objId");
            int objType = RequestHelper.GetValInt("objType");
            int scroe = RequestHelper.GetValInt("scroe");
            var bmi = _widgetService.AddNewVisited(objId, objType, scroe, UserId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        //to do Landing Management(GoSearch)
        #endregion
        #endregion

        #region Video
        public ActionResult Video()
        {
            this.Title = "Homepage Management";
            this.PageTitle = "Supplier Management";
            return View();

        }
        /// <summary>
        /// Add New Video
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddNewVideo()
        {
            return View();
        }
        #endregion

        #region Country & City Management

        #region Country Management
        public ActionResult CountryManage()
        {
            this.Title = "Country & City Management";
            this.PageTitle = "Country & City Management";
            return View();

        }
        /// <summary>
        /// Add New Country
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddNewCountry()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFlag(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    int userId = RequestHelper.GetValInt("userId");
                    int countryId = RequestHelper.GetValInt("countryId");
                    string countryName = "flag";
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/temp/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    string savePath = Server.MapPath("~/Staticfile/flag/");
                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }
                    string saveName = countryName.ToString() + fileExtension; // 保存文件名称


                    //原图处理
                    MakeThumbImage.MakeThumbPhoto(filePath + fileName, filePath + "Cut_" + saveName, 282, 282, MakeThumbImage.ThumbPhotoModel.Cut);
                    fileData.SaveAs(filePath + fileName);
                    //生成50*50图片 todo(xg)                   
                    ImageCropHelper.GenerateBitmap(filePath + "Cut_" + saveName, 0, 0, 50, 50, savePath + saveName, 282, 282);

                    return Json(new { Success = true, FileName = fileName, SaveName = saveName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose files to upload !" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddNewCountry(string returnUrl)
        {
            string countryName = RequestHelper.GetVal("countryName");
            int score = RequestHelper.GetValInt("score");
            string flagSrc = RequestHelper.GetVal("flag");

            var bmi = _widgetService.AddNewCountry(countryName, score, flagSrc);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        [HttpPost]
        public JsonResult EditCountry(string returnUrl)
        {
            string flagSrc = RequestHelper.GetVal("flag");
            int countryId = RequestHelper.GetValInt("countryId");
            string countryName = RequestHelper.GetVal("countryName");
            int score = RequestHelper.GetValInt("score");

            var bmi = _widgetService.EditCountry(countryId, countryName, score, flagSrc);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        /// <summary>
        /// Country Detail 返回Country Name
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCountryName()
        {
            int countryId = RequestHelper.GetValInt("objId");
            string countryName = _supplierService.GetCountryInfo(countryId).CountryName;
            return Json(new BoolMessageItem<string>("", true, countryName), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Country data
        /// </summary>
        /// <returns></returns>
        public JsonResult CountryListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            string strOrderType = RequestHelper.GetVal("OrderBy");
            //string strOrderType = RequestHelper.GetVal("hidOrderTypeName");  //Todo:赋值给JS（hidOrderTypeName）
            Identifier.SupplyDestinationOrderBy emunOrderType;
            switch (strOrderType)
            {
                default:
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Sort;
                    break;
                case "Title":
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Title;
                    break;
            }
            Identifier.OrderBy emunOrderValue;
            //string OrderValue = GetOrderValue();
            string OrderValue = RequestHelper.GetVal("DescOrAsc");
            if (OrderValue == "Desc")
            {
                emunOrderValue = Identifier.OrderBy.DESC;
            }
            else
            {
                emunOrderValue = Identifier.OrderBy.ASC;
            }
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            //string strWhere = string.IsNullOrEmpty(SearchKeyword) ? "" : " AND dbo.Countries.CountryName LIKE '%" + SearchKeyword.ToString() + @"%'";

            int total = 0;
            int itemTotal = 0;
            int OrderBy = (int)emunOrderType;  //排序
            string DescOrAsc = emunOrderValue.ToString();
            //declare @rowCount INT
            //exec IBT_BrowseList 1,'','','',1,100,@rowCount  OUTPUT
            //select @rowCount
            var list = CountryListData(OrderBy, DescOrAsc, SearchKeyword, page, pageSize, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 返回CountryList的数据
        /// </summary>
        /// <returns></returns>
        private string CountryListData(int OrderBy, string DescOrAsc, string SearchKeyword, int page, int pageSize, out int total, out int itemTotal)
        {
            var list = _widgetService.GetCountryList(OrderBy, DescOrAsc, SearchKeyword, UserId, "", "", page, pageSize, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //if (page < 2)
            //{
            //    sb.Append("<tr>");
            //    sb.Append("<th width=\"3%\">&nbsp;</th>");
            //    sb.Append("<th width=\"12%\" ><a href=\"javascript:void(0);\" onclick=\"NameSort();\">Country</a></th>");
            //    sb.Append("<th  width=\"14%\">Recommended</th>");
            //    sb.Append("<th  width=\"10%\">Want to Go</th>");
            //    sb.Append("<th  width=\"10%\">Bucket List</th>");
            //    sb.Append("<th  width=\"10%\">Rating</th>");
            //    sb.Append("<th  width=\"15%\">Flag</th>");
            //    sb.Append("<th  width=\"10%\"><a href=\"javascript:void(0);\" onclick=\"NumSort();\">Score</a></th>");
            //    sb.Append("<th width=\"4%\">&nbsp;</th>");
            //    sb.Append("</tr>");
            //}
            foreach (var item in list)
            {
                sb.Append(" <tr>");
                sb.Append("     <td>" + item.RowId.ToString() + ".</td>");
                sb.Append("     <td><a href=\"/Country/NewsFeed/1/" + item.CountryId.ToString() + "\">" + item.CountryName + "</a></td>");
                sb.Append("     <td>" + item.RecommendedCount.ToString() + "</td>");
                sb.Append("     <td>" + item.WanttoGoCount.ToString() + "</td>");
                sb.Append("     <td>" + item.BucketListCount.ToString() + "</td>");
                sb.Append("     <td>" + item.RatingCount.ToString() + "</td>");
                sb.Append("     <td><img class=\"resize27\" src=\"" + item.FlagSrc + "\" alt=\"" + item.CountryName + "\" /></td>");
                sb.Append("     <td class=\"cssTaskSn\"><a href=\"javascript:void(0);\" onclick=\"GetCountryId(" + item.CountryId + ");\">" + item.Sort.ToString() + "</a></td>");
                sb.Append("     <td class=\" alignR\">");
                sb.Append("       <div class=\"controlBg control2\">");
                sb.Append("          <span class=\"bg\">");
                //sb.Append("          <a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddNewCountry?countryId=" + item.CountryId + "','Get','',function(){$('#hidcountryId').val(" + item.CountryId + "); CountryManage.editInfo($('#hidcountryId').val());CountryManage.uploadFlag();$('#Countryform').bValidator({singleError:true}); },'Edit Country');\" class=\"edit NborderL\"  ></a>");
                sb.Append("          <a href=\"/admin/home/EditCountryManage/" + item.CountryId.ToString() + "\" class=\"edit NborderL\"  ></a>");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"CountryManage.DelCountry(" + item.CountryId + ");\" class=\"delete " + (Convert.ToInt32(item.StatusId) != 3 ? "" : "active") + "\" ></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }

            return sb.ToString();
        }

        public JsonResult SetCountryIsDel()
        {
            int countryId = RequestHelper.GetValInt("objId");
            var bmi = _widgetService.SetCountryIsDel(countryId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }


        public ActionResult EditCountryManage(int id = 0)
        {
            ViewData["Country"] = _supplierService.GetCountryInfo(id);
            ViewData["CountryDes"] = _ibtService.GetCountryDesInfo(id);

            return View();
        }

        /// <summary>
        /// 更新国家信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditCountryManageSave()
        {
            Country country = new Country();
            CountriesDe des = new CountriesDe();

            country.CountryId = RequestHelper.GetValInt("id");
            country.CountryName = RequestHelper.GetVal("name");
            //country.FlagSrc = RequestHelper.GetVal("flag");
            country.FlagSrc = string.Empty;
            country.CoordsLongitude = RequestHelper.GetValDecimal("longitude");
            country.CoordsLatitude = RequestHelper.GetValDecimal("latitude");
            des.CountryTxt = RequestHelper.GetVal("txt");
            des.TTD = RequestHelper.GetVal("ttd");
            des.SN = RequestHelper.GetVal("sn");
            des.FAD = RequestHelper.GetVal("fad");
            des.Events = RequestHelper.GetVal("eve");


            string flag = RequestHelper.GetVal("flag");
            //国旗图片处理
            if (!string.IsNullOrEmpty(flag))
            {
                string path = Server.MapPath("~" + flag);
                string extension = Path.GetExtension(path);
                string saveName = string.Empty;
                string savePath = string.Empty;
                if (country.CountryId > 0)
                {
                    Country editCountry = _ibtService.GetCountry(country.CountryId);
                    if (editCountry != null && !string.IsNullOrEmpty(editCountry.FlagSrc))
                    {
                        savePath = Server.MapPath("~" + PageHelper.GetCountryFlagToConvert(editCountry.FlagSrc));
                        //country.FlagSrc = editCountry.FlagSrc;
                    }
                    else
                    {
                        saveName = "flag_170_170_" + country.CountryId.ToString() + extension;
                        savePath = Server.MapPath("~/Staticfile/flag/" + saveName);
                        country.FlagSrc = "/Staticfile/flag/" + saveName;
                    }
                }
                else
                {
                    saveName = Guid.NewGuid().ToString().Replace("-", string.Empty) + extension;
                    savePath = Server.MapPath("~/Staticfile/flag/" + saveName);
                    country.FlagSrc = "/Staticfile/flag/" + saveName;
                }
                //将文件夹中的图片复制到国旗目录
                System.IO.File.Copy(path, savePath, true);
            }

            var bm = _ibtService.EditCountry(country, des);

            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传国旗到临时文件夹
        /// </summary>
        /// <param name="fileData"></param>
        /// <returns></returns>
        public JsonResult UploadCountryFlag(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    string filePath = Server.MapPath("~/Staticfile/temp/Country/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string extension = Path.GetExtension(fileData.FileName);
                    string fileName = Guid.NewGuid().ToString().Replace("-", "") + extension;
                    //保存原图
                    fileData.SaveAs(filePath + fileName);
                    //原图处理
                    MakeThumbImage.MakeThumbPhoto(filePath + fileName, filePath + "Cut_" + fileName, 170, 170, MakeThumbImage.ThumbPhotoModel.Fit);
                    return Json(new BoolMessageItem<string>("/Staticfile/temp/Country/Cut_" + fileName, true, "Success"), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new BoolMessageItem<string>(string.Empty, false, ex.Message), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new BoolMessageItem<string>(string.Empty, false, "Please choose files to upload !"));
            }
        }

        #endregion

        #region City Management
        public ActionResult CityManage()
        {
            this.Title = "Country & City Management";
            this.PageTitle = "Country & City Management";
            return View();

        }
        /// <summary>
        /// Add New City
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddNewCity()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddNewCity(string returnUrl)
        {
            int countryId = RequestHelper.GetValInt("countryId");
            string cityName = RequestHelper.GetVal("cityName");
            int score = RequestHelper.GetValInt("score");
            string direction = RequestHelper.GetVal("direction");

            var bmi = _widgetService.AddNewCity(countryId, cityName, score, direction);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        [HttpPost]
        public JsonResult EditCity(string returnUrl)
        {
            string direction = RequestHelper.GetVal("direction");
            int cityId = RequestHelper.GetValInt("cityId");
            int countryId = RequestHelper.GetValInt("countryId");
            string cityName = RequestHelper.GetVal("cityName");
            int score = RequestHelper.GetValInt("score");

            var bmi = _widgetService.EditCity(cityId, countryId, cityName, score, direction);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        /// <summary>
        /// City Detail 返回(City Name)
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCityName()
        {
            int cityId = RequestHelper.GetValInt("cityId");
            string cityName = "";
            cityName = _supplierService.GetCityInfo(cityId).CityName;

            return Json(new BoolMessageItem<string>("", true, cityName), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// City data
        /// </summary>
        /// <returns></returns>
        public JsonResult CityListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            string strOrderType = RequestHelper.GetVal("OrderBy");
            //string strOrderType = RequestHelper.GetVal("hidOrderTypeName");  //Todo:赋值给JS（hidOrderTypeName）
            Identifier.SupplyDestinationOrderBy emunOrderType;
            switch (strOrderType)
            {
                default:
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Sort;
                    break;
                case "Title":
                    emunOrderType = Identifier.SupplyDestinationOrderBy.Title;
                    break;
            }
            Identifier.OrderBy emunOrderValue;
            //string OrderValue = GetOrderValue();
            string OrderValue = RequestHelper.GetVal("DescOrAsc");
            if (OrderValue == "" || OrderValue == "Desc")
            {
                emunOrderValue = Identifier.OrderBy.DESC;
            }
            else
            {
                emunOrderValue = Identifier.OrderBy.ASC;
            }
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            //string strWhere = string.IsNullOrEmpty(SearchKeyword) ? "" : " AND dbo.Cities.CityName LIKE '%" + SearchKeyword.ToString() + @"%'";

            int total = 0;
            int itemTotal = 0;
            int OrderBy = (int)emunOrderType;  //排序
            string DescOrAsc = emunOrderValue.ToString();
            //declare @rowCount INT
            //exec IBT_BrowseList 1,'','','',1,100,@rowCount  OUTPUT
            //select @rowCount
            var list = CityListData(OrderBy, DescOrAsc, SearchKeyword, "", page, pageSize, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 返回CityList的数据
        /// </summary>
        /// <returns></returns>
        private string CityListData(int OrderBy, string DescOrAsc, string filterKeyword, string strWhere, int page, int pageSize, out int total, out int itemTotal)
        {
            var list = _widgetService.GetCityList(OrderBy, DescOrAsc, filterKeyword, UserId, strWhere, "", page, pageSize, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //if (page < 2)
            //{
            //    sb.Append("<tr>");
            //    sb.Append("<th width=\"3%\">&nbsp;</th>");
            //    sb.Append("<th width=\"12%\" ><a href=\"javascript:void(0);\" onclick=\"NameSort();\">City</a></th>");
            //    sb.Append("<th  width=\"14%\">Recommended</th>");
            //    sb.Append("<th  width=\"10%\">Want to Go</th>");
            //    sb.Append("<th  width=\"10%\">Bucket List</th>");
            //    sb.Append("<th  width=\"10%\">Rating</th>");
            //    sb.Append("<th  width=\"10%\"><a href=\"javascript:void(0);\" onclick=\"NumSort();\">Score</a></th>");
            //    sb.Append("<th width=\"4%\">&nbsp;</th>");
            //    sb.Append("</tr>");
            //}
            foreach (var item in list)
            {
                sb.Append(" <tr>");
                sb.Append("     <td>" + item.RowId.ToString() + ".</td>");
                sb.Append("     <td><a href=\"/City/NewsFeed/2/" + item.CityId.ToString() + "\">" + item.CityName + "</a></td>");
                sb.Append("     <td>" + item.RecommendedCount.ToString() + "</td>");
                sb.Append("     <td>" + item.WanttoGoCount.ToString() + "</td>");
                sb.Append("     <td>" + item.BucketListCount.ToString() + "</td>");
                sb.Append("     <td>" + item.Rating.ToString() + "</td>");
                sb.Append("     <td class=\"cssTaskSn\"><a href=\"javascript:void(0);\" onclick=\"GetCityId(" + item.CityId + ");\">" + item.Sort.ToString() + "</a></td>");
                sb.Append("     <td class=\" alignR\">");
                sb.Append("       <div class=\"controlBg control2\">");
                sb.Append("          <span class=\"bg\">");
                //sb.Append("          <a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddNewCity?countryId=" + item.CountryId + "&cityId=" + item.CityId + "','Get','',function(){$('#countryId').val(" + item.CountryId + "); $('#cityId').val(" + item.CityId + ");GetCountryByJquery();CityManage.editInfo($('#cityId').val());$('#Cityform').bValidator({singleError:true}); },'Edit City');\" class=\"edit NborderL\"  ></a>");
                sb.Append("          <a href=\"/admin/home/EditCityManage/" + item.CityId.ToString() + "\" class=\"edit NborderL\"  ></a>");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"CityManage.DelCity(" + item.CityId + ");\" class=\"delete " + (Convert.ToInt32(item.StatusId) != 3 ? "" : "active") + "\" ></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }

            return sb.ToString();
        }

        public JsonResult SetCityIsDel()
        {
            int countryId = RequestHelper.GetValInt("objId");
            var bmi = _widgetService.SetCityIsDel(countryId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditCityManage(int id = 0)
        {
            City city = _supplierService.GetCityInfo(id);

            var countryList = _supplierService.GetAllCountryItem();
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem() { Text = "Select One", Value = "0" });
            countryList.ForEach(c =>
            {
                selectList.Add(new SelectListItem() { Text = c.Value, Value = c.Key });
            });
            if (city != null)
                selectList.FirstOrDefault(c => c.Value == city.CountryId.ToString()).Selected = true;
            else
                selectList.FirstOrDefault(c => c.Value == "0").Selected = true;

            ViewData["City"] = city;
            ViewData["CountryList"] = selectList;
            return View();
        }

        /// <summary>
        /// 更新城市信息
        /// </summary>
        /// <returns></returns>
        public JsonResult EditCityManageSave()
        {
            City city = new City();
            city.CityDescription = new CityDescription();

            city.CityId = RequestHelper.GetValInt("cityId");
            city.CountryId = RequestHelper.GetValInt("countryId");
            city.CityName = RequestHelper.GetVal("cityName");
            city.CoordsLongitude = RequestHelper.GetValDecimal("longitude");
            city.CoordsLatitude = RequestHelper.GetValDecimal("latitude");
            city.FlagSrc = RequestHelper.GetVal("flag");

            city.CityDescription.Txt = RequestHelper.GetVal("txt");
            city.CityDescription.Url = RequestHelper.GetVal("cityUrl");
            city.CityDescription.PhotoUrl = RequestHelper.GetVal("pho");
            city.CityDescription.TTDUrl = RequestHelper.GetVal("ttd");
            city.CityDescription.ShoppingUrl = RequestHelper.GetVal("sho");
            city.CityDescription.RestaurantsUrl = RequestHelper.GetVal("res");
            city.CityDescription.HotelsUrl = RequestHelper.GetVal("hot");
            city.CityDescription.EventsUrl = RequestHelper.GetVal("eve");

            string attSrc = RequestHelper.GetVal("flag");

            //图片处理
            if (!string.IsNullOrEmpty(city.FlagSrc))
            {
                string savePath = Server.MapPath("~/Staticfile/flag/cityflag/");
                string path = Server.MapPath("~" + city.FlagSrc);
                string extension = Path.GetExtension(path);
                string saveName = string.Empty;
                if (city.CityId > 0)
                {
                    saveName = "cityflag_170_170_" + city.CityId.ToString() + extension;
                }
                else
                {
                    saveName = Guid.NewGuid().ToString().Replace("-", string.Empty) + extension;
                }
                if (!Directory.Exists(savePath))
                {
                    Directory.CreateDirectory(savePath);
                }
                //将临时文件夹中的图片复制到市旗目录
                System.IO.File.Copy(path, savePath + saveName, true);

                city.FlagSrc = "/Staticfile/flag/cityflag/" + saveName;
            }

            var bm = _ibtService.EditCity(city);

            if (bm.Success && !string.IsNullOrEmpty(attSrc))
            {//更新图片发newsfeed
                attSrc = attSrc.Replace("Cut_", string.Empty);
                string filePath = Server.MapPath("~" + attSrc);
                string guid = Guid.NewGuid().ToString().Replace("-", string.Empty);
                NewsFeedService newsFeedService = new NewsFeedService();
                _ibtService.AttachmentNewsFeedImage(UserId, (int)Identifier.AttachmentType.NewsFeed, guid, filePath);
                var bmi = newsFeedService.AddNewsFeed(UserId, city.CountryId, Convert.ToInt32(bm.Message), 0, Identifier.IBTType.City, Identifier.FeedActiveType.PostComments, city.CityName, string.Empty, DateTime.Now);
                _ibtService.SetAttachmentObjId(guid, bmi.Item);
            }

            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UploadCityFlag(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    string filePath = Server.MapPath("~/Staticfile/temp/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string extension = Path.GetExtension(fileData.FileName);
                    string fileName = Guid.NewGuid().ToString().Replace("-", "") + extension;
                    //保存原图
                    fileData.SaveAs(filePath + fileName);
                    //原图处理
                    Image o = Image.FromFile(filePath + fileName);
                    if (o.Width > 170 || o.Height > 170)
                    {
                        MakeThumbImage.MakeThumbPhoto(filePath + fileName, filePath + "Cut_" + fileName, 170, 170, MakeThumbImage.ThumbPhotoModel.Fit);
                    }
                   
                    else
                    {
                        if (System.IO.File.Exists(fileName + "Cut_" + fileName))
                            System.IO.File.Delete(fileName + "Cut_" + fileName);
                        System.IO.File.Copy(filePath + fileName, filePath + "Cut_" + fileName);
                    }
                    return Json(new BoolMessageItem<string>("/Staticfile/temp/Cut_" + fileName, true, "Success"), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new BoolMessageItem<string>(string.Empty, false, ex.Message), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new BoolMessageItem<string>(string.Empty, false, "Please choose files to upload !"));
            }
        }

        public JsonResult ImportCityDescriptSave(HttpPostedFileBase fileData)
        {
            BoolMessage bm = new BoolMessage(false, string.Empty);
            if (fileData != null && Validation.IsMatchRegEx(fileData.FileName.ToLower(), false, @"^.*\.(xls)$"))
            {
                string savePath = Server.MapPath("~/Staticfile/temp/" + fileData.FileName);
                fileData.SaveAs(savePath);
                FileStream fs = new FileStream(savePath, FileMode.Open);
                bm = _ibtService.ImportCityDescript(fs);
            }
            return Json(bm, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ImportCityDescript()
        {
            return View();
        }

        #endregion

        #endregion

        #region Landing Management

        #region IndexTop10Supplies
        /// <summary>
        /// Go Search
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GoSearch()
        {
            return View();
        }

        /// <summary>
        /// Upload Img
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateLanding()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadImg(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    int userId = RequestHelper.GetValInt("UserId");
                    int supplyId = RequestHelper.GetValInt("SupplyId");
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Supplier/Index/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    string saveName = userId.ToString() + "_" + supplyId.ToString() + "_Indexsupply" + fileExtension; // 保存文件名称
                    fileData.SaveAs(filePath + saveName);
                    //原图处理
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + "Cut_" + saveName, 270, 103, MakeThumbImage.ThumbPhotoModel.Cut);

                    //生成270 * 103图片 todo(xg)                   
                    int dir = supplyId / 500;
                    //string staticPath = "/Staticfile/Supplier/Index/" + dir.ToString() + "/" + userId.ToString() + "/" + DateTime.Now.ToString("yyyyMM") + "/";
                    string staticPath = "/Staticfile/Supplier/Index/" + DateTime.Now.ToString("yyyyMM") + "/";
                    string path = System.Web.HttpContext.Current.Server.MapPath("~" + staticPath);
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, path + "Index_" + saveName, 270, 103, MakeThumbImage.ThumbPhotoModel.Cut);

                    /*数据库存储430*282的图片地址*/
                    saveName = (staticPath + "Index_" + saveName);

                    return Json(new { Success = true, FileName = fileName, SaveName = saveName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose files to upload !" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult EditIndexSupply(string returnUrl)
        {
            string imgSrc = RequestHelper.GetVal("imgSrc");
            string txtDesc = RequestHelper.GetUrlDecodeVal("txtDesc");
            int ITSId = RequestHelper.GetValInt("IndexId");

            var bmi = _supplierService.EditIndexSupply(ITSId, imgSrc, txtDesc);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        public ActionResult LandingManage()
        {
            this.Title = "Landing Management";
            this.PageTitle = "Landing Management";
            var list = _mapsService.GetIndexTopList();
            System.Text.StringBuilder sb = new StringBuilder();
            sb.Append("[");
            int idx = 1;
            foreach (var item in list)
            {
                sb.Append("['" + item.Title.Replace("'", "\'") + "'," + item.lat.ToString() + "," + item.lng.ToString() + "," + idx + "," + item.Id.ToString() + "," + item.typ.ToString() + ",'" + PageHelper.GetSupplyMapsIcon(item.Id) + "'],\n");
                idx++;
            }
            sb.Append("];");

            ViewBag.MapsTopList = sb.ToString();
            return View();

        }

        /// <summary>
        /// Landing data
        /// </summary>
        /// <returns></returns>
        public JsonResult LandingListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;


            int total = 0;
            int itemTotal = 0;
            string strWhere = "";
            var list = LandingListData(strWhere, page, pageSize, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 返回LandingList的数据
        /// </summary>
        /// <returns></returns>
        private string LandingListData(string strWhere, int page, int pageSize, out int total, out int itemTotal)
        {
            var list = _supplierService.GetHomeTopList(strWhere, page, pageSize, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            /*20130206裕冲注释*/
            //if (page < 2)
            //{
            //    sb.Append("<tr id=\"0\">");
            //    sb.Append(" <th width=\"3%\">&nbsp;</th>");
            //    sb.Append(" <th width=\"10%\">Date Added</th>");
            //    sb.Append(" <th width=\"12%\" >Name</th>");
            //    sb.Append(" <th  width=\"10%\">Country</th>");
            //    sb.Append(" <th  width=\"10%\">City</th>");
            //    sb.Append(" <th  width=\"10%\">State</th>");
            //    sb.Append(" <th  width=\"10%\">Type</th>");
            //    sb.Append(" <th  width=\"10%\">Score</th>");
            //    sb.Append(" <th width=\"4%\">&nbsp;</th>");
            //    sb.Append("</tr>");
            //}
            /*注释结束*/
            string date = "";
            string ITTypeStr = "";
            foreach (var item in list)
            {
                sb.Append(" <tr id=\"" + item.Id + "\">");
                sb.Append("     <td>" + item.row_number.ToString() + ".</td>");
                date = DateTime.Parse(item.CreateDate.ToString()).ToString("d MMMM yyyy");
                sb.Append("     <td>" + date + "</td>");
                sb.Append("     <td><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a></td>");
                sb.Append("     <td><a href=\"#\">" + item.CountryName + "</a></td>");
                sb.Append("     <td>" + item.CityName + "</td>");
                sb.Append("     <td>" + item.InfoState + "</td>");
                if (item.ITType == (int)Identifier.IBTType.Supply)
                {
                    ITTypeStr = "Supplier";
                }
                else if (item.ITType == (int)Identifier.IBTType.Country)
                {
                    ITTypeStr = "Country";
                }
                else
                {
                    ITTypeStr = "City";
                }
                sb.Append("     <td>" + ITTypeStr + "</td>");
                //sb.Append("     <td>" + item.Score.ToString() + "</td>");
                sb.Append("     <td class=\" alignR\">");
                sb.Append("       <div class=\"controlBg control2\">");
                sb.Append("          <span class=\"bg\">");
                sb.Append("         <a  class=\"edit NborderL \" href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/UpdateLanding?indexId=" + item.Id + "','Get','',function(){LandingManage.uploadImg();$('#hidIndexId').val(" + item.Id + ");$('#hidSupplyId').val(" + item.SupplyId + ")},'Edit');\" ></a>");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"LandingManage.DelIndexSupply(" + item.Id + ");\" class=\"delete " + (Convert.ToInt32(item.StatusId) != 3 ? "" : "active") + "\" ></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }
            //if (itemTotal == 0)
            //{
            //    sb.Append(" <tr>");
            //    sb.Append("   <td class=\" alignR\" colspan=\"9\">");
            //    sb.Append("      <div class=\"controlBg control1\">");
            //    sb.Append("      <span class=\"bg\">");
            //    sb.Append("         <a  class=\"add NborderR \" href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/GoSearch','Get','',function(){selectSpan();$('.jscroll-e').show();scroll_init(scroll_blueSearch);SearchManage.initData();SearchManage.init();$('#popupSearch').bValidator({singleError:true});},'Search');\" ></a>");
            //    sb.Append("      </span>");
            //    sb.Append("      </div>");
            //    sb.Append("   </td>");
            //    sb.Append(" </tr>");
            //}

            return sb.ToString();
        }

        [HttpPost]
        public JsonResult SetIndexSupplySort(string returnUrl)
        {

            string ArrayData = RequestHelper.GetVal("arrayData");
            List<string> SortList = new List<string>() { ArrayData };
            List<int> IndexIds = new List<int>() { };
            string[] arr = ArrayData.Split(',');//拆分后的字符数组  
            int indexId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    SortList.Remove(arr[i]);//存在则从SortList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out indexId);
                    IndexIds.Add(indexId);
                }
            }
            var bmi = _supplierService.SetIndexSupplySort(IndexIds);
            return Json(bmi);
        }

        public JsonResult SetLandingIsDel()
        {
            int indexId = RequestHelper.GetValInt("objId");
            var bmi = _supplierService.SetIndexSupplyIsDel(indexId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }

        public JsonResult AddIndexItem()
        {
            int objId = RequestHelper.GetValInt("objId");
            int objType = RequestHelper.GetValInt("objType");
            int scroe = RequestHelper.GetValInt("scroe");
            var bmi = _supplierService.AddIndexSupply(objId, objType, scroe);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        #endregion

        #region Suppliers(Go Search)
        /// <summary>
        /// City data
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCity()
        {
            int countryId = RequestHelper.GetValInt("objId");
            var list = CityItemList(countryId);
            return Json(new BoolMessageItem<int>(1, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回CityItemList的数据
        /// </summary>
        /// <returns></returns>
        private string CityItemList(int countryId)
        {
            var list = _supplierService.GetCityList(countryId);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append(" <li onclick=\"getCityId(" + item.CityId + ")\">" + item.CityName + "</li>");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Select data
        /// </summary>
        /// <returns></returns>
        public JsonResult SelectItemAjx()
        {
            string selAttrIds = RequestHelper.GetVal("selAttrIds");
            List<string> TypeList = new List<string>() { selAttrIds };
            List<int> AttrIdList = new List<int>() { };
            string[] arr = selAttrIds.Split(',');//拆分后的字符数组  
            int attrId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    TypeList.Remove(arr[i]);//存在则从ArrayList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out attrId);
                    AttrIdList.Add(attrId);
                }
            }
            var list = SelectItemList(AttrIdList);

            return Json(new BoolMessageItem<int>(1, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SelectItemList的数据
        /// </summary>
        /// <returns></returns>
        private string SelectItemList(IList<int> SupplyAttrType)
        {
            int attrId = 0;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < SupplyAttrType.Count(); i++)
            {
                attrId = SupplyAttrType[i];
                sb.Append("  <span class=\"selectedBox\" objId=" + attrId + ">" + _supplierService.GetAttrInfo(attrId).AttrDataName + " <a href=\"#\" onclick=\"remove(" + attrId + ");\">x</a></span>");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Supply all data
        /// </summary>
        /// <returns></returns>
        public JsonResult SupplySearchAjx()
        {
            int pageType = RequestHelper.GetValInt("pageType");//1:Landing Management 2:Recommend Management
            int type = RequestHelper.GetValInt("type", 1);
            int tab = RequestHelper.GetValInt("tab");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            int sortby = RequestHelper.GetValInt("sortBy");
            string keyWord = RequestHelper.GetVal("keyWord");
            string countryId = RequestHelper.GetVal("countryId");
            string cityId = RequestHelper.GetVal("cityId");
            string cateIds = RequestHelper.GetVal("cateIds");
            if (keyWord == "Type in something to search")
                keyWord = "";
            //判断拼接条件字符串
            string strKeywordCondition = string.IsNullOrEmpty(keyWord) ? "" : " AND (temp.Title LIKE '%" + keyWord.ToString() + @"%')";
            string strCountryCondition = string.IsNullOrEmpty(countryId) ? "" : " AND temp.CountryId=" + countryId;
            string strCityCondition = string.IsNullOrEmpty(cityId) ? "" : " AND temp.CityId=" + cityId;
            string strCateCondition = string.IsNullOrEmpty(cateIds) ? "" : " AND EXISTS( SELECT 1 FROM SupplyAttributeData  WHERE temp.SupplyId = SupplyId AND ArrtDataId IN (" + cateIds + "))";
            string strRecommend = ""; //需要修改
            if (tab == 1) //Recommended
            {
                strRecommend = " AND temp.SupplyId IN (SELECT ObjId FROM RRWB WHERE StatusId=1 AND IBTType=3 AND Recommended=1 Group BY ObjId) ";
            }
            else if (tab == 2) // Not Recommended
            {
                strRecommend = " AND temp.SupplyId IN (SELECT ObjId FROM RRWB WHERE StatusId=1 AND IBTType=3 AND Recommended=0 Group BY ObjId) ";
            }
            else
            {
                strRecommend = "";
            }
            string strSortBy = "";
            if (sortby == 2)
            {
                strSortBy = " ORDER BY  WanttoGo DESC";
            }
            else if (sortby == 1)
            {
                strSortBy = " ORDER BY  Recommended DESC";
            }
            else
            {
                strSortBy = "";
            }
            string strWhere = strKeywordCondition + strCountryCondition + strCityCondition + strCateCondition + strRecommend;

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = SupplySearchListData(pageType, type, tab, page, pageSize, strWhere, strSortBy, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SupplySearchListData的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string SupplySearchListData(int pageType, int type, int tab, int page, int pageSize, string strWhere, string strOrderby, out int total, out int itemTotal)
        {
            IList<SupplyListShort> list = new List<SupplyListShort>();

            int indexTotal = 0;
            /*20130205裕冲修改:在存储过程中分页后得到结果*/
            //var list = _searchService.GetSupplyAdvancedSearch(UserId, strWhere, page, pageSize, out total);   
            //list = _searchService.GetSupplyAdvancedSearchByPage(UserId, strWhere, strOrderby, page, pageSize, out total);
            list = _searchService.GetRecommendAdvancedSearchByPage(UserId, strWhere, strOrderby, page, pageSize, out total);
            /*修改结束*/
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string imgshow = "";
            string imgSrc = "";
            string alt = "";
            string click = "";
            //获取选中的10个recommend
            IList<IndexTop10Supplies> top10List = _supplierService.GetHomeTopListCheck();
            IList<WidgetMostRecommentdPlace> widgetList = _widgetService.GetHomeRecommendList();
            int[] top10ids = (from a in top10List where a.ITType == 1 select a.ObjId).ToArray();
            int[] top10ids2 = (from a in top10List where a.ITType == 2 select a.ObjId).ToArray();
            int[] top10ids3 = (from a in top10List where a.ITType == 3 select a.ObjId).ToArray();
            int[] widgetids = (from a in widgetList where a.MRPType == 1 select a.ObjId).ToArray();
            int[] widgetids2 = (from a in widgetList where a.MRPType == 2 select a.ObjId).ToArray();
            int[] widgetids3 = (from a in widgetList where a.MRPType == 3 select a.ObjId).ToArray();
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append("   <div class=\"control\">");

                if (pageType == 1) //Landing Management
                {
                    //int count = _supplierService.GetHomeTopList(" AND dbo.IndexTop10Supplies.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).Count;
                    if (top10ids.Contains(item.ObjId) || top10ids2.Contains(item.ObjId) || top10ids3.Contains(item.ObjId))
                    {
                        //int indexId = _supplierService.GetHomeTopList(" AND dbo.IndexTop10Supplies.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).FirstOrDefault().Id;
                        int indexId = (from a in top10List where a.ObjId == item.ObjId select a.ITSId).FirstOrDefault();
                        imgshow = "/App/images/icon/lower.gif";
                        alt = "Lower";
                        click = "LandingManage.DelIndexSupply(" + indexId + ");"; //删除
                    }
                    else
                    {
                        int score = _supplierService.GetHomeTopList("", page, pageSize, out indexTotal).Count + 1;
                        imgshow = "/App/images/icon/add.gif";
                        alt = "ADD";
                        click = "LandingManage.AddIndexItem(" + item.ObjId + "," + item.Type + "," + score + ");"; //添加
                    }
                }
                else
                {
                    //int count = _widgetService.GetHomeRecommendList(" AND dbo.WidgetMostRecommentdPlaces.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).Count;
                    if (widgetids.Contains(item.ObjId) || widgetids2.Contains(item.ObjId) || widgetids3.Contains(item.ObjId))
                    {
                        //int MRPId = _widgetService.GetHomeRecommendList(" AND dbo.WidgetMostRecommentdPlaces.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).FirstOrDefault().MRPId;
                        int MRPId = (from a in widgetList where (a.ObjId == item.ObjId && a.MRPType == item.Type) select a.MRPId).FirstOrDefault();
                        imgshow = "/App/images/icon/lower.gif";
                        alt = "Lower";
                        click = "AddNewRecommend.DelRecommend(" + MRPId + ");"; //删除
                    }
                    else
                    {
                        int score = _supplierService.GetHomeTopList("", page, pageSize, out indexTotal).Count + 1;
                        imgshow = "/App/images/icon/add.gif";
                        alt = "ADD";
                        click = "AddNewRecommend.AddRecommendItem(" + item.ObjId + "," + item.Type + "," + score + ");"; //添加
                    }
                }
                sb.Append("      <a href=\"javascript:void(0);\" onclick=\"" + click + "\"><img src=\"" + imgshow + "\" alt=\"" + alt + "\" /></a>");
                sb.Append("   </div>");
                if (item.Type == (int)Identifier.IBTType.Supply)
                {
                    imgSrc = PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn);
                }
                else
                {
                    imgSrc = item.CoverPhotoSrc;
                }
                sb.Append("    <a href=\"" + item.TitleUrl + "\" class=\"img\"><img src=\"" + imgSrc + "\" alt=\"" + item.Title + "\"  /></a>");
                sb.Append("    <dl>");
                sb.Append("      <dt>");
                //标题（城市/国家）
                if (item.Type == (int)Identifier.IBTType.Country)
                {
                    sb.Append("  <span class=\"title\"><a href=\"" + item.TitleUrl + "\" >" + item.Title + "</a></span>");
                }
                else if (item.Type == (int)Identifier.IBTType.City)
                {
                    sb.Append("  <span class=\"title\"><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a> <span class=\"f11  \">" + item.CountryName + "</span></span>");
                }
                else
                {
                    sb.Append("  <span class=\"title\"><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.CountryName + "</span></span>");
                }
                //string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                //sb.Append("        <span class=\"title\"> <a href=\"" + item.SupplyUrl + "\" >" + item.Title + "</a>  " + addr_city + item.CountryName + "</span>");
                #region //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）

                //Elson(showNick) and 9 Friends has Been There 
                //string showNick = type == 1 ? "You " : "";
                //int friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                //string strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //int selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (friendCount > 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}

                //int friendCount = _supplierService.HasBeenThereFriendsCount(UserId, 0, 0, item.SupplyId);
                //string strcount = friendCount == 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //if (friendCount == 0)
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " are the first to have been there!</span>");
                //else
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"Boxy.get(this).hideAndUnload();ibtAdmin.loadUrlPop('/ShowFriend/3/" + item.SupplyId.ToString() + "','Get','',null,'" + strcount + " Who\\'ve Been There');\">" + friendCount.ToString() + " Friends</a> has been there!</span>");


                #endregion
                sb.Append("      </dt>");
                sb.Append("    </dl>");
                sb.Append(" </li>");

                //sb.Append("     <div class=\"fixbox\">");
                //string TitleUrl = "/Supplier/v/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.Title);
                //sb.Append("       <a class=\"userPic\" href=\"" + TitleUrl + "\"><img alt=\"Supplier Name\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn) + "\"></a>");
                //sb.Append("       <dl>");
                //string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                //sb.Append("         <dt><a href=\"" + TitleUrl + "\">" + item.Title + "</a></dt>");
                //sb.Append("         <dd class=\"italic Ca0a0a0\"><span class=\"txt\">" + addr_city + item.CountryName + "</span></dd>");
                //sb.Append("       </dl>");
                //sb.Append("    </div>");
                //sb.Append(" </li>");
            }
            return sb.ToString();
        }

        public JsonResult SupplySearchAjx_Visited()
        {
            int pageType = RequestHelper.GetValInt("pageType");//1:Landing Management 2:Recommend Management
            int type = RequestHelper.GetValInt("type", 1);
            int tab = RequestHelper.GetValInt("tab");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            int sortby = RequestHelper.GetValInt("sortBy");
            string keyWord = RequestHelper.GetVal("keyWord");
            string countryId = RequestHelper.GetVal("countryId");
            string cityId = RequestHelper.GetVal("cityId");
            string cateIds = RequestHelper.GetVal("cateIds");
            if (keyWord == "Type in something to search")
                keyWord = "";
            //判断拼接条件字符串
            string strKeywordCondition = string.IsNullOrEmpty(keyWord) ? "" : " AND temp.Title LIKE '%" + keyWord.ToString() + @"%'";
            string strCountryCondition = string.IsNullOrEmpty(countryId) ? "" : " AND temp.CountryId=" + countryId;
            string strCityCondition = string.IsNullOrEmpty(cityId) ? "" : " AND temp.CityId=" + cityId;
            string strCateCondition = string.IsNullOrEmpty(cateIds) ? "" : " AND EXISTS( SELECT 1 FROM SupplyAttributeData  WHERE temp.SupplyId = SupplyId AND ArrtDataId IN (" + cateIds + "))";
            string strVisited = "";
            if (tab == 1) //Visited
            {
                strVisited = " AND (temp.SupplyId IN (SELECT SupplyId FROM PostVists WHERE StatusId=1 AND IBTType=3 Group BY SupplyId) OR temp.CountryId IN (SELECT CountryId FROM PostVists WHERE StatusId=1 AND IBTType=1 Group BY CountryId) OR temp.CityId IN (SELECT CityId FROM PostVists WHERE StatusId=1 AND IBTType=2 Group BY CityId)) ";
            }
            else if (tab == 2) // Not Visited
            {
                strVisited = " AND (temp.SupplyId NOT IN (SELECT SupplyId FROM PostVists WHERE StatusId=1 AND IBTType=3 Group BY SupplyId) OR temp.CountryId IN (SELECT CountryId FROM PostVists WHERE StatusId=1 AND IBTType=1 Group BY CountryId) OR temp.CityId IN (SELECT CityId FROM PostVists WHERE StatusId=1 AND IBTType=2 Group BY CityId)) ";
            }
            else
            {
                strVisited = "";
            }
            string strSortBy = "";
            if (sortby == 2)
            {
                strSortBy = " ORDER BY  WanttoGo DESC";
            }
            else if (sortby == 1)
            {
                strSortBy = " ORDER BY  Recommended DESC";
            }
            else
            {
                strSortBy = "";
            }
            string strWhere = strKeywordCondition + strCountryCondition + strCityCondition + strCateCondition + strVisited;

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = SupplySearchListData_Visited(pageType, type, tab, page, pageSize, strWhere, strSortBy, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        private string SupplySearchListData_Visited(int pageType, int type, int tab, int page, int pageSize, string strWhere, string strOrderby, out int total, out int itemTotal)
        {
            IList<SupplyListShort> list = new List<SupplyListShort>();

            int indexTotal = 0;
            /*20130205裕冲修改:在存储过程中分页后得到结果*/
            //var list = _searchService.GetSupplyAdvancedSearch(UserId, strWhere, page, pageSize, out total);   
            list = _searchService.GetRecommendAdvancedSearchByPage(UserId, strWhere, strOrderby, page, pageSize, out total);
            /*修改结束*/
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string imgshow = "";
            string alt = "";
            string click = "";
            string imgSrc = "";
            IList<IndexTop10Supplies> top10List = _supplierService.GetHomeTopListCheck();
            IList<WidgetMostVisitedPlace> visitedList = _widgetService.GetHomeVisitedCheck();
            int[] top10ids = (from a in top10List where a.ITType == 1 select a.ObjId).ToArray();
            int[] top10ids2 = (from a in top10List where a.ITType == 2 select a.ObjId).ToArray();
            int[] top10ids3 = (from a in top10List where a.ITType == 3 select a.ObjId).ToArray();
            int[] visitedids = (from a in visitedList where a.MVPType == 1 select a.ObjId).ToArray();
            int[] visitedids2 = (from a in visitedList where a.MVPType == 2 select a.ObjId).ToArray();
            int[] visitedids3 = (from a in visitedList where a.MVPType == 3 select a.ObjId).ToArray();
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append("   <div class=\"control\">");

                if (pageType == 1) //Landing Management
                {
                    //int count = _supplierService.GetHomeTopList(" AND dbo.IndexTop10Supplies.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).Count;

                    if (top10ids.Contains(item.ObjId) || top10ids2.Contains(item.ObjId) || top10ids3.Contains(item.ObjId))
                    {
                        //int indexId = _supplierService.GetHomeTopList(" AND dbo.IndexTop10Supplies.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).FirstOrDefault().Id;
                        int indexId = (from a in top10List where a.ObjId == item.ObjId select a.ITSId).FirstOrDefault();
                        imgshow = "/App/images/icon/lower.gif";
                        alt = "Lower";
                        click = "LandingManage.DelIndexSupply(" + indexId + ");"; //删除
                    }
                    else
                    {
                        int score = _supplierService.GetHomeTopList("", page, pageSize, out indexTotal).Count + 1;
                        imgshow = "/App/images/icon/add.gif";
                        alt = "ADD";
                        click = "LandingManage.AddIndexItem(" + item.ObjId + "," + item.Type + "," + score + ");"; //添加
                    }
                }
                else
                {
                    //int count = _widgetService.GetHomeVisitedList(" AND dbo.WidgetMostVisitedPlaces.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).Count;

                    if (visitedids.Contains(item.ObjId) || visitedids2.Contains(item.ObjId) || visitedids3.Contains(item.ObjId))
                    {
                        //int MVPId = _widgetService.GetHomeVisitedList(" AND dbo.WidgetMostVisitedPlaces.SupplyId=" + item.SupplyId.ToString(), page, pageSize, out indexTotal).FirstOrDefault().MVPId;
                        int MVPId = (from a in visitedList where (a.ObjId == item.ObjId && a.MVPType == item.Type) select a.MVPId).FirstOrDefault();
                        imgshow = "/App/images/icon/lower.gif";
                        alt = "Lower";
                        click = "AddNewVisited.DelVisited(" + MVPId + ");"; //删除
                    }
                    else
                    {
                        int score = _supplierService.GetHomeTopList("", page, pageSize, out indexTotal).Count + 1;
                        imgshow = "/App/images/icon/add.gif";
                        alt = "ADD";
                        click = "AddNewVisited.AddVisitedItem(" + item.ObjId + "," + item.Type + "," + score + ");"; //添加
                    }
                }
                sb.Append("      <a href=\"javascript:void(0);\" onclick=\"" + click + "\"><img src=\"" + imgshow + "\" alt=\"" + alt + "\" /></a>");
                sb.Append("   </div>");
                if (item.Type == (int)Identifier.IBTType.Supply)
                {
                    imgSrc = PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn);
                }
                else
                {
                    imgSrc = item.CoverPhotoSrc;
                }
                sb.Append("    <a href=\"" + item.TitleUrl + "\" class=\"img\"><img src=\"" + imgSrc + "\" alt=\"" + item.Title + "\"  /></a>");
                sb.Append("    <dl>");
                sb.Append("      <dt>");
                //标题（城市/国家）
                if (item.Type == (int)Identifier.IBTType.Country)
                {
                    sb.Append("  <span class=\"title\"><a href=\"" + item.TitleUrl + "\" >" + item.Title + "</a></span>");
                }
                else if (item.Type == (int)Identifier.IBTType.City)
                {
                    sb.Append("  <span class=\"title\"><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a> <span class=\"f11  \">" + item.CountryName + "</span></span>");
                }
                else
                {
                    sb.Append("  <span class=\"title\"><a href=\"" + item.TitleUrl + "\">" + item.Title + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.CountryName + "</span></span>");
                }
                //string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                //sb.Append("        <span class=\"title\"> <a href=\"" + item.SupplyUrl + "\" >" + item.Title + "</a>  " + addr_city + item.CountryName + "</span>");

                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 
                //string showNick = type == 1 ? "You " : "";
                //int friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                //string strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //int selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (friendCount > 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                sb.Append("      </dt>");
                sb.Append("    </dl>");
                sb.Append(" </li>");
            }
            return sb.ToString();
        }
        #endregion

        #endregion

        #region Widget_country_management

        #region Load CountryList Data

        [HttpGet]
        public ActionResult WidgetCountriesManage()
        {
            this.Title = "Widget Management";
            this.PageTitle = "Widget Management";
            return View();
        }

        public JsonResult WidgetCountryListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            string strWhere = "";
            var list = WidgetCountryListData(strWhere, page, pageSize, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        public string WidgetCountryListData(string strWhere, int page, int pageSize, out int total, out int itemTotal)
        {
            var CountryData = _supplierService.GetAllWidgetIBTCountry(strWhere, page, pageSize, out total);
            itemTotal = CountryData.Count;
            StringBuilder sbHtml = new StringBuilder();
            //if (page < 2)
            //{
            //    sbHtml.Append("<tr>");
            //    sbHtml.Append("  <th width=\"3%\">&nbsp;</th>");
            //    sbHtml.Append("  <th width=\"6%\">Date Added</th>");
            //    sbHtml.Append("  <th width=\"8%\">Country</th>");
            //    sbHtml.Append("  <th width=\"4%\">&nbsp;</th>");
            //    sbHtml.Append("</tr>");
            //}
            int i = 1;
            string date = "";
            foreach (WidgetIBT itemCountry in CountryData)
            {
                sbHtml.Append("<tr id=" + itemCountry.ibtId + ">");
                sbHtml.Append("  <td>" + i + "</td>");
                date = DateTime.Parse(itemCountry.CreateDate.ToString()).ToString("d MMMM yyyy");
                sbHtml.Append("  <td>" + date + "</td>");
                sbHtml.Append("  <td><a href=\"/Country/NewsFeed/1/" + itemCountry.ObjId.ToString() + "\">" + itemCountry.ObjName + "</a></td>");
                sbHtml.Append("  <td class=\"alignR\">");
                sbHtml.Append("    <div class=\"controlBg control1\">");
                sbHtml.Append("      <span class=\"bg\">");
                sbHtml.Append("      <a href=\"javascript:void(0);\" onclick=\"WidgetCountryManage.DelWidgetCountry(" + itemCountry.ibtId + ");\" class=\"delete NborderR\"></a>");
                sbHtml.Append("      </span>");
                sbHtml.Append("    </div>");
                sbHtml.Append("  </td>");
                sbHtml.Append("</tr>");
                i++;
            }
            //if (itemTotal == 0)
            //{
            //    sbHtml.Append("<tr>");
            //    sbHtml.Append("  <td class=\"alignR\" colspan=\"9\">");
            //    sbHtml.Append("    <div class=\"controlBg control1\">");
            //    sbHtml.Append("      <span class=\"bg\">");
            //    sbHtml.Append("        <a href=\"javascript:void(0);\" class=\"add NborderR\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddWidgetCountry','Get','',function(){$('.jscroll-e').show();scroll_init(scroll_blueSearch2);SearchData.initData(1);SearchData.init(1);},'Search');\" ></a>");
            //    sbHtml.Append("      </span>");
            //    sbHtml.Append("    </div>");
            //    sbHtml.Append("  </td>");
            //    sbHtml.Append("<tr>");
            //}
            return sbHtml.ToString();
        }
        #endregion

        #region delete Country

        public JsonResult WidgetCountryManegeDelete()
        {
            int objId = RequestHelper.GetValInt("objId");
            var bmi = _supplierService.DeleteWidgetIBTCountry(objId);
            if (bmi.Success)
            {

            }
            return Json(bmi);


        }


        #endregion

        #region click Add button
        [HttpGet]
        public ActionResult AddWidgetCountry()
        {//点击增加的方法
            return View();
        }

        public JsonResult SearchCountryList()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            string txtValue = RequestHelper.GetVal("keyword");//试图中自定义标签的值 
            if (txtValue == "Type in something to search")
                txtValue = "";
            var list = CountryListData(page, pageSize, out total, out itemTotal, txtValue);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        private string CountryListData(int page, int pageSize, out int total, out int itemTotal, string txtValue)
        {
            int ibtTotal = 0;
            var list = _supplierService.GetAllSearchCountry(txtValue, page, pageSize, out total);
            itemTotal = list.Count();
            System.Text.StringBuilder sbHtml = new System.Text.StringBuilder();
            string imgshow = "";
            string alt = "";
            string click = "";
            foreach (var item in list)
            {
                sbHtml.Append(" <li>");
                sbHtml.Append("   <div class=\"control\">");


                int count = _supplierService.GetAllWidgetIBTCountry(" AND dbo.WidgetIBT.ObjId=" + item.CountryId.ToString(), page, pageSize, out ibtTotal).Count;
                if (count > 0)
                {
                    int ibtId = _supplierService.GetAllWidgetIBTCountry(" AND dbo.WidgetIBT.ObjId=" + item.CountryId.ToString(), page, pageSize, out ibtTotal).FirstOrDefault().ibtId;
                    imgshow = "/App/images/icon/lower.gif";
                    alt = "Lower";
                    click = "WidgetCountryManage.DelWidgetCountry(" + ibtId + ");"; //删除
                }
                else
                {
                    int score = _supplierService.GetAllWidgetIBTCountry("", page, pageSize, out ibtTotal).Count + 1;
                    imgshow = "/App/images/icon/add.gif";
                    alt = "ADD";
                    click = "WidgetCountryManage.AddWidgetCountry(" + item.CountryId + "," + score + ");"; //添加
                }

                sbHtml.Append("      <a href=\"javascript:void(0);\" onclick=\"" + click + "\"><img src=\"" + imgshow + "\" alt=\"" + alt + "\" /></a>");
                sbHtml.Append("   </div>");
                sbHtml.Append("    <a href=\"#\" class=\"img\"><img src=\"" + item.FlagSrc + "\" alt=\"" + item.CountryName + "\"  /></a>");
                sbHtml.Append("    <dl>");
                sbHtml.Append("      <dt>");
                sbHtml.Append("        <span class=\"title\"> <a href=\"#\" >" + item.CountryName + "</a></span>");
                sbHtml.Append("      </dt>");
                sbHtml.Append("    </dl>");
                sbHtml.Append(" </li>");

            }
            return sbHtml.ToString();
        }
        #endregion

        #region Edit
        /// <summary>
        /// PaiXuCountry
        /// </summary>
        /// <returns></returns>
        public JsonResult PaiXuCountry()
        {//将最新数据保存到数据库
            string arrList = RequestHelper.GetVal("arrayData");

            List<string> SortList = new List<string>() { arrList };
            List<int> ObjIds = new List<int>() { };
            string[] arr = arrList.Split(',');//拆分后的字符数组  
            int objId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    SortList.Remove(arr[i]);//存在则从SortList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out objId);
                    ObjIds.Add(objId);
                }
            }
            var bmi = _supplierService.SetWidgetCountrySort(ObjIds);
            if (bmi.Success)
            {

            }
            return Json(bmi);
        }

        public JsonResult AddWidgetCountryAjax()
        {
            int countryId = RequestHelper.GetValInt("objId");
            int scroe = RequestHelper.GetValInt("scroe");
            var bmi = _supplierService.AddWidgetCountry(UserId, countryId, scroe);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }

        #endregion

        #endregion

        #region Widget_city_management

        #region Load CityList Date
        public ActionResult WidgetCitiesManage()
        {//添加城市列表
            this.Title = "Widget Management";
            this.PageTitle = "Widget Management";
            return View();
        }

        public JsonResult WidgetCityListAjx()
        {

            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;
            string strWhere = "";
            int total = 0;
            int itemTotal = 0;
            var list = WidgetCityListData(strWhere, page, pageSize, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        public string WidgetCityListData(string strWhere, int page, int pageSize, out int total, out int itemTotal)
        {
            var list = _supplierService.GetAllWidgetIBTCity(strWhere, page, pageSize, out total);
            itemTotal = list.Count();
            StringBuilder sbHtml = new StringBuilder();
            //if (page < 2)
            //{
            //    sbHtml.Append("<tr>");
            //    sbHtml.Append("  <th width=\"3%\">&nbsp;</th>");
            //    sbHtml.Append("  <th width=\"6%\">Date Added</th>");
            //    sbHtml.Append("  <th width=\"8%\">Country</th>");
            //    sbHtml.Append("  <th width=\"8%\">City</th>");
            //    sbHtml.Append("  <th width=\"4%\">&nbsp;</th>");
            //    sbHtml.Append("</tr>");
            //}
            int i = 1;
            string date = "";
            foreach (WidgetIBTCity itemCity in list)
            {
                sbHtml.Append("<tr id=" + itemCity.ibtId + ">");
                sbHtml.Append("  <td>" + i + "</td>");
                date = DateTime.Parse(itemCity.CreateDate.ToString()).ToString("d MMMM yyyy");
                sbHtml.Append("  <td>" + date + "</td>");
                sbHtml.Append("  <td><a href=\"/Country/NewsFeed/1/" + itemCity.CountryId.ToString() + "\">" + itemCity.CountryName + "</a></td>");
                sbHtml.Append("  <td><a href=\"/City/NewsFeed/2/" + itemCity.ObjId.ToString() + "\">" + itemCity.ObjName + "</a></td>");
                sbHtml.Append("  <td class=\"alignR\">");
                sbHtml.Append("    <div class=\"controlBg control1\">");
                sbHtml.Append("      <span class=bg>");
                sbHtml.Append("      <a href=\"javascript:void(0);\" onclick=\"WidgetCityManage.DelWidgetCity(" + itemCity.ibtId + ");\" class=\"delete NborderR\" ></a>");
                sbHtml.Append("      </span>");
                sbHtml.Append("    </div>");
                sbHtml.Append("  </td>");
                sbHtml.Append("</tr>");
                i++;
            }
            //if (itemTotal == 0)
            //{
            //    sbHtml.Append("<tr>");
            //    sbHtml.Append("  <td class=\"alignR\" colspan=\"9\">");
            //    sbHtml.Append("    <div class=\"controlBg control1\">");
            //    sbHtml.Append("      <span class=\"bg\">");
            //    sbHtml.Append("        <a href=\"javascript:void(0);\" class=\"add NborderR\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddWidgetCity','Get','',function(){$('.jscroll-e').show();scroll_init(scroll_blueSearch3);SearchData.initData(2);SearchData.init(2);},'Search');\" ></a>");
            //    sbHtml.Append("      </span>");
            //    sbHtml.Append("    </div>");
            //    sbHtml.Append("  </td>");
            //    sbHtml.Append("<tr>");
            //}
            return sbHtml.ToString();

        }
        #endregion

        #region delete City

        public JsonResult WidgetCityManegeDelete()
        {
            int objId = RequestHelper.GetValInt("objId");
            var bmi = _supplierService.DeleteWidgetIBTCity(objId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        #endregion

        #region click add button

        public ActionResult AddWidgetCity()
        {//点击新增的方法
            return View();
        }

        public JsonResult SearchCityList()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            string txtValue = RequestHelper.GetVal("keyword");//试图中自定义标签的值 
            if (txtValue == "Type in something to search")
                txtValue = "";
            string countryId = RequestHelper.GetVal("countryId");
            string cityId = RequestHelper.GetVal("cityId");
            string strKeywordCondition = " AND cy.CityName LIKE '%" + txtValue + @"%'";
            string strCountryCondition = string.IsNullOrEmpty(countryId) ? "" : " AND cy.CountryId=" + countryId;
            string strCityCondition = string.IsNullOrEmpty(cityId) ? "" : " AND cy.CityId=" + cityId;
            string strWhere = strKeywordCondition + strCountryCondition + strCityCondition;
            var list = CityListData(page, pageSize, out total, out itemTotal, strWhere);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        private string CityListData(int page, int pageSize, out int total, out int itemTotal, string strWhere)
        {
            int indexTotal = 0;
            var list = _supplierService.GetAllSearchCity(strWhere, page, pageSize, out total);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string imgshow = "";
            string alt = "";
            string click = "";
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append("   <div class=\"control\">");


                int count = _supplierService.GetAllWidgetIBTCity(" AND bt.ObjId=" + item.CityId.ToString(), page, pageSize, out indexTotal).Count;
                if (count > 0)
                {
                    int ibtId = _supplierService.GetAllWidgetIBTCity(" AND bt.ObjId=" + item.CityId.ToString(), page, pageSize, out indexTotal).FirstOrDefault().ibtId;
                    imgshow = "/App/images/icon/lower.gif";
                    alt = "Lower";
                    click = "WidgetCityManage.DelWidgetCity(" + ibtId + ");"; //删除
                }
                else
                {
                    int score = _supplierService.GetAllWidgetIBTCity("", page, pageSize, out indexTotal).Count + 1;
                    imgshow = "/App/images/icon/add.gif";
                    alt = "ADD";
                    click = "WidgetCityManage.AddWidgetCity(" + item.CityId + "," + score + ");"; //添加
                }

                sb.Append("      <a href=\"javascript:void(0);\" onclick=\"" + click + "\"><img src=\"" + imgshow + "\" alt=\"" + alt + "\" /></a>");
                sb.Append("   </div>");
                sb.Append("    <a href=\"#\" class=\"img\"><img src=\"" + item.ImgSrc_City + "\" alt=\"" + item.CityName + "\" width=\"50\" height=\"50\" /></a>");
                sb.Append("    <dl>");
                sb.Append("      <dt>");
                //string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                sb.Append("        <span class=\"title\"> <a href=\"#\" >" + item.CityName + "</a>  " + item.CountryName + "</span>");
                sb.Append("      </dt>");
                sb.Append("    </dl>");
                sb.Append(" </li>");
            }
            return sb.ToString();
        }

        #endregion

        #region Edit
        /// <summary>
        /// Sort City
        /// </summary>
        /// <returns></returns>
        public JsonResult PaiXuCity()
        {//将最新数据保存到数据库
            string arrList = RequestHelper.GetVal("arrayData");

            List<string> SortList = new List<string>() { arrList };
            List<int> ObjIds = new List<int>() { };
            string[] arr = arrList.Split(',');//拆分后的字符数组  
            int objId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    SortList.Remove(arr[i]);//存在则从SortList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out objId);
                    ObjIds.Add(objId);
                }
            }
            var bmi = _supplierService.SetWidgetCitySort(ObjIds);
            if (bmi.Success)
            {

            }
            return Json(bmi);
        }
        public JsonResult AddWidgetCityAjax()
        {
            int cityId = RequestHelper.GetValInt("objId");
            int scroe = RequestHelper.GetValInt("scroe");
            var bmi = _supplierService.AddWidgetCity(UserId, cityId, scroe);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }

        #endregion

        #endregion

        #region Ads and Promotions Management

        #region Load Widget Media
        [HttpGet]
        public ActionResult SpecialAdsManage()
        {
            this.Title = "Ads and Promotions Management";
            this.PageTitle = "Ads and Promotions Management";
            return View();
        }

        public JsonResult GetWidgetListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            string strWhere = "";
            var list = WidgetListData(strWhere, page, pageSize, out total, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        public string WidgetListData(string strWhere, int page, int pageSize, out int total, out int itemTotal)
        {
            var List = _supplierService.GetAllWidgetAdsInfo(strWhere, page, pageSize, out total);
            itemTotal = List.Count;
            StringBuilder sbHtml = new StringBuilder();
            //if (page < 2)
            //{
            //    sbHtml.Append("<tr>");
            //    sbHtml.Append("  <th width=\"3%\">&nbsp;</th>");
            //    sbHtml.Append("  <th width=\"10%\">Name</th>");
            //    sbHtml.Append("  <th width=\"10%\">StartDate</th>");
            //    sbHtml.Append("  <th width=\"10%\">EndDate</th>");
            //    sbHtml.Append("  <th width=\"16%\">Target URL</th>");
            //    sbHtml.Append("  <th width=\"8%\">MediaType</th>");
            //    sbHtml.Append("  <th width=\"7%\">Position</th>");
            //    sbHtml.Append("  <th width=\"10%\">&nbsp;</th>");
            //    sbHtml.Append("</tr>");
            //}
            string startDate = "";
            string endDate = "";
            string MediaType = "";
            string Position = "";
            string SpecType = "";
            string TitleUrl = "";
            foreach (var item in List)
            {
                sbHtml.Append("<tr>");
                sbHtml.Append("  <td>" + item.RowId.ToString() + ".</td>");
                if (item.IbtType == (int)Identifier.IBTType.Country)
                {
                    TitleUrl = "/Country/Specials/1/" + item.CountryId.ToString();
                }
                else if (item.IbtType == (int)Identifier.IBTType.City)
                {
                    TitleUrl = "/City/Specials/2/" + item.CityId.ToString();
                }
                else if (item.IbtType == (int)Identifier.IBTType.Supply)
                {
                    TitleUrl = "/Supplier/v/" + item.SupplyId.ToString() + "?p=specials";
                }
                else
                {
                    TitleUrl = "javascript:void(0);";
                }
                sbHtml.Append("  <td><a href=\"" + (item.Position == (int)Identifier.PositionTypes.Promotions ? TitleUrl : "#") + "\">" + item.Title + "</a></td>");
                startDate = DateTime.Parse(item.StartDate.ToString()).ToString("d MMMM yyyy");
                endDate = DateTime.Parse(item.ExpiryDate.ToString()).ToString("d MMMM yyyy");
                sbHtml.Append("  <td>" + startDate + "</td>");
                sbHtml.Append("  <td>" + endDate + "</td>");

                if (item.MediaType == (int)Identifier.MediaTypes.Video)
                {
                    MediaType = "Video";
                }
                else if (item.MediaType == (int)Identifier.MediaTypes.picture)
                {
                    MediaType = "Picture";
                }
                else
                {
                    MediaType = "HtmlCode";
                }

                #region 链接
                //HtmlCode显示“-”
                //如果点击isURL和有target url, ads链接到 target url
                //如果没有点击isURL，有target url, 有supplier selection, ads链接到supplier listing
                //如果没有点击isURL，有target url, 没有supplier selection， ads不会有链接。
                #endregion

                if (item.MediaType == (int)Identifier.MediaTypes.HtmlCode)
                {
                    sbHtml.Append("  <td><a href=\"#\">-</a></td>");
                }
                else if (item.IsLink == true)
                {
                    if (string.IsNullOrEmpty(item.TargetUrl))
                    {
                        sbHtml.Append("  <td><a href=\"#\">-</a></td>");
                    }
                    else
                    {
                        sbHtml.Append("  <td><a href=\"" + item.TargetUrl + "\" target=\"_blank\">" + item.TargetUrl + "</a></td>");
                    }
                }
                else
                {
                    if (item.SupplyId > 0)
                    {
                        string supplierURL = "http://" + Request.Url.Authority;
                        if (item.Position == (int)Identifier.PositionTypes.Advertisement)
                            supplierURL = supplierURL + "/Supplier/v/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.SupplyName);
                        else
                            supplierURL = supplierURL + "/Supplier/s/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.SupplyName);
                        sbHtml.Append("  <td><a href=\"" + supplierURL + "\" target=\"_blank\">" + supplierURL + "</a></td>");
                    }
                    else
                    {
                        if (item.IbtType == (int)Identifier.IBTType.Country || item.IbtType == (int)Identifier.IBTType.City)
                        {
                            string itemUrl = "http://" + Request.Url.Authority + TitleUrl;
                            sbHtml.Append("  <td><a href=\"" + itemUrl + "\">" + itemUrl + "</a></td>");
                        }
                        else
                            sbHtml.Append("  <td><a href=\"#\">-</a></td>");
                    }
                }

                sbHtml.Append("  <td>" + MediaType + "</td>");
                Position = item.Position == (int)Identifier.PositionTypes.Promotions ? "Specials" : "Advertisement";
                sbHtml.Append("  <td>" + Position + "</td>");
                if (item.IbtType == (int)Identifier.IBTType.Country)
                {
                    SpecType = "Country";
                }
                else if (item.IbtType == (int)Identifier.IBTType.City)
                {
                    SpecType = "City";
                }
                else if (item.IbtType == (int)Identifier.IBTType.Supply)
                {
                    SpecType = "Supplier";
                }
                else
                {
                    SpecType = "None";
                }
                sbHtml.Append("  <td>" + SpecType + "</td>");
                sbHtml.Append("  <td class=\"alignR\">");
                sbHtml.Append("    <div class=\"controlBg control2\">");
                sbHtml.Append("      <span class=\"bg\">");
                sbHtml.Append("      <a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/AddNewMedia?mediaId=" + item.MediaId.ToString() + "','Get','',function(){initAllData();WidgetManage.uploadImg();selectSpan();scroll_init(scroll_blue);countCharacters(); },'Edit Media');\"  class=\"edit NborderL\"></a>");
                //sbHtml.Append("      <a href=\"javascript:void(0);\"  class=\"admin\"></a>");
                sbHtml.Append("      <a href=\"javascript:void(0);\" onclick=\"WidgetManage.DelWidgetMedia(" + item.MediaId + ");\" class=\"delete NborderR\"></a>");
                sbHtml.Append("      </span>");
                sbHtml.Append("    </div>");
                sbHtml.Append("  </td>");
                sbHtml.Append("</tr>");
            }
            return sbHtml.ToString();
        }
        #endregion

        #region Popup_AdsInfo
        /// <summary>
        /// Add New Banner Ads
        /// </summary>
        /// <returns></returns>
        public ActionResult AddNewMedia(int mediaId)
        {
            if (mediaId != 0)
            {
                ViewBag.mediaId = mediaId.ToString();
                ViewBag.MediaInfo = _supplierService.GetWidgetMediaInfo(mediaId);
                IMeet.IBT.DAL.Media media = ViewBag.MediaInfo as IMeet.IBT.DAL.Media;
                ViewBag.supplyId = media.SupplyId;
                ViewBag.countryId = media.CountryId;
                ViewBag.cityId = media.CityId;
                ViewBag.ibtType = media.IBTType;
                ViewBag.mediaTab = media.Position;
                ViewBag.mediaType = media.MediaType.ToString();
                ViewBag.IsLink = media.IsLink.ToString();
                ViewBag.MediaTitle = media.Title;
                ViewBag.Desp = media.Desp;
                ViewBag.StartDate = media.StartDate;
                ViewBag.ExpiryDate = media.ExpiryDate;
                ViewBag.Code = media.Code;
                ViewBag.FileSrc = media.FileSrc;
                ViewBag.TargetUrl = media.TargetUrl;
            }
            else
            {
                ViewBag.mediaId = mediaId.ToString();
                ViewBag.mediaTab = ((int)Identifier.PositionTypes.Advertisement).ToString();
                ViewBag.mediaType = ((int)Identifier.MediaTypes.picture).ToString();
            }
            return View();
        }

        /// <summary>
        /// Supplier data
        /// </summary>
        /// <returns></returns>
        public JsonResult SelectSupplier()
        {

            string countryId = RequestHelper.GetVal("countryId");
            string cityId = RequestHelper.GetVal("cityId");
            string cateId = RequestHelper.GetVal("cateId");

            string strCountryCondition = string.IsNullOrEmpty(countryId == "-1" ? "" : countryId) ? "" : " AND sp.CountryId=" + countryId;
            string strCityCondition = string.IsNullOrEmpty(cityId == "-1" ? "" : cityId) ? "" : " AND sp.CityId=" + cityId;
            string strCateCondition = string.IsNullOrEmpty(cateId == "-1" ? "" : cateId) ? "" : " AND EXISTS( SELECT 1 FROM SupplyAttributeData  WHERE sp.SupplyId = SupplyId AND ArrtDataId IN (" + cateId + "))";
            string strWhere = strCountryCondition + strCityCondition + strCateCondition;
            var list = SupplierItemList(strWhere);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
            //return Json(_supplierService.GetSupplyItemDetail(strWhere).ToList(), JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// 返回SupplierItemList的数据
        /// </summary>
        /// <returns></returns>
        private string SupplierItemList(string strWhere)
        {
            var list = _supplierService.GetSupplyItemDetail(strWhere);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<option value=\"-1\">Select one Supplier</option>");
            foreach (var item in list)
            {
                sb.Append("<option value=\"" + item.SupplyId + "\">" + item.Title + "</option>");

                //sb.Append(" <li onclick=\"getSupplyId(" + item.SupplyId.ToString() + ")\">" + item.Title + "</li>");
            }
            return sb.ToString();
        }
        #endregion

        #region Add/Edit Media
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadMediaImg(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    HttpPostedFile hfile = System.Web.HttpContext.Current.Request.Files[0];
                    int tab = RequestHelper.GetValInt("mediaTab");
                    int type = RequestHelper.GetValInt("mediaType");
                    int supplyId = RequestHelper.GetValInt("supplyId");
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/temp/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    string savePath = Server.MapPath("~/Staticfile/Media/");
                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }
                    string saveName = ""; // 保存文件名称
                    string typeImgName = DateTime.Now.ToString("yyyyMMddmmss").ToString();
                    if (tab == 1)
                    {
                        saveName = "Special_Offers_" + typeImgName.ToString();
                        //saveName = "Special_Offers_" + supplyId.ToString() + "_" + tab.ToString() + "_" + type.ToString();
                    }
                    else
                    {
                        saveName = "Ads_" + typeImgName.ToString();
                        //saveName = "Ads_" + supplyId.ToString() + "_" + tab.ToString() + "_" + type.ToString();
                    }
                    //resize
                    //Video (116X85)  Ads (205X169) Special Offer (304X198) 默认(110X80) 列表（86*86）
                    //生成小图
                    string[] strsSavePath = new string[] { savePath + saveName + "_ads.jpg", savePath + saveName + "_vd.jpg", savePath + saveName + "_view.jpg" };
                    int[] intMaxLength = new int[] { (int)Identifier.MediaPhotoSize._ads, (int)Identifier.MediaPhotoSize._vd, (int)Identifier.MediaPhotoSize._def };
                    bool blSucResize = false;
                    try
                    {
                        //生成原图
                        hfile.SaveAs(savePath + saveName + "_ori.jpg");
                        blSucResize = IMeet.IBT.Common.Utilities.ThumbnailImage.ResizeFromPostedFile(hfile, strsSavePath, intMaxLength);
                        //Cut
                        IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(savePath + saveName + "_ori.jpg", savePath + saveName + "_big.jpg", (int)Identifier.SuppilyPostVisitPhotoSize._big, (int)Identifier.SuppilyPostVisitPhotoSize._big, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                        IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(savePath + saveName + "_ori.jpg", savePath + saveName + "_sw.jpg", (int)Identifier.MediaPhotoSize._sw, (int)Identifier.MediaPhotoSize._sw, Common.Utilities.MakeThumbImage.ThumbPhotoModel.Cut);
                        IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(savePath + saveName + "_ori.jpg", savePath + saveName + "_spec.jpg", (int)Identifier.MediaPhotoSize._spec, (int)Identifier.MediaPhotoSize._spec, Common.Utilities.MakeThumbImage.ThumbPhotoModel.Cut);

                    }
                    catch (Exception ex)
                    {
                        return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                    fileData.SaveAs(filePath + fileName);
                    return Json(new { Success = true, FileName = saveName, SaveName = saveName + "_view.jpg" });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose files to upload !" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddWidgetMediaAjax(string returnUrl)
        {
            int ibtType = RequestHelper.GetValInt("ibtType");
            int inputType = RequestHelper.GetValInt("inputType");
            int position = RequestHelper.GetValInt("position");
            int mediaType = RequestHelper.GetValInt("mediaType");
            string title = RequestHelper.GetVal("title");
            string desc = RequestHelper.GetVal("desc");
            int supplyId = RequestHelper.GetValInt("SupplyId");
            int countryId = RequestHelper.GetValInt("CountryId");
            int cityId = RequestHelper.GetValInt("CityId");
            string startDate = RequestHelper.GetVal("startDate");
            string endTime = RequestHelper.GetVal("endTime");
            string isUrl = RequestHelper.GetVal("isLink");
            string imgUrl = RequestHelper.GetUrlDecodeVal("imgUrl");
            string txtCode = RequestHelper.GetVal("txtCode");
            string htmlCode2 = RequestHelper.GetVal("htmlCode2");
            string targetUrl = RequestHelper.GetVal("targetUrl");

            bool isLink = false;
            if (isUrl == "True")
            {
                isLink = true;
            }
            string code = txtCode;
            if (inputType == 1) //HtmlCode
            {
                code = htmlCode2;
                imgUrl = "";
                targetUrl = "";
            }
            var bmi = _supplierService.AddWidgetMedia(ibtType, supplyId, countryId, cityId, position, mediaType, title, desc, startDate, endTime, imgUrl, code, isLink, targetUrl);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult EditWidgetMediaAjax(string returnUrl)
        {
            int ibtType = RequestHelper.GetValInt("ibtType");
            int inputType = RequestHelper.GetValInt("inputType");
            int mediaId = RequestHelper.GetValInt("MediaId");
            int position = RequestHelper.GetValInt("position");
            int mediaType = RequestHelper.GetValInt("mediaType");
            string title = RequestHelper.GetVal("title");
            string desc = RequestHelper.GetVal("desc");
            int supplyId = RequestHelper.GetValInt("SupplyId");
            int countryId = RequestHelper.GetValInt("CountryId");
            int cityId = RequestHelper.GetValInt("CityId");
            string startDate = RequestHelper.GetVal("startDate");
            string endTime = RequestHelper.GetVal("endTime");
            string isUrl = RequestHelper.GetVal("isLink");
            string imgUrl = RequestHelper.GetUrlDecodeVal("imgUrl");
            string txtCode = RequestHelper.GetVal("txtCode");
            string htmlCode2 = RequestHelper.GetVal("htmlCode2");
            string targetUrl = RequestHelper.GetVal("targetUrl");
            if (ibtType == (int)Identifier.IBTType.Country || ibtType == (int)Identifier.IBTType.City)
            {
                supplyId = 0;
            }

            bool isLink = false;
            if (isUrl == "True")
            {
                isLink = true;
            }
            string code = txtCode;
            if (inputType == 1)
            {
                code = htmlCode2;
                imgUrl = "";
                targetUrl = "";
            }
            var bmi = _supplierService.EditWidgetMedia(mediaId, ibtType, supplyId, countryId, cityId, position, mediaType, title, desc, startDate, endTime, imgUrl, code, isLink, targetUrl);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }
        #endregion

        #region delete Media

        public JsonResult DelWidgetMedia()
        {
            int objId = RequestHelper.GetValInt("objId");
            var bmi = _supplierService.DeleteWidgetAds(objId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        #endregion

        #endregion

        #region Supplier Batch Import
        /// <summary>
        /// Supplier Batch Import
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SupplierBatchImport()
        {
            this.Title = "Supplier Batch Import";
            this.PageTitle = "Supplier Batch Import";
            return View();
        }
        //[HttpPost]
        //public ActionResult SupplierBatchImport(HttpPostedFileBase filebase)
        //{
        //    HttpPostedFileBase file = Request.Files["files"];
        //    string FileName;
        //    string savePath;
        //    if (file == null || file.ContentLength <= 0)
        //    {
        //        ViewBag.error = "文件不能为空";
        //        return View();
        //    }
        //    else
        //    {
        //        string filename = Path.GetFileName(file.FileName);
        //        int filesize = file.ContentLength;//获取上传文件的大小单位为字节byte
        //        string fileEx = System.IO.Path.GetExtension(filename);//获取上传文件的扩展名
        //        string NoFileName = System.IO.Path.GetFileNameWithoutExtension(filename);//获取无扩展名的文件名
        //        int Maxsize = 4000 * 1024;//定义上传文件的最大空间大小为4M
        //        string FileType = ".xls,.xlsx";//定义上传文件的类型字符串

        //        FileName = NoFileName + DateTime.Now.ToString("yyyyMMddhhmmss") + fileEx;
        //        if (!FileType.Contains(fileEx))
        //        {
        //            ViewBag.error = "文件类型不对，只能导入xls和xlsx格式的文件";
        //            return View();
        //        }
        //        if (filesize >= Maxsize)
        //        {
        //            ViewBag.error = "上传文件超过4M，不能上传";
        //            return View();
        //        }
        //        string path = AppDomain.CurrentDomain.BaseDirectory + "uploads/excel/";
        //        savePath = Path.Combine(path, FileName);
        //        file.SaveAs(savePath);
        //    }

        //    //string result = string.Empty;
        //    string strConn;
        //    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + savePath + ";" + "Extended Properties=Excel 8.0";
        //    OleDbConnection conn = new OleDbConnection(strConn);
        //    conn.Open();
        //    OleDbDataAdapter myCommand = new OleDbDataAdapter("select * from [Sheet1$]", strConn);
        //    DataSet myDataSet = new DataSet();
        //    try
        //    {
        //        myCommand.Fill(myDataSet, "ExcelInfo");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View();
        //    }
        //    DataTable table = myDataSet.Tables["ExcelInfo"].DefaultView.ToTable();

        //    //引用事务机制，出错时，事务回滚
        //    using (TransactionScope transaction = new TransactionScope())
        //    {
        //        for (int i = 0; i < table.Rows.Count; i++)
        //        {
        //            //获取地区名称
        //            string _areaName = table.Rows[i][0].ToString();
        //            //判断地区是否存在
        //            if (!_areaRepository.CheckAreaExist(_areaName))
        //            {
        //                ViewBag.error = "导入的文件中：" + _areaName + "地区不存在，请先添加该地区";
        //                return View();
        //            }
        //            else
        //            {
        //                Station station = new Station();
        //                station.AreaID = _areaRepository.GetIdByAreaName(_areaName).AreaID;
        //                station.StationName = table.Rows[i][1].ToString();
        //                station.TerminaAddress = table.Rows[i][2].ToString();
        //                station.CapacityGrade = table.Rows[i][3].ToString();
        //                station.OilEngineCapacity = decimal.Parse(table.Rows[i][4].ToString());
        //                _stationRepository.AddStation(station);
        //            }
        //        }
        //        transaction.Complete();
        //    }
        //    ViewBag.error = "导入成功";
        //    System.Threading.Thread.Sleep(2000);
        //    return RedirectToAction("Index");
        //}

        public FileResult GetFile()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "App_Data/Template/";
            string fileName = "SP New_Template.xls";
            return File(path + fileName, "text/plain", fileName);
        }

        #endregion

        #region Member Management
        public ActionResult MemberManage()
        {
            this.Title = "Member Management";
            this.PageTitle = "Member Management";
            return View();
        }

        class activationList
        {
            public int UserId { get; set; }
            public String Firstname { get; set; }
            public String Lastname { get; set; }
            public String Email { get; set; }
            public DateTime? ActivateDate { get; set; }
            public DateTime? LastLoginDate { get; set; }
        }

        public FileResult GetActivatedUsersList()
        {
            //IMeet.IBT.DAL.Models.UserTravelBadge erer = new UserTravelBadge();
            //IBT.Services.AccountService aService;
            //var fdfd= aService._db.Users.ToList().ToList();
            //UserPermanentPoint esse =new UserPermanentPoint();
            // var erere=esse.UserId;
            //_travelBadgeService.GetUserTravelbadge().TotalPoints
            // return this.UserPermanentPoint.PermanentPointTotal + this.UserBonusPoint.BonusPointTotal + this.BadgeSpecialTotal;

            byte[] content = { 0 };
            StringBuilder sb = new StringBuilder();
            try
            {
                IBTEntities ibt = new IBTEntities();
                var activedUsersList = ibt.Database.SqlQuery<activationList>(@"SELECT [Users].[UserId], [Profiles].[Firstname],
                                                          [Profiles].[Lastname], [Users].[Email] AS [Email],
                                                          [Users].[ActivateDate], [Users].[LastLoginDate] FROM [Users], [Profiles]
                                                          WHERE [Users].[UserId] = [Profiles].[UserID] AND [IsActivate]=1;").ToList();
                //var activedUsersList = ibt.Users.Where(o => o.IsActivate.Equals(1)).ToList();
                sb.Append("User ID, First Name, Last Name, Points, Email, Activation Date, Date of Last Login, ,Total Users: " + activedUsersList.Count.ToString() + '\n');

                //for (int i = 0; i < activedUsersList.Count; i++)
                for (int i = 0; i < activedUsersList.Count; i++)
                {
                    sb.Append(activedUsersList[i].UserId.ToString() + ", ");
                    sb.Append(activedUsersList[i].Firstname.Replace(',', ';') + ", ");
                    sb.Append(activedUsersList[i].Lastname.Replace(',', ';') + ", ");
                    //uncomment if you need total point for each user
                    sb.Append(_travelBadgeService.GetUserTravelbadge(_profileService.GetUserInfo(activedUsersList[i].UserId)).TotalPoints.ToString() + ", ");
                    sb.Append(activedUsersList[i].Email + ", ");
                    sb.Append(activedUsersList[i].ActivateDate.ToString() + ", ");
                    sb.Append(activedUsersList[i].LastLoginDate.ToString());
                    sb.Append("\n");
                }
                content = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
            }
            catch (Exception ex)
            {
                content = System.Text.Encoding.UTF8.GetBytes("Something went wrong during making report, please call the maintenance team.\n \n" + ex.Message);
            }
            return File(content, "text/comma-separated-values", "Activated-Users-" + DateTime.Now.Date.ToShortDateString() + ".csv");
        }


        public ActionResult testMail()
        {

            //IMeet.IBT.Controllers.StructureMapBootrstrapper.Configure();
            // IAdminEmailService _adminEmailService = ObjectFactory.GetInstance<IAdminEmailService>();

            
            //AdminEmailService _adminEmailService = new AdminEmailService();
            //_adminEmailService.SendEmail();

            return Content("Done");

        }

        private string GetOrderValue()
        {
            string strOrderValue = "";
            switch (RequestHelper.GetVal("hidOrderTypeName"))
            {
                default:
                    strOrderValue = RequestHelper.GetVal("hidUserIdSort");
                    break;
                case "UserName":
                    strOrderValue = RequestHelper.GetVal("hidUserNameSort");
                    break;
                case "Birth":
                    strOrderValue = RequestHelper.GetVal("hidBirthSort");
                    break;
                case "Email":
                    strOrderValue = RequestHelper.GetVal("hidEmailSort");
                    break;
                case "MemberFeatured":
                    strOrderValue = RequestHelper.GetVal("hidMemberFeaturedSort");
                    break;
                case "Gender":
                    strOrderValue = RequestHelper.GetVal("hidGenderSort");
                    break;
                case "Location":
                    strOrderValue = RequestHelper.GetVal("hidLocationSort");
                    break;
                case "Method":
                    strOrderValue = RequestHelper.GetVal("hidMethodSort");
                    break;
                case "IsActive":
                    strOrderValue = RequestHelper.GetVal("hidOrderActive");
                    break;
                case "Password":
                    strOrderValue = RequestHelper.GetVal("hidPasswordSort");
                    break;
                case "SortNum":
                    strOrderValue = RequestHelper.GetVal("hidOrderSortNum");
                    break;
            }
            return strOrderValue;
        }
        /// <summary>
        /// Member data
        /// </summary>
        /// <returns></returns>
        public JsonResult MemberListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");

            string strOrderType = RequestHelper.GetVal("OrderBy");
            //string strOrderType = RequestHelper.GetVal("hidOrderTypeName");  //Todo:赋值给JS（hidOrderTypeName）
            Identifier.MemberSearchOrderBy emunOrderType;
            switch (strOrderType)
            {
                default:
                    emunOrderType = Identifier.MemberSearchOrderBy.SortNum;
                    break;
                case "FirstName":
                    emunOrderType = Identifier.MemberSearchOrderBy.FirstName;
                    break;
                case "UserId":
                    emunOrderType = Identifier.MemberSearchOrderBy.UserID;
                    break;
                case "Birth":
                    emunOrderType = Identifier.MemberSearchOrderBy.Birth;
                    break;
                case "Email":
                    emunOrderType = Identifier.MemberSearchOrderBy.Email;
                    break;
                case "MemberFeatured":
                    emunOrderType = Identifier.MemberSearchOrderBy.MemberFeatured;
                    break;
                case "Gender":
                    emunOrderType = Identifier.MemberSearchOrderBy.Gender;
                    break;
                case "Location":
                    emunOrderType = Identifier.MemberSearchOrderBy.Location;
                    break;
                case "Method":
                    emunOrderType = Identifier.MemberSearchOrderBy.Method;
                    break;
                case "IsActive":
                    emunOrderType = Identifier.MemberSearchOrderBy.Activate;
                    break;
                case "Password":
                    emunOrderType = Identifier.MemberSearchOrderBy.Password;
                    break;
            }
            Identifier.OrderBy emunOrderValue;
            //string OrderValue = GetOrderValue();
            string OrderValue = RequestHelper.GetVal("DescOrAsc");
            if (OrderValue == "" || OrderValue == "Desc")
            {
                emunOrderValue = Identifier.OrderBy.DESC;
            }
            else
            {
                emunOrderValue = Identifier.OrderBy.ASC;
            }

            //string strListType = "Recommended"; //Recommended,WanttoGo,BucketList
            //if (string.Equals(listType, "WanttoGo",StringComparison.OrdinalIgnoreCase))
            //    strListType = "WanttoGo";
            //else if (string.Equals(listType, "BucketList", StringComparison.OrdinalIgnoreCase))
            //    strListType = "BucketList";

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            int OrderBy = (int)emunOrderType;  //排序
            string DescOrAsc = emunOrderValue.ToString();
            //IBT_IMeetGetAllMemberList 0,'DESC','',-1,1,10,0,1,-1
            var list = MemberListData(OrderBy, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回MemberList的数据
        /// </summary>
        /// <param name="OrderType">UserId</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string MemberListData(int OrderBy, string DescOrAsc, int page, int pageSize, string SearchKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _profileService.GetMemberList(OrderBy, UserId, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string birth = "";
            string addm_attemail = "";
            string gender = "";
            string addr_city = "";
            string type = "";
            string source = "";
            foreach (var item in list)
            {
                sb.Append(" <tr>");
                sb.Append("     <td>" + item.row_number.ToString() + ".</td>");
                sb.Append("     <td><a target=\"_blank\" href=\"/Profile/" + item.UserId + "\">" + item.FirstName + " " + item.LastName + "</a></td>");
                birth = string.IsNullOrEmpty(item.Birth.ToString()) ? "" : DateTime.Parse(item.Birth.ToString()).ToString("d MMMM yyyy");
                sb.Append("     <td>" + birth + "</td>");  //DateTime.Parse(item.Birth.ToString()).ToString("d MMMM yyyy")
                addm_attemail = string.IsNullOrWhiteSpace(item.AlternateEmail) ? "" : "<br/>/<a href=\"mailto:" + item.AlternateEmail + "\">" + item.AlternateEmail + "</a>";
                sb.Append("     <td><a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>" + addm_attemail + "</td>");

                if (Convert.ToInt32(item.Gender) == 0)
                {
                    gender = "Male";
                }
                else if (Convert.ToInt32(item.Gender) == 1)
                {
                    gender = " Female";
                }
                else
                {
                    gender = " Unspecified";
                }
                sb.Append("     <td>" + gender + "</td>");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "Null," : item.CityName + ",";
                sb.Append("     <td>" + addr_city + item.CountryName + "</td>");
                sb.Append("     <td>" + item.CurrentLoginSnsId.ToString() + "</td>");
                sb.Append("     <td>" + (Convert.ToInt32(item.IsApproved) == 0 ? "NO" : "YES") + "</td>");
                sb.Append("     <td>" + (Convert.ToInt32(item.StatusId) == 0 ? "NO" : "YES") + "</td>");
                sb.Append("     <td>" + item.Password + "</td>");

                if (item.UserType == 1) //用户类型 !=0
                {
                    type = "Supplier";
                }
                else if (item.UserType == 2)
                {
                    type = "Planner";
                }
                else
                {
                    type = "Consumer";
                }
                sb.Append("     <td>" + type + "</td>");

                if (item.Source == "0") //用户类型
                {
                    source = "IBT";
                }
                else if (item.Source == "1")
                {
                    source = "Facebook";
                }
                else if (item.Source == "2")
                {
                    source = "Twitter";
                }
                else if (item.Source == "3")
                {
                    source = "LinkedIn";
                }
                else
                {
                    source = "IMeet";
                }
                sb.Append("     <td>" + source + "</td>");
                sb.Append("     <td class=\"cssTaskSn\"><a href=\"javascript:void(0);\" onclick=\"GetUserId(" + item.UserId + ");\">" + (item.Sort.ToString() == "" ? "0" : item.Sort.ToString()) + "</a></td>");
                sb.Append("     <td class=\" alignR\">");
                sb.Append("       <div class=\"controlBg control3\">");
                sb.Append("          <span class=\"bg\">");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"memberManager.SetIsApproved(" + item.UserId + ");\" class=\"activation " + (Convert.ToInt32(item.IsApproved) == 0 ? "" : "active") + "\"  ></a>");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"memberManager.SetIsFeatured(" + item.UserId + ");\" class=\"feature " + (Convert.ToInt32(item.StatusId) == 0 ? "" : "active") + "\" ></a>");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"memberManager.Del(" + item.UserId + ");\" class=\"delete " + (Convert.ToInt32(item.StatusId) != 3 ? "" : "active") + "\" ></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }

            return sb.ToString();
        }

        //设置 IsActivate
        public JsonResult SetIsApproved()
        {
            int userId = RequestHelper.GetValInt("objId");
            var bmi = _accountService.SetIsApproved(userId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        //设置 IsFeatured
        public JsonResult SetIsFeatured()
        {
            int userId = RequestHelper.GetValInt("objId");
            var bmi = _accountService.SetIsFeatured(userId);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        //设置 IsDel
        public JsonResult SetIsDel()
        {
            int sid = RequestHelper.GetValInt("objId");
            var bmi = _accountService.SetIsDel(sid);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }


        #endregion

        #region Supplier Type Management
        public ActionResult SupplierTypeManage()
        {
            this.Title = "Supplier Type Management";
            this.PageTitle = "Supplier Type Management";
            ViewBag.TypeTitle = "Add New Type";
            return View();

        }

        [HttpPost]
        public JsonResult AddNewSupplyType(string returnUrl)
        {
            string typeName = RequestHelper.GetUrlDecodeVal("typeName");
            string imgSrc = RequestHelper.GetVal("imgSave");
            string mapSrc = RequestHelper.GetVal("imgMapSave");
            string maptSrc = RequestHelper.GetVal("imgMap2Save");
            int attrDataId = RequestHelper.GetValInt("attrDataId");
            int attrCateId = RequestHelper.GetValInt("attrCateId");

            var bmi = _supplierService.AddNewType(attrDataId, attrCateId, typeName, imgSrc, mapSrc, maptSrc);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        [HttpPost]
        public JsonResult UpdateSupplyType(string returnUrl)
        {
            string typeName = RequestHelper.GetUrlDecodeVal("typeName");
            string imgSrc = RequestHelper.GetVal("imgSave");
            string mapSrc = RequestHelper.GetVal("imgMapSave");
            string maptSrc = RequestHelper.GetVal("imgMap2Save");
            int attrDataId = RequestHelper.GetValInt("attrDataId");
            int attrCateId = RequestHelper.GetValInt("attrCateId");

            var bmi = _supplierService.UpdateAttributeData(attrDataId, attrCateId, typeName, imgSrc, mapSrc, maptSrc);

            if (bmi.Success)
            {
            }
            return Json(bmi);
        }

        #region 图片上传（Icon、 Map）
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostIconUpload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    int userId = RequestHelper.GetValInt("UserId");
                    int attrDataId = RequestHelper.GetValInt("SupplyId");
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Supplier/Icon/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    string saveName = userId.ToString() + "_" + attrDataId.ToString() + "_attr" + fileExtension; // 保存文件名称
                    fileData.SaveAs(filePath + saveName);

                    //原图处理
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + "Cut_" + saveName, 110, 110, MakeThumbImage.ThumbPhotoModel.Cut);

                    string url = "";
                    bool isUpload = false;
                    HttpPostedFile hfile = System.Web.HttpContext.Current.Request.Files[0];
                    if (hfile != null && hfile.ContentLength > 0)
                    {
                        int dir = attrDataId / 500;
                        string staticPath = "/Staticfile/Supplier/Icon/" + dir.ToString() + "/" + userId.ToString() + "/" + DateTime.Now.ToString("yyyyMM") + "/";
                        string path = System.Web.HttpContext.Current.Server.MapPath("~" + staticPath);
                        if (!System.IO.Directory.Exists(path))
                            System.IO.Directory.CreateDirectory(path);

                        //string fileName = Request.Params["fileName"];
                        //if (string.IsNullOrEmpty(fileName))
                        string fileName2 = System.Guid.NewGuid().ToString().Substring(0, 9).Replace("-", "");

                        //生成小图
                        string[] strsSavePath = new string[] { path + fileName2 + "_big.jpg", path + fileName2 + "_bn.jpg", path + fileName2 + "_mn.jpg", path + fileName2 + "_tn.jpg", path + fileName2 + "_xtn.jpg" };
                        int[] intMaxLength = new int[] { (int)Identifier.SuppilyPhotoSize._big, (int)Identifier.SuppilyPhotoSize._bn, (int)Identifier.SuppilyPhotoSize._mn, (int)Identifier.SuppilyPhotoSize._tn, (int)Identifier.SuppilyPhotoSize._xtn };
                        bool blSucResize = false;
                        try
                        {
                            blSucResize = IMeet.IBT.Common.Utilities.ThumbnailImage.ResizeFromPostedFile(hfile, strsSavePath, intMaxLength);
                            Image image = Image.FromFile(path + fileName2 + "_100x100.jpg");
                            //context.Response.Write("{\"src\":\"" + Ran.Core.Common.Default.GetAvatarsPath(userId, fileName, AvatarSize._mn) + "?id=" + System.Guid.NewGuid().ToString().Substring(0, 9).Replace("-", "") + "\",\"w\":\"" + image.PhysicalDimension.Width.ToString() + "\",\"h\":\"" + image.PhysicalDimension.Height.ToString() + "\"}");
                            image.Dispose();
                            isUpload = true;
                            url = staticPath + fileName2 + "_55x55.jpg";
                        }
                        catch (Exception ex)
                        {
                            isUpload = false;
                            _logger.Error("用户上传图片！（Post Icon）", ex);
                        }
                    }
                    return Json(new { Success = true, FileName = fileName, SaveName = saveName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose to upload files!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostMapUpload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    int userId = RequestHelper.GetValInt("UserId");
                    int attrDataId = RequestHelper.GetValInt("AttrDataId");
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Supplier/Icon/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    string saveName = userId.ToString() + "_" + attrDataId.ToString() + "_map" + fileExtension; // 保存文件名称
                    fileData.SaveAs(filePath + saveName);
                    //原图处理
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + "Cut_" + saveName, 110, 110, MakeThumbImage.ThumbPhotoModel.Cut);
                    return Json(new { Success = true, FileName = fileName, SaveName = saveName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose to upload files!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostMap2Upload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    int userId = RequestHelper.GetValInt("UserId");
                    int attrDataId = RequestHelper.GetValInt("AttrDataId");
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Supplier/Icon/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名
                    string saveName = userId.ToString() + "_" + attrDataId.ToString() + "_map2" + fileExtension; // 保存文件名称
                    //原图处理
                    fileData.SaveAs(filePath + saveName);
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + "Cut_" + saveName, 110, 110, MakeThumbImage.ThumbPhotoModel.Cut);
                    return Json(new { Success = true, FileName = fileName, SaveName = saveName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose to upload files!" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public JsonResult SetSupplyTypeSort(string returnUrl)
        {

            string ArrayData = RequestHelper.GetVal("arrayData");
            List<string> SortList = new List<string>() { ArrayData };
            List<int> SupplyAttr = new List<int>() { };
            string[] arr = ArrayData.Split(',');//拆分后的字符数组  
            int attrId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    SortList.Remove(arr[i]);//存在则从ArrayList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out attrId);
                    SupplyAttr.Add(attrId);
                }
            }
            // SupplyAttr = new List<string>(ArrayData.Split(',')).ConvertAll(i => int.Parse(i));
            var bmi = _supplierService.SetTypeSort(SupplyAttr);

            return Json(bmi);
        }
        [HttpPost]
        public JsonResult EditSupplyType(string returnUrl)
        {
            int supplyId = RequestHelper.GetValInt("supplyId");
            string SelattrId = RequestHelper.GetVal("selattrId");
            List<string> TypeList = new List<string>() { SelattrId };
            List<int> AttrIdList = new List<int>() { };
            string[] arr = SelattrId.Split(',');//拆分后的字符数组  
            int attrId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    TypeList.Remove(arr[i]);//存在则从ArrayList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out attrId);
                    AttrIdList.Add(attrId);
                }
            }
            // SupplyAttr = new List<string>(ArrayData.Split(',')).ConvertAll(i => int.Parse(i));
            var bmi = _supplierService.EditType(supplyId, AttrIdList);

            return Json(bmi);
        }


        /// <summary>
        /// Supply Type Sort
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SupplyTypeSort()
        {
            return View();
        }

        /// <summary>
        /// Edit Supply Type 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditSupplyTypes()
        {
            return View();
        }
        /// <summary>
        /// Type data(AttrDataName)返回类型名
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSupplyTypeAjx()
        {
            int attrId = RequestHelper.GetValInt("attrId");
            string attrDataName = _supplierService.GetAttrInfo(attrId).AttrDataName;
            return Json(new BoolMessageItem<string>("", true, attrDataName), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Supplier Detail(SupplyName)返回Supplier Title
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSupplyName()
        {
            int supplyId = RequestHelper.GetValInt("supplyId");
            string supplyName = _supplierService.GetSupplyDetail(supplyId).Title;
            return Json(new BoolMessageItem<string>("", true, supplyName), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Sort data
        /// </summary>
        /// <returns></returns>
        public JsonResult SupplySortListAjx()
        {
            int itemTotal = 0;
            //string popType = RequestHelper.GetVal("popType");
            int supplyId = RequestHelper.GetValInt("objId");

            //string strPopType = "Order"; //Recommended,WanttoGo,BucketList
            //if (string.Equals(popType, "AttrDataName", StringComparison.OrdinalIgnoreCase))
            //    strPopType = "AttrDataName";

            var list = SupplySortListData(supplyId, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SupplySortListData的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string SupplySortListData(int supplyId, out int itemTotal)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            var list = _supplierService.GetAllSupplyTypeSort(supplyId);
            itemTotal = list.Count();
            sb.Append("  <tr id=\"0\">");
            sb.Append("     <th width=\"80%\">Type</th>");
            sb.Append("     <th width=\"16%\">Sort</th>");
            sb.Append(" </tr>");
            foreach (var item in list)
            {
                sb.Append(" <tr id=\"" + item.SupplyAttr + "\">");
                sb.Append("       <td>" + item.AttrDataName + "</td>");
                sb.Append("       <td>" + item.Score + "</td>");
                sb.Append("</tr>");

            }
            return sb.ToString();
        }
        /// <summary>
        /// Type data
        /// </summary>
        /// <returns></returns>
        public JsonResult SupplyTypeListAjx()
        {
            int itemTotal = 0;
            int supplyId = RequestHelper.GetValInt("objId");
            int typeId = RequestHelper.GetValInt("typeId");
            var list = SupplyTypeListData(typeId, supplyId, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SupplySortListData的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string SupplyTypeListData(int typeId, int supplyId, out int itemTotal)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            var list = _supplierService.GetAttributeDataList(0);
            itemTotal = list.Count();
            if (typeId == 1)
            {
                foreach (var item in list)
                {
                    sb.Append("  <li class=\"formLi\">");
                    sb.Append("     <h3>" + item.AttrDataName + "</h3>");
                    sb.Append("     <ul>");
                    var list2 = _supplierService.GetAttributeDataList(item.AttrDataId);
                    foreach (var item2 in list2)
                    {
                        sb.Append("       <li><a href=\"javascript:void(0);\" onclick=\"supplyTypeManager.editType(" + item2.AttrDataId + "," + item.AttrDataId + ");\" >" + item2.AttrDataName + "</a></li>");
                    }
                    sb.Append("     </ul>");
                    sb.Append("  </li>");
                }
            }
            else
            {
                string uncheckbox = "";
                foreach (var item in list)
                {
                    sb.Append("  <li class=\"formLi\">");
                    sb.Append("     <h3>" + item.AttrDataName + "</h3>");
                    sb.Append("     <ul>");
                    var list2 = _supplierService.GetAttributeDataList(item.AttrDataId);
                    foreach (var item2 in list2)
                    {

                        var list3 = _supplierService.GetAllSupplyTypeSort(supplyId);

                        foreach (var item3 in list3)
                        {
                            //选择的标记出来
                            if (item3.AttrDataName != null && item2.AttrDataName.Contains(item3.AttrDataName.ToString()))
                            {
                                uncheckbox = " uncheckbox";
                            }
                        }
                        sb.Append("       <li><span data-attrId=\"" + item2.AttrDataId + "\" class=\"checkboxMedium " + uncheckbox + "\"></span> <label>" + item2.AttrDataName + "</label></li>");

                    }
                    sb.Append("     </ul>");
                    sb.Append("  </li>");
                }
            }
            return sb.ToString();
        }

        public JsonResult GetAttrData(int id)
        {
            return Json(_supplierService.GetAttributeDataList(id).ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAttrDataImg(int id)
        {
            return Json(_supplierService.GetAttributeDataImg(id).ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Supplier Management
        public ActionResult SupplierManage()
        {
            this.Title = "Supplier Management";
            this.PageTitle = "Supplier Management";
            return View();

        }
        public ActionResult EditSupplierManage(int sid = 0)
        {
            this.Title = "Supplier Management";
            this.PageTitle = "Supplier Management";
            //读取
            Supply supply = _supplierService.GetSupplyDetail(sid);
            IList<SupplyAttributeData> dataList = null;
            IList<SuppliesAttachment> attaList_logo = null;
            IList<SuppliesAttachment> attaList_images = null;
            IList<SuppliesAttachmentBind> attaBindList = null;
            if (supply.SupplyId == 0)
            {
                this.PageTitle2 = "New Supplier";
            }
            else
            {
                this.PageTitle2 = "Edit Supplier";
                dataList = _supplierService.GetSupplyAttributeData(supply.SupplyId);
                attaBindList = _supplierService.GetSuppliesAttachmentBind(supply.SupplyId);
                string[] guids_logo = (from a in attaBindList where a.Type == 1 select a.AttGuidId).ToArray();
                string[] guids_images = (from a in attaBindList where a.Type == 0 select a.AttGuidId).ToArray();
                attaList_logo = _supplierService.GetSuppliesAttachment(guids_logo);
                attaList_images = _supplierService.GetSuppliesAttachment(guids_images);
            }

            Supply parent_supply = _supplierService.GetSupplyDetail(supply.ParentSupplyId ?? 0);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("supply", supply);
            dic.Add("parent_supply", parent_supply);
            dic.Add("dataList", dataList);
            dic.Add("attaList_logo", attaList_logo);
            dic.Add("attaList_images", attaList_images);
            return View(dic);
        }
        /// <summary>
        /// Supplier data
        /// </summary>
        /// <returns></returns>
        public JsonResult SupplierListAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            //string strOrderType = RequestHelper.GetVal("hidOrderTypeName");  //Todo:赋值给JS（hidOrderTypeName）

            Identifier.SupplySearchOrderBy emunOrderType;
            string strOrderType = RequestHelper.GetVal("OrderBy");
            switch (strOrderType)
            {
                default:
                    emunOrderType = Identifier.SupplySearchOrderBy.Score;
                    break;
                case "Title":
                    emunOrderType = Identifier.SupplySearchOrderBy.Title;
                    break;
            }

            Identifier.OrderBy emunOrderValue;
            //string OrderValue = GetOrderValue();
            string OrderValue = RequestHelper.GetVal("DescOrAsc");
            if (OrderValue == "" || OrderValue == "Desc")
            {
                emunOrderValue = Identifier.OrderBy.DESC;
            }
            else
            {
                emunOrderValue = Identifier.OrderBy.ASC;
            }

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            int OrderBy = (int)emunOrderType;  //排序
            var list = SupplierListData(OrderBy, emunOrderValue.ToString(), page, pageSize, SearchKeyword, filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Type3OfSupplier()
        {
            return View();
        }
        public JsonResult SponsorSupplieDataAjx()
        {
            int pageType = RequestHelper.GetValInt("pageType");//1:Landing Management 2:Recommend Management
            int type = RequestHelper.GetValInt("type", 1);
            int tab = RequestHelper.GetValInt("tab");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            int sortby = RequestHelper.GetValInt("sortBy");
            string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            string countryId = RequestHelper.GetVal("countryId");
            string cityId = RequestHelper.GetVal("cityId");
            string cateIds = RequestHelper.GetVal("cateIds");
            if (SearchKeyword == "Type in something to search")
                SearchKeyword = "";
            //判断拼接条件字符串
            string strWhere = " AND sp.SupplierTypes = 3";
            string strKeywordCondition = string.IsNullOrEmpty(SearchKeyword) ? "" : " AND (sp.Title LIKE '%" + SearchKeyword.ToString() + @"%' OR sp.Desp LIKE '%" + SearchKeyword.ToString() + @"%')";
            string strCountryCondition = string.IsNullOrEmpty(countryId) ? "" : " AND sp.CountryId=" + countryId;
            string strCityCondition = string.IsNullOrEmpty(cityId) ? "" : " AND sp.CityId=" + cityId;
            string strCateCondition = string.IsNullOrEmpty(cateIds) ? "" : " AND EXISTS( SELECT 1 FROM SupplyAttributeData  WHERE sp.SupplyId = SupplyId AND ArrtDataId IN (" + cateIds + "))";
            string strRecommend = "";
            if (tab == 1) //Recommended
            {
                strRecommend = " AND sp.SupplyId IN (SELECT ObjId FROM RRWB WHERE StatusId=1 AND IBTType=3 AND Recommended=1 Group BY ObjId) ";
            }
            else if (tab == 2) // Not Recommended
            {
                strRecommend = " AND sp.SupplyId IN (SELECT ObjId FROM RRWB WHERE StatusId=1 AND IBTType=3 AND Recommended=0 Group BY ObjId) ";
            }
            else
            {
                strRecommend = "";
            }
            string strSortBy = "";
            if (sortby == 0)
            {
                strSortBy = " ORDER BY  Score DESC";
            }
            else if (sortby == 1)
            {
                strSortBy = " ORDER BY  Recommended DESC";
            }
            else
            {
                strSortBy = " ORDER BY  WanttoGo DESC";
            }
            strWhere = strKeywordCondition + strCountryCondition + strCityCondition + strCateCondition + strRecommend;

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = SponsorSupplieListData_Choose(pageType, type, tab, page, pageSize, strWhere, strSortBy, out total, out itemTotal);

            //int page = RequestHelper.GetValInt("page", 1);
            //int pageSize = RequestHelper.GetValInt("pageSize", 10);
            //string SearchKeyword = RequestHelper.GetUrlDecodeVal("SearchKeyword");
            //string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            //if (SearchKeyword == "Type in something to search")
            //    SearchKeyword = "";
            //pageSize = pageSize > 50 ? 10 : pageSize;
            //pageSize = pageSize < 1 ? 10 : pageSize;

            //int total = 0;
            //int itemTotal = 0;
            //var list = SponsorSupplieListData(page, pageSize, SearchKeyword, out total, out itemTotal, " and sp.SupplierTypes = 3 ");

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// edit supplier management编辑页面提交的action
        /// </summary>
        /// <returns></returns>
        public JsonResult SubmitEditDataAjx()
        {
            int supplyId = RequestHelper.GetValInt("supplyId", 0);
            /*A&W Restaurants*/
            string supplierName = RequestHelper.GetUrlDecodeVal("supplierName");
            int typeOfSupplier = RequestHelper.GetValInt("typeOfSupplier", 1);
            int parentSupplierId = RequestHelper.GetValInt("parentSupplierId", 0);
            /*Location*/
            int country = RequestHelper.GetValInt("country", 1);
            int city = RequestHelper.GetValInt("city", 1);
            string stateOrProvince = RequestHelper.GetUrlDecodeVal("stateOrProvince");
            string address = RequestHelper.GetUrlDecodeVal("address");
            string zipcode = RequestHelper.GetUrlDecodeVal("zipcode");
            decimal longitude = RequestHelper.GetValDecimal("coordsLongitude", 0);
            decimal latitude = RequestHelper.GetValDecimal("coordsLatitude", 0);
            /*info*/
            string description = RequestHelper.GetUrlDecodeVal("description");
            string emailAddress = RequestHelper.GetUrlDecodeVal("emailAddress");
            string phone = RequestHelper.GetUrlDecodeVal("phone");
            string fax = RequestHelper.GetUrlDecodeVal("fax");
            string webSite = RequestHelper.GetUrlDecodeVal("webSite");
            /*categories*/
            string categoriesIds = RequestHelper.GetUrlDecodeVal("categoriesIds");
            string[] cids = categoriesIds.Split(',');
            /*logo*/
            string logoImage = RequestHelper.GetUrlDecodeVal("logoImage");      //logo的guid
            string logoX1 = RequestHelper.GetUrlDecodeVal("logoX1");
            string logoX2 = RequestHelper.GetUrlDecodeVal("logoX2");
            string logoY1 = RequestHelper.GetUrlDecodeVal("logoY1");
            string logoY2 = RequestHelper.GetUrlDecodeVal("logoY2");
            /*images*/
            string images = RequestHelper.GetUrlDecodeVal("images");            //图片集合的guid

            #region 创建supply
            Supply supply = _supplierService.GetSupplyDetail(supplyId);
            if (supplyId == 0)
            {
                supply.Score = 0;
                supply.Rating = 0;
                supply.RecommendedCount = 0;
                supply.WanttoGoCount = 0;
                supply.BucketListCount = 0;
                supply.CommentsCount = 0;
                supply.StatusId = 1;
                supply.CreateDate = supply.UpdateDate = DateTime.Now;
            }

            /*赋值*/
            supply.CountryId = country;
            supply.CityId = city;
            supply.Title = supplierName;
            supply.SupplierTypes = typeOfSupplier;
            supply.ParentSupplyId = typeOfSupplier == 2 ? parentSupplierId : 0;
            supply.Desp = description;
            supply.CoordsLatitude = latitude;//经纬度
            supply.CoordsLongitude = longitude;
            supply.CoverPhotoSrc = "/Staticfile/Supplier/SuppliesAttachment/" + logoImage; // 图片地址
            supply.InfoAddress = address;
            supply.InfoZip = zipcode;
            supply.InfoPhone = phone;
            supply.InfoFax = fax;
            supply.InfoState = stateOrProvince;
            supply.InfoWebsite = webSite;
            supply.InfoEmail = emailAddress;

            /*Categories关系表赋值*/
            List<SupplyAttributeData> datalist = new List<SupplyAttributeData>();
            foreach (var item in cids)
            {
                try
                {
                    int c = Convert.ToInt32(item);
                    SupplyAttributeData data = new SupplyAttributeData();
                    data.ArrtDataId = c;
                    datalist.Add(data);
                }
                catch
                {
                }
            }
            supply.UpdateDate = DateTime.Now;
            var result = _supplierService.UpdateSupplier(supply, datalist); //result=supply.SupplyId
            #endregion

            //解除绑定
            _supplierService.DeleteSupplyAttachmentBind(supplyId);

            #region logo关联
            SuppliesAttachmentBind sab = new SuppliesAttachmentBind();
            sab.AttGuidId = logoImage;
            sab.SupplyId = supply.SupplyId;
            sab.Type = 1;                       /*0普通，1logo*/
            _supplierService.AddSupplyAttachmentBind(sab);
            #endregion

            #region images关联
            if (!string.IsNullOrEmpty(images))
            {
                string[] imgs = images.Split(',');
                foreach (var i in imgs)
                {
                    sab = new SuppliesAttachmentBind();
                    sab.AttGuidId = i;
                    sab.SupplyId = supply.SupplyId;
                    sab.Type = 0;                       /*0普通，1logo*/
                    _supplierService.AddSupplyAttachmentBind(sab);
                }
            }
            #endregion

            return Json(result);
        }
        /// <summary>
        /// 返回SupplierList的数据
        /// </summary>
        /// <param name="OrderType">UserId</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string SupplierListData(int OrderBy, string DescOrAsc, int page, int pageSize, string SearchKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _supplierService.GetSupplierList(OrderBy, UserId, DescOrAsc, page, pageSize, SearchKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string TitleUrl = "";
            foreach (var item in list)
            {
                TitleUrl = "/Supplier/v/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.Title);
                sb.Append(" <tr>");
                sb.Append("     <td>" + item.row_number.ToString() + ".</td>");
                sb.Append("     <td><a target=\"_blank\" href=\"" + TitleUrl + "\">" + item.Title + "</a></td>");
                //sb.Append("     <td>MemberSuggested</td>");  
                sb.Append("     <td>" + item.CountryName + "</td>");
                sb.Append("     <td>" + item.CityName + "</td>");
                sb.Append("     <td>" + item.InfoState + "</td>");
                sb.Append("     <td>" + item.UpdateDate.ToString() + "</td>");
                sb.Append("     <td>" + item.TypesOfSupplier + "(<a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/Admin/Home/SupplyTypeSort','Get','',function(){supplyTypeManager.GetPopType(" + item.SupplyId + ",1);},'Sort Category');\" class=\"underline italic C31AEE3\">order</a>)</td>");
                //sb.Append("     <td>1</td>");
                sb.Append("     <td class=\"cssTaskSn\"><a href=\"javascript:void(0);\" onclick=\"GetSupplyId(" + item.SupplyId + ");\">" + item.Score + "</a></td>");
                //sb.Append("     <td><a href=\"#\" class=\"underline italic C31AEE3\">Confiq</a></td>");
                sb.Append("     <td class=\"alignR\">");
                sb.Append("       <div class=\"controlBg control3\">");
                sb.Append("          <span class=\"bg\">");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"\" class=\"preview NborderL\"></a>");
                sb.Append("          <a href=\"/admin/home/EditSupplierManage?sid=" + item.SupplyId + "\" title=\"edit\" onclick=\"\" class=\"edit\"></a>");
                //sb.Append("          <a href=\"#\"href=\"javascript:void(0);\"  class=\"admin \"></a>");
                sb.Append("          <a href=\"javascript:void(0);\" onclick=\"supplyManager.Del(" + item.SupplyId + ");\" class=\"delete NborderR\"></a>");
                sb.Append("          </span>");
                sb.Append("       </div>");
                sb.Append("    </td>");
                sb.Append(" </tr>");
            }

            return sb.ToString();
        }

        private string SponsorSupplieListData_Choose(int pageType, int type, int tab, int page, int pageSize, string strWhere, string strOrderby, out int total, out int itemTotal)
        {
            var list = _searchService.GetSupplyAdvancedSearchByPage(UserId, strWhere, strOrderby, page, pageSize, out total);
            //var list = _supplierService.GetSupplierList(0, UserId, "ASC", page, pageSize, SearchKeyword, "", out total, Where);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new StringBuilder();
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            foreach (var item in list)
            {
                sb.Append(" <li onclick=\"supplyEdit.Choose(" + item.SupplyId + ",'" + item.Title + "','" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._bn) + "');\">");
                //sb.Append(" <div class=\"control\">");
                //sb.Append("     <a href=\"javascript:void(0);\">");
                //sb.Append("         <span onclick=\"showPop2();\" class=\"checkboxBig right\"></span>");
                //sb.Append("     </a>");
                //sb.Append(" </div>");
                sb.Append("     <a href=\"javascript:void(0);\" class=\"img\">");
                sb.Append("         <img src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn) + "\" alt=\"\" />");
                sb.Append("     </a>");
                sb.Append("     <dl>");
                sb.Append("         <dt>");
                sb.Append("             <span class=\"title\">");
                sb.Append("                 <a href=\"javascript:void(0);\">" + item.Title + "</a>&nbsp;" + (string.IsNullOrEmpty(item.CityName) ? "null," : item.CityName + ",") + item.CountryName + "");
                sb.Append("             </span>");
                showNick = type == 1 ? "You " : "";
                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;
                if (friendCount == 0 && selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                }
                else if (friendCount == 0 && selfId == UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                }
                else if (friendCount > 0 && selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibtAdmin.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }
                //sb.Append("             <span class=\"friendsInfo italic f10 C7a7a7a\">");
                //sb.Append("                 <a href=\"javascript:void(0);\">" + (item.CommentsCount == 0 ? "0" : item.CommentsCount.ToString()) + " Friends</a>  has Been There");
                //sb.Append("             </span>");
                sb.Append("         </dt>");
                sb.Append("     </dl>");
                sb.Append("</li>");
            }

            return sb.ToString();
        }

        public JsonResult SetSupplierIsDel()
        {
            int sid = RequestHelper.GetValInt("objId", 0);

            BoolMessageItem<int> result = _supplierService.SetSupplyIsDel(sid);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SupplierImgUpload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Supplier/SuppliesAttachment/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string guid = Guid.NewGuid().ToString();
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = ".jpg";// Path.GetExtension(fileName); // 文件扩展名
                    string saveName = guid + fileExtension; // 保存文件名称
                    //50
                    string f1 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn.ToString() + fileExtension;
                    //80
                    string f2 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._tn.ToString() + fileExtension;
                    //100
                    string f3 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._mn.ToString() + fileExtension;
                    //156
                    string f4 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._bn.ToString() + fileExtension;

                    fileData.SaveAs(filePath + saveName);
                    //原图处理 50*50
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f1, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn, MakeThumbImage.ThumbPhotoModel.Fit);
                    //80*80
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f2, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._tn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._tn, MakeThumbImage.ThumbPhotoModel.Fit);
                    //100*100
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f3, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._mn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._mn, MakeThumbImage.ThumbPhotoModel.Fit);
                    //156*156
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f4, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._bn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._bn, MakeThumbImage.ThumbPhotoModel.Fit);

                    /*添加到数据库*/
                    SuppliesAttachment suppliesatt = new SuppliesAttachment();
                    suppliesatt.Guid = guid;
                    suppliesatt.FileName = fileName;
                    suppliesatt.SourceFileName = "/Staticfile/Supplier/SuppliesAttachment/" + guid;
                    suppliesatt.StatusId = 0;
                    suppliesatt.CreateData = DateTime.Now;

                    _supplierService.AddSupplyAttachment(suppliesatt);
                    /*添加数据库结束*/

                    return Json(new { Success = true, FileName = fileName, SaveName = guid });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message, SaveName = string.Empty }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose files to upload !" }, JsonRequestBehavior.AllowGet);
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SupplierImgagesUpload(HttpPostedFileBase fileData)
        {
            if (fileData != null)
            {
                try
                {
                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/Supplier/SuppliesAttachment/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string guid = Guid.NewGuid().ToString();
                    string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                    string fileExtension = ".jpg";// Path.GetExtension(fileName); // 文件扩展名
                    string saveName = guid + fileExtension; // 保存文件名称
                    //50
                    string f1 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn.ToString() + fileExtension;
                    //80
                    string f2 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._tn.ToString() + fileExtension;
                    //100
                    string f3 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._mn.ToString() + fileExtension;
                    //156
                    string f4 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._bn.ToString() + fileExtension;
                    //466X270
                    string f5 = guid + IMeet.IBT.Services.Identifier.SuppilyPhotoSize._large.ToString() + fileExtension;
                    //450*314
                    string f6 = guid + "_450x314" + fileExtension;

                    fileData.SaveAs(filePath + saveName);
                    //原图处理 50*50
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f1, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn, MakeThumbImage.ThumbPhotoModel.Cut);
                    //80*80
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f2, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._tn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._tn, MakeThumbImage.ThumbPhotoModel.Cut);
                    //100*100
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f3, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._mn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._mn, MakeThumbImage.ThumbPhotoModel.Cut);
                    //156*156
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f4, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._bn, (int)IMeet.IBT.Services.Identifier.SuppilyPhotoSize._bn, MakeThumbImage.ThumbPhotoModel.Cut);
                    //466*270
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f5, 466, 260, MakeThumbImage.ThumbPhotoModel.H);
                    //450*314
                    MakeThumbImage.MakeThumbPhoto(filePath + saveName, filePath + f6, 450, 314, MakeThumbImage.ThumbPhotoModel.Cut);

                    /*添加到数据库*/
                    SuppliesAttachment suppliesatt = new SuppliesAttachment();
                    suppliesatt.Guid = guid;
                    suppliesatt.FileName = fileName;
                    suppliesatt.SourceFileName = "/Staticfile/Supplier/SuppliesAttachment/" + guid;
                    suppliesatt.StatusId = 0;
                    suppliesatt.CreateData = DateTime.Now;

                    BoolMessageItem<string> b = _supplierService.AddSupplyAttachment(suppliesatt);
                    /*添加数据库结束*/
                    if (b.Success == true)
                    {
                        return Json(new { Success = true, FileName = fileName, SaveName = guid });
                    }
                    else
                    {
                        return Json(new { Sucess = false, Message = b.Message, saveName = string.Empty });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, Message = ex.Message, SaveName = string.Empty }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { Success = false, Message = "Please choose files to upload !" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region 排序
        //设置排序规则
        public JsonResult SetMemberSort()
        {
            int userId = RequestHelper.GetValInt("objId");
            int newNum = RequestHelper.GetValInt("newText");
            var bmi = _profileService.SetMemberSort(userId, newNum);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        //设置排序规则
        public JsonResult SetSortby()
        {
            int supplyId = RequestHelper.GetValInt("objId");
            int newNum = RequestHelper.GetValInt("newText");
            var bmi = _supplierService.SetSortby(supplyId, newNum);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }

        public JsonResult SetDestinationSort()
        {
            int dweekId = RequestHelper.GetValInt("objId");
            int newNum = RequestHelper.GetValInt("newText");
            var bmi = _widgetService.SetDestinationSort(dweekId, newNum);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        public JsonResult SetRecommendSort()
        {
            int mrpId = RequestHelper.GetValInt("objId");
            int newNum = RequestHelper.GetValInt("newText");
            var bmi = _widgetService.SetRecommendSort(mrpId, newNum);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        public JsonResult SetCountrySort()
        {
            int countryId = RequestHelper.GetValInt("objId");
            int newNum = RequestHelper.GetValInt("newText");
            var bmi = _widgetService.SetCountrySort(countryId, newNum);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        public JsonResult SetCitySort()
        {
            int cityId = RequestHelper.GetValInt("objId");
            int newNum = RequestHelper.GetValInt("newText");
            var bmi = _widgetService.SetCitySort(cityId, newNum);
            if (bmi.Success)
            {

            }
            return Json(bmi);

        }
        #endregion

        #region autoComplate / Select List

        public JsonResult GetCountry()
        {
            var list = _supplierService.GetAllCountryItem();
            return Json(list, JsonRequestBehavior.AllowGet);
            //return Json(_supplierService.GetAllCountry().ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult SelectCity(int id)
        {
            var list = _supplierService.GetAllCityItem(id);
            return Json(list, JsonRequestBehavior.AllowGet);
            //return Json(_supplierService.GetCityList(id).ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCategory()
        {
            var list = _searchService.GetAttributeDataItem();
            return Json(list, JsonRequestBehavior.AllowGet);
            //return Json(_searchService.GetAttributeDataList().ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchSupplier()
        {
            string keyWord = RequestHelper.GetVal("name");
            if (keyWord == "Type a Supplier's Name")
                keyWord = "";
            //判断拼接条件字符串
            string strKeywordCondition = " AND (sp.Title LIKE '%" + keyWord + @"%')";
            var list = _supplierService.GetSupplierItem(keyWord);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSearchName()
        {
            string title = RequestHelper.GetVal("name");
            //var list = _supplierService.GetSupplierItem(title);
            var list = _supplierService.GetAllSupplier(title);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public string GetItemDespOrType()
        {
            int id = RequestHelper.GetValInt("id");
            var supplier = _supplierService.GetSupplyDetail(id);
            if (supplier != null)
                return supplier.Desp ?? " ";
            else
                return " ";
        }
        #endregion

        #region Widget Code Create
        public ActionResult WidgetCode()
        {
            this.Title = "WidgetCode";
            this.PageTitle = "WidgetCode";
            //读取
            //Supply supply = _supplierService.GetSupplyDetail(sid);
            //IList<SupplyAttributeData> dataList = null;
            //IList<SuppliesAttachment> attaList_logo = null;
            //IList<SuppliesAttachment> attaList_images = null;
            //IList<SuppliesAttachmentBind> attaBindList = null;
            //if (supply.SupplyId == 0)
            //{
            //    this.PageTitle2 = "New Supplier";
            //}
            //else
            //{
            //    this.PageTitle2 = "Edit Supplier";
            //    dataList = _supplierService.GetSupplyAttributeData(supply.SupplyId);
            //    attaBindList = _supplierService.GetSuppliesAttachmentBind(supply.SupplyId);
            //    string[] guids_logo = (from a in attaBindList where a.Type == 1 select a.AttGuidId).ToArray();
            //    string[] guids_images = (from a in attaBindList where a.Type == 0 select a.AttGuidId).ToArray();
            //    attaList_logo = _supplierService.GetSuppliesAttachment(guids_logo);
            //    attaList_images = _supplierService.GetSuppliesAttachment(guids_images);
            //}

            //Supply parent_supply = _supplierService.GetSupplyDetail(supply.ParentSupplyId ?? 0);

            //Dictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("supply", supply);
            //dic.Add("parent_supply", parent_supply);
            //dic.Add("dataList", dataList);
            //dic.Add("attaList_logo", attaList_logo);
            //dic.Add("attaList_images", attaList_images);
            //return View(dic);
            return View();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult RecommendWidgetListAjx()
        {
            int tabId = RequestHelper.GetValInt("tabId", 1); //1 Destination ,2 Recommend , 3 Visited
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string Width = RequestHelper.GetUrlDecodeVal("Width");
            string Height = RequestHelper.GetUrlDecodeVal("Height");
            string BgColor = RequestHelper.GetVal("BgColor");


            int total = 0;
            int itemTotal = 0;
            string list = "";


            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Badge Specials

        /// <summary>
        /// get all badge specials for admin badgespecials admin page
        /// </summary>
        /// <returns></returns>
        public ActionResult BadgeSpecials()
        {
            this.PageTitle = "Badge Special Management";
            return View();
        }

        /// <summary>
        /// get badge specials list
        /// </summary>
        /// <returns></returns>
        public JsonResult BadgeSpecialsList()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pagesize = RequestHelper.GetValInt("pagesize", 10);

            string orderby = RequestHelper.GetVal("orderby");
            string sortord = RequestHelper.GetVal("sortord");

            int total = 0;
            List<BadgeSpecial> list = _travelBadgeService.GetBadgeSpecialsPage(page, pagesize, out total, orderby, sortord);

            int index = (page - 1) * pagesize + 1;
            string html = this.BadgeSpecialsListHtml(list, index);

            BoolMessageItem<int> bm = new BoolMessageItem<int>(total, true, html);

            return Json(bm);
        }

        /// <summary>
        /// html for badge specials list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private string BadgeSpecialsListHtml(List<BadgeSpecial> list, int index = 1)
        {
            StringBuilder html = new StringBuilder();

            int currentId = 0;
            if (list.Count > 0)
            {
                currentId = list.FirstOrDefault().BadgeSpecialId;
            }

            list.ForEach(bs =>
            {
                html.Append("<div class='adnCntRow left'>");

                //id
                html.Append("   <div class='adnCntNumb left'>");
                html.Append("       <p>" + index + ",</p>");
                html.Append("   </div>");

                //title
                html.Append("   <div class='adnCntName left'>");
                html.Append("       <p>" + bs.Title + "</p>");
                html.Append("   </div>");

                //point
                html.Append("   <div class='adnCntPoints left'>");
                if (bs.Type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity)
                {
                    html.Append("       <p>" + bs.USAPoint + "</p>");
                }
                else
                {
                    html.Append("       <p>" + bs.Point + "</p>");
                }
                html.Append("   </div>");

                //startdata
                html.Append("   <div class='adnCntStartDate left'>");
                html.Append("       <p>" + bs.StartDate.ToShortDateString() + "</p>");
                html.Append("   </div>");

                //end data
                html.Append("   <div class='adnCntEndDate left'>");
                html.Append("       <p>" + bs.EndDate.ToShortDateString() + "</p>");
                html.Append("   </div>");

                //type
                string type = "";

                switch (bs.Type)
                {
                    case (int)Identifier.BadgeSpecialType.IbtDriven:
                        type = "CLICK FOR POINTS";
                        break;
                    case (int)Identifier.BadgeSpecialType.EmailSignup:
                        type = "EXTERNAL EVENT";
                        break;
                    case (int)Identifier.BadgeSpecialType.UniqueIbt:
                        type = "POSTING ACTIVITY BONUS";
                        break;
                    case (int)Identifier.BadgeSpecialType.UniqueSpecificActivity:
                        type = "COMMENT/RATE BONUS";
                        break;
                    default:
                        type = "";
                        break;
                }

                html.Append("   <div class='adnCntType left'>");
                html.Append("       <p>" + type + "</p>");
                html.Append("   </div>");

                string visible = "Non-visible";
                if (bs.Visible == (int)Identifier.BadgeSpecialVisible.Visible)
                {
                    visible = "Visible";
                }
                html.Append("   <div class=\"adnCntVisible left\"><p>" + visible + "</p></div>");

                //start adnCntActions
                html.Append("   <div class='adnCntActions left'>");
                //start actionsContainer
                html.Append("       <div class='actionsContainer left'>");
                html.Append("           <div class='actionsButton left'><a href='/admin/home/badgespecial/" + bs.BadgeSpecialId + "' class='actionsBtnEdit' title='Edit Special'></a></div>");
                html.Append("           <div class='actionsButton left'><a href='/admin/home/badgespecialviewreport/" + bs.BadgeSpecialId + "' class='actionsBtnPreview' title='View Report'></a></div>");
                if (bs.Type == (int)Identifier.BadgeSpecialType.EmailSignup)
                    html.Append("           <div class='actionsButton left'><a href='/admin/home/EmailCommunication/" + bs.BadgeSpecialId + "' class='actionsBtnMail' title='Email Communication'></a></div>");
                else
                    html.Append("           <div class='actionsButton left'><a href='javascript:void(0);' class='btnMailClosed' title='Email Communication'></a></div>");
                html.Append("       </div>");
                //end actionsContainer div
                html.Append("           <div class='adminDeleteContainer left'><a href='javascript:void(0);' onclick='jBadgeSpecials.del(" + bs.BadgeSpecialId + ")' class='adminDeleteBtn' title='Delete Special'></a></div>");
                html.Append("   </div>");
                //end adnCntActions

                html.Append("</div>");
                currentId = bs.BadgeSpecialId;
                index++;
            });

            return html.ToString();
        }

        /// <summary>
        /// get one badge special for update
        /// </summary>
        /// <returns></returns>
        public ActionResult BadgeSpecial(int id = 0)
        {
            this.PageTitle = "<a class='doubleArrow' href='/admin/home/badgespecials'>Badge Special Management</a>";
            this.PageTitle2 = "Create New Badge Special";

            BadgeSpecial badgeSpecial = null;

            if (id > 0)
            {
                badgeSpecial = _travelBadgeService.GetBadgeSpecial(id);
                this.PageTitle2 = "Edit Badge Special";
            }

            ViewBag.BadgeSpecial = badgeSpecial;

            return View();
        }

        /// <summary>
        /// save badgespecial when add or update badgespecial
        /// </summary>
        /// <returns></returns>
        public JsonResult BadgeSpecialSave()
        {
            int id = RequestHelper.GetValInt("id");
            int type = RequestHelper.GetValInt("type");
            string title = RequestHelper.GetVal("title");
            string shortDescription = RequestHelper.GetVal("shortDescription");
            string longDescription = RequestHelper.GetVal("longDescription");
            string imageSrc = RequestHelper.GetVal("imagesrc");
            DateTime startDate = RequestHelper.GetValDateTime("startDate");
            DateTime endDate = RequestHelper.GetValDateTime("endDate");
            string link = RequestHelper.GetVal("link");
            int point = RequestHelper.GetValInt("numbOfPoints");
            string strvisible = RequestHelper.GetVal("visible");

            int postVisitCount = RequestHelper.GetValInt("UIBTPV");
            int percentage = RequestHelper.GetValInt("UIBTPOF");
            string activity = RequestHelper.GetVal("USAActivity");
            int upoint = RequestHelper.GetValInt("USAPoint");

            int visible = (int)Identifier.BadgeSpecialVisible.Invisible;
            if (strvisible == "on")
            {
                visible = (int)Identifier.BadgeSpecialVisible.Visible;
            }
            else
            {
                visible = (int)Identifier.BadgeSpecialVisible.Invisible;
            }

            BadgeSpecial bs = new BadgeSpecial()
            {
                Type = type,
                Title = title,
                ShortDescription = shortDescription,
                LongDescription = longDescription,
                ImageSrc = imageSrc,
                StartDate = startDate,
                EndDate = endDate,
                Link = link,
                Point = point,
                Visible = visible,
                UpdateDate = DateTime.Now
            };

            if (type == (int)Identifier.BadgeSpecialType.UniqueIbt)
            {
                bs.UIBTPV = postVisitCount;
                bs.UIBTPOF = percentage;
            }

            if (type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity)
            {
                bs.USAActivity = activity;
                bs.USAPoint = upoint;
            }

            int newId = 0;
            BoolMessageItem<int> bm = null;

            try
            {
                if (id == 0)
                {//add new badgespecial
                    bs.CreateDate = DateTime.Now;
                    bs.UpdateDate = DateTime.Now;
                    newId = _travelBadgeService.AddBadgeSpecial(bs).BadgeSpecialId;
                }
                else
                {//updated badgeSpecial 
                    newId = _travelBadgeService.EditBadgeSpecial(bs, id).BadgeSpecialId;
                }

                bm = new BoolMessageItem<int>(newId, true, id.ToString());
            }
            catch (Exception ex)
            {
                bm = new BoolMessageItem<int>(newId, false, ex.Message);
            }

            return Json(bm);
        }

        /// <summary>
        /// upload badge special images
        /// </summary>
        /// <returns></returns>
        public JsonResult BadgeSpecialUpload(HttpPostedFileBase fileData)
        {
            //文件上传后的保存路径
            string filePath = Server.MapPath("~/Staticfile/BadgeSpecial/");
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
            string fileExtension = Path.GetExtension(fileName); // 文件扩展名
            //string saveName = userId.ToString() + "_" + supplyId.ToString() + "_Indexsupply" + fileExtension; // 保存文件名称
            string saveName = Guid.NewGuid().ToString("N") + fileExtension;
            fileData.SaveAs(filePath + saveName);

            string returnFileName = "/Staticfile/BadgeSpecial/" + saveName;

            BoolMessageItem<string> bm = new BoolMessageItem<string>("", true, returnFileName);

            return Json(bm);
        }

        /// <summary>
        /// deleted a badge special for admin management
        /// </summary>
        /// <returns></returns>
        public JsonResult BadgeSpecialDel()
        {
            int id = RequestHelper.GetValInt("id");

            BoolMessage bm = new BoolMessage(false, "");

            if (id > 0)
            {
                bm = new BoolMessage(_travelBadgeService.DelBadgeSpecial(id), "");
            }

            return Json(bm);
        }

        /// <summary>
        /// sort by special badge
        /// </summary>
        /// <returns></returns>
        public JsonResult BadgeSpecialSortby()
        {
            int pid = RequestHelper.GetValInt("pid");
            int id = RequestHelper.GetValInt("id");

            if (_travelBadgeService.SortbyBadgeSpecial(pid, id))
            {
                BoolMessage bm = new BoolMessage(true, "");
                return Json(bm);
            }
            var bmf = new BoolMessage(false, "");
            return Json(bmf);
        }
        /// <summary>
        /// email communication
        /// </summary>
        /// <returns></returns>
        public ActionResult EmailCommunication(int id = 0)
        {
            this.PageTitle = "<a class='doubleArrow' href='/admin/home/badgespecials'>Badge Special Management</a>";
            this.PageTitle2 = "Email Communication";

            BadgeSpecial bs = _travelBadgeService.GetBadgeSpecial(id);
            if (bs != null)
                ViewBag.BadgeSpecialEmail = _travelBadgeService.GetBadgeSpecialEmailForBSId(bs.BadgeSpecialId);
            else
                ViewBag.BadgeSpecialEmail = null;

            ViewBag.BadgeSpecial = bs;
            return View();
        }
        /// <summary>
        /// admin home email communication 里面content的上传图片处理
        /// </summary>
        /// <param name="upload"></param>
        /// <param name="ckEditorFuncNum"></param>
        /// <param name="ckEditor"></param>
        /// <param name="langCode"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult EmailCommunicationUploadImage(HttpPostedFileBase upload, string ckEditorFuncNum, string ckEditor, string langCode)
        {
            try
            {
                if (upload.ContentType == "image/jpeg" || upload.ContentType == "image/pjpeg")
                {
                    string url = Request.Url.GetLeftPart(UriPartial.Authority);
                    string message = "Image was saved correctly";

                    //文件上传后的保存路径
                    string filePath = Server.MapPath("~/Staticfile/BadgeSpecial/EmailContentImages/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string fileName = Path.GetFileName(upload.FileName);// 原始文件名称
                    string fileExtension = Path.GetExtension(fileName); // 文件扩展名

                    string saveName = Guid.NewGuid().ToString("N") + fileExtension;
                    string savePath = filePath + saveName;
                    upload.SaveAs(savePath);

                    string output = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + ckEditorFuncNum + ", \"" + url + "/Staticfile/BadgeSpecial/EmailContentImages/" + saveName + "\", \"" + message + "\");</script></body></html>";
                    return Content(output);
                }
                else
                {
                    throw new Exception("Upload format error");
                }
            }
            catch (Exception ex)
            {
                string output = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + ckEditorFuncNum + ", \" \", \"" + ex.Message + "\");</script></body></html>";
                return Content(output);
            }
        }
        /// <summary>
        /// get emiail list
        /// </summary>
        /// <returns></returns>
        public JsonResult GetEmailList()
        {
            int id = RequestHelper.GetValInt("id");
            int page = RequestHelper.GetValInt("page", 1);
            int pagesize = RequestHelper.GetValInt("pagesize", 10);
            string orderBy = RequestHelper.GetVal("orderby");

            int total = 0;

            List<UserBadgeSpecial> list = _travelBadgeService.GetUserBadgeSpecialsPage(id, page, pagesize, out total, orderBy);

            string listHtml = this._GetEmailListHTML(list);
            string pageHtml = this._GetEmailListHTMLPage(page, pagesize, total);

            Dictionary<string, string> dc = new Dictionary<string, string>();
            dc.Add("List", listHtml);
            dc.Add("Page", pageHtml);

            BoolMessageItem<Dictionary<string, string>> bmd = new BoolMessageItem<Dictionary<string, string>>(dc, true, total.ToString());

            return Json(bmd);
        }
        /// <summary>
        /// save badge special email
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult BadgeSpecialEmailSave()
        {
            int id = RequestHelper.GetValInt("id");
            int bseid = RequestHelper.GetValInt("bseid");

            string fromEmail = RequestHelper.GetVal("fromEmail");
            string title = RequestHelper.GetVal("title");
            string msg = RequestHelper.GetVal("msg");

            BadgeSpecialEmail bse = new BadgeSpecialEmail()
            {
                BadgeSpecialId = id,
                Title = title,
                EmailFrom = fromEmail,
                Content = msg,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now
            };

            if (bseid > 0)
                bse = _travelBadgeService.UpdatedBadgeSpecialEmail(bse, bseid);
            else
                bse = _travelBadgeService.AddBadgeSpecialEmail(bse);

            BoolMessage bm = new BoolMessage(true, bse.BadgeSpecialEmailId.ToString());

            return Json(bm);
        }
        /// <summary>
        /// badge special email send for test
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult BadgeSpecialEmailSendForTest()
        {
            string fromEmail = RequestHelper.GetVal("fromEmail");
            string title = RequestHelper.GetVal("title");
            string msg = RequestHelper.GetVal("msg");

            string testEmail = RequestHelper.GetVal("testEmail");

            IMeet.IBT.Common.Email.EmailService _emailService = new Common.Email.EmailService();
            _emailService.SendBadgeSpecial(testEmail, fromEmail, title, msg, true);

            BoolMessage bm = new BoolMessage(true, "");

            return Json("");
        }
        /// <summary>
        /// badge special email send
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult BadgeSpecialEmailSend()
        {
            int id = RequestHelper.GetValInt("id");

            int bseid = RequestHelper.GetValInt("bseid");

            string fromEmail = RequestHelper.GetVal("fromEmail");
            string title = RequestHelper.GetVal("title");
            string msg = RequestHelper.GetVal("msg");

            BadgeSpecialEmail bse = new BadgeSpecialEmail()
            {
                BadgeSpecialId = id,
                Title = title,
                EmailFrom = fromEmail,
                Content = msg,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now
            };

            if (bseid > 0)
                bse = _travelBadgeService.UpdatedBadgeSpecialEmail(bse, bseid);
            else
                bse = _travelBadgeService.AddBadgeSpecialEmail(bse);

            //BadgeSpecialEmail bse = _travelBadgeService.GetBadgeSpecialEmailForBSId(id);

            BoolMessage bm = new BoolMessage(false, "Please save email");

            if (bse != null)
            {
                Common.Email.EmailService _emailService = new Common.Email.EmailService();

                List<UserBadgeSpecial> list = _travelBadgeService.GetUserBadgeSpecials(bse.BadgeSpecialId);

                list.AsParallel().ForAll(ubs =>
                {
                    try
                    {
                        _emailService.SendBadgeSpecial(ubs.User.Email, bse.EmailFrom, bse.Title, bse.Content, true);
                    }
                    catch (Exception ex)
                    { }
                });

                bm = new BoolMessage(true, "");
            }

            return Json(bm);
        }
        /// <summary>
        /// badge special view report
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ViewResult BadgeSpecialViewReport(int id = 0)
        {
            this.PageTitle = "<a class='doubleArrow' href='/admin/home/badgespecials'>Badge Special Management</a>";
            this.PageTitle2 = "View Report";

            BadgeSpecial bs = _travelBadgeService.GetBadgeSpecial(id);
            if (bs != null)
                ViewBag.BadgeSpecialEmail = _travelBadgeService.GetBadgeSpecialEmailForBSId(bs.BadgeSpecialId);
            else
                ViewBag.BadgeSpecialEmail = null;

            ViewBag.BadgeSpecial = bs;
            return View();
        }
        public ViewResult BadgeSpecialExportReport(int id = 0)
        {

            BadgeSpecial bs = _travelBadgeService.GetBadgeSpecial(id);

            if (bs == null)
                return null;

            List<UserBadgeSpecial> list = _travelBadgeService.GetUserBadgeSpecials(bs.BadgeSpecialId).OrderBy(bss => bss.User.Profile.Firstname).ThenBy(bss => bss.User.Profile.Lastname).ToList();

            HSSFWorkbook hssfworkbook = new HSSFWorkbook();
            ISheet sheet = hssfworkbook.CreateSheet("report");
            IRow row = sheet.CreateRow(0);

            //ICellStyle cs = hssfworkbook.CreateCellStyle();
            //cs.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.BLUE.index;
            //cs.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.BLUE.index;

            row.CreateCell(0).SetCellValue("Name");
            row.CreateCell(1).SetCellValue("Email Address");
            row.CreateCell(2).SetCellValue("Date of Action");
            row.CreateCell(3).SetCellValue("Total Number of Points(as of register)");

            int row_num = 1;
            list.ForEach(ubs =>
            {
                IRow row_u = sheet.CreateRow(row_num);
                row_u.CreateCell(0).SetCellValue(ubs.User.Profile.Firstname + " " + ubs.User.Profile.Lastname);
                row_u.CreateCell(1).SetCellValue(ubs.User.Email);
                row_u.CreateCell(2).SetCellValue(ubs.ReceivedDate.Value.ToShortDateString());
                row_u.CreateCell(3).SetCellValue(ubs.point.Value);

                row_num++;
            });

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Techsailor";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "ibt badge special report";

            hssfworkbook.SummaryInformation = si;

            string filename = bs.Title + "(EmailReport" + DateTime.Now.ToString("yyyyMMdd") + ").xls";
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
            Response.Clear();

            System.IO.MemoryStream msFile = new MemoryStream();
            hssfworkbook.Write(msFile);

            Response.BinaryWrite(msFile.GetBuffer());
            Response.End();
            return null;
        }
        /// <summary>
        /// 上传excel email
        /// </summary>
        /// <param name="fileData"></param>
        /// <returns></returns>
        public JsonResult BadgeSpecialUploadEmail(HttpPostedFileBase fileData)
        {
            try
            {
                int id = RequestHelper.GetValInt("id");

                BoolMessage bm = new BoolMessage(true, "");

                //文件上传后的保存路径
                string filePath = Server.MapPath("~/Staticfile/BadgeSpecial/ImportEmail/");
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                string fileExtension = Path.GetExtension(fileName); // 文件扩展名

                string saveName = filePath + Guid.NewGuid().ToString("N") + fileExtension;
                fileData.SaveAs(saveName);

                using (FileStream fStream = new FileStream(saveName, FileMode.Open, FileAccess.Read))
                {
                    HSSFWorkbook workBook = new HSSFWorkbook(fStream);

                    ISheet sheet = workBook.GetSheetAt(0);

                    List<string> emails = new List<string>();
                    for (int i = 1; i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        string email = row.GetCell(0).StringCellValue;
                        if (!string.IsNullOrEmpty(email))
                        {
                            emails.Add(email);
                        }
                    }

                    if (emails.Count() > 0)
                    {
                        bm = _travelBadgeService.ImportBadgeSpecialEmail(emails, id);
                    }
                }

                return Json(bm);

            }
            catch (Exception ex)
            {
                BoolMessage bm = new BoolMessage(false, ex.Message);

                return Json(bm);
            }
        }
        public string BadgeSpecialUploadEmailFalse()
        {
            string emails = RequestHelper.GetVal("emails");

            try
            {
                if (!string.IsNullOrWhiteSpace(emails))
                {
                    var list = emails.Split(',');

                    HSSFWorkbook workbook = new HSSFWorkbook();
                    ISheet sheet = workbook.CreateSheet();

                    for (int i = 0; i < list.Count(); i++)
                    {
                        IRow row = sheet.CreateRow(i);

                        ICell cell = row.CreateCell(0);
                        cell.SetCellValue(list[i]);
                    }

                    DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                    dsi.Company = "Techsailor";
                    workbook.DocumentSummaryInformation = dsi;

                    SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                    si.Author = "ibt";
                    si.ApplicationName = "ibt";
                    si.LastAuthor = "ibt admin";
                    si.Comments = "ibt";
                    si.Title = "ibt email infor";
                    si.Subject = "email";
                    si.CreateDateTime = DateTime.Now;
                    workbook.SummaryInformation = si;

                    using (MemoryStream ms = new MemoryStream())
                    {
                        workbook.Write(ms);
                        ms.Flush();
                        ms.Position = 0;

                        Response.ContentType = "application/vnd.ms-excel";
                        Response.ContentEncoding = Encoding.UTF8;
                        Response.Charset = "";
                        Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode("EmailNotImportSuccessfully(" + DateTime.Now.ToString("yyyyMMddHHmmss") + ").xls", Encoding.UTF8));
                        Response.BinaryWrite(ms.GetBuffer());
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                return emails;
            }

            return emails;
        }
        /// <summary>
        /// get html string for get email list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private string _GetEmailListHTML(List<UserBadgeSpecial> list)
        {
            StringBuilder sb = new StringBuilder();

            list.ForEach(ubs =>
            {

                string userName = "";
                if (ubs.User != null)
                    userName = ubs.User.Profile.Firstname + " " + ubs.User.Profile.Lastname;

                sb.Append("<div class=\"emailListRow left\">");

                sb.Append("    <div class=\"emlListName left\">");
                sb.Append("        <p>" + userName + "</p>");
                sb.Append("    </div>");

                sb.Append("    <div class=\"emlListEmailAddress left\">");
                sb.Append("        <p>" + ubs.User.Email + "</p>");
                sb.Append("    </div>");

                sb.Append("    <div class=\"emlListDateOfAction left\">");
                sb.Append("        <p>" + ubs.ReceivedDate.Value.ToShortDateString() + "</p>");
                sb.Append("    </div>");

                sb.Append("    <div class=\"emlListTotalPoints left\">");
                sb.Append("        <p>" + ubs.point + "</p>");
                sb.Append("    </div>");

                sb.Append("</div>");
            });

            return sb.ToString();
        }
        /// <summary>
        /// get html string for get emial list's page
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string _GetEmailListHTMLPage(int page, int pagesize, int total)
        {
            var a = (double)total / (double)pagesize;
            int lastPage = (int)Math.Ceiling(a);

            StringBuilder sb = new StringBuilder();

            sb.Append("<ul>");

            if (page == 1)
                sb.Append("    <li class=\"emlPageBtn left\"><a href=\"javascript:void(0);\" class=\"emlPgBtnPrevious\"></a></li>");
            else
                sb.Append("    <li class=\"emlPageBtn left\"><a href=\"javascript:void(0);\" onclick=\"j_email_list.searchPage(" + (page - 1) + ");\" class=\"emlPgBtnPrevious\"></a></li>");

            //button space numeric
            sb.Append("    <li class=\"emlPageBtn left\"><a class=\"emlPgBtnNumeric\"></a></li><li class=\"emlPageBtn left\"><a class=\"emlPgBtnNumeric\"></a></li>");

            int leftPage = lastPage - page - 5;
            if (leftPage < 0)
            {
                leftPage = page - 5 + leftPage;
            }
            else
            {
                leftPage = page - 5;
            }

            for (int i = leftPage; i < page; i++)
            {
                if (i >= 1)
                    sb.Append("    <li class=\"emlPageBtn left\"><a href=\"javascript:void(0);\" onclick=\"j_email_list.searchPage(" + i + ");\" class=\"emlPgBtnNumeric\">" + i + "</a></li>");
            }

            sb.Append("    <li class=\"emlPageBtn left\"><a href=\"javascript:void(0);\" class=\"emlPgBtnNumeric emlSelectedListPg\">" + page + "</a></li>");

            int rightPage = page - 6;
            if (rightPage < 0)
                rightPage = page + 5 - rightPage;
            else
                rightPage = page + 5;

            for (int i = (page + 1); i <= rightPage; i++)
            {
                if (i <= lastPage)
                    sb.Append("    <li class=\"emlPageBtn left\"><a href=\"javascript:void(0);\" onclick=\"j_email_list.searchPage(" + i + ");\" class=\"emlPgBtnNumeric\">" + i + "</a></li>");
            }

            //button space numeric
            sb.Append("    <li class=\"emlPageBtn left\"><a class=\"emlPgBtnNumeric\"></a></li><li class=\"emlPageBtn left\"><a class=\"emlPgBtnNumeric\"></a></li>");

            if (lastPage == page)
                sb.Append("    <li class=\"emlPageBtn left\"><a href=\"javascript:void(0);\" class=\"emlPgBtnNext\"></a></li>");
            else
                sb.Append("    <li class=\"emlPageBtn left\"><a href=\"javascript:void(0);\" onclick=\"j_email_list.searchPage(" + (page + 1) + ");\" class=\"emlPgBtnNext\"></a></li>");

            sb.Append("</ul>");


            //<ul>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnPrevious"></a></li>
            //    <li class="emlPageBtn left"><a class="emlPgBtnNumeric">
            //        <!-- button space numeric -->
            //    </a></li>
            //    <li class="emlPageBtn left"><a class="emlPgBtnNumeric">
            //        <!-- button space numeric -->
            //    </a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric emlSelectedListPg">1</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">2</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">3</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">4</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">5</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">6</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">7</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">8</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">9</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">10</a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNumeric">. . .</a></li>
            //    <li class="emlPageBtn left"><a class="emlPgBtnNumeric">
            //        <!-- button space numeric -->
            //    </a></li>
            //    <li class="emlPageBtn left"><a class="emlPgBtnNumeric">
            //        <!-- button space numeric -->
            //    </a></li>
            //    <li class="emlPageBtn left"><a href="#" class="emlPgBtnNext"></a></li>
            //</ul>
            return sb.ToString();
        }
        /// <summary>
        /// send email to auto active
        /// </summary>
        /// <returns></returns>
        public string SendMailToAutoActive()
        {
            _profileService.SendEmailToAutoActive();
            return "Congratulations";
        }
        #endregion

        #region Email Tools

        /// <summary>
        /// Email List page
        /// 邮件列表页
        /// </summary>
        /// <returns></returns>
        public ViewResult EmailList()
        {
            this.PageTitle = "Email Campaign Management";
            return View();
        }
        /// <summary>
        /// New Email List Page
        /// </summary>
        /// <returns></returns>
        public ViewResult NewEmailList()//EDM
        {
            this.PageTitle = "Email Campaign Management";
            return View();
        }
        /// <summary>
        /// 获取email list 分页数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetEmailListPage()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            int total;
            var list = _adminEmailService.GetEmailList(page, pageSize, out total);
            string html = this._GetEmailListPageHtml(list, page, pageSize);
            BoolMessageItem<int> bm = new BoolMessageItem<int>(total, true, html);

            return Json(bm);
        }
        public JsonResult GetNewEmailListPage()//EDM
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            int total;
            var list = _adminEmailService.GetNewEmailList(page, pageSize, out total);
            string html = this._GetNewEmailListPageHtml(list, page, pageSize);
            BoolMessageItem<int> bm = new BoolMessageItem<int>(total, true, html);

            return Json(bm);
        }
        /// <summary>
        /// New Email list
        /// </summary>
        /// <param name="list"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private string _GetNewEmailListPageHtml(List<Email> list, int page, int pageSize)//EDM
        {
            int index = (page - 1) * pageSize + 1;
            string html = "";

            list.ForEach(e =>
            {
                html += "<li class=\"email-status-rows left\">";

                html += "<div class=\"email-stat-number left\"><p>" + index + ".</p></div>";
                html += "<div class=\"email-stat-campg-title left\"><p>" + e.Title + "</p></div>";
                html += "<div class=\"email-stat-blast left\"><p>" + e.CreateDate.Value.ToString("yyyy-MM-dd") + "</p></div>";
                html += "<div class=\"email-stat-recipients left\"><p>" + this.RecipientsTostring(e.Recipients.Value) + "</p></div>";
                html += "<div class=\"email-stat-created-by left\"><p>" + e.User.Profile.Firstname + " " + e.User.Profile.Lastname + "</p></div>";

                string sentStatusClass = "";
                string sentStatus = this.SentStatusTostring(e.SendStatus, out sentStatusClass);
                html += "<div class=\"email-stat-status left\">";
                html += "    <div class=\"email-stat-stauts-holder left\"><div class=\"email-stat-status-generate email-genrt-" + sentStatusClass + "\"></div></div>";
                html += "    <div class=\"email-stat-status-content left\"><span class=\"email-stat-status-report\">" + sentStatus + "</span></div>";
                html += "</div>";

                html += "<div class=\"email-stat-buttons left\">";
                html += "    <ul class=\"email-stat-buttons-container left\">";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/newemailupdate/" + e.EmailId + "\" class=\"actionsBtnEdit\" title=\"Edit Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/viewnewemail/" + e.EmailId + "\" class=\"actionsBtnPreview\" target=\"_blank\" title=\"View Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/newemailupdate/" + e.EmailId + "?duplicate=email\" class=\"actionsBtnMail\" title=\"Duplicate email\"></a></li>";
                html += "    </ul>";
                html += "    <div class=\"email-stat-btn-delete left\"><a class=\"emailDeleteBtn\" title=\"Delete Email\" onclick=\"jEmailList.Del(" + e.EmailId + ");\"></a></div>";
                html += "</div>";

                html += "</li>";

                index++;
            });

            return html;
        }
        /// <summary>
        /// 获取GetEmailListPage的html
        /// </summary>
        /// <param name="list"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private string _GetEmailListPageHtml(List<Email> list, int page, int pageSize)
        {
            int index = (page - 1) * pageSize + 1;
            string html = "";

            list.ForEach(e =>
            {
                html += "<li class=\"email-status-rows left\">";

                html += "<div class=\"email-stat-number left\"><p>" + index + ".</p></div>";
                html += "<div class=\"email-stat-campg-title left\"><p>" + e.Title + "</p></div>";
                html += "<div class=\"email-stat-blast left\"><p>" + e.CreateDate.Value.ToString("yyyy-MM-dd") + "</p></div>";
                html += "<div class=\"email-stat-recipients left\"><p>" + this.RecipientsTostring(e.Recipients.Value) + "</p></div>";
                html += "<div class=\"email-stat-created-by left\"><p>" + e.User.Profile.Firstname + " " + e.User.Profile.Lastname + "</p></div>";

                string sentStatusClass = "";
                string sentStatus = this.SentStatusTostring(e.SendStatus, out sentStatusClass);
                html += "<div class=\"email-stat-status left\">";
                html += "    <div class=\"email-stat-stauts-holder left\"><div class=\"email-stat-status-generate email-genrt-" + sentStatusClass + "\"></div></div>";
                html += "    <div class=\"email-stat-status-content left\"><span class=\"email-stat-status-report\">" + sentStatus + "</span></div>";
                html += "</div>";

                html += "<div class=\"email-stat-buttons left\">";
                html += "    <ul class=\"email-stat-buttons-container left\">";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/emailupdate/" + e.EmailId + "\" class=\"actionsBtnEdit\" title=\"Edit Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/viewemail/" + e.EmailId + "\" class=\"actionsBtnPreview\" target=\"_blank\" title=\"View Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/emailupdate/" + e.EmailId + "?duplicate=email\" class=\"actionsBtnMail\" title=\"Duplicate email\"></a></li>";
                html += "    </ul>";
                html += "    <div class=\"email-stat-btn-delete left\"><a class=\"emailDeleteBtn\" title=\"Delete Email\" onclick=\"jEmailList.Del(" + e.EmailId + ");\"></a></div>";
                html += "</div>";

                html += "</li>";

                index++;
            });

            return html;
        }
        /// <summary>
        /// recipients int to string
        /// </summary>
        /// <param name="recipients"></param>
        /// <returns></returns>
        private string RecipientsTostring(int recipients)
        {
            string result = "";

            switch (recipients)
            {
                case (int)Identifier.EmailRecipients.AllMembers:
                    result = "All Members";
                    break;
                case (int)Identifier.EmailRecipients.BadgesMembers:
                    result = "Badges Members";
                    break;
                case (int)Identifier.EmailRecipients.NotActivatedMembers:
                    result = "Not Activated Members";
                    break;
                case (int)Identifier.EmailRecipients.Custom:
                    result = "Custom";
                    break;
                default:
                    break;
            }

            return result;
        }
        /// <summary>
        /// Email sentStatus to string
        /// </summary>
        /// <param name="sendStatus"></param>
        /// <param name="sendStatusClass"></param>
        /// <returns></returns>
        private string SentStatusTostring(int? sendStatus, out string sendStatusClass)
        {
            string result = "";
            sendStatusClass = "";

            if (sendStatus != null)
            {
                switch (sendStatus.Value)
                {
                    case (int)Identifier.EmailSendStatus.Sent:
                        result = "Sent";
                        sendStatusClass = "sent";
                        break;
                    case (int)Identifier.EmailSendStatus.HaventSent:
                        result = "Havent Sent";
                        sendStatusClass = "notsent";
                        break;
                    default:
                        result = "Achive";
                        sendStatusClass = "achive";
                        break;
                }
            }
            else
            {
                result = "Achive";
                sendStatusClass = "achive";
            }

            return result;
        }

        /// <summary>
        /// 删除email
        /// </summary>
        /// <returns></returns>
        public JsonResult EmailListDel()
        {
            int id = RequestHelper.GetValInt("id");
            _adminEmailService.Del(id);
            var bm = new BoolMessage(true, "");
            return Json(bm);
        }
        public JsonResult NewEmailListDel()//EDM
        {
            int id = RequestHelper.GetValInt("id");
            _adminEmailService.Del(id);
            var bm = new BoolMessage(true, "");
            return Json(bm);
        }
        /// <summary>
        /// Delete Schedule Email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteScheduleEmail(int? id)
        {
            if (!String.IsNullOrWhiteSpace(id.ToString()))
            {
                using (var _db = new IBTEntities())
                {
                    _db.Database.ExecuteSqlCommand(string.Format("DELETE [EmailSendLog] WHERE [EmailId]={0} AND (DATEDIFF(SECOND, getdate(),[ScheduleTime]) >= 0);", id));
                }
            }
            return Redirect(Request.UrlReferrer.AbsolutePath);
        }

        /// <summary>
        /// add or edit email
        /// 添加或者修改email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ViewResult EmailUpdate(int id = 0)
        {
            this.PageTitle = "<a class='doubleArrow' href='/admin/home/emaillist'>Email Campaign Management</a>";
            string pageTitle2 = "Create An Email";

            string duplicate = RequestHelper.GetVal("duplicate");
            //string schedule = RequestHelper.GetVal("schedule");
            //ViewBag.EmailCampaignManagementURL = ;
            ViewBag.newEmail = 0;
            //= Request.Url.Host + Url.Action("emaillist", "home");

            SiteConfiguration _configuration = new SiteConfiguration(); ;
            ViewBag.SiteUrl = _configuration.SiteUrl;

            if (id > 0)
            {
                pageTitle2 = "Edit An Email";
            }
            else
            {
                ViewBag.newEmail = 1;
            }

            Email returnEMail = _adminEmailService.Find(id);

            if (string.IsNullOrEmpty(duplicate))
            {
                ViewBag.Email = returnEMail;
            }
            else if (duplicate == "email")
            {
                pageTitle2 = "Create An Email";
                Email email = new Email();
                email.SenderEmail = returnEMail.SenderEmail;
                email.Title = returnEMail.Title;
                email.SubTitle = returnEMail.SubTitle;
                email.Logo = returnEMail.Logo;
                email.Subject = returnEMail.Subject;
                email.Recipients = returnEMail.Recipients;
                email.RecipientsEmail = returnEMail.RecipientsEmail;
                email.Content = returnEMail.Content;
                email.SenderName = returnEMail.SenderName;

                ViewBag.Email = email;
            }


            using (var _db = new IBTEntities())
            {
                int previousCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0);", id).FirstOrDefault();
                //SELECT TOP 1 [ScheduleTime] from [dbo].[EmailSendLog] where [EmailId] =11 AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 20) AND (DATEDIFF(DAY, [ScheduleTime], getdate()) >=0) order by [ScheduleTime] desc;
                if (previousCount > 0)
                {
                    var previousDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0)", id).First();
                    //DateTime previousDate = DateTime.Parse(previousDateVar);
                    //This email has been sent to 2 member(s) in last 7 day(s) with the latest date being on 4/5/2015 9:00 AM
                    ViewBag.sentEmailMsg = string.Format("This email has been sent to {0} member(s) in 7 day(s) with the latest date being on {1}", previousCount, previousDate.ToString());
                }

                int nextCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                if (nextCount > 0)
                {
                    var nextDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                    ViewBag.nextEmailMsg = string.Format("This email has been scheduled to be sent on the {0} to {1} member(s)", nextDate.ToString(), nextCount);
                }
            }

            this.PageTitle2 = pageTitle2;
            return View();
        }
        /// <summary>
        /// add or edit new-email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ViewResult NewEmailUpdate(int id = 0)
        {
            this.PageTitle = "<a class='doubleArrow' href='/admin/home/newemaillist'>Email Campaign Management</a>";
            string pageTitle2 = "Create An Email";

            string duplicate = RequestHelper.GetVal("duplicate");
            //string schedule = RequestHelper.GetVal("schedule");
            //ViewBag.EmailCampaignManagementURL = ;
            ViewBag.newEmail = 0;
            //= Request.Url.Host + Url.Action("emaillist", "home");

            SiteConfiguration _configuration = new SiteConfiguration(); ;
            ViewBag.SiteUrl = _configuration.SiteUrl;

            if (id > 0)
            {
                pageTitle2 = "Edit An Email";
            }
            else
            {
                ViewBag.newEmail = 1;
            }

            Email returnEMail = _adminEmailService.Find(id);

            if (string.IsNullOrEmpty(duplicate))
            {
                ViewBag.Email = returnEMail;
            }
            else if (duplicate == "email")
            {
                pageTitle2 = "Create An Email";
                Email email = new Email();
                email.SenderEmail = returnEMail.SenderEmail;
                email.Title = returnEMail.Title;
                email.SubTitle = returnEMail.SubTitle;
                email.Logo = returnEMail.Logo;
                email.Subject = returnEMail.Subject;
                email.Recipients = returnEMail.Recipients;
                email.RecipientsEmail = returnEMail.RecipientsEmail;
                email.Content = returnEMail.Content;
                email.SenderName = returnEMail.SenderName;

                ViewBag.Email = email;
            }


            using (var _db = new IBTEntities())
            {
                int previousCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0);", id).FirstOrDefault();
                //SELECT TOP 1 [ScheduleTime] from [dbo].[EmailSendLog] where [EmailId] =11 AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 20) AND (DATEDIFF(DAY, [ScheduleTime], getdate()) >=0) order by [ScheduleTime] desc;
                if (previousCount > 0)
                {
                    var previousDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0)", id).First();
                    //DateTime previousDate = DateTime.Parse(previousDateVar);
                    //This email has been sent to 2 member(s) in last 7 day(s) with the latest date being on 4/5/2015 9:00 AM
                    ViewBag.sentEmailMsg = string.Format("This email has been sent to {0} member(s) in 7 day(s) with the latest date being on {1}", previousCount, previousDate.ToString());
                }

                int nextCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                if (nextCount > 0)
                {
                    var nextDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                    ViewBag.nextEmailMsg = string.Format("This email has been scheduled to be sent on the {0} to {1} member(s)", nextDate.ToString(), nextCount);
                }
            }

            this.PageTitle2 = pageTitle2;
            return View();
        }
        /// <summary>
        /// 保存Email
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult EmailSave()
        {
            Email email = new Email();
            email.TypeId = (int)Identifier.EmailType.email;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.Content = RequestHelper.GetVal("Content");
            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;
            email.ShowMPB = RequestHelper.GetValInt("MPB");
            

            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            var bm = new BoolMessageItem<int>(email.EmailId, true, "");

            return Json(bm);
        }
        /// <summary>
        /// NewEmail
        /// </summary>
        /// <returns></returns>
         [ValidateInput(false)]
        public JsonResult NewEmailSave()//EDM
        {
            Email email = new Email();
            email.TypeId = (int)Identifier.EmailType.newemail;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.Content = RequestHelper.GetVal("Content");

            string Content1 = RequestHelper.GetVal("emailContent1");
            string Content2 = RequestHelper.GetVal("emailContent2");            

            //add all content and suggestedList to Email Content
            email.Content = Content1 + " (Le+|-(((w4+---====r-GQ " + Content2;// +" (Le+|-(((w4+---====r-GQ ";
            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;
            email.ShowMPB = RequestHelper.GetValInt("MPB");


            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            var bm = new BoolMessageItem<int>(email.EmailId, true, "");

            return Json(bm);
        }
        /// <summary>
        /// 保Email
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult EmailConfirm()
        {
            Email email = new Email();
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.Content = RequestHelper.GetVal("Content");
            email.ScheduleStatus = RequestHelper.GetValInt("ScheduleStatus");
            email.TypeId = (int)Identifier.EmailType.email;
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            if (email.ScheduleStatus == (int)Identifier.EmailScheduleStatus.Immediate)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                email.ScheduleTime = DateTime.Now;
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime", DateTime.Now);
            }
            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;

            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            _adminEmailService.AddEmailSendLogTreading(email.EmailId);

            var bm = new BoolMessageItem<int>(email.EmailId, true, "");
            return Json(bm);
        }

        /// <summary>
        /// 保Email
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult NewEmailConfirm()
        {
            Email email = new Email();
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");

            string Content1 = RequestHelper.GetVal("emailContent1");
            string Content2 = RequestHelper.GetVal("emailContent2");

            //add all content and suggestedList to Email Content
            email.Content = Content1 + " (Le+|-(((w4+---====r-GQ " + Content2;
            //email.Content = Content1 + Content2;

            email.ScheduleStatus = RequestHelper.GetValInt("ScheduleStatus");
            email.TypeId = (int)Identifier.EmailType.newemail;
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            if (email.ScheduleStatus == (int)Identifier.EmailScheduleStatus.Immediate)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                email.ScheduleTime = DateTime.Now;
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime", DateTime.Now);
            }
            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;

            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            _adminEmailService.AddEmailSendLogTreading(email.EmailId);
            var bm = new BoolMessageItem<int>(email.EmailId, true, "");
            return Json(bm);
        }

        /// <summary>
        /// save email logo image
        /// 
        /// 保存email上传的logo图片
        /// </summary>
        /// <param name="fileData"></param>
        /// <returns></returns>
        ///  [HttpPost]
        public ActionResult EmailLogoUpload(HttpPostedFileBase fileData)
        {

            //var erer = fileData.ContentType;
            //var rerer = fileData.ContentLength;
            //var rwqerer = fileData; 
            //    fileData.SaveAs("C:\\AliTestingFolder\\boodf.png");

            //    //BoolMessageItem<string> bm =
            //    return Json(new BoolMessageItem<string>("", true, "C:\\AliTestingFolder\\boodf.png"));

            try
            {
                string path = "/Staticfile/Email/Logo/" + DateTime.Now.ToString("yyyyMMdd") + "/";
                //文件上传后的保存路径
                string filePath = Server.MapPath("~" + path);
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                string fileName = Path.GetFileName(fileData.FileName);// 原始文件名称
                string fileExtension = Path.GetExtension(fileName); // 文件扩展名

                string saveName = Guid.NewGuid().ToString("N") + fileExtension;
                fileData.SaveAs(filePath + saveName);

                string returnFileName = path + saveName;

                BoolMessageItem<string> bm = new BoolMessageItem<string>("", true, returnFileName);
                return Json(bm);

            }
            catch (Exception ex)
            {
                BoolMessageItem<string> bm = new BoolMessageItem<string>("", false, ex.Message);
                return Json(bm);
            }
        }
        /// <summary>
        /// 获取用户信息列表
        /// </summary>
        /// <returns></returns>
        public JsonResult UserList()
        {
            string keyword = RequestHelper.GetVal("keyword");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string emails = RequestHelper.GetVal("emails");

            int total = 0;

            var list = _profileService.GetProfileList(out total, page, pageSize, keyword);
            string html = this._UserListHtml(list, emails);
            BoolMessageItem<int> bm = new BoolMessageItem<int>(total, true, html);

            return Json(bm);
        }
        /// <summary>
        /// get user emails for email select all
        /// </summary>
        /// <returns></returns>
        public JsonResult UserListSelectAll()
        {
            string keyword = RequestHelper.GetVal("keyword");
            int total = 0;
            var list = _profileService.GetProfileList(out total, 0, 0, keyword);

            List<string> rEmails = new List<string>();
            list.ForEach(e =>
            {
                rEmails.Add(e.Email);
            });

            BoolMessageItem<List<string>> bm = new BoolMessageItem<List<string>>(rEmails, true, "");

            return Json(bm);
        }
        /// <summary>
        /// 拼接html 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private string _UserListHtml(List<Profile> list, string email)
        {
            List<string> emails = email.Split(',').ToList();
            string html = "";

            list.ForEach(p =>
            {
                string imgUrl = _profileService.GetUserAvatar(p.UserId, 100);

                string badgeMember = "Badge Member";
                if (p.User.IsActivate == (int)Identifier.IsActivate.False)
                    badgeMember = "Not Badge";

                string popupBtn = "popup-btn-add";
                if (emails.Contains(p.Email))
                    popupBtn = "popup-btn-min";

                html += "<li class=\"popup-recipients-row\">";

                html += "<div class=\"popup-rcp-photo left\"><div class=\"popup-rcp-photo-container\"><img src=\"" + imgUrl + "\" /></div></div>";
                html += "<div class=\"popup-rcp-name left\"><p>" + p.Firstname + " " + p.Lastname + "</p></div>";
                html += "<div class=\"popup-rcp-email left\"><p>" + p.Email + "</p></div>";
                html += "<div class=\"popup-rcp-role left\"><p>" + badgeMember + "</p></div>";
                html += "<div class=\"popup-rcp-btn left\"><a class=\"" + popupBtn + "\" onclick=\"jEmail.Btn(this);\" email=\"" + p.Email + "\" ></a></div>";

                html += "</li>";
            });

            return html;
        }
        /// <summary>
        /// 发送测试邮件
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult EmailTestSend()
        {
            Email email = new Email();
           // email.TypeId = (int)Identifier.EmailType.email;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.Content = RequestHelper.GetVal("Content");
            email.EmailId = RequestHelper.GetValInt("EmailId");
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            string testEmail = RequestHelper.GetVal("testEmail");
            bool success = true;
            string msg = "";

            if (!string.IsNullOrEmpty(testEmail))
            {
                success = _adminEmailService.AdminEmailSendForTest(UserId, email.SenderEmail, email.Subject, email.Content, testEmail, email.EmailId.ToString(), email.Title, email.Logo, email.SenderName,(int)email.ShowMPB);
                if (!success)
                {
                    msg = "User not a member";
                }
            }
            else
            {
                success = false;
                msg = "Service error";
            }

            BoolMessageItem bm = new BoolMessageItem("", success, msg);

            return Json(bm);
        }

        /// <summary>
        /// 发送测试邮件
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult NewEmailTestSend()
        {
            Email email = new Email();
            // email.TypeId = (int)Identifier.EmailType.email;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.Content = RequestHelper.GetVal("Content");
            email.EmailId = RequestHelper.GetValInt("EmailId");
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            string testEmail = RequestHelper.GetVal("testEmail");
            bool success = true;
            string msg = "";

            if (!string.IsNullOrEmpty(testEmail))
            {
                success = _adminEmailService.NewAdminEmailSendForTest(UserId, email.SenderEmail, email.Subject, email.Content, testEmail, email.EmailId.ToString(), email.Title, email.Logo, email.SenderName, (int)email.ShowMPB);
                if (!success)
                {
                    msg = "User not a member";
                }
            }
            else
            {
                success = false;
                msg = "Service error";
            }

            BoolMessageItem bm = new BoolMessageItem("", success, msg);

            return Json(bm);
        }
        /// <summary>
        /// 预览邮件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ViewResult ViewEmail(int id = 0)
        {
            var ubt = _travelBadgeService.GetUserTravelbadge();
            ViewBag.ubt = ubt;
            Email email = _adminEmailService.Find(id);
            if (email != null)
                ViewBag.Email = email;
            else
                ViewBag.Email = new Email();

            //show email to the public without login
            if (!Request.IsAuthenticated)
            {
                return View("ViewEmailPublic");
            }
            if (ubt.User.IsActivate == (int)Identifier.IsActivate.True)
                return View("ViewEmailActivate");
            else
                return View("ViewEmailNotActivate");
        }

        /// <summary>
        /// Viewing New Email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ViewResult ViewNewEmail(int id = 0)
        {
            var ubt = _travelBadgeService.GetUserTravelbadge();
            ViewBag.ubt = ubt;
            Email email = _adminEmailService.Find(id);
            if (email != null)
                ViewBag.Email = email;
            else
                ViewBag.Email = new Email();

            //show email to the public without login
            if (!Request.IsAuthenticated)
            {
                return View("ViewNewEmailPublic");
            }
            if (ubt.User.IsActivate == (int)Identifier.IsActivate.True)
                return View("ViewNewEmailActivate");
            else
                return View("ViewNewEmailNotActivate");
        }

        #endregion

        #region Email Survay

        public ActionResult SurveyList()
        {
            this.PageTitle = "Survey Management";
            return View();
        }
        public ActionResult NewSurveyList()
        {
            this.PageTitle = "Survey Management";
            return View();
        }
        public JsonResult getSurveyListPage()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            int total;
            var list = _adminEmailService.GetSurveyList(page, pageSize, out total);
            string html = this._GetSurveyListPageHtml(list, page, pageSize);
            BoolMessageItem<int> bm = new BoolMessageItem<int>(total, true, html);

            return Json(bm);
        }
        public JsonResult getNewSurveyListPage()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            int total;
            var list = _adminEmailService.GetNewSurveyList(page, pageSize, out total);
            string html = this._GetNewSurveyListPageHtml(list, page, pageSize);
            BoolMessageItem<int> bm = new BoolMessageItem<int>(total, true, html);

            return Json(bm);
        }
        private string _GetSurveyListPageHtml(List<Email> list, int page, int pageSize)
        {
            int index = (page - 1) * pageSize + 1;
            string html = "";

            list.ForEach(e =>
            {
                html += "<li class=\"email-status-rows left\">";

                html += "<div class=\"email-stat-number left\"><p>" + index + ".</p></div>";
                html += "<div class=\"email-stat-campg-title left\"><p>" + e.Title + "</p></div>";
                html += "<div class=\"email-stat-blast left\"><p>" + e.CreateDate.Value.ToString("yyyy-MM-dd") + "</p></div>";
                html += "<div class=\"email-stat-recipients left\"><p>" + this.RecipientsTostring(e.Recipients.Value) + "</p></div>";
                html += "<div class=\"email-stat-created-by left\"><p>" + e.User.Profile.Firstname + " " + e.User.Profile.Lastname + "</p></div>";

                string sentStatusClass = "";
                string sentStatus = this.SentStatusTostring(e.SendStatus, out sentStatusClass);
                html += "<div class=\"email-stat-status left\">";
                html += "    <div class=\"email-stat-stauts-holder left\"><div class=\"email-stat-status-generate email-genrt-" + sentStatusClass + "\"></div></div>";
                html += "    <div class=\"email-stat-status-content left\"><span class=\"email-stat-status-report\">" + sentStatus + "</span></div>";
                html += "</div>";

                html += "<div class=\"email-stat-buttons left\">";
                html += "    <ul class=\"email-stat-buttons-container left\">";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/surveyupdate/" + e.EmailId + "\" class=\"actionsBtnEdit\" title=\"Edit Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/viewsurvey/" + e.EmailId + "\" class=\"actionsBtnPreview\" target=\"_blank\" title=\"View Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/surveyupdate/" + e.EmailId + "?duplicate=email\" class=\"actionsBtnMail\" title=\"Duplicate email\"></a></li>";
                html += "    </ul>";
                html += "    <div class=\"email-stat-btn-delete left\"><a class=\"emailDeleteBtn\" title=\"Delete Email\" onclick=\"jEmailList.Del(" + e.EmailId + ");\"></a></div>";
                html += "</div>";

                html += "</li>";

                index++;
            });

            return html;
        }

        private string _GetNewSurveyListPageHtml(List<Email> list, int page, int pageSize)
        {
            int index = (page - 1) * pageSize + 1;
            string html = "";

            list.ForEach(e =>
            {
                html += "<li class=\"email-status-rows left\">";

                html += "<div class=\"email-stat-number left\"><p>" + index + ".</p></div>";
                html += "<div class=\"email-stat-campg-title left\"><p>" + e.Title + "</p></div>";
                html += "<div class=\"email-stat-blast left\"><p>" + e.CreateDate.Value.ToString("yyyy-MM-dd") + "</p></div>";
                html += "<div class=\"email-stat-recipients left\"><p>" + this.RecipientsTostring(e.Recipients.Value) + "</p></div>";
                html += "<div class=\"email-stat-created-by left\"><p>" + e.User.Profile.Firstname + " " + e.User.Profile.Lastname + "</p></div>";

                string sentStatusClass = "";
                string sentStatus = this.SentStatusTostring(e.SendStatus, out sentStatusClass);
                html += "<div class=\"email-stat-status left\">";
                html += "    <div class=\"email-stat-stauts-holder left\"><div class=\"email-stat-status-generate email-genrt-" + sentStatusClass + "\"></div></div>";
                html += "    <div class=\"email-stat-status-content left\"><span class=\"email-stat-status-report\">" + sentStatus + "</span></div>";
                html += "</div>";

                html += "<div class=\"email-stat-buttons left\">";
                html += "    <ul class=\"email-stat-buttons-container left\">";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/newsurveyupdate/" + e.EmailId + "\" class=\"actionsBtnEdit\" title=\"Edit Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/viewnewsurvey/" + e.EmailId + "\" class=\"actionsBtnPreview\" target=\"_blank\" title=\"View Email\"></a></li>";
                html += "        <li class=\"actionsButton left\"><a href=\"/admin/home/newsurveyupdate/" + e.EmailId + "?duplicate=email\" class=\"actionsBtnMail\" title=\"Duplicate email\"></a></li>";
                html += "    </ul>";
                html += "    <div class=\"email-stat-btn-delete left\"><a class=\"emailDeleteBtn\" title=\"Delete Email\" onclick=\"jEmailList.Del(" + e.EmailId + ");\"></a></div>";
                html += "</div>";

                html += "</li>";

                index++;
            });

            return html;
        }
        public ActionResult SurveyUpdate(int id = 0)
        {
            this.PageTitle = "<a class='doubleArrow' href='/admin/home/surveyupdate'>Email Survey Management</a>";
            string pageTitle2 = "Create a Survey";

            string duplicate = RequestHelper.GetVal("duplicate");
            //string schedule = RequestHelper.GetVal("schedule");
            //ViewBag.EmailCampaignManagementURL = ;
            ViewBag.newEmail = 0;
            //= Request.Url.Host + Url.Action("emaillist", "home");

            SiteConfiguration _configuration = new SiteConfiguration(); ;
            ViewBag.SiteUrl = _configuration.SiteUrl;

            if (id > 0)
            {
                pageTitle2 = "Edit A Survey";
            }
            else
            {
                ViewBag.newEmail = 1;
            }
            Email returnEMail = _adminEmailService.Find(id);

            if (string.IsNullOrEmpty(duplicate))
            {
                ViewBag.Email = returnEMail;
            }
            else if (duplicate == "email")
            {
                pageTitle2 = "Create A Survey";
                Email email = new Email();
                email.SenderEmail = returnEMail.SenderEmail;
                email.Title = returnEMail.Title;
                email.SubTitle = returnEMail.SubTitle;
                email.Logo = returnEMail.Logo;
                email.Subject = returnEMail.Subject;
                email.Recipients = returnEMail.Recipients;
                email.RecipientsEmail = returnEMail.RecipientsEmail;
                email.SenderName = returnEMail.SenderName;
                email.TypeId = (int)Identifier.EmailType.survey;

                //var erere = returnEMail.Content.Split(new string[] { " (Le+|-(((w4+---====r-GQ "},StringSplitOptions.None);

                ViewBag.Email = email;
            }

            using (var _db = new IBTEntities())
            {
                int previousCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0);", id).FirstOrDefault();
                //SELECT TOP 1 [ScheduleTime] from [dbo].[EmailSendLog] where [EmailId] =11 AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 20) AND (DATEDIFF(DAY, [ScheduleTime], getdate()) >=0) order by [ScheduleTime] desc;
                if (previousCount > 0)
                {
                    var previousDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0)", id).First();
                    //DateTime previousDate = DateTime.Parse(previousDateVar);
                    //This email has been sent to 2 member(s) in last 7 day(s) with the latest date being on 4/5/2015 9:00 AM
                    ViewBag.sentEmailMsg = string.Format("This email has been sent to {0} member(s) in 7 day(s) with the latest date being on {1}", previousCount, previousDate.ToString());
                }

                int nextCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                if (nextCount > 0)
                {
                    var nextDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                    ViewBag.nextEmailMsg = string.Format("This email has been scheduled to be sent on the {0} to {1} member(s)", nextDate.ToString(), nextCount);
                }
            }

            this.PageTitle2 = pageTitle2;
            return View();
        }

        public ActionResult NewSurveyUpdate(int id = 0)
        {
            this.PageTitle = "<a class='doubleArrow' href='/admin/home/newsurveyupdate'>Email Survey Management</a>";
            string pageTitle2 = "Create a Survey";

            string duplicate = RequestHelper.GetVal("duplicate");
            //string schedule = RequestHelper.GetVal("schedule");
            //ViewBag.EmailCampaignManagementURL = ;
            ViewBag.newEmail = 0;
            //= Request.Url.Host + Url.Action("emaillist", "home");

            SiteConfiguration _configuration = new SiteConfiguration(); 
            ViewBag.SiteUrl = _configuration.SiteUrl;

            if (id > 0)
            {
                pageTitle2 = "Edit A Survey";
            }
            else
            {
                ViewBag.newEmail = 1;
            }
            Email returnEMail = _adminEmailService.Find(id);

            if (string.IsNullOrEmpty(duplicate))
            {
                ViewBag.Email = returnEMail;
            }
            else if (duplicate == "email")
            {
                pageTitle2 = "Create A Survey";
                Email email = new Email();
                email.SenderEmail = returnEMail.SenderEmail;
                email.Title = returnEMail.Title;
                email.SubTitle = returnEMail.SubTitle;
                email.Logo = returnEMail.Logo;
                email.Subject = returnEMail.Subject;
                email.Recipients = returnEMail.Recipients;
                email.RecipientsEmail = returnEMail.RecipientsEmail;
                email.SenderName = returnEMail.SenderName;
                email.TypeId = (int)Identifier.EmailType.newsurvey;

                //var erere = returnEMail.Content.Split(new string[] { " (Le+|-(((w4+---====r-GQ "},StringSplitOptions.None);

                ViewBag.Email = email;
            }

            using (var _db = new IBTEntities())
            {
                int previousCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0);", id).FirstOrDefault();
                //SELECT TOP 1 [ScheduleTime] from [dbo].[EmailSendLog] where [EmailId] =11 AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 20) AND (DATEDIFF(DAY, [ScheduleTime], getdate()) >=0) order by [ScheduleTime] desc;
                if (previousCount > 0)
                {
                    var previousDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(DAY, [ScheduleTime], getdate()) <= 7) AND (DATEDIFF(minute, [ScheduleTime], getdate()) >=0)", id).First();
                    //DateTime previousDate = DateTime.Parse(previousDateVar);
                    //This email has been sent to 2 member(s) in last 7 day(s) with the latest date being on 4/5/2015 9:00 AM
                    ViewBag.sentEmailMsg = string.Format("This email has been sent to {0} member(s) in 7 day(s) with the latest date being on {1}", previousCount, previousDate.ToString());
                }

                int nextCount = _db.Database.SqlQuery<int>("SELECT COUNT(*) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                if (nextCount > 0)
                {
                    var nextDate = _db.Database.SqlQuery<DateTime>("SELECT MAX([ScheduleTime]) from [dbo].[EmailSendLog] where [EmailId] ={0} AND (DATEDIFF(minute, getdate(), [ScheduleTime]) > 0);", id).First();
                    ViewBag.nextEmailMsg = string.Format("This email has been scheduled to be sent on the {0} to {1} member(s)", nextDate.ToString(), nextCount);
                }
            }

            this.PageTitle2 = pageTitle2;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public JsonResult SurveySave()
        {
            Email email = new Email();
            email.TypeId = (int)Identifier.EmailType.survey;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.ShowMPB = RequestHelper.GetValInt("MPB");            

            //get the survey content
            string Content1 = RequestHelper.GetVal("surveyContent1");
            string Content2 = RequestHelper.GetVal("surveyContent2");
            string SuggestedList = RequestHelper.GetVal("SuggestedListHtml").Trim();

            //add all content and suggestedList to Email Content
            email.Content = Content1 + " (Le+|-(((w4+---====r-GQ " + Content2 + " (Le+|-(((w4+---====r-GQ " + SuggestedList.Trim();

            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;

            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            var bm = new BoolMessageItem<int>(email.EmailId, true, "");

            return Json(bm);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public JsonResult NewSurveySave()
        {
            Email email = new Email();
            email.TypeId = (int)Identifier.EmailType.newsurvey;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.ShowMPB = RequestHelper.GetValInt("MPB");
            //email.Content = RequestHelper.GetVal("Content");

            //get the survey content
            string Content1 = RequestHelper.GetVal("surveyContent1");
            string Content2 = RequestHelper.GetVal("surveyContent2");
            string Content3 = RequestHelper.GetVal("surveyContent3");
            string SuggestedList = RequestHelper.GetVal("SuggestedListHtml").Trim();

            //add all content and suggestedList to Email Content
            email.Content = Content1 + " (Le+|-(((w4+---====r-GQ " + Content2 + " (Le+|-(((w4+---====r-GQ " + Content3 + " (Le+|-(((w4+---====r-GQ " + SuggestedList.Trim();

            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;

            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Achive;
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            var bm = new BoolMessageItem<int>(email.EmailId, true, "");

            return Json(bm);
        }
        
        [ValidateInput(false)]
        public JsonResult SurveyConfirm()
        {
            Email email = new Email();
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.ScheduleStatus = RequestHelper.GetValInt("ScheduleStatus");
            email.TypeId = (int)Identifier.EmailType.survey;
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            //get the survey content
            string Content1 = RequestHelper.GetVal("surveyContent1");
            string Content2 = RequestHelper.GetVal("surveyContent2");            
            string SuggestedList = RequestHelper.GetVal("SuggestedListHtml");

            //add all content and suggestedList to Email Content
            email.Content = Content1 + " (Le+|-(((w4+---====r-GQ " + Content2 + " (Le+|-(((w4+---====r-GQ " + SuggestedList.Trim();

            if (email.ScheduleStatus == (int)Identifier.EmailScheduleStatus.Immediate)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                email.ScheduleTime = DateTime.Now;
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime", DateTime.Now);
            }
            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;

            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            _adminEmailService.AddEmailSendLogTreading(email.EmailId);
            var bm = new BoolMessageItem<int>(email.EmailId, true, "");
            return Json(bm);
        }

        [ValidateInput(false)]
        public JsonResult NewSurveyConfirm()
        {
            Email email = new Email();
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.ScheduleStatus = RequestHelper.GetValInt("ScheduleStatus");
            email.TypeId = (int)Identifier.EmailType.newsurvey;
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            //get the survey content
            string Content1 = RequestHelper.GetVal("surveyContent1");
            string Content2 = RequestHelper.GetVal("surveyContent2");
            string Content3 = RequestHelper.GetVal("surveyContent3");
            string SuggestedList = RequestHelper.GetVal("SuggestedListHtml");

            //add all content and suggestedList to Email Content
            email.Content = Content1 + " (Le+|-(((w4+---====r-GQ " + Content2 + " (Le+|-(((w4+---====r-GQ " + Content3 + " (Le+|-(((w4+---====r-GQ " + SuggestedList.Trim();

            if (email.ScheduleStatus == (int)Identifier.EmailScheduleStatus.Immediate)
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                email.ScheduleTime = DateTime.Now;
            }
            else
            {
                email.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime", DateTime.Now);
            }
            //email.ScheduleTime = RequestHelper.GetValDateTime("ScheduleTime",null);
            email.UserId = UserId;

            int id = RequestHelper.GetValInt("EmailId");

            if (id > 0)
            {
                email = _adminEmailService.Update(email, id);
            }
            else
            {
                email.CreateDate = DateTime.Now;
                email.StatusId = (int)Identifier.StatusType.Enabled;
                _adminEmailService.Add(email);
            }

            _adminEmailService.AddEmailSendLogTreading(email.EmailId);
           
            var bm = new BoolMessageItem<int>(email.EmailId, true, "");
            return Json(bm);
        }


        [ValidateInput(false)]
        public JsonResult SurveyTestSend()
        {
            Email email = new Email();
            email.TypeId = (int)Identifier.EmailType.survey;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            //get the survey content
            string Content1 = RequestHelper.GetVal("surveyContent1");
            string Content2 = RequestHelper.GetVal("surveyContent2");
            string SuggestedList = RequestHelper.GetVal("SuggestedListHtml").Trim();
            string SuggestedList1 = RequestHelper.GetVal("SuggestedList").Trim();           
            email.EmailId = RequestHelper.GetValInt("EmailId");
            string testEmail = RequestHelper.GetVal("testEmail");
            bool success = true;
            string msg = "";           

            if (!string.IsNullOrEmpty(testEmail))
            {
                success = _adminEmailService.AdminSurveySendForTest(UserId, email.SenderEmail, email.Subject, Content1, SuggestedList, Content2, testEmail, email.EmailId.ToString(), email.Title, email.Logo, email.SenderName , (int) email.ShowMPB);
                
                if (!success)
                {
                    msg = "User not a member";
                }
            }
            else
            {
                success = false;
                msg = "Service error";
            }

            BoolMessageItem bm = new BoolMessageItem("", success, msg);

            return Json(bm);
        }


        [ValidateInput(false)]
        public JsonResult NewSurveyTestSend()
        {
            Email email = new Email();
            email.TypeId = (int)Identifier.EmailType.newsurvey;
            email.SenderName = RequestHelper.GetVal("SenderName");
            email.SenderEmail = RequestHelper.GetVal("SenderEmail");
            email.Title = RequestHelper.GetVal("Title");
            email.SubTitle = RequestHelper.GetVal("SubTitle");
            email.Logo = RequestHelper.GetVal("Logo");
            email.Subject = RequestHelper.GetVal("Subject");
            email.Recipients = RequestHelper.GetValInt("Recipients");
            email.RecipientsEmail = RequestHelper.GetVal("RecipientsEmail");
            email.ShowMPB = RequestHelper.GetValInt("MPB");

            //get the survey content
            string Content1 = RequestHelper.GetVal("surveyContent1") + RequestHelper.GetVal("surveyContent2");
            string Content2 = RequestHelper.GetVal("surveyContent3");
            string SuggestedList = RequestHelper.GetVal("SuggestedListHtml").Trim();


            email.EmailId = RequestHelper.GetValInt("EmailId");
            string testEmail = RequestHelper.GetVal("testEmail");
            bool success = true;
            string msg = "";

            if (!string.IsNullOrEmpty(testEmail))
            {
                success = _adminEmailService.NewAdminSurveySendForTest(UserId, email.SenderEmail, email.Subject, Content1, SuggestedList, Content2, testEmail, email.EmailId.ToString(), email.Title, email.Logo, email.SenderName, (int)email.ShowMPB);

                if (!success)
                {
                    msg = "User not a member";
                }
            }
            else
            {
                success = false;
                msg = "Service error";
            }

            BoolMessageItem bm = new BoolMessageItem("", success, msg);

            return Json(bm);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getCountriesList()
        {

            IBTEntities e = new IBTEntities();
            var countryLst = e.Countries.Select(c => new { c.CountryId, c.CountryName, c.FlagSrc }).OrderBy(c => c.CountryName).ToList().ToArray();

            BoolMessageItem bm = new BoolMessageItem(countryLst, true, "you have data");
            return Json(bm);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getCitiesList(object ppObj)
        {
            BoolMessageItem bm;
            int CountryId = int.Parse(RequestHelper.GetVal("countryId"));
            if (CountryId > 0)
            {
                IBTEntities e = new IBTEntities();
                var citiesLst = e.Cities.Where(c => c.CountryId == CountryId).Select(c => new { c.CityId, c.CityName }).OrderBy(c => c.CityName).ToList().ToArray();

                bm = new BoolMessageItem(citiesLst, true, "you have data");
            }
            else
            {
                bm = new BoolMessageItem(null, false, "No data");
            }
            return Json(bm);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getPlacesList(object ppObj)
        {
            BoolMessageItem bm;
            int CityId = int.Parse(RequestHelper.GetVal("cityId"));
            if (CityId > 0)
            {
                IBTEntities en = new IBTEntities();
                var PlacesLst = en.Supplies.Where(s => s.CityId == CityId).Select(s => new { s.SupplyId, s.Title }).OrderBy(s => s.Title).ToList().ToArray();

                bm = new BoolMessageItem(PlacesLst, true, "you have data");
            }
            else
            {
                bm = new BoolMessageItem(null, false, "No data");
            }
            return Json(bm);
        }

        public ActionResult ViewSurvey(string id)
        { 
            AccountService aService = new AccountService();
            //IBTEntities e = new IBTEntities();
            Services.SupplierService ss = new SupplierService();
            INewsFeedService _newsFeedService = ObjectFactory.GetInstance<INewsFeedService>();
            List<List<string>> optionsArrList = new List<List<string>>();

            var userIdEncrypted = RequestHelper.GetVal("uid");
            //ViewBag.userIdEncrypted = userIdEncrypted;
            string options = RequestHelper.GetVal("option");
            string eMailIDVal = RequestHelper.GetVal("eid");
            int eMailID = 0;

            var UrlDecoded = userIdEncrypted;//HttpUtility.UrlDecode(userIdEncrypted);
            //in GET request doesn't need to decode the request
            if (System.Web.HttpContext.Current.Request.HttpMethod.ToString() != "GET")
            {
               UrlDecoded= HttpUtility.UrlDecode(userIdEncrypted);
            }

            var _userIDtemp = Common.Encrypt.EncodeHelper.AES_Decrypt(UrlDecoded);
            int _userID =-1;
            if (UserId>0)
            {
                // When the user is already loged in
                _userID = UserId;
            }
            else
            {
                int.TryParse(_userIDtemp, out _userID);
            }
            int emailId = 0;
            if (int.TryParse(id, out emailId) && emailId > 0)
            {
                eMailIDVal = emailId.ToString();
            }
           

            if (!((_userID >= 0) && int.TryParse(eMailIDVal, out eMailID)))
            {
                //there is an error, no userID or emailID here
                 return Redirect("/home/index");
            }


            //Encrypt and Encode the userId
            ViewBag.userIdEncrypted = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt(_userID.ToString()));

            var _emailObj = aService._db.Emails.Where(e => e.EmailId == eMailID && e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.survey).FirstOrDefault();
            ViewBag.emailObj = _emailObj;

            
            if (!string.IsNullOrWhiteSpace(options) && _userID >0)
            {
                var valuesLst = options.Split(',');

                foreach (var item in valuesLst)
                {
                    List<string> tempList = new List<string>();
                    //var _item = Common.Encrypt.EncodeHelper.AES_Decrypt(HttpUtility.UrlDecode(item));
                    string indicator = item.Substring(0, 2).ToString();
                    int indicatorID = int.Parse(item.Substring(2).ToString());
                    if (indicator == "pl" && indicatorID > 0)
                    {
                        var placeObj = aService._db.Supplies.Find(indicatorID);
                        DateTime? pvDate = null;
                        string firstName = _profileService.GetUserProfile(_userID).Firstname;
                        
                        string strmsg = firstName + " has posted a visit here.";
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Country, placeObj.CountryId, 0, 0, pvDate);
                        ss.PostVisted(_userID, (int)Identifier.IBTType.City, placeObj.CountryId, placeObj.CityId, 0, pvDate);
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Supply, placeObj.CountryId, placeObj.CityId, placeObj.SupplyId, pvDate);
                        _newsFeedService.AddNewsFeed(_userID, placeObj.CountryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        _newsFeedService.AddNewsFeed(_userID, placeObj.CountryId, placeObj.CityId, 0, Identifier.IBTType.City, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        _newsFeedService.AddNewsFeed(_userID, placeObj.CountryId, placeObj.CityId, placeObj.SupplyId, Identifier.IBTType.Supply, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);

                        ss.IBeenThere(_userID, placeObj.CountryId, 0, 0, strmsg);
                        ss.IBeenThere(_userID, placeObj.CountryId, placeObj.CityId, 0, strmsg);
                        ss.IBeenThere(_userID, placeObj.CountryId, placeObj.CityId, placeObj.SupplyId, strmsg);

                        var countryObj = aService._db.Countries.Find(placeObj.CountryId);
                        var cityObj = aService._db.Cities.Find(placeObj.CityId);

                        tempList.Add("place");
                        tempList.Add(countryObj.FlagSrc);
                        tempList.Add(countryObj.CountryId.ToString());
                        tempList.Add(countryObj.CountryName);
                        tempList.Add(cityObj.CityId.ToString());
                        tempList.Add(cityObj.CityName);
                        tempList.Add(placeObj.SupplyId.ToString());
                        tempList.Add(placeObj.Title);
                    }
                    else if (indicator == "ci" && indicatorID > 0)
                    {
                        var cityObj = aService._db.Cities.Find(indicatorID);
                        DateTime? pvDate = null;
                        string firstName = _profileService.GetUserProfile(_userID).Firstname;
                        
                        string strmsg = firstName + " has posted a visit here.";
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Country, cityObj.CountryId, 0, 0, pvDate);
                        ss.PostVisted(_userID, (int)Identifier.IBTType.City, cityObj.CountryId, cityObj.CityId, 0, pvDate);
                        _newsFeedService.AddNewsFeed(_userID, cityObj.CountryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        _newsFeedService.AddNewsFeed(_userID, cityObj.CountryId, cityObj.CityId, 0, Identifier.IBTType.City, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);

                        ss.IBeenThere(_userID, cityObj.CountryId, 0, 0, strmsg);
                        ss.IBeenThere(_userID, cityObj.CountryId, cityObj.CityId, 0, strmsg);

                        var countryObj = aService._db.Countries.Find(cityObj.CountryId);
                        tempList.Add("city");
                        tempList.Add(countryObj.FlagSrc);
                        tempList.Add(countryObj.CountryId.ToString());
                        tempList.Add(countryObj.CountryName);
                        tempList.Add(cityObj.CityId.ToString());
                        tempList.Add(cityObj.CityName);
                    }
                    else if (indicator == "co" && indicatorID > 0)
                    {
                        var countryObj = aService._db.Countries.Find(indicatorID);
                        DateTime? pvDate = null;
                        string firstName = _profileService.GetUserProfile(_userID).Firstname;
                        
                        string strmsg = firstName + " has posted a visit here.";
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Country, countryObj.CountryId, 0, 0, pvDate);
                        _newsFeedService.AddNewsFeed(_userID, countryObj.CountryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        ss.IBeenThere(_userID, countryObj.CountryId, 0, 0, strmsg);

                        tempList.Add("country");
                        tempList.Add(countryObj.FlagSrc);
                        tempList.Add(countryObj.CountryId.ToString());
                        tempList.Add(countryObj.CountryName);
                    }
                    else
                    {
                        continue;
                    }
                    optionsArrList.Add(tempList);
                }
            }
            else
            {
                //shows the places to mark them
                var SurveyContent = _emailObj.Content.Split(new string[] { " (Le+|-(((w4+---====r-GQ " }, StringSplitOptions.None);
                AdminEmailService adminEmailS = new AdminEmailService();
                ViewBag.Content1 = SurveyContent[0].ToString();
                ViewBag.Content2 = SurveyContent[1].ToString();
                ViewBag.placesList = adminEmailS.SugestedList2EmailHtmlTemplate(SurveyContent[2]);

                if (_userID<=0)
                {
                    return View("ViewSurveyPublic");
                }

                if (_userID>0)
                {
                    ViewBag.UserName = aService._db.Users.Find(_userID).Profile.Firstname;    
                }
                return View("ViewSurveyMark");
            }
         
            ViewBag.optionsArrList = optionsArrList;
            return View();
        }
        public ActionResult ViewNewSurvey(string id)
        {
            AccountService aService = new AccountService();
            //IBTEntities e = new IBTEntities();
            Services.SupplierService ss = new SupplierService();
            INewsFeedService _newsFeedService = ObjectFactory.GetInstance<INewsFeedService>();
            List<List<string>> optionsArrList = new List<List<string>>();

            var userIdEncrypted = RequestHelper.GetVal("uid");
            //ViewBag.userIdEncrypted = userIdEncrypted;
            string options = RequestHelper.GetVal("option");
            string eMailIDVal = RequestHelper.GetVal("eid");
            int eMailID = 0;

            var UrlDecoded = userIdEncrypted;//HttpUtility.UrlDecode(userIdEncrypted);
            //in GET request doesn't need to decode the request
            if (System.Web.HttpContext.Current.Request.HttpMethod.ToString() != "GET")
            {
                UrlDecoded = HttpUtility.UrlDecode(userIdEncrypted);
            }

            var _userIDtemp = Common.Encrypt.EncodeHelper.AES_Decrypt(UrlDecoded);
            int _userID = -1;
            if (UserId > 0)
            {
                // When the user is already loged in
                _userID = UserId;
            }
            else
            {
                int.TryParse(_userIDtemp, out _userID);
            }
            int emailId = 0;
            if (int.TryParse(id, out emailId) && emailId > 0)
            {
                eMailIDVal = emailId.ToString();
            }


            if (!((_userID >= 0) && int.TryParse(eMailIDVal, out eMailID)))
            {
                //there is an error, no userID or emailID here
                return Redirect("/home/index");
            }


            //Encrypt and Encode the userId
            ViewBag.userIdEncrypted = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt(_userID.ToString()));

            var _emailObj = aService._db.Emails.Where(e => e.EmailId == eMailID && e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.newsurvey).FirstOrDefault();
            ViewBag.emailObj = _emailObj;


            if (!string.IsNullOrWhiteSpace(options) && _userID > 0)
            {
                var valuesLst = options.Split(',');

                foreach (var item in valuesLst)
                {
                    List<string> tempList = new List<string>();
                    //var _item = Common.Encrypt.EncodeHelper.AES_Decrypt(HttpUtility.UrlDecode(item));
                    string indicator = item.Substring(0, 2).ToString();
                    int indicatorID = int.Parse(item.Substring(2).ToString());
                    if (indicator == "pl" && indicatorID > 0)
                    {
                        var placeObj = aService._db.Supplies.Find(indicatorID);
                        DateTime? pvDate = null;
                        string firstName = _profileService.GetUserProfile(_userID).Firstname;

                        string strmsg = firstName + " has posted a visit here.";
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Country, placeObj.CountryId, 0, 0, pvDate);
                        ss.PostVisted(_userID, (int)Identifier.IBTType.City, placeObj.CountryId, placeObj.CityId, 0, pvDate);
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Supply, placeObj.CountryId, placeObj.CityId, placeObj.SupplyId, pvDate);
                        _newsFeedService.AddNewsFeed(_userID, placeObj.CountryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        _newsFeedService.AddNewsFeed(_userID, placeObj.CountryId, placeObj.CityId, 0, Identifier.IBTType.City, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        _newsFeedService.AddNewsFeed(_userID, placeObj.CountryId, placeObj.CityId, placeObj.SupplyId, Identifier.IBTType.Supply, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);

                        ss.IBeenThere(_userID, placeObj.CountryId, 0, 0, strmsg);
                        ss.IBeenThere(_userID, placeObj.CountryId, placeObj.CityId, 0, strmsg);
                        ss.IBeenThere(_userID, placeObj.CountryId, placeObj.CityId, placeObj.SupplyId, strmsg);

                        var countryObj = aService._db.Countries.Find(placeObj.CountryId);
                        var cityObj = aService._db.Cities.Find(placeObj.CityId);

                        tempList.Add("place");
                        tempList.Add(countryObj.FlagSrc);
                        tempList.Add(countryObj.CountryId.ToString());
                        tempList.Add(countryObj.CountryName);
                        tempList.Add(cityObj.CityId.ToString());
                        tempList.Add(cityObj.CityName);
                        tempList.Add(placeObj.SupplyId.ToString());
                        tempList.Add(!string.IsNullOrEmpty(placeObj.CoverPhotoSrc) ? placeObj.CoverPhotoSrc : countryObj.FlagSrc);//Added by Dev
                        
                        tempList.Add(placeObj.Title);
                    }
                    else if (indicator == "ci" && indicatorID > 0)
                    {
                        var cityObj = aService._db.Cities.Find(indicatorID);
                        DateTime? pvDate = null;
                        string firstName = _profileService.GetUserProfile(_userID).Firstname;

                        string strmsg = firstName + " has posted a visit here.";
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Country, cityObj.CountryId, 0, 0, pvDate);
                        ss.PostVisted(_userID, (int)Identifier.IBTType.City, cityObj.CountryId, cityObj.CityId, 0, pvDate);
                        _newsFeedService.AddNewsFeed(_userID, cityObj.CountryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        _newsFeedService.AddNewsFeed(_userID, cityObj.CountryId, cityObj.CityId, 0, Identifier.IBTType.City, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);

                        ss.IBeenThere(_userID, cityObj.CountryId, 0, 0, strmsg);
                        ss.IBeenThere(_userID, cityObj.CountryId, cityObj.CityId, 0, strmsg);

                        var countryObj = aService._db.Countries.Find(cityObj.CountryId);
                        tempList.Add("city");
                        tempList.Add(countryObj.FlagSrc);
                        tempList.Add(countryObj.CountryId.ToString());
                        tempList.Add(countryObj.CountryName);
                        tempList.Add(cityObj.CityId.ToString());
                        tempList.Add(!string.IsNullOrEmpty(cityObj.FlagSrc) ? cityObj.FlagSrc : countryObj.FlagSrc);//Added by Dev                        
                        tempList.Add(cityObj.CityName);
                    }
                    else if (indicator == "co" && indicatorID > 0)
                    {
                        var countryObj = aService._db.Countries.Find(indicatorID);
                        DateTime? pvDate = null;
                        string firstName = _profileService.GetUserProfile(_userID).Firstname;

                        string strmsg = firstName + " has posted a visit here.";
                        ss.PostVisted(_userID, (int)Identifier.IBTType.Country, countryObj.CountryId, 0, 0, pvDate);
                        _newsFeedService.AddNewsFeed(_userID, countryObj.CountryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                        ss.IBeenThere(_userID, countryObj.CountryId, 0, 0, strmsg);

                        tempList.Add("country");
                        tempList.Add(countryObj.FlagSrc);
                        tempList.Add(countryObj.CountryId.ToString());
                        tempList.Add(countryObj.CountryName);
                    }
                    else
                    {
                        continue;
                    }
                    optionsArrList.Add(tempList);
                }
            }
            else
            {
                //shows the places to mark them
                var SurveyContent = _emailObj.Content.Split(new string[] { " (Le+|-(((w4+---====r-GQ " }, StringSplitOptions.None);
                AdminEmailService adminEmailS = new AdminEmailService();
                ViewBag.Content1 = SurveyContent[0].ToString();
                ViewBag.Content2 = SurveyContent[1].ToString();
                ViewBag.Content3 = SurveyContent[2].ToString();
                ViewBag.placesList = adminEmailS.NewSugestedList2EmailHtmlTemplate(SurveyContent[3]);

                if (_userID <= 0)
                {
                    return View("ViewNewSurveyPublic");
                }

                if (_userID > 0)
                {
                    ViewBag.UserName = aService._db.Users.Find(_userID).Profile.Firstname;
                }
                return View("ViewNewSurveyMark");
            }

            ViewBag.optionsArrList = optionsArrList;
            return View();
        }
        [HttpPost]
        public JsonResult RatingSurvey(FormCollection collection)
        {
            BoolMessageItem bm;
            string rowData = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            var data = rowData.Split('&');
            var userIdencrypt = RequestHelper.GetVal("userid");
            var _userIDtemp = Common.Encrypt.EncodeHelper.AES_Decrypt(HttpUtility.UrlDecode(userIdencrypt));
            int _userID = int.Parse(_userIDtemp);
            if (_userID <=0)
            {
                //there is an error in here
                bm = new BoolMessageItem<string>("", false,"There is an error!!");
                return Json(bm);
                //return Redirect("http://www.ive-been-there.com");
            }
            //_supplierService.RRWB(83443, Identifier.PostTypes.PostVisit, Identifier.IBTType.Supply, 203659, Identifier.RRWBType.Rating, 5);

            for (int i = 0; i < data.Length; i++)
            {
                var item = data[i].Split('=');
                if (item[0].Substring(0, 6) == "rating")
                {
                    Identifier.IBTType ibtType;
                    string indicator = item[0].Substring(7, 2);
                    int indicatorID = int.Parse(item[0].Substring(9));
                    int ratingValue = int.Parse(item[1].ToString());

                    if (indicator == "co")
                    {
                        ibtType = Identifier.IBTType.Country;
                    }
                    else if (indicator == "ci")
                    {
                        ibtType = Identifier.IBTType.City;
                    }
                    else if (indicator == "pl")
                    {
                        ibtType = Identifier.IBTType.Supply;
                    }
                    else
                    {
                        continue;
                    }
                    _supplierService.RRWB(_userID, Identifier.PostTypes.PostVisit, ibtType, indicatorID, Identifier.RRWBType.Rating, ratingValue);
                }
            }

            bm = new BoolMessageItem<string>("", true, "");
            return Json(bm);

          //  return Redirect("/Account/PointSummary?key=" + userIdencrypt);
        }

        #endregion

        public string test()
        {
           // Services.AdminEmailService objjd = new AdminEmailService();
           // objjd.SendEmail();

            string browser = Request.Browser.Browser;
            return browser;
        }
        //static void WriteLog(string message)
        //{
        //    try
        //    {
        //        string logFileName = "D:\\log\\ErrorLog(" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ").txt";

        //        if (!Directory.Exists("D:\\log"))
        //        {
        //            Directory.CreateDirectory("D:\\log");
        //        }

        //        if (!  System.IO.File.Exists(logFileName))
        //        {
        //            using (StreamWriter sw = System.IO.File.CreateText(logFileName))
        //            {
        //                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + message);
        //            }
        //        }
        //        else
        //        {
        //            using (StreamWriter sw = System.IO.File.AppendText(logFileName))
        //            {
        //                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + message);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
              
        //    }
        //}
    }
}
