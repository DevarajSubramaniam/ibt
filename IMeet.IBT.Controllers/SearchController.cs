﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Extensions;
using IMeet.IBT.Common.Helpers;

using IMeet.IBT.DAL;

namespace IMeet.IBT.Controllers
{
    /// <summary>
    /// 页面search的操作时用
    /// </summary>
    public class SearchController : BaseController
    {
        private ISearchService _searchService;
        private ISupplierService _supplierService;
        public SearchController(ISearchService searchService, ISupplierService supplierService)
        {
            _searchService = searchService;
            _supplierService = supplierService;
        }


        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string keyword)
        {
            this.Title = "Search";
            this.Keywords = "keywords";
            this.Description = "description";
            ViewBag.keyword = keyword;
            //int searchNum = _searchService.GetSearchResultListCount(keyword);
            //ViewBag.SearchNum = searchNum.ToString();
            //todo: index 
            //int membersNum = _searchService.GetAllMemberCount(keyword);
            //ViewBag.MembersNum = membersNum.ToString();
            //int supplyNum = _searchService.GetAllSupplierCount(UserId, keyword);
            //ViewBag.SupplyNum = supplyNum.ToString();
            return View();
        }
        public JsonResult GetAllMemberCount()
        {
            int itemTotal = 0;
            string baseKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            if (baseKeyword == "POST A VISIT HERE!  Find, Rate and ADD to your Travels - Countries, Cities, Hotels, Venues and More")
                baseKeyword = "";
            int membersNum = _searchService.GetAllMemberCount(baseKeyword);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, membersNum.ToString()), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllSupplierCount()
        {
            int itemTotal = 0;
            string baseKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            if (baseKeyword == "POST A VISIT HERE! Find, Rate and ADD to your Travels - Countries, Cities, Hotels, Venues and More")
                baseKeyword = "";
            int supplyNum = _searchService.GetAllSupplierCount(UserId, baseKeyword);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, supplyNum.ToString()), JsonRequestBehavior.AllowGet);
        }

        #region Browse By Country

        /// <summary>
        /// Browse By Country
        /// </summary>
        /// <returns></returns>
        public ActionResult BrowseByCountry(string keyword)
        {
            this.Title = "Browse By Country";
            this.Keywords = "keywords";
            this.Description = "description";
            return View();
        }

        /// <summary>
        /// Browse data
        /// </summary>
        /// <returns></returns>
        public JsonResult BrowseListAjx()
        {
            int userId = RequestHelper.GetValInt("userId", UserId);
            //int userId =Convert.ToInt32( ViewData["userId"]);
            int type = RequestHelper.GetValInt("type", 1);
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterLetter = RequestHelper.GetVal("saveLetter");
            string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            if (filterKeyword == "POST A VISIT HERE!  Find, Rate and ADD to your Travels - Countries, Cities, Hotels, Venues and More")
                filterKeyword = "";
            string strWhere = "";
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = BrowseListData(type, userId, page, pageSize, strWhere, filterLetter, filterKeyword, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Browse by Country List
        /// </summary>
        /// <returns></returns>
        private string BrowseListData(int type, int userId, int page, int pageSize, string strWhere, string filterLetter, string filterKeyword, out int total, out int itemTotal)
        {
            IList<CountryListItem> browseList = new List<CountryListItem>();

            browseList = _searchService.GetBrowseList(userId, page, pageSize, strWhere, filterLetter, filterKeyword, out total);
            //browseList = _supplierService.FeedUserActivtieLists(userId, page, pageSize);


            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            itemTotal = browseList.Count;
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in browseList)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"1\" data-objId=\"" + item.ObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                //ibt I've been there
                sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Country, item.CountryId) + "\" class=\"btnBlueH45\" >");
                if (item.IbtCount < 1)
                    sb.Append("     <span class=\"bg\">I've been there</span>");
                else
                    sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"/Country/NewsFeed/1/" + item.ObjId.ToString() + "\"><img alt=\"\" src=\"" + item.FlagSrc + "\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"/Country/NewsFeed/1/" + item.ObjId.ToString() + "\" class=\"f15\">" + item.CountryName + "</a>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 

                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, 0, 0);
                if (friendCount > 0)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: <a href='javascript:void(0);' onclick=\"ibt.loadUrlPop('/ShowFriend/1/" + item.CountryId + "','Get','',null,' " + friendCount + " Friends Who\\'ve Been There');\"> " + friendCount + "</a></span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: 0</span>");
                }

                //showNick = type == 1 ? "You " : "";
                //friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, 0, 0);
                //strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //selfId = _supplierService.GetIBTUser(UserId, 0, item.CountryId, 0).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (friendCount > 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/1/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/1/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}

                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose\">");
                uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"1\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                sb.Append("          </span>");
                uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + "\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"1\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                sb.Append("          </span>");
                uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + "\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"1\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                sb.Append("          </span>");
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");
            }
            return sb.ToString();
        }

        #endregion

        #region Base Search

        #region Country Search of data
        /// <summary>
        /// Country data
        /// </summary>
        /// <returns></returns>
        public JsonResult CountryResultList()
        {
            int topcount = 1;
            int itemTotal = 0;
            string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");


            var list = CountryResultListData(topcount, baseKeyword, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回CountryResultList的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string CountryResultListData(int topcount, string baseKeyword, out int itemTotal)
        {
            var list = _searchService.GetBrowseList(UserId, 1, topcount, "", "", baseKeyword, out itemTotal);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append(" <dl>");
                sb.Append("     <dt><a href=\"/Country/NewsFeed/1/" + item.CountryId.ToString() + "\"><img alt=\"\" src=\"" + item.FlagSrc + "\"></a></dt>");
                sb.Append("       <dd><a href=\"/Country/NewsFeed/1/" + item.CountryId.ToString() + "\"> <strong>" + item.CountryName + "</strong> </a></dd>");
                sb.Append("       <dd>Country</dd>");
                sb.Append("</dl>");

            }
            return sb.ToString();
        }
        #endregion

        #region City Search of data
        /// <summary>
        /// City data
        /// </summary>
        /// <returns></returns>
        public JsonResult CityResultList()
        {
            int topcount = 3;
            int itemTotal = 0;
            string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");


            var list = CityResultListData(topcount, baseKeyword, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回CityResultList的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string CityResultListData(int topcount, string baseKeyword, out int itemTotal)
        {
            var list = _searchService.GetCityResultList(UserId, 1, topcount, "", baseKeyword, out itemTotal);
            itemTotal = topcount;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //string addr_city = "";
            foreach (var item in list)
            {
                sb.Append(" <dl>");
                sb.Append("     <dt><a href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\"><img alt=\"\" src=\"" + item.ExShowImg + "\" width=\"50\" height=\"50\"></a></dt>");
                //addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                sb.Append("       <dd><a href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\">" + item.CityName + " </a></dd>");
                sb.Append("       <dd><strong>" + item.CountryName + "</strong></dd>");
                sb.Append("       <dd><i>City</i></dd>");
                sb.Append("</dl>");

            }
            return sb.ToString();
        }

        /// <summary>
        /// city data
        /// </summary>
        /// <returns></returns>
        public JsonResult CityListAjx()
        {
            int userId = RequestHelper.GetValInt("userId", UserId);
            //int userId =Convert.ToInt32( ViewData["userId"]);
            int type = RequestHelper.GetValInt("type", 1);
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            if (filterKeyword == "POST A VISIT HERE!  Find, Rate and ADD to your Travels - Countries, Cities, Hotels, Venues and More")
                filterKeyword = "";
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = CityListData(type, userId, page, pageSize, filterLetter, filterKeyword, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// City List
        /// </summary>
        /// <returns></returns>
        private string CityListData(int type, int userId, int page, int pageSize, string filterLetter, string filterKeyword, out int total, out int itemTotal)
        {
            IList<CityListItem> cityList = new List<CityListItem>();

            cityList = _searchService.GetCityResultList(userId, page, pageSize, filterLetter, filterKeyword, out total);
            //browseList = _supplierService.FeedUserActivtieLists(userId, page, pageSize);


            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            itemTotal = cityList.Count;
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in cityList)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"2\" data-objId=\"" + item.ExObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                //ibt I've been there
                sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.City, item.CityId) + "\" class=\"btnBlueH45\" >");
                if (item.IbtCount < 1)
                    sb.Append("     <span class=\"bg\">I've been there</span>");
                else
                    sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\"><img alt=\"\" src=\"" + item.ExShowImg + "\" width=\"50\" height=\"50\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\" class=\"f15\">" + item.CityName + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.CountryName + "</span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 
                showNick = type == 1 ? "You " : "";
                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, 0);
                strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                selfId = _supplierService.GetIBTUser(UserId, 0, item.CountryId, item.CityId).UserId;
                if (friendCount == 0 && selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                }
                else if (friendCount == 0 && selfId == UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                }
                else if (friendCount > 0 && selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }

                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose NBorderB\">");
                uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"2\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                sb.Append("          </span>");
                uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"2\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                sb.Append("          </span>");
                uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"2\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                sb.Append("          </span>");
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");
            }
            return sb.ToString();
        }
        #endregion

        #region Member Search of data
        /// <summary>
        /// Member data
        /// </summary>
        /// <returns></returns>
        public JsonResult MemberResultList()
        {
            int topcount = 3;
            int itemTotal = 0;
            string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");


            var list = MemberResultListData(topcount, baseKeyword, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回MemberResultList的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string MemberResultListData(int topcount, string baseKeyword, out int itemTotal)
        {
            var list = _searchService.GetSearchMemberAllList(1, topcount, baseKeyword, "", "", out itemTotal);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string addr_city = "";
            foreach (var item in list)
            {
                sb.Append(" <dl>");
                sb.Append("     <dt><a class=\"userPic\" href=\"/Profile/" + item.UserId + "\"><img alt=\"User Name\" src=\"" + PageHelper.GetUserAvatar(item.UserId, (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._xat) + "\"></a></dt>");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                sb.Append("       <dd><a href=\"/Profile/" + item.UserId + "\">" + item.FirstName + " " + item.LastName + "</a></dd>");
                sb.Append("       <dd><span class=\"txt\">" + addr_city + item.CountryName + "</span></dd>");
                sb.Append("</dl>");

            }
            return sb.ToString();
        }
        /// <summary>
        /// Member all data
        /// </summary>
        /// <returns></returns>
        public JsonResult MemberAllList()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            string filterLetter = RequestHelper.GetVal("filterLetter");
            if (baseKeyword == "POST A VISIT HERE! Find, Rate and ADD to your Travels - Countries, Cities, Hotels, Venues and More")
                baseKeyword = "";

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = SearchMemberListData(page, pageSize, baseKeyword, filterLetter, filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SearchMemberList的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string SearchMemberListData(int page, int pageSize, string baseKeyword, string filterLetter, string filterField, out int total, out int itemTotal, bool hasClass = false)
        {
            var list = _searchService.GetSearchMemberAllList(page, pageSize, baseKeyword, filterLetter, filterField, out total);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            int FromStatusId;
            int ToStatusId;
            int Fromcount;
            string addr_city = "";
            foreach (var item in list)
            {
                sb.Append(" <li " + (hasClass ? "class='memberLi'" : "") + ">");
                sb.Append("     <div class=\"fixbox\">");
                FromStatusId = _profileService.GetUserConnection(UserId, item.UserId).StatusId;  //判断状态
                ToStatusId = _profileService.GetUserConnection(item.UserId, UserId).StatusId;
                Fromcount = _profileService.GetFriendCount(UserId, item.UserId);
                if (ToStatusId == 1 && FromStatusId == 1)
                {
                    sb.Append("       <span class=\"btnBg IEPng right\">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.UserId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.remove(" + item.UserId + ");\" class=\"last\">");
                    sb.Append("         <img alt=\"" + item.UserName + "\" src=\"/images/icon/friend.gif\" class=\"mr5\">Friends</a>");

                    // sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.UserId + ");\"><img alt=\"\" src=\"/images/icon/img.gif\" class=\"addFriend2\"></a>");
                }
                else if (Fromcount == 1 && FromStatusId == 0 && ToStatusId == 0)
                {
                    sb.Append("       <span class=\"btnBg IEPng btnBgGray right\">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.UserId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" class=\"last\"><img alt=\"" + item.UserName + "\" src=\"/images/icon/pending.gif\" class=\"mR5\"> Pending</a>");

                }
                else
                {
                    sb.Append("       <span class=\"btnBg IEPng right\">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.UserId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.UserId + ");\" class=\"last\"><img alt=\"" + item.UserName + "\" src=\"/images/icon/friend_blue_small.gif\" class=\"mR10\"> Add</a>");
                }
                sb.Append("       </span>");
                sb.Append("       <a class=\"userPic\" href=\"/Profile/" + item.UserId + "\"><img alt=\"User Name\" src=\"" + PageHelper.GetUserAvatar(item.UserId, (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._xat) + "\"></a>");
                sb.Append("       <dl>");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ", ";
                sb.Append("         <dt><a href=\"/Profile/" + item.UserId + "\">" + item.FirstName + " " + item.LastName + "</a><span class=\"txt\">" + addr_city + item.CountryName + "</span></dt>");
                sb.Append("         <dd class=\"italic Ca0a0a0\">Last Been To:<strong>" + _supplierService.LastBeenToCityCountryName(item.UserId) + "</strong></dd>");
                sb.Append("       </dl>");
                sb.Append("    </div>");
                sb.Append(" </li>");
            }
            return sb.ToString();
        }
        #endregion

        #region Supplier Search of data
        /// <summary>
        /// Supplier data
        /// </summary>
        /// <returns></returns>
        public JsonResult SupplierResultList()
        {
            int topcount = 3;
            int itemTotal = 0;
            string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");
            var list = SupplierResultListData(topcount, baseKeyword, out itemTotal);
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SupplierResultList的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string SupplierResultListData(int topcount, string baseKeyword, out int itemTotal)
        {
            var list = _searchService.GetSearchSupplierAllList(UserId, 1, topcount, baseKeyword, "", out itemTotal);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string TitleUrl = "";
            string addr_city = "";
            foreach (var item in list)
            {
                sb.Append(" <dl>");
                TitleUrl = "/Supplier/v/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.Title);
                sb.Append("     <dt><a class=\"userPic\" href=\"" + TitleUrl + "\"><img alt=\"" + item.Title + "\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, Identifier.SuppilyPhotoSize._xtn) + "\"></a></dt>");
                sb.Append("       <dd><a href=\"" + TitleUrl + "\">" + item.Title + "</a></dd>");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                sb.Append("       <dd><span class=\"txt\">" + addr_city + item.CountryName + "</span></dd>");
                sb.Append("</dl>");

            }
            return sb.ToString();
        }

        /// <summary>
        /// Supply all data
        /// </summary>
        /// <returns></returns>
        public JsonResult SupplyAllList()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            int type = RequestHelper.GetValInt("type", 1);
            string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");
            string filterLetter = RequestHelper.GetVal("filterLetter");
            if (baseKeyword == "POST A VISIT HERE!  Find, Rate and ADD to your Travels - Countries, Cities, Hotels, Venues and More")
                baseKeyword = "";

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = SearchSupplyListData(type, page, pageSize, baseKeyword, filterLetter, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SearchSupplyList的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string SearchSupplyListData(int type, int page, int pageSize, string baseKeyword, string filterLetter, out int total, out int itemTotal)
        {
            var list = _searchService.GetSearchSupplierAllList(UserId, page, pageSize, baseKeyword, filterLetter, out total);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in list)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"3\" data-objId=\"" + item.ObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                if (item.ServiceType == (int)Identifier.ServiceType.None)
                {
                    //ibt I've been there
                    sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Supply, item.SupplyId) + "\"  class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                        sb.Append("     <span class=\"bg\">I've been there</span>");
                    else
                        sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                }
                else
                {
                    //ibt I've used the service
                    sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Supply, item.SupplyId) + "\" class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                        sb.Append("     <span class=\"bg\">I've used the service</span>");
                    else
                        sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've used the service</span>" + item.IbtCount.ToString() + "</span>");
                }
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"" + item.TitleUrl + "\"><img alt=\"" + item.Title + "\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, Identifier.SuppilyPhotoSize._xtn) + "\" width=\"50\" height=\"50\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"" + item.TitleUrl + "\" class=\"f15\">" + item.Title + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.CountryName + "</span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 
                showNick = type == 1 ? "You " : "";
                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;
                if (friendCount == 0 && selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                }
                else if (friendCount == 0 && selfId == UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                }
                else if (selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }
                //sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"#\">" + _supplierService.HasBeenThereFriendsCount(UserId, 0, item.CityId, 0).ToString() + " Friends</a> has Been There</span>");
                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose NBorderB\">");

                if (item.ServiceType == (int)Identifier.ServiceType.None)
                {
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"3\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                    uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"3\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                    sb.Append("          </span>");
                    uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"3\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                    sb.Append("          </span>");
                }
                else
                {
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"3\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                }
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");

                //sb.Append(" <li>");
                //sb.Append("     <div class=\"fixbox\">");
                //string TitleUrl = "/Supplier/v/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.Title);
                //sb.Append("       <a class=\"userPic\" href=\"" + TitleUrl + "\"><img alt=\"Supplier Name\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn) + "\"></a>");
                //sb.Append("       <dl>");
                //string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                //sb.Append("         <dt><a href=\"" + TitleUrl + "\">" + item.Title + "</a></dt>");
                //sb.Append("         <dd class=\"italic Ca0a0a0\"><span class=\"txt\">" + addr_city + item.CountryName + "</span></dd>");
                //sb.Append("       </dl>");
                //sb.Append("    </div>");
                //sb.Append(" </li>");
            }
            return sb.ToString();
        }
        #endregion

        #endregion

        #region All Search of List(暂不用)
        /// <summary>
        /// ALL data
        /// </summary>
        /// <returns></returns>
        public JsonResult SearchResultListAjx()
        {
            string listType = RequestHelper.GetVal("listType");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            //string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");
            string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;

            //IBT_IMeetGetAllMemberList 0,'DESC','',-1,1,10,0,1,-1
            var list = SearchListData(listType, page, pageSize, baseKeyword, "", filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回SearchList的数据
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="SearchKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string SearchListData(string listType, int page, int pageSize, string baseKeyword, string filterLetter, string filterField, out int total, out int itemTotal)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (listType == "Member")
            {
                var list = _searchService.GetSearchMemberAllList(page, pageSize, baseKeyword, filterLetter, filterField, out total);
                itemTotal = list.Count;

                foreach (var item in list)
                {
                    sb.Append(" <li>");
                    sb.Append("     <div class=\"fixbox\">");
                    sb.Append("       <span class=\"btnBg IEPng right\">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.UserId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.UserId + ");\"><img alt=\"\" src=\"/images/icon/img.gif\" class=\"addFriend2\"></a>");
                    sb.Append("       </span>");
                    sb.Append("       <a class=\"userPic\" href=\"/Profile/" + item.UserId + "\"><img alt=\"User Name\" src=\"" + PageHelper.GetUserAvatar(item.UserId, (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._xat) + "\"></a>");
                    sb.Append("       <dl>");
                    string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                    sb.Append("         <dt><a href=\"/Profile/" + item.UserId + "\">" + item.FirstName + " " + item.LastName + "</a><span class=\"txt\">" + addr_city + item.CountryName + "</span></dt>");
                    sb.Append("         <dd class=\"italic Ca0a0a0\">Last Been To:<strong>" + _supplierService.LastBeenToCityCountryName(item.UserId) + "</strong></dd>");
                    sb.Append("       </dl>");
                    sb.Append("    </div>");
                    sb.Append(" </li>");
                }
            }
            else
            {
                var list = _searchService.GetSearchSupplierAllList(UserId, page, pageSize, baseKeyword, filterLetter, out total);
                itemTotal = list.Count;
                foreach (var item in list)
                {
                    sb.Append(" <li>");
                    sb.Append("     <div class=\"fixbox\">");
                    string TitleUrl = "/Supplier/v/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.Title);
                    sb.Append("       <a class=\"userPic\" href=\"" + TitleUrl + "\"><img alt=\"Supplier Name\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn) + "\"></a>");
                    sb.Append("       <dl>");
                    string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                    sb.Append("         <dt><a href=\"" + TitleUrl + "\">" + item.Title + "</a></dt>");
                    sb.Append("         <dd class=\"italic Ca0a0a0\"><span class=\"txt\">" + addr_city + item.CountryName + "</span></dd>");
                    sb.Append("       </dl>");
                    sb.Append("    </div>");
                    sb.Append(" </li>");
                }
            }
            return sb.ToString();
        }
        #endregion

        #region Advanced Search

        public ActionResult ResultSearch()
        {
            this.Title = "ResultSearch";
            this.Keywords = "keywords";
            this.Description = "description";

            return View();
        }
        /// <summary>
        /// City data(暂不用)
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCity()
        {
            int countryId = RequestHelper.GetValInt("objId");
            var list = CityItemList(countryId);
            return Json(new BoolMessageItem<int>(1, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回CityItemList的数据
        /// </summary>
        /// <returns></returns>
        private string CityItemList(int countryId)
        {
            var list = _supplierService.GetCityList(countryId);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append(" <li data-cityId=\"" + item.CityId + "\" onclick=\"getCityId(" + item.CityId + ")\">" + item.CityName + "</li>");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Select data
        /// </summary>
        /// <returns></returns>
        public JsonResult SelectItemAjx()
        {
            string selAttrIds = RequestHelper.GetVal("selAttrIds");
            List<string> TypeList = new List<string>() { selAttrIds };
            List<int> AttrIdList = new List<int>() { };
            string[] arr = selAttrIds.Split(',');//拆分后的字符数组  
            int attrId = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "0" || arr[i] == "")//判断   
                {
                    TypeList.Remove(arr[i]);//存在则从ArrayList中删除  
                }
                else
                {
                    int.TryParse(arr[i], out attrId);
                    AttrIdList.Add(attrId);
                }
            }
            var list = SelectItemList(AttrIdList);

            return Json(new BoolMessageItem<int>(1, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SelectItemList的数据
        /// </summary>
        /// <returns></returns>
        private string SelectItemList(IList<int> SupplyAttrType)
        {
            int attrId = 0;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < SupplyAttrType.Count(); i++)
            {
                attrId = SupplyAttrType[i];
                sb.Append("  <span class=\"selectedBox\" objId=" + attrId + ">" + _supplierService.GetAttrInfo(attrId).AttrDataName + " <a href=\"javascript:void(0);\" onclick=\"remove_item(" + attrId + ");\">x</a></span>");
            }
            return sb.ToString();
        }

        #region Coutry
        /// <summary>
        /// Coutry data
        /// </summary>
        /// <returns></returns>
        public JsonResult CoutryListAjx()
        {
            int userId = RequestHelper.GetValInt("userId", UserId);
            //int userId =Convert.ToInt32( ViewData["userId"]);
            int type = RequestHelper.GetValInt("type", 1);
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterLetter = RequestHelper.GetVal("saveLetter");
            int countryId = RequestHelper.GetValInt("countryId");
            string filterKeyword = "";
            //string countryName = _supplierService.GetCountryInfo(countryId).CountryName;
            string strWhere = " AND dbo.Countries.CountryId =" + countryId.ToString();
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = BrowseListData(type, userId, page, pageSize, strWhere, filterLetter, filterKeyword, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region City (Cities)
        /// <summary>
        /// city data
        /// </summary>
        /// <returns></returns>
        public JsonResult CitySearchAjx()
        {
            int userId = RequestHelper.GetValInt("userId", UserId);
            //int userId =Convert.ToInt32( ViewData["userId"]);
            int type = RequestHelper.GetValInt("type", 1); //0 Friends' Activties （Friends travel） 1: my activties (my travel)
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            int countryId = RequestHelper.GetValInt("countryId");
            string cityId = RequestHelper.GetVal("cityId");
            string strWhere = "";
            if (cityId == "" || cityId == "-1")
            {
                strWhere = " AND dbo.Cities.CountryId =" + countryId.ToString();
            }
            else
            {
                strWhere = " AND dbo.Cities.CityId =" + cityId;
            }
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = CitySearchData(type, userId, page, pageSize, filterLetter, strWhere, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// City Search
        /// </summary>
        /// <returns></returns>
        private string CitySearchData(int type, int userId, int page, int pageSize, string filterLetter, string strWhere, out int total, out int itemTotal)
        {
            IList<CityListItem> cityList = new List<CityListItem>();

            cityList = _searchService.GetCityAdvancedSearch(userId, strWhere, filterLetter, page, pageSize, out total);
            //browseList = _supplierService.FeedUserActivtieLists(userId, page, pageSize);


            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            itemTotal = cityList.Count;
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in cityList)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"2\" data-objId=\"" + item.ExObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                //ibt I've been there
                //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/PostVisit/2/" + item.ExObjId.ToString() + "','Get','',function(){ibt.PvPopLoad();},'POst visit');\"  class=\"btnBlueH45\" >");
                sb.Append("    <a href=\"" + @PageHelper.PostVisitNUrl((int)IMeet.IBT.Services.Identifier.IBTType.City, item.ExObjId) + "\" class=\"btnBlueH45\" >");
                if (item.IbtCount < 1)
                    sb.Append("     <span class=\"bg\">I've been there</span>");
                else
                    sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\"><img alt=\"\" src=\"" + item.ExShowImg + "\" width=\"50\" height=\"50\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\" class=\"f15\">" + item.CityName + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.CountryName + "</span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 

                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, 0);
                if (friendCount > 0)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: <a href='javascript:void(0);' onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.CityId + "','Get','',null,' " + friendCount + " Friends Who\\'ve Been There');\"> " + friendCount + "</a></span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: 0</span>");
                }

                //showNick = type == 1 ? "You " : "";
                //friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, 0);
                //strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //selfId = _supplierService.GetIBTUser(UserId, 0, item.CountryId, item.CityId).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}

                //sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"#\">" + _supplierService.HasBeenThereFriendsCount(UserId, 0, item.CityId, 0).ToString() + " Friends</a> has Been There</span>");
                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose NBorderB\">");
                uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"2\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                sb.Append("          </span>");
                uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"2\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                sb.Append("          </span>");
                uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"2\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                sb.Append("          </span>");
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");
            }
            return sb.ToString();
        }
        #endregion

        #region Suppliers
        /// <summary>
        /// Supply all data
        /// </summary>
        /// <returns></returns>
        public JsonResult SupplySearchAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            int type = RequestHelper.GetValInt("type", 1);
            int sortby = RequestHelper.GetValInt("sortBy");
            string filterLetter = RequestHelper.GetVal("filterLetter");
            int countryId = RequestHelper.GetValInt("countryId");
            int cityId = RequestHelper.GetValInt("cityId");
            string cateIds = RequestHelper.GetVal("cateIds");
            //判断拼接条件字符串
            string strCountryCondition = (countryId <= 0) ? "" : " AND sp.CountryId=" + countryId.ToString();
            string strCityCondition = (cityId <= 0) ? "" : " AND sp.CityId=" + cityId.ToString();
            //string strCateCondition = string.IsNullOrEmpty(cateIds) ? "" : " AND sa.ArrtDataId IN (" + cateIds + ")";

            string strLetterCondition = "";
            if (Validation.IsMatchRegEx(filterLetter, false, @"^[a-zA-Z]$"))
            {
                strLetterCondition = string.IsNullOrEmpty(filterLetter) ? "" : " AND sp.Title LIKE '" + filterLetter.ToString() + @"%'";
            }
            if (!Validation.IsMatchRegEx(cateIds, false, @"^\d*[,\d*]*$"))
            {
                cateIds = string.Empty;
            }

            string strSortBy = " ORDER BY rw.Rating DESC,sp.Title ASC";
            if (sortby == 1)
            {
                strSortBy = " ORDER BY rw.Recommended DESC,sp.Title ASC";
            }
            else if (sortby == 2)
            {
                strSortBy = " ORDER BY bt.IbtCount DESC,sp.Title ASC";
            }

            //string strWhere = strLetterCondition + strCateCondition + strCountryCondition + strCityCondition + strSortBy;
            string strWhere = strLetterCondition + strCountryCondition + strCityCondition + strSortBy;

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            List<SupplyListShort> supplyList = new List<SupplyListShort>();

            supplyList = _searchService.GetSupplyAdvancedSearch(UserId, countryId, cityId, sortby, filterLetter, cateIds, page, pageSize, out total);
            var list = this.SupplySearchListData(supplyList, type, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回SupplySearchListData的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string SupplySearchListData(List<SupplyListShort> list, int type, out int itemTotal)
        {
            //var list = _searchService.GetSupplyAdvancedSearch(UserId, strWhere, cateIds, page, pageSize, out total);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in list)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"3\" data-objId=\"" + item.ObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                if (item.ServiceType == (int)Identifier.ServiceType.ServiceProvider)
                {
                    //ibt I've used the service
                    sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Supply, item.SupplyId) + "\" class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                        sb.Append("     <span class=\"bg\">I've used the service</span>");
                    else
                        sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've used the service</span>" + item.IbtCount.ToString() + "</span>");
                }
                else
                {
                    //ibt I've been there
                    sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Supply, item.SupplyId) + "\" class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                        sb.Append("     <span class=\"bg\">I've been there</span>");
                    else
                        sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                }
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"" + item.TitleUrl + "\"><img alt=\"" + item.Title + "\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, Identifier.SuppilyPhotoSize._xtn) + "\" width=\"50\" height=\"50\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"" + item.TitleUrl + "\" class=\"f15\">" + item.Title + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.InfoState + ", " + item.CountryName + "</span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 

                //showNick = type == 1 ? "You " : "";
                //friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                //strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}

                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                if (friendCount > 0)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: <a href='javascript:void(0);' onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.SupplyId + "','Get','',null,' " + friendCount + " Friends Who\\'ve Been There');\"> " + friendCount + "</a></span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: 0</span>");
                }

                //sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"#\">" + _supplierService.HasBeenThereFriendsCount(UserId, 0, item.CityId, 0).ToString() + " Friends</a> has Been There</span>");
                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose NBorderB\">");
                if (item.ServiceType == (int)Identifier.ServiceType.ServiceProvider)
                {
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"3\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                }
                else
                {
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"3\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                    uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"3\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                    sb.Append("          </span>");
                    uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"3\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                    sb.Append("          </span>");
                }
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");

                //sb.Append(" <li>");
                //sb.Append("     <div class=\"fixbox\">");
                //string TitleUrl = "/Supplier/v/" + item.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(item.Title);
                //sb.Append("       <a class=\"userPic\" href=\"" + TitleUrl + "\"><img alt=\"Supplier Name\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, IMeet.IBT.Services.Identifier.SuppilyPhotoSize._xtn) + "\"></a>");
                //sb.Append("       <dl>");
                //string addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                //sb.Append("         <dt><a href=\"" + TitleUrl + "\">" + item.Title + "</a></dt>");
                //sb.Append("         <dd class=\"italic Ca0a0a0\"><span class=\"txt\">" + addr_city + item.CountryName + "</span></dd>");
                //sb.Append("       </dl>");
                //sb.Append("    </div>");
                //sb.Append(" </li>");
            }
            return sb.ToString();
        }
        #endregion

        #region Member
        /// <summary>
        /// Member all data
        /// </summary>
        /// <returns></returns>
        public JsonResult MemberSearchAjx()
        {
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            //string baseKeyword = RequestHelper.GetUrlDecodeVal("baseKeyword");
            //string filterField = RequestHelper.GetUrlDecodeVal("filterField");
            string filterLetter = RequestHelper.GetVal("filterLetter");
            int countryId = RequestHelper.GetValInt("countryId");
            string cityId = RequestHelper.GetVal("cityId");
            string strLetterCondition = string.IsNullOrEmpty(filterLetter) ? "" : " AND mp.FirstName LIKE '" + filterLetter.ToString() + @"%'";
            string strSelect = "";
            if (cityId == "" || cityId == "-1")
            {
                strSelect = " AND mp.CountryId =" + countryId.ToString();
            }
            else
            {
                strSelect = " AND mp.CityId =" + cityId;
            }
            string strWhere = strLetterCondition + strSelect;
            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = MemberSearchListData(page, pageSize, strWhere, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 返回MemberSearchListData的数据
        /// </summary>
        /// <param name="topcount"></param>
        /// <param name="baseKeyword"></param>
        /// <returns></returns>
        private string MemberSearchListData(int page, int pageSize, string strWhere, out int total, out int itemTotal)
        {
            var list = _searchService.GetMemberAdvancedSearch(strWhere, page, pageSize, out total);
            itemTotal = list.Count();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string addr_city = "";
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append("     <div class=\"fixbox\">");
                int FromStatusId = _profileService.GetUserConnection(UserId, item.UserId).StatusId;  //判断好友
                int ToStatusId = _profileService.GetUserConnection(item.UserId, UserId).StatusId;
                int Fromcount = _profileService.GetFriendCount(UserId, item.UserId);
                if (ToStatusId == 1 && FromStatusId == 1)
                {
                    sb.Append("       <span class=\"btnBg IEPng right\">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.UserId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.remove(" + item.UserId + ");\" class=\"last\">");
                    sb.Append("         <img alt=\"" + item.UserName + "\" src=\"/images/icon/friend.gif\" class=\"mr5\">Friends</a>");

                    // sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.UserId + ");\"><img alt=\"\" src=\"/images/icon/img.gif\" class=\"addFriend2\"></a>");
                }
                else if (Fromcount != 0 && FromStatusId == 0 && ToStatusId == 0)
                {
                    sb.Append("       <span class=\"btnBg IEPng btnBgGray right\">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.UserId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" class=\"last\"><img alt=\"" + item.UserName + "\" src=\"/images/icon/pending.gif\" class=\"mR5\"> Pending</a>");

                }
                else
                {
                    sb.Append("       <span class=\"btnBg IEPng right\">");
                    sb.Append("         <a href=\"/Connection/Compose/" + item.UserId + "\"><img alt=\"Email\" src=\"/images/icon/img.gif\" class=\"readEmail\"></a>");
                    sb.Append("         <a href=\"javascript:void(0);\" onclick=\"Connection.request(" + item.UserId + ");\" class=\"last\"><img alt=\"" + item.UserName + "\" src=\"/images/icon/friend_blue_small.gif\" class=\"mR10\"> Add</a>");
                }
                sb.Append("       </span>");
                sb.Append("       <a class=\"userPic\" href=\"/Profile/" + item.UserId + "\"><img alt=\"User Name\" src=\"" + PageHelper.GetUserAvatar(item.UserId, (int)IMeet.IBT.Services.Identifier.AccountSettingAvatarSize._xat) + "\"></a>");
                sb.Append("       <dl>");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ", ";
                sb.Append("         <dt><a href=\"/Profile/" + item.UserId + "\">" + item.FirstName + " " + item.LastName + "</a><span class=\"txt\">" + addr_city + item.CountryName + "</span></dt>");
                sb.Append("         <dd class=\"italic Ca0a0a0\">Last Been To:<strong>" + _supplierService.LastBeenToCityCountryName(item.UserId) + "</strong></dd>");
                sb.Append("       </dl>");
                sb.Append("    </div>");
                sb.Append(" </li>");
            }
            return sb.ToString();
        }
        #endregion

        #endregion

        #region search listing

        /// <summary>
        /// listing page
        /// </summary>
        /// <returns></returns>
        public ActionResult Listing()
        {
            this.Title = "Search";

            return View();
        }

        /// <summary>
        /// get listing
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListing()
        {
            int pageIndex = RequestHelper.GetValInt("pageIndex");
            int pageSize = RequestHelper.GetValInt("pageSize");
            string filter = RequestHelper.GetVal("filter");
            string keyWord = RequestHelper.GetVal("keyWord");
            string searchBy = RequestHelper.GetVal("searchBy");

            int total = 0;
            int itemTotal = 0;
            Dictionary<string, string> dic = new Dictionary<string, string>();
            BoolMessageItem<Dictionary<string, string>> bm = new BoolMessageItem<Dictionary<string, string>>(dic, false, "Undefined searchBy!");
            try
            {
                switch (searchBy)
                {
                    case "countries":
                        List<CountryListItem> countryList = _searchService.GetCountrySearchListing(UserId, pageIndex, pageSize, filter, keyWord, out total);
                        string messageCountry = CountryListingStr(countryList);
                        dic.Add("total", total.ToString());
                        dic.Add("msg", "About " + total.ToString() + " Countries in the world");
                        bm = new BoolMessageItem<Dictionary<string, string>>(dic, true, messageCountry);
                        break;
                    case "cities":
                        List<CityListItem> cityList = _searchService.GetCitySearchListing(UserId, pageIndex, pageSize, filter, keyWord, out total);
                        string messageCities = CityListingStr(cityList);
                        dic.Add("total", total.ToString());
                        dic.Add("msg", total.ToString() + " Cities Found");
                        bm = new BoolMessageItem<Dictionary<string, string>>(dic, true, messageCities);
                        break;
                    case "hotels":
                    case "other":
                    case "all":
                        string attDataName = "";

                        if (searchBy == "hotels")
                            attDataName = "'Hotels'";

                        if (searchBy == "other")
                            attDataName = "'Lifestyle','Attractions'";

                        List<SupplyListShort> list = _searchService.GetSupplySearchListing(UserId, pageIndex, pageSize, attDataName, filter, keyWord, out total);
                        itemTotal = list.Count;
                        string messageSupply = SupplyListingStr(list);
                        dic.Add("total", total.ToString());
                        if (searchBy == "hotels")
                        {
                            dic.Add("msg", total.ToString() + " Hotels Found");
                        }
                        else
                        {
                            dic.Add("msg", total.ToString() + " Suppliers Found");
                        }
                        bm = new BoolMessageItem<Dictionary<string, string>>(dic, true, messageSupply);
                        break;
                    case "member":
                        string messageMember = SearchMemberListData(pageIndex, pageSize, keyWord, filter, "", out total, out itemTotal, true);
                        dic.Add("total", total.ToString());
                        dic.Add("msg", total.ToString() + " Members Found");
                        bm = new BoolMessageItem<Dictionary<string, string>>(dic, true, messageMember);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                bm = new BoolMessageItem<Dictionary<string, string>>(dic, false, ex.Message);
            }

            return Json(bm);
        }

        #region country
        /// <summary>
        /// country list string
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private string CountryListingStr(List<CountryListItem> list)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in list)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"1\" data-objId=\"" + item.ObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                //ibt I've been there
                sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Country, item.CountryId) + "\" class=\"btnBlueH45\" >");
                if (item.IbtCount < 1)
                    sb.Append("     <span class=\"bg\">I've been there</span>");
                else
                    sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"/Country/NewsFeed/1/" + item.ObjId.ToString() + "\"><img alt=\"\" src=\"" + item.FlagSrc + "\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"/Country/NewsFeed/1/" + item.ObjId.ToString() + "\" class=\"f15\">" + item.CountryName + "</a>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 
                //showNick = type == 1 ? "You " : "";

                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, 0, 0);
                if (friendCount > 0)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: <a href='javascript:void(0);' onclick=\"ibt.loadUrlPop('/ShowFriend/1/"+item.CountryId+"','Get','',null,' "+friendCount+" Friends Who\\'ve Been There');\"> " + friendCount + "</a></span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: 0</span>");
                }
                //showNick = "You";
                //friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, 0, 0);
                //strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //selfId = _supplierService.GetIBTUser(UserId, 0, item.CountryId, 0).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (friendCount > 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/1/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/1/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}

                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose\">");
                uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"1\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                sb.Append("          </span>");
                uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + "\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"1\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                sb.Append("          </span>");
                uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + "\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"1\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                sb.Append("          </span>");
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");
            }
            return sb.ToString();
        }

        #endregion

        #region city

        private string CityListingStr(List<CityListItem> list)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in list)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"2\" data-objId=\"" + item.ExObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                //ibt I've been there
                sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.City, item.CityId) + "\" class=\"btnBlueH45\" >");
                if (item.IbtCount < 1)
                    sb.Append("     <span class=\"bg\">I've been there</span>");
                else
                    sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\"><img alt=\"\" src=\"" + item.ExShowImg + "\" width=\"50\" height=\"50\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"/City/NewsFeed/2/" + item.ExObjId.ToString() + "\" class=\"f15\">" + item.CityName + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.CountryName + "</span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 
                //showNick = type == 1 ? "You " : "";

                //showNick = "You";
                //friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, 0);
                //strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //selfId = _supplierService.GetIBTUser(UserId, 0, item.CountryId, item.CityId).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (friendCount > 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}

                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, 0);
                if (friendCount > 0)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: <a href='javascript:void(0);' onclick=\"ibt.loadUrlPop('/ShowFriend/2/" + item.CityId + "','Get','',null,' " + friendCount + " Friend Who\\'ve Been There');\">" + friendCount + "</a></span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: 0</span>");
                }

                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose NBorderB\">");
                uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"2\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                sb.Append("          </span>");
                uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"2\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                sb.Append("          </span>");
                uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"2\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                sb.Append("          </span>");
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");
            }
            return sb.ToString();
        }

        #endregion

        #region supply

        /// <summary>
        /// supply list string
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private string SupplyListingStr(List<SupplyListShort> list)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string showNick = "";
            int friendCount;
            string strcount = "";
            int selfId;
            string uncheckbox_recommended = "";
            string uncheckbox_WantGoBack = "";
            string uncheckbox_BucketList = "";
            foreach (var item in list)
            {
                sb.Append("<li>");

                sb.Append("<span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"3\" data-objId=\"" + item.ObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                if (item.ServiceType == (int)Identifier.ServiceType.None)
                {
                    //ibt I've been there
                    sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Supply, item.SupplyId) + "\"  class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                        sb.Append("     <span class=\"bg\">I've been there</span>");
                    else
                        sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                }
                else
                {
                    //ibt I've used the service
                    sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Supply, item.SupplyId) + "\" class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                        sb.Append("     <span class=\"bg\">I've used the service</span>");
                    else
                        sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've used the service</span>" + item.IbtCount.ToString() + "</span>");
                }
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");

                //图片
                sb.Append("<a class=\"img\" href=\"" + item.TitleUrl + "\"><img alt=\"" + item.Title + "\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, Identifier.SuppilyPhotoSize._xtn) + "\" width=\"50\" height=\"50\"></a>");

                sb.Append("<dl class=\"info\">");
                //标题（城市/国家）
                sb.Append("  <dt><a href=\"" + item.TitleUrl + "\" class=\"f15\">" + item.Title + "</a> <span class=\"f11  \">" + item.CityName + ", "+item.InfoState+", " + item.CountryName + "</span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 

                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                if (friendCount > 0)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: <a href='javascript:void(0);' onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.SupplyId + "','Get','',null,' " + friendCount + " Friend Who\\'ve Been There');\">" + friendCount + "</a></span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: 0</span>");
                }

                //showNick = "You ";
                //friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                //strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                //selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;
                //if (friendCount == 0 && selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                //}
                //else if (friendCount == 0 && selfId == UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                //}
                //else if (selfId != UserId)
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}
                //else
                //{
                //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/3/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                //}

                //sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"#\">" + _supplierService.HasBeenThereFriendsCount(UserId, 0, item.CityId, 0).ToString() + " Friends</a> has Been There</span>");
                sb.Append("  </dt>");
                //RWB
                sb.Append("  <dd>");
                sb.Append("      <div class=\"choose NBorderB\">");

                if (item.ServiceType == (int)Identifier.ServiceType.None)
                {
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"3\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                    uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"3\"  data-readonly=\"false\" data-ibtType=\"3\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                    sb.Append("          </span>");
                    uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5\"  onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"4\"  data-readonly=\"false\" data-ibtType=\"3\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                    sb.Append("          </span>");
                }
                else
                {
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5\" onclick=\"ibt.rrwb_rwb(this);\" data-postType=\"0\" data-rrwbType=\"2\" data-readonly=\"false\"  data-ibtType=\"3\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                }
                sb.Append("      </div>");
                sb.Append("  </dd>");
                //end:dl info
                sb.Append("</dl>");
                sb.Append("</li>");
            }
            return sb.ToString();
        }

        #endregion


        #endregion
    }
}
