﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Drawing;
using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Extensions;
using IMeet.IBT.Common.Helpers;
using IMeet.IBT.DAL;

namespace IMeet.IBT.Controllers
{
    public class IBTController : BaseController
    {
        private IIBTService _IBTService;
        private ISupplierService _supplierService;
        private INewsFeedService _newsFeedService;
        public IBTController(IIBTService IBTService, ISupplierService supplierService, INewsFeedService newsFeedService)
        {
            _IBTService = IBTService;
            _supplierService = supplierService;
            _newsFeedService = newsFeedService;
        }

        #region Country
        /// <summary>
        /// rrwb update
        /// </summary>
        /// <returns></returns>
        public JsonResult RRWB()
        {
            int ibtType = RequestHelper.GetValInt("ibtType");
            int ibtObjId = RequestHelper.GetValInt("ibtObjId");
            int rrwbType = RequestHelper.GetValInt("rrwbType");
            int val = RequestHelper.GetValInt("val");
            if (UserId <= 0)
            {
                return Json(new BoolMessageItem<int>(0, false, "please login.."), JsonRequestBehavior.AllowGet);
            }
            int a = 1;
            Identifier.PostTypes enum_postType = (Identifier.PostTypes)Enum.Parse(typeof(Identifier.PostTypes), a.ToString(), true);
            Identifier.RRWBType enum_rrwbType = (Identifier.RRWBType)Enum.Parse(typeof(Identifier.RRWBType), rrwbType.ToString());
            Identifier.IBTType enum_ibtType = (Identifier.IBTType)Enum.Parse(typeof(Identifier.IBTType), ibtType.ToString());
            var bm = _supplierService.RRWB(UserId, enum_postType, enum_ibtType, ibtObjId, enum_rrwbType, val);

            return Json(new BoolMessageItem<int>(0, bm.Success, bm.Message), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// rrwb rating update
        /// </summary>
        /// <returns></returns>
        public JsonResult RRWBRating()
        {
            int score = RequestHelper.GetValInt("score");
            int ibtType = RequestHelper.GetValInt("ibtType");
            int ibtObjId = RequestHelper.GetValInt("ibtObjId");//countryId or cityId or supplyId 
            if (UserId <= 0)
            {
                return Json(new BoolMessageItem<int>(0, false, "please login.."), JsonRequestBehavior.AllowGet);
            }
            int postType = 1;
            Identifier.PostTypes enum_postType = (Identifier.PostTypes)Enum.Parse(typeof(Identifier.PostTypes), postType.ToString(), true);
            Identifier.RRWBType enum_rrwbType = (Identifier.RRWBType)Enum.Parse(typeof(Identifier.RRWBType), ((int)Identifier.RRWBType.Rating).ToString());
            Identifier.IBTType enum_ibtType = (Identifier.IBTType)Enum.Parse(typeof(Identifier.IBTType), ibtType.ToString());
            BoolMessage bm = _supplierService.RRWB(UserId, enum_postType, enum_ibtType, ibtObjId, enum_rrwbType, score);
            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Updload Attachment

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <returns></returns>
        public JsonResult AttachmentUpload()
        {
            BoolMessageItem<int> bm = new BoolMessageItem<int>(0, false, "please login..");
            //int uid = _profileService.GetUserId();
            if (Request.Files.Count > 0)
            {
                HttpPostedFile file = System.Web.HttpContext.Current.Request.Files[0];
                if (file != null && file.ContentLength > 0 && Validation.IsMatchRegEx(file.FileName.ToLower(), false, @"^.*\.(jpg|jpeg|png|gif)$"))
                {
                    string guid = RequestHelper.GetVal("guid");
                    int attType = RequestHelper.GetValInt("attType");
                    string tempPath = Server.MapPath("~/Staticfile/temp/");
                    string tempFile = tempPath + Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                    file.SaveAs(tempFile);
                    bm = _IBTService.AttachmentNewsFeedImage(UserId, attType, guid, tempFile);
                }
            }
            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AttachementList()
        {
            int type = RequestHelper.GetValInt("type", 0);
            int objid = RequestHelper.GetValInt("objid", 0);    //userId or countryid or cityid or supplyid
            int objtype = RequestHelper.GetValInt("objtype", 0);    //0userid,1countryid,2cityid,3supplyid
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            string strWhere = "";
            switch (objtype)
            {
                case 0: strWhere = " and dbo.NewsFeed.Userid=" + objid + " "; break;
                case 1: strWhere = " and dbo.NewsFeed.CountryId=" + objid + " "; break;
                case 2: strWhere = " and dbo.NewsFeed.CityId=" + objid + " "; break;
                case 3: strWhere = " and dbo.NewsFeed.SupplyId=" + objid + " "; break;
                default:
                    break;
            }
            strWhere = strWhere + " and dbo.Attachment.atttype=" + type.ToString() + " ";
            IList<Attachment> list = _IBTService.GetAttachmentListByPage(strWhere, page, pageSize);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append("     <li class=\"mR10\" onclick=\"PhotoManage.GetPhotoDetail(" + item.AttId + ", " + item.ObjId + ");\" style=\"text-align:center\">");
                sb.Append("         <a href=\"javascript:void(0);\" class=\"photoImg\"><img src=\"" + item.SourceFileName + Identifier.SuppilyPhotoSize._bn + ".jpg\" alt=\"\" /></a>");
                sb.Append("         <div class=\"photosInfo\">");
                sb.Append("             <div class=\"fixbox bg\">");
                sb.Append("                 <a href=\"javascript:void(0);\" class=\"img\">");
                NewsFeed _nf = _newsFeedService.GetNewsFeedByNewsFeedId(item.ObjId);
                if (_nf == null)
                {
                    _nf = new NewsFeed();
                }
                var profile = _profileService.GetUserProfile(_nf.UserId);
                sb.Append("                     <img src=\"" + _profileService.GetUserAvatar(_nf.UserId, 26) + "\" alt=\"\" />");
                sb.Append("                 </a>");
                sb.Append("                 <dl>");
                sb.Append("                     <dt>" + profile.Firstname + "</dt>");
                sb.Append("                     <dd>" + item.CreateData.ToString("dd MMM yyyy") + "</dd>");
                sb.Append("                 </dl>");
                sb.Append("             </div>");
                sb.Append("         </div>");
                sb.Append("     </li>");
            }
            BoolMessageItem<int> bm = new BoolMessageItem<int>(list.Count, true, sb.ToString());
            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 图片状态修改
        /// </summary>
        /// <returns></returns>
        public JsonResult AttachmentChange()
        {
            int attId = RequestHelper.GetValInt("attId");
            int statusId = RequestHelper.GetValInt("status");
            var bm = _IBTService.AttachmentChange(attId, statusId);
            return Json(bm);
        }

        #endregion

        #region newsfeed
        /// <summary>
        /// save newfeed
        /// </summary>
        /// <returns></returns>
        public JsonResult NewsFeedSave()
        {
            string guid = RequestHelper.GetVal("guid");
            int attType = RequestHelper.GetValInt("attType");
            string val = RequestHelper.GetVal("val");
            int countryId = RequestHelper.GetValInt("countryId");
            int ibtType = RequestHelper.GetValInt("ibtType");
            if (UserId <= 0)
            {
                return Json(new BoolMessage(false, "please login.."));
               
            }
            BoolMessageItem<int> bm = new BoolMessageItem<int>(0, false, string.Empty);
            try
            {
                switch (ibtType)
                {
                    case (int)Identifier.IBTType.Country:
                        {
                            bm = _newsFeedService.AddNewsFeed(UserId, countryId, 0, 0, ibtType, Identifier.FeedActiveType.PostComments, val, string.Empty, DateTime.Now);
                            break;
                        }
                    case (int)Identifier.IBTType.City:
                        {
                            City city = _IBTService.GetCity(countryId);
                            bm = _newsFeedService.AddNewsFeed(UserId, city.CountryId, city.CityId, 0, ibtType, Identifier.FeedActiveType.PostComments, val, string.Empty, DateTime.Now);
                            break;
                        }
                    default:
                        break;
                }
                _IBTService.SetAttachmentObjId(guid, bm.Item);
                return Json(new BoolMessage(true, string.Empty));
            }
            catch (Exception ex)
            {
                return Json(bm);
            }

        }
        /// <summary>
        /// get FeedComment list
        /// </summary>
        /// <returns></returns>
        public JsonResult GetFeedCommentList()
        {
            int newsFeedId = RequestHelper.GetValInt("newsFeedId");
            int pageIndex = RequestHelper.GetValInt("pageIndex", 1);
            if (newsFeedId <= 0)
                return Json(new BoolMessageItem<int>(0, false, string.Empty));
            int pageSize = 3;
            var feedCommentList = _IBTService.GetFeedcommentEx(newsFeedId, pageIndex, pageSize).ToList();
            int total = _IBTService.GetNewsFeedInfo(newsFeedId).CommentCount;

            PagedList<FeedcommentsEx> pageList = new PagedList<FeedcommentsEx>(pageIndex, pageSize, total, feedCommentList);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //paging
            sb.Append("<span id=\"ibt_common_paging_" + newsFeedId + "\" class='paging right show'>");
            if (pageList.TotalPages > 1)
            {
                if (pageIndex - 1 > 0)
                    sb.Append("<a href='javascript:void(0);' class='prev' title='Prev' onclick=\"ibt_common.getFeedCommentPage(" + newsFeedId.ToString() + "," + (pageIndex - 1).ToString() + ");\"></a>");
                if (pageIndex - 2 > 0)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"more\" title=\"More...\" onclick=\"ibt_common.getFeedCommentPage(" + newsFeedId.ToString() + "," + (pageIndex - 2).ToString() + ");\"></a>");
                if (pageIndex - 1 > 0)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"bg\" onclick=\"ibt_common.getFeedCommentPage(" + newsFeedId.ToString() + "," + (pageIndex - 1).ToString() + ");\">" + (pageIndex - 1).ToString() + "</a>");
                sb.Append("<a href=\"javascript:void(0);\" class=\"bg bgActive\">" + pageIndex + "</a>");
                if (pageIndex + 1 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"bg\" onclick=\"ibt_common.getFeedCommentPage(" + newsFeedId.ToString() + "," + (pageIndex + 1).ToString() + ");\">" + (pageIndex + 1).ToString() + "</a>");
                if (pageIndex + 2 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"more\" title=\"More...\" onclick=\"ibt_common.getFeedCommentPage(" + newsFeedId.ToString() + "," + (pageIndex + 2).ToString() + ");\"></a>");
                if (pageIndex + 1 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"next\" title=\"Next\" onclick=\"ibt_common.getFeedCommentPage(" + newsFeedId.ToString() + "," + (pageIndex + 1).ToString() + ");\"></a>");
            }
            sb.Append("</span>");
            //Comments count
            sb.Append("<a href=\"javascript:void(0);\" id=\"ibtcommon_comments_" + newsFeedId.ToString() + "\" onclick=\"ibt_common.getFeedComment(" + @newsFeedId.ToString() + ");\" > Comments (" + total.ToString() + ")  </a>");
            //replyBox
            sb.Append("<div id='replyBox_" + newsFeedId.ToString() + "' class='replyBox show'>");
            sb.Append("<ul id=\"commentList_" + newsFeedId + "\">");
            feedCommentList.ForEach(comment =>
            {
                string createDate = comment.CreateDate.ToString("MMM dd, yyyy");
                sb.Append("<li>");
                sb.Append("     <span class=\"time italic\">wrote a comment . " + createDate + "</span>");
                sb.Append("     <a href=\"/Profile/" + comment.UserId + "\" class=\"userPic\"><img src=\"" + PageHelper.GetUserAvatar(comment.UserId, (int)Identifier.AccountSettingAvatarSize._xxlat) + "\" alt=\"\" /></a>");
                sb.Append("     <div class=\"replyMain\">");
                sb.Append("         <a href=\"/Profile/" + comment.UserId + "\" class=\"name\">" + comment.FirstName + "</a>");
                sb.Append("         <p>" + comment.Comments + "</p>");
                sb.Append("     </div>");
                sb.Append("</li>");
            });
            sb.Append("</ul>");
            sb.Append("<ul>");
            sb.Append("    <li><textarea id=\"commnetTxt_" + newsFeedId + "\">Write a message...</textarea><input type=\"button\" onclick=\"ibt_common.saveFeedComment(" + newsFeedId + ")\" value=\"Comment\"/></li>");
            sb.Append("</ul>");
            sb.Append("</div>");

            BoolMessageItem<int> bm = new BoolMessageItem<int>(total, true, sb.ToString());
            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存feedcomment
        /// </summary>
        /// <returns></returns>
        public JsonResult SaveFeedComment()
        {
            int newsFeedId = RequestHelper.GetValInt("newsFeedId");
            string val = RequestHelper.GetVal("val");
            if (UserId <= 0)
            {
                return Json(new BoolMessageItem<int>(0, false, "please login.."));
            }

            var bm = _newsFeedService.AddFeedComments(newsFeedId, UserId, val);
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //string createDate = DateTime.Now.ToString("MMM dd, yyyy");
            //sb.Append("<li>");
            //sb.Append("     <span class=\"time italic\">wrote a comment . " + createDate + "</span>");
            //sb.Append("     <a href=\"/Profile/"+UserId+"\" class=\"userPic\"><img src=\"" + PageHelper.GetUserAvatar(UserId, (int)Identifier.AccountSettingAvatarSize._xxlat) + "\" alt=\"\" /></a>");
            //sb.Append("     <div class=\"replyMain\">");
            //sb.Append("         <a href=\"/Profile/"+UserId+"\" class=\"name\">" + _profileService.GetUserProfile(UserId).Firstname + "</a>");
            //string str = val.Replace("<", "&lt;").Replace(">", "&gt;");
            //str = str.Replace("\n", "<br />");
            //sb.Append("         <p>" + str + "</p>");
            //sb.Append("     </div>");
            //sb.Append("</li>");
            //return Json(new BoolMessageItem<int>(0, true, sb.ToString()), JsonRequestBehavior.AllowGet);
            return Json(bm, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// get feedlist
        /// </summary>
        /// <returns>html</returns>
        public PartialViewResult GetFeedList()
        {
            int ibtType = RequestHelper.GetValInt("ibtType");
            int ibtObjId = RequestHelper.GetValInt("ibtObjId");
            int pageIndex = RequestHelper.GetValInt("pageIndex");
            DateTime dt = RequestHelper.GetValDateTime("searchTime");
            int pageSize = 10;
            List<FeedList> feedList = new List<FeedList>();
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    {
                        Country country = _IBTService.GetCountry(ibtObjId);
                        ViewBag.IbtObject = country;
                        feedList = _IBTService.GetFeedList(ibtObjId, 0, 0, ibtType, dt, pageIndex, pageSize).ToList();
                        ViewBag.IBTCount = _IBTService.IBTCount(UserId, ibtObjId, 0, 0);
                        ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, ibtObjId, 0, 0);
                        break;
                    }
                case (int)Identifier.IBTType.City:
                    {
                        City city = _IBTService.GetCity(ibtObjId);
                        ViewBag.IbtObject = city;
                        feedList = _IBTService.GetFeedList(city.CountryId, city.CityId, 0, ibtType, dt, pageIndex, pageSize).ToList();
                        ViewBag.IBTCount = _IBTService.IBTCount(UserId, city.CountryId, city.CityId, 0);
                        ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, city.CountryId, city.CityId, 0);
                        break;
                    }
                case (int)Identifier.IBTType.Supply:
                    break;
                default:
                    break;
            }
            ViewBag.FeedList = feedList;
            ViewBag.IbtType = ibtType;
            return PartialView("IBT/_FeedList");
        }

        #endregion

        #region countrySpecials
        /// <summary>
        /// Specials data
        /// </summary>
        /// <returns></returns>
        public JsonResult specialsListAjx()  //用户ID(参数)有问题
        {
            int countryId = RequestHelper.GetValInt("countryId");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = SpecialsListData(countryId, page, pageSize, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回SpecialsList的数据
        /// </summary>
        /// <returns></returns>
        private string SpecialsListData(int countryId, int page, int pageSize, out int total, out int itemTotal)
        {
            var list = _supplierService.GetSpecialsOfCountry(countryId, page, pageSize, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append(" <span class=\"img\">");
                if (item.MediaType == 2)
                {
                    sb.Append("      <img src=\"" + item.FileSrc + "_spec.jpg\"></span>");
                }
                else
                {
                    item.Code.ToString();
                }
                sb.Append("  <div class=\"infoMain\"> ");
                sb.Append("     <dl>");
                sb.Append("     <dt><a href=\"#\">" + item.Title + "</a> <span class=\"date  f11\">From " + DateTime.Parse(item.StartDate.ToString()).ToString("d MMMM yyyy") + " - " + DateTime.Parse(item.ExpiryDate.ToString()).ToString("d MMMM yyyy") + "</span></dt>");
                if (item.Desp.ToString().Contains("<") || item.Desp.ToString().Contains(">"))
                {
                    sb.Append("<dd class=\"mT5\">" + item.Desp.ToString() + "</dd>");

                }
                else
                {
                    sb.Append(string.IsNullOrEmpty(item.Desp.ToString()) ? "" : "<dd class=\"mT5\">" + IMeet.IBT.Common.StringHelper.Truncate(item.Desp, 150, "...") + "</dd>");

                }
                sb.Append("     <dd class=\"italic f11 Cd4d4d4\">(\"Executive Care\" $50 Per Person) * </dd>");
                sb.Append("     </dl>");
                sb.Append("     <div class=\"mT20\">");
                sb.Append("         <a class=\"arrowBlueR mT10 bold\" href=\"#\">Request Info</a>");
                sb.Append("    </div>");
                sb.Append(" </div>");
                sb.Append("</li>");
            }

            return sb.ToString();
        }

        /// <summary>
        /// 返回SpecialsList的数据
        /// </summary>
        /// <param name="mediaList"></param>
        /// <returns></returns>
        private string SpecialsListData(List<Media> mediaList)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in mediaList)
            {
                sb.Append(" <li>");
                sb.Append(" <span class=\"img\">");
                if (item.MediaType == 2)
                {
                    sb.Append("<a href=\"" + item.FileSrc + "_ori.jpg\" rel=\"lightbox[img_" + item.MediaId.ToString() + "]\" title=\"" + item.Title + "\" class=\"mR5\"><img src=\"" + item.FileSrc + "_spec.jpg\" alt=\"Photos description \" /></a>");
                    //sb.Append("      <img src=\"" + item.FileSrc + "_spec.jpg\">");
                }
                else
                {
                    sb.Append(item.Code.ToString());
                }
                sb.Append(" </span>");
                sb.Append("  <div class=\"infoMain\"> ");
                sb.Append("     <dl>");
                //sb.Append("     <dt><a href=\"/Supplier/s/" + item.SupplyId + "\">" + item.Title + "</a> <span class=\"date  f11\">From " + DateTime.Parse(item.StartDate.ToString()).ToString("d MMMM yyyy") + " - " + DateTime.Parse(item.ExpiryDate.ToString()).ToString("d MMMM yyyy") + "</span></dt>");
                sb.Append("     <dt><a href=\"javascript:void(0);\" onclick=\"GetCountOfHBrowse(" + item.MediaId + "," + item.IBTType + "," + item.SupplyId + "," + item.CountryId + "," + item.CityId + ")\">" + item.Title + "</a> <span class=\"date  f11\">From " + DateTime.Parse(item.StartDate.ToString()).ToString("d MMMM yyyy") + " - " + DateTime.Parse(item.ExpiryDate.ToString()).ToString("d MMMM yyyy") + "</span></dt>");
                if (item.Desp.ToString().Contains("<") || item.Desp.ToString().Contains(">"))
                {
                    sb.Append("<dd class=\"mT5\">" + item.Desp.ToString() + "</dd>");

                }
                else
                {
                    sb.Append(string.IsNullOrEmpty(item.Desp.ToString()) ? "" : "<dd class=\"mT5\">" + IMeet.IBT.Common.StringHelper.Truncate(item.Desp, 150, "...") + "</dd>");

                }
                sb.Append("     <dd class=\"italic f11 Cd4d4d4\"></dd>");
                sb.Append("     </dl>");
                //sb.Append("     <div class=\"mT20\">");
                //sb.Append("         <a class=\"arrowBlueR mT10 bold\" href=\"#\">Request Info</a>");
                //sb.Append("    </div>");
                sb.Append(" </div>");
                sb.Append("</li>");
            }

            return sb.ToString();
        }

        /// <summary>
        /// get specials list.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSpecialsList()
        {
            int ibtType = RequestHelper.GetValInt("ibtType");
            int ibtObjId = RequestHelper.GetValInt("ibtObjId");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            List<Media> list = new List<Media>();
            if (ibtType == (int)Identifier.IBTType.Country)
                list = _supplierService.GetSpecialsOfCountry(ibtObjId, page, pageSize, out total).ToList();
            if (ibtType == (int)Identifier.IBTType.City)
                list = _IBTService.GetSpecialsOfCity(ibtObjId, page, pageSize, out total).ToList();
            itemTotal = list.Count();
            string str = SpecialsListData(list);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, str), JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// about
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="ibtObjId"></param>
        /// <returns></returns>
        public ActionResult About(int ibtType, int ibtObjId)
        {
            DateTime dt = DateTime.Now;
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    {
                        Country country = _IBTService.GetCountry(ibtObjId);
                        if (country != null)
                        {
                            ViewBag.CountCity = _IBTService.CityCount(ibtObjId);
                            ViewBag.CountSupply = _IBTService.SuppliersCount(ibtObjId);
                            ViewBag.CountMember = _IBTService.MembersCount(ibtObjId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(ibtObjId, 0, 0);
                            ViewBag.IbtObject = country;
                            ViewBag.CountryDes = _IBTService.GetCountryDes(ibtObjId);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                case (int)Identifier.IBTType.City:
                    {
                        City city = _IBTService.GetCity(ibtObjId);
                        if (city != null)
                        {
                            ViewBag.CountSupply = _IBTService.CountSupplierForCity(city.CityId);
                            ViewBag.CountMember = _IBTService.CountMembersCountForCity(city.CityId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(city.CountryId, city.CityId, 0);
                            ViewBag.IbtObject = city;
                            //城市描述为空时，获取该城市的国家描述
                            if (city.CityDescription == null)
                            {
                                ViewBag.CountryDes = _IBTService.GetCountryDes(city.CountryId);
                            }
                            else if (string.IsNullOrEmpty(city.CityDescription.Txt))
                            {
                                CountriesDe des = _IBTService.GetCountryDes(city.CountryId);
                                city.CityDescription.Txt = des == null ? "" : des.CountryTxt;
                            }
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                default:
                    {
                        return RedirectToAction("Index", "Home");
                    }
            }
            ViewBag.User = _profileService.GetUserInfo(UserId);
            ViewBag.RRWB = _IBTService.GetRRWB(UserId, ibtObjId, ibtType) ?? new RRWB();
            ViewBag.SearchTime = dt.ToString();
            ViewBag.IbtType = ibtType;
            return View();
        }

        /// <summary>
        /// NewsFeed
        /// </summary>
        /// <param name="ibtType">ibtType</param>
        /// <param name="ibtObjId">CountryID、CityID or SupplyId</param>
        /// <returns></returns>
        public ActionResult NewsFeed(int ibtType, int ibtObjId)
        {
            List<FeedList> feedList = new List<FeedList>();
            DateTime dt = DateTime.Now;
            int pageSize = 10;
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    {
                        Country country = _IBTService.GetCountry(ibtObjId);
                        if (country != null)
                        {
                            ViewBag.CountCity = _IBTService.CityCount(ibtObjId);
                            ViewBag.CountSupply = _IBTService.SuppliersCount(ibtObjId);
                            ViewBag.CountMember = _IBTService.MembersCount(ibtObjId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(ibtObjId, 0, 0);
                            ViewBag.IbtObject = country;
                            feedList = _IBTService.GetFeedList(ibtObjId, 0, 0, ibtType, dt, 1, pageSize).ToList();
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                case (int)Identifier.IBTType.City:
                    {
                        City city = _IBTService.GetCity(ibtObjId);
                        if (city != null)
                        {
                            ViewBag.CountSupply = _IBTService.CountSupplierForCity(city.CityId);
                            ViewBag.CountMember = _IBTService.CountMembersCountForCity(city.CityId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(city.CountryId, city.CityId, 0);
                            ViewBag.IbtObject = city;
                            feedList = _IBTService.GetFeedList(city.CountryId, city.CityId, 0, ibtType, dt, 1, pageSize).ToList();
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                default:
                    {
                        return RedirectToAction("Index", "Home");
                    }
            }
            ViewBag.User = _profileService.GetUserInfo(UserId);
            ViewBag.RRWB = _IBTService.GetRRWB(UserId, ibtObjId, ibtType) ?? new RRWB();
            ViewBag.FeedList = feedList;
            ViewBag.SearchTime = dt.ToString();
            ViewBag.IbtType = ibtType;

            return View();
        }

        /// <summary>
        /// Photo Album
        /// </summary>
        /// <param name="ibtType">ibtType</param>
        /// <param name="ibtObjId">CountryID、CityID or SupplyId</param>
        /// <returns></returns>
        public ActionResult PhotoAlbum(int ibtType, int ibtObjId)
        {
            DateTime dt = DateTime.Now;
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    {
                        Country country = _IBTService.GetCountry(ibtObjId);
                        if (country != null)
                        {
                            ViewBag.CountCity = _IBTService.CityCount(ibtObjId);
                            ViewBag.CountSupply = _IBTService.SuppliersCount(ibtObjId);
                            ViewBag.CountMember = _IBTService.MembersCount(ibtObjId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(ibtObjId, 0, 0);
                            ViewBag.IbtObject = country;
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                case (int)Identifier.IBTType.City:
                    {
                        City city = _IBTService.GetCity(ibtObjId);
                        if (city != null)
                        {
                            ViewBag.CountSupply = _IBTService.CountSupplierForCity(city.CityId);
                            ViewBag.CountMember = _IBTService.CountMembersCountForCity(city.CityId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(city.CountryId, city.CityId, 0);
                            ViewBag.IbtObject = city;
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                default:
                    {
                        return RedirectToAction("Index", "Home");
                    }
            }
            ViewBag.User = _profileService.GetUserInfo(UserId);
            ViewBag.RRWB = _IBTService.GetRRWB(UserId, ibtObjId, ibtType) ?? new RRWB();
            ViewBag.SearchTime = dt.ToString();
            ViewBag.IbtType = ibtType;
            return View();
        }

        /// <summary>
        /// specials
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="ibtObjId"></param>
        /// <returns></returns>
        public ActionResult Specials(int ibtType, int ibtObjId)
        {
            DateTime dt = DateTime.Now;
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    {
                        Country country = _IBTService.GetCountry(ibtObjId);
                        //判断国家是否存在
                        if (country != null)
                        {
                            ViewBag.CountCity = _IBTService.CityCount(ibtObjId);
                            ViewBag.CountSupply = _IBTService.SuppliersCount(ibtObjId);
                            ViewBag.CountMember = _IBTService.MembersCount(ibtObjId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, ibtObjId, 0, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(ibtObjId, 0, 0);
                            ViewBag.IbtObject = country;
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                case (int)Identifier.IBTType.City:
                    {
                        City city = _IBTService.GetCity(ibtObjId);
                        //判断城市是否存在
                        if (city != null)
                        {
                            ViewBag.CountSupply = _IBTService.CountSupplierForCity(city.CityId);
                            ViewBag.CountMember = _IBTService.CountMembersCountForCity(city.CityId);
                            ViewBag.IBTCount = _IBTService.IBTCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTFriendsCount = _IBTService.IBTFriendsCount(UserId, city.CountryId, city.CityId, 0);
                            ViewBag.IBTPoepleCount = _IBTService.IBTPeopleCount(city.CountryId, city.CityId, 0);
                            ViewBag.IbtObject = city;
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        break;
                    }
                default:
                    {
                        return RedirectToAction("Index", "Home");
                    }
            }
            ViewBag.User = _profileService.GetUserInfo(UserId);
            ViewBag.RRWB = _IBTService.GetRRWB(UserId, ibtObjId, ibtType) ?? new RRWB();
            ViewBag.SearchTime = dt.ToString();
            ViewBag.IbtType = ibtType;
            return View();
        }

    }

}
