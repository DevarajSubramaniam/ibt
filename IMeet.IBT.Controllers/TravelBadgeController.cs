﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Drawing;
using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Extensions;
using IMeet.IBT.Common.Helpers;
using IMeet.IBT.DAL;

namespace IMeet.IBT.Controllers
{
    public class TravelBadgeController:BaseController
    {
        private ITravelBadgeService _travelBadgeService;

        public TravelBadgeController(ITravelBadgeService travelBadgeService)
        {
            _travelBadgeService = travelBadgeService;
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Earning()
        {
            return View();
        }

        public ActionResult Achievement()
        {
            return View();
        }

        public ActionResult Rewards()
        {
            return View();
        }

        /// <summary>
        /// badge specials page
        /// </summary>
        /// <returns></returns>
        public ActionResult BadgeSpecials() 
        {
            ViewBag.list = _travelBadgeService.GetBadgeSpecials();

            return View();
        }

        /// <summary>
        /// get the basge special
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult BadgeSpecial(int id)
        {
            ViewBag.badgeSpecial = _travelBadgeService.GetBadgeSpecial(id);

            return View();
        }

        /// <summary>
        /// badge special get point for user
        /// </summary>
        /// <returns></returns>
        public JsonResult BadgeSpecialGetPoint()
        {
            int id = RequestHelper.GetValInt("id");

            bool bmSuccess = _travelBadgeService.GetPoint(id, UserId);

            BoolMessage bm = new BoolMessage(bmSuccess, "");

            return Json(bm);
        }

        //public string SendEmailToIsActive()
        //{
        //    _profileService.SendEmailToIsActive();
        //    return "Send success";
        //}

        /// <summary>
        /// Badge specials over view
        /// </summary>
        /// <returns></returns>
        public ActionResult OverView()
        {
            
            return View();
        }


    }
}
