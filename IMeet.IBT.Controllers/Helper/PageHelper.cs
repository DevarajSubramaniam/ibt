﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using StructureMap;
using IMeet.IBT.Services;

namespace IMeet.IBT.Controllers
{
   public static class PageHelper
    {
        /// <summary>
        /// 获取用户头像
        /// </summary>
        /// <param name="avatarSize">avatarSize:100X100,75X75,55X55,50X50,36X36,26X26</param>
        /// <returns></returns>
       public static string GetUserAvatar(int userId,int avatarSize){
           var _profileService = ObjectFactory.GetInstance<IProfileService>();
           return _profileService.GetUserAvatar(userId, avatarSize);
       }
       public static string GetUserCutAvatar(int userId, int avatarSize)
       {
           var _profileService = ObjectFactory.GetInstance<IProfileService>();
           return _profileService.GetUserCutAvatar(userId, avatarSize);
       }
        /// <summary>
        /// 获取supply的封面图片（单张）
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="coverPhotoSrc"></param>
        /// <param name="size"></param>
        /// <returns></returns>
       public static string GetSupplyPhoto(int supplyId, string coverPhotoSrc, Identifier.SuppilyPhotoSize size)
       {
           var _supplyService = ObjectFactory.GetInstance<ISupplierService>();
           return _supplyService.GetSupplyPhoto(supplyId, coverPhotoSrc, size);
       }

       /// <summary>
       /// 返回地图的图标
       /// </summary>
       /// <param name="typ">0：blue ，1：orange</param>
       /// <param name="supplyId"></param>
       /// <returns></returns>
       public static string GetSupplyMapsIcon(int supplyId,int typ=0)
       {
           var _supplyService = ObjectFactory.GetInstance<ISupplierService>();

           var res = _supplyService.GetSupplyTypes(supplyId);
           if (res != null && !string.IsNullOrWhiteSpace(res.IconMap))
           {
               if (typ == 0)
                   return res.IconMap;
               else
                   return res.IconMap2;
           }
           else
           {
               if (typ == 0)
                   return "/images/sign/blue/hotel.png";
               else
                   return "/images/sign/orange/hotel.png";
           }
       }

       /// <summary>
       /// 返回****
       /// </summary>
       /// <param name="rating"></param>
       /// <returns></returns>
       public static string GetRatingString(decimal rating)
       {
           System.Text.StringBuilder sb = new StringBuilder();
           int _rating = Convert.ToInt32(rating);

           for (int i = 0; i < _rating; i++)
           {
               sb.Append("<span class=\"starW12 full\"></span>");
           }
           for (int i = 0; i < 5 - _rating; i++)
           {
               sb.Append("<span class=\"starW12 \"></span>");
           }
           return sb.ToString() ;
       }

       /// <summary>
       /// 根据cityID返回countryName,CityName
       /// </summary>
       /// <param name="cityId"></param>
       /// <returns></returns>
       public static CountryCityItem GetCountryCityName(int cityId)
       {
           var _supplyService = ObjectFactory.GetInstance<ISupplierService>();
           return _supplyService.GetCountryCityInfo(cityId);
       }

       #region Country Flag
       public static string GetCountryFlagToConvert(string flagSrc)
       {
           if (flagSrc.IndexOf("PNG") > 0 && flagSrc.IndexOf("flag") > 0)
           {
               flagSrc = flagSrc.Replace("PNG", "gif").Replace("flag","flag_170");
               return flagSrc.ToString();
           }
           else
           {
               return flagSrc.ToString();
           }

       }
       /// <summary>
       /// 获取Interactive supply的封面图片（单张）
       /// </summary>
       /// <param name="supplyId"></param>
       /// <param name="coverPhotoSrc"></param>
       /// <param name="size"></param>
       /// <returns></returns>
       public static string GetInterSupplyPhoto(int supplyId, string coverPhotoSrc, Identifier.SuppilyPhotoSize size)
       {
           var _supplyService = ObjectFactory.GetInstance<ISupplierService>();
           return _supplyService.GetInterSupplyPhoto(supplyId, coverPhotoSrc, size);
       }
       #endregion

        #region 获取Post visit url
       /// <summary>
       /// 根据ibtType跟objId返回相应到的Postvisit页面
       /// </summary>
       /// <param name="ibtType"></param>
       /// <param name="objId"></param>
       /// <returns></returns>
       public static string PostVisitNUrl(int ibtType, int objId)
       {
           switch (ibtType)
           {
               case 3:
                   return "/postvisitn/supply/" + objId.ToString();
               case 2:
                   return "/postvisitn/city/" + objId.ToString();
               case 1:
                   return "/postvisitn/country/" + objId.ToString();
               default:
                   return "#";
           }
       }
        #endregion
    }
}
