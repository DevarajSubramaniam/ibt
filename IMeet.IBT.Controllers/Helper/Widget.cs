﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using StructureMap;
using IMeet.IBT.Services;
using IMeet.IBT.Common;

namespace IMeet.IBT.Controllers.Helper
{
   public static class Widget
    {
        /// <summary>
        /// Media (Special Offers)
        /// </summary>
        /// <returns></returns>
        public static IList<Media> GetWidgetSpecialOffersIBT(string localUrl, out int showType)
        {
            var widget = ObjectFactory.GetInstance<IWidgetService>();

            System.Random rd = new Random();
            int num = rd.Next(0, 11);
            if (num < 4)
            {
                showType = 1;
                return widget.GetWidgetOfPosition(2, Identifier.PositionTypes.Promotions, localUrl);
            }
            else if (num < 7)
            {
                showType = 2;
                return widget.GetWidgetOfPosition(2, Identifier.PositionTypes.Promotions, localUrl);
            }
            else
            {
                showType = 0;
                return widget.GetWidgetOfPosition(2, Identifier.PositionTypes.Promotions, localUrl);
            }

        }
        /// <summary>
        /// Media (Ads)
        /// </summary>
        /// <returns></returns>
        public static IList<Media> GetWidgetAdsIBT(string localUrl, out int showType)
        {
            var widget = ObjectFactory.GetInstance<IWidgetService>();

            System.Random rd = new Random();
            int num = rd.Next(0, 11);
            if (num < 4)
            {
                showType = 1;
                return widget.GetWidgetOfPosition(1, Identifier.PositionTypes.Advertisement, localUrl);
            }
            else if (num < 7)
            {
                showType = 2;
                return widget.GetWidgetOfPosition(1, Identifier.PositionTypes.Advertisement, localUrl);
            }
            else
            {
                showType = 0;
                return widget.GetWidgetOfPosition(1, Identifier.PositionTypes.Advertisement, localUrl);
            }

        }
       /// <summary>
       /// IBT
       /// </summary>
       /// <returns></returns>
       public static IList<WidgetIBT> GetWidgetIBT(out int showType)
       {
           var widget = ObjectFactory.GetInstance<IWidgetService>();

           System.Random rd = new Random();
           int num = rd.Next(0, 11);
           if (num < 4)
           {
               showType = 1;
               //先隐藏City
               return widget.GetWidgetRecommonIBT(Identifier.IBTType.City);
               //return widget.GetWidgetRecommonIBT(Identifier.IBTType.Country);
           }
           else if (num < 7)
           {
               showType = 2;
               return widget.GetWidgetRecommonIBT(Identifier.IBTType.Country);
           }
           else
           {
               showType = 3;
               return widget.GetWidgetAllCountryIBT("");
           }
       }

       public static IList<City> GetCitiesByCids(int[] cids)
       {
           var supplier = ObjectFactory.GetInstance<SupplierService>();

           return supplier.GetCityListByCityids(cids);
       }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="tabId">1 Countries 2 Cities 3 Hotels 4 LIFESTYLE 5 ATTRACTIONS</param>
       /// <param name="userId">用户ID，如果是0就是所有（首页的)</param>
       /// <returns></returns>
       public static int IbtStatistics(int tabId,int userId)
       {
           var _supplyService = ObjectFactory.GetInstance<ISupplierService>();

           return _supplyService.IbtStatistics(userId, tabId);
       }

       /// <summary>
       /// Last seen at
       /// </summary>
       /// <param name="userId"></param>
       /// <returns></returns>
       public static BoolMessage ProfileLastSeenAt(int userId)
       {
           var _supplyService = ObjectFactory.GetInstance<ISupplierService>();
           return _supplyService.LastPosteVistName(userId);
       }

    }
}
