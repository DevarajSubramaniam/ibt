﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Extensions;
using IMeet.IBT.Common.Helpers;

using IMeet.IBT.DAL;

namespace IMeet.IBT.Controllers
{
    /// <summary>
    /// 页面wedget的操作时用
    /// </summary>
   public class WidgetController : BaseController
    {
       private IWidgetService _widgetService;
       private ISupplierService _supplierService;

       public WidgetController(IWidgetService widgetService, ISupplierService supplierService)
       {
           _widgetService = widgetService;
           _supplierService = supplierService;
       }

        #region I'VE BEEN THERE

       public JsonResult IBTCountryList()
       {
           string letter = RequestHelper.GetVal("letter");
           string selObjIds = RequestHelper.GetVal("selObjIds");

           var list = _widgetService.GetWidgetAllCountryIBT(letter);
           System.Text.StringBuilder sb = new System.Text.StringBuilder();
           string[] selValIds = null;
           if (!string.IsNullOrWhiteSpace(selObjIds))
           {
               selValIds = selObjIds.Split(','); //选中了那些国家
           }
           string uncheckbox = "";
           foreach (var item in list)
	        {
		        sb.Append("<li><span class=\"name\">"+ item.ObjName +"</span>");
                if (selValIds != null && selValIds.Contains(item.ObjId.ToString()))
                {
                    uncheckbox = " uncheckbox";
                }
                sb.Append("<span class=\"checkbox "+ uncheckbox +"\" onclick=\"widget.checkbox(this);\"  data-objId=\""+ item.ObjId +"\" ></span></li>");
	        }
           
           return Json(new BoolMessage(true, sb.ToString()), JsonRequestBehavior.AllowGet);
       }

       public JsonResult IBTSave()
       {
           //todo:如果没登录的ibtSave，需要在js处理。
           int ibt_ibtType = RequestHelper.GetValInt("ibt_ibtType"); //1 or 2 ,如果是2就需要根据id获取国家ID
           string ibt_ObjIds = RequestHelper.GetVal("ibt_ObjIds");

           if (ibt_ibtType == 1) //国家
           {
               var ids = ibt_ObjIds.Split(',');
               foreach (var item in ids)
	            {
		          if (Utils.IsNumeric(item))
                   {
                       _supplierService.IBeenThere(0,UserId, Convert.ToInt32(item), 0, 0);
                   }
	            }
               return Json(new BoolMessage(true, ""), JsonRequestBehavior.AllowGet);
           }
           else if (ibt_ibtType == 2)
           { //city
               var ids = ibt_ObjIds.Split(',');
               int countryId;
               foreach (var item in ids)
               {
                   if (Utils.IsNumeric(item)) //item is cityId
                   {
                       countryId = _supplierService.GetCityInfo(Convert.ToInt32(item)).CountryId;
                       _supplierService.IBeenThere(0,UserId,countryId, Convert.ToInt32(item), 0);
                   }
               }
               return Json(new BoolMessage(true, ""), JsonRequestBehavior.AllowGet);
           }
           else
           {//no
               return Json(new BoolMessage(false, ""), JsonRequestBehavior.AllowGet);
           }
       }
        #endregion

        #region Widget Statistics of count
       [HttpPost]
       [ValidateInput(false)]
       public JsonResult InitWidgetAriseCount(string returnUrl)
       {
           int mediaId = RequestHelper.GetValInt("objId");
           string url = Request.Url.AbsolutePath; 
           //string url = Request.Url.ToString();
           var bmi = _widgetService.InitWidgetAriseCount(mediaId,url);

           if (bmi.Success)
           {
           }
           return Json(bmi);
       }
             [HttpPost]
       [ValidateInput(false)]
       public JsonResult StatisticsWidgetHit(string returnUrl)
       {
           int tabId = RequestHelper.GetValInt("tabId",1);
           int objId = RequestHelper.GetValInt("objId");  //mediaId
           string url = RequestHelper.GetVal("localUrl");
           var bmi = _widgetService.StatisticsWidgetHit(objId, url);

           if (bmi.Success)
           {
           }
           return Json(bmi);
       }
        #endregion

    }
}
