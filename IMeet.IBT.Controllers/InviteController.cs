﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using IMeet.IBT.Services;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Helpers;

namespace IMeet.IBT.Controllers
{
    public class InviteController : BaseController
    {
        private ISNSConnectService _snsConnectService;
        public InviteController(ISNSConnectService snsConnectService)
        {
            _snsConnectService = snsConnectService;
        }

        public ActionResult Facebook()
        {
            //过期
            //string token = "AAACEdEose0cBAOhllIBck7RntJEUPwDt4E7COrGBQRT7aoWddvtNwPK5GWEe14JfnNNG9bckOZAmN4Ibe9KFedn8HcWED5EYZCYD27AMPwWcELuWSI";

            //没过期
            string token = getToken(); //"AAABky546PGMBAE8LA7H4GP0vdDcqZBbrAgJpmP9ZCZAgE0fijpTibEnW8peBHiT7j7sYtTLJAHf6P05DLUWZCZBBal7azG3cWnaSZBTNJJNgZDZD";
            
            //现在检查access_token是否过期,如果不检查已经过期。就是返回是空的
            var bm = SNSHelper.FacebookClient.CheckTokenHasExpired(token);

            ViewBag.Tips = "";
            var allFriendList = new List<SNSHelper.friendData>();
            var joinIBTList = new List<SNSHelper.friendData>();
            if (!bm.Success)
            {
                ViewBag.Tips = "Token has Expired!";
                ViewBag.FriendList = allFriendList;
                ViewBag.joinIBTList = joinIBTList;
            }
            else
            {

                //然后才获取数据
                allFriendList = SNSHelper.FriendList.GetFriendList(token);

                joinIBTList = allFriendList.Where(c => c.installed.HasValue && c.installed.Value.Equals(true)).ToList();
                List<string> ids = new List<string>();
                foreach (var item in joinIBTList)
                {
                    ids.Add(item.id);
                }
                Dictionary<string, int> dic = _snsConnectService.GetUserIdBySnsId(ids, (int)Identifier.SnsType.Facebook);
                foreach (var item in joinIBTList)
                {
                    if (dic.ContainsKey(item.id))
                    {
                        item.UserId = dic[item.id];
                    }
                }
                ViewBag.FriendList = allFriendList.Take(30).ToList();
                ViewBag.joinIBTList = joinIBTList.Where(c => c.UserId.HasValue).ToList();
            }

            return View();
        }

        private string getToken()
        {
            //过期
            //string token = "AAACEdEose0cBAOhllIBck7RntJEUPwDt4E7COrGBQRT7aoWddvtNwPK5GWEe14JfnNNG9bckOZAmN4Ibe9KFedn8HcWED5EYZCYD27AMPwWcELuWSI";
            string key = "fb_token_"+ UserId.ToString();
            return  _cache.GetOrInsert<string>(key, 1200, true, () =>
            {
                var sns = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Facebook);
                if (sns != null && !string.IsNullOrWhiteSpace(sns.OauthToken))
                {
                    return sns.OauthToken;
                }
                else
                {
                    return "";
                }
            });
        }

        [ValidateInput(false)]
        public JsonResult FacebookFriendSearch()
        {
            string token = getToken(); //"AAABky546PGMBAE8LA7H4GP0vdDcqZBbrAgJpmP9ZCZAgE0fijpTibEnW8peBHiT7j7sYtTLJAHf6P05DLUWZCZBBal7azG3cWnaSZBTNJJNgZDZD";
            string key = RequestHelper.GetUrlDecodeVal("k");
            var allFriendList = SNSHelper.FriendList.GetFriendList(token);

            var list = allFriendList;
            if(!string.IsNullOrWhiteSpace(key)){
                 list = allFriendList.Where(c => c.name.IndexOf(key) > -1).ToList();
            }


            System.Text.StringBuilder sb = new StringBuilder();
            
            foreach (var item in list)
	        {
               sb.Append("<li id=\"fb_"+ item.id +"\">");
               sb.Append("   <div class=\"friend\">");
               sb.Append("       <a href=\"http://www.facebook.com/"+ item.id+ "\" target=\"_blank\"><img alt=\""+ item.name+ "\" width=\"35px\" src=\""+ item.picture.data.url+ "\"></a>");
               sb.Append(item.name);
               sb.Append("   </div>");
               sb.Append("   <div class=\"btn\"><a class=\" btnBlueH33\" href=\"javascript:void(0);\" onclick=\"FacebookInviteFriends('" + item.id + "','" + item.name + "')\"><span class=\"bg\">+ INVITE</span></a></div>");
               sb.Append("</li>");
	        }

            return Json(new BoolMessage(true,sb.ToString()),JsonRequestBehavior.AllowGet);
        }


    }
}
