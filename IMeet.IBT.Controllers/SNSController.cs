﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Extensions;
using IMeet.IBT.Common.Helpers;

using IMeet.IBT.DAL;

using System.Configuration;
using System.Net;
using DotNetOpenAuth.OAuth2;
using IMeet.IBT.Controllers.SNSHelper;
using System.IO;

namespace IMeet.IBT.Controllers
{
    public class SNSController : BaseController
    {
        private IAccountService _accountService;
        private ISNSConnectService _snsConnectService;
        private ISupplierService _supplierService;
        public SNSController(IAccountService accountService, ISNSConnectService snsConnectService, ISupplierService supplierService)
        {
            _accountService = accountService;
            _snsConnectService = snsConnectService;
            _supplierService = supplierService;
        }

        #region Facebook
        /// <summary>
        /// facebook login/register
        /// </summary>
        /// <returns></returns>
        public ActionResult Facebook()
        {
            //int userId = RequestHelper.GetValInt("userId", 0);
            FacebookClient client = new FacebookClient
            {
                ClientIdentifier = ConfigurationManager.AppSettings["facebookAppID"],
                ClientCredentialApplicator = ClientCredentialApplicator.PostParameter(ConfigurationManager.AppSettings["facebookAppSecret"]),
            };

            IAuthorizationState authorization = client.ProcessUserAuthorization();
            string token = string.Empty;
            if (authorization == null)
            {
                client.RequestUserAuthorization();
            }
            else
            {
                token = authorization.AccessToken;
                //获取个人的资料： firstName,lastName,country,city,emali,profileImg

                var request = WebRequest.Create("https://graph.facebook.com/me?scope=email&fields=id,name,email,first_name,last_name,picture&access_token=" + Uri.EscapeDataString(authorization.AccessToken));
                var graph = new FacebookGraph();
                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        var reader = new System.IO.StreamReader(responseStream, System.Text.Encoding.UTF8);
                        string responseString = reader.ReadToEnd();
                        //System.Web.HttpContext.Current.Response.Write(responseString);
                        //graph = FacebookGraph.Deserialize(responseStream);
                        graph = FacebookGraph.Deserialize(responseString);
                        /* {"id":"100000396765290","name":"Randy Ran","email":"randy\u0040oubk.com","first_name":"Randy","last_name":"Ran","link":"http:\/\/www.facebook.com\/randy.ran.12327","picture":{"data":{"url":"http:\/\/profile.ak.fbcdn.net\/hprofile-ak-prn1\/161258_100000396765290_8284559_q.jpg","is_silhouette":false}}} */
                        //this.nameLabel.Text = HttpUtility.HtmlEncode(graph.Name + "," + graph.Email + ",picture:" + graph.Picture.data.url);
                    }
                }
                //然后查看数据是否有记录。如果有就获取用户ID然后直接登录
                var bm = SNSBind(token, "", Identifier.SnsType.Facebook, graph.id.ToString(), graph.first_name, graph.last_name, graph.email, "", "");

                return Redirect(bm.Message);
            }
            return Redirect("/Account/signup");
        }

        #endregion

        #region LinkedIn
        public ActionResult LinkedInCallback()
        {
            if (LinkedInConsumer.IsLinkedInConsumerConfigured)
            {
                var response = SNSHelper.LinkedInConsumer.LinkedInSignIn.ProcessUserAuthorization();
                if (response == null)
                {
                    return Redirect("/Account/signup");
                }
                string accessToken = response.AccessToken;
                string tokenSecret = SNSHelper.LinkedInConsumer.ShortTermUserSessionTokenManager.GetTokenSecret(response.AccessToken);
                //获取个人资料id,firstName,lastName,country,city,emali
                SNSHelper.LinkedInBaseInfo linkedInInfo = SNSHelper.LinkedInConsumer.GetUserBaseInfo(response.AccessToken);
                var bm = SNSBind(accessToken, tokenSecret, Identifier.SnsType.LinkedIn, linkedInInfo.Id, linkedInInfo.FirstName, linkedInInfo.LastName, linkedInInfo.Email, linkedInInfo.City, linkedInInfo.country);
                //return Redirect("/home?token=" + response.AccessToken + ",sc=" + SNSHelper.LinkedInConsumer.ShortTermUserSessionTokenManager.GetTokenSecret(response.AccessToken));
                return Redirect(bm.Message);
            }
            return Redirect("/Account/signup");
        }


        public ActionResult LinkedIn()
        {
            LinkedInConsumer.StartSignInWithhLinkedIn().Send();

            return new EmptyResult();
        }

        /// <summary>
        /// linkedin share
        /// </summary>
        /// <returns></returns>
        public JsonResult LinkedinShare()
        {
            string xml = @"<share>
                          <comment>Check out the LinkedIn Share API! test " + DateTime.Now.ToString() + @"</comment>
                          <content>
                            <title>LinkedIn Developers Documentation On Using the Share API</title>
                            <description>Leverage the Share API to maximize engagement on user-generated content on LinkedIn," + DateTime.Now.ToString() + @"</description>
                            <submitted-url>https://developer.linkedin.com/documents/share-api</submitted-url>
                            <submitted-image-url>http://m3.licdn.com/media/p/3/000/124/1a6/089a29a.png</submitted-image-url> 
                          </content>
                          <visibility> 
                            <code>anyone</code> 
                          </visibility>
                        </share>";
            var bm = SNSHelper.LinkedInConsumer.Share("2cb543f3-8ccd-4edb-be93-83f9c9df39cb", xml);

            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PicUrl()
        {
            var bm = SNSHelper.LinkedInConsumer.GetUserOriginalPictureUrl("2b7bdc68-6eaf-4c62-8e40-5d104fa9c65f");
            return Json(bm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Twitter
        public ActionResult TwitterCallback()
        {
            if (!TwitterConsumer.IsTwitterConsumerConfigured)
            {
                return Redirect("/Account/signup");
            }

            var response = SNSHelper.TwitterConsumer.TwitterSignIn.ProcessUserAuthorization();
            if (response == null)
            {
                return Redirect("/Account/signup");
            }

            string accessToken = response.AccessToken;
            string tokenSecret = SNSHelper.LinkedInConsumer.ShortTermUserSessionTokenManager.GetTokenSecret(response.AccessToken);
            //获取个人资料id,firstName,lastName,country,city,emali
            SNSHelper.TwitterBaseInfo inInfo = SNSHelper.TwitterConsumer.GetUserBaseInfo(response.AccessToken);
            var bm = SNSBind(accessToken, tokenSecret, Identifier.SnsType.Twitter, inInfo.Id, inInfo.FirstName, inInfo.LastName, inInfo.Email, inInfo.City, inInfo.country);
            return Redirect(bm.Message);
        }

        public ActionResult Twitter()
        {
            string isReLogin = RequestHelper.GetVal("reLogin");
            TwitterConsumer.StartSignInWithTwitter(isReLogin == "true" ? true : false).Send();

            return new EmptyResult();
        }

        public JsonResult TwitterUpdateStatus()
        {
            var rst = SNSHelper.TwitterConsumer.StatusesUpdate("123596341-opaldaz1RWmPRM45LMSdb3UEJtvktVAyYljvmRmW", "test api " + DateTime.Now.ToString());

            return Json(rst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TwitterUpdateStatus2()
        {
            string path = System.Web.HttpContext.Current.Request.MapPath("~/images/logo/logo.gif");
            var rst = SNSHelper.TwitterConsumer.StatusesUpdate("123596341-opaldaz1RWmPRM45LMSdb3UEJtvktVAyYljvmRmW", "test api2 " + DateTime.Now.ToString(), path);

            return Json(rst, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region i-Meet

        public ActionResult iMeetCallback()
        {
            string strToken = RequestHelper.GetVal("access_token");
            string strEmail = RequestHelper.GetVal("imeet_uid");
            if (!string.IsNullOrWhiteSpace(strToken) && !string.IsNullOrWhiteSpace(strEmail))
            {

                SNSHelper.IMeetBaseInfo imeetBaseInfo = SNSHelper.iMeetConsumer.GetUserBaseInfo(strToken, strEmail);
                if (!string.IsNullOrWhiteSpace(imeetBaseInfo.Email))
                {
                    var bm = SNSBind(strToken, strToken, Identifier.SnsType.IMeet, imeetBaseInfo.Email, imeetBaseInfo.FirstName, imeetBaseInfo.LastName, imeetBaseInfo.Email, imeetBaseInfo.City, imeetBaseInfo.Country);
                    return Redirect(bm.Message);
                }
            }
            return Redirect("/Account/signup");
        }

        public ActionResult iMeet()
        {
            iMeetConsumer.SignUpWithiMeet();
            return Redirect("/Account/signup");
        }

        #endregion

        /// <summary>
        /// 登录，注册，绑定都是他搞定：D
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="tokenSecret"></param>
        /// <param name="snsType"></param>
        /// <param name="snsId"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="city"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        private BoolMessage SNSBind(string accessToken, string tokenSecret, Identifier.SnsType snsType, string snsId, string firstName, string lastName, string email, string city, string country)
        {
            BoolMessage bm = new BoolMessage(false, "/Account/signup");
            IAccountLogService accountLogService = new AccountLogService();
            //然后查看数据是否有记录。如果有就获取用户ID然后直接登录
            var snsItem = _snsConnectService.GetSNSConnect(snsId, snsType);
            if (snsItem != null && snsItem.UserId > 0)
            {
                //更新token
                snsItem.OauthToken = accessToken;
                snsItem.OauthTokenSecret = tokenSecret;
                snsItem.Comment = DateTime.Now.ToString();

                _snsConnectService.UpdateSNSConnect(snsItem);
                _accountService.SignIn(snsItem.UserId, false);
                accountLogService.WriteSns(snsItem.UserId, snsType); //写登录日志
                bm = new BoolMessage(true, "/home");
            }
            else
            {
                //这里需要检查email是否注册,如果注册直接绑定
                DAL.User userEmail = _accountService.GetUserByEmail(email);
                //if (userEmail != null && userEmail.UserId > 0)
                //{
                //    bm = new BoolMessage(false, "/Account/signup?msg=email is exists!");
                //    return bm;
                //}
                if (User.Identity.IsAuthenticated || userEmail.UserId > 0)
                {
                    int userId = userEmail.UserId;
                    userId = User.Identity.IsAuthenticated ? Convert.ToInt32(User.Identity.Name) : userId;
                    //是绑定。添加完了直接返回到/profile
                    _snsConnectService.AddSNSConnect(userId, snsId, (int)snsType, accessToken, tokenSecret);
                    if (userEmail.UserId > 0)
                    {
                        //登录
                        _accountService.SignIn(userId, false);
                        //写日志
                        accountLogService.WriteSns(userId, snsType);
                    }
                    //return Redirect("/profile");
                    bm = new BoolMessage(true, "/Account/AccountSetting");
                }
                else
                {
                    bm = new BoolMessage(true, "/Account/BCreateUser?accessToken=" + accessToken + "&tokenSecret=" + tokenSecret + "&snsType=" + snsType + "&snsId=" + snsId + "&firstName=" + firstName + "&lastName=" + lastName + "&email=" + email + "&city=" + city + "&country=" + country);
                    //return Redirect("newUrl?fir..."); //firstName lastName  email  country,city,(int)snsType
                    //..
                    //没有记录就先添加用户
                    //string statisticsCountry = _supplierService.IbtStatistics(1, 0).ToString();
                    //string statisticsCity = _supplierService.IbtStatistics(2, 0).ToString();
                    //string statisticsHotel = _supplierService.IbtStatistics(3, 0).ToString();
                    //string statisticsVenue = _supplierService.IbtStatistics(4, 0).ToString();
                    //string statisticsOther = _supplierService.IbtStatistics(5, 0).ToString();

                    ////todo:这里需要把城市名称转换成int 
                    //int countryId = 0;
                    //int cityId = 0;
                    ////todo:如果sns过来的没有email.随机生成一个email@ibt-sns.com
                    //email = string.IsNullOrWhiteSpace(email) ? System.Guid.NewGuid().ToString().Substring(0, 12) + "@ibt-sns.com" : email;
                    //var bmi = _accountService.CreateUser(firstName, lastName, System.Guid.NewGuid().ToString(), email, countryId, cityId, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther,3, source: snsType.ToString());

                    //if (bmi.Success)
                    //{
                    //    //todo:SNS头像生成

                    //    //然后绑定
                    //    _snsConnectService.AddSNSConnect(bmi.Item, snsId, (int)snsType, accessToken, tokenSecret);

                    //    //然后登录
                    //    _accountService.SignIn(bmi.Item, false);

                    //    //return Redirect("/home");
                    //    bm = new BoolMessage(true, "/home");
                    //}
                }
            }
            return bm;
        }

    }
}
