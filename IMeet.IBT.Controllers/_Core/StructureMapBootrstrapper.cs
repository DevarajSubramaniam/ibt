﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using StructureMap;
using StructureMap.Configuration.DSL;

using IMeet.IBT.Services;
using IMeet.IBT.Common.Caching;
using IMeet.IBT.Common.Logging;

namespace IMeet.IBT.Controllers
{
    public class StructureMapBootrstrapper
    {
        public static void Configure()
        {
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<ControllerRegistry>();

            });
        }

        public class ControllerRegistry : Registry
        {
            public ControllerRegistry()
            {
                //cache
                For<ICache>().Use<CacheAspNet>();
                //logging
                For<ILogger>().Use<Log4NetAdapter>();

                For<IAccountService>().Use<AccountService>();

                For<ISupplierService>().Use<SupplierService>();
                //测试的
                For<IHelloService>().Use<HelloService>();

                For<IConnectionService>().Use<ConnectionService>();

                For<IProfileService>().Use<ProfileService>();

                For<IWidgetService>().Use<WidgetService>();
                For<INewsFeedService>().Use<NewsFeedService>();
                For<ISearchService>().Use<SearchService>();
                For<IMapsService>().Use<MapsService>();
                For<ISNSConnectService>().Use<SNSConnectService>();
                For<IIBTService>().Use<IBTService>();
                For<ITravelBadgeService>().Use<TravelBadgeService>();
                For<IAdminEmailService>().Use<AdminEmailService>();
            }
        }
    }

}
