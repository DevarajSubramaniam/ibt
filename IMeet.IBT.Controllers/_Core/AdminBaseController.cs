﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.Mvc;

using StructureMap;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Logging;
using IMeet.IBT.Services;

namespace IMeet.IBT.Controllers
{
    public class AdminBaseController:Controller
    {
        public ILogger _logger;
        public IProfileService _profileService;

        public AdminBaseController()
        {
            ViewBag.Title = Title;
            ViewBag.Keywords = Keywords;
            ViewBag.Description = Description;
            _logger = ObjectFactory.GetInstance<ILogger>();
            _profileService = ObjectFactory.GetInstance<IProfileService>();
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;

            string[] allowPage = System.Configuration.ConfigurationManager.AppSettings["AdminAllowPage"].Split('|');

            if (!allowPage.Contains(controllName.ToLower() + "/" + actionName.ToLower()))
            {
                int? roleId = _profileService.GetUserInfo(UserId).RoleId;
                if (!roleId.HasValue && roleId != 1 && roleId != null)
                {
                    base.HttpContext.Response.Redirect("/home");
                    return;
                }
            }

            base.OnActionExecuting(filterContext);
        }
        /// <summary>
        /// 当前用户的ID
        /// </summary>
        public int UserId
        {
            get { return _profileService.GetUserId(); }
        }

        private string _defaultTitle = "I've Been There!";
        /// <summary>
        /// 页面标题
        /// </summary>
        public string Title
        {
            get {return ViewBag.Title == null? _defaultTitle : ViewBag.Title;}
            set { ViewBag.Title = value + " -- I've Been There!"; }
        }

        /// <summary>
        /// 页面标题
        /// </summary>
        public string PageTitle
        {
            get { return ViewBag.PageTitle == null ? _defaultTitle : ViewBag.Title; }
            set { ViewBag.PageTitle = value; }
        }
        /// <summary>
        /// 2级标题
        /// </summary>
        public string PageTitle2
        {
            get { return ViewBag.PageTitle2 == null ? string.Empty : ViewBag.Title; }
            set { ViewBag.PageTitle2 = value; }
        }

        private string _defaultKeywords = "I've Been There!";
        /// <summary>
        /// 页面的关键字
        /// </summary>
        public string Keywords
        {
            get { return ViewBag.Keywords == null ? _defaultKeywords : ViewBag.Keywords; }
            set { ViewBag.Keywords = value + " -- I've Been There!"; }
        }

        private string _defaultDescription = "I've Been There!";
        /// <summary>
        /// 页面的描述
        /// </summary>
        public string Description
        {
            get { return ViewBag.Description == null ? _defaultDescription : ViewBag.Description; }
            set { ViewBag.Description = value + " -- I've Been There!"; }
        }
    }
}
