﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using StructureMap;

namespace IMeet.IBT.Controllers
{
    public class IoCControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType != null)
            {
                return ObjectFactory.GetInstance(controllerType) as IController;
            }
            else
            {
                try
                {
                    return base.GetControllerInstance(requestContext, controllerType);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
