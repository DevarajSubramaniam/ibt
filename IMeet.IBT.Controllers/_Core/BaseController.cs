﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.Mvc;

using StructureMap;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Logging;
using IMeet.IBT.Common.Caching;
using IMeet.IBT.Services;

namespace IMeet.IBT.Controllers
{
    public class BaseController:Controller
    {
        public ICache _cache;
        public ILogger _logger;
        public IProfileService _profileService;

        public BaseController()
        {
            ViewBag.Title = Title;
            ViewBag.Keywords = Keywords;
            ViewBag.Description = Description;
            _logger = ObjectFactory.GetInstance<ILogger>();
            _cache = ObjectFactory.GetInstance<ICache>();
            _profileService = ObjectFactory.GetInstance<IProfileService>();
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;

            string[] allowPage = System.Configuration.ConfigurationManager.AppSettings["AllowPage"].Split('|');

            if (!allowPage.Contains(controllName.ToLower() + "/" + actionName.ToLower()) && _profileService.GetUserId() <1)
            {
                if (actionName != "Login_popup")//TestCode
                {
                    base.HttpContext.Response.Redirect("/home/index"); 
                    return;
                }                
            }
            else if (!allowPage.Contains(controllName.ToLower() + "/" + actionName.ToLower()) && _profileService.GetUserId() > 1)
            {
                if (actionName == "Login_popup")//TestCode
                {
                    base.HttpContext.Response.Redirect("/home");
                    return;
                }               
            }
            
             //var area = filterContext.RouteData.Values["area"];
            //var controller = filterContext.RouteData.Values["controller"];
            //var action = filterContext.RouteData.Values["action"];
            //string verb = filterContext.HttpContext.Request.HttpMethod;

            base.OnActionExecuting(filterContext);
        }
        /// <summary>
        /// 当前用户的ID
        /// </summary>
        public int UserId
        {
            get { return _profileService.GetUserId(); }
        }

        private string _defaultTitle = "I've Been There!";
        /// <summary>
        /// 页面标题
        /// </summary>
        public string Title
        {
            get {return ViewBag.Title == null? _defaultTitle : ViewBag.Title;}
            set { ViewBag.Title = value + ""; }//" -- I've Been There!" is replaced by the ""
        }

        private string _defaultKeywords = "I've Been There!";
        /// <summary>
        /// 页面的关键字
        /// </summary>
        public string Keywords
        {
            get { return ViewBag.Keywords == null ? _defaultKeywords : ViewBag.Keywords; }
            set { ViewBag.Keywords = value + " -- I've Been There!"; }
        }

        private string _defaultDescription = "I've Been There!";
        /// <summary>
        /// 页面的描述
        /// </summary>
        public string Description
        {
            get { return ViewBag.Description == null ? _defaultDescription : ViewBag.Description; }
            set { ViewBag.Description = value + " -- I've Been There!"; }
        }
    }
}
