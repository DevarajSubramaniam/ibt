﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using System.Drawing;
using IMeet.IBT.Services;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Extensions;
using IMeet.IBT.Common.Helpers;

using IMeet.IBT.DAL;
using System.IO;


namespace IMeet.IBT.Controllers
{
    public class SupplierController : BaseController
    {
        private ISupplierService _supplierService;
        private INewsFeedService _newsFeedService;
        private IMapsService _mapsService;
        private ISNSConnectService _snsConnectService;
        private IIBTService _ibtServices;
        private ITravelBadgeService _travelBadgeService;

        public SupplierController(ISupplierService supplierService,
                                  INewsFeedService newsFeedService,
                                  IMapsService mapsService,
                                  ISNSConnectService snsConnectService, IIBTService ibtServices, ITravelBadgeService travelBadgeService)
        {
            _supplierService = supplierService;
            _newsFeedService = newsFeedService;
            _mapsService = mapsService;
            _snsConnectService = snsConnectService;
            _ibtServices = ibtServices;
            _travelBadgeService = travelBadgeService;
        }

        #region Supply Detail
        public ActionResult Detail(string SupplyId)
        {
            string id = SupplyId.Split('-')[0];
            int supplyId = Int32.TryParse(id, out supplyId) ? supplyId : 0;
            Supply supply = _supplierService.GetSupplyDetail(supplyId);
            if (supply == null || supply.SupplyId < 1||supply.StatusId!=(int)Identifier.StatusType.Enabled)
            {
                return Redirect("/");
            }

            this.Title = supply.Title;
            this.Keywords = supply.Title;
            this.Description = StringHelper.Truncate(supply.Desp, 150, "...");


            ViewBag.Supply = supply;

            //Staffs' Contacts
            IList<StaffsContact> staffsContact = _supplierService.GetStaffsContactList(supply.SupplyId);
            ViewBag.StaffsContact = staffsContact;

            //RRWB
            ViewBag.RRWB = _supplierService.GetUserOnlyRRWB(UserId, supply.SupplyId, Identifier.IBTType.Supply);

            if (supply.SupplierTypes.HasValue && supply.SupplierTypes.Value == (int)Identifier.SupplierTypes.Sponsored)
            {
                //parent image
                int parentId = supply.ParentSupplyId.HasValue ? Convert.ToInt32(supply.ParentSupplyId) : 0;
                var parentSupply = _supplierService.GetSupplyDetail(parentId);
                parentSupply = parentSupply == null ? new Supply() : parentSupply;
                ViewBag.ParentImg = PageHelper.GetSupplyPhoto(parentSupply.SupplyId, parentSupply.CoverPhotoSrc, Identifier.SuppilyPhotoSize._mn);
                ViewBag.parentSupply = parentSupply;
                return View("Sponsored");
            }
            else if (supply.SupplierTypes.HasValue && supply.SupplierTypes.Value == (int)Identifier.SupplierTypes.Interactive)
            {

                return View("Interactive");
            }
            else
            {
                return View("Basic");
            }
        }

        #endregion

        #region  Special Offers
        public ActionResult SpecialAds(string SupplyId)
        {
            //Basic = 1,
            //Sponsored=2,
            //Interactive =3
            string id = SupplyId.Split('-')[0];
            int supplyId = Int32.TryParse(id, out supplyId) ? supplyId : 0;
            //Supply supply = _supplierService.GetSupplyDetail(Convert.ToInt32(id));
            Supply supply = _supplierService.GetSupplyDetail(supplyId);
            //Supply supply = _supplierService.GetSupplyDetail(Convert.ToInt32(SupplyId));
            if (supply == null || supply.SupplyId < 1)
            {
                return Redirect("/");
            }
            this.Title = supply.Title;
            this.Keywords = supply.Title;
            this.Description = StringHelper.Truncate(supply.Desp, 150, "...");
            ViewBag.Supply = supply;
            //Ads / Special Offers
            IList<Media> media = _supplierService.GetSpecialAdsList(supply.SupplyId);
            ViewBag.SpecialAds = media;
            if (supply.SupplierTypes.HasValue && supply.SupplierTypes.Value == (int)Identifier.SupplierTypes.Sponsored)
            {
                //parent image
                int parentId = supply.ParentSupplyId.HasValue ? Convert.ToInt32(supply.ParentSupplyId) : 0;
                var parentSupply = _supplierService.GetSupplyDetail(parentId);
                parentSupply = parentSupply == null ? new Supply() : parentSupply;
                ViewBag.ParentImg = PageHelper.GetSupplyPhoto(parentSupply.SupplyId, parentSupply.CoverPhotoSrc, Identifier.SuppilyPhotoSize._bn);
                ViewBag.parentSupply = parentSupply;
                return View("Sponsored_Specials");
            }
            else if (supply.SupplierTypes.HasValue && supply.SupplierTypes.Value == (int)Identifier.SupplierTypes.Interactive)
            {

                return View("Interactive_Specials");
            }
            else
            {
                return View("Basic_Specials");
            }
        }
        #endregion

        #region I've RRWB#我对目的地的评分/推荐/想去（去过）/愿望单
        public JsonResult RRWB()
        {
            /*
             * rrwbType 1.rating 2.Recommended 3.Want to Go/Go Back 4.Bucket List
             * ibtType: 1.Country 2.City 3.Supply
             */
            int postType = RequestHelper.GetValInt("postType");
            int ibtType = RequestHelper.GetValInt("ibtType");
            int rrwbType = RequestHelper.GetValInt("rrwbType");
            int objId = RequestHelper.GetValInt("objId");
            int val = RequestHelper.GetValInt("val");
            //接收的参数
            //# 类型1，类型1ID ,类型2，类型2ID
            //1-50-1-3 -->1 （国家），编号 50， 1 评分，3 (意思是给国家编号为50的评了3分)
            //#Recommended,Want to Go/Go Back,Bucket List 1是选中，0是未选
            Identifier.PostTypes enum_postType;  //new
            if (Enum.IsDefined(typeof(Identifier.PostTypes), postType))
                enum_postType = (Identifier.PostTypes)Enum.Parse(typeof(Identifier.PostTypes), postType.ToString(), true);
            else
                return Json(new BoolMessage(false, "PostType Error!"), JsonRequestBehavior.AllowGet);

            Identifier.RRWBType enum_rrwbType;
            if (Enum.IsDefined(typeof(Identifier.RRWBType), rrwbType))
                enum_rrwbType = (Identifier.RRWBType)Enum.Parse(typeof(Identifier.RRWBType), rrwbType.ToString(), true);
            else
                return Json(new BoolMessage(false, "RRWBType Error!"), JsonRequestBehavior.AllowGet);

            Identifier.IBTType enum_ibtType;
            if (Enum.IsDefined(typeof(Identifier.IBTType), ibtType))
                enum_ibtType = (Identifier.IBTType)Enum.Parse(typeof(Identifier.IBTType), ibtType.ToString(), true);
            else
                return Json(new BoolMessage(false, "IBTType Error!"), JsonRequestBehavior.AllowGet);

            var bm = _supplierService.RRWB(UserId, enum_postType, enum_ibtType, objId, enum_rrwbType, val);
            return Json(bm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region I've Been There

        public JsonResult IBT()
        {
            //hard code?
            var bm = _supplierService.IBeenThere(0, 1, 1, 0, 0);

            return Json(bm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Travels Add
        /// <summary>
        /// TravelsAdd step 1
        /// </summary>
        /// <returns></returns>
        public ActionResult TravelsAdd()
        {
            //如果未登录
            if (!User.Identity.IsAuthenticated)
            {
                string key = RequestHelper.GetVal("key");
                if (!string.IsNullOrEmpty(key))
                {
                    string strId = Common.Encrypt.EncodeHelper.AES_Decrypt(key);
                    int id = 0;
                    if (int.TryParse(strId, out id) && id > 0)
                    {
                        IAccountService _accountService = StructureMap.ObjectFactory.GetInstance<IAccountService>();
                        _accountService.SignIn(id, true);
                        return Redirect("/supplier/travelsadd");
                    }
                }
                return Redirect("/account/signup");
            }
            else
            {
                string key = RequestHelper.GetVal("key");
                if (!string.IsNullOrEmpty(key))
                    return Redirect("/supplier/travelsadd");
            }

            //先删除用户的临时数据
            _supplierService.DelAddTravels(UserId);
            //todo:TravelsAdd
            long epoch = DateTime.Now.Epoch();
            ViewBag.Epoch = epoch;
            ViewBag.MapsFirst = _mapsService.FirstAddTravelsItem(UserId);

            int t = RequestHelper.GetValInt("t");
            if (t > 0)
            {
                string ids = RequestHelper.GetVal("ids");
                string cids = RequestHelper.GetVal("cids");
                if (t == 1 && !string.IsNullOrEmpty(ids) && !string.IsNullOrEmpty(cids))
                {
                    List<City> cityList = new List<City>();
                    string[] idsArray = ids.Split(',');
                    string[] cidsArray = cids.Split(',');
                    for (int i = 0; i < idsArray.Length; i++)
                    {
                        City city = new City();
                        city.CountryId = int.Parse(idsArray[i]);
                        city.CityId = int.Parse(cidsArray[i]);
                        cityList.Add(city);
                    }
                    if (cityList.Count > 0)
                    {
                        _supplierService.SaveToHistory(cityList, UserId, epoch);
                    }
                } if (!string.IsNullOrEmpty(ids))
                {
                    _supplierService.AddTravels(UserId, epoch, Identifier.AddTravelsType.Country, 0, ids);
                }
            }

            return View();
        }

        /// <summary>
        /// TravelsAdd 第一步
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Step1()
        {
            string countryIds = RequestHelper.GetVal("countryIds");
            string Keyword = RequestHelper.GetVal("Keyword");
            if (Keyword == "Search By Country")
                Keyword = "";
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //listMain
            sb.Append(GetAllCountryListByNew(countryIds, Keyword));

            return Json(new BoolMessage(true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Step1 search country
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Step1Search()
        {
            string countryIds = RequestHelper.GetVal("countryIds");
            string keyWord = RequestHelper.GetVal("keyWord");
            string filter = RequestHelper.GetVal("filter");
            if (keyWord == "Search By Country")
                keyWord = "";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(this.SearchCountryList(countryIds, keyWord, filter));
            return Json(new BoolMessage(true, sb.ToString()));
        }

        private string SearchCountryList(string countryIds, string keyWord, string filter)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            List<Country> countryList = _supplierService.GetAllCountry().Where(c => c.StatusId == (int)Identifier.StatusType.Enabled).ToList();

            if (!string.IsNullOrEmpty(keyWord))
                countryList = countryList.Where(c => c.CountryName.ToUpper().Contains(keyWord.ToUpper())).ToList();

            if (!string.IsNullOrEmpty(filter))
                countryList = countryList.Where(c => c.CountryName.ToUpper().StartsWith(filter)).ToList();

            countryList = countryList.OrderBy(c => c.CountryName).ToList();

            string[] selValIds = null;
            if (!string.IsNullOrWhiteSpace(countryIds))
            {
                selValIds = countryIds.Split(','); //选中了那些国家
            }
            string uncheckbox = "";
            string histroy;

            List<DAL.IBT> ibtList = _supplierService.GetUserIbt(UserId);
            if (ibtList.Count > 0)
                ibtList = ibtList.Where(i => i.CountryId > 0 && i.CityId == 0 && i.SupplyId == 0).ToList();

            foreach (Country item in countryList)
            {
                histroy = "";
                uncheckbox = "";
                if (selValIds != null && selValIds.Contains(item.CountryId.ToString()))
                {
                    uncheckbox = " uncheckbox";
                }

                if (ibtList.Exists(i => i.CountryId == item.CountryId))
                {
                    histroy = "histiory";
                }

                sb.Append("<li class=\"\">");
                sb.Append("  <span onclick=\"jTravelsAdd.ckbCountry(this);\" class=\"checkboxBig right " + uncheckbox + "\"  data-countryId=\"" + item.CountryId + "\"></span>");
                sb.Append("  <span class=\"main " + histroy + "\">");
                sb.Append("  <img src=\"" + item.FlagSrc + "\" alt=\"" + item.CountryName + "\"  width=\"27\" height=\"27\" /> " + item.CountryName);
                sb.Append("  </span>");
                sb.Append("</li>");
            }

            return sb.ToString();
        }

        /// <summary>
        /// 保存第一步的选择的国家
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Step1_Save()
        {
            //step1: save目前选中的
            string selCountryIds = RequestHelper.GetVal("selCountryIds");
            long epoch = RequestHelper.GetValLong("epoch");

            //Save
            _supplierService.AddTravels(UserId, epoch, Identifier.AddTravelsType.Country, 0, selCountryIds);
            //end save

            return Json(new BoolMessageItem<int>(0, true, ""), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// TravelsAdd 第二步
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Step2()
        {
            string keyWord = RequestHelper.GetVal("keyWord");
            string countryList = RequestHelper.GetVal("countryIds");
            string iptCountryCity = RequestHelper.GetVal("cityids");
            int selectCountryId = RequestHelper.GetValInt("selectCountryId");

            long epoch = RequestHelper.GetValLong("epoch");
            if (keyWord == "Search By City")
                keyWord = "";
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sideColumn
            //letterBarV
            sb.Append("<div class=\"sideColumn\">");
            //sb.Append(letterBarV("", "jTravelsAdd.CountryFilter", 2));

            //Country's been selected
            sb.Append("<div class=\"country\">");
            sb.Append("  <div class=\"top\"><p class=\"italic\">Countries Selected</p></div>");
            //#Country's been selected
            //if (selectCountryId == 0 && Utils.IsNumeric(countryList.Split(',')[0]))
            //{
            //    selectCountryId = Convert.ToInt32(countryList.Split(',')[0]);
            //}
            if (!string.IsNullOrEmpty(countryList))
            {
                var counties = _supplierService.GetAllCountry().Where(c => c.StatusId == (int)Identifier.StatusType.Enabled).ToList();
                IList<int> ids = new List<int>();
                string[] cIds = countryList.Split(',');
                foreach (string num in cIds)
                {
                    if (Utils.IsNumeric(num))
                    {
                        ids.Add(Convert.ToInt32(num));
                    }
                }
                selectCountryId = counties.Where(c => ids.Contains(c.CountryId)).OrderBy(c => c.CountryName).First().CountryId;
            }

            sb.Append(CountrysBeenSelectedByNew(countryList, selectCountryId));//选择的国家列表
            sb.Append("</div>");
            sb.Append("</div>");//#sideColumn

            //listMain #Select Countries you have visited
            sb.Append("<div class=\"listMain\" id=\"CityLists\">");
            sb.Append("<div class=\"topBox\">");
            sb.Append("  <p class=\"italic\">Select Cities you have visited</p>");
            sb.Append("  <input type=\"text\" id=\"cityKeyword\" value=\"Search By City\" onfocus=\"if(this.value=='Search By City'){this.value=''}\" onblur=\"if(this.value==''){this.value='Search By City'}\" onkeyup='jTravelsAdd.citySearch(event);' />");
            sb.Append("</div>");
            sb.Append("<div id=\"step2Loading\" style=\"display: none;\"><img src=\"/images/icon_waiting.gif\">Loading....</div>");

            sb.Append("  <div class=\"letterBarVertical\">");
            sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.cityFilterSearch(this,'');\">ALL</a>");
            for (int i = 65; i < 91; i++)
            {
                sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.cityFilterSearch(this,'" + Convert.ToChar(i).ToString() + "');\">" + Convert.ToChar(i).ToString() + "</a>");
            }
            sb.Append("  </div>");

            sb.Append("  <div id=\"CityListsScrollBox\" class=\"scrollBox\">");
            //第一个国家的城市的列表
            //已经选中的城市：#从数据库获取
            string selCityIds_this = selectCountryId > 0 ? _supplierService.GetAddTravels(UserId, epoch, Identifier.AddTravelsType.City, selectCountryId).Ids : "";
            int selCityId = 0;
            sb.Append(CitiesListByCountryIdNew(keyWord, selectCountryId, selCityIds_this, true, 0, out selCityId));
            sb.Append("  </div>");
            sb.Append("</div>"); //#listMain

            //sb.Append("<div class=\"outBtn fixbox mT10 mB20\">");
            //sb.Append(" <span class=\"mL10 txt left\">");
            //sb.Append("    <p><span class=\"checkbox disable\"></span> Check the place that I've Been</p>");
            //sb.Append("    <p><span class=\"smallHistiory\"></span> The place that I've posted</p>");
            //sb.Append(" </span>");
            //sb.Append(" <span class=\"right\">");
            //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step1(2);\" class=\"btnBlueH33\"><span class=\"bg\">BACK</span></a>");
            //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step3(2);\" class=\"btnBlueH33\"><span class=\"bg\">NEXT</span></a>");
            //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step4(2);\" class=\"btnBlueH33\"><span class=\"bg\">FINISH</span></a>");
            //sb.Append("</span>");
            //sb.Append("</div>");

            AddTravelsTemp att = new AddTravelsTemp()
            {
                ParentId = selectCountryId,
                Ids = selCityIds_this
            };

            return Json(new BoolMessageItem<AddTravelsTemp>(att, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult TravelsAdd_Step2Search()
        {
            string keyWord = RequestHelper.GetVal("keyWord");
            string cityids = RequestHelper.GetVal("cityids");
            int selectCountryId = RequestHelper.GetValInt("selectCountryId");
            string filter = RequestHelper.GetVal("filter");
            int a = 0;

            if (keyWord == "Search By City")
                keyWord = "";

            string result = CitiesListByCountryIdNew(keyWord, selectCountryId, cityids, true, 0, out a, filter);

            return Json(new BoolMessageItem<int>(selectCountryId, true, result));
        }
        /// <summary>
        /// 选择国家列出下面的城市供用户选择
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Step2_1()
        {
            //step1: save目前选中的
            int saveCountryId = RequestHelper.GetValInt("saveCountryId");
            string selCityIds = RequestHelper.GetVal("selCityIds");
            long epoch = RequestHelper.GetValLong("epoch");
            int countryId = RequestHelper.GetValInt("countryId"); //选择的国家
            int stepNum = RequestHelper.GetValInt("stepNum");
            if (stepNum != 3) //如果stepNum=3就是根据国家列下面的城市然后选择下面的景点的
            {
                //Save
                _supplierService.AddTravels(_profileService.GetUserId(), epoch, Identifier.AddTravelsType.City, saveCountryId, selCityIds);
                //end save
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (stepNum != 3)
            {
                sb.Append("<div class=\"topBox\">");
                sb.Append("  <p class=\"italic\">Select Cities you have visited</p>");
                sb.Append("  <input type=\"text\" id=\"cityKeyword\" value=\"Search By City\" onfocus=\"if(this.value=='Search By City'){this.value=''}\" onblur=\"if(this.value==''){this.value='Search By City'}\" onkeyup='jTravelsAdd.citySearch(event);' />");
                sb.Append("</div>");
                sb.Append("<div id=\"step2Loading\" style=\"display: none;\"><img src=\"/images/icon_waiting.gif\">Loading....</div>");

                sb.Append("  <div class=\"letterBarVertical\">");
                sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.cityFilterSearch(this,'')\">ALL</a>");
                for (int i = 65; i < 91; i++)
                {
                    sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.cityFilterSearch(this,'" + Convert.ToChar(i).ToString() + "')\">" + Convert.ToChar(i).ToString() + "</a>");
                }
                sb.Append("  </div>");

                sb.Append("  <div id=\"CityListsScrollBox\" class=\"scrollBox\">");
            }
            //已经选中的城市：#从数据库获取
            string selCityIds_this = _supplierService.GetAddTravels(UserId, epoch, Identifier.AddTravelsType.City, countryId).Ids;
            int selCity = 0;
            if (stepNum == 3)
            {
                //列出用户选择的
                sb.Append(CitiesListByCountryIdNew("", countryId, selCityIds_this, false, epoch, out selCity));
            }
            else
            {
                sb.Append(CitiesListByCountryIdNew("", countryId, selCityIds_this, true, 0, out selCity));
            }
            sb.Append("  </div>");

            return Json(new BoolMessageItem<string>(selCityIds_this, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存第二步的选择城市记录
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Step2_Save()
        {
            //step1: save目前选中的
            int saveCountryId = RequestHelper.GetValInt("saveCountryId");
            string selCityIds = RequestHelper.GetVal("selCityIds");
            long epoch = RequestHelper.GetValLong("epoch");
            int countryId = saveCountryId;//就是最后一个 RequestHelper.GetValInt("countryId"); //选择的国家

            //Save
            _supplierService.AddTravels(UserId, epoch, Identifier.AddTravelsType.City, saveCountryId, selCityIds);
            //end save

            return Json(new BoolMessageItem<int>(countryId, true, ""), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 第三步
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Step3()
        {
            string keyWord = RequestHelper.GetVal("keyWord");
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string countryIds = RequestHelper.GetVal("countryIds"); //有哪些国家
            int selCountryId = RequestHelper.GetValInt("selCountryId"); //当前选中的是那个国家
            int cityId = RequestHelper.GetValInt("selectCityId"); //选中的城市
            string selVenueIds = RequestHelper.GetVal("selVenueIds"); //已经选择了哪些Venues
            long epoch = RequestHelper.GetValLong("epoch");
            string sortName = RequestHelper.GetUrlDecodeVal("sortName");
            if (keyWord == "Search Supplier Name")
                keyWord = "";
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //sideColumn
            sb.Append("<div class=\"sideColumn\">");
            //sb.Append(letterBarV("", "jTravelsAdd.CountryFilter"));
            //Country's been selected
            sb.Append("<div class=\"country\">");
            sb.Append("  <div class=\"top\"><p class=\"italic\" >Countries Selected</p></div>");
            //#Country's been selected
            //if (selCountryId == 0 && Utils.IsNumeric(countryIds.Split(',')[0]))
            //{
            //    selCountryId = Convert.ToInt32(countryIds.Split(',')[0]);
            //}

            if (!string.IsNullOrEmpty(countryIds))
            {
                List<Country> countryList = _supplierService.GetAllCountry().Where(c => c.StatusId == (int)Identifier.StatusType.Enabled).OrderBy(c => c.CountryName).ToList();
                string[] countryIdsArray = countryIds.Split(',');
                selCountryId = _supplierService.GetAllCountry().Where(c => c.StatusId == (int)Identifier.StatusType.Enabled && countryIdsArray.Contains(c.CountryId.ToString())).OrderBy(c => c.CountryName).FirstOrDefault().CountryId;
            }
            else
            {
                selCountryId = 0;
            }

            sb.Append(CountrysBeenSelectedByNew(countryIds, selCountryId, 3));//选择的国家列表
            sb.Append("</div>");
            sb.Append("</div>");//#sideColumn

            //listMain
            //listMain #Select Countries you have visited
            sb.Append("<div class=\"listMain\" id=\"CityLists\">");
            //sb.Append("  <p class=\"italic\" id=\"step3CityTxt\">Cities Selected</p>");

            sb.Append("  <div class=\"scrollBox\" id=\"step3CitiesList\">");
            //根据国家获取选择的第一个城市
            int selCityId = 0;
            //第一个国家的城市的列表
            sb.Append(CitiesListByCountryIdNew("", selCountryId, "", false, epoch, out selCityId));
            sb.Append("  </div>");
            sb.Append("</div>"); //#listMain

            //venues
            sb.Append("<div class=\"venues\">");
            sb.Append(" <div class=\"topBox\"><p class=\"italic mB0\">Select Venues</p>");
            sb.Append(" <input type=\"text\" id=\"supplyKeyword\" value=\"Search Supplier Name\"/ onkeydown=\"sub(event);\">");
            sb.Append("<span class=\"select w140\">");
            sb.Append("<span class=\"title arrow2\" id=\"SortByName\">Sort By</span>");
            sb.Append("<span class=\"list\">");
            sb.Append("    <ul>");
            sb.Append("    <li class=\"selected\" data-name=\"All\" onclick=\"jTravelsAdd.Step3VenuesSort('All',1);\">All</li>");
            sb.Append("    <li data-name=\"Hotels\" onclick=\"jTravelsAdd.Step3VenuesSort('Hotels',1);\">Hotels</li>");
            //sb.Append("    <li data-name=\"Lifestyle\" onclick=\"jTravelsAdd.Step3VenuesSort('Lifestyle',1);\">Sort By Lifestyle</li>");
            //sb.Append("    <li data-name=\"Attractions\" onclick=\"jTravelsAdd.Step3VenuesSort('Attractions',1);\">Sort By Attractions</li>");
            sb.Append("    <li data-name=\"Hotels\" onclick=\"jTravelsAdd.Step3VenuesSort('Others',1);\">Other Venues</li>");
            sb.Append("    </ul>");
            sb.Append("</span>");
            sb.Append("</span></div>");

            sb.Append("  <div class=\"letterBarVertical\">");
            sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.quickSearch(this,'','" + selCityId.ToString() + "');\">ALL</a>");
            for (int i = 65; i < 91; i++)
            {
                sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.quickSearch(this,'" + Convert.ToChar(i).ToString() + "','" + selCityId.ToString() + "');\">" + Convert.ToChar(i).ToString() + "</a>");
            }
            sb.Append("  </div>");

            sb.Append("<div class=\"scrollBox\">");
            sb.Append("  <div class=\"fixbox\">");
            //sb.Append("  <div class=\"letterBarVertical\">");
            //for (int i = 65; i < 91; i++)
            //{
            //    sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.quickSearch(this,'" + Convert.ToChar(i).ToString() + "','" + selCityId.ToString() + "');\">" + Convert.ToChar(i).ToString() + "</a>");
            //}
            //sb.Append("  </div>");
            sb.Append("<div id=\"step3Loading\" style=\"display:none;\" ><img src=\"/images/icon_waiting.gif\" >Loading....</div>");
            sb.Append("<ul class=\"feedBox\" id=\"feedBox\">");

            string venuesIds = "";
            if (selCityId > 0)
            {
                venuesIds = _supplierService.GetAddTravels(UserId, epoch, Identifier.AddTravelsType.Venue, selCityId).Ids;
            }

            var bmi = VenuesListByCityIdNew(keyWord, filterLetter, selCityId, epoch, sortName, venuesIds);
            sb.Append(bmi.Message);//Venues list
            sb.Append("</ul>");
            //See More /*20130225裕冲修改*/
            //sb.Append("<div id=\"loadSeeMore\" onclick=\"jTravelsAdd.Step3LoadMore();\"><span><a href=\"javascript:void(0);\">See More</a></span></div><br />");
            sb.Append("  <div class=\"fixbox\">");
            sb.Append("<div class=\"mR20\">");
            sb.Append(" <span>");
            //sb.Append("     <a id=\"loadSeeMorePrev\" onclick=\"jTravelsAdd.Step3LoadMorePrev();\" href=\"javascript:void(0);\">prev</a>&nbsp;&nbsp;");
            sb.Append("     <a id=\"loadSeeMore\" onclick=\"jTravelsAdd.Step3LoadMore();\" href=\"javascript:void(0);\">load more</a>");
            sb.Append(" </span>");
            sb.Append("</div><br /><br />");
            sb.Append("</div>");

            //btn
            //sb.Append("<div class=\"outBtn fixbox mT10 mB20\">");
            //sb.Append(" <span class=\"mL10 txt left\">");
            //sb.Append("    <p><span class=\"checkbox disable\"></span> Check the place that I've Been</p>");
            //sb.Append("    <p><span class=\"smallHistiory\"></span> The place that I've posted</p>");
            //sb.Append(" </span>");
            //sb.Append(" <span class=\"right\">");
            //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step2(3);\" class=\"btnBlueH33\"><span class=\"bg\">BACK</span></a>");
            //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step4(3);\" class=\"btnBlueH33\"><span class=\"bg\">FINISH</span></a>");
            //sb.Append(" </span>");
            //sb.Append("</div>");

            sb.Append("</div>");//#venues



            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("cityId", selCityId.ToString());
            dic.Add("item", bmi.Item.ToString());
            dic.Add("Ids", venuesIds);
            return Json(new BoolMessageItem(dic, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult TravelsAdd_Step3_1()
        {
            string keyWord = RequestHelper.GetVal("keyWord");
            string filterLetter = RequestHelper.GetVal("filterLetter");
            int cityId = RequestHelper.GetValInt("cityId");
            long epoch = RequestHelper.GetValLong("epoch");
            int selCityId = RequestHelper.GetValInt("selCityId");
            string selVenueIds = RequestHelper.GetVal("selVenueIds");
            string sortName = RequestHelper.GetUrlDecodeVal("sortName");
            int page = RequestHelper.GetValInt("page", 1);
            sortName = sortName.ToLower() == "all" ? "" : sortName;
            if (keyWord == "Search Supplier Name")
                keyWord = "";
            //保存上次选择的：
            if (string.IsNullOrWhiteSpace(sortName) && selCityId > 0)
            {
                _supplierService.AddTravels(UserId, epoch, Identifier.AddTravelsType.Venue, selCityId, selVenueIds);
            }


            string venuesIds = "";
            if (cityId == selCityId)
            {
                venuesIds = selVenueIds;
            }
            else if (selCityId > 0)
            {
                venuesIds = _supplierService.GetAddTravels(UserId, epoch, Identifier.AddTravelsType.Venue, cityId).Ids;
            }

            var bmi = VenuesListByCityIdNew(keyWord, filterLetter, cityId, epoch, sortName, venuesIds, page);

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("cityId", cityId.ToString());
            dic.Add("item", bmi.Item.ToString()); //如果item > 10,显示分页
            dic.Add("Ids", venuesIds);
            return Json(new BoolMessageItem(dic, true, bmi.Message), JsonRequestBehavior.AllowGet);
        }

        public JsonResult TravelsAdd_Step3_Save()
        {
            long epoch = RequestHelper.GetValLong("epoch");
            int selCityId = RequestHelper.GetValInt("selCityId");
            string selVenueIds = RequestHelper.GetVal("selVenueIds");

            //保存上次选择的：
            _supplierService.AddTravels(UserId, epoch, Identifier.AddTravelsType.Venue, selCityId, selVenueIds);

            return Json(new BoolMessageItem<int>(selCityId, true, ""), JsonRequestBehavior.AllowGet);
        }

        public JsonResult TravelsAdd_Step4()
        {
            long epoch = RequestHelper.GetValLong("epoch");

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sb.Append("<div class=\"mapBox\" id=\"map_canvas\">");
            //sb.Append("</div>");
            //sb.Append("<div class=\"mapBar\">");
            //sb.Append("Legends:");
            //sb.Append("    <span class=\"mark markIBT\">— I've Been There Countries & Cities</span>");
            //sb.Append("    <span class=\"mark markNDC\">— New Discovered Countries & Cities</span>");
            ////sb.Append("    <span class=\"mark markNV\">— NEW Venues</span>");
            //sb.Append("</div>");
            sb.Append("<div class=\"placeBox\">");
            sb.Append("    <p class=\"italic mL20\">You've added</p> ");
            sb.Append("    <div class=\"fixbox\">");
            sb.Append("    <dl class=\"NborderL\">");
            var countries_list = _supplierService.AddTravelsGetSelectCountry(UserId,epoch);
            if (countries_list.Count <= 1)
                sb.Append("        <dt><span class=\"num\">" + countries_list.Count.ToString() + "</span> country </dt>");
            else
                sb.Append("        <dt><span class=\"num\">" + countries_list.Count.ToString() + "</span> countries</dt>");

            sb.Append("            <dd><div class='scrollBox'><ul>");
            foreach (var item in countries_list) { 
               sb.Append(" <li><img src=\""+item.FlagSrc+"\" width=\"16px\" height=\"16px\" /> "+item.CountryName+"</li>");
            }
            sb.Append("            </ul></div></dd>");

            sb.Append("    </dl>");
            sb.Append("    <dl>");

            var cities_list = _supplierService.AddTravelsGetSelectCity(UserId, epoch);
            if (cities_list.Count <= 1)
                sb.Append("        <dt><span class=\"num\">" + cities_list.Count.ToString() + "</span> city</dt>");
            else
                sb.Append("        <dt><span class=\"num\">" + cities_list.Count.ToString() + "</span> cities</dt>");

            sb.Append("            <dd><div class='scrollBox'><ul>");
            countries_list.ForEach(country => {
                var cityList = cities_list.Where(c => c.CountryId == country.CountryId).ToList();
                cityList.ForEach(city =>
                {
                    sb.Append(" <li> " + city.CityName + "," + country.CountryName + "</li>");
                });
            });
            sb.Append("            </ul></div></dd>");

            sb.Append("    </dl>");
            sb.Append("    <dl class=\"NborderR\">");

            var venues_list = _supplierService.AddTravelsGetSelectSupply(UserId, epoch);
            if (venues_list.Count <= 1)
                sb.Append("        <dt><span class=\"num\">" + venues_list.Count.ToString() + "</span> venue</dt>");
            else
                sb.Append("        <dt><span class=\"num\">" + venues_list.Count.ToString() + "</span> venues</dt>");

            sb.Append("            <dd><div class='scrollBox'><ul>");
            countries_list.ForEach(country =>
            {
                var cityList = cities_list.Where(c => c.CountryId == country.CountryId).ToList();
                cityList.ForEach(city =>
                {
                    //sb.Append(" <li> " + country.CountryName + "," + city.CityName + "</li>");
                    var supplyList = venues_list.Where(v => v.CityId == city.CityId).ToList();
                    supplyList.ForEach(s => {
                        sb.Append(" <li> " + s.Title + "<br/>," + city.CityName + "," + country.CountryName + "</li>");
                    });
                });
            });
            sb.Append("            </ul></div></dd>");

            //foreach (var item in venues_list)
            //{
            //    sb.Append("        <dd>" + item + "</dd>");
            //}

            sb.Append("    </dl>");
            sb.Append("    </div>");

            //btn
            //sb.Append("<div class=\"outBtn fixbox mT10 mB20\">");
            //sb.Append(" <span class=\"mL10 txt left\">");
            //sb.Append("    <p><span class=\"checkbox disable\"></span> Check the place that I've Been</p>");
            //sb.Append("    <p><span class=\"smallHistiory\"></span> The place that I've posted</p>");
            //sb.Append(" </span>");
            //sb.Append(" <span class=\"right\">");
            //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step1(4);\" class=\"btnBlueH33\"><span class=\"bg\">EDIT</span></a>");
            //sb.Append("    <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.btnConfrim();\" class=\"btnBlueH33\"><span class=\"bg\">CONFIRM</span></a>");
            //sb.Append("</span>");
            //sb.Append("</div>");

            sb.Append("</div>");
            return Json(new BoolMessage(true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 第四步的confirm确认保存
        /// </summary>
        /// <returns></returns>
        public JsonResult TravelsAdd_Confirm()
        {
            //保存到ibt
            long epoch = RequestHelper.GetValLong("epoch");
            return Json(_supplierService.IBeenThere(UserId, epoch), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 列出所有国家 (需要传入国家ID，然后这些国家都是选中的)
        /// </summary>
        /// <param name="selVals">选中了那些国家，如：10,35</param>
        /// <returns></returns>
        private string GetAllCountryListByNew(string selVals, string Keyword)
        {
            var countryList = _supplierService.GetAllCountryNew(Keyword);
            //var postVisitList = _supplierService.GetUserPostVisitList(UserId);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<div class=\"topBox\">");
            sb.Append("  <input type=\"text\" class=\"right\" id=\"filterKeyword\" value=\"Search By Country\" onkeyup='jTravelsAdd.countrySearch(event);' onfocus=\"if(this.value=='Search By Country'){this.value=''}\" onblur=\"if(this.value==''){this.value='Search By Country'}\" />");
            sb.Append("  <p class=\"italic\">Please Check the countries that you've been </p>");
            sb.Append("</div>");

            sb.Append("  <div class=\"letterBarVertical\">");
            sb.Append("     <a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.countryFilterSearch(this,'');\">ALL</a>");
            for (int i = 65; i < 91; i++)
            {
                sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.countryFilterSearch(this,'" + Convert.ToChar(i).ToString() + "');\">" + Convert.ToChar(i).ToString() + "</a>");
            }
            sb.Append("  </div>");

            sb.Append("<div class=\"listMain\" id=\"AllCountryList_Step1\">");
            //sb.Append("   <div class=\"scrollBox\" style=\"overflow: hidden; position: relative; padding: 0px;\"><div style=\"top: 0px; z-index: 3; position: relative; padding-right: 14px;\" class=\"jscroll-c\"><div style=\"height:0px;overflow:hidden\"></div>");
            sb.Append("   <div class=\"scrollBox\">");
            sb.Append("<div id='step1Loading' style='display:none;'> <img src='/images/icon_waiting.gif'> Loading.... </div>");
            sb.Append("     <ul id='countriesList' class=\"countriesList\">");
            string[] selValIds = null;
            if (!string.IsNullOrWhiteSpace(selVals))
            {
                selValIds = selVals.Split(','); //选中了那些国家
            }
            string uncheckbox = "";
            string histroy;

            List<DAL.IBT> ibtList=_supplierService.GetUserIbt(UserId);
            if (ibtList.Count > 0)
                ibtList = ibtList.Where(i => i.CountryId > 0 && i.CityId == 0 && i.SupplyId == 0).ToList();

            foreach (Country item in countryList)
            {
                histroy = "";
                uncheckbox = "";
                if (selValIds != null && selValIds.Contains(item.CountryId.ToString()))
                {
                    uncheckbox = " uncheckbox";
                }
                //if (_supplierService.HasUserPVCountries(UserId, item.CountryId) > 0) //是否post visited
                if (ibtList.Exists(i => i.CountryId == item.CountryId))
                {
                    histroy = "histiory";
                }
                //if (postVisitList.Where(p => p.CountryId.Equals(item.CountryId) && p.IBTType.Equals(Utils.IntToByte(1))).FirstOrDefault() != null)
                //{//判断用户是否到过该国家
                //    histroy = "histiory";
                //}
                sb.Append("<li class=\"\">");
                sb.Append("  <span onclick=\"jTravelsAdd.ckbCountry(this);\" class=\"checkboxBig right " + uncheckbox + "\"  data-countryId=\"" + item.CountryId + "\"></span>");
                sb.Append("  <span class=\"main " + histroy + "\">");
                sb.Append("  <img src=\"" + item.FlagSrc + "\" alt=\"" + item.CountryName + "\"  width=\"27\" height=\"27\" /> " + item.CountryName);
                sb.Append("  </span>");
                sb.Append("</li>");
            }
            sb.Append("</ul>");
            sb.Append("</div>");
            //sb.Append("<div style=\"height: 100%; top: 0px; right: 0px; -moz-user-select: none; position: absolute; z-index: 3; width: 14px; background: url('../images/scroll_bg_top.gif') repeat-y scroll -48px 0px transparent;\" unselectable=\"on\" class=\"jscroll-e\"><div style=\"position: absolute; top: -1px; width: 100%; left: -3px; background: url('../images/scroll_bg_top.gif') repeat scroll 0px 0px transparent; overflow: hidden; height: 0px;\" class=\"jscroll-u\"></div><div style=\"background: url('../images/scroll_bg_bot.gif') no-repeat scroll left bottom transparent; position: absolute; left: 0px; -moz-user-select: none; top: 0px; border-color: rgb(181, 181, 181); width: 12px; height: 386.811px;\" unselectable=\"on\" class=\"jscroll-h\"><div class=\"jscroll-hD\"></div></div><div style=\"position: absolute; bottom: 0px; width: 100%; left: -3px; background: url('../images/scroll_bg_top.gif') repeat scroll 0px -14px transparent; overflow: hidden; height: 0px;\" class=\"jscroll-d\"></div></div></div>");
            //sb.Append("</div>");
            sb.Append("</div>");
            //sb.Append("<div class=\"outBtn fixbox mT10 mB20\">");
            //sb.Append(" <span class=\"mL10 txt left\">");
            //sb.Append("    <p><span class=\"checkbox disable\"></span> Check the place that I've Been</p>");
            //sb.Append("    <p><span class=\"smallHistiory\"></span> The place that I've posted</p>");
            //sb.Append(" </span>");
            //sb.Append(" <span class=\"right\">");
            //sb.Append(" <a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step2(1);\"><span class=\"bg\">NEXT</span></a>");
            //sb.Append(" <a class=\"btnBlueH33 mR5\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step4(1);\"><span class=\"bg\">FINISH</span></a>");
            //sb.Append(" </span>");
            //sb.Append("</div>");

            return sb.ToString();
        }

        /// <summary>
        /// ADD TRAVELS 第二步，第三步 的列车选择了那些国家
        /// </summary>
        /// <param name="counryIds"></param>
        /// <param name="selId">选中的ID</param>
        /// <returns></returns>
        private string CountrysBeenSelectedByNew(string counryIds, int selId, int stepN = 0)
        {
            var countryList = _supplierService.GetAllCountry().Where(c => c.StatusId == (int)Identifier.StatusType.Enabled).OrderBy(c => c.CountryName).ToList();

            IList<int> ids = new List<int>();
            string[] cIds = counryIds.Split(',');
            foreach (string num in cIds)
            {
                if (Utils.IsNumeric(num))
                {
                    ids.Add(Convert.ToInt32(num));
                }
            }
            var list = from c in countryList where ids.Contains(c.CountryId) select c;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(" <div class=\"scrollBox\">");
            sb.Append("<ul>");
            string active = "";
            string histiory;
            foreach (Country item in list)
            {
                histiory = "";
                if (_supplierService.HasUserPVCountries(UserId, item.CountryId) > 0)
                {
                    histiory = "histiory";
                }
                active = selId == item.CountryId ? "active" : "";
                sb.Append(" <li class=\"" + active + "\"><a class=\"" + histiory + "\" href=\"javascript:void(0);\" onclick=\"jTravelsAdd.GetCityList(this," + item.CountryId.ToString() + "," + stepN.ToString() + ")\" title=\"" + item.CountryName + "\"><img src=\"" + item.FlagSrc + "\" width=\"27\" height=\"27\" /><span class=\"name\">" + StringHelper.Truncate(item.CountryName, 14, "..") + "</span></a></li>");
            }
            sb.Append("</ul>");
            sb.Append("</div>");
            return sb.ToString();
        }

        /// <summary>
        /// 列出国家下面的所有城市
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="selVals">选中的城市列表；如：1,2,50</param>
        /// <param name="isAddCheckBox">是否需要加Checkbox(只有第二步的选择才需要添加)</param>
        /// <returns></returns>
        private string CitiesListByCountryIdNew(string keyWord, int countryId, string selVals, bool isAddCheckBox, long epoch, out int selCityId,string filter="")
        {
            selCityId = 0;
            IList<City> cityList = null;
            if (isAddCheckBox)
                cityList = _supplierService.GetCityListNew(countryId, keyWord);
            else
            {
                string cityListIds = _supplierService.GetAddTravels(UserId, epoch, Identifier.AddTravelsType.City, countryId).Ids;
                cityList = _supplierService.GetCityListByIds(cityListIds);
            }

            if (!string.IsNullOrEmpty(filter))
            {
                cityList = cityList.Where(c => c.CityName.ToUpper().StartsWith(filter)).ToList();
            }

            cityList = cityList.OrderBy(c => c.CityName).ToList();
            if (cityList.Count > 0)
                selCityId = cityList.First().CityId;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (epoch != 0)
            {
                sb.Append("  <div class=\"topBox\"><p class=\"italic\">Cities Selected</p></div>");
            }
            sb.Append("<dl>");
            int idx = 0;
            string[] selVals_1 = null;
            if (!string.IsNullOrWhiteSpace(selVals))
            {
                selVals_1 = selVals.Split(',');
            }
            string histroy;

            List<DAL.IBT> ibtList = _supplierService.GetUserIBTCities(UserId);
            foreach (City item in cityList)
            {
                //City c = _supplierService.GetCityInfo(item.CityId);
                City c = item;
                if (c == null) c = new City();
                if (c.CountryId == countryId)
                {
                    histroy = "";
                    //if (_supplierService.HasUserPVCities(UserId, item.CityId) > 0) //是否post visited
                    //判断是否i have been there
                    if (ibtList.Exists(ibt => ibt.CityId == item.CityId))
                    {
                        histroy = "histiory";
                        //histroy = "<img class=\"mL10\"  src=\"/images/icon/visited_icon.png\" alt=\"\" />";
                    }
                    if (selVals == "" && isAddCheckBox == false && idx == 0)
                    {
                        selCityId = item.CityId;
                        sb.Append("<dd class=\"active " + histroy + "\"  onclick=\"jTravelsAdd.GetVenuesList(this," + item.CityId.ToString() + ",3);\">");
                    }
                    //如果isAddCheckBox == false的话selVals就只有一个数字了
                    else if (item.CityId.ToString() == selVals && isAddCheckBox == false)
                    {
                        //选择的标记出来 css_nobox_act //step 3的不需要noLineT
                        sb.Append("<dd class=\"active " + histroy + "\" onclick=\"jTravelsAdd.GetVenuesList(this," + item.CityId.ToString() + ",3);\">");
                    }
                    else if (isAddCheckBox == false)
                    {
                        sb.Append("<dd class=\"" + histroy + "\" onclick=\"jTravelsAdd.GetVenuesList(this," + item.CityId.ToString() + ",3);\">");
                    }
                    else
                    {
                        if (idx == 0)
                            sb.Append("<dd class=\"noLineT " + histroy + "\">");
                        else
                            sb.Append("<dd class=\"" + histroy + "\">");
                    }

                    if (isAddCheckBox)
                    {
                        //选择的标记出来
                        if (selVals != null && selVals_1 != null && selVals_1.Contains(item.CityId.ToString()))
                        {
                            sb.Append("<span class=\"checkboxBig uncheckbox\" onclick=\"jTravelsAdd.ckbCity(this);\" data-cityId=\"" + item.CityId.ToString() + "\"></span>"); //uncheckbox
                        }
                        else
                        {
                            sb.Append("<span class=\"checkboxBig\" onclick=\"jTravelsAdd.ckbCity(this);\"  data-cityId=\"" + item.CityId.ToString() + "\"></span>"); //uncheckbox
                        }
                    }

                    sb.Append("<span class=\"name\">" + item.CityName + "</span></dd>");

                    idx++;
                }
            }
            sb.Append("</dl>");

            return sb.ToString();
        }

        /// <summary>
        /// 列出城市下面的所有Supply 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        private BoolMessageItem<int> VenuesListByCityIdNew(string keyWord, string filterLetter, int cityId, long epoch, string sortName, string selVenueIds = "", int page = 1)
        {
            int pageSize = 10;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //获取已经选取的
            var selIds = selVenueIds;// _supplierService.GetAddTravels(UserId, epoch, Identifier.AddTravelsType.Venue, cityId).Ids;
            string[] selVals_1 = null;
            if (!string.IsNullOrWhiteSpace(selIds))
            {
                selVals_1 = selIds.Split(',');
            }

            IList<SupplyListShort> supplyList = new List<SupplyListShort>();
            //if (string.IsNullOrWhiteSpace(sortName)&&cityId>0)
            //{
            //    //supplyList = _supplierService.AddTravelsGetSupplyListAll(keyWord, filterLetter, UserId, cityId, pageSize, page);
            //    supplyList = _supplierService.AddTravelsGetSupplyListSortBy(keyWord, filterLetter, UserId, cityId, pageSize, page);
            //}
            //else if (cityId > 0)
            //{
            //    supplyList = _supplierService.AddTravelsGetSupplyListSortByCategory(keyWord, filterLetter, UserId, cityId, sortName, pageSize, page);
            //    //supplyList = _supplierService.AddTravelsGetSupplyListSortBy(keyWord, filterLetter, UserId, cityId, pageSize, page, sortName);
            //}
            if (cityId > 0)
            {
                supplyList = _supplierService.AddTravelsGetSupplyListSortBy(keyWord, filterLetter, UserId, cityId, pageSize, page, sortName);
            }
            else
            {
                sb.Append("");
            }
            string histiory;
            string uncheckbox1;
            string uncheckbox2;
            string uncheckbox3;
            foreach (SupplyListShort item in supplyList)
            {
                histiory = "";
                uncheckbox1 = "";
                uncheckbox2 = "";
                uncheckbox3 = "";
                if (_supplierService.HasUserPVSupply(UserId, item.SupplyId) > 0)
                {
                    histiory = "histiory";
                    //histiory = "<img style=\"float:left;\"  src=\"/images/icon/visited_icon.png\" alt=\"\" />";
                }
                sb.Append("<li><div class=\"main\"><div class=\"" + histiory + " mainBg\"><a class=\"img\" href='javascript:void(0);' onclick='jTravelsAdd.clickVenues(\""+item.TitleUrl+"\");'><img alt=\"\" src=\"" + PageHelper.GetSupplyPhoto(item.SupplyId, item.CoverPhotoSrc, Identifier.SuppilyPhotoSize._xtn) + "\"></a>");
                //sb.Append("<li class=\"" + histiory + "\"><a class=\"img\" href=\"" + item.SupplyUrl + "\"  target=\"_blank\"><img alt=\"\" src=\"" + item.CoverPhotoSrc + "\"></a>");
                sb.Append("    <dl>");
                sb.Append("        <dt><a href=\"javascript:void(0);\" onclick='jTravelsAdd.clickVenues(\"" + item.TitleUrl + "\");' >" + item.Title + "</a>");
                sb.Append("            <span class=\"location\">" + item.CityName + "," + item.CountryName + "</span>");
                sb.Append("        </dt>");
                sb.Append("        <dd><span class=\"rating\">RATING</span> ");
                sb.Append("            <span class=\"StepRating\" data-rating=\"" + item.Rating.ToString() + "\" data-objId=\"" + item.SupplyId.ToString() + "\"></span> ");
                sb.Append("         </dd>");
                sb.Append("         <dd>");
                //sb.Append("        <div class=\"choose\">");
                //uncheckbox1 = item.Recommended.ToString() == "1" ? "uncheckbox" : "";
                //sb.Append("            <span class=\"checkboxMTxt " + uncheckbox1 + "\" onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"2\"  data-ibtType=\"3\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.SupplyId.ToString() + "\">");
                //sb.Append("	        <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                //sb.Append("            </span>");
                //uncheckbox2 = item.WanttoGo.ToString() == "1" ? "uncheckbox" : "";
                //sb.Append("            <span class=\"checkboxMTxt " + uncheckbox2 + "\" onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"3\" data-ibtType=\"3\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.SupplyId.ToString() + "\">");
                //sb.Append("	        <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                //sb.Append("            </span>");
                //uncheckbox3 = item.BucketList.ToString() == "1" ? "uncheckbox" : "";
                //sb.Append("            <span class=\"checkboxMTxt " + uncheckbox3 + "\" onclick=\"ibt.rrwb_rwb(this);\"  data-rrwbType=\"4\" data-ibtType=\"3\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.SupplyId.ToString() + "\">");
                //sb.Append("	        <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                //sb.Append("            </span>");
                //sb.Append("        </div>");
                sb.Append("         </dd>");
                sb.Append("         <dd class=\"btn\">");
                if (selIds != null && selIds.Contains(item.SupplyId.ToString())) //已经选择的
                {
                    //if (item.IbtCount == 0)
                    //    sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step3IBT(this," + item.SupplyId.ToString() + ");\" class=\"btnBlueH33\" data-id=\"" + item.SupplyId.ToString() + "\" data-sel=\"1\" data-ibtnum=\"" + item.IbtCount.ToString() + "\"><span class=\"bg\">I've Been There</span></a>");
                    //else
                    item.IbtCount = item.IbtCount == 0 ? 1 : item.IbtCount; //已经选择至少有+1的。
                    sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step3IBT(this," + item.SupplyId.ToString() + ");\" class=\"btnBlueOkH27\"  data-id=\"" + item.SupplyId.ToString() + "\" data-sel=\"1\" data-ibtnum=\"" + item.IbtCount.ToString() + "\"><span class=\"btnBlueNumH27\"><span class=\"btnBlueAddH27\">I've Been There</span>" + item.IbtCount.ToString() + "</span></a>");
                }
                else
                {
                    if (item.IbtCount == 0)
                        sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step3IBT(this," + item.SupplyId.ToString() + ");\" class=\"feedBox_IBT btnGrayDotH29\"  data-id=\"" + item.SupplyId.ToString() + "\" data-sel=\"0\" data-ibtnum=\"" + item.IbtCount.ToString() + "\"><span class=\"bg\">I've Been There</span></a>");
                    else
                        sb.Append("<a href=\"javascript:void(0);\" onclick=\"jTravelsAdd.Step3IBT(this," + item.SupplyId.ToString() + ");\" class=\"btnGrayOkH29\"  data-id=\"" + item.SupplyId.ToString() + "\" data-sel=\"0\" data-ibtnum=\"" + item.IbtCount.ToString() + "\"><span class=\"btnGrayNumH29\"><span class=\"btnGrayAddH29\">I've Been There</span>" + item.IbtCount.ToString() + "</span></a>");
                }
                sb.Append("</dd>");
                sb.Append("         <dd class=\"operating\" style=\"display:none;\">");
                sb.Append("            <a href=\"/Supplier/v/" + item.SupplyId.ToString() + "-" + UrlSeoUtils.BuildValidUrl(item.Title) + "\" target=\"_blank\">Comments(" + item.CommentsCount.ToString() + ")</a>");// |  <a onclick=\"ibt.Share(3," + item.SupplyId + ")\" href=\"javascript:void(0);\">Share</a>
                sb.Append("         </dd>");
                sb.Append("    </dl>");
                sb.Append("  </div>");
                sb.Append(" </div>");
                sb.Append("</li>");
            }

            return new BoolMessageItem<int>(supplyList.Count, true, sb.ToString());
        }

        #endregion

        #region Travel History
        public ActionResult TravelHistory()
        {
            return View();
        }
        /// <summary>
        ///  MyHistoryList data
        /// </summary>
        /// <returns></returns>
        public JsonResult MyHistoryListAjx()
        {
            int listCountryId = RequestHelper.GetValInt("listCountryId");
            string listCountryName = RequestHelper.GetVal("listCountryName");
            //int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            if (filterKeyword == "Search By Country")
                filterKeyword = "";
            //pageSize = pageSize > 50 ? 10 : pageSize;
            //pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = MyHistoryListData(listCountryId, listCountryName, filterKeyword, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回MyHistoryList的数据
        /// </summary>
        /// <param name="listType">Recommended</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string MyHistoryListData(int listCountryId, string listCountryName, string filterKeyword, out int total, out int itemTotal)
        {
            var list = _supplierService.GetHistoryCityList(UserId, listCountryId, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sbHtml = new System.Text.StringBuilder();

            sbHtml.Append("<li class=\"frist row\">");
            sbHtml.Append("  <div class=\"rowMain\">");
            int cnCount = _supplierService.HasCountriesPostVisitOnlyCount(UserId, listCountryId);
            sbHtml.Append("  <span class=\"visitedPost\">" + cnCount.ToString() + " <span class=\"f12\">posted visits</span></span>");
            sbHtml.Append("  <span class=\"checkboxBig disable mR10\"></span>");
            sbHtml.Append("  <span class=\"txt\" id=\"CountryName\">" + listCountryName + " <span class=\"italic mL5 f12\">/  Country</span></span>");
            sbHtml.Append("  </div>");
            sbHtml.Append("</li>");
            int cycount;
            foreach (var item in list)
            {
                sbHtml.Append(" <li class=\"row  \">");
                sbHtml.Append("   <div class=\"rowMain\">");
                cycount = _supplierService.HasCitiesPostVisitCount(UserId, item.CityId);
                sbHtml.Append("     <span class=\"visitedPost C31AEE3\">" + cycount.ToString() + "<span class=\"f12\">posted visits</span></span>");
                sbHtml.Append("     <span class=\"txt C31AEE3 close\" name=\"chkItem\" data-load=\"0\" data-cityId=\"" + item.CityId + "\" onclick=\"SelectVenues(this,'" + item.CityName + "'," + item.CityId + ");return false;\"><span >" + item.CityName + " </span><span class=\"italic mL5 f12\">/  Cities</span></span>");
                sbHtml.Append("   </div>");
                sbHtml.Append("   <div class=\"subList \" style=\"display:none\" id=\"sub_cid_" + item.CityId + "\" data-show=\"0\">");
                sbHtml.Append("     <ul id=\"subList_" + item.CityId + "\">");
                sbHtml.Append("    </ul>");
            }
            return sbHtml.ToString();
        }

        /// <summary>
        ///  MyVenuesList data
        /// </summary>
        /// <returns></returns>
        public JsonResult MyVenuesListAjx()
        {
            int listCountryId = RequestHelper.GetValInt("listCountryId");
            int listCityId = RequestHelper.GetValInt("listCityId");
            string listCityName = RequestHelper.GetVal("listCityName");

            //pageSize = pageSize > 50 ? 10 : pageSize;
            //pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = MyVenuesListData(listCountryId, listCityId, listCityName, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回MyVenuesList的数据
        /// </summary>
        /// <param name="listType">Recommended</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string MyVenuesListData(int listCountryId, int listCityId, string listCityName, out int total, out int itemTotal)
        {
            var list = _supplierService.GetHistoryVenuesList(UserId, listCountryId, listCityId, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sbHtml = new System.Text.StringBuilder();

            sbHtml.Append("<li class=\"titleBar\">");
            int cyCount = _supplierService.HasCitiesPostVisitOnlyCount(UserId, listCityId);
            sbHtml.Append("  <span class=\"visitedPost C31AEE3\">" + cyCount + " </span>");
            sbHtml.Append("   <span class=\"checkboxBig uncheckbox disable mR10\"></span>");
            sbHtml.Append("   <span class=\"txt\"><span class=\"C31AEE3\">" + listCityName + "</span> <span class=\"italic mL5 f12\">/  Cities</span></span>");
            sbHtml.Append("</li>");
            int supplycount;
            foreach (var item in list)
            {
                sbHtml.Append(" <li class=\"\">");
                supplycount = _supplierService.HasVenuesPostVisitCount(UserId, item.SupplyId);
                sbHtml.Append("    <span class=\"visitedPost C31AEE3\">" + supplycount.ToString() + "</span>");
                sbHtml.Append("    <span class=\"checkboxBig uncheckbox disable mR10\"></span>");
                sbHtml.Append("    <span class=\"txt\"><a href=\"" + item.SupplyUrl + "\" class=\"\">" + item.Title + "</a> <span class=\"italic mL5 f12\">/  " + item.TypesOfSupplier + "</span></span>");
                sbHtml.Append(" </li>");
            }
            return sbHtml.ToString();
        }
        #endregion

        #region Post visit
        public ActionResult PostVisit(int ibtType, int objId)
        {
            ViewBag.Profile = _profileService.GetUserProfile(UserId);
            ViewBag.IbtType = ibtType;
            //todo:pv需要处理国家与城市的
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCountryInfo(UserId, objId);
                    break;
                case (int)Identifier.IBTType.City:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCityInfo(UserId, objId);
                    break;
                default:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitSupplyInfo(UserId, objId);
                    break;
            }

            #region 年月日绑定
            //绑定年
            List<SelectListItem> yearList = new List<SelectListItem>();
            for (int y = DateTime.Now.Year; y >= 1950; y--)
            {
                yearList.Add(new SelectListItem() { Text = y.ToString(), Value = y.ToString() });
            }
            ViewBag.YearList = yearList;

            //绑定月
            List<SelectListItem> monthList = new List<SelectListItem>();
            monthList.Add(new SelectListItem() { Text = "Jan", Value = "1" });
            monthList.Add(new SelectListItem() { Text = "Feb", Value = "2" });
            monthList.Add(new SelectListItem() { Text = "Mar", Value = "3" });
            monthList.Add(new SelectListItem() { Text = "Apr", Value = "4" });
            monthList.Add(new SelectListItem() { Text = "May", Value = "5" });
            monthList.Add(new SelectListItem() { Text = "Jun", Value = "6" });
            monthList.Add(new SelectListItem() { Text = "Jul", Value = "7" });
            monthList.Add(new SelectListItem() { Text = "Aug", Value = "8" });
            monthList.Add(new SelectListItem() { Text = "Sept", Value = "9" });
            monthList.Add(new SelectListItem() { Text = "Oct", Value = "10" });
            monthList.Add(new SelectListItem() { Text = "Nov", Value = "11" });
            monthList.Add(new SelectListItem() { Text = "Dec", Value = "12" });
            //monthList.ForEach(m => m.Selected = (m.Value == DateTime.Now.Month.ToString()));
            ViewBag.MonthList = monthList;

            //绑定日
            List<SelectListItem> dayList = new List<SelectListItem>();
            for (int d = 1; d <= 31; d++)
            {
                dayList.Add(new SelectListItem() { Text = d.ToString().PadLeft(2, '0'), Value = d.ToString() });
            }
            ViewBag.DayList = dayList;
            #endregion

            return View();
        }

        /// <summary>
        /// postvistSave
        /// </summary>
        /// <returns></returns>
        public JsonResult PostVistSave()
        {
            int newsfeedId = RequestHelper.GetValInt("newsfeedId");
            int activeType = RequestHelper.GetValInt("activeType");

            int handleType = RequestHelper.GetValInt("handleType");
            int postType = RequestHelper.GetValInt("postType");
            int supplyId = RequestHelper.GetValInt("supplyId");
            string experience = RequestHelper.GetUrlDecodeVal("experience");
            string date = RequestHelper.GetUrlDecodeVal("date");
            string selDate = RequestHelper.GetVal("selDate");
            string sharing = RequestHelper.GetUrlDecodeVal("sharing"); //3-4-2
            string guid = RequestHelper.GetUrlDecodeVal("guid");
            int countryId = RequestHelper.GetValInt("countryId");
            int cityId = RequestHelper.GetValInt("cityId");
            int ibtType = RequestHelper.GetValInt("ibtType");

            string share_title = RequestHelper.GetUrlDecodeVal("share_title");
            string share_desp = RequestHelper.GetUrlDecodeVal("share_desp");
            string share_comment = RequestHelper.GetUrlDecodeVal("share_comment");
            //pic facebook,linkedin 是需要http://开头的. twitter是需要c:\images 的物理地址上传到上面去的
            string share_pic = RequestHelper.GetUrlDecodeVal("share_pic"); // /images/...
            string share_url = RequestHelper.GetUrlDecodeVal("share_url");
            string twitter_title = RequestHelper.GetUrlDecodeVal("twitter_title");
            int share_facebook = RequestHelper.GetValInt("share_facebook"); //1 选中 ; 0 未选中
            int share_linkedin = RequestHelper.GetValInt("share_linkedin");
            int share_twitter = RequestHelper.GetValInt("share_twitter");
            int share_imeet = RequestHelper.GetValInt("share_imeet");

            int isChooseCity = RequestHelper.GetValInt("isChooseCity");//1 选中；0 未选中
            int isChooseCountry = RequestHelper.GetValInt("isChooseCountry");

            IMeet.IBT.Common.Configurations.SiteConfiguration _config = new IMeet.IBT.Common.Configurations.SiteConfiguration();
            string siteURL = _config.AlphaSiteUrl;
            string siteName = _config.SiteName;

            string http_pic = "";//share_pic facebook,linkedin 是需要http://开头的
            string phy_pic = ""; //twitter是需要c:\images 的物理地址上传到上面去的
            share_url = siteURL + share_url;
            if (!string.IsNullOrWhiteSpace(share_pic))
            {
                http_pic = siteURL + share_pic;
                phy_pic = Server.MapPath(share_pic);
            }

            DateTime? pvDate = null;
            if (selDate.ToLower().Equals("true"))
            {
                pvDate = Convert.ToDateTime(date);
            }
            #region LinkedIn
            if (share_linkedin == 1)
            {
                var linkedinShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.LinkedIn);
                if (linkedinShare != null && linkedinShare.UserId > 0)
                {
                    if (string.IsNullOrWhiteSpace(http_pic))
                    {
                        http_pic = siteURL + "/images/logo/logo_w100.gif";
                    }
                    string linkedin_token = linkedinShare.OauthToken;
                    string linkedInXML = @"<share>
                          <comment>" + share_comment + @"</comment>
                          <content>
                            <title>" + share_title + @"</title>
                            <description>" + share_desp + @"</description>
                            <submitted-url>" + share_url + @"</submitted-url>
                            <submitted-image-url>" + http_pic + @"</submitted-image-url> 
                          </content>
                          <visibility> 
                            <code>anyone</code> 
                          </visibility>
                        </share>";
                    SNSHelper.LinkedInConsumer.Share(linkedin_token, linkedInXML);
                }
            }
            #endregion

            #region facebook
            if (share_facebook == 1)
            {
                var facebookShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Facebook);
                if (facebookShare != null && facebookShare.UserId > 0)
                {
                    string facebook_Token = facebookShare.OauthToken;

                    if (string.IsNullOrWhiteSpace(http_pic))
                    {
                        http_pic = siteURL + "/images/logo/logo_w100.gif";
                    }

                    SNSHelper.FacebookClient.Share(facebook_Token, share_title, share_url, "", share_desp, http_pic, share_comment);
                }
            }
            #endregion

            #region Twitter
            if (share_twitter == 1)
            {
                var twitterShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Twitter);
                if (twitterShare != null && twitterShare.UserId > 0)
                {
                    string twitter_str = "I've posted a visit to {0}. Start/share your own travel map and travel history!";
                    string twitter_shareurl = " @i_meet " + share_url;
                    string twitter_msg = String.Format(twitter_str, twitter_title);
                    int len = twitter_shareurl.Length + twitter_msg.Length;
                    if (len > 140)
                    {
                        twitter_msg = StringHelper.Truncate(twitter_msg, (twitter_msg.Length - len + 136), "...!");
                    }
                    twitter_msg += twitter_shareurl;
                    string twitter_token = twitterShare.OauthToken;
                    if (!string.IsNullOrWhiteSpace(phy_pic))
                    {
                        SNSHelper.TwitterConsumer.StatusesUpdate(twitter_token, twitter_msg, phy_pic);
                    }
                    else
                    {
                        SNSHelper.TwitterConsumer.StatusesUpdate(twitter_token, twitter_msg);
                    }
                }
            }
            #endregion

            #region iMeet
            if (share_imeet == 1)
            {
                var imeetShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.IMeet);
                if (imeetShare != null && imeetShare.UserId > 0)
                {
                    string imeet_token = imeetShare.OauthToken;
                    string imeet_msg = "I've posted a visit to " + twitter_title;
                    imeet_msg += " " + share_url;
                    if (!string.IsNullOrWhiteSpace(share_pic))
                    {
                        SNSHelper.iMeetConsumer.StatusesUpdate(imeet_token, imeetShare.Sns_UId, imeet_msg, share_url, http_pic);
                    }
                    else
                    {
                        SNSHelper.iMeetConsumer.StatusesUpdate(imeet_token, imeetShare.Sns_UId, imeet_msg);
                    }
                }
            }
            #endregion

            string pvId = "";
            if (activeType == (int)Identifier.FeedActiveType.PostVisit)
            {
                pvId = _newsFeedService.GetNewsFeedInfo(newsfeedId).ObjData;
            }
            var bmi = new BoolMessageItem<int>(0, false, "");
            //添加或者编辑修改(未完成)
          
            if (isChooseCountry == 1)
            {
                _supplierService.SavePostVisit(postType, UserId, (int)Identifier.IBTType.Country, countryId, 0, 0, experience, sharing, pvDate);
            }
            if (isChooseCity == 1)
            {
                _supplierService.SavePostVisit(postType, UserId, (int)Identifier.IBTType.City, countryId, cityId, 0, experience, sharing, pvDate);
            }
            if (handleType == 0)
            {
                //添加
                bmi = _supplierService.SavePostVisit(postType, UserId, ibtType, countryId, cityId, supplyId, experience, sharing, pvDate);
            }
            else
            {
                if (pvId != "")
                {
                    //编辑
                    bmi = _supplierService.EditPostVisit(Convert.ToInt32(pvId.ToString()), newsfeedId, experience, pvDate);

                }
                else
                {
                    bmi = _newsFeedService.EditNewsFeed(newsfeedId, experience, pvId, pvDate);
                }
            }
            //更新用户上传的图片
            _supplierService.SetPostVistAttachmentPvId(guid, bmi.Item);
            if (string.IsNullOrWhiteSpace(newsfeedId.ToString()) || newsfeedId == 0)
            {
                newsfeedId = Convert.ToInt32(bmi.Message.ToString());
            }
            _ibtServices.SetAttachmentObjId(guid, newsfeedId);

            return Json(bmi, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// POST VISIT 的上传图只要一张小图与大图就可以了。
        /// </summary>
        /// <returns></returns>
        public JsonResult PostVisitUpload()
        {
            string url = "";
            bool isUpload = false;
            string AttId = "";
            if (Request.Files.Count > 0)
            {
                HttpPostedFile hfile = System.Web.HttpContext.Current.Request.Files[0];
                if (hfile != null && hfile.ContentLength > 0)
                {
                    int supplyId = RequestHelper.GetValInt("supplyId");
                    string guid = RequestHelper.GetVal("guid");

                    int dir = supplyId / 500;
                    string staticPath = "/Staticfile/comments/" + dir.ToString() + "/" + supplyId.ToString() + "/" + DateTime.Now.ToString("yyyyMM") + "/";
                    string path = System.Web.HttpContext.Current.Server.MapPath("~" + staticPath);
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    //string fileName = Request.Params["fileName"];
                    //if (string.IsNullOrEmpty(fileName))
                    string fileName = System.Guid.NewGuid().ToString().Substring(0, 15).Replace("-", "");

                    #region Cut imgwo
                    try
                    {
                        /*20130204裕冲修改：_ori.jpg为原图，生成tn[w 80],big[w 1024]*/
                        hfile.SaveAs(path + fileName + "_ori.jpg");
                        //cut
                        IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_tn.jpg", (int)Identifier.SuppilyPostVisitPhotoSize._tn, (int)Identifier.SuppilyPostVisitPhotoSize._tn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.Cut);

                        Image o = Image.FromFile(path + fileName + "_ori.jpg");
                        if (o.Width > (int)Identifier.SuppilyPostVisitPhotoSize._big || o.Height > (int)Identifier.SuppilyPostVisitPhotoSize._big)
                        {
                            IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_big.jpg", (int)Identifier.SuppilyPostVisitPhotoSize._big, (int)Identifier.SuppilyPostVisitPhotoSize._big, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                        }
                        else
                        {
                            if (System.IO.File.Exists(path + fileName + "_big.jpg")) { System.IO.File.Delete(path + fileName + "_big.jpg"); }
                            System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_big.jpg");
                        }
                        //_570x430 //用于前台页面相册的popul
                        if (o.Width > 570 || o.Height > 430)
                        {
                            IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_570x430.jpg", 570, 430, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                        }
                        else
                        {
                            if (System.IO.File.Exists(path + fileName + "_570x430.jpg"))
                                System.IO.File.Delete(path + fileName + "_570x430.jpg");
                            System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_570x430.jpg");
                        }
                        //bn
                        if (o.Width > (int)Identifier.SuppilyPhotoSize._bn || o.Height > (int)Identifier.SuppilyPhotoSize._bn)
                        {
                            IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_bn.jpg", (int)Identifier.SuppilyPhotoSize._bn, (int)Identifier.SuppilyPhotoSize._bn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                        }
                        else
                        {
                            if (System.IO.File.Exists(path + fileName + "_bn.jpg"))
                                System.IO.File.Delete(path + fileName + "_bn.jpg");
                            System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_bn.jpg");
                        }

                        o.Dispose();
                        /*20130204裕冲修改结束*/

                        BoolMessageItem<int> bmi = _supplierService.SavePostvisitFile(guid, hfile.FileName, staticPath + fileName);
                        AttId = bmi.Item.ToString();
                        isUpload = true;
                        url = staticPath + fileName + "_tn.jpg";
                        if (_supplierService.GetPostVisitImageInfo(staticPath + fileName).StatusId == Utils.IntToByte(1))
                        {
                            //需要添加到Attachment附件表
                            Attachment att = new Attachment()
                            {
                                Guid = guid,
                                AttType = (int)Identifier.AttachmentType.NewsFeed,
                                SourceFileName = staticPath + fileName,
                                FileName = hfile.FileName,
                                StatusId = (int)Identifier.StatusType.Enabled,
                                CreateData = DateTime.Now
                            };
                            _ibtServices.AddAttachment(att);
                        }

                    }
                    catch (Exception ex)
                    {
                        isUpload = false;
                        _logger.Error("用户上传文件！（POst visit）", ex);
                    }

                    #endregion

                    /*
                    //生成小图
                    string[] strsSavePath = new string[] { path + fileName + "_big.jpg", path + fileName + "_tn.jpg" };
                    int[] intMaxLength = new int[] { (int)Identifier.SuppilyPostVisitPhotoSize._big,  (int)Identifier.SuppilyPostVisitPhotoSize._tn };
                    bool blSucResize = false;
                    try
                    {
                        blSucResize = IMeet.IBT.Common.Utilities.ThumbnailImage.ResizeFromPostedFile(hfile, strsSavePath, intMaxLength);

                        _supplierService.SavePostvisitFile(guid, Request.Params["fileName"], staticPath + fileName);
                        //Image image = Image.FromFile(path + fileName + "_mn.jpg");
                        //context.Response.Write("{\"src\":\"" + Ran.Core.Common.Default.GetAvatarsPath(userId, fileName, AvatarSize._mn) + "?id=" + System.Guid.NewGuid().ToString().Substring(0, 9).Replace("-", "") + "\",\"w\":\"" + image.PhysicalDimension.Width.ToString() + "\",\"h\":\"" + image.PhysicalDimension.Height.ToString() + "\"}");
                        //image.Dispose();
                        isUpload = true;
                        url = staticPath + fileName + "_tn.jpg";
                    }
                    catch (Exception ex)
                    {
                        isUpload = false;
                        _logger.Error("用户上传文件！（POst visit）", ex);
                    }*/
                }
            }
            return Json(new BoolMessageItem<string>(AttId, isUpload, url), JsonRequestBehavior.DenyGet);
        }
        public JsonResult PostVisitIsDel()
        {
            string Guid = RequestHelper.GetUrlDecodeVal("objId");
            string AttId = RequestHelper.GetUrlDecodeVal("attId");
            string SourceFileName = _supplierService.GetPostVisitInfo(Guid).SourceFileName;
            string imageurl = SourceFileName + "_tn.jpg";
            var bmi = _supplierService.PostVisitIsDel(AttId);
            if (bmi.Success)
            {
                _ibtServices.AttachmentDelForPVAttID(RequestHelper.GetValInt("attId"));
                //删除已上传的图片 /Staticfile/comments/3/1685/201212/54e6b4ff88cf4_tn.jpg
                //(判断文件是否存在)---不用删除图片文件  System.IO.File.Delete(filename)删除文件
                //string path = System.Web.HttpContext.Current.Server.MapPath("~" + imageurl);
                //if (System.IO.File.Exists(path))
                //{
                //    System.IO.File.Delete(path);
                //}

            }
            return Json(bmi);
        }

        #region new post visit
        public ActionResult PostVisitN(int ibtType, int objId)
        {
            var re = Request;
            ViewBag.Profile = _profileService.GetUserProfile(UserId);
            ViewBag.IbtType = ibtType;
            //todo:pv需要处理国家与城市的
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCountryInfo(UserId, objId);
                    break;
                case (int)Identifier.IBTType.City:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCityInfo(UserId, objId);
                    break;
                default:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitSupplyInfo(UserId, objId);
                    break;
            }

            #region 年月日绑定
            //绑定年
            List<SelectListItem> yearList = new List<SelectListItem>();
            for (int y = DateTime.Now.Year; y >= 1950; y--)
            {
                yearList.Add(new SelectListItem() { Text = y.ToString(), Value = y.ToString() });
            }
            ViewBag.YearList = yearList;

            //绑定月
            List<SelectListItem> monthList = new List<SelectListItem>();
            monthList.Add(new SelectListItem() { Text = "Jan", Value = "1" });
            monthList.Add(new SelectListItem() { Text = "Feb", Value = "2" });
            monthList.Add(new SelectListItem() { Text = "Mar", Value = "3" });
            monthList.Add(new SelectListItem() { Text = "Apr", Value = "4" });
            monthList.Add(new SelectListItem() { Text = "May", Value = "5" });
            monthList.Add(new SelectListItem() { Text = "Jun", Value = "6" });
            monthList.Add(new SelectListItem() { Text = "Jul", Value = "7" });
            monthList.Add(new SelectListItem() { Text = "Aug", Value = "8" });
            monthList.Add(new SelectListItem() { Text = "Sept", Value = "9" });
            monthList.Add(new SelectListItem() { Text = "Oct", Value = "10" });
            monthList.Add(new SelectListItem() { Text = "Nov", Value = "11" });
            monthList.Add(new SelectListItem() { Text = "Dec", Value = "12" });
            //monthList.ForEach(m => m.Selected = (m.Value == DateTime.Now.Month.ToString()));
            ViewBag.MonthList = monthList;

            //绑定日
            List<SelectListItem> dayList = new List<SelectListItem>();
            for (int d = 1; d <= 31; d++)
            {
                dayList.Add(new SelectListItem() { Text = d.ToString().PadLeft(2, '0'), Value = d.ToString() });
            }
            ViewBag.DayList = dayList;
            #endregion

            return View();
        }
        /// <summary>
        /// post visit save 
        /// </summary>
        /// <returns></returns>
        public JsonResult PostVisitNSave() {
            int newsfeedId = RequestHelper.GetValInt("newsfeedId");
            int activeType = RequestHelper.GetValInt("activeType");

            int handleType = RequestHelper.GetValInt("handleType");
            int postType = RequestHelper.GetValInt("postType");
            int supplyId = RequestHelper.GetValInt("supplyId");
            string experience = RequestHelper.GetUrlDecodeVal("experience");
            string date = RequestHelper.GetUrlDecodeVal("date");
            string selDate = RequestHelper.GetVal("selDate");
            string sharing = RequestHelper.GetUrlDecodeVal("sharing"); //3-4-2
            string guid = RequestHelper.GetUrlDecodeVal("guid");
            int countryId = RequestHelper.GetValInt("countryId");
            int cityId = RequestHelper.GetValInt("cityId");
            int ibtType = RequestHelper.GetValInt("ibtType");

            string share_title = RequestHelper.GetUrlDecodeVal("share_title");
            string share_desp = RequestHelper.GetUrlDecodeVal("share_desp");
            string share_comment = RequestHelper.GetUrlDecodeVal("share_comment");
            //pic facebook,linkedin 是需要http://开头的. twitter是需要c:\images 的物理地址上传到上面去的
            string share_pic = RequestHelper.GetUrlDecodeVal("share_pic"); // /images/...
            string share_url = RequestHelper.GetUrlDecodeVal("share_url");
            string twitter_title = RequestHelper.GetUrlDecodeVal("twitter_title");
            int share_facebook = RequestHelper.GetValInt("share_facebook"); //1 选中 ; 0 未选中
            int share_linkedin = RequestHelper.GetValInt("share_linkedin");
            int share_twitter = RequestHelper.GetValInt("share_twitter");
            int share_imeet = RequestHelper.GetValInt("share_imeet");

            string isExpand = RequestHelper.GetVal("isExpand");

            int isChooseCity = RequestHelper.GetValInt("isChooseCity");//1 选中；0 未选中
            int isChooseCountry = RequestHelper.GetValInt("isChooseCountry");

            IMeet.IBT.Common.Configurations.SiteConfiguration _config = new IMeet.IBT.Common.Configurations.SiteConfiguration();
            string siteURL = _config.AlphaSiteUrl;
            string siteName = _config.SiteName;

            MeetingPlan meetingPlan = new MeetingPlan();
            meetingPlan.CreateDate = DateTime.Now;
            meetingPlan.IbtType = ibtType;
            meetingPlan.UserId = UserId;
            meetingPlan.CountryId = countryId;
            meetingPlan.CityId = cityId;
            meetingPlan.SupplyId = supplyId;
            meetingPlan.IsOnSiteManagement = RequestHelper.GetValInt("isOSM");
            meetingPlan.IsSiteInsepection = RequestHelper.GetValInt("isSI");
            meetingPlan.IsGeneralTravel = RequestHelper.GetValInt("isGT");
            if (meetingPlan.IsOnSiteManagement > 0)
            {
                meetingPlan.IsPoloticalSocialCrisis = RequestHelper.GetValInt("isPSC");
                meetingPlan.IsStrike = RequestHelper.GetValInt("isStrike");
                meetingPlan.IsNaturalDisaster = RequestHelper.GetValInt("isND");
                meetingPlan.IsGobalAudience = RequestHelper.GetValInt("isGA");
                meetingPlan.IsNonHotelVenue = RequestHelper.GetValInt("isNHV");
                meetingPlan.IsGroupAttendees = RequestHelper.GetValInt("isGroupA");
                meetingPlan.IsVirtualComponent = RequestHelper.GetValInt("isVC");
                meetingPlan.IsCPLP = RequestHelper.GetValInt("isCPLP");
            }
            meetingPlan.Comments = RequestHelper.GetUrlDecodeVal("meetPlanComment");

            if (!string.IsNullOrEmpty(meetingPlan.Comments))
            {
                experience += "<br/><b>Meeting Planning Comments</b><br/>";
                experience += meetingPlan.Comments;
            }

            string http_pic = "";//share_pic facebook,linkedin 是需要http://开头的
            string phy_pic = ""; //twitter是需要c:\images 的物理地址上传到上面去的
            share_url = siteURL + share_url;
            if (!string.IsNullOrWhiteSpace(share_pic))
            {
                string siteurl = Request.Url.Authority;
                http_pic = siteURL + share_pic.Replace("http://"+siteurl,"");
                phy_pic = Server.MapPath(share_pic.Replace("http://" + siteurl, ""));
            }

            DateTime? pvDate = null;
            if (selDate.ToLower().Equals("true"))
            {
                pvDate = Convert.ToDateTime(date);
            }
            #region LinkedIn
            if (share_linkedin == 1)
            {
                var linkedinShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.LinkedIn);
                if (linkedinShare != null && linkedinShare.UserId > 0)
                {
                    if (string.IsNullOrWhiteSpace(http_pic))
                    {
                        http_pic = siteURL + "/images/logo/logo_w100.gif";
                    }
                    string linkedin_token = linkedinShare.OauthToken;
                    string linkedInXML = @"<share>
                          <comment>" + share_comment + @"</comment>
                          <content>
                            <title>" + share_title + @"</title>
                            <description>" + share_desp + @"</description>
                            <submitted-url>" + share_url + @"</submitted-url>
                            <submitted-image-url>" + http_pic + @"</submitted-image-url> 
                          </content>
                          <visibility> 
                            <code>anyone</code> 
                          </visibility>
                        </share>";
                    SNSHelper.LinkedInConsumer.Share(linkedin_token, linkedInXML);
                }
            }
            #endregion

            #region facebook
            if (share_facebook == 1)
            {
                var facebookShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Facebook);
                if (facebookShare != null && facebookShare.UserId > 0)
                {
                    string facebook_Token = facebookShare.OauthToken;

                    if (string.IsNullOrWhiteSpace(http_pic))
                    {
                        http_pic = siteURL + "/images/logo/logo_w100.gif";
                    }

                    SNSHelper.FacebookClient.Share(facebook_Token, share_title, share_url, "", share_desp, http_pic, share_comment);
                }
            }
            #endregion

            #region Twitter
            if (share_twitter == 1)
            {
                var twitterShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Twitter);
                if (twitterShare != null && twitterShare.UserId > 0)
                {
                    string twitter_str = "I've posted a visit to {0}. Start/share your own travel map and travel history!";
                    string twitter_shareurl = " @i_meet " + share_url;
                    string twitter_msg = String.Format(twitter_str, twitter_title);
                    int len = twitter_shareurl.Length + twitter_msg.Length;
                    if (len > 140)
                    {
                        twitter_msg = StringHelper.Truncate(twitter_msg, (twitter_msg.Length - len + 136), "...!");
                    }
                    twitter_msg += twitter_shareurl;
                    string twitter_token = twitterShare.OauthToken;
                    if (!string.IsNullOrWhiteSpace(phy_pic))
                    {
                        SNSHelper.TwitterConsumer.StatusesUpdate(twitter_token, twitter_msg, phy_pic);
                    }
                    else
                    {
                        SNSHelper.TwitterConsumer.StatusesUpdate(twitter_token, twitter_msg);
                    }
                }
            }
            #endregion

            #region iMeet
            if (share_imeet == 1)
            {
                var imeetShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.IMeet);

                string imeet_http_pic = http_pic.Replace("_570x430.jpg", "_bn.jpg");
                if (imeetShare != null && imeetShare.UserId > 0)
                {
                    string imeet_token = imeetShare.OauthToken;
                    string imeet_msg = "I've posted a visit to " + twitter_title;
                    imeet_msg += " " + share_url;
                    if (!string.IsNullOrWhiteSpace(share_pic))
                    {
                        SNSHelper.iMeetConsumer.StatusesUpdate(imeet_token, imeetShare.Sns_UId, imeet_msg, share_url, imeet_http_pic);
                    }
                    else
                    {
                        SNSHelper.iMeetConsumer.StatusesUpdate(imeet_token, imeetShare.Sns_UId, imeet_msg);
                    }
                }
            }
            #endregion

            string pvId = "";
            if (activeType == (int)Identifier.FeedActiveType.PostVisit)
            {
                pvId = _newsFeedService.GetNewsFeedInfo(newsfeedId).ObjData;
            }

            var bmi = new BoolMessageItem<int>(0, false, "");
           
            if ((isChooseCountry == 1) || (ibtType >= (int)Identifier.IBTType.City && !_supplierService.HasUserPostVisited(UserId, (int)Identifier.IBTType.Country, countryId)))
            {
               var bmCountry= _supplierService.SavePostVisit(postType, UserId, (int)Identifier.IBTType.Country, countryId, 0, 0, experience, sharing, pvDate);              
            }
            if ((isChooseCity == 1) || (ibtType == (int)Identifier.IBTType.Supply) && !_supplierService.HasUserPostVisited(UserId, (int)Identifier.IBTType.City, cityId))
            {
                var bmCity = _supplierService.SavePostVisit(postType, UserId, (int)Identifier.IBTType.City, countryId, cityId, 0, experience, sharing, pvDate);
            }

            //添加或者编辑修改(未完成)
            if (handleType == 0)
            {
                //添加
                bmi = _supplierService.SavePostVisit(postType, UserId, ibtType, countryId, cityId, supplyId, experience, sharing, pvDate);
                //有填写experience时添加积分
                if (!string.IsNullOrEmpty(experience))
                {
                    _travelBadgeService.InsertPointForGeneralComments(experience, UserId, bmi.Item);
                }

                //添加meeting plan
                if (bmi.Success && isExpand == "true")
                {
                    meetingPlan.PVId = bmi.Item;
                    _travelBadgeService.InsertMeetingPlan(meetingPlan);
                }
            }
            else
            {
                if (pvId != "")
                {
                    //编辑
                    bmi = _supplierService.EditPostVisit(Convert.ToInt32(pvId.ToString()), newsfeedId, experience, pvDate);

                }
                else
                {
                    bmi = _newsFeedService.EditNewsFeed(newsfeedId, experience, pvId, pvDate);
                }
            }

            //更新用户上传的图片
            _supplierService.SetPostVistAttachmentPvId(guid, bmi.Item);
            if (string.IsNullOrWhiteSpace(newsfeedId.ToString()) || newsfeedId == 0)
            {
                newsfeedId = Convert.ToInt32(bmi.Message.ToString());
            }
            _ibtServices.SetAttachmentObjId(guid, newsfeedId);

            return Json(bmi, JsonRequestBehavior.DenyGet);
        }      

        
        /// <summary>
        /// post visit 图片旋转
        /// </summary>
        /// <returns></returns>
        public JsonResult PVRotate()
        {
            int attId = RequestHelper.GetValInt("id");
            int rorate = RequestHelper.GetValInt("rorate");

            var bm=_ibtServices.PVImageRotate(attId, rorate, HttpContext.Server.MapPath("~"));

            return Json(bm);
        }

        #endregion

        #endregion

        #region Show friend
        public ActionResult ShowFriend(int ibtType, int objId)
        {
            ViewBag.UserId = UserId;
            ViewBag.Profile = _profileService.GetUserProfile(UserId);
            ViewBag.IbtType = ibtType;
            //todo:pv需要处理国家与城市的
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCountryInfo(UserId, objId);
                    break;
                case (int)Identifier.IBTType.City:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCityInfo(UserId, objId);
                    break;
                default:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitSupplyInfo(UserId, objId);
                    break;
            }

            return View();
        }
        #endregion

        #region Share
        public ActionResult Share(int ibtType, int objId)  //(暂不用)
        {
            ViewBag.Profile = _profileService.GetUserProfile(UserId);
            //int ibtType = RequestHelper.GetValInt("ibtType");
            //int objId = RequestHelper.GetValInt("objId");
            ViewBag.IbtType = ibtType;
            //todo:pv需要处理国家与城市的
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCountryInfo(UserId, objId);
                    break;
                case (int)Identifier.IBTType.City:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitCityInfo(UserId, objId);
                    break;
                default:
                    ViewBag.PostVisitInfo = _supplierService.GetPostVisitSupplyInfo(UserId, objId);
                    break;
            }

            return View();
        }

        [ValidateInput(false)]
        public JsonResult Sharing()
        {
            string share_title = RequestHelper.GetUrlDecodeVal("share_title");
            string share_desp = RequestHelper.GetUrlDecodeVal("share_desp");
            string share_comment = RequestHelper.GetUrlDecodeVal("share_comment");
            //pic facebook,linkedin 是需要http://开头的. twitter是需要c:\images 的物理地址上传到上面去的
            string share_pic = RequestHelper.GetUrlDecodeVal("share_pic"); // /images/logo/logo.gif
            string share_url = RequestHelper.GetUrlDecodeVal("share_url");

            int share_facebook = RequestHelper.GetValInt("share_facebook"); //1 选中 ; 0 未选中
            int share_linkedin = RequestHelper.GetValInt("share_linkedin");
            int share_twitter = RequestHelper.GetValInt("share_twitter");
            int share_imeet = RequestHelper.GetValInt("share_imeet");

            string http_pic = ""; //share_pic facebook,linkedin 是需要http://开头的
            string phy_pic = ""; //twitter是需要c:\images 的物理地址上传到上面去的

            #region LinkedIn
            if (share_linkedin == 1)
            {
                var linkedinShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.LinkedIn);
                if (linkedinShare != null && linkedinShare.UserId > 0)
                {
                    string linkedin_token = linkedinShare.OauthToken;
                    string linkedInXML = @"<share>
                          <comment>" + share_comment + @"</comment>
                          <content>
                            <title>" + share_title + @"</title>
                            <description>" + share_title + @"</description>
                            <submitted-url>" + share_url + @"</submitted-url>
                            <submitted-image-url>" + http_pic + @"</submitted-image-url> 
                          </content>
                          <visibility> 
                            <code>anyone</code> 
                          </visibility>
                        </share>";
                    SNSHelper.LinkedInConsumer.Share(linkedin_token, linkedInXML);
                }
            }
            #endregion

            #region facebook
            if (share_facebook == 1)
            {
                var facebookShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Facebook);
                if (facebookShare != null && facebookShare.UserId > 0)
                {
                    string facebook_Token = facebookShare.OauthToken;
                    SNSHelper.FacebookClient.Share(facebook_Token, share_title, share_url, "", share_desp, http_pic, share_comment);
                }
            }
            #endregion

            #region Twitter
            if (share_twitter == 1)
            {
                var twitterShare = _snsConnectService.GetSNSConnect(UserId, Identifier.SnsType.Twitter);
                if (twitterShare != null && twitterShare.UserId > 0)
                {
                    string twitter_token = twitterShare.OauthToken;
                    string twitter_msg = string.IsNullOrWhiteSpace(share_comment) ? share_title + "," + share_desp : share_comment;
                    twitter_msg = StringHelper.Truncate(twitter_msg, 136, "...");
                    SNSHelper.TwitterConsumer.StatusesUpdate(twitter_token, twitter_msg, phy_pic);
                }
            }
            #endregion

            return Json(new BoolMessage(true, ""), JsonRequestBehavior.DenyGet);
        }
        #endregion

        #region Profile & profile Feeds
        public ActionResult Profile(int userId)
        {
            bool showMenu = false;
            int IsShowMyProfile = 1;//0是看好友的。 1是看自己的 ,2,大家的
            if (userId == UserId)
            {
                IsShowMyProfile = 2; //地址参数是自己的。查看好友的feed
                showMenu = true;
            }
            else
            {
                if (userId < 1) //无。查看好友的feed
                {
                    userId = UserId;
                    IsShowMyProfile = 2;
                    showMenu = true;
                }

            }
            ViewBag.FirstMaps = _mapsService.ProfileFirstItem(userId);
            ViewBag.IsShowMyProfile = IsShowMyProfile;
            ViewBag.UserId = userId;
            ViewBag.ShowMenu = showMenu;
            User user = _profileService.GetUserInfo(userId);
            ViewBag.UserTravelBadge = _travelBadgeService.GetUserTravelbadge(user);
            return View();
        }

        /// <summary>
        /// profile & my travel feed list
        /// </summary>
        /// <returns></returns>
        public JsonResult FeedList()
        {
            int userId = RequestHelper.GetValInt("userId", UserId);
            int tabId = RequestHelper.GetValInt("tabId", 1); //1 profile,2 mytravel
            int type = RequestHelper.GetValInt("type", 2); //0 Friends' Activties （Friends travel） 1: my activties (my travel) ,2:all activties
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 20);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string sortByRating = RequestHelper.GetVal("sortByRating");
            int categoryId = RequestHelper.GetValInt("categoryId", 1);//dbo.AttributeData.ParentId=@categoryId: -1:Countries、-2:Cities、3:Hotels、4:Lifestyle、5:Attractions
            IList<FeedList> feedList = new List<FeedList>();

            if (tabId == 2)
            {
                //travel
                feedList = _supplierService.FeedTravelList(userId, categoryId, page, pageSize, filterLetter, sortByRating);
            }
            else
            {
                if (type == 0)
                    //Friends' Activties
                    feedList = _supplierService.FeedFriendActivtieLists(userId, page, pageSize);
                else if (type == 1)
                    //my activties
                    feedList = _supplierService.FeedUserActivtieLists(userId, page, pageSize);
                else
                    feedList = _supplierService.FeedAllActivitiesLists(userId, page, pageSize);
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            int addTravel = (int)Identifier.FeedActiveType.AddTravel;
            int postVisit = (int)Identifier.FeedActiveType.PostVisit;
            int rating = (int)Identifier.FeedActiveType.Rating;
            int recommended = (int)Identifier.FeedActiveType.Recommended;
            int wantGoBack = (int)Identifier.FeedActiveType.WantGoBack;
            int bucketList = (int)Identifier.FeedActiveType.BucketList;
            int ibt = (int)Identifier.FeedActiveType.IBT;

            int itemTotal = feedList.Count;
            string ShowImg;
            string showNick;
            int friendCount;
            string strcount;
            int selfId;
            string uncheckbox_recommended;
            string uncheckbox_WantGoBack;
            string uncheckbox_BucketList;
            foreach (var item in feedList)
            {
                ShowImg = "";
                showNick = "";
                friendCount = 0;
                strcount = "";
                selfId = 0;
                uncheckbox_recommended = "";
                uncheckbox_WantGoBack = "";
                uncheckbox_BucketList = "";
                #region post visit
                if (item.ActiveType == addTravel || item.ActiveType == postVisit || item.ActiveType == rating || item.ActiveType == recommended || item.ActiveType == wantGoBack || item.ActiveType == bucketList || item.ActiveType == ibt)
                {
                    sb.Append("<li class=\"feed\">");
                    sb.Append("  <span class=\"operating\">");
                    //评分
                    sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                    sb.Append("      <span class=\"FeedRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\"></span> ");
                    sb.Append("    </span>");
                    //ibt I've been there
                    int ibtCount = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IbtCount;
                    sb.Append("    <a href=\""+PageHelper.PostVisitNUrl(item.IBTType,item.ExObjId)+"\" class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                    {
                        if (item.IBTType == (int)Identifier.IBTType.Supply && item.ServiceType == (int)Identifier.ServiceType.ServiceProvider)
                            sb.Append("     <span class=\"bg\">I've used the service</span>");
                        else
                            sb.Append("     <span class=\"bg\">I've been there</span>");
                    }
                    else
                    {
                        if (item.IBTType == (int)Identifier.IBTType.Supply && item.ServiceType == (int)Identifier.ServiceType.ServiceProvider)
                            sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've used the service</span><span id=\"feedIBTCount_" + item.NewsFeedId + "\" data-ibtcount=\"" + ibtCount + "\"> " + item.IbtCount.ToString() + "</span></span>");
                        else
                            sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span><span id=\"feedIBTCount_" + item.NewsFeedId + "\" data-ibtcount=\"" + ibtCount + "\"> " + item.IbtCount.ToString() + "</span></span>");
                    }
                    sb.Append("    </a>");
                    //end:operating
                    sb.Append("</span>	");
                    if (item.SupplyId > 0)
                    {
                        ShowImg = PageHelper.GetSupplyPhoto(item.SupplyId, item.ExShowImg, Identifier.SuppilyPhotoSize._xtn);
                    }
                    else
                    {
                        ShowImg = item.ExShowImg;
                    }
                    //图片
                    sb.Append("<a class=\"userPic\" href=\"/Profile/" + item.UserId.ToString() + "\"><img alt=\"\" src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\"></a>");
                    sb.Append("<dl class=\"info\">");
                    sb.Append("  <dt>");
                    //标题（城市/国家）
                    if (item.IBTType == (int)Identifier.IBTType.Country)
                    {
                        if (item.ActiveType == rating)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has rated <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == recommended)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> recommended <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == wantGoBack)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> want to go / go back to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == bucketList)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has added <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a> to " + item.ExGender + " bucket list </span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                    }
                    else if (item.IBTType == (int)Identifier.IBTType.City)
                    {
                        if (item.ActiveType == rating)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has rated <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == recommended)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> recommended <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == wantGoBack)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> want to go / go back to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == bucketList)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has added <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a> to " + item.ExGender + " bucket list </span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                    }
                    else
                    {
                        if (item.ActiveType == rating)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has rated <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == recommended)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> recommended <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == wantGoBack)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> want to go / go back to <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == bucketList)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has added <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a> to " + item.ExGender + " bucket list </span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                    }
                    //RWB
                    sb.Append("  <div class=\"choose\">");
                    sb.Append("     <span class=\"checkBg\"><a href=\"" + item.ExTitleUrl + "\"><img src=\"" + ShowImg + "\" class=\"mR5\" alt=\"\"  width=\"36\"/></a>");
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5 cursordefault\" onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"2\" data-readonly=\"true\"  data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                    sb.Append("<span class=\"line\"></span>");
                    if (!(item.IBTType == (int)Identifier.IBTType.Supply && item.ServiceType == (int)Identifier.ServiceType.ServiceProvider))
                    {
                        uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                        sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5 cursordefault\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"3\"  data-readonly=\"true\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                        sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                        sb.Append("          </span>");
                        sb.Append("<span class=\"line\"></span>");
                        uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                        sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5 cursordefault\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"4\"  data-readonly=\"true\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                        sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                        sb.Append("          </span>");
                        sb.Append("      </span>");
                    }
                    //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                    //Elson(showNick) and 9 Friends has Been There 

                    //showNick = type == 1 ? "You " : item.FirstName;
                    friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                    //strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                    //selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;  //
                    if (!(item.IBTType == (int)Identifier.IBTType.Supply && item.ServiceType == (int)Identifier.ServiceType.ServiceProvider))
                    {
                        if (friendCount == 0)
                        {
                            sb.Append("         <span class=\"friendsInfo f10 C7a7a7a\">Friends That Have Been Here: " + friendCount + "</span>");
                        }
                        else {
                            sb.Append("         <span class=\"friendsInfo f10 C7a7a7a\">Friends That Have Been Here: <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + friendCount + " Friend Who\\'ve Been There');\" class=\"underline\">" + friendCount + "</a></span>");
                        }

                        //if (friendCount == 0 && selfId == UserId)
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                        //}
                        //else if (friendCount == 0 && selfId != UserId)
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                        //}
                        //else if (friendCount > 0 && selfId != UserId)
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                        //}
                        //else if (showNick == item.FirstName && friendCount == 1)
                        //{
                        //    strcount = "You";
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and " + strcount + " has Been There</span>");
                        //}
                        //else
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                        //}
                    }
                    sb.Append("  </div>");
                    sb.Append("  </dt>");
                    sb.Append("  <dd>");
                    //编辑Edit和删除deleted (判断登录用户是否和该用户一致)
                    if ((item.ActiveType == (int)Identifier.FeedActiveType.IBT || item.ActiveType == (int)Identifier.FeedActiveType.AddTravel || item.ActiveType == (int)Identifier.FeedActiveType.PostVisit) && item.UserId == UserId)
                    {
                        sb.Append("    <div class=\"relative\">");
                        sb.Append("    <div class=\"editBox select\">");
                        sb.Append("    <span class=\"arrow\">&nbsp;</span>");
                        sb.Append("    <div class=\"list\">");
                        sb.Append("      <ul>");
                        sb.Append("        <li onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "?newsfeedId=" + item.NewsFeedId.ToString() + "','Get','',function(){$('#handleType').val(1);ibt.PvPopLoad();},'Edit POst visit');\">Edit</li>");
                        int IBTId = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IBTId;
                        sb.Append("        <li onclick=\"jTravelFeed.DelPVNewsfeed(" + item.NewsFeedId + "," + item.NewsFeedId + "," + IBTId + ");\">Delete</li>");
                        sb.Append("      </ul>");
                        sb.Append("    </div>");
                        sb.Append("    </div>");
                        sb.Append("    </div>");
                    }
                    //Post visit 上传的图片
                    //sb.Append(this.getFeedPostVisitPhotoList(item.NewsFeedId, item.PostVistAttachmentList));
                    #region 评论的图片列表(user: country / city / supplier listing post)
                    IList<Attachment> attachmentList = _supplierService.GetAttachment(item.NewsFeedId, Identifier.AttachmentType.NewsFeed);
                    if (attachmentList.Count > 0)
                    {
                        sb.Append("  <div class=\"fixbox photo\">");
                        foreach (var att in attachmentList)
                        {
                            sb.Append("           <a href=\"javascript:void(0);\" onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\"><img class=\"mR10\" src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" /></a>");
                        }
                        sb.Append("  </div>");
                    }
                    #endregion
                    //------------group------------------- 只有post visit会显示，因为这个是experience
                    if (item.ActiveType == postVisit)
                    {
                        sb.Append("  <p>");
                        sb.Append(string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal);
                        sb.Append("  </p>");
                    }

                    sb.Append("<div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("     <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                    //replyBox---comments
                    //sb.Append("<div class=\"replyBox\" style=\"display: none;\"  id=\"replyBox_" + item.NewsFeedId.ToString() + "\">");
                    //sb.Append("   <ul id=\"replyList_" + item.NewsFeedId.ToString() + "\">");
                    //sb.Append("   </ul>");
                    ////replyBox
                    //sb.Append("   <ul>");
                    //sb.Append("     <li>");
                    //sb.Append("     <textarea class=\"autogrow-short\" id=\"commnetTxt_" + item.NewsFeedId.ToString() + "\">Write a message...</textarea>");
                    //sb.Append("     <span class=\"commnet_btnSave\"><input onclick=\"feedComments.SaveComment(" + item.NewsFeedId.ToString() + ");\" type=\"button\" value=\"Comment\" /></span>");
                    //sb.Append("     </li>");
                    //sb.Append("   </ul>");
                    //sb.Append("</div>");
                    sb.Append("</div>");
                    //history
                    //int historycount = GetHistoryCount(tabId, item.IBTType, item.ExObjId, item.UserId);
                    if (item.IbtCount > 1)
                    {
                        sb.Append("   <div class=\"history\">");
                        sb.Append("      <span class=\"sliderD sliderU \" id=\"feedHistoryTxt_" + item.NewsFeedId.ToString() + "\"  onclick=\"feedHistory.ShowFeedHistory('" + tabId.ToString() + "','" + item.NewsFeedId.ToString() + "','" + item.IBTType.ToString() + "','" + item.ExObjId.ToString() + "','" + item.UserId.ToString() + "');\"> view history  </span>");
                        sb.Append("        <div class=\"historyList\" id=\"feedHistoryList_" + item.NewsFeedId.ToString() + "\">");
                        sb.Append("        </div>");
                        sb.Append("   </div>");
                    }
                    sb.Append("   </dd>");
                    sb.Append("   </dl>");
                    sb.Append("</li>");

                }
                #endregion

                #region update profile image
                else if (item.ActiveType == (int)Identifier.FeedActiveType.ProImage) //更新profile image
                {
                    sb.Append("<li>");
                    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\" alt=\"User Name\"></a>");
                    sb.Append("  <dl>");
                    sb.Append("     <dt><a href=\"/Profile/" + item.UserId.ToString() + "\">" + item.FirstName + "</a><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span></dt>");
                    sb.Append("     <dd>");
                    sb.Append("     <p>" + (string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal) + " </p>");
                    sb.Append("     <div class=\"fixbox photo\">");
                    string feedPath = "/Staticfile/Newsfeed/" + (item.UserId / 1000) + "/" + item.UserId.ToString() + "/";
                    sb.Append("     <img class=\"mR10\" width=\"126\" alt=\"\" src=\"" + feedPath + item.ObjData + "\">");
                    sb.Append("     </div>");
                    //评论数
                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                    sb.Append("</div>");
                    sb.Append("     </dd>");
                    sb.Append("  </dl>");
                    sb.Append("</li>");
                }
                #endregion

                #region update status
                else if (item.ActiveType == (int)Identifier.FeedActiveType.UpdateStatus)//更新status
                {
                    sb.Append("<li>");
                    sb.Append("  <a class=\"userPic\" href=\"/Profile/" + item.UserId.ToString() + "\"><img alt=\"\" src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\"></a>");
                    sb.Append("  <dl class=\"info\">");
                    sb.Append("     <dt><a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a> <span class=\"f15\">updated " + item.ExGender + " profile status </span> <span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span></dt>");
                    sb.Append("     <dd>");
                    sb.Append("     <p>" + (string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal) + "</p>");
                    //评论数
                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                    sb.Append("</div>");
                    sb.Append("     </dd>");
                    sb.Append("  </dl>");
                    sb.Append("</li>");
                }
                #endregion

                #region add new friends
                else if (item.ActiveType == (int)Identifier.FeedActiveType.AddNewFriend)//添加新好友（最近7天）
                {
                    IList<ConnectionListItem> connectionList = _supplierService.GetNewsfeedFriend(item.UserId, item.NewsFeedId);
                    sb.Append("<li>");
                    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" class=\"userPic\"><img src=\"" + PageHelper.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\" alt=\"User Name\"></a>");
                    sb.Append("  <dl>");
                    sb.Append("     <dt><a href=\"/Profile/" + item.UserId.ToString() + "\">" + item.FirstName + "</a><span class=\"f15\"> has made new friends </span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                    sb.Append("          <span class=\"friendInfo\">" + item.FirstName + " is now friends with ");
                    #region 好友列表
                    if (connectionList.Count > 0)
                    {
                        string friendName;
                        int i = 0;
                        foreach (var con in connectionList)
                        {
                            if (con.UserId == 1)
                            {
                                friendName = _profileService.GetUserProfile(con.FriendId).Firstname;
                            }
                            else
                            {
                                friendName = _profileService.GetUserProfile(con.FriendId).Firstname;
                            }
                            if (i == 0)
                            {
                                friendName = " <a href=\"/Profile/" + con.FriendId.ToString() + "\">" + friendName + "</a>";
                            }
                            else if (i == connectionList.Count - 1)
                            {
                                friendName = " and " + "<a href=\"/Profile/" + con.FriendId.ToString() + "\">" + friendName + "</a>";
                            }
                            else
                            {
                                friendName = "," + " <a href=\"/Profile/" + con.FriendId.ToString() + "\">" + friendName + "</a>";
                            }
                            sb.Append(friendName);
                            i++;
                        }
                        sb.Append("     </span></dt><dd class=\"\">");
                        sb.Append("  <div class=\"fixbox  smallPhoto\">");
                        foreach (var conn in connectionList)
                        {
                            sb.Append("     <a href=\"/Profile/" + conn.FriendId.ToString() + "\" class=\"userPic\"><img class=\"mR5\" alt=\"\" src=\"" + PageHelper.GetUserAvatar(conn.FriendId, (int)Identifier.AccountSettingAvatarSize._xlat) + "\" width=\"40\"></a>");
                        }
                        sb.Append("  </div>");
                    }
                    #endregion
                    #region 评论列表和评论的回复框
                    //评论数
                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                    sb.Append("</div>");
                    #endregion
                    sb.Append("     </dd>");
                    sb.Append("  </dl>");
                    sb.Append("</li>");
                }
                #endregion

                #region post new comments
                else if (item.ActiveType == (int)Identifier.FeedActiveType.PostComments)//发表评论Comments
                {
                    sb.Append("<li>");
                    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\" alt=\"User Name\"></a>");
                    sb.Append("  <dl>");
                    sb.Append("     <dt><a href=\"/Profile/" + item.UserId.ToString() + "\">" + item.FirstName + "</a><span class=\"f15\"> has written a post to <a href=\"" + item.ExTitleUrl + "\">" + item.ExTitle.ToString() + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span></dt>");
                    sb.Append("     <dd>");
                    sb.Append("     <p>" + (string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal) + "</p>");

                    #region 评论的图片列表(user: country / city / supplier listing post)
                    IList<Attachment> attachmentList = _supplierService.GetAttachment(item.NewsFeedId, Identifier.AttachmentType.NewsFeed);
                    if (attachmentList.Count > 0)
                    {
                        sb.Append("  <div class=\"fixbox photo\">");
                        foreach (var att in attachmentList)
                        {
                            sb.Append("           <a href=\"javascript:void(0);\" onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\"><img class=\"mR10\" src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" /></a>");
                        }
                        sb.Append("  </div>");
                    }
                    #endregion

                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\">");
                    #region 评论的列表和评论的回复框
                    //评论数
                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                    sb.Append("</div>");
                    #endregion
                    sb.Append("</div>");

                    sb.Append("     </dd>");
                    sb.Append("  </dl>");
                    sb.Append("</li>");
                }
                #endregion

                #region admin post or country / city / supplier listing post
                else //(int)Identifier.FeedActiveType.ObjPost
                {
                    if (item.SupplyId > 0)
                    {
                        ShowImg = PageHelper.GetSupplyPhoto(item.SupplyId, item.ExShowImg, Identifier.SuppilyPhotoSize._xtn);
                    }
                    else
                    {
                        ShowImg = item.ExShowImg;
                    }
                    sb.Append("<li>");
                    sb.Append("  <a href=\"" + item.ExTitleUrl + "\" class=\"userPic\"><img src=\"" + ShowImg + "\" alt=\"User Name\"></a>");
                    sb.Append("  <dl>");
                    sb.Append("     <dt><a href=\"" + item.ExTitleUrl + "\">" + item.ExTitle + "</a><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span></dt>");
                    sb.Append("     <dd>");
                    sb.Append("     <p>" + (string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal) + " </p>");

                    #region 评论的图片列表(admin post or country / city / supplier listing post)
                    IList<Attachment> attachmentList = _supplierService.GetAttachment(item.NewsFeedId, Identifier.AttachmentType.NewsFeed);
                    if (attachmentList.Count > 0)
                    {
                        sb.Append("  <div class=\"fixbox photo\">");
                        foreach (var att in attachmentList)
                        {
                            sb.Append("           <a href=\"javascript:void(0);\" onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\"><img class=\"mR10\" src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" /></a>");
                        }
                        sb.Append("  </div>");
                    }
                    #endregion

                    #region 评论的列表和评论的回复框
                    //评论数
                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                    sb.Append("</div>");
                    #endregion

                    sb.Append("     </dd>");
                    sb.Append("  </dl>");
                    sb.Append("</li>");
                }
                #endregion
            }

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DelPVNewsfeed()
        {
            int newfeedId = RequestHelper.GetValInt("objId");
            int IBTId = RequestHelper.GetValInt("ibtId");
            var bmi = _newsFeedService.NewsFeedIsDel(newfeedId);
            if (bmi.Success)
            {
                //相应的ibtcount要减一
                _supplierService.ReIBTCount(IBTId);
            }
            return Json(bmi);

        }

        #region PV上传的图片
        /// <summary>
        /// Post visit 上传的图片
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <param name="list"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        private string getFeedPostVisitPhotoList(int newsFeedId, List<PostVistAttachment> list, string title = "")
        {
            if (list == null || list.Count == 0)
                return "";
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<div class=\"photo\">");
                foreach (var item in list)
                {
                    sb.Append("<a href=\"" + item.SourceFileName + "_big.jpg\" rel=\"lightbox[img_" + newsFeedId.ToString() + "]\" title=\"" + title + "\" class=\"mR5\"><img src=\"" + item.SourceFileName + "_tn.jpg\" alt=\"Photos description \" /></a>");
                }
                sb.Append("</div>");
                return sb.ToString();
            }
        }
        #endregion

        #region posted visit history
        /// <summary>
        /// Post visit History
        /// </summary>
        /// <returns></returns>
        public JsonResult getFeedPostVisitHistoryList()
        {
            int tabId = RequestHelper.GetValInt("tabId");
            int feedId = RequestHelper.GetValInt("feedId");
            int ibtType = RequestHelper.GetValInt("ibtType");
            int objId = RequestHelper.GetValInt("objId");
            int userId = RequestHelper.GetValInt("userId");
            string strWhere = "";
            string strActive = " AND ActiveType IN (5,6,7)";

            if (ibtType == (int)Identifier.IBTType.Country)
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.CountryId=" + objId.ToString();
            }
            else if (ibtType == (int)Identifier.IBTType.City)
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.CityId=" + objId.ToString();
            }
            else
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.SupplyId=" + objId.ToString();
            }
            int total = 0;
            int itemTotal = 0;
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);

            var list = HistoryListData(userId, feedId, strWhere, out total, out itemTotal);
            return Json(new BoolMessageItem(null, true, list), JsonRequestBehavior.AllowGet);

        }

        public int GetHistoryCount(int tabId, int ibtType, int objId, int userId)
        {
            string strWhere = "";
            string strActive = "";
            if (tabId == 2)
            {
                strActive = " AND ActiveType IN (5,6,7)";
            }
            if (ibtType == (int)Identifier.IBTType.Country)
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.CountryId=" + objId.ToString();
            }
            else if (ibtType == (int)Identifier.IBTType.City)
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.CityId=" + objId.ToString();
            }
            else
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.SupplyId=" + objId.ToString();
            }

            int total = 0;
            var list = _newsFeedService.GetFeedHistoryList(strWhere, userId, out total);

            return list.Count();
        }

        private string HistoryListData(int userId, int feedId, string strWhere, out int total, out int itemTotal)
        {
            var list = _newsFeedService.GetFeedHistoryList(strWhere, userId, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {

                sb.Append("<dl id=\"HistoryItem_" + item.NewsFeedId.ToString() + "\">");
                sb.Append("  <dt>");
                //编辑Edit和删除deleted 
                if ((item.ActiveType == (int)Identifier.FeedActiveType.IBT || item.ActiveType == (int)Identifier.FeedActiveType.AddTravel || item.ActiveType == (int)Identifier.FeedActiveType.PostVisit) && item.UserId == UserId)
                {
                    sb.Append("    <div class=\"relative\">");
                    sb.Append("    <div class=\"editBox select\">");
                    sb.Append("    <span class=\"arrow\">&nbsp;</span>");
                    sb.Append("    <div class=\"list\">");
                    sb.Append("      <ul>");
                    sb.Append("        <li onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "?newsfeedId=" + item.NewsFeedId.ToString() + "','Get','',function(){$('#handleType').val(1);ibt.PvPopLoad();},'Edit POst visit');\">Edit</li>");
                    int IBTId = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IBTId;
                    sb.Append("        <li onclick=\"jTravelFeed.DelPVNewsfeed(" + feedId + "," + item.NewsFeedId + "," + IBTId + ");\">Delete</li>");
                    sb.Append("      </ul>");
                    sb.Append("    </div>");
                    sb.Append("    </div>");
                    sb.Append("    </div>");
                }
                //标题（城市/国家）
                if (item.IBTType == (int)Identifier.IBTType.Country)
                {
                    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                }
                else if (item.IBTType == (int)Identifier.IBTType.City)
                {
                    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                }
                else
                {
                    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                }
                //if (item.IBTType == (int)Identifier.IBTType.Country)
                //{
                //    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                //}
                //else if (item.IBTType == (int)Identifier.IBTType.City)
                //{
                //    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                //}
                //else
                //{
                //    sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted visit to <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                //}
                sb.Append("    </dt>");
                sb.Append("    <dd>");
                if (item.ActiveType == (int)Identifier.FeedActiveType.PostVisit)
                {
                    sb.Append("  <p>");
                    sb.Append(string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal);
                    sb.Append("  </p>");
                }
                sb.Append("<div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                //comments(数量)
                int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                sb.Append("     <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                //评论数
                //sb.Append("    <span class=\"commentsTxt\">");
                //sb.Append("        <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\" class=\"mR5\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + item.CommentCount.ToString() + "\" >Comments (" + item.CommentCount.ToString() + ")</div></a>");//|<a onclick=\"ibt.Share(" + item.IBTType + "," + item.ExObjId + ")\" href=\"javascript:void(0);\" class=\"mL5\">Share</a>
                //sb.Append("    </span>");
                ////replyBox---comments
                //sb.Append("<div class=\"replyBox\" style=\"display: none;\"  id=\"replyBox_" + item.NewsFeedId.ToString() + "\">");
                //sb.Append("   <ul id=\"replyList_" + item.NewsFeedId.ToString() + "\">");
                //sb.Append("   </ul>");
                ////replyBox
                //sb.Append("   <ul>");
                //sb.Append("     <li>");
                //sb.Append("     <textarea class=\"autogrow-short\" id=\"commnetTxt_" + item.NewsFeedId.ToString() + "\">Write a message...</textarea>");
                //sb.Append("     <span class=\"commnet_btnSave\"><input onclick=\"feedComments.SaveComment(" + item.NewsFeedId.ToString() + ");\" type=\"button\" value=\"Comment\" /></span>");
                //sb.Append("     </li>");
                //sb.Append("   </ul>");
                //sb.Append("</div>");
                sb.Append("    </dd>");
                sb.Append("</dl>");
            }
            return sb.ToString();
        }
        #endregion

        #region newsfeed date sort
        public JsonResult getFeedDateList()
        {
            int ibtType = RequestHelper.GetValInt("ibtType");
            int userId = RequestHelper.GetValInt("userId");
            var list = _newsFeedService.GetFeedDateList(userId, ibtType);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append("<a class=\"btnColorG IERound\" href=\"javascript:void(0);\" onclick=\"jTravelFeed.dateSearch(this,'" + DateTime.Parse(item.YearMonth).ToString("MMM yyyy") + "');\">" + DateTime.Parse(item.YearMonth).ToString("MMM yyyy") + "</a>");
            }
            return Json(new BoolMessageItem(null, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getServicesDate()
        {
            int ibtType = RequestHelper.GetValInt("ibtType");
            int userId = RequestHelper.GetValInt("userId");
            var list = _newsFeedService.GetServicesDate(userId, ibtType);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append("<a class=\"btnColorG IERound\" href=\"javascript:void(0);\" onclick=\"jTravelFeed.dateSearch(this,'" + DateTime.Parse(item.YearMonth).ToString("MMM yyyy") + "');\">" + DateTime.Parse(item.YearMonth).ToString("MMM yyyy") + "</a>");
            }
            return Json(new BoolMessageItem(null, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getFeedDataList()
        {
            int userId = RequestHelper.GetValInt("userId", UserId);
            string yearMonth = RequestHelper.GetVal("yearMonth");
            string year = "";
            string month = "";
            if (!string.IsNullOrEmpty(yearMonth))
            {
                year = DateTime.Parse(yearMonth).Year.ToString();
                month = DateTime.Parse(yearMonth).Month.ToString();
            }
            int type = RequestHelper.GetValInt("type", 2); //0 Friends' Activties （Friends travel） 1: my activties (my travel) ,2:all activties
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 20);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string sortByRating = RequestHelper.GetVal("sortByRating");
            int categoryId = RequestHelper.GetValInt("categoryId", 1);//dbo.AttributeData.ParentId=@categoryId: -1:Countries、-2:Cities、3:Hotels、4:Lifestyle、5:Attractions
            IList<FeedList> feedList = new List<FeedList>();
            //travel
            feedList = _supplierService.FeedDateTravelList(userId, year, month, categoryId, page, pageSize, filterLetter, sortByRating);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            int itemTotal = feedList.Count;
            string ShowImg;
            string showNick;
            int friendCount;
            string strcount;
            int selfId;
            string uncheckbox_recommended;
            string uncheckbox_WantGoBack;
            string uncheckbox_BucketList;
            foreach (var item in feedList)
            {
                ShowImg = "";
                showNick = "";
                friendCount = 0;
                strcount = "";
                selfId = 0;
                uncheckbox_recommended = "";
                uncheckbox_WantGoBack = "";
                uncheckbox_BucketList = "";

                sb.Append("<li class=\"feed\">");
                sb.Append("  <span class=\"operating\">");
                //评分
                sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("      <span class=\"FeedRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\"></span> ");
                sb.Append("    </span>");
                //ibt I've been there
                int ibtCount = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IbtCount;
                sb.Append("    <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',function(){ibt.PvPopLoad();},'POst visit');\"  class=\"btnBlueH45\" >");
                if (item.IbtCount < 1)
                    sb.Append("     <span class=\"bg\">I've been there</span>");
                else
                    sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span><span id=\"feedIBTCount_" + item.NewsFeedId + "\" data-ibtcount=\"" + ibtCount + "\"> " + item.IbtCount.ToString() + "</span></span>");
                sb.Append("    </a>");
                //end:operating
                sb.Append("</span>	");
                if (item.SupplyId > 0)
                {
                    ShowImg = PageHelper.GetSupplyPhoto(item.SupplyId, item.ExShowImg, Identifier.SuppilyPhotoSize._xtn);
                }
                else
                {
                    ShowImg = item.ExShowImg;
                }
                //图片
                sb.Append("<a class=\"userPic\" href=\"/Profile/" + item.UserId.ToString() + "\"><img alt=\"\" src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\" width=\"50\" height=\"50\"></a>");

                sb.Append("<dl class=\"info\">");

                //标题（城市/国家）
                if (item.IBTType == (int)Identifier.IBTType.Country)
                {
                    sb.Append("  <dt><a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted visit to <a href=\"#\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                }
                else if (item.IBTType == (int)Identifier.IBTType.City)
                {
                    sb.Append("  <dt><a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted visit to <a href=\"#\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                }
                else
                {
                    sb.Append("  <dt><a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted visit to <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                }
                //RWB
                sb.Append("  <div class=\"choose\">");
                sb.Append("     <span class=\"checkBg\"><a href=\"" + item.ExTitleUrl + "\"><img src=\"" + ShowImg + "\" class=\"mR5\" alt=\"\"  width=\"36\"/></a>");
                uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5 cursordefault\" onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"2\" data-readonly=\"true\"  data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                sb.Append("          </span>");
                sb.Append("<span class=\"line\"></span>");
                uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5 cursordefault\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"3\"  data-readonly=\"true\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                sb.Append("          </span>");
                sb.Append("<span class=\"line\"></span>");
                uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5 cursordefault\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"4\"  data-readonly=\"true\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                sb.Append("          </span>");
                sb.Append("      </span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                //Elson(showNick) and 9 Friends has Been There 
                showNick = type == 1 ? "You " : item.FirstName;
                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;  //
                if (friendCount == 0 && selfId == UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                }
                else if (friendCount == 0 && selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                }
                else if (friendCount > 0 && selfId != UserId)
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }
                else if (showNick == item.FirstName && friendCount == 1)
                {
                    strcount = "You";
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and " + strcount + " has Been There</span>");
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                }
                sb.Append("  </div>");
                sb.Append("  </dt>");
                sb.Append("  <dd>");
                //编辑Edit和删除deleted (判断是否为admin)
                if (item.RoleId == 1)
                {
                    sb.Append("    <div class=\"relative\">");
                    sb.Append("    <div class=\"editBox select\">");
                    sb.Append("    <span class=\"arrow\">&nbsp;</span>");
                    sb.Append("    <div class=\"list\">");
                    sb.Append("      <ul>");
                    sb.Append("        <li onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "?newsfeedId=" + item.NewsFeedId.ToString() + "','Get','',function(){$('#handleType').val(1);ibt.PvPopLoad();},'Edit POst visit');\">Edit</li>");
                    int IBTId = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IBTId;
                    sb.Append("        <li onclick=\"jTravelFeed.DelPVNewsfeed(" + item.NewsFeedId + "," + item.NewsFeedId + "," + IBTId + ");\">Delete</li>");
                    sb.Append("      </ul>");
                    sb.Append("    </div>");
                    sb.Append("    </div>");
                    sb.Append("    </div>");
                }
                //Post visit 上传的图片
                sb.Append(this.getFeedPostVisitPhotoList(item.NewsFeedId, item.PostVistAttachmentList));
                //------------group-------------------
                sb.Append("  <p>");
                sb.Append(string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal);
                sb.Append("  </p>");
                sb.Append("<div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                //comments(数量)
                int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                sb.Append("     <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");
                ////评论数
                //sb.Append("    <span class=\"commentsTxt\">");
                //sb.Append("        <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\" class=\"mR5\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + item.CommentCount.ToString() + "\" >Comments (" + item.CommentCount.ToString() + ")</div></a>");//|<a onclick=\"ibt.Share(" + item.IBTType + "," + item.ExObjId + ")\" href=\"javascript:void(0);\" class=\"mL5\">Share</a>
                //sb.Append("    </span>");
                ////replyBox---comments
                //sb.Append("<div class=\"replyBox\" style=\"display: none;\"  id=\"replyBox_" + item.NewsFeedId.ToString() + "\">");
                //sb.Append("   <ul id=\"replyList_" + item.NewsFeedId.ToString() + "\">");
                //sb.Append("   </ul>");
                ////replyBox
                //sb.Append("   <ul>");
                //sb.Append("     <li>");
                //sb.Append("     <textarea class=\"autogrow-short\" id=\"commnetTxt_" + item.NewsFeedId.ToString() + "\">Write a message...</textarea>");
                //sb.Append("     <span class=\"commnet_btnSave\"><input onclick=\"feedComments.SaveComment(" + item.NewsFeedId.ToString() + ");\" type=\"button\" value=\"Comment\" /></span>");
                //sb.Append("     </li>");
                //sb.Append("   </ul>");
                //sb.Append("</div>");
                //history
                int historycount = GetHistoryCount(2, item.IBTType, item.ExObjId, item.UserId);
                if (historycount > 0)
                {
                    sb.Append("   <div class=\"history\">");
                    sb.Append("      <span class=\"sliderD sliderU \"  onclick=\"feedHistory.ShowFeedHistory('2','" + item.NewsFeedId.ToString() + "','" + item.IBTType.ToString() + "','" + item.ExObjId.ToString() + "','" + item.UserId.ToString() + "');\"> view history  </span>");
                    sb.Append("        <div class=\"historyList\" id=\"feedHistoryList_" + item.NewsFeedId.ToString() + "\">");
                    sb.Append("        </div>");
                    sb.Append("   </div>");
                }
                sb.Append("   </dd>");
                sb.Append("   </dl>");
                sb.Append("</li>");


            }
            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, sb.ToString()), JsonRequestBehavior.AllowGet);

        }
        #endregion

        #endregion

        #region My Lists
        public ActionResult MyList()
        {
            //todo:MyList 还差Filter

            ViewBag.MapsItem = _mapsService.FirstMyListItem(UserId);
            return View();
        }

        /// <summary>
        /// MyList data
        /// </summary>
        /// <returns></returns>
        public JsonResult MyListAjx()
        {
            string listType = RequestHelper.GetVal("listType");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");

            string strListType = "Recommended"; //Recommended,WanttoGo,BucketList
            if (string.Equals(listType, "WanttoGo", StringComparison.OrdinalIgnoreCase))
                strListType = "WanttoGo";
            else if (string.Equals(listType, "BucketList", StringComparison.OrdinalIgnoreCase))
                strListType = "BucketList";

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = MyListData(strListType, page, pageSize, filterLetter, filterKeyword, filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回MyList的数据
        /// </summary>
        /// <param name="listType">Recommended</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private string MyListData(string listType, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _supplierService.GetMyList(UserId, listType, page, pageSize, filterLetter, filterKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            int friendCount;
            string strcount = "";
            string r_checkbox = "";
            string w_checkbox = "";
            string b_checkbox = "";
            string addr_city = "";
            string ShowImg = "";
            foreach (var item in list)
            {
                sb.Append(" <li>");
                sb.Append(" <span class=\"operating\">");
                sb.Append("     <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                sb.Append("         <span class=\"IBTRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-objId=\"" + item.ObjId.ToString() + "\"></span> ");
                sb.Append("     </span>");
                //todo:这里还需要显示+1，+N的去过的次数
                sb.Append("    <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ObjId.ToString() + "','Get','',function(){ibt.PvPopLoad();},'POst visit');\"  class=\"btnBlueH45\" >");
                if (item.IbtCount < 1)
                    sb.Append("     <span class=\"bg\">I've been there</span>");
                else
                    sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span>" + item.IbtCount.ToString() + "</span>");
                sb.Append("    </a>");

                //sb.Append("     <a onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ObjId.ToString() + "','Get','',function(){ibt.PvPopLoad();},'POst visit');\" class=\"btnBlueH45\" href=\"javascript:void(0);\"><span class=\"bg\">I've been there</span></a>");
                sb.Append(" </span>	");
                if (item.SupplyId > 0)
                {
                    ShowImg = PageHelper.GetSupplyPhoto(item.SupplyId, item.ShowImgSrc, Identifier.SuppilyPhotoSize._xtn);
                }
                else
                {
                    ShowImg = item.ShowImgSrc;
                }
                sb.Append(" <a href=\"" + item.TitleUrl + "\" class=\"img\"><img src=\"" + ShowImg + "\" alt=\"" + item.Title + "\" width=\"50\" height=\"50\"></a>");
                sb.Append(" <dl class=\"info\">");
                addr_city = string.IsNullOrWhiteSpace(item.CityName) ? "" : "," + item.CityName;
                //标题（城市/国家）
                if (item.IBTType == (int)Identifier.IBTType.Country)
                {
                    sb.Append("  <dt><a href=\"" + item.TitleUrl + "\" class=\"f15\">" + item.Title + "</a>");
                }
                else if (item.IBTType == (int)Identifier.IBTType.City)
                {
                    sb.Append("  <dt><a href=\"" + item.TitleUrl + "\" class=\"f15\">" + item.Title + "</a> <span class=\"f11  \">" + item.CountryName + "</span>");
                }
                else
                {
                    sb.Append("  <dt><a href=\"" + item.TitleUrl + "\" class=\"f15\">" + item.Title + "</a> <span class=\"f11  \">" + item.CityName + ", " + item.CountryName + "</span>");
                }
                //sb.Append("     <dt><a class=\"f15\" href=\"" + item.TitleUrl + "\">" + item.Title + "</a> <span class=\"f11  \">" + addr_city + item.CountryName + "</span>");
                //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                if (item.IbtCount > 0)
                {
                    if (friendCount == 0)
                    {
                        sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                    }
                    else
                    {
                        sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                    }
                }
                else
                {
                    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                }

                //sb.Append("         <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"#\">" + item.FriendCount.ToString() + " Friends</a> has Been There</span>");
                sb.Append("     </dt>");
                sb.Append("     <dd>");
                sb.Append("     <div class=\"choose\">");
                r_checkbox = item.Recommended == IMeet.IBT.Common.Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("         <span class=\"checkboxMTxt " + r_checkbox + " \"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"2\"  data-ibtType=\"" + item.IBTType + "\" data-val=\"" + item.Recommended + "\" data-objId=\"" + item.ObjId.ToString() + "\" data-readonly=\"true\">");
                sb.Append("         <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                sb.Append("         </span>");
                w_checkbox = item.WanttoGo == IMeet.IBT.Common.Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("         <span class=\"checkboxMTxt " + w_checkbox + "\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"3\"  data-ibtType=\"" + item.IBTType + "\" data-val=\"" + item.WanttoGo + "\" data-objId=\"" + item.ObjId.ToString() + "\" data-readonly=\"true\">");
                sb.Append("         <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                sb.Append("         </span>");
                b_checkbox = item.BucketList == IMeet.IBT.Common.Utils.IntToByte(1) ? "uncheckbox" : "";
                sb.Append("         <span class=\"checkboxMTxt " + b_checkbox + "\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"4\"  data-ibtType=\"" + item.IBTType + "\" data-val=\"" + item.BucketList + "\" data-objId=\"" + item.ObjId.ToString() + "\" data-readonly=\"true\">");
                sb.Append("         <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                sb.Append("         </span>");
                sb.Append("     </div>");
                sb.Append("     </dd>");
                sb.Append(" </dl>");
                sb.Append(" </li>");
            }

            return sb.ToString();
        }
        #endregion

        #region MyTravel
        public ActionResult MyTravel(int userId, int categoryId)
        {
            int IsShowMyProfile = 0;
            if (userId == UserId)
            {
                IsShowMyProfile = 1;
            }
            else
            {
                if (userId < 1)
                {
                    userId = UserId;
                    IsShowMyProfile = 1;
                }
            }
            string categoryName = "Hotels";
            switch (categoryId)
            {
                case 3:
                    categoryName = "Hotels";
                    break;
                case 4:
                    categoryName = "Lifestyle";
                    break;
                case 5:
                    categoryName = "Attractions";
                    break;
                default:
                    categoryName = "Hotels";
                    break;
            }

            ViewBag.IsShowMyProfile = IsShowMyProfile;
            ViewBag.UserId = userId;
            ViewBag.CategoryId = categoryId;
            ViewBag.CategoryName = categoryName;

            return View();
        }
        #endregion

        /*20130204裕冲添加*/
        #region MyCountries
        public ActionResult MyCountries(int userId)
        {
            //friend
            int IsShowMyProfile = 0;
            if (userId == UserId)
            {
                //my
                IsShowMyProfile = 1;
            }
            else
            {
                if (userId < 1)
                {
                    userId = UserId;
                    IsShowMyProfile = 1;
                }
            }
            string categoryName = "Countries";

            ViewBag.IsShowMyProfile = IsShowMyProfile;
            ViewBag.UserId = userId;
            ViewBag.CategoryName = categoryName;
            ViewBag.CategoryId = -1;

            return View();
        }
        #endregion

        #region MyCities
        public ActionResult MyCities(int userId)
        {
            int IsShowMyProfile = 0;
            if (userId == UserId)
            {
                IsShowMyProfile = 1;
            }
            else
            {
                if (userId < 1)
                {
                    userId = UserId;
                    IsShowMyProfile = 1;
                }
            }
            string categoryName = "Cities";

            ViewBag.IsShowMyProfile = IsShowMyProfile;
            ViewBag.UserId = userId;
            ViewBag.CategoryName = categoryName;
            ViewBag.CategoryId = -2;

            return View();
        }
        #endregion
        /*添加结束*/

        #region Feed comments list
        /// <summary>
        /// feed评论的列表
        /// </summary>
        /// <returns></returns>
        public JsonResult FeedCommentList()
        {
            int feedId = RequestHelper.GetValInt("feedId");
            int pageIndex = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 3);

            var list = _newsFeedService.GetFeedCommentList(feedId, pageIndex, pageSize);
            int total = _ibtServices.GetNewsFeedInfo(feedId).CommentCount;

            PagedList<FeedcommentsEx> pageList = new PagedList<FeedcommentsEx>(pageIndex, pageSize, total, list);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //paging
            sb.Append("<span id=\"supply_common_paging_" + feedId + "\" class='paging right show'>");
            if (total > 0)
            {
                if (pageIndex - 1 > 0)
                    sb.Append("<a href='javascript:void(0);' class='prev' title='Prev' onclick=\"feedComments.ShowMessageBoxPage(" + feedId.ToString() + "," + (pageIndex - 1).ToString() + ");\"></a>");
                if (pageIndex - 2 > 0)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"more\" title=\"More...\" onclick=\"feedComments.ShowMessageBoxPage(" + feedId.ToString() + "," + (pageIndex - 2).ToString() + ");\"></a>");
                if (pageIndex - 1 > 0)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"bg\" onclick=\"feedComments.ShowMessageBoxPage(" + feedId.ToString() + "," + (pageIndex - 1).ToString() + ");\">" + (pageIndex - 1).ToString() + "</a>");
                sb.Append("<a href=\"javascript:void(0);\" class=\"bg bgActive\">" + pageIndex + "</a>");
                if (pageIndex + 1 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"bg\" onclick=\"feedComments.ShowMessageBoxPage(" + feedId.ToString() + "," + (pageIndex + 1).ToString() + ");\">" + (pageIndex + 1).ToString() + "</a>");
                if (pageIndex + 2 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"more\" title=\"More...\" onclick=\"feedComments.ShowMessageBoxPage(" + feedId.ToString() + "," + (pageIndex + 2).ToString() + ");\"></a>");
                if (pageIndex + 1 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"next\" title=\"Next\" onclick=\"feedComments.ShowMessageBoxPage(" + feedId.ToString() + "," + (pageIndex + 1).ToString() + ");\"></a>");
            }
            sb.Append("</span>");
            //Comments count
            sb.Append("<a href=\"javascript:void(0);\" id=\"supplycommon_comments_" + feedId.ToString() + "\" onclick=\"feedComments.ShowMessageBox(" + feedId.ToString() + ");\" > Comments (" + total.ToString() + ")  </a>");
            //replyBox
            sb.Append("<div id='replyBox_" + feedId.ToString() + "' class='replyBox show'>");
            sb.Append("<ul id=\"commentList_" + feedId + "\">");
            list.ForEach(comment =>
            {
                string createDate = comment.CreateDate.ToString("MMM dd, yyyy");
                sb.Append("<li>");
                sb.Append("     <span class=\"time italic\">wrote a comment . " + createDate + "</span>");
                sb.Append("     <a href=\"/Profile/" + comment.UserId + "\" class=\"userPic\"><img src=\"" + PageHelper.GetUserAvatar(comment.UserId, (int)Identifier.AccountSettingAvatarSize._xxlat) + "\" alt=\"\" /></a>");
                sb.Append("     <div class=\"replyMain\">");
                sb.Append("         <a href=\"/Profile/" + comment.UserId + "\" class=\"name\">" + comment.FirstName + "</a>");
                sb.Append("         <p>" + comment.Comments + "</p>");
                sb.Append("     </div>");
                sb.Append("</li>");
            });
            sb.Append("</ul>");
            sb.Append("<ul>");
            sb.Append("    <li><textarea id=\"commnetTxt_" + feedId + "\">Write a message...</textarea><input type=\"button\" onclick=\"feedComments.SaveComment(" + feedId + ")\" value=\"Comment\"/></li>");
            sb.Append("</ul>");
            sb.Append("</div>");
            return Json(new BoolMessageItem(total, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 给feed发评论
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult FeedCommentSave()
        {
            int feedId = RequestHelper.GetValInt("feedId");
            string commentTxt = RequestHelper.GetUrlDecodeVal("commentTxt");
            commentTxt = HttpContext.Server.HtmlDecode(commentTxt);
            //commentTxt = commentTxt.Replace("\n", "<br />");
            int userId = UserId;
            if (userId <= 0)
            {
                return Json(new BoolMessageItem(null, false, "Please login.."), JsonRequestBehavior.AllowGet);
            }

            _newsFeedService.AddFeedComments(feedId, userId, commentTxt);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("                     <li>");
            sb.Append("                         <span class=\"time italic\">wrote a comment . " + DateTime.Now.ToString("dd MMM yyyy") + "</span>");
            sb.Append("    <a class=\"userPic\" href=\"" + _profileService.GetUserAvatar(userId, 36) + "\"><img style=\"width:36px\" alt=\"\" src=\"" + _profileService.GetUserAvatar(userId, 36) + "\"></a>");
            sb.Append("                             <div class=\"replyMain\">");
            sb.Append("                                 <a href=\"#\" class=\"name\">" + _profileService.GetUserProfile(userId).Firstname + "</a>");
            string str = commentTxt.Replace("<", "&lt;").Replace(">", "&gt;");
            str = str.Replace("\n", "<br />");
            sb.Append("                                 <p>" + str + "</p>");
            sb.Append("                             </div>");
            sb.Append("                     </li>");

            return Json(new BoolMessageItem(null, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Supply的评论
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SupplyFeedbacks()
        {
            int supplyId = RequestHelper.GetValInt("supplyId");
            int userId = _profileService.GetUserId();
            string comments = RequestHelper.GetUrlDecodeVal("comments");
            string att_ids = RequestHelper.GetUrlDecodeVal("att_ids");
            if (userId <= 0)
            {
                return Json(new BoolMessageItem(null, false, "Please login.."), JsonRequestBehavior.AllowGet);
            }

            var bm = _supplierService.SupplyFeedbacks(supplyId, userId, comments);

            if (!bm.Success)
            {
                return Json(new BoolMessageItem(null, false, "Data is empty,please try again!"), JsonRequestBehavior.AllowGet);
            }

            int newsFeedId = Convert.ToInt32(bm.Item);
            //关联图片
            if (!string.IsNullOrEmpty(att_ids))
            {
                string[] ids = att_ids.Split(',');
                foreach (var id in ids)
                {
                    _ibtServices.SetAttachmentObjIdByAttId(id, newsFeedId);
                }
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            var profile = _profileService.GetUserProfile(userId);
            string firstName = profile != null ? profile.Firstname : "Anonymous";

            Supply _supply = _supplierService.GetSupplyDetail(supplyId);
            //            NewsFeed nf = _supplierService.getNewsFeed(1, 1, 0, 0, 0, supplyId).FirstOrDefault();
            if (userId > 0)
            {
                sb.Append("<li><a class=\"userPic\" href=\"/Profile/" + userId.ToString() + "\"><img src=\"" + _profileService.GetUserAvatar(userId, 36) + "\" alt=\"" + profile.Firstname + "\" /></a>");
            }
            else
            {
                sb.Append("<li><span class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(userId, 36) + "\" alt=\"" + profile.Firstname + "\"></span>");
            }
            Country country = _supplierService.GetCountryInfo(_supply.CountryId);
            if (country == null)
            {
                country = new Country();
            }
            City city = _supplierService.GetCityInfo(_supply.CityId);
            if (city == null)
            {
                city = new City();
            }
            sb.Append("<dl>");
            //sb.Append("     <dt><a href=\"/Search/Index/" + country.CountryName + "\">" + country.CountryName + "</a><span class=\"time\">" + DateTime.Now.ToString("dd MMM yyyy") + "</span></dt>");
            sb.Append("     <dt><a href=\"/Profile/" + profile.UserId + "\">" + firstName + "</a><span class=\"f15\"> has written a post to <a href=\"/Supplier/v/" + _supply.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(_supply.Title) + "\">" + _supply.Title.ToString() + "</a></span><span class=\"time\">" + DateTime.Now.ToString("dd MMM yyyy") + "</span></dt>");
            sb.Append("         <dd>");
            comments = comments.Replace("<", "&lt;").Replace(">", "&gt;");
            comments = comments.Replace("\n", "<br />");
            sb.Append("             <p>" + comments + "</p>");
            IList<Attachment> attachmentList = _supplierService.GetAttachment(newsFeedId, Identifier.AttachmentType.NewsFeed);
            if (attachmentList.Count > 0)
            {
                sb.Append("             <div class=\"fixbox\">");
                foreach (var att in attachmentList)
                {
                    //sb.Append("                 <img  onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\" src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" />");
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\"><img src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" /></a>");
                }
                sb.Append("             </div>");
            }
            sb.Append("             <div class=\"operating\">");
            int feedcommentsCount = _newsFeedService.GetFeedcommentcount(newsFeedId);
            sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + newsFeedId + ");\"><div id=\"feedcommentsCount_" + newsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");//| <a href=\"#\">Share</a>");

            #region 回复列表以及回复文本框
            sb.Append("                 <div class=\"replyBox\" style=\"display: none;\" id=\"replyBox_" + newsFeedId + "\">");
            sb.Append("                     <ul id=\"replyList_" + newsFeedId + "\">");
            sb.Append("                     </ul>");
            sb.Append("                     <ul>");
            sb.Append("                         <li>");
            sb.Append("                             <textarea id=\"commnetTxt_" + newsFeedId + "\">Write a message...</textarea>");
            sb.Append("                             <input style=\"float: right;\" type=\"button\" value=\"Comment\" onclick=\"feedComments.SaveComment(" + newsFeedId + ")\" />");
            sb.Append("                         </li>");
            sb.Append("                     </ul>");
            sb.Append("                 </div>");
            #endregion
            sb.Append("             </div>");
            sb.Append("      </dd>");
            sb.Append(" </dl>");
            sb.Append("</li>");

            return Json(new BoolMessageItem(null, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 评论的列表
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public JsonResult FeedbackLists()
        {
            int supplyId = RequestHelper.GetValInt("supplyId");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);

            page = page < 1 ? 1 : page;
            pageSize = pageSize < 1 ? 10 : pageSize;


            IList<FeedList> feedList = new List<DAL.FeedList>();
            //feedList = _supplierService.FeedActivtieListsBySupplyId(supplyId, page, pageSize);
            Supply supply = _supplierService.GetSupplyDetail(supplyId);
            feedList = _ibtServices.GetFeedList(supply.CountryId, supply.CityId, supplyId, (int)Identifier.IBTType.Supply, DateTime.Now, page, pageSize);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in feedList)
            {
                Supply _supply = _supplierService.GetSupplyDetail(item.SupplyId);
                Profile profile = _profileService.GetUserProfile(item.UserId);
                if (item.ActiveType == (int)Identifier.FeedActiveType.AddTravel || item.ActiveType == (int)Identifier.FeedActiveType.PostVisit || item.ActiveType == (int)Identifier.FeedActiveType.Rating || item.ActiveType == (int)Identifier.FeedActiveType.Recommended || item.ActiveType == (int)Identifier.FeedActiveType.WantGoBack || item.ActiveType == (int)Identifier.FeedActiveType.BucketList || item.ActiveType == (int)Identifier.FeedActiveType.IBT)
                {
                    #region postview
                    sb.Append("<li class=\"feed\">");
                    sb.Append("  <span class=\"operating\">");
                    //评分
                    sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                    sb.Append("      <span class=\"FeedRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\"></span> ");
                    sb.Append("    </span>");
                    //ibt I've been there
                    int ibtCount = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IbtCount;
                    sb.Append("    <a href=\"" + PageHelper.PostVisitNUrl((int)Identifier.IBTType.Supply, item.SupplyId) + "\" class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                    {
                        if (_supply.ServiceType != (int)Identifier.ServiceType.ServiceProvider)
                            sb.Append("     <span class=\"bg\">I've been there</span>");
                        else
                            sb.Append("     <span class=\"bg\">I've used the service</span>");
                    }
                    else
                    {
                        if (_supply.ServiceType != (int)Identifier.ServiceType.ServiceProvider)
                            sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've been there</span><span id=\"feedIBTCount_" + item.NewsFeedId + "\" data-ibtcount=\"" + ibtCount + "\"> " + item.IbtCount.ToString() + "</span></span>");
                        else
                            sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've used the service</span><span id=\"feedIBTCount_" + item.NewsFeedId + "\" data-ibtcount=\"" + ibtCount + "\"> " + item.IbtCount.ToString() + "</span></span>");
                    }
                    sb.Append("    </a>");
                    //end:operating
                    sb.Append("</span>	");
                    string ShowImg = item.ExShowImg;
                    if (item.SupplyId > 0)
                    {
                        ShowImg = PageHelper.GetSupplyPhoto(item.SupplyId, item.ExShowImg, Identifier.SuppilyPhotoSize._xtn);
                    }
                    //图片
                    sb.Append("<a class=\"userPic\" href=\"/Profile/" + item.UserId.ToString() + "\"><img alt=\"\" src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\" width=\"50\" height=\"50\"></a>");

                    sb.Append("<dl class=\"info\">");
                    sb.Append("<dt>");
                    #region //标题（城市/国家）
                    string ExTitle = _supply.Title;
                    string ExTitleUrl = "/Supplier/v/" + _supply.SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(_supply.Title);

                    if (item.ActiveType == (int)Identifier.FeedActiveType.Rating)
                    {
                        sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has rated <a href=\"" + ExTitleUrl + "\" class=\"f15\">" + ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                    }
                    else if (item.ActiveType == (int)Identifier.FeedActiveType.Recommended)
                    {
                        sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> recommended <a href=\"" + ExTitleUrl + "\" class=\"f15\">" + ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                    }
                    else if (item.ActiveType == (int)Identifier.FeedActiveType.WantGoBack)
                    {
                        sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> want to go / go back to <a href=\"" + ExTitleUrl + "\" class=\"f15\">" + ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                    }
                    else if (item.ActiveType == (int)Identifier.FeedActiveType.BucketList)
                    {
                        sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has added <a href=\"" + ExTitleUrl + "\" class=\"f15\">" + ExTitle + "</a> to " + item.ExGender + " bucket list </span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                    }
                    else
                    {
                        sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + ExTitleUrl + "\" class=\"f15\">" + ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                    }

                    #endregion
                    //RWB
                    sb.Append("  <div class=\"choose\">");
                    sb.Append("     <span class=\"checkBg\"><a href=\"" + item.ExTitleUrl + "\"><img src=\"" + ShowImg + "\" class=\"mR5\" alt=\"\"  width=\"36\"/></a>");
                    string uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5 cursordefault\" onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"2\" data-readonly=\"true\"  data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                    sb.Append("<span class=\"line\"></span>");
                    if (_supply.ServiceType != (int)Identifier.ServiceType.ServiceProvider)
                    {//serviceprovider 不显示
                        string uncheckbox_WantGoBack = item.WanttoGo == Utils.IntToByte(1) ? "uncheckbox" : "";
                        sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_WantGoBack + " mR5 cursordefault\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"3\"  data-readonly=\"true\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.WanttoGo.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                        sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Want to Go/Go Back</span></span>");
                        sb.Append("          </span>");
                        sb.Append("<span class=\"line\"></span>");
                        string uncheckbox_BucketList = item.BucketList == Utils.IntToByte(1) ? "uncheckbox" : "";
                        sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_BucketList + " mR5 cursordefault\"  onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"4\"  data-readonly=\"true\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.BucketList.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                        sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Bucket List</span></span>");
                        sb.Append("          </span>");
                        sb.Append("      </span>");
                    }
                    //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                    //Elson(showNick) and 9 Friends has Been There 
                    int friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                    string strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                    int selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;  //
                    if (_supply.ServiceType != (int)Identifier.ServiceType.ServiceProvider)
                    {
                        if (friendCount == 0)
                        {
                            sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: " + friendCount + "</span>");
                        }
                        else {
                            sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">Friends That Have Been Here: <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + friendCount + "</a></span>");
                        }
                        //if (friendCount == 0 && selfId == UserId)
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                        //}
                        //else if (friendCount == 0 && selfId != UserId)
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                        //}
                        //else if (friendCount > 0 && selfId != UserId)
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                        //}
                        //else if (friendCount == 1)
                        //{
                        //    strcount = "You";
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + item.FirstName + " and " + strcount + " has Been There</span>");
                        //}
                        //else
                        //{
                        //    sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + item.FirstName + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                        //}
                    }
                    sb.Append("  </div>");
                    sb.Append("  </dt>");
                    sb.Append("  <dd>");
                    //编辑Edit和删除deleted 
                    if ((item.ActiveType == (int)Identifier.FeedActiveType.IBT || item.ActiveType == (int)Identifier.FeedActiveType.AddTravel || item.ActiveType == (int)Identifier.FeedActiveType.PostVisit) && item.UserId == UserId)
                    {
                        sb.Append("    <div class=\"relative\">");
                        sb.Append("    <div class=\"editBox select\">");
                        sb.Append("    <span class=\"arrow\">&nbsp;</span>");
                        sb.Append("    <div class=\"list\">");
                        sb.Append("      <ul>");
                        sb.Append("        <li onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "?newsfeedId=" + item.NewsFeedId.ToString() + "','Get','',function(){$('#handleType').val(1);ibt.PvPopLoad();},'Edit POst visit');\">Edit</li>");
                        int IBTId = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IBTId;
                        sb.Append("        <li onclick=\"jTravelFeed.DelPVNewsfeed(" + item.NewsFeedId + "," + item.NewsFeedId + "," + IBTId + ");\">Delete</li>");
                        sb.Append("      </ul>");
                        sb.Append("    </div>");
                        sb.Append("    </div>");
                        sb.Append("    </div>");
                    }
                    ////Post visit 上传的图片
                    //sb.Append(this.getFeedPostVisitPhotoList(item.NewsFeedId, item.PostVistAttachmentList));
                    #region 评论的图片列表(user: country / city / supplier listing post)
                    IList<Attachment> attachmentList = _supplierService.GetAttachment(item.NewsFeedId, Identifier.AttachmentType.NewsFeed);
                    if (attachmentList.Count > 0)
                    {
                        sb.Append("  <div class=\"fixbox photo\">");
                        foreach (var att in attachmentList)
                        {
                            sb.Append("           <a href=\"javascript:void(0);\" onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\"><img class=\"mR10\" src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" /></a>");
                        }
                        sb.Append("  </div>");
                    }
                    #endregion
                    //------------group-------------------
                    sb.Append("  <p>");
                    sb.Append(string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal);
                    sb.Append("  </p>");
                    //评论数
                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");

                    #region 回复列表以及回复文本框
                    //sb.Append("                 <div class=\"replyBox\" style=\"display: none;\" id=\"replyBox_" + item.NewsFeedId + "\">");
                    //sb.Append("                     <ul id=\"replyList_" + item.NewsFeedId + "\">");
                    //sb.Append("                     </ul>");
                    //sb.Append("                     <ul>");
                    //sb.Append("                         <li>");
                    //sb.Append("                             <textarea id=\"commnetTxt_" + item.NewsFeedId + "\">Write a message...</textarea>");
                    //sb.Append("                             <input style=\"float: right;\" type=\"button\" value=\"Comment\" onclick=\"feedComments.SaveComment(" + item.NewsFeedId + ")\" />");
                    //sb.Append("                         </li>");
                    //sb.Append("                     </ul>");
                    //sb.Append("                 </div>");
                    #endregion

                    sb.Append("             </div>");

                    //sb.Append("    <span id=\"operating_" + item.NewsFeedId + "\" class=\"commentsTxt\">");
                    //sb.Append("        <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\" class=\"mR5\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + item.CommentCount.ToString() + "\" >Comments (" + item.CommentCount.ToString() + ")</div></a>");
                    //sb.Append("    </span>");
                    ////replyBox---comments
                    //sb.Append("<div class=\"replyBox\" style=\"display: none;\"  id=\"replyBox_" + item.NewsFeedId.ToString() + "\">");
                    //sb.Append("   <ul id=\"replyList_" + item.NewsFeedId.ToString() + "\">");
                    //sb.Append("   </ul>");
                    ////replyBox
                    //sb.Append("   <ul>");
                    //sb.Append("     <li>");
                    //sb.Append("     <textarea class=\"autogrow-short\" id=\"commnetTxt_" + item.NewsFeedId.ToString() + "\">Write a message...</textarea>");
                    //sb.Append("     <span class=\"commnet_btnSave\"><input onclick=\"feedComments.SaveComment(" + item.NewsFeedId.ToString() + ");\" type=\"button\" value=\"Comment\" /></span>");
                    //sb.Append("     </li>");
                    //sb.Append("   </ul>");
                    //sb.Append("</div>");
                    //history
                    if (item.ActiveType == (int)Identifier.FeedActiveType.IBT || item.ActiveType == (int)Identifier.FeedActiveType.AddTravel || item.ActiveType == (int)Identifier.FeedActiveType.PostVisit)
                    {
                        //int historycount = GetHistoryCount(2, item.IBTType, item.ExObjId, item.UserId);
                        if (item.IbtCount > 1)
                        {
                            sb.Append("   <div class=\"history\">");
                            sb.Append("      <span class=\"sliderD sliderU \" id=\"feedHistoryTxt_" + item.NewsFeedId.ToString() + "\"  onclick=\"feedHistory.ShowFeedHistory('" + 2.ToString() + "','" + item.NewsFeedId.ToString() + "','" + item.IBTType.ToString() + "','" + item.ExObjId.ToString() + "','" + item.UserId.ToString() + "');\"> view history  </span>");
                            sb.Append("        <div class=\"historyList\" id=\"feedHistoryList_" + item.NewsFeedId.ToString() + "\">");
                            sb.Append("        </div>");
                            sb.Append("   </div>");
                        }
                    }
                    sb.Append("   </dd>");
                    sb.Append("   </dl>");
                    sb.Append("</li>");
                    #endregion
                }
                else
                {
                    #region commit
                    if (item.UserId > 0)
                    {
                        sb.Append("<li><a class=\"userPic\" href=\"/Profile/" + item.UserId.ToString() + "\"><img src=\"" + _profileService.GetUserAvatar(item.UserId, 50) + "\" alt=\"" + profile.Firstname + "\" /></a>");
                    }
                    else
                    {
                        sb.Append("<li><span class=\"userPic\"><img src=\"" + _profileService.GetUserAvatar(item.UserId, 50) + "\" alt=\"" + profile.Firstname + "\"></span>");
                    }
                    Country country = _supplierService.GetCountryInfo(_supply.CountryId);
                    if (country == null)
                    {
                        country = new Country();
                    }
                    City city = _supplierService.GetCityInfo(_supply.CityId);
                    if (city == null)
                    {
                        city = new City();
                    }
                    sb.Append("<dl>");

                    //sb.Append("     <dt><a href=\"/Search/Index/" + country.CountryName + "\">" + country.CountryName + "</a><span class=\"time\">" + item.CreateDate.ToString("dd MMM yyyy") + "</span></dt>");
                    sb.Append("     <dt><a href=\"/Profile/" + item.UserId.ToString() + "\">" + item.FirstName + "</a><span class=\"f15\"> has written a post to <a href=\"/Supplier/v/" + _supply.SupplyId + "\">" + _supply.Title + "</a></span><span class=\"time\">" + item.CreateDate.ToString("dd MMM yyyy") + "</span></dt>");

                    sb.Append("         <dd>");
                    string comments = item.ObjVal.Replace("<", "&lt;").Replace(">", "&gt;");
                    comments = comments.Replace("\n", "<br />");
                    sb.Append("             <p>" + comments + "</p>");
                    #region 评论中图片的列表
                    IList<Attachment> attachmentList = _supplierService.GetAttachment(item.NewsFeedId, Identifier.AttachmentType.NewsFeed);
                    if (attachmentList.Count > 0)
                    {
                        sb.Append("             <div class=\"fixbox photo\">");
                        foreach (var att in attachmentList)
                        {
                            sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\"><img src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" /></a>");
                        }
                        sb.Append("             </div>");
                    }
                    #endregion
                    sb.Append("             <div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");//| <a href=\"#\">Share</a>");

                    #region 回复列表以及回复文本框
                    //sb.Append("                 <div class=\"replyBox\" style=\"display: none;\" id=\"replyBox_" + item.NewsFeedId + "\">");
                    //sb.Append("                     <ul id=\"replyList_" + item.NewsFeedId + "\">");
                    //sb.Append("                     </ul>");
                    //sb.Append("                     <ul>");
                    //sb.Append("                         <li>");
                    //sb.Append("                             <textarea id=\"commnetTxt_" + item.NewsFeedId + "\">Write a message...</textarea>");
                    //sb.Append("                             <input style=\"float: right;\" type=\"button\" value=\"Comment\" onclick=\"feedComments.SaveComment(" + item.NewsFeedId + ")\" />");
                    //sb.Append("                         </li>");
                    //sb.Append("                     </ul>");
                    //sb.Append("                 </div>");
                    #endregion

                    sb.Append("             </div>");
                    sb.Append("      </dd>");
                    sb.Append(" </dl>");
                    sb.Append("</li>");
                    #endregion
                }
            }

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", feedList.Count);
            return Json(new BoolMessageItem(dic, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// feed评论的列表-pop
        /// </summary>
        /// <returns></returns>
        public JsonResult FeedCommentListPop()
        {
            int feedId = RequestHelper.GetValInt("feedId");
            int pageIndex = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 3);

            var list = _newsFeedService.GetFeedCommentList(feedId, pageIndex, pageSize);
            int total = _ibtServices.GetNewsFeedInfo(feedId).CommentCount;

            PagedList<FeedcommentsEx> pageList = new PagedList<FeedcommentsEx>(pageIndex, pageSize, total, list);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //paging
            sb.Append("<span id=\"supply_common_paging_pop_" + feedId + "\" class='paging right show'>");
            if (total > 0)
            {
                if (pageIndex - 1 > 0)
                    sb.Append("<a href='javascript:void(0);' class='prev' title='Prev' onclick=\"feedComments.ShowMessageBoxPopPage(" + feedId.ToString() + "," + (pageIndex - 1).ToString() + ");\"></a>");
                if (pageIndex - 2 > 0)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"more\" title=\"More...\" onclick=\"feedComments.ShowMessageBoxPopPage(" + feedId.ToString() + "," + (pageIndex - 2).ToString() + ");\"></a>");
                if (pageIndex - 1 > 0)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"bg\" onclick=\"feedComments.ShowMessageBoxPopPage(" + feedId.ToString() + "," + (pageIndex - 1).ToString() + ");\">" + (pageIndex - 1).ToString() + "</a>");
                sb.Append("<a href=\"javascript:void(0);\" class=\"bg bgActive\">" + pageIndex + "</a>");
                if (pageIndex + 1 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"bg\" onclick=\"feedComments.ShowMessageBoxPopPage(" + feedId.ToString() + "," + (pageIndex + 1).ToString() + ");\">" + (pageIndex + 1).ToString() + "</a>");
                if (pageIndex + 2 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"more\" title=\"More...\" onclick=\"feedComments.ShowMessageBoxPopPage(" + feedId.ToString() + "," + (pageIndex + 2).ToString() + ");\"></a>");
                if (pageIndex + 1 <= pageList.TotalPages)
                    sb.Append("<a href=\"javascript:void(0);\" class=\"next\" title=\"Next\" onclick=\"feedComments.ShowMessageBoxPopPage(" + feedId.ToString() + "," + (pageIndex + 1).ToString() + ");\"></a>");
            }
            sb.Append("</span>");
            //Comments count
            sb.Append("<a href=\"javascript:void(0);\" id=\"supplycommon_comments_" + feedId.ToString() + "\" onclick=\"feedComments.ShowMessageBoxPop(" + feedId.ToString() + ");\" > Comments (" + total.ToString() + ")  </a>");
            //replyBox
            sb.Append("<div id='replyBoxPop_" + feedId.ToString() + "' class='replyBox show'>");
            sb.Append("<ul id=\"commentListPop_" + feedId + "\">");
            list.ForEach(comment =>
            {
                string createDate = comment.CreateDate.ToString("MMM dd, yyyy");
                sb.Append("<li>");
                sb.Append("     <span class=\"time italic\">wrote a comment . " + createDate + "</span>");
                sb.Append("     <a href=\"/Profile/" + comment.UserId + "\" class=\"userPic\"><img src=\"" + PageHelper.GetUserAvatar(comment.UserId, (int)Identifier.AccountSettingAvatarSize._xxlat) + "\" alt=\"\" /></a>");
                sb.Append("     <div class=\"replyMain\">");
                sb.Append("         <a href=\"/Profile/" + comment.UserId + "\" class=\"name\">" + comment.FirstName + "</a>");
                sb.Append("         <p>" + comment.Comments + "</p>");
                sb.Append("     </div>");
                sb.Append("</li>");
            });
            sb.Append("<li><textarea id=\"commnetTxtPop_" + feedId + "\" style=\"width:368px;\">Write a message...</textarea><input type=\"button\" onclick=\"feedComments.SaveCommentPop(" + feedId + ")\" value=\"Comment\"/></li>");
            sb.Append("</ul>");
            sb.Append("</div>");
            return Json(new BoolMessageItem(total, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 给feed发评论-pop
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult FeedCommentSavePop()
        {
            int feedId = RequestHelper.GetValInt("feedId");
            string commentTxt = RequestHelper.GetUrlDecodeVal("commentTxt");
            commentTxt = HttpContext.Server.HtmlDecode(commentTxt);
            //commentTxt = commentTxt.Replace("\n", "<br />");
            int userId = UserId;
            if (userId <= 0)
            {
                return Json(new BoolMessageItem(null, false, "Please login.."), JsonRequestBehavior.AllowGet);
            }

            _newsFeedService.AddFeedComments(feedId, userId, commentTxt);
            return Json(new BoolMessageItem(null, true, ""), JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult Search()
        {
            return View();
        }

        #region Specials Listing
        public ActionResult SpecialsListing()
        {
            return View();
        }
        /// <summary>
        /// Specials data
        /// </summary>
        /// <returns></returns>
        public JsonResult specialsListAjx()  //用户ID(参数)有问题
        {
            string filterSort = RequestHelper.GetVal("filterSort");
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 10);
            string filterKeyword = RequestHelper.GetUrlDecodeVal("filterKeyword");
            string filterField = RequestHelper.GetUrlDecodeVal("filterField");

            pageSize = pageSize > 50 ? 10 : pageSize;
            pageSize = pageSize < 1 ? 10 : pageSize;

            int total = 0;
            int itemTotal = 0;
            var list = SpecialsListData(filterSort, page, pageSize, filterKeyword, filterField, out total, out itemTotal);

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("total", total);
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, list), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 返回SpecialsList的数据
        /// </summary>
        /// <returns></returns>
        private string SpecialsListData(string filterSort, int page, int pageSize, string filterKeyword, string filterField, out int total, out int itemTotal)
        {
            var list = _supplierService.GetSpecialsList(filterSort, page, pageSize, filterKeyword, filterField, out total);
            itemTotal = list.Count;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in list)
            {
                sb.Append(" <li>");
                //sb.Append(" <a href=\"/Supplier/s/" + item.SupplyId + "\" class=\"img\">");
                sb.Append("<a href=\"" + item.FileSrc + "_ori.jpg\" rel=\"lightbox[img_" + item.MediaId.ToString() + "]\" title=\"" + item.Title + "\" class=\"img\">");
                //sb.Append(" <a href=\"javascript:void(0);\" class=\"img\" onclick=\"GetCountOfHBrowse(" + item.MediaId + "," + item.IBTType + "," + item.SupplyId + "," + item.CountryId + "," + item.CityId + ")\">");
                if (item.MediaType == (int)Identifier.MediaTypes.picture)
                {
                    //sb.Append("<a href=\"" + item.FileSrc + "_spec.jpg\" rel=\"lightbox[img_" + item.MediaId.ToString() + "]\" title=\"" + item.Title + "\" class=\"mR5\"><img src=\"" + item.FileSrc + "_sw.jpg\" alt=\"Photos description \" /></a>");
                    sb.Append("      <img src=\"" + (string.IsNullOrEmpty(item.FileSrc) ? "" : item.FileSrc + "_sw.jpg") + "\" alt=\"\" />");
                }
                else
                {
                    sb.Append(item.Code.ToString());
                }
                sb.Append(" </a> ");
                sb.Append(" <div class=\"infoMain\">");
                sb.Append("     <dl>");
                sb.Append("     <dt><a href=\"javascript:void(0);\" onclick=\"GetCountOfHBrowse(" + item.MediaId + "," + item.IBTType + "," + item.SupplyId + "," + item.CountryId + "," + item.CityId + ")\">" + item.Title + "</a> <span class=\"date  f11\">From " + DateTime.Parse(item.StartDate.ToString()).ToString("d MMMM yyyy") + " - " + DateTime.Parse(item.ExpiryDate.ToString()).ToString("d MMMM yyyy") + "</span></dt>");
                if (item.Desp.ToString().Contains("<") || item.Desp.ToString().Contains(">"))
                {
                    sb.Append("<dd class=\"mT5\">" + item.Desp.ToString() + "</dd>");

                }
                else
                {
                    sb.Append(string.IsNullOrEmpty(item.Desp.ToString()) ? "" : "<dd class=\"mT5\">" + IMeet.IBT.Common.StringHelper.Truncate(item.Desp, 150, "...") + "</dd>");

                }
                //sb.Append("     <dd class=\"mT5\">The shape is uber -modern,with a helix-shaper flcor space surrounding an empty core.</dd>");
                sb.Append("     <dd class=\"italic f11 Cd4d4d4\"></dd>");
                sb.Append("     </dl>");
                sb.Append("        <a href=\"javascript:void(0);\" class=\"arrowBlueR mT10\" onclick=\"GetCountOfHBrowse(" + item.MediaId + "," + item.IBTType + "," + item.SupplyId + "," + item.CountryId + "," + item.CityId + ")\">More</a>");
                sb.Append(" </div>");
                sb.Append("</li>");
            }

            return sb.ToString();
        }
        #endregion

        #region Services List
        public ActionResult Services(int userId, int categoryId)
        {
            int IsShowMyProfile = 0;
            if (userId == UserId)
            {
                IsShowMyProfile = 1;
            }
            else
            {
                if (userId < 1)
                {
                    userId = UserId;
                    IsShowMyProfile = 1;
                }
            }


            ViewBag.IsShowMyProfile = IsShowMyProfile;
            ViewBag.UserId = userId;
            ViewBag.CategoryId = categoryId;

            return View();
        }
        ///// <summary>
        ///// profile & my travel feed list
        ///// </summary>
        ///// <returns></returns>
        public JsonResult ServicesList()
        {
            int userId = RequestHelper.GetValInt("userId", UserId);
            int tabId = RequestHelper.GetValInt("tabId", 3); //1 profile,2 mytravel 3 Services
            int type = RequestHelper.GetValInt("type", 2); //0 Friends' Activties （Friends travel） 1: my activties (my travel) ,2:all activties
            int page = RequestHelper.GetValInt("page", 1);
            int pageSize = RequestHelper.GetValInt("pageSize", 20);
            string filterLetter = RequestHelper.GetVal("filterLetter");
            string sortByRating = RequestHelper.GetVal("sortByRating");
            int categoryId = RequestHelper.GetValInt("categoryId", 1);//dbo.AttributeData.ParentId=@categoryId: -1:Countries、-2:Cities、3:Hotels、4:Lifestyle、5:Attractions
            IList<FeedList> feedList = new List<FeedList>();

            if (tabId == 3)
            {
                //travel
                feedList = _supplierService.ServicesSPList(userId, categoryId, page, pageSize, filterLetter, sortByRating);
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            int addTravel = (int)Identifier.FeedActiveType.AddTravel;
            int postVisit = (int)Identifier.FeedActiveType.PostVisit;
            int rating = (int)Identifier.FeedActiveType.Rating;
            int recommended = (int)Identifier.FeedActiveType.Recommended;
            int ibt = (int)Identifier.FeedActiveType.IBT;

            int itemTotal = feedList.Count;
            string ShowImg;
            string showNick;
            int friendCount;
            string strcount;
            int selfId;
            string uncheckbox_recommended;

            foreach (var item in feedList)
            {
                ShowImg = "";
                showNick = "";
                friendCount = 0;
                strcount = "";
                selfId = 0;
                uncheckbox_recommended = "";

                #region post visit
                if (item.ActiveType == addTravel || item.ActiveType == postVisit || item.ActiveType == rating || item.ActiveType == recommended || item.ActiveType == ibt)
                {
                    sb.Append("<li class=\"feed\">");
                    sb.Append("  <span class=\"operating\">");
                    //评分
                    sb.Append("    <span class=\"rating\"><span class=\"f10  mR5\"> RATING</span>");
                    sb.Append("      <span class=\"FeedRating\" data-rating=\"" + item.Rating.ToString() + "\" data-ibtType=\"" + item.IBTType.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\"></span> ");
                    sb.Append("    </span>");
                    //ibt I've used the service
                    int ibtCount = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IbtCount;
                    sb.Append("    <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',function(){ibt.PvPopLoad();},'POst visit');\"  class=\"btnBlueH45\" >");
                    if (item.IbtCount < 1)
                    {
                        sb.Append("     <span class=\"bg\">I've used the service</span>");
                    }
                    else
                    {
                        sb.Append("<span class=\"btnBlueNumH45\"><span class=\"btnBlueAddH45\">I've used the service</span><span id=\"feedIBTCount_" + item.NewsFeedId + "\" data-ibtcount=\"" + ibtCount + "\"> " + item.IbtCount.ToString() + "</span></span>");
                    }
                    sb.Append("    </a>");
                    //end:operating
                    sb.Append("</span>	");
                    if (item.SupplyId > 0)
                    {
                        ShowImg = PageHelper.GetSupplyPhoto(item.SupplyId, item.ExShowImg, Identifier.SuppilyPhotoSize._xtn);
                    }
                    else
                    {
                        ShowImg = item.ExShowImg;
                    }
                    //图片
                    sb.Append("<a class=\"userPic\" href=\"/Profile/" + item.UserId.ToString() + "\"><img alt=\"\" src=\"" + _profileService.GetUserAvatar(item.UserId, (int)Identifier.AccountSettingAvatarSize._xat) + "\"></a>");
                    sb.Append("<dl class=\"info\">");
                    sb.Append("  <dt>");
                    //标题（城市/国家）
                    if (item.IBTType == (int)Identifier.IBTType.Country)
                    {
                        if (item.ActiveType == rating)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has rated <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == recommended)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> recommended <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                    }
                    else if (item.IBTType == (int)Identifier.IBTType.City)
                    {
                        if (item.ActiveType == rating)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has rated <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == recommended)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> recommended <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" >" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                    }
                    else
                    {
                        if (item.ActiveType == rating)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> has rated <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else if (item.ActiveType == recommended)
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> recommended <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                        else
                        {
                            sb.Append("  <a href=\"/Profile/" + item.UserId.ToString() + "\" >" + item.FirstName + "</a><span class=\"f15  \"> posted a visit to <a href=\"" + item.ExTitleUrl + "\" class=\"f15\">" + item.ExTitle + "</a></span><span class=\"time\">" + item.CreateDate.ToString("MMM dd, yyyy") + "</span>");
                        }
                    }
                    //RWB
                    sb.Append("  <div class=\"choose\">");
                    sb.Append("     <span class=\"checkBg\"><a href=\"" + item.ExTitleUrl + "\"><img src=\"" + ShowImg + "\" class=\"mR5\" alt=\"\"  width=\"36\"/></a>");
                    uncheckbox_recommended = item.Recommended == Utils.IntToByte(1) ? "uncheckbox" : "";
                    sb.Append("          <span class=\"checkboxMTxt " + uncheckbox_recommended + "  mR5 cursordefault\" onclick=\"ibt.rrwb_rwb(this);\" data-rrwbType=\"2\" data-readonly=\"true\"  data-ibtType=\"" + item.IBTType.ToString() + "\" data-val=\"" + item.Recommended.ToString() + "\" data-objId=\"" + item.ExObjId.ToString() + "\">");
                    sb.Append("              <span class=\"bg\"><span class=\"alignM\"></span><span class=\"txt\">Recommended</span></span>");
                    sb.Append("          </span>");
                    sb.Append("<span class=\"line\"></span>");

                    //todo:Elson and 9 Friends has Been There 这里的9个好友是自己的（当前登录用户的）
                    //Elson(showNick) and 9 Friends has Been There 
                    showNick = type == 1 ? "You " : item.FirstName;
                    friendCount = _supplierService.HasBeenThereFriendsCount(UserId, item.CountryId, item.CityId, item.SupplyId);
                    strcount = friendCount <= 1 ? friendCount.ToString() + " Friend" : friendCount.ToString() + " Friends";
                    selfId = _supplierService.GetIBTUser(UserId, item.SupplyId, item.CountryId, item.CityId).UserId;  //
                    if (!(item.IBTType == (int)Identifier.IBTType.Supply && item.ServiceType == (int)Identifier.ServiceType.ServiceProvider))
                    {
                        if (friendCount == 0 && selfId == UserId)
                        {
                            sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">You are the first to have Been There!</span>");
                        }
                        else if (friendCount == 0 && selfId != UserId)
                        {
                            sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">None of your friends has Been There! Have you?</span>");
                        }
                        else if (friendCount > 0 && selfId != UserId)
                        {
                            sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\"><a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                        }
                        else if (showNick == item.FirstName && friendCount == 1)
                        {
                            strcount = "You";
                            sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and " + strcount + " has Been There</span>");
                        }
                        else
                        {
                            sb.Append("          <span class=\"friendsInfo italic f10 C7a7a7a\">" + showNick + " and <a href=\"javascript:void(0);\" onclick=\"ibt.loadUrlPop('/ShowFriend/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "','Get','',null,' " + strcount + " Who\\'ve Been There');\">" + strcount + "</a> has Been There</span>");
                        }
                    }
                    sb.Append("  </div>");
                    sb.Append("  </dt>");
                    sb.Append("  <dd>");
                    //编辑Edit和删除deleted (判断登录用户是否和该用户一致)
                    if ((item.ActiveType == (int)Identifier.FeedActiveType.IBT || item.ActiveType == (int)Identifier.FeedActiveType.AddTravel || item.ActiveType == (int)Identifier.FeedActiveType.PostVisit) && item.UserId == UserId)
                    {
                        sb.Append("    <div class=\"relative\">");
                        sb.Append("    <div class=\"editBox select\">");
                        sb.Append("    <span class=\"arrow\">&nbsp;</span>");
                        sb.Append("    <div class=\"list\">");
                        sb.Append("      <ul>");
                        sb.Append("        <li onclick=\"ibt.loadUrlPop('/PostVisit/" + item.IBTType.ToString() + "/" + item.ExObjId.ToString() + "?newsfeedId=" + item.NewsFeedId.ToString() + "','Get','',function(){$('#handleType').val(1);ibt.PvPopLoad();},'Edit POst visit');\">Edit</li>");
                        int IBTId = _supplierService.GetIBTInfo(item.UserId, item.SupplyId, item.CountryId, item.CityId).IBTId;
                        sb.Append("        <li onclick=\"jTravelFeed.DelPVNewsfeed(" + item.NewsFeedId + "," + item.NewsFeedId + "," + IBTId + ");\">Delete</li>");
                        sb.Append("      </ul>");
                        sb.Append("    </div>");
                        sb.Append("    </div>");
                        sb.Append("    </div>");
                    }
                    //Post visit 上传的图片
                    //sb.Append(this.getFeedPostVisitPhotoList(item.NewsFeedId, item.PostVistAttachmentList));
                    #region 评论的图片列表(user: country / city / supplier listing post)
                    IList<Attachment> attachmentList = _supplierService.GetAttachment(item.NewsFeedId, Identifier.AttachmentType.NewsFeed);
                    if (attachmentList.Count > 0)
                    {
                        sb.Append("  <div class=\"fixbox photo\">");
                        foreach (var att in attachmentList)
                        {
                            sb.Append("           <a href=\"javascript:void(0);\" onclick=\"PhotoManage.GetPhotoDetail(" + att.AttId + ", " + att.ObjId + ");\"><img class=\"mR10\" src=\"" + att.SourceFileName + Identifier.SuppilyPhotoSize._tn + ".jpg\" alt=\"\" /></a>");
                        }
                        sb.Append("  </div>");
                    }
                    #endregion
                    //------------group------------------- 只有post visit会显示，因为这个是experience
                    if (item.ActiveType == postVisit)
                    {
                        sb.Append("  <p>");
                        sb.Append(string.IsNullOrEmpty(item.ObjVal) ? "&nbsp;&nbsp;" : item.ObjVal);
                        sb.Append("  </p>");
                    }

                    sb.Append("<div class=\"operating\" id=\"operating_" + item.NewsFeedId + "\" style=\"float: none; text-align: left; width: 648px;\">");
                    //comments(数量)
                    int feedcommentsCount = _newsFeedService.GetFeedcommentcount(item.NewsFeedId);
                    sb.Append("     <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBox(" + item.NewsFeedId + ");\"><div id=\"feedcommentsCount_" + item.NewsFeedId + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");

                    sb.Append("</div>");
                    //history
                    //int historycount = GetHistoryCount(tabId, item.IBTType, item.ExObjId, item.UserId);
                    if (item.IbtCount > 1)
                    {
                        sb.Append("   <div class=\"history\">");
                        sb.Append("      <span class=\"sliderD sliderU \" id=\"feedHistoryTxt_" + item.NewsFeedId.ToString() + "\"  onclick=\"feedHistory.ShowFeedHistory('" + tabId.ToString() + "','" + item.NewsFeedId.ToString() + "','" + item.IBTType.ToString() + "','" + item.ExObjId.ToString() + "','" + item.UserId.ToString() + "');\"> view history  </span>");
                        sb.Append("        <div class=\"historyList\" id=\"feedHistoryList_" + item.NewsFeedId.ToString() + "\">");
                        sb.Append("        </div>");
                        sb.Append("   </div>");
                    }
                    sb.Append("   </dd>");
                    sb.Append("   </dl>");
                    sb.Append("</li>");

                }
                #endregion
            }

            Dictionary<string, int> dic = new Dictionary<string, int>();
            dic.Add("itemTotal", itemTotal);
            //如果返回的item=0 则不可以再下拉
            return Json(new BoolMessageItem(dic, true, sb.ToString()), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region photo's popup
        public ActionResult Attachment()
        {
            return View();
        }

        public JsonResult AttachmentPopup()
        {
            int attid = RequestHelper.GetValInt("attid", 0);
            Attachment atttachment = _ibtServices.GetAttachmentByAttid(attid);
            BoolMessageItem<int> bm = null;
            if (atttachment == null)
            {
                bm = new BoolMessageItem<int>(0, false, "atttachment is null");
                return Json(bm, JsonRequestBehavior.AllowGet);
            }
            //int newsfeedid = RequestHelper.GetValInt("newsfeedid", 0);
            int newsfeedid = atttachment.ObjId;

            NewsFeed nf = _newsFeedService.GetNewsFeedByNewsFeedId(newsfeedid);
            if (nf == null)
            {
                bm = new BoolMessageItem<int>(0, false, "newsfeed is null");
                return Json(bm, JsonRequestBehavior.AllowGet);
            }
            Profile profile = _profileService.GetUserProfile(nf.UserId);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //            <div class="btnBar alignR">
            //   <a href="#" class="mR15"><img src="/images/button/rotate_left.png" alt="Rotate Left" /></a>
            //   <a href="#" class="mR15"><img src="/images/button/rotate_right.png" alt="Rotate Right" /></a>
            //   <a href="#" class="mR15"><img src="/images/button/rotate_save.png" alt="Rotate Save" /></a>
            //</div>

            sb.Append("<div class=\"popup2\" style=\"width: 1004px;\">");
            sb.Append("<div class=\"popupMain\">");
            sb.Append("<a href=\"javascript:void(0);\" onclick=\"PhotoManage.DeletePhotoPopup();\" class=\"close\" title=\"Close\">X</a>");
            sb.Append("<div class=\"fixbox\">");
            sb.Append("<div class=\"imgBig\">");
            sb.Append("<span class=\"middle\"></span>");
            sb.Append("<img id=\"_rotate\" src=\"" + atttachment.SourceFileName + "_570x430.jpg\" style=\"\" alt=\"\" />");
            sb.Append("<div class=\"btnBar alignR\">");
            sb.Append("<a href=\"javascript:void(0);\" class=\"mR15\"><img src=\"/images/button/rotate_right.png\" alt=\"Rotate Right\" onclick=\"PhotoManage.RightRotate();\" /></a>");
            sb.Append("<a href=\"javascript:void(0);\" class=\"mR15\"><img src=\"/images/button/rotate_left.png\" alt=\"Rotate Left\" onclick=\"PhotoManage.LeftRotate();\" /></a>");
            if (UserId == nf.UserId)
            {
                sb.Append("<a href=\"javascript:void(0);\" class=\"mR15\"><img src=\"/images/button/rotate_save.png\" alt=\"Rotate Save\" onclick=\"PhotoManage.UpdateRotate(this);\" data-att=\"" + atttachment.AttId + "\" /></a>");
            }
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<div class=\"commentBox\">");
            sb.Append("<div class=\"scrollBox\">");
            sb.Append("<ul class=\"fixbox\">");
            sb.Append("<li>");
            sb.Append("<dl>");
            sb.Append("<dt class=\"fixbox\">");
            sb.Append("<a href=\"" + "/Profile/" + nf.UserId + "\" class=\"userPic\">");
            sb.Append("<img src=\"" + _profileService.GetUserAvatar(nf.UserId, 55) + "\" alt=\"" + profile.Firstname + "\"></a>");
            sb.Append("<div class=\"country\"><a href=\"" + "/Profile/"+nf.UserId + "\">" + profile.Firstname + "</a><span class=\"time\">" + nf.CreateDate.ToString("dd MMM yyyy") + "</span></div>");
            sb.Append("</dt>");
            sb.Append("<dd>");
            sb.Append("<p>" + nf.ObjVal + "</p>");
            sb.Append("<div class=\"operating\" id=\"operatingPop_" + newsfeedid + "\">");
            //comments(数量)
            int feedcommentsCount = _newsFeedService.GetFeedcommentcount(newsfeedid);
            sb.Append("                 <a href=\"javascript:void(0);\" onclick=\"feedComments.ShowMessageBoxPop(" + newsfeedid + ");\"><div id=\"feedcommentsCountPop_" + newsfeedid + "\" data-comments=\"" + feedcommentsCount + "\" >Comments (" + feedcommentsCount + ")</div></a>");

            #region 回复列表以及回复文本框
            //sb.Append("<div class=\"replyBox\" style=\"display: none;\" id=\"replyBoxPop_" + newsfeedid + "\">");
            //sb.Append("<ul id=\"replyListPop_" + newsfeedid + "\" >");
            //sb.Append("</ul>");
            //sb.Append("</div>");
            #endregion

            sb.Append("</div>");
            sb.Append("</dd>");
            sb.Append("</dl>");
            sb.Append("</li>");
            sb.Append("</ul>");
            sb.Append("</div>");
            //sb.Append("<div class=\"replyText\">");
            //sb.Append("<textarea id=\"commnetTxtPop_" + newsfeedid + "\" >Write a message...</textarea>");
            //sb.Append("<input style=\"float: right;\" type=\"button\" value=\"Comment\" onclick=\"feedComments.SaveCommentPop(" + newsfeedid + ")\" />");
            //sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");

            bm = new BoolMessageItem<int>(1, true, sb.ToString());
            return Json(bm, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 更新旋转后的图片
        /// </summary>
        /// <returns></returns>
        public JsonResult AttachmentRotateUpdate()
        {
            int attid = RequestHelper.GetValInt("att");
            int rotate = RequestHelper.GetValInt("rotate");
            BoolMessageItem<string> bms = new BoolMessageItem<string>(string.Empty, false, "Image rotation false!");
            if (rotate > 0 && rotate <= 3)
            {
                bms = _ibtServices.AttachmentRotate(UserId, attid, rotate);
            }
            return Json(bms, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
