﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Encrypt;
using IMeet.IBT.Common.Email;

namespace IMeet.IBT.Services
{
    public class AccountLogService:BaseService,IAccountLogService
    {
        /// <summary>
        /// 写IBT用户登录日志
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        public void Write(string userName, string passWord,bool validateSuccess,int bmiItem)
        {
            AccountLog log = new AccountLog()
            {
                UserName = userName,
                SnsType = (int)Identifier.SnsType.IBT,
                LoginIp = Utils.GetIP(),
                CreateDate = DateTime.Now
            };
            //登录验证是否成功
            if (validateSuccess)
            {
                log.UserId = bmiItem;
                log.AccountStatusType = (int)Identifier.AccountStatusType.Success;
            }
            else
            {
                log.UserId = 0;
                log.FailureInfoType = bmiItem;
                log.AccountStatusType = (int)Identifier.AccountStatusType.Fail;
                //是否密码验证失败
                if (log.FailureInfoType == (int)Identifier.FailureInfoType.PassowrdIncorrect)
                    log.Password = passWord;
            }

            _db.AccountLogs.Add(log);
            _db.SaveChanges();
        }



        /// <summary>
        /// 写sns登录日志
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="snsType"></param>
        public void WriteSns(int userId, Identifier.SnsType snsType)
        {
            AccountLog log = new AccountLog() { 
                LoginIp=Utils.GetIP(),
                UserId=userId,
                SnsType=(int)snsType,
                CreateDate=DateTime.Now,
                AccountStatusType=(int)Identifier.AccountStatusType.Success
            };
            _db.AccountLogs.Add(log);
            _db.SaveChanges();
        }
    }
}
