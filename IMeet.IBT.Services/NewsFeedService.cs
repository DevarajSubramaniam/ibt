﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public class NewsFeedService : BaseService, INewsFeedService
    {
        #region NewsFeed
        /// <summary>
        /// Add newsfeed 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <param name="ibtType">ibtType</param>
        /// <param name="activeType"></param>
        /// <param name="objVal"></param>
        /// <param name="objData"></param>
        /// <returns></returns>
        public BoolMessageItem<int> AddNewsFeed(int userId, int countryId, int cityId, int supplyId, Identifier.IBTType ibtType, Identifier.FeedActiveType activeType, string objVal, string objData, DateTime? pvDate)
        {
            return this.AddNewsFeed(userId, countryId, cityId, supplyId, (int)ibtType, activeType, objVal, objData, pvDate);
        }

        public BoolMessageItem<int> AddNewsFeed(int userId, int countryId, int cityId, int supplyId, int ibtType, Identifier.FeedActiveType activeType, string objVal, string objData, DateTime? pvDate)
        {
            try
            {
                //1.没有就新添加
                NewsFeed feed = new NewsFeed();
                feed.UserId = userId;
                feed.CountryId = countryId;
                feed.CityId = cityId;
                feed.SupplyId = supplyId;
                feed.IBTType = (int)ibtType;
                feed.ActiveType = (int)activeType;
                feed.ObjVal = objVal;
                feed.ObjData = objData;
                feed.StatusId = (int)Identifier.StatusType.Enabled;
                if (pvDate == null)
                {
                    feed.CreateDate = DateTime.Now;
                }
                else
                {
                    feed.CreateDate = DateTime.Parse(pvDate.ToString());
                }
                feed.CommentCount = 0;

                _db.NewsFeeds.Add(feed);

                //添加BadgeSpecial point
                #region 添加BadgeSpecial point
                string commentType = "";

                if (feed.IBTType == (int)Identifier.IBTType.Country)
                {
                    commentType = Enum.GetName(typeof(Identifier.BadgeSpecialCommentType), Identifier.BadgeSpecialCommentType.Country);
                }
                else if (feed.IBTType == (int)Identifier.IBTType.City)
                {
                    commentType = Enum.GetName(typeof(Identifier.BadgeSpecialCommentType), Identifier.BadgeSpecialCommentType.City);
                }
                else if (feed.IBTType == (int)Identifier.IBTType.Supply)
                {
                    commentType = Enum.GetName(typeof(Identifier.BadgeSpecialCommentType), Identifier.BadgeSpecialCommentType.Supply);
                }

                if (!string.IsNullOrEmpty(commentType))
                {
                    string key = "GetUserTravelbadge_" + userId.ToString();
                    if (_cache.Contains(key))
                        _cache.Remove(key);

                    List<BadgeSpecial> list = _db.BadgeSpecials.Where(b => b.USAActivity == commentType && b.Type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity && b.StatusId == 0 && b.StartDate <= DateTime.Now && b.EndDate >= DateTime.Now).ToList();

                    list.ForEach(bs =>
                    {
                        UserBadgeSpecial ubs = new UserBadgeSpecial()
                        {
                            UserId = feed.UserId,
                            BadgeSpecialId = bs.BadgeSpecialId,
                            point = bs.USAPoint,
                            ReceivedDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            StatusId = 0
                        };

                        _db.UserBadgeSpecials.Add(ubs);
                    });
                }

                #endregion

                if (_db.SaveChanges() > 0)
                {
                    return new BoolMessageItem<int>(feed.NewsFeedId, true, "");
                }
                else
                {
                    return new BoolMessageItem<int>(-1, false, "");
                }


            }
            catch (Exception ex)
            {
                _logger.Error("AddNewFeed", ex);
                return new BoolMessageItem<int>(-1, false, "Save Error!");
            }
        }

        /// <summary>
        /// Edit newsfeed 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <param name="ibtType"></param>
        /// <param name="activeType"></param>
        /// <param name="objVal"></param>
        /// <param name="objData"></param>
        /// <returns></returns>
        public BoolMessageItem<int> EditNewsFeed(int newsfeedId, string objVal, string objData, DateTime? pvDate)
        {
            try
            {
                var item = (from c in _db.NewsFeeds where (c.NewsFeedId.Equals(newsfeedId)) select c).FirstOrDefault();
                //查找是否已经有了。如果有了就是更新(pv)
                if (item != null)
                {
                    item.ObjVal = objVal;
                    item.ObjData = objData;
                    if (pvDate == null)
                    {
                        item.CreateDate = DateTime.Now;
                    }
                    else
                    {
                        item.CreateDate = DateTime.Parse(pvDate.ToString());
                    }
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(item.NewsFeedId, true, "");
                }
                else
                {
                    return new BoolMessageItem<int>(-1, false, "");
                }


            }
            catch (Exception ex)
            {
                _logger.Error("EditNewFeed", ex);
                return new BoolMessageItem<int>(-1, false, "Save Error!");
            }
        }

        public BoolMessageItem<int> NewsFeedIsDel(int newsfeedId)
        {
            string strsql = @"Update [NewsFeed] SET [StatusId]=3
                             WHERE [NewsFeedId] = {0}";
            _db.Database.ExecuteSqlCommand(strsql, newsfeedId);
            return new BoolMessageItem<int>(1, true, "Delete Successful!");
        }

        /// <summary>
        /// 根据newsFeedId会删除post
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <returns></returns>
        public BoolMessageItem<int> HasPVRemove(int newsFeedId)
        {
            NewsFeed newsfeed = _db.NewsFeeds.FirstOrDefault(x => x.NewsFeedId.Equals(newsFeedId));
            if (newsfeed != null && newsfeed.StatusId != 3)
            {
                newsfeed.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(newsfeed.NewsFeedId, true, "The post has been successfully deleted!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "The post is not successfully deleted. Please try again!");
            }
        }

        /// <summary>
        /// Profile feedlist
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<FeedList> GetNewsFeedList(int userId, int page, int pageSize, out int total)
        {
            total = 0;
            return new List<FeedList>();
        }

        public IList<FeedList> GetFeedHistoryList(string strWhere, int userId, out int total)
        {
            string strSql = @"
                   SELECT 	t1.* FROM (
		SELECT    dbo.NewsFeed.NewsFeedId, dbo.NewsFeed.UserId, dbo.NewsFeed.CountryId, dbo.NewsFeed.CityId, dbo.NewsFeed.SupplyId, dbo.NewsFeed.IBTType, dbo.NewsFeed.CreateDate,
				  dbo.NewsFeed.ObjData, dbo.NewsFeed.CommentCount, dbo.NewsFeed.ActiveType, dbo.NewsFeed.ObjVal, ISNULL(dbo.IBT.IbtCount,0) AS IbtCount, dbo.Cities.CityName, 
				  dbo.Countries.CountryName, dbo.Countries.FlagSrc, dbo.Supplies.Title AS SupplyName, dbo.Supplies.CoverPhotoSrc, dbo.Profiles.Firstname, dbo.Profiles.ProfileImage,
				  ROW_NUMBER() OVER (ORDER BY dbo.NewsFeed.NewsFeedId DESC) AS RowId,dbo.Users.RoleId, dbo.NewsFeed.StatusId
		FROM      dbo.IBT 
		          LEFT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND 
				  dbo.IBT.UserId = dbo.NewsFeed.UserId 
				  LEFT JOIN dbo.Countries ON dbo.NewsFeed.CountryId = dbo.Countries.CountryId 
				  LEFT JOIN dbo.Cities ON dbo.NewsFeed.CityId = dbo.Cities.CityId
				  LEFT JOIN dbo.Supplies ON dbo.NewsFeed.SupplyId = dbo.Supplies.SupplyId 
				  LEFT JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId
				  LEFT JOIN dbo.Users ON dbo.NewsFeed.UserId=dbo.Users.UserId				  
				WHERE 1=1 " + strWhere + @" AND dbo.NewsFeed.UserId={0} AND dbo.NewsFeed.StatusId=1 			
	) As t1
	WHERE RowId > 1 AND StatusId=1";
            IList<FeedList> list = _db.Database.SqlQuery<FeedList>(strSql, userId).ToList<FeedList>();
            total = list.Count();
            return list;
        }
        /// <summary>
        /// My travel List
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tabId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<FeedList> GetMyTravelList(int userId, int tabId, int page, int pageSize, out int total)
        {
            total = 0;
            return new List<FeedList>();
        }

        /// <summary>
        /// 根据newsfeedId获得NewsFeed表的信息
        /// </summary>
        /// <param name="newsfeedId"></param>
        /// <returns></returns>
        public NewsFeed GetNewsFeedInfo(int newsfeedId)
        {
            return (from c in _db.NewsFeeds where c.NewsFeedId.Equals(newsfeedId) select c).FirstOrDefault();
        }
        #endregion

        #region FeedComments
        /// <summary>
        /// feed添加评论
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        /// <remarks>同时更新评论数</remarks>
        public BoolMessageItem<int> AddFeedComments(int newsFeedId, int userId, string comments)
        {
            FeedComment feedComment = new FeedComment();
            feedComment.UserId = userId;
            feedComment.NewsFeedId = newsFeedId;
            feedComment.Comments = comments;
            feedComment.StatusId = (int)Identifier.StatusType.Enabled;
            feedComment.CreateDate = DateTime.Now;

            _db.FeedComments.Add(feedComment);
            _db.SaveChanges();

            //更新评论数
            string sql = @"UPDATE [NewsFeed]
                               SET [CommentCount] = (SELECT COUNT(1) FROM FeedComments WHERE NewsFeedId={0} AND StatusId=1)
                               WHERE  NewsFeedId={0}";
            _db.Database.ExecuteSqlCommand(sql, newsFeedId);

            #region 添加badge special point

            NewsFeed newsFeed = _db.NewsFeeds.Find(newsFeedId);

            if (newsFeed != null)
            {
                string commentType = "";

                switch (newsFeed.IBTType)
                {
                    case (int)Identifier.IBTType.Country:
                        commentType = Enum.GetName(typeof(Identifier.BadgeSpecialCommentType), Identifier.BadgeSpecialCommentType.Country);
                        break;
                    case (int)Identifier.IBTType.City:
                        commentType = Enum.GetName(typeof(Identifier.BadgeSpecialCommentType), Identifier.BadgeSpecialCommentType.City);
                        break;
                    case (int)Identifier.IBTType.Supply:
                        commentType = Enum.GetName(typeof(Identifier.BadgeSpecialCommentType), Identifier.BadgeSpecialCommentType.Supply);
                        break;
                    default:
                        break;
                }

                //产生COMMENT/RATE BONUS积分
                if (!string.IsNullOrWhiteSpace(commentType))
                {
                    string key = "GetUserTravelbadge_" + userId.ToString();
                    if (_cache.Contains(key))
                        _cache.Remove(key);

                    List<BadgeSpecial> list = _db.BadgeSpecials.Where(b => b.StatusId == 0 && b.Type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity && b.USAActivity == commentType && b.StartDate <= DateTime.Now && b.EndDate >= DateTime.Now).ToList();

                    list.ForEach(b =>
                    {
                        UserBadgeSpecial userBadgeSpecial = new UserBadgeSpecial()
                        {
                            UserId = userId,
                            BadgeSpecialId = b.BadgeSpecialId,
                            point = b.USAPoint,
                            ReceivedDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            PID = feedComment.feedCommentId
                        };

                        _db.UserBadgeSpecials.Add(userBadgeSpecial);

                        //if (_db.UserBadgeSpecials.Count(ubs => ubs.BadgeSpecialId == b.BadgeSpecialId && ubs.UserId == userId && ubs.StatusId == 0) == 0)
                        //{
                        //    UserBadgeSpecial userBadgeSpecial = new UserBadgeSpecial()
                        //    {
                        //        UserId = userId,
                        //        BadgeSpecialId = b.BadgeSpecialId,
                        //        point = b.USAPoint,
                        //        ReceivedDate = DateTime.Now,
                        //        UpdateDate = DateTime.Now,
                        //        PID = feedComment.feedCommentId
                        //    };

                        //    _db.UserBadgeSpecials.Add(userBadgeSpecial);
                        //}
                    });
                }

                _db.SaveChanges();
            }

            #endregion

            return new BoolMessageItem<int>(0, false, "");
        }
        /// <summary>
        /// 获取指定的feed的评论列表
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IList<FeedcommentsEx> GetFeedCommentList(int newsFeedId, int page, int pageSize)
        {
            string sql = @"
                    SELECT * FROM(
	                    SELECT     dbo.FeedComments.*, dbo.Profiles.Firstname, dbo.Profiles.ProfileImage
	                    , ROW_NUMBER() OVER (ORDER BY feedCommentId DESC) AS RowID
                    FROM   dbo.FeedComments INNER JOIN
                           dbo.Profiles ON dbo.FeedComments.UserId = dbo.Profiles.UserId
                    where NewsFeedId={0} AND FeedComments.StatusId=1
                    ) AS page WHERE page.RowID BETWEEN {1} AND {2}";
            return _db.Database.SqlQuery<FeedcommentsEx>(sql, newsFeedId, (page - 1) * pageSize + 1, (page - 1) * pageSize + pageSize).Reverse().ToList<FeedcommentsEx>();
        }
        /// <summary>
        /// 获取某个feedcomment的总数
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <returns></returns>
        public int GetFeedcommentcount(int newsFeedId)
        {
            var count = (from a in _db.FeedComments where a.NewsFeedId == newsFeedId && a.UserId > 0 select a).ToList();

            return count.Count;
        }
        public NewsFeed GetNewsFeedByNewsFeedId(int newsFeedId)
        {
            var nf = (from a in _db.NewsFeeds where a.NewsFeedId == newsFeedId select a).FirstOrDefault();
            return nf;
        }
        #endregion

        #region 获取feed的年月
        public IList<FeedItem> GetFeedDateList(int userId, int ibtType)
        {
            string strSql = @"SELECT distinct convert(varchar(7),CreateDate, 120) AS YearMonth FROM dbo.NewsFeed 
                              WHERE  dbo.NewsFeed.ActiveType IN (5,6,7) AND dbo.NewsFeed.UserId={0} AND dbo.NewsFeed.IBTType={1} AND dbo.NewsFeed.StatusId=1 
                              ORDER BY YearMonth DESC";
            return _db.Database.SqlQuery<FeedItem>(strSql, userId, ibtType).ToList<FeedItem>();
        }
        public IList<FeedItem> GetServicesDate(int userId, int ibtType)
        {
            string strSql = @"SELECT distinct convert(varchar(7),dbo.NewsFeed.CreateDate, 120) AS YearMonth FROM dbo.NewsFeed 
                              LEFT JOIN dbo.Supplies ON dbo.NewsFeed.SupplyId = dbo.Supplies.SupplyId
                              WHERE  dbo.NewsFeed.ActiveType IN (5,6,7) 
                              AND dbo.NewsFeed.UserId={0} AND dbo.NewsFeed.IBTType={1} 
                              AND dbo.NewsFeed.StatusId=1 AND dbo.Supplies.ServiceType=1
                              ORDER BY YearMonth DESC";
            return _db.Database.SqlQuery<FeedItem>(strSql, userId, ibtType).ToList<FeedItem>();
        }
        #endregion
    }
}
