﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.DAL.Models;
using System.Data.SqlClient;

namespace IMeet.IBT.Services
{
    public class WidgetService : BaseService, IWidgetService
    {
        #region Widget Statistics

        #region 统计出现次数(暂时不用)
        public BoolMessageItem<int> InitWidgetAriseCount(int mediaId, string url)
        {
            try
            {
                var item = (from c in _db.StatisticsReportInfoes where (c.MediaId.Equals(mediaId) && c.LocalUrl.Equals(url)) select c).FirstOrDefault();
                //1.查找是否已经有了。如果有了就是更新
                if (item != null)
                {
                    item.AriseCount++;
                    item.UpdateDate = DateTime.Now;
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(item.ReportId, true, "");
                }
                else
                {
                    //2.没有就新添加
                    DAL.StatisticsReportInfo report = new StatisticsReportInfo();
                    report.MediaId = mediaId;
                    report.AriseCount = 1;
                    report.ClickCount = 0;
                    report.BrowseCount = 0;
                    report.LocalUrl = url;
                    report.CreateDate = DateTime.Now;
                    report.UpdateDate = DateTime.Now;
                    //report.StatusId = (int)Identifier.StatusType.Enabled;
                    _db.StatisticsReportInfoes.Add(report);
                    if (_db.SaveChanges() > 0)
                    {
                        return new BoolMessageItem<int>(report.ReportId, true, "");
                    }
                    else
                    {
                        return new BoolMessageItem<int>(-1, false, "");
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Report", ex);
                return new BoolMessageItem<int>(-1, false, "Save Error!");
            }

        }
        #endregion

        #region 统计 点击数、浏览数
        public BoolMessageItem<int> StatisticsWidgetHit(int mediaId, string visitUrl)
        {
            StatisticsReportInfo report = _db.StatisticsReportInfoes.FirstOrDefault(x => x.MediaId.Equals(mediaId) && x.LocalUrl.Equals(visitUrl));
            if (report != null)
            {
                report.BrowseCount++;
                report.ClickCount++;
                report.UpdateDate = DateTime.Now;
                _db.SaveChanges();
                return new BoolMessageItem<int>(report.ReportId, true, "Update successful!");

            }
            else
            {
                DAL.StatisticsReportInfo newreport = new StatisticsReportInfo();
                newreport.MediaId = mediaId;
                newreport.AriseCount = 1;
                newreport.ClickCount = 1;
                newreport.BrowseCount = 1;
                newreport.LocalUrl = visitUrl;
                newreport.CreateDate = DateTime.Now;
                newreport.UpdateDate = DateTime.Now;
                //report.StatusId = (int)Identifier.StatusType.Enabled;
                _db.StatisticsReportInfoes.Add(newreport);
                if (_db.SaveChanges() > 0)
                {
                    return new BoolMessageItem<int>(newreport.ReportId, true, "Create successful!");
                }
                else
                {
                    return new BoolMessageItem<int>(-1, false, "Create failure!");
                }
            }
        }
        #endregion

        #endregion

        #region Ads / Special Offers
        /// <summary>
        /// 返回widget media 
        /// </summary>
        /// <param name="Position"></param>
        /// <returns></returns>
        public IList<Media> GetWidgetMediaIBT(int count, Identifier.PositionTypes Position, Identifier.MediaTypes MediaType)
        {
            //todo:这里在后台的时候需要清空这里的chche GetWidgetRecommonIBT
            int _Position = (int)Position;
            int _MediaType = (int)MediaType;
            byte statusId = Utils.IntToByte(1);
            return _cache.GetOrInsert<IList<Media>>("GetWidgetMediaIBT_" + Position.ToString() + "" + MediaType.ToString(), 30, true, () =>
            {
                string sql = @"SELECT TOP " + count.ToString() + @" [MediaId]
      ,[SupplyId]
      ,[CountryId]
      ,[CityId]
      ,[IBTType]
      ,[Position]
      ,[MediaType]
      ,[Title]
      ,[Desp]
      ,[StartDate]
      ,[ExpiryDate]
      ,[FileSrc]
      ,[Code]
      ,[IsLink]
      ,[TargetUrl]
      ,[StatusId]
      ,[CreateDate]
      ,[UpdateDate]
  FROM Medias WHERE WHERE Convert(varchar(10),dbo.Medias.ExpiryDate,120) >= Convert(varchar(10),getdate(),120) AND StatusId=1 AND Position=" + _Position.ToString() + " AND MediaType=" + _MediaType.ToString() + @" ORDER BY newid()";

                return _db.Database.SqlQuery<Media>(sql).ToList<Media>();
            });
        }
        public IList<Media> GetWidgetOfPosition(int count, Identifier.PositionTypes Position, string localUrl)
        {
            int _Position = (int)Position;
            byte statusId = Utils.IntToByte(1);

            string sql = @"SELECT TOP " + count.ToString() + @" [MediaId]
      ,[SupplyId]
      ,[CountryId]
      ,[CityId]
      ,[IBTType]
      ,[Position]
      ,[MediaType]
      ,[Title]
      ,[Desp]
      ,[StartDate]
      ,[ExpiryDate]
      ,[FileSrc]
      ,[Code]
      ,[IsLink]
      ,[TargetUrl]
      ,[StatusId]
      ,[CreateDate]
      ,[UpdateDate]
  FROM Medias WHERE Convert(varchar(10),ExpiryDate,120) >= Convert(varchar(10),getdate(),120) AND StatusId=1 AND Position=" + _Position.ToString() + @" ORDER BY newid()";
            var list = _db.Database.SqlQuery<Media>(sql).ToList<Media>();
            foreach (var item in list)
            {
                var report = (from c in _db.StatisticsReportInfoes where (c.MediaId.Equals(item.MediaId)) select c).FirstOrDefault();
                //1.查找是否已经有了。如果有了就是更新
                if (report != null)
                {
                    report.AriseCount++;
                    report.LocalUrl = localUrl;
                    report.UpdateDate = DateTime.Now;
                    _db.SaveChanges();
                }
                else  //没有就添加
                {
                    DAL.StatisticsReportInfo newreport = new StatisticsReportInfo();
                    newreport.MediaId = item.MediaId;
                    newreport.AriseCount = 1;
                    newreport.ClickCount = 0;
                    newreport.BrowseCount = 0;
                    newreport.LocalUrl = localUrl;
                    newreport.CreateDate = DateTime.Now;
                    newreport.UpdateDate = DateTime.Now;
                    _db.StatisticsReportInfoes.Add(newreport);
                    _db.SaveChanges();
                }
            }
            return list.ToList();
        }
        #endregion

        #region IBT
        /// <summary>
        /// 返回widget ibt 列表
        /// </summary>
        /// <param name="ibtType"></param>
        /// <returns></returns>
        public IList<WidgetIBT> GetWidgetRecommonIBT(Identifier.IBTType ibtType)
        {
            //todo:这里在后台的时候需要清空这里的chche GetWidgetRecommonIBT
            int _ibtType = (int)ibtType;
            byte statusId = Utils.IntToByte(1);
            return _cache.GetOrInsert<IList<WidgetIBT>>("GetWidgetRecommonIBT" + ibtType.ToString(), 30, true, () =>
            {
                return (from c in _db.WidgetIBTs where (c.StatusId.Equals(statusId) && c.IBTType.Equals(_ibtType)) orderby c.Sort ascending select c).ToList<WidgetIBT>();
            });
        }

        /// <summary>
        /// 返回所有的国家IBT
        /// </summary>
        /// <param name="firstLetter"></param>
        /// <returns></returns>
        public IList<WidgetIBT> GetWidgetAllCountryIBT(string firstLetter)
        {
            string sql = @"SELECT [CountryId] AS ibtId,
	                       [CountryId] AS [ObjId],
                           [CountryName] AS ObjName,
                           0 AS Sort,
                           1 As IBTType,
                           [StatusId],
                           0 AS UserId,
                           [CreateDate],
                           [UpdateDate]
                      FROM [Countries]";
            if (!string.IsNullOrWhiteSpace(firstLetter))
            {
                sql = sql + " WHERE CountryName LIKE {0} ";
            }
            sql = sql + "  ORDER BY sort ASC";

            return _db.Database.SqlQuery<WidgetIBT>(sql, firstLetter + "%").ToList<WidgetIBT>();
        }
        #endregion

        #region Destination of the Week (Home page)
        public IList<DestinationWeekItem> GetWidgetDestinationWeekList(int userId, int topN)
        {
            //            string sql = @"SELECT TOP " + topN.ToString() + @" dbo.Supplies.CountryId, dbo.Supplies.CityId, dbo.Countries.CountryName, dbo.Cities.CityName, dbo.WidgetDWeek.ImgSrc, dbo.Supplies.Title, 
            //                                                  dbo.Supplies.SupplyId, dbo.Supplies.Rating, dbo.Supplies.InfoAddress, dbo.WidgetDWeek.Desp
            //                            FROM         dbo.Countries INNER JOIN
            //                                                  dbo.Supplies ON dbo.Countries.CountryId = dbo.Supplies.CountryId LEFT JOIN
            //                                                  dbo.Cities ON dbo.Supplies.CityId = dbo.Cities.CityId INNER JOIN
            //                                                  dbo.WidgetDWeek ON dbo.Supplies.SupplyId = dbo.WidgetDWeek.SupplyId
            //                            WHERE dbo.WidgetDWeek.StatusId=1
            //                            ORDER BY WidgetDWeek.Sort ASC";
            string sql = @"	
	SELECT * FROM (
		SELECT  ROW_NUMBER() OVER (ORDER BY Sort ASC, temp_rowid ASC) AS RowId,* 
		FROM(
		SELECT 0 AS temp_rowid,wd.ObjId,wd.DWType,
		0 AS SupplyId,cn.CountryName AS Title,'' AS InfoAddress,ISNULL(wd.Desp, '') AS Desp,
        wd.ImgSrc,
        cn.CountryId,cn.CountryName,
        0 AS CityId,'' AS CityName,
        ISNULL(cn.Rating,0) AS Rating,
        wd.Sort                           
                   FROM  dbo.WidgetDWeek wd
                   LEFT JOIN dbo.Countries cn ON wd.ObjId=cn.CountryId 
                   LEFT JOIN dbo.CountriesDes cd ON cn.CountryId = cd.CountryId 
                   WHERE wd.StatusId=1 AND wd.DWType=1
        UNION
        SELECT 1 AS temp_rowid,wd.ObjId,wd.DWType,
		0 AS SupplyId,cy.CityName AS Title,'' AS InfoAddress,ISNULL(wd.Desp, '') AS Desp,
        wd.ImgSrc,
        cn.CountryId AS CountryId,cn.CountryName AS CountryName,
        cy.CityId,cy.CityName,
        ISNULL(cy.Rating,0) AS Rating,
        wd.Sort                          
                    FROM  dbo.WidgetDWeek wd
                    LEFT JOIN  dbo.Cities cy ON wd.ObjId=cy.CityId 
                    LEFT JOIN  dbo.Countries cn ON cy.CountryId = cn.CountryId  
                    LEFT JOIN dbo.CitiesDescription cd ON cy.CityId = cd.CityId
                    WHERE wd.StatusId=1 AND wd.DWType=2    
        UNION
        SELECT 2 AS temp_rowid,wd.ObjId,wd.DWType,
		sp.SupplyId,sp.Title,sp.InfoAddress,ISNULL(wd.Desp, '') AS Desp,
        wd.ImgSrc,sp.CountryId,cn.CountryName,
        sp.CityId,ISNULL(cy.CityName, '') AS CityName,
        ISNULL(sp.Rating,0) AS Rating,
        wd.Sort                          
                   FROM  dbo.WidgetDWeek wd
                            LEFT JOIN dbo.Supplies sp ON wd.ObjId=sp.SupplyId 
                            LEFT JOIN dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN dbo.Cities cy ON sp.CityId = cy.CityId 
                            WHERE wd.StatusId=1 AND wd.DWType=3
        )AS temp  WHERE temp.SupplyId  IS NOT NULL
	) As t1
	WHERE RowId BETWEEN 1 AND 4";

     
		
            return _db.Database.SqlQuery<DestinationWeekItem>(sql).ToList<DestinationWeekItem>();
        }
        public IList<SupplyHomeListShort> GetMostRecommendedPlaces(int userId, int topN)
        {
            /* 20130119裕冲修改
            string sql = @"SELECT  TOP " + topN.ToString() + @"   
                            dbo.WidgetMostRecommentdPlaces.SupplyId, 
                            dbo.Cities.CountryId, 
                            dbo.Countries.CountryName,
                            dbo.Supplies.CityId,
                            dbo.Cities.CityName, 
				            dbo.Supplies.Title, 
                            dbo.Supplies.InfoAddress,
                            dbo.Countries.FlagSrc AS CoverPhotoSrc,
                            ISNULL(dbo.Supplies.CommentsCount,0) AS CommentsCount,
                            ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
                            ISNULL(dbo.RRWB.Rating,0) AS Rating, 
                            ISNULL(dbo.RRWB.Recommended, 0) AS Recommended,
                            ISNULL(dbo.RRWB.WanttoGo,0) AS WanttoGo, 
                            ISNULL(dbo.RRWB.BucketList,0) AS BucketList
                        FROM dbo.Cities 
                            INNER JOIN dbo.WidgetMostRecommentdPlaces 
                            INNER JOIN dbo.Supplies ON dbo.WidgetMostRecommentdPlaces.SupplyId = dbo.Supplies.SupplyId 
                            INNER JOIN dbo.Countries ON dbo.Supplies.CountryId = dbo.Countries.CountryId ON dbo.Cities.CityId = dbo.Supplies.CityId 
                            LEFT OUTER JOIN dbo.RRWB ON dbo.Supplies.SupplyId = dbo.RRWB.ObjId AND RRWB.IBTType=3 AND dbo.RRWB.UserId=dbo.WidgetMostRecommentdPlaces.UserId
                            LEFT OUTER JOIN dbo.IBT ON dbo.Supplies.SupplyId = dbo.IBT.SupplyId AND ibt.UserId=dbo.WidgetMostRecommentdPlaces.UserId
                        WHERE WidgetMostRecommentdPlaces.StatusId=1
                        ORDER BY WidgetMostRecommentdPlaces.Sort ASC";
            * */
            //            string sql = @"SELECT TOP " + topN.ToString() + @" 
            //                            wr.ObjId, 
            //                            ISNULL(ct.CountryId, 0) AS CountryId, 
            //                            ISNULL(ct.CityName,'-') AS CityName, 
            //                            sp.CityId,
            //                            sp.Title, 
            //                            sp.InfoAddress,
            //                            cn.CountryName,
            //                            sp.CoverPhotoSrc,
            //                            wr.Sort,
            //                            ISNULL(sp.CommentsCount,0) AS CommentsCount,
            //                            ISNULL(it.IbtCount, 0) AS IbtCount,
            //                            ISNULL(sp.Rating,0) AS Rating, 
            //                            ISNULL(sp.RecommendedCount, 0) AS RecommendedCount,
            //                            ISNULL(sp.WanttoGoCount,0) AS WanttoGoCount, 
            //                            ISNULL(sp.BucketListCount,0) AS BucketListCount
            //                        FROM 
            //                            dbo.WidgetMostRecommentdPlaces wr 				
            //                            LEFT JOIN dbo.Supplies sp on sp.SupplyId=wr.ObjId
            //                            LEFT JOIN dbo.Countries cn ON sp.CountryId = cn.CountryId 
            //                            LEFT JOIN dbo.Cities ct ON sp.CityId = ct.CityId 
            //                            LEFT OUTER JOIN dbo.RRWB rb ON sp.SupplyId = rb.ObjId AND rb.IBTType=3 AND rb.UserId=wr.UserId
            //                            LEFT OUTER JOIN dbo.IBT it ON sp.SupplyId = it.SupplyId AND it.UserId=wr.UserId
            //                        WHERE wr.StatusId <> 3 
            //                    ORDER BY wr.Sort ASC,wr.ObjId DESC";
            string sql = @"
	  SELECT * FROM (
		SELECT  ROW_NUMBER() OVER (ORDER BY Sort ASC, temp_rowid ASC) AS RowId,* 
		FROM(
		SELECT 0 AS temp_rowid,
		0 AS SupplyId,cn.CountryName AS Title,1 AS objType,ISNULL(SUBSTRING(cd.CountryTxt,1,datalength(cd.CountryTxt)), '') AS Desp,
        cn.FlagSrc AS CoverPhotoSrc,
        cn.CountryId,cn.CountryName,
        0 AS CityId,'' AS CityName,
        ISNULL(ibt.IbtCount, 0) AS IbtCount,
        ISNULL(cn.Rating,0) AS Rating,
        wr.Sort                           
                   FROM  dbo.WidgetMostRecommentdPlaces wr
                   LEFT JOIN dbo.Countries cn ON wr.ObjId=cn.CountryId 
                   LEFT JOIN dbo.CountriesDes cd ON cn.CountryId = cd.CountryId 
                   LEFT OUTER JOIN dbo.IBT ibt ON wr.ObjId = ibt.CountryId AND ibt.CityId=0 AND ibt.SupplyId=0 AND ibt.UserId={0}
                   WHERE wr.StatusId=1 AND wr.MRPType=1
        UNION
        SELECT 1 AS temp_rowid,
		0 AS SupplyId,cy.CityName AS Title,2 AS objType,ISNULL(SUBSTRING(cd.Txt,1,datalength(cd.Txt)), '') AS Desp,
        cn.FlagSrc AS CoverPhotoSrc,
        cn.CountryId AS CountryId,cn.CountryName AS CountryName,
        cy.CityId,cy.CityName,
        ISNULL(ibt.IbtCount, 0) AS IbtCount,
        ISNULL(cy.Rating,0) AS Rating,
        wr.Sort                          
                    FROM  dbo.WidgetMostRecommentdPlaces wr
                    LEFT JOIN  dbo.Cities cy ON wr.ObjId=cy.CityId 
                    LEFT JOIN  dbo.Countries cn ON cy.CountryId = cn.CountryId  
                    LEFT JOIN dbo.CitiesDescription cd ON cy.CityId = cd.CityId
                    LEFT OUTER JOIN dbo.IBT ibt ON wr.ObjId = ibt.CityId AND ibt.SupplyId=0 AND ibt.UserId={0}
                    WHERE wr.StatusId=1 AND wr.MRPType=2    
        UNION
        SELECT 2 AS temp_rowid,
		sp.SupplyId,sp.Title,3 AS objType,sp.Desp,
        sp.CoverPhotoSrc,sp.CountryId,cn.CountryName,
        sp.CityId,cy.CityName,
        ISNULL(ibt.IbtCount, 0) AS IbtCount,
        ISNULL(sp.Rating,0) AS Rating,
        wr.Sort                          
                   FROM  dbo.WidgetMostRecommentdPlaces wr
                            LEFT JOIN dbo.Supplies sp ON wr.ObjId=sp.SupplyId 
                            LEFT JOIN dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN dbo.Cities cy ON sp.CityId = cy.CityId 
                            LEFT OUTER JOIN dbo.IBT ibt ON wr.ObjId = ibt.SupplyId AND ibt.UserId={0}
                            WHERE wr.StatusId=1 AND wr.MRPType=3
        )AS temp  
	) As t1
	WHERE RowId BETWEEN 1 AND 5";
            /*修改结束*/
            return _db.Database.SqlQuery<SupplyHomeListShort>(sql, userId).ToList<SupplyHomeListShort>();
        }
        #endregion

        #region DestinationList Management (Admin)

        public int GetDestinationListCount(string strWhere)
        {
            return 0;
        }
        public DAL.WidgetDWeek GetWidgetDWeekInfo(int dWeekId)
        {
            var wdw = _db.WidgetDWeeks.Where(c => c.DWeekId.Equals(dWeekId)).FirstOrDefault();
            if (wdw != null)
                return wdw;
            else
            {
                return new DAL.WidgetDWeek();
            }
        }
        public BoolMessageItem<int> AddNewDestination(int ObjId, int objType, int userId, string desp, string imgsrc)
        {
            var item = (from c in _db.WidgetDWeeks
                        where (c.ObjId.Equals(ObjId) && c.DWType.Equals(objType))
                        select c).FirstOrDefault();
            if (item != null)
            {
                if (item.StatusId.Equals(3))
                {
                    item.ObjId = ObjId;
                    item.UserId = userId;
                    item.Desp = desp;
                    item.ImgSrc = imgsrc;
                    item.Sort = 0;
                    item.StatusId = 1;
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(1, true, "You have successfully added a featured destination on the home page.");
                }
                else
                {
                    return new BoolMessageItem<int>(0, false, "Cannot repeat add!");
                }
            }
            else
            {
                WidgetDWeek wdw = new WidgetDWeek();
                wdw.ImgSrc = imgsrc;
                wdw.ObjId = ObjId;
                wdw.DWType = objType;
                wdw.UserId = userId;
                wdw.UpdateDate = DateTime.Now;
                wdw.Sort = 0;
                wdw.CreateDate = DateTime.Now;
                wdw.Desp = desp;
                wdw.StatusId = 1;
                _db.WidgetDWeeks.Add(wdw);
                _db.SaveChanges();
                return new BoolMessageItem<int>(wdw.DWeekId, true, "You have successfully added a featured destination on the home page.");  //不用激活
            }
        }
        public BoolMessageItem<int> EditDestination(int dWeekId, int sort, int ObjId, int userId, string desp, string imgsrc)
        {
            WidgetDWeek wdw = _db.WidgetDWeeks.FirstOrDefault(x => x.DWeekId.Equals(dWeekId));
            if (wdw != null)
            {
                wdw.ImgSrc = imgsrc;
                wdw.ObjId = ObjId;
                wdw.UserId = userId;
                wdw.UpdateDate = DateTime.Now;
                wdw.Sort = sort;
                //wdw.CreateDate = DateTime.Now;
                wdw.Desp = desp;
                wdw.StatusId = 1;
                _db.SaveChanges();
                return new BoolMessageItem<int>(wdw.DWeekId, true, "Edit successful!");

            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Edit failure!");
            }
        }
        /// <summary>
        /// Destination Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<DestinationWeekItem> GetDestinationList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total)
        {
            //--IBT_IMeetGetAllDestinationList 0,'DESC','',1,1,10,0,1,-1
            IList<DestinationWeekItem> list = _db.Database.SqlQuery<DestinationWeekItem>("EXEC IBT_IMeetGetAllDestinationList {0},{1},{2},{3},{4},{5}", OrderBy, DescOrAsc, filterKeyword, userId, page, pageSize).ToList<DestinationWeekItem>();
            total = list.Count();

            if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            {
                //todo:GetMyList filterField
                return new List<DestinationWeekItem>();
            }
            else
            {
                return list;
            }

            //todo 下面还需要返回成员数
        }
        //不用这方法
        public BoolMessageItem<int> SetDestinationSort(int dweekId, int newText)
        {
            WidgetDWeek wdw = _db.WidgetDWeeks.FirstOrDefault(x => x.DWeekId.Equals(dweekId));
            if (wdw != null)
            {
                wdw.Sort = newText;
                _db.SaveChanges();
                return new BoolMessageItem<int>(wdw.DWeekId, true, "Modified successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Modified failure!");
            }
        }
        public BoolMessageItem<int> SetDestinationIsDel(int dweekId)
        {
            WidgetDWeek dweek = _db.WidgetDWeeks.FirstOrDefault(x => x.DWeekId.Equals(dweekId));
            if (dweek != null && dweek.StatusId != 3)
            {
                dweek.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(dweek.DWeekId, true, "Deleted successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Delete failure!");
            }
        }
        /// <summary>
        /// Destination Sort
        /// </summary>
        /// <param name="ObjIds"></param>
        /// <returns></returns>
        public BoolMessageItem<int> SetWidgetDestinationSort(List<int> ObjIds)
        {
            int objId = 0;
            WidgetDWeek widgetDWeek;
            for (int i = 0; i < ObjIds.Count(); i++)
            {
                objId = ObjIds[i];
                widgetDWeek = _db.WidgetDWeeks.FirstOrDefault(x => x.DWeekId.Equals(objId) && x.StatusId.Equals(1));
                widgetDWeek.Sort = i + 1;
                _db.SaveChanges();

            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }
        #endregion

        #region Recommend
        public BoolMessageItem<int> AddNewRecommend(int ObjId, int objType, int score, int userId)
        {
            var item = (from c in _db.WidgetMostRecommentdPlaces
                        where (c.ObjId.Equals(ObjId) && c.MRPType.Equals(objType))
                        select c).FirstOrDefault();
            if (item != null )
            {
                if (item.StatusId.Equals(3))
                {
                    item.Sort = score;
                    item.UserId = userId;
                    item.StatusId = 1;
                    item.Sort = 0;
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(1, true, "Add successful!");
                }
                else
                {
                    return new BoolMessageItem<int>(0, false, "Cannot repeat add!");
                }
            }
            else
            {
                WidgetMostRecommentdPlace wrp = new WidgetMostRecommentdPlace();
                wrp.ObjId = ObjId;
                wrp.MRPType = objType;
                wrp.UserId = userId;
                wrp.UpdateDate = DateTime.Now;
                wrp.Sort = 0;
                wrp.CreateDate = DateTime.Now;
                wrp.StatusId = 1;
                _db.WidgetMostRecommentdPlaces.Add(wrp);
                _db.SaveChanges();
                return new BoolMessageItem<int>(wrp.MRPId, true, "Add successful!");  //不用激活
            }
        }
        public BoolMessageItem<int> SetRecommendSort(int mrpId, int newText)
        {
            WidgetMostRecommentdPlace wrp = _db.WidgetMostRecommentdPlaces.FirstOrDefault(x => x.MRPId.Equals(mrpId));
            if (wrp != null)
            {
                wrp.Sort = newText;
                _db.SaveChanges();
                return new BoolMessageItem<int>(wrp.MRPId, true, "Modified successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Modified failure!");
            }
        }
        public BoolMessageItem<int> SetRecommendIsDel(int mrpId)
        {
            WidgetMostRecommentdPlace wrp = _db.WidgetMostRecommentdPlaces.FirstOrDefault(x => x.MRPId.Equals(mrpId));
            if (wrp != null && wrp.StatusId != 3)
            {
                wrp.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(wrp.MRPId, true, "Deleted successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Delete failure. Please try again!");
            }
        }

        /// <summary>
        /// Recommend Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<RecommendWeekItem> GetRecommendList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total)
        {
            //--IBT_IMeetGetAllRecommendList 0,'DESC','',1,1,10,0,1,-1
            IList<RecommendWeekItem> list = _db.Database.SqlQuery<RecommendWeekItem>("EXEC IBT_IMeetGetAllRecommendList {0},{1},{2},{3},{4},{5}", OrderBy, DescOrAsc, filterKeyword, userId, page, pageSize).ToList<RecommendWeekItem>();
            total = list.Count();

            if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            {
                //todo:GetMyList filterField
                return new List<RecommendWeekItem>();
            }
            else
            {
                return list;
            }

            //todo 下面还需要返回成员数
        }
        public IList<RecommendWeekItem> GetHomeRecommendList(string strWhere, int page, int pageSize, out int total)
        {
            string strSql = @"SELECT   ROW_NUMBER() OVER (ORDER BY dbo.WidgetMostRecommentdPlaces.Sort ASC, dbo.WidgetMostRecommentdPlaces.SupplyId ASC) AS row_number,
                dbo.WidgetMostRecommentdPlaces.MRPId ,dbo.WidgetMostRecommentdPlaces.SupplyId AS SupplyId, dbo.Supplies.Title,dbo.WidgetMostRecommentdPlaces.Sort AS Score,
                dbo.Supplies.CoordsLatitude, dbo.Supplies.CoordsLongitude,dbo.supply_type(dbo.Supplies.SupplyId) AS TypesOfSupplier,3 AS typ,
                dbo.Supplies.CoverPhotoSrc,dbo.Supplies.InfoAddress,dbo.Supplies.InfoZip,dbo.Supplies.InfoPhone,
                dbo.Supplies.InfoFax,dbo.Supplies.InfoState,dbo.Supplies.InfoWebsite,dbo.WidgetMostRecommentdPlaces.CreateDate,dbo.WidgetMostRecommentdPlaces.UpdateDate,dbo.Supplies.CountryId,dbo.Countries.CountryName,dbo.Supplies.CityId,dbo.Cities.CityName
                                FROM    dbo.WidgetMostRecommentdPlaces 
                                LEFT JOIN dbo.Supplies ON dbo.WidgetMostRecommentdPlaces.SupplyId = dbo.Supplies.SupplyId
                                LEFT JOIN  dbo.Countries ON dbo.Supplies.CountryId = dbo.Countries.CountryId 
                                LEFT JOIN  dbo.Cities ON dbo.Supplies.CityId = dbo.Cities.CityId 
                                WHERE 1=1 " + strWhere.ToString() + @" AND dbo.WidgetMostRecommentdPlaces.StatusId=1";


            IList<RecommendWeekItem> list = _db.Database.SqlQuery<RecommendWeekItem>(strSql).ToList<RecommendWeekItem>();
            total = list.Count();
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.Sort ascending select c).Skip(startRecord).Take(pageSize).ToList<RecommendWeekItem>();
        }
        public IList<WidgetMostRecommentdPlace> GetHomeRecommendList()
        {
            var list = (from a in _db.WidgetMostRecommentdPlaces where a.StatusId == 1 select a);
            return list.ToList();
        }
        /// <summary>
        /// Recommend Sort
        /// </summary>
        /// <param name="ObjIds"></param>
        /// <returns></returns>
        public BoolMessageItem<int> SetWidgetRecommendSort(List<int> ObjIds)  //拖动排序
        {
            int objId = 0;
            WidgetMostRecommentdPlace wrp;
            for (int i = 0; i < ObjIds.Count(); i++)
            {
                objId = ObjIds[i];
                wrp = _db.WidgetMostRecommentdPlaces.FirstOrDefault(x => x.MRPId.Equals(objId) && x.StatusId.Equals(1));
                wrp.Sort = i + 1;
                _db.SaveChanges();

            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }
        #endregion

        #region Visited
        public BoolMessageItem<int> AddNewVisited(int ObjId, int objType, int score, int userId)
        {
            var item = (from c in _db.WidgetMostVisitedPlaces
                        where c.ObjId.Equals(ObjId) && c.MVPType.Equals(objType)
                        select c).FirstOrDefault();
            if (item != null)
            {
                if (item.StatusId.Equals(3))
                {
                    item.UserId = userId;
                    item.StatusId = 1;
                    item.Sort = 0;
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(1, true, "Add successful!");
                }
                else
                {
                    return new BoolMessageItem<int>(0, false, "Cannot repeat add!");
                }
            }
            else
            {
                WidgetMostVisitedPlace wrp = new WidgetMostVisitedPlace();
                wrp.ObjId = ObjId;
                wrp.MVPType = objType;
                wrp.UserId = userId;
                wrp.UpdateDate = DateTime.Now;
                wrp.Sort = 0;
                wrp.CreateDate = DateTime.Now;
                wrp.StatusId = 1;
                _db.WidgetMostVisitedPlaces.Add(wrp);
                _db.SaveChanges();
                return new BoolMessageItem<int>(wrp.MVPId, true, "Add successful!");  //不用激活
            }
        }
        public BoolMessageItem<int> SetVisitedSort(int mvpId, int newText)
        {
            WidgetMostVisitedPlace wrp = _db.WidgetMostVisitedPlaces.FirstOrDefault(x => x.MVPId.Equals(mvpId));
            if (wrp != null)
            {
                wrp.Sort = newText;
                _db.SaveChanges();
                return new BoolMessageItem<int>(wrp.MVPId, true, "Modified successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Modified failure!");
            }
        }
        public BoolMessageItem<int> SetVisitedIsDel(int mvpId)
        {
            WidgetMostVisitedPlace wrp = _db.WidgetMostVisitedPlaces.FirstOrDefault(x => x.MVPId.Equals(mvpId));
            if (wrp != null && wrp.StatusId != 3)
            {
                wrp.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(wrp.MVPId, true, "Deleted successfully.");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Delete failure. Please try again!");
            }
        }
        public IList<VisitedWeekItem> GetVisitedList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total)
        {
            //--IBT_IMeetGetAllVisitedList 0,'DESC','',1,1,10,0,1,-1
            IList<VisitedWeekItem> list = _db.Database.SqlQuery<VisitedWeekItem>("EXEC IBT_IMeetGetAllVisitedList {0},{1},{2},{3},{4},{5}", OrderBy, DescOrAsc, filterKeyword, userId, page, pageSize).ToList<VisitedWeekItem>();
            total = list.Count();

            if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            {
                //todo:GetMyList filterField
                return new List<VisitedWeekItem>();
            }
            else
            {
                //return (from c in list orderby c.Sort ascending select c).ToList<VisitedWeekItem>();
                return list;
            }

            //todo 下面还需要返回成员数
        }
        public IList<VisitedWeekItem> GetHomeVisitedList(string strWhere, int page, int pageSize, out int total)
        {
            string strSql = @"SELECT   ROW_NUMBER() OVER (ORDER BY dbo.WidgetMostVisitedPlaces.Sort ASC, dbo.WidgetMostVisitedPlaces.SupplyId ASC) AS row_number,
    dbo.WidgetMostVisitedPlaces.MVPId ,dbo.WidgetMostVisitedPlaces.SupplyId AS SupplyId, dbo.Supplies.Title,dbo.WidgetMostVisitedPlaces.Sort AS Score,
    dbo.Supplies.CoordsLatitude, dbo.Supplies.CoordsLongitude,dbo.supply_type(dbo.Supplies.SupplyId) AS TypesOfSupplier,3 AS typ,
    dbo.Supplies.CoverPhotoSrc,dbo.Supplies.InfoAddress,dbo.Supplies.InfoZip,dbo.Supplies.InfoPhone,
    dbo.Supplies.InfoFax,dbo.Supplies.InfoState,dbo.Supplies.InfoWebsite,dbo.WidgetMostVisitedPlaces.CreateDate,dbo.WidgetMostVisitedPlaces.UpdateDate,dbo.Supplies.CountryId,dbo.Countries.CountryName,dbo.Supplies.CityId,dbo.Cities.CityName
                    FROM    dbo.WidgetMostVisitedPlaces 
                    LEFT JOIN dbo.Supplies ON dbo.WidgetMostVisitedPlaces.SupplyId = dbo.Supplies.SupplyId
                    LEFT JOIN  dbo.Countries ON dbo.Supplies.CountryId = dbo.Countries.CountryId 
                    LEFT JOIN  dbo.Cities ON dbo.Supplies.CityId = dbo.Cities.CityId 
                    WHERE 1=1 " + strWhere.ToString() + @" AND dbo.WidgetMostVisitedPlaces.StatusId=1";


            IList<VisitedWeekItem> list = _db.Database.SqlQuery<VisitedWeekItem>(strSql).ToList<VisitedWeekItem>();
            total = list.Count();
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.Sort ascending select c).Skip(startRecord).Take(pageSize).ToList<VisitedWeekItem>();
        }
        public BoolMessageItem<int> SetWidgetVisitedSort(List<int> ObjIds)  //拖动排序
        {
            int objId = 0;
            WidgetMostVisitedPlace wrp;
            for (int i = 0; i < ObjIds.Count(); i++)
            {
                objId = ObjIds[i];
                wrp = _db.WidgetMostVisitedPlaces.FirstOrDefault(x => x.MVPId.Equals(objId) && x.StatusId.Equals(1));
                wrp.Sort = i + 1;
                _db.SaveChanges();

            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }
        public IList<WidgetMostVisitedPlace> GetHomeVisitedCheck()
        {

            var list = (from a in _db.WidgetMostVisitedPlaces where a.StatusId == 1 select a);
            return list.ToList();
        }
        #endregion

        #region Country & City Management

        #region Country
        public BoolMessageItem<int> AddNewCountry(string countryName, int score, string flagSrc)
        {
            var q = from o in _db.Countries
                    where o.CountryName.Equals(countryName)
                    select o;
            if (q.Count() > 0)
            {
                return new BoolMessageItem<int>(0, false, "Cannot repeat add!");
            }
            else
            {
                Country country = new Country();
                country.CountryName = countryName;
                country.FlagSrc = flagSrc;
                country.UpdateDate = DateTime.Now;
                country.RecommendedCount = 0;
                country.WanttoGoCount = 0;
                country.BucketListCount = 0;
                country.Sort = score;
                country.CreateDate = DateTime.Now;
                country.StatusId = 1;
                _db.Countries.Add(country);
                _db.SaveChanges();
                return new BoolMessageItem<int>(country.CountryId, true, "Add success!");
            }
        }

        public BoolMessageItem<int> EditCountry(int countryId, string countryName, int score, string flagSrc)
        {
            Country country = _db.Countries.FirstOrDefault(x => x.CountryId.Equals(countryId));
            if (country != null)
            {
                country.CountryName = countryName;
                country.FlagSrc = flagSrc;
                country.UpdateDate = DateTime.Now;
                country.Sort = score;//xg
                country.CreateDate = DateTime.Now;
                country.StatusId = 1;
                _db.SaveChanges();
                return new BoolMessageItem<int>(country.CountryId, true, "Edit success!");

            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Edit failure!");
            }
        }

        /// <summary>
        /// Country Lists 的数据
        /// </summary>
        /// <returns></returns>
        public IList<CountryAllListItem> GetCountryList(int OrderBy, string DescOrAsc, string filterKeyword, int userId, string strWhere, string filterLetter, int page, int pageSize, out int total)
        {
            //--IBT_IMeetGetAllCountryList 0,'ASC','',1,'','',1,10,0,1,-1
            IList<CountryAllListItem> list = _db.Database.SqlQuery<CountryAllListItem>("EXEC IBT_IMeetGetAllCountryList {0},{1},{2},{3},{4},{5},{6},{7}", OrderBy, DescOrAsc, filterKeyword, userId, strWhere, filterLetter, page, pageSize).ToList<CountryAllListItem>();
            total = list.Count();
            return list;

        }
        public BoolMessageItem<int> SetCountrySort(int countryId, int newText) //xg
        {
            Country country = _db.Countries.FirstOrDefault(x => x.CountryId.Equals(countryId));
            if (country != null)
            {
                country.Sort = newText;
                _db.SaveChanges();
                return new BoolMessageItem<int>(country.CountryId, true, "Modified successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Modified failure!");
            }
        }
        /// <summary>
        /// 把国家设置为删除状态同时相应国家的城市也设置为删除
        /// </summary>
        /// <param name="countryId">国家Id</param>
        /// <returns></returns>
        public BoolMessageItem<int> SetCountryIsDel(int countryId)
        {
            Country country = _db.Countries.FirstOrDefault(x => x.CountryId.Equals(countryId));
            if (country != null && country.StatusId != 3)
            {
                //删除国家时把相应国家下的城市也设置为删除
                List<City> cityList = _db.Cities.Where(c => c.CountryId == country.CountryId).ToList();
                cityList.ForEach(c => c.StatusId = 3);
                country.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(country.CountryId, true, "Deleted successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Delete failure!");
            }
        }
        #endregion

        #region City
        public BoolMessageItem<int> AddNewCity(int countryId, string cityName, int score, string direction)
        {
            var q = from o in _db.Cities
                    where o.CityName.Equals(cityName) && o.CountryId.Equals(countryId)
                    select o;
            if (q.Count() > 0)
            {
                return new BoolMessageItem<int>(0, false, "Cannot repeat add!");
            }
            else
            {
                City city = new City();
                city.CityName = cityName;
                city.Direction = "";
                city.UpdateDate = DateTime.Now;
                city.RecommendedCount = 0;
                city.WanttoGoCount = 0;
                city.BucketListCount = 0;
                city.CreateDate = DateTime.Now;
                city.Sort = score;//xg
                city.StatusId = 1;
                _db.Cities.Add(city);
                _db.SaveChanges();
                return new BoolMessageItem<int>(city.CityId, true, "Add successful!");  //不用激活
            }
        }

        public BoolMessageItem<int> EditCity(int cityId, int countryId, string cityName, int score, string direction)
        {
            City city = _db.Cities.FirstOrDefault(x => x.CityId.Equals(cityId));
            if (city != null)
            {
                city.CountryId = countryId;
                city.CityName = cityName;
                city.Direction = "";
                city.UpdateDate = DateTime.Now;
                city.Sort = score;//xg
                city.CreateDate = DateTime.Now;
                city.StatusId = 1;
                _db.SaveChanges();
                return new BoolMessageItem<int>(city.CityId, true, "Edit successful!");

            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Edit failure!");
            }
        }

        /// <summary>
        /// City Lists 的数据
        /// </summary>
        /// <returns></returns>
        public IList<CityAllListItem> GetCityList(int OrderBy, string DescOrAsc, string filterKeyword, int userId, string strWhere, string filterLetter, int page, int pageSize, out int total)
        {
            //--IBT_IMeetGetAllCountryList 0,'ASC','',1,'','',1,10,0,1,-1
            IList<CityAllListItem> list = _db.Database.SqlQuery<CityAllListItem>("EXEC IBT_IMeetGetAllCityList {0},{1},{2},{3},{4},{5},{6},{7}", OrderBy, DescOrAsc, filterKeyword, userId, strWhere, filterLetter, page, pageSize).ToList<CityAllListItem>();
            total = list.Count();
            return list;

        }
        public BoolMessageItem<int> SetCitySort(int cityId, int newText) //xg
        {
            City city = _db.Cities.FirstOrDefault(x => x.CityId.Equals(cityId));
            if (city != null)
            {
                city.Sort = newText;
                _db.SaveChanges();
                return new BoolMessageItem<int>(city.CityId, true, "Modified successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Modified failure!");
            }
            //RRWB rrwb = _db.RRWBs.FirstOrDefault(x => x.ObjId.Equals(cityId) && x.IBTType == 2);
            //if (rrwb != null)
            //{
            //    rrwb.Rating = Utils.IntToByte(newText);
            //    _db.SaveChanges();
            //    return new BoolMessageItem<int>(rrwb.RRWBId, true, "Modified successful!");
            //}
            //else
            //{
            //    return new BoolMessageItem<int>(0, false, "Modified failure!");
            //}
        }
        public BoolMessageItem<int> SetCityIsDel(int cityId)
        {
            City city = _db.Cities.FirstOrDefault(x => x.CityId.Equals(cityId));
            if (city != null && city.StatusId != 3)
            {
                city.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(city.CityId, true, "Deleted successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Delete failure!");
            }
        }
        #endregion

        #endregion

        #region Country & City List
        /// <summary>
        /// Country & City List
        /// </summary>
        /// <returns></returns>
        public List<WidgetCountryManageService> GetCountryManageList(out int pageCount, out int rowCount, int page, int pageSize)
        {
            pageCount = 0;
            rowCount = 0;
            var outParam = new SqlParameter();
            var outParam1 = new SqlParameter();
            outParam.ParameterName = "rowCount";
            outParam1.ParameterName = "pageCount";
            //outParam.ParameterName = "pageCount";
            outParam.SqlDbType = SqlDbType.Int;
            outParam.Direction = ParameterDirection.Output;
            outParam1.SqlDbType = SqlDbType.Int;
            outParam1.Direction = ParameterDirection.Output;
            var data = _db.Database.SqlQuery<WidgetCountryManageService>("EXEC IBT_IMeetWidgetIBTData @page, @pageSize, @rowCount OUT, @pageCount OUT",
                           new SqlParameter("page", page),
                           new SqlParameter("pageSize", pageSize),
                           outParam,
                           outParam1);
            var result = data.ToList();
            rowCount = (int)outParam.Value;
            pageCount = (int)outParam1.Value;
            return result;
        }

        public List<WidgetCountryManageService> GetCountryList()
        {
            string sql = @"select c.CreateDate,c.CountryName from WidgetIBT w 
                         left join Countries c on w.ObjId=c.CountryId 
                         where w.IBTType=1";
            return _db.Database.SqlQuery<WidgetCountryManageService>(sql).ToList<WidgetCountryManageService>();
        }

        #endregion


        public List<AddWidgetCountry> GetCountryListForAddAfter(string txtValue, out int pageCount, out int rowCount, int page, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
