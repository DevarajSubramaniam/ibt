﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMeet.IBT.Common;
using IMeet.IBT.DAL;

namespace IMeet.IBT.Services
{
    public interface IProfileService
    {
        /// <summary>
        /// 获取当前登录的ID
        /// </summary>
        /// <returns></returns>
        int GetUserId();
        /// <summary>
        /// 获取总的会员数
        /// </summary>
        /// <returns></returns>
        int GetAllMemberCount();
        /// <summary>
        /// 根据用户ID返回用户的资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Profile GetUserProfile(int userId);
        User GetUserInfo(int userId);
        User GetUserNewEMailVerification(string token);
        DAL.User GetBindUser(string email, string password);
        int GetUserCount(string email);
          /// <summary>
        /// 获取用户头像
        /// </summary>
        /// <param name="avatarSize">avatarSize:100X100,75X75,55X55,50X50,36X36,26X26</param>
        /// <returns></returns>
        string GetUserAvatar(int userId, int avatarSize);
        string GetUserFeedAvatar(int userId, int avatarSize);
        string GetUserCutAvatar(int userId, int avatarSize);
        /// <summary>
        /// 根据用户ID返回用户的好友资料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        Connection GetUserConnection(int userId,int friendId);
        /// <summary>
        /// 根据用户ID获取信息数量
        /// </summary>
        /// <returns></returns>
        int GetMsgCount(int userId);
        int GetReplyCount(int msgId, int userId);
         /// <summary>
        /// 根据用户ID获取好友数量
        /// </summary>
        /// <returns></returns>
        int GetConnectionCount(int userId);
        /// <summary>
        /// 根据用户ID获取好友请求数量
        /// </summary>
        /// <returns></returns>
        int GetRequestCount(int userId);
        int GetFriendCount(int userId, int friendId);
        int GetTotalCount(int userId);
         /// <summary>
        /// 根据国家ID返回国家的资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        string GetUserCountry(int userId,int countryId);
         /// <summary>
        /// 根据城市ID返回城市的资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        string GetUserCity(int userId,int cityId);
        /// <summary>
        /// 激活用户TravelBadge
        /// </summary>
        /// <param name="userid">userid</param>
        /// <returns></returns>
        BoolMessage SetUserActivateTravelBadge(int userid);
        /// <summary>
        /// send email to user who isactive TravelBadge
        /// </summary>
        /// <returns></returns>
        BoolMessage SendEmailToIsActive();
        /// <summary>
        /// send emai to auto active
        /// </summary>
        void SendEmailToAutoActive();


        #region MemberList(MemberManage)
        int GetMemberListCount(string strWhere);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<MemberListItem> GetMemberList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string searchKeyword, string filterField, out int total);
        BoolMessageItem<int> SetMemberSort(int userId, int newText);
        #endregion

        /// <summary>
        /// 查找profiles
        /// </summary>
        /// <param name="total"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="keyword">
        List<Profile> GetProfileList(out int total, int page = 0, int pageSize = 0, string keyword = "");
    }
}
