﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public interface ISNSConnectService
    {
        /// <summary>
        /// 根据用户ID获取用户绑定了那些sns
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<SnsConnect> GetSNSConnectList(int userId);

        /// <summary>
        /// 根据userid,typeId返回相关信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        SnsConnect GetSNSConnect(int userId, Identifier.SnsType snsTypeId);
        
        /// <summary>
        /// 根据snsUid,typeId返回相关信息
        /// </summary>
        /// <param name="snsUid"></param>
        /// <param name="snsTypeId"></param>
        /// <returns></returns>
        SnsConnect GetSNSConnect(string snsUid, Identifier.SnsType snsTypeId);

        /// <summary>
        /// 根据accessToken,typeId返回相关信息
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="snsTypeId"></param>
        /// <returns></returns>
        SnsConnect GetSNSConnectByAccessTokenToken(string accessToken, Identifier.SnsType snsTypeId);
        /// <summary>
        /// 新加/绑定
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="snsId"></param>
        /// <param name="snsTypeId"></param>
        /// <param name="token"></param>
        /// <param name="tokenSecret"></param>
        void AddSNSConnect(int userId, string snsId, int snsTypeId, string token, string tokenSecret);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="connect"></param>
        void UpdateSNSConnect(SnsConnect connect);

                /// <summary>
        /// 取消绑定（删除）
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="snsTypeId"></param>
        void DeleteSNSConnect(int userId, int snsTypeId);

        /// <summary>
        /// 根据sns的ID返回ibt的用户ID
        /// </summary>
        /// <param name="snsIds"></param>
        /// <returns></returns>
        Dictionary<string, int> GetUserIdBySnsId(List<string> snsIds, int snsTypeId);
    }
}
