﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.Services
{
    public interface IHelloService
    {
        string SayHello(string name);
    }
}
