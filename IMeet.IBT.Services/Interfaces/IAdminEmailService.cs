﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.DAL.Models;

namespace IMeet.IBT.Services
{
    public interface IAdminEmailService
    {
        Email Find(int id);
        Email Add(Email email);
        Email Update(Email email, int id);
        List<Email> GetEmailList(int page, int pageSize, out int total);
        List<Email> GetSurveyList(int page, int pageSize, out int total);
        List<Email> GetNewEmailList(int page, int pageSize, out int total);//EDM
        List<Email> GetNewSurveyList(int page, int pageSize, out int total);
        bool Del(int id);
        bool AddEmailSendLog(int emailId);
        /// <summary>
        /// 开启多线程添加EmailLog
        /// </summary>
        /// <param name="emailId"></param>
        void AddEmailSendLogTreading(int emailId);
        void SendEmail(int digit);
        bool AdminEmailSendForTest(int userId, string senderEmail, string subject, string content, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB);
        bool AdminSurveySendForTest(int userId, string senderEmail, string subject, string content1, string suggestedList, string content2, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB);
        bool NewAdminEmailSendForTest(int userId, string senderEmail, string subject, string content, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB);
        bool NewAdminSurveySendForTest(int userId, string senderEmail, string subject, string content1, string suggestedList, string content2, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB);

    }
}
