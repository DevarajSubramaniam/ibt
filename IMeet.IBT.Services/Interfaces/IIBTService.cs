﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.DAL.Models;
using System.Data.SqlClient;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;

namespace IMeet.IBT.Services
{
    public interface IIBTService
    {
        #region Country and City

        /// <summary>
        /// get country by ID
        /// </summary>
        /// <param name="countryId">countryID</param>
        /// <returns></returns>
        Country GetCountry(int countryId);
        /// <summary>
        /// 获取City
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        City GetCity(int cityId);
        /// <summary>
        /// get country descript by countryId(cache)
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        CountriesDe GetCountryDes(int countryId);
        /// <summary>
        /// get country descript by countryId
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        CountriesDe GetCountryDesInfo(int countryId);
        /// <summary>
        /// 新增或修改国家跟国家的description
        /// </summary>
        /// <param name="country"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        BoolMessageItem<int> EditCountry(Country country, CountriesDe description);
        /// <summary>
        /// 新增或修改城市跟城市的description
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        BoolMessageItem<int> EditCity(City city);
        /// <summary>
        /// 导入城市description
        /// </summary>
        /// <param name="fs"></param>
        /// <returns></returns>
        BoolMessage ImportCityDescript(FileStream fs);
        /// <summary>
        /// 获取国家的城市数量（cache）
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        int CityCount(int countryId);
        /// <summary>
        /// 获取国家的supply数量（cache）
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        int SuppliersCount(int countryId);
        /// <summary>
        /// 获取国家的Member数量
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        int MembersCount(int countryId);
        /// <summary>
        /// 获取城市的supply数量
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        int CountSupplierForCity(int cityId);
        /// <summary>
        /// 获取城市的member数量
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        int CountMembersCountForCity(int cityId);
        /// <summary>
        /// 获取ibt friends 数量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        int IBTFriendsCount(int userId, int countryId, int cityId, int supplyId);
        /// <summary>
        /// 获取ibt 数量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        int IBTCount(int userId, int countryId, int cityId, int supplyId);
        /// <summary>
        /// 获取ibt people数量
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        int IBTPeopleCount(int countryId, int cityId, int supplyId);
        /// <summary>
        /// 获取用户been there次数
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        int GetHistoryCount(int tabId, int ibtType, int objId, int userId);
        /// <summary>
        /// 获取city的specials
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<Media> GetSpecialsOfCity(int cityId, int pageNumber, int pageSize, out int total);

        #endregion

        #region rrwb
        /// <summary>
        /// 获取rrwb信息
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="objId">country、city、supply id</param>
        /// <param name="ibtType">ibttype</param>
        /// <returns></returns>
        RRWB GetRRWB(int userId, int objId, int ibtType);
        #endregion

        #region Attachment(图片)
        /// <summary>
        /// 设置Attachment为删除状态
        /// </summary>
        /// <param name="attId"></param>
        /// <returns></returns>
        BoolMessage AttachmentDel(int attId);
        /// <summary>
        /// 根据post visit的附件删除newsfeed的附件
        /// </summary>
        /// <param name="pvAttId"></param>
        BoolMessage AttachmentDelForPVAttID(int pvAttId);
        /// <summary>
        /// change attachment statusid
        /// </summary>
        /// <param name="attId">attId</param>
        /// <param name="statusId">statusId</param>
        /// <returns></returns>
        BoolMessage AttachmentChange(int attId, int statusId);
        /// <summary>
        /// 根据guid更新attachment的objId
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="objId"></param>
        void SetAttachmentObjId(string guid, int objId);
        /// <summary>
        /// 根据attid更新attachment的objid
        /// </summary>
        /// <param name="attid"></param>
        /// <param name="objId"></param>
        void SetAttachmentObjIdByAttId(string attid, int objId);
        /// <summary>
        /// 根据用户ID获取附件
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="attType"></param>
        /// <returns></returns>
        IList<Attachment> GetAttachmentList(int userId, int attType);
        /// <summary>
        /// 根据id获取attachment
        /// </summary>
        /// <param name="attid"></param>
        /// <returns></returns>
        Attachment GetAttachmentByAttid(int attid);
        /// <summary>
        /// 获取attachment list(分页)
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<Attachment> GetAttachmentListByPage(string strWhere, int page, int pageSize);
        /// <summary>
        /// 添加attachment
        /// </summary>
        /// <param name="att"></param>
        void AddAttachment(Attachment att);
        /// <summary>
        /// newsFeed上传的图片处理
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="attType"></param>
        /// <param name="guid"></param>
        /// <param name="orgFilePath"></param>
        /// <returns></returns>
        BoolMessageItem<int> AttachmentNewsFeedImage(int userId, int attType, string guid, string orgFilePath);
        /// <summary>
        /// 图片翻转
        /// </summary>
        /// <param name="userId">userid</param>
        /// <param name="attType"></param>
        /// <param name="id">attachment id</param>
        /// <param name="rotate">翻转角度（1:90,2:180,3:270）</param>
        /// <returns></returns>
        BoolMessageItem<string> AttachmentRotate(int userId, int id, int rotate);

        /// <summary>
        /// post visit图片旋转
        /// </summary>
        /// <param name="attId"></param>
        /// <param name="rotate">翻转角度（1:90,2:180,3:270）</param>
        /// <returns></returns>
        BoolMessageItem<string> PVImageRotate(int attId, int rotate, string basePath);
        #endregion

        #region newsfeed
        /// <summary>
        /// get newsfeed infomation by id
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <returns></returns>
        NewsFeed GetNewsFeedInfo(int newsFeedId);
        /// <summary>
        /// get feedlist pages
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <param name="ibtType"></param>
        /// <param name="dt"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<FeedList> GetFeedList(int countryId, int cityId, int supplyId, int ibtType, DateTime dt, int pageIndex, int pageSize);
        /// <summary>
        /// 加载feedlist的Attachment
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        IList<FeedList> GetFeedListAttachment(List<FeedList> list);
        /// <summary>
        /// 获取Feedcomment
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<FeedcommentsEx> GetFeedcommentEx(int newsFeedId, int pageIndex, int pageSize);
        #endregion
    }
}
