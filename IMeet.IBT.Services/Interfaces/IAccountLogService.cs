﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.Services
{
    public interface IAccountLogService
    {
        /// <summary>
        /// 写IBT用户登录日志
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        void Write(string userName, string passWord, bool validateSuccess, int bmiItem);

        /// <summary>
        /// 写sns登录日志
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="snsType"></param>
        void WriteSns(int userId, Identifier.SnsType snsType);
    }
}
