﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public interface IAccountService
    {
        BoolMessageItem<int> ValidateUser(string userName, string password);
        BoolMessageItem<int> ValidateAdminUser(string userName, string password);
        void SignIn(int userId, bool createPersistentCookie);
        void SignOut();
        BoolMessageItem<int> CreateUser(string firstname, string lastname, string password, string email, int countryId, int cityId, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther, int userType, string source, bool isPleasure, bool isBusiness);
        BoolMessageItem<int> JudgeUser(string email, string password);
        BoolMessageItem<int> UpdateProfile(int userId, string username, string firstname, string lastname, DateTime birth, string email, string newemail, string altemail, int gender, string statusUpdate, int countryId, int cityId, int usertype, bool isPleasure, bool isBusiness, bool isEmailSubscribe);
        BoolMessageItem<int> UpdateAvatar(int userId, string ProfileImage, string sourceFile, string destinationFile, string objData);
        void SetAvatarNewsfeed(string sourceFile, string destinationFile);
        BoolMessageItem<int> SendPassword(string email, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther);
        BoolMessageItem<int> ChangePassword(int userId, string oldpassword, string newpassword);
        BoolMessageItem<int> ChangeUserAndProfileEmail(int userId, string newemail);
        BoolMessageItem<int> SetIsApproved(int userId);
        BoolMessageItem<int> SetIsFeatured(int userId);
        BoolMessageItem<int> SetIsDel(int userId);
        BoolMessageItem<int> EmailUnsubscribe(int userId);
        DAL.User GetUserByEmail(string email);
        BoolMessageItem<int> UpdateAccountSeeWorld(int userid, int type, int value);
    }
}
