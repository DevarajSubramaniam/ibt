﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public interface INewsFeedService
    {
        #region NewsFeed
        /// <summary>
        /// Add newsfeed 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <param name="ibtType"></param>
        /// <param name="activeType"></param>
        /// <param name="objVal"></param>
        /// <param name="objData"></param>
        /// <returns></returns>
        BoolMessageItem<int> AddNewsFeed(int userId, int countryId, int cityId, int supplyId, Identifier.IBTType ibtType, Identifier.FeedActiveType activeType, string objVal, string objData, DateTime? pvDate);
        BoolMessageItem<int> AddNewsFeed(int userId, int countryId, int cityId, int supplyId, int ibtType, Identifier.FeedActiveType activeType, string objVal, string objData, DateTime? pvDate);
        /// <summary>
        /// Edit newsfeed 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <param name="ibtType"></param>
        /// <param name="activeType"></param>
        /// <param name="objVal"></param>
        /// <param name="objData"></param>
        /// <returns></returns>
        BoolMessageItem<int> EditNewsFeed(int newsfeedId, string objVal, string objData, DateTime? pvDate);
        BoolMessageItem<int> NewsFeedIsDel(int newsfeedId);
        IList<FeedList> GetFeedHistoryList(string strWhere, int userId, out int total);
        /// <summary>
        /// Profile feedlist
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<FeedList> GetNewsFeedList(int userId, int page, int pageSize, out int total);

        /// <summary>
        /// My travel List
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tabId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<FeedList> GetMyTravelList(int userId, int tabId, int page, int pageSize, out int total);
        NewsFeed GetNewsFeedInfo(int newsfeedId);
        #endregion

        #region FeedComments
        /// <summary>
        /// feed添加评论
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        /// <remarks>同时更新评论数</remarks>
        BoolMessageItem<int> AddFeedComments(int newsFeedId, int userId, string comments);
        /// <summary>
        /// 获取指定的feed的评论列表
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<FeedcommentsEx> GetFeedCommentList(int newsFeedId, int page, int pageSize);
        /// <summary>
        /// 获取某个newsfeed的评论总数
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <returns></returns>
        int GetFeedcommentcount(int newsFeedId);
        NewsFeed GetNewsFeedByNewsFeedId(int newsFeedId);
        #endregion

        #region 获取feed的年月
        IList<FeedItem> GetFeedDateList(int userId, int ibtType);
        IList<FeedItem> GetServicesDate(int userId, int ibtType);
        #endregion
    }
}
