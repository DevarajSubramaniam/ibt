﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public interface ISearchService
    {
        #region Search of List
        //Linq
        int GetSearchResultListCount(string strWhere);
        int GetSearchMemberResultCount(string strWhere);
        int GetSearchSupplyResultCount(string strWhere);

        IList<MemberListItem> GetMemberSearchList(int topN, string baseSearch);
        //IList<SupplierListItem> GetSupplierSearchList(string keyword);
        IList<SupplierListItem> GetSupplierSearchList(int topN, string baseSearch);
        List<SupplyListShort> GetSupplySearchListing(int userid, int pageIndex, int pageSize, string attDataName, string filter, string keyWord, out int outTotal);
        IList<CountryListItem> GetCountrySearchList(int topN, int userId, string baseSearch);
        List<CountryListItem> GetCountrySearchListing(int userid, int index, int pageSize, string filter, string keyWork, out int total);
        IList<CityListItem> GetCitySearchList(int topN, int userId, string baseSearch);
        IList<CityListItem> GetCityResultList(int userId, int page, int pageSize, string filterLetter, string filterKeyword, out int total);
        List<CityListItem> GetCitySearchListing(int userid, int index, int pageSize, string filter, string keyWork, out int total);
        IList<CountryListItem> GetBrowseList(int userId, int page, int pageSize, string strWhere, string filterLetter, string filterKeyword, out int total);
        //SQL
        int GetCountryfilterCount(int userId, string strWhere, string filterLetter, string keyWord);
        int GetCityfilterCount(int userId, string filterLetter, string keyWord);
        int GetAllSupplierCount(int userId, string keyword);
        int GetAllMemberCount(string keyword);
        /// <summary>
        /// GetSearchList
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<MemberListItem> GetSearchMemberAllList(int page, int pageSize, string SearchKeyword, string filterLetter, string filterField, out int total);
        IList<SupplyListShort> GetSearchSupplierAllList(int userId, int page, int pageSize, string SearchKeyword, string filterLetter, out int total);
        IList<AttributeData> GetAttributeDataList();
        Dictionary<string, string> GetAttributeDataItem();

        #region Advanced Search
        int GetCountryAdvancedSearchCount(int userId, string strWhere, string filterLetter);
        IList<CityListItem> GetCityAdvancedSearch(int userId, string strWhere, string filterLetter, int page, int pageSize, out int total);
        IList<SupplyListShort> GetSupplyAdvancedSearch(int userId, string strWhere, int page, int pageSize, out int total);
        List<SupplyListShort> GetSupplyAdvancedSearch(int userId, int countryId, int cityId, int sortby, string filterLetter, string cateIds, int page, int pageSize, out int total);
        IList<SupplyListShort> GetSupplyAdvancedSearchByPage(int userId, string strWhere, string strOrderby, int page, int pageSize, out int total);
        IList<SupplyListShort> GetRecommendAdvancedSearchByPage(int userId, string strWhere, string strOrderby, int page, int pageSize, out int total);

        /// <summary>
        /// City Search Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="strWhere"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        IList<CityListItem> GetCitySearchtList(int userId, int page, int pageSize, string filterLetter, string strWhere, out int total);
        IList<MemberListItem> GetMemberAdvancedSearch(string strWhere, int page, int pageSize, out int total);
        #endregion

        #endregion

    }
}
