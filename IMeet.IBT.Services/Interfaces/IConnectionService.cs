﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using IMeet.IBT.Common;
using IMeet.IBT.DAL;

namespace IMeet.IBT.Services
{
    public interface IConnectionService
    {
        BoolMessageItem<int> InviteFriend(List<string> emailList, string msg, string firstname, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther);
      

        #region MessageList
        IList<Messge> GetMsgList(int msgId, int toUserId, int fromUserId);
        Messge GetUserMsg(int toUserId, int fromUserId);
        BoolMessageItem<int> SetMsgRead(int msgId);
        BoolMessageItem<int> SetIsRead(int msgId);
        BoolMessageItem<int> SetAllRead(List<int> MsgDataId);
        BoolMessageItem<int> SetIsDel(int msgId);
        BoolMessageItem<int> SetAllDel(List<int> MsgDataId);
        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="fromUserId"></param>
        /// <param name="toUserId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        BoolMessage SendMessage(int fromUserId, int toUserId, string comments);
        /// <summary>
        /// Reply
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="fromUserId"></param>
        /// <param name="toUserId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        BoolMessage SendReply(int msgId, int fromUserId, int toUserId, string comments);
        int GetMessageListCount(int userId);
        /// <summary>
        /// GetMessageList
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="IsDel"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <returns></returns>
        IList<MsgListItem> GetMessageList(int userId, bool IsDel, string strWhere, int page, int pageSize, string filterKeyword, string filterField, out int total);
        /// <summary>
        /// GetReplyList
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="IsDel"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<MsgListItem> GetReplyList(int msgId, bool IsDel, out int total);
        #endregion
              
        #region ConnectionList
        List<Profile> GetAllConnection(int UserId);
        BoolMessageItem<int> RemoveConnection(int UserId, int friendId);
        BoolMessageItem<int> SendRequest(int UserId, int friendId);
        BoolMessageItem<int> CreateConnection(int UserId, int friendId);
        BoolMessageItem<int> IgnoreConnection(int userId, int friendId);
        IList<Connection> GetConList(int userId, int statusId);

        int GetConnectionListCount(int userId, int statusId);
        /// <summary>
        /// GetConnectionList
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<ConnectionListItem> GetConnectionList(int userId, int statusId,int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total);
        IList<ConnectionListItem> GetRequestList(int userId, int statusId, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total);
        IList<ConnectionListItem> GetAllConnectionProfile(int userId, string name);  //xg
        #endregion
    }
}
