﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.DAL.Models;

namespace IMeet.IBT.Services
{
    public interface IWidgetService
    {
        #region Widget Statistics

        #region 统计出现次数
        BoolMessageItem<int> InitWidgetAriseCount(int mediaId, string url);
        #endregion

        #region 统计 点击数、浏览数
        BoolMessageItem<int> StatisticsWidgetHit(int mediaId, string visitUrl);      
        #endregion

        #endregion

        #region Widget Media(Ads/Special)
        IList<Media> GetWidgetMediaIBT(int count, Identifier.PositionTypes Position, Identifier.MediaTypes MediaType);
        IList<Media> GetWidgetOfPosition(int count, Identifier.PositionTypes Position, string localUrl);
        #endregion

        #region IBT
        IList<WidgetIBT> GetWidgetRecommonIBT(Identifier.IBTType ibtType);
        IList<WidgetIBT> GetWidgetAllCountryIBT(string firstLetter);
        #endregion

        #region Destination of the Week
        IList<DestinationWeekItem> GetWidgetDestinationWeekList(int userId,int topN);
        IList<SupplyHomeListShort> GetMostRecommendedPlaces(int userId,int topN);
        BoolMessageItem<int> AddNewDestination(int supplyId, int objType, int userId, string desp, string imgsrc);
        BoolMessageItem<int> EditDestination(int dWeekId, int sort, int supplyId, int userId, string desp, string imgsrc);
        #endregion

        #region DestinationList(Home Page Management)
        int GetDestinationListCount(string strWhere);
        DAL.WidgetDWeek GetWidgetDWeekInfo(int dWeekId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<DestinationWeekItem> GetDestinationList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total);
        BoolMessageItem<int> SetDestinationSort(int dweekId, int newText);
        BoolMessageItem<int> SetDestinationIsDel(int dweekId);
        BoolMessageItem<int> SetWidgetDestinationSort(List<int> ObjIds);
        #endregion

        #region RecommendList(Home Page Management)
        BoolMessageItem<int> AddNewRecommend(int ObjId, int objType, int scroe, int userId);
        IList<RecommendWeekItem> GetRecommendList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total);
        IList<RecommendWeekItem> GetHomeRecommendList(string strWhere, int page, int pageSize, out int total);
        IList<WidgetMostRecommentdPlace> GetHomeRecommendList();
        BoolMessageItem<int> SetRecommendSort(int mrpId, int newText);
        BoolMessageItem<int> SetRecommendIsDel(int mrpId);
        BoolMessageItem<int> SetWidgetRecommendSort(List<int> ObjIds);
        #endregion

        #region VisitedList(Home Page Management)
        BoolMessageItem<int> AddNewVisited(int ObjId,int objType, int score, int userId);
        IList<VisitedWeekItem> GetVisitedList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total);
        IList<VisitedWeekItem> GetHomeVisitedList(string strWhere, int page, int pageSize, out int total);
        IList<WidgetMostVisitedPlace> GetHomeVisitedCheck();
        BoolMessageItem<int> SetVisitedSort(int mvpId, int newText);
        BoolMessageItem<int> SetVisitedIsDel(int mvpId);
        BoolMessageItem<int> SetWidgetVisitedSort(List<int> ObjIds);
        #endregion

        #region  Country & City Management
        BoolMessageItem<int> AddNewCountry(string countryName, int score, string flagSrc);
        BoolMessageItem<int> EditCountry(int countryId, string countryName, int score, string flagSrc);
        IList<CountryAllListItem> GetCountryList(int OrderBy, string DescOrAsc, string filterKeyword, int userId, string strWhere, string filterLetter, int page, int pageSize, out int total);
        BoolMessageItem<int> SetCountrySort(int countryId, int newText);
        BoolMessageItem<int> SetCountryIsDel(int countryId);

        BoolMessageItem<int> AddNewCity(int countryId, string cityName, int score, string direction);
        BoolMessageItem<int> EditCity(int cityId, int countryId, string cityName, int score, string direction);
        IList<CityAllListItem> GetCityList(int OrderBy, string DescOrAsc, string filterKeyword, int userId,string strWhere, string filterLetter, int page, int pageSize, out int total);
        BoolMessageItem<int> SetCitySort(int cityId, int newText); //xg
        /// <summary>
        /// 把国家设置为删除状态同时相应国家的城市也设置为删除
        /// </summary>
        /// <param name="countryId">国家Id</param>
        /// <returns></returns>
        BoolMessageItem<int> SetCityIsDel(int cityId);
        #endregion

        #region Country & City List
        /// <summary>
        /// Country & City List
        /// </summary>
        /// <returns></returns>
        List<WidgetCountryManageService> GetCountryManageList(out int pageCount, out int rowCount, int page, int pageSize);


        List<AddWidgetCountry> GetCountryListForAddAfter(string txtValue, out int pageCount, out int rowCount,int page,int pageSize);
        #endregion

    }
}
