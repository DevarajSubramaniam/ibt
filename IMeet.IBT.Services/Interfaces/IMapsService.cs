﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMeet.IBT.Common;
using IMeet.IBT.DAL;

namespace IMeet.IBT.Services
{
    public interface IMapsService
    {
        #region 获取坐标的

        /// <summary>
        /// 国家de坐标
        /// </summary>
        /// <param name="countryId">如果是0就是给所有没坐标设定坐标，如果大于0则给指定的更新坐标</param>
        void GeoCountry(int countryId = 0);
        /// <summary>
        /// 城市de坐标
        /// </summary>
        /// <param name="cityId">如果是0就是给所有没坐标设定坐标，如果大于0则给指定的更新坐标</param>
        void GeoCity(int cityId);
        /// <summary>
        /// supply de 坐标
        /// </summary>
        /// <param name="supplyId">如果是0就是给所有没坐标设定坐标，如果大于0则给指定的更新坐标</param>
        void GeoSupply(int supplyId);

        #endregion

        #region Profile Maps
        /// <summary>
        /// 获取用户的最近一条记录的地图信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        MapsItem ProfileFirstItem(int userId);
        /// <summary>
        /// 根据坐标获取用户的相关数据(Profile page)
        /// </summary>
        /// <param name="ibtType">1:country,2:ctiy,3:supply</param>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <returns></returns>
        IList<MapsItem> GetProfileMapsList(Identifier.IBTType ibtType, int userId, decimal coordsLatitude, decimal coordsLongitude);

        /// <summary>
        /// Profile 的map infobox 的内容
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        MapsProfileViewItem GetProfileSupplyInfo(int userId, int supplyId);
        #endregion

        #region  myList Maps

        /// <summary>
        /// 获取用户的最近一条记录的地图信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        MapsItem FirstMyListItem(int userId);

        /// <summary>
        /// 根据坐标获取用户的相关数据(Mylist page)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <returns></returns>
        IList<MapsItem> GetMyListMapsList(int userId, decimal coordsLatitude, decimal coordsLongitude);

        /// <summary>
        /// 根据坐标获取用户的相关数据(Mylist page)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <param name="ibttype"></param>
        /// <returns></returns>
        IList<MapsItem> GetMyListMapsList(int userId, decimal coordsLatitude, decimal coordsLongitude, Identifier.IBTType ibttype);

        /// <summary>
        /// mylist maps supply info
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        MapsMyListViewItem GetMyListSupplyInfo(int supplyId, int userId);

        #endregion

        #region index maps
        /// <summary>
        /// 获取首页的列表
        /// </summary>
        /// <returns></returns>
        IList<MapsItem> GetIndexTopList();

        /// <summary>
        /// 首页地图box的信息
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        MapsIndexViewItem GetIndexSupplyInfo(int objId, int objType);

        #endregion

        #region Add Travels
        //先只列出国家与城市列表

        /// <summary>
        /// 获取用户的最近一条记录的地图信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        MapsItem FirstAddTravelsItem(int userId);

        /// <summary>
        /// 根据坐标获取用户的相关数据(add travels step 4 page) ---IBT table
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <returns></returns>
        IList<MapsItem> GetAddTravelsMapsList(Identifier.IBTType ibtType, int userId, decimal coordsLatitude, decimal coordsLongitude);

        /// <summary>
        /// 根据Ids 返回相关相关的MapsItems
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        IList<MapsItem> GetAddTravelsNewList(Identifier.IBTType ibtType, string ids);

        #endregion

    }
}
