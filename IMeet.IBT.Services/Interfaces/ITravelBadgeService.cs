﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.DAL.Models;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public interface ITravelBadgeService
    {
        /// <summary>
        /// 获取用户BonusPoint信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        UserBonusPoint GetUserBonusPoint(User user);
        /// <summary>
        /// 获取用户的SpecialPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        List<SpecialPoint> GetUserSpecialPoint(User user);
        /// <summary>
        /// 获取用户的PermanentPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        UserPermanentPoint GetUserPermanentPoint(User user);
        /// <summary>
        /// 获取当前登录用户的TravelBadge信息
        /// </summary>
        /// <returns></returns>
        UserTravelBadge GetUserTravelbadge();
        /// <summary>
        /// 获取用户的TravelBadge信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        UserTravelBadge GetUserTravelbadge(User user);
        /// <summary>
        /// 获取用户的PointSummary信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        UserPointSummaryByYear GetUserPointSummaryByYear(User user);
        /// <summary>
        /// 获取用户每年的PermanentPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        List<UserPermanentPoint> GetUserPermanentPointByYear(User user);
        /// <summary>
        /// 获取用户每年的BonusPoint信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        List<UserBonusPoint> GetUserBonusPointByYear(User user);
        /// <summary>
        /// 获取用户每年的SpecialPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        List<UserSpecialPointByYear> GetUserSpecialPointByYear(User user);
        /// <summary>
        /// insert meetPlan
        /// </summary>
        /// <param name="meetingPlan"></param>
        void InsertMeetingPlan(MeetingPlan meetingPlan);
        /// <summary>
        /// get the badge specials
        /// </summary>
        List<BadgeSpecial> GetBadgeSpecials();
        /// <summary>
        /// get the badge special
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BadgeSpecial GetBadgeSpecial(int id);
        /// <summary>
        /// 获取用户的badgespecial
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        List<UserBadgeSpecial> GetUserBadgeSpecial(User user);
        /// <summary>
        /// 获取用户的badgespecials 按分页
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="total"></param>
        /// <param name="orderby"></param>
        /// <param name="sortord"></param>
        /// <returns></returns>
        List<BadgeSpecial> GetBadgeSpecialsPage(int page, int pagesize, out int total, string orderby = "", string sortord = "desc");
        /// <summary>
        /// add new BadgeSpecial
        /// </summary>
        /// <param name="badgeSpecial"></param>
        /// <returns></returns>
        BadgeSpecial AddBadgeSpecial(BadgeSpecial badgeSpecial);
        /// <summary>
        /// edit badgeSpecial
        /// </summary>
        /// <param name="bs"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        BadgeSpecial EditBadgeSpecial(BadgeSpecial bs, int id);
        /// <summary>
        /// deleted BadgeSpecial
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DelBadgeSpecial(int id);
        /// <summary>
        /// set badge special sort
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        bool SortbyBadgeSpecial(int pid, int id);
        /// <summary>
        /// add badge special point for user
        /// </summary>
        /// <param name="badgeSpecialId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool GetPoint(int badgeSpecialId, int userId);
        /// <summary>
        /// get user badge special
        /// </summary>
        /// <param name="badgeSpecialsId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="total"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        List<UserBadgeSpecial> GetUserBadgeSpecialsPage(int badgeSpecialsId, int page, int pagesize, out int total, string orderBy = "");
        List<UserBadgeSpecial> GetUserBadgeSpecials(int badgeSpecialsId);

        BadgeSpecialEmail GetBadgeSpecialEmailForBSId(int badgeSpecialId);
        BadgeSpecialEmail AddBadgeSpecialEmail(BadgeSpecialEmail bse);
        BadgeSpecialEmail UpdatedBadgeSpecialEmail(BadgeSpecialEmail bse, int id);
        BoolMessage ImportBadgeSpecialEmail(List<string> emails, int badgeSpecialId);

        /// <summary>
        /// post visit时如果有填写Tell us your experience添加积分
        /// (comment/rate bonus[General Comments on Post Visit]
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="userId"></param>
        /// <param name="pvid"></param>
        void InsertPointForGeneralComments(string comment, int userId, int pvid);
    }
}
