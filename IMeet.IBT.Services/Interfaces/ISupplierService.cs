﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.DAL.Models;

namespace IMeet.IBT.Services
{
    public interface ISupplierService
    {
        #region Country & City
        /// <summary>
        /// 返回所有国家
        /// </summary>
        /// <returns></returns>
        IList<Country> GetAllCountry();
        IList<Country> GetAllCountryNew(string strKeyWord);
        Dictionary<string, string> GetAllCountryItem();
        /// <summary>
        ///返回所有城市 
        /// </summary>
        /// <returns></returns>
        List<CityList> GetAllCity(int page, int pageSize, out int total, out int itemTotal, string txtValue);
        /// <summary>
        /// 统计国家的RRWB
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="ibtType"></param>
        /// <param name="rrwbType"></param>
        /// <returns></returns>
        BoolMessage _RRWBStatisticsCountry(int countryId, Identifier.IBTType ibtType, Identifier.RRWBType rrwbType);
        /// <summary>
        /// 统计城市的RRwb
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="ibtType"></param>
        /// <param name="rrwbType"></param>
        /// <returns></returns>
        BoolMessage _RRWBStatisticsCity(int cityId, Identifier.IBTType ibtType, Identifier.RRWBType rrwbType);
        /// <summary>
        /// 根据国家返回下面的所有城市
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        IList<City> GetCityList(int countryId);
        IList<City> GetCityListNew(int countryId, string keyword);
        IList<CityListItem> GetHistoryCityList(int userId, int countryId, out int total);
        Dictionary<string, string> GetAllCityItem(int countryId);

        /// <summary>
        /// 根据城市IDs返回下面的所有城市
        /// </summary>
        /// <param name="Ids">多个以“，”分开的城市ID</param>
        /// <returns></returns>
        IList<City> GetCityListByIds(string cityIds);

        /// <summary>
        /// 根据国家ID获取国家信息
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        Country GetCountryInfo(int countryId);
        Country GetAllCountryInfo(string countryName);
        /// <summary>
        /// 根据城市ID获取城市信息
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        City GetCityInfo(int cityId);
        City GetAllCityInfo(string cityName);
        /// <summary>
        /// 根据城市ID返回国家名称与城市名称
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        CountryCityItem GetCountryCityInfo(int cityId);

        #endregion

        #region Widget Country/City

        #region 新增国家 Widget Country
        BoolMessageItem<int> AddWidgetCountry(int userId, int countryId, int scroe);
        #endregion

        #region 新增城市 Widget City
        BoolMessageItem<int> AddWidgetCity(int userId, int cityId, int scroe);
        #endregion

        #region 返回所有添加的国家
        /// <summary>
        /// 返回所有添加的国家
        /// </summary>
        /// <returns></returns>
        IList<WidgetIBT> GetAllWidgetIBTCountry(string strWhere, int page, int pageSize, out int total);

        IList<Country> GetAllSearchCountry(string keyWord, int page, int pageSize, out int total);

        #endregion

        #region 返回所有添加的城市
        /// <summary>
        /// 返回所有添加的城市
        /// </summary>
        /// <returns></returns>
        List<WidgetIBTCity> GetAllWidgetIBTCity(string strWhere, int page, int pageSize, out int total);

        IList<CountryCityItem> GetAllSearchCity(string strWhere, int page, int pageSize, out int total);

        #endregion

        #region 查询城市列表
        /// <summary>
        /// 查询城市列表
        /// </summary>
        /// <returns></returns>
        List<WidgetIBTCity> GetAllWidgetIBTCityAfterClick(int page, int pageSize, out int total, out int itemTotal);

        #endregion

        #region 根据id删除国家
        /// <summary>
        /// 根据id删除国家
        /// </summary>
        /// <returns></returns>
        BoolMessageItem<int> DeleteWidgetIBTCountry(int objId);

        #endregion

        #region  根据id删除城市
        /// <summary>
        /// 根据id删除城市
        /// </summary>
        /// <returns></returns>
        BoolMessageItem<int> DeleteWidgetIBTCity(int objId);

        #endregion

        /// <summary>
        /// Country Sort
        /// </summary>
        /// <param name="ObjIds"></param>
        /// <returns></returns>
        BoolMessageItem<int> SetWidgetCountrySort(List<int> ObjIds);


        /// <summary>
        /// City Sort
        /// </summary>
        /// <param name="ObjIds"></param>
        /// <returns></returns>
        BoolMessageItem<int> SetWidgetCitySort(List<int> ObjIds);

        #endregion

        #region I've RRWB#我对目的地的评分/推荐/想去（去过）/愿望单
        /// <summary>
        /// 页面的Rating/Recommended/Want to Go/Go Back/Bucket List 单个评论，就是一次只能更改一个值
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="rrwbType"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        BoolMessage RRWB(int userId, Identifier.PostTypes postType, Identifier.IBTType ibtType, int objId, Identifier.RRWBType rrwbType, int val);
        /// <summary>
        /// 页面的Rating/Recommended/Want to Go/Go Back/Bucket List 一次改几个值
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="rating"></param>
        /// <param name="recommended"></param>
        /// <param name="wggb"></param>
        /// <param name="bucketList"></param>
        /// <returns></returns>
        BoolMessage RRWB(int userId, Identifier.IBTType ibtType, int objId, int rating, int recommended, int wggb, int bucketList);

        /// <summary>
        /// 根据用户ID，SupplyId返回RRWB
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="objId"></param>
        /// <param name="ibtType"></param>
        /// <returns></returns>
        RRWB GetUserOnlyRRWB(int userId, int objId, Identifier.IBTType ibtType);
        #endregion

        #region The Recent Places that We've Been
        /// <summary>
        /// The Recent Places that We've Been list
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        IList<RecentPlacesItem> GetHomeRecentPlacesList(int limit);
        #endregion

        #region I've Been There
        /// <summary>
        /// 首页，Prolie的ibt统计
        /// </summary>
        /// <param name="userId">用户ID，如果是0就是所有（首页的）</param>
        /// <param name="tabId">1 Countries 2 Cities 3 Hotels 4 LIFESTYLE 5 ATTRACTIONS</param>
        /// <returns></returns>
        int IbtStatistics(int userId, int tabId);

        /// <summary>
        /// Step4 Confrim的时候保存的
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <returns></returns>
        BoolMessage IBeenThere(int userId, long epoch);

        /// <summary>
        /// I've Been There
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        BoolMessage IBeenThere(int postType, int userId, int countryId, int cityId, int supplyId);

        BoolMessage IBeenThere(int userId, int countryId, int cityId, int supplyId, string strMsg);

        void ReIBTCount(int ibtId);
        /// <summary>
        /// 返回用户Last been to:city,country
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>city,country</returns>
        string LastBeenToCityCountryName(int userId);
        #endregion

        #region Post visit
        /// <summary>
        /// 返回首页（index）的PostedVisitsCount
        /// </summary>
        /// <returns></returns>
        int GetPostedVisitsCount();
        int HasUserPVCountries(int userId, int countryId);
        int HasUserPVCities(int userId, int cityId);
        int HasUserPVSupply(int userId, int supplyId);
        /// <summary>
        /// PostVisit 的Country的信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        PostVisitItem GetPostVisitCountryInfo(int userId, int countryId);
        /// <summary>
        /// PostVisit 的City的信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        PostVisitItem GetPostVisitCityInfo(int userId, int cityId);
        /// <summary>
        /// PostVisit 的Supply的信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        PostVisitItem GetPostVisitSupplyInfo(int userId, int supplyId);
        /// <summary>
        /// SavePostVisit
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="CountryId"></param>
        /// <param name="cityId"></param>
        /// <param name="SupplyId"></param>
        /// <param name="experiences"></param>
        /// <param name="sharingData"></param>
        /// <param name="pvDate"></param>
        /// <returns></returns>
        BoolMessageItem<int> SavePostVisit(int postType, int userId, int ibtType, int countryId, int cityId, int supplyId, string experiences, string sharingData, DateTime? pvDate);
        BoolMessageItem<int> EditPostVisit(int pvId, int newsfeedId, string experiences, DateTime? pvDate);
        /// <summary>
        /// 保存用户上传的图片
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="fileName">图形名称</param>
        /// <param name="sourceFileName">路径（/0/0/tesst/guid.jpg）</param>
        /// <returns></returns>
        BoolMessageItem<int> SavePostvisitFile(string guid, string fileName, string sourceFileName);

        /// <summary>
        /// 更改PvId
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="pvId"></param>
        void SetPostVistAttachmentPvId(string guid, int pvId);

        /// <summary>
        /// 返回用户LAST POSTED VISIT: cityName,countryName
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        string LastPostedVistPlaceName(int userId);

        /// <summary>
        /// 返回用户的Last supply
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        BoolMessage LastPostedVistSupplyName(int userId);
        /// <summary>
        /// 返回用户的last postvisit
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        BoolMessage LastPosteVistName(int userId);
        PostVistAttachment GetPostVisitInfo(string Guid);
        PostVistAttachment GetPostVisitImageInfo(string SourceFileName);
        BoolMessageItem<int> PostVisitIsDel(string Guid);

        /// <summary>
        /// 判断是否有postvisit过
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <returns></returns>
        bool HasUserPostVisited(int userId, int ibtType, int objId);

        #endregion

        #region Show friend
        IList<ConnectionListItem> GetAllFriend(int userId, int countryId, int cityId, int supplyId);
        #endregion

        #region AddTravels
        /// <summary>
        /// 获取用户选择的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="Epoch"></param>
        /// <param name="travelType"></param>
        /// <param name="objId"></param>
        /// <returns></returns>
        AddTravelsTemp GetAddTravels(int userId, long epoch, Identifier.AddTravelsType travelType, int parentId);

        /// <summary>
        /// AddTravels
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <param name="travelType"></param>
        /// <param name="parentId"></param>
        /// <param name="vals"></param>
        void AddTravels(int userId, long epoch, Identifier.AddTravelsType travelType, int parentId, string vals);

        /// <summary>
        /// 删除用户选择的临时数据
        /// </summary>
        /// <param name="userId"></param>
        void DelAddTravels(int userId);

        /// <summary>
        /// AddTravels
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        IList<SupplyListShort> AddTravelsGetSupplyListAll(string keyWord, string filterLetter, int userId, int cityId, int pageSize = 10, int pageNumber = 1);
        /// <summary>
        /// AddTravels search supply list
        /// </summary>
        /// <param name="keyWord"></param>
        /// <param name="filterLetter"></param>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        List<SupplyListShort> AddTravelsGetSupplyListSortBy(string keyWord, string filterLetter, int userId, int cityId, int pageSize = 10, int pageNumber = 1, string sortBy = "");
        /// <summary>
        /// AddTravelsGetSupplyListSortByActivity
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <param name="categoryName">Hotels,Lifestyle,Attractions</param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        IList<SupplyListShort> AddTravelsGetSupplyListSortByCategory(string keyWord, string filterLetter, int userId, int cityId, string categoryName, int pageSize = 10, int pageNumber = 1);

        /// <summary>
        /// 获取用户添加的国家，城市与venues (step4)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <param name="travelType"></param>
        /// <returns></returns>
        IList<string> AddTravelsGetSelectItem(int userId, long epoch, Identifier.AddTravelsType travelType);
        List<Country> AddTravelsGetSelectCountry(int userId, long epoch);
        List<City> AddTravelsGetSelectCity(int userId, long epoch);
        List<Supply> AddTravelsGetSelectSupply(int userId, long epoch);
        /// <summary>
        /// 获取Ids
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <param name="travelType"></param>
        /// <returns></returns>
        IList<int> AddTravelsGetSelectIds(int userId, long epoch, Identifier.AddTravelsType travelType);

        #endregion

        #region Travels History
        void SaveToHistory(List<City> cityList, int userId, long epoch);
        int HasPostVisitAllCount(int userId);
        int HasCountriesPostVisitCount(int userId, int countryId);
        int HasCountriesPostVisitOnlyCount(int userId, int countryId);
        int HasCitiesPostVisitCount(int userId, int cityId);
        int HasCitiesPostVisitOnlyCount(int userId, int cityId);
        int HasVenuesPostVisitCount(int userId, int supplyId);
        IList<SupplierListItem> GetHistoryVenuesList(int userId, int countryId, int cityId, out int total);
        #endregion

        #region Supplier & Supplier category Types
        /// <summary>
        /// 根据supplyId获取下面的信息
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        Supply GetSupplyDetail(int supplyId);
        /// <summary>
        /// 根据countryId,cityId,cateId获取下面的信息
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="cateId"></param>
        /// <returns></returns>
        IList<Supply> GetSupplyItemDetail(string strWhere);
        /// <summary>
        /// 根据城市ID返回下面所有的Supply
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        IList<Supply> GetSupplyList(int cityId);

        /// <summary>
        /// Add Travels Step3的列表(暂时不用这，改用AddTravelsGetSupplyListSortByHotel，AddTravelsGetSupplyListSortByActivity)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <param name="sortBy"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        IList<SupplyListShort> GetSupplyList(int userId, int cityId, int sortBy, int pageSize, int pageNumber);

        /// <summary>
        /// 根据supplyId获取简要信息
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        SupplyListShort GetSupplyShortInfo(int userId, int supplyId);

        /// <summary>
        /// GetStaffsContactList
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        IList<StaffsContact> GetStaffsContactList(int supplyId);

        /// <summary>
        /// 获取supply的封面图片（单张）
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="coverPhotoSrc">数据库保存的地址</param>
        /// <param name="size"></param>
        /// <returns></returns>
        string GetSupplyPhoto(int supplyId, string coverPhotoSrc, Identifier.SuppilyPhotoSize size);
        string GetInterSupplyPhoto(int supplyId, string coverPhotoSrc, Identifier.SuppilyPhotoSize size);
        /// <summary>
        /// 根据SupplyId返回Supplier所属的分类
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        AttributeData GetSupplyTypes(int supplyId);
        #endregion

        #region Home#Most Recommended Places/Most Visited Places/We've Been/Destination of the Week
        /// <summary>
        /// 个人首页的Most Recommended Places
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<SupplyHomeListShort> GetMostRecommendedPlaces(int userId, int limit);

        /// <summary>
        /// 个人首页的Most Visited Places
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<SupplyHomeListShort> GetMostVisitedPlaces(int userId, int limit);

        #endregion

        #region MyList
        int GetMyListCount(int userId, string listType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="listType">Recommended,WanttoGo,BucketList</param>
        /// <returns></returns>
        IList<MyListItem> GetMyList(int userId, string listType, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total);
        #endregion

        #region Profile Activties List &  My Travel
        /// <summary>
        /// Profile Activties: my （user ）Activties
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<FeedList> FeedUserActivtieLists(int userId, int page, int pageSize);

        /// <summary>
        /// Profile Activties: All Activities
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <remarks>
        /// 1. 从数据获取主要的数据
        /// 2. 获取RRWB的数据
        /// 3. 如果是PV的。看是否是否上传有图片
        /// </remarks>
        IList<FeedList> FeedAllActivitiesLists(int userId, int page, int pageSize);
        /// <summary>
        /// Profile Activties: Friend Activties
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <remarks>
        /// 1. 从数据获取主要的数据
        /// 2. 获取RRWB的数据
        /// 3. 如果是PV的。看是否是否上传有图片
        /// </remarks>
        IList<FeedList> FeedFriendActivtieLists(int userId, int page, int pageSize);

        /// <summary>
        /// userId's Travel List
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="categoryId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IList<FeedList> FeedTravelList(int userId, int categoryId, int page, int pageSize, string filterLetter, string sortByRating);
        IList<FeedList> FeedDateTravelList(int userId, string year, string month, int categoryId, int page, int pageSize, string filterLetter, string sortByRating);
        IList<FeedList> FeedActivtieListsBySupplyId(int supplyId, int page, int pageSize);
        /// <summary>
        /// 统计userId的好友去过objId的次数 （ 9 Friends has Been There）
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        int HasBeenThereFriendsCount(int userId, int countryId, int cityId, int supplyId);

        IList<NewsFeed> getNewsFeed(int page, int pageSize, int userId = 0, int countryId = 0, int cityId = 0, int supplyId = 0);
        IList<NewsFeed> getNewsFeedAll(int userId = 0, int countryId = 0, int cityId = 0, int supplyId = 0);
        /// <summary>
        /// 有可能是国家，城市，supply
        /// </summary>
        /// <param name="feedList"></param>
        /// <returns></returns>
        List<FeedList> GetFeedRRWB(List<FeedList> feedList);
        #endregion

        #region Supply Type(SupplyTypeManagement)
        AttributeData GetAttrInfo(int attrId);
        BoolMessageItem<int> UpdateAttributeData(int attrDataId, int attrCateId, string attrDataName, string imgsrc, string mapsrc, string maptsrc);
        BoolMessageItem<int> AddNewType(int attrDataId, int attrCateId, string attrDataName, string imgsrc, string mapsrc, string maptsrc);
        BoolMessageItem<int> SetTypeSort(List<int> SupplyAttr);
        BoolMessageItem<int> EditType(int supplyId, List<int> ArrtDataId);
        /// <summary>
        /// 返回SupplyType Sort列表（SupplyId,AttrDataName,Score）
        /// </summary>
        /// <returns></returns>
        IList<SupplyTypeInfo> GetAllSupplyTypeSort(int supplyId);
        AttributeData GetAttributeData(int supplyId);
        /// <summary>
        /// GetAttributeDataList
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        IList<AttributeData> GetAttributeDataList(int ParentId);
        IList<AttributeData> GetAttributeDataImg(int AttrDataId);
        #endregion

        #region SupplierList(SupplierManagement)
        /// <summary>
        /// 返回Supplier
        /// </summary>
        /// <returns></returns>
        IList<DestinationWeekItem> GetAllSupplier(string title);
        Dictionary<string, string> GetSupplierItem(string title);
        int GetSupplierListCount(string strWhere);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<SupplierListItem> GetSupplierList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total, string Where = "");
        BoolMessageItem<int> SetSortby(int supplyId, int newText);
        BoolMessageItem<int> SetSupplyIsDel(int supplyId);
        BoolMessageItem<int> UpdateSupplier(Supply supply, List<SupplyAttributeData> datalist);
        IList<SupplyAttributeData> GetSupplyAttributeData(int supplyid);
        BoolMessageItem<string> AddSupplyAttachment(SuppliesAttachment SuppliesAttachment);
        BoolMessageItem<int> AddSupplyAttachmentBind(SuppliesAttachmentBind sab);
        /// <summary>
        /// 根据supplierid来读取相关的图片
        /// </summary>
        /// <param name="supplyId">supplyId</param>
        /// <param name="limit">读取的数量，0表示全部</param>
        /// <param name="type">是否包含logo</param>
        /// <returns></returns>
        IList<SuppliesAttachment> GetSuppliesAttachment(int supplyId, int limit = 0, bool haslogo = false);
        IList<SuppliesAttachment> GetSuppliesAttachment(string[] guid);
        IList<SuppliesAttachmentBind> GetSuppliesAttachmentBind(int supplyId);
        BoolMessageItem<int> DeleteSupplyAttachmentBind(int supplyid);
        #endregion

        #region Feedbacks
        /// <summary>
        /// Supply的评论
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        BoolMessageItem SupplyFeedbacks(int supplyId, int userId, string comments);

        /// <summary>
        /// 评论的列表
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        IList<SupplyFeedbackItem> FeedbackLists(int supplyId, int page, int pageSize);
        #endregion

        #region Landing Management
        IList<SupplierListItem> GetHomeTopList(string strWhere, int page, int pageSize, out int total);
        IList<IndexTop10Supplies> GetHomeTopListCheck();
        BoolMessageItem<int> EditIndexSupply(int indexId, string imgsrc, string txtDesc);
        BoolMessageItem<int> SetIndexSupplySort(List<int> IndexIds);
        BoolMessageItem<int> SetIndexSupplyIsDel(int indexId);
        BoolMessageItem<int> AddIndexSupply(int objId, int objType, int scroe);
        IndexTop10Supplies GetIndexSupplyInfo(int indexId);
        #endregion

        #region IBT(ibtcount)
        DAL.IBT GetIBTInfo(int userId, int supplyId, int countryId, int cityId);
        DAL.IBT GetIBTUser(int userId, int supplyId, int countryId, int cityId);
        /// <summary>
        /// 获取用户ibt过的city的ibt列表
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<DAL.IBT> GetUserIBTCities(int userId);
        List<DAL.IBT> GetUserIbt(int userId);
        #endregion

        #region Widget Management

        #region 新增Media
        BoolMessageItem<int> AddWidgetMedia(int ibtType, int supplyId, int countryId, int cityId, int position, int mediaType, string title, string desc, string startDate, string endTime, string imgUrl, string htmlCode, bool isLink, string targetUrl);
        #endregion

        #region Edit Media
        BoolMessageItem<int> EditWidgetMedia(int mediaId, int ibtType, int supplyId, int countryId, int cityId, int position, int mediaType, string title, string desc, string startDate, string endTime, string imgUrl, string htmlCode, bool isLink, string targetUrl);
        #endregion

        DAL.Media GetWidgetMediaInfo(int mediaId);
        /// <summary>
        /// GetSpecialAdsList
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        IList<Media> GetSpecialAdsList(int supplyId);
        IList<Media> GetSpecialsOfCountry(int countryId, int pageNumber, int pageSize, out int total);
        IList<WidgetMedia> GetAllWidgetAdsInfo(string strWhere, int page, int pageSize, out int total);

        #region 根据id删除Ads
        /// <summary>
        /// 根据id删除Ads
        /// </summary>
        /// <returns></returns>
        BoolMessageItem<int> DeleteWidgetAds(int objId);
        #endregion

        #endregion

        #region Supplier Specials
        /// <summary>
        /// Specials Lists 的数据
        /// </summary>
        /// <returns></returns>
        IList<Media> GetSpecialsList(string filterSort, int page, int pageSize, string filterKeyword, string filterField, out int total);
        #endregion

        #region supplier Attachment--附件 and AttachmentComments--附件评论
        IList<Attachment> GetAttachment(int objId, Services.Identifier.AttachmentType type);
        IList<AttachmentComment> GetAttachmentComments(int attid, Services.Identifier.AttachmentType type);
        #endregion

        #region Newsfeed好友列表
        /// <summary>
        /// 返回最新7天好友列表
        /// </summary>
        /// <returns></returns>
        IList<ConnectionListItem> GetNewsfeedFriend(int userId, int newsfeedId);
        #endregion

        #region Services列表
        IList<FeedList> ServicesSPList(int userId, int categoryId, int page, int pageSize, string filterLetter, string sortByRating);
        #endregion
    }
}
