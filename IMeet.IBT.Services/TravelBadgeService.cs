﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMeet.IBT.DAL;
using IMeet.IBT.DAL.Models;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public class TravelBadgeService : BaseService, ITravelBadgeService
    {
        private IProfileService _profileService;

        public TravelBadgeService(IProfileService profileService)
        {
            _profileService = profileService;
        }
        /// <summary>
        /// 获取用户BonusPoint信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserBonusPoint GetUserBonusPoint(User user)
        {
            string sql = "";
            //判断是否Meeting Planners
            if (user.IsMeetingPlanner())
            {
                sql = @"SELECT UserId
                    ,SUM(IsOnSiteManagement)  'OSMNumber'
                    ,SUM(IsSiteInsepection) 'SINumber'
                    ,SUM(IsGeneralTravel) 'GTNumber'
                    ,SUM(IsPoloticalSocialCrisis) 'PSCNumber'
                    ,SUM(IsStrike) 'StrikeNumber'
                    ,SUM(IsNaturalDisaster) 'NDNumber'
                    ,SUM(IsGobalAudience) 'GANumber'
                    ,SUM(IsNonHotelVenue) 'NHVNumber'
                    ,SUM(IsGroupAttendees) 'GroupANumber'
                    ,SUM(IsVirtualComponent) 'VCNumber'
                    ,SUM(IsCPLP) 'CPLPNumber'
                    ,SUM(IsOthers) 'OthersNumber'
                    FROM dbo.MeetingPlan
                    WHERE UserId={0}
                    AND CreateDate>={1}
                    GROUP BY UserId;";
                //            else
                //                sql = @"SELECT UserId
                //                        ,SUM(IsGeneralTravel) 'GTNumber'
                //                        FROM dbo.MeetingPlan
                //                        WHERE UserId={0}
                //                        AND CreateDate>={1}
                //                        GROUP BY UserId;";
                UserBonusPoint ubp = _db.Database.SqlQuery<UserBonusPoint>(sql, user.UserId, new DateTime(DateTime.Now.Year - 2, 1, 1)).FirstOrDefault();
                if (ubp == null)
                    ubp = new UserBonusPoint() { UserId = user.UserId };
                return ubp;
            }
            else
                return new UserBonusPoint() { UserId = user.UserId };
        }
        /// <summary>
        /// 获取用户的SpecialPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<SpecialPoint> GetUserSpecialPoint(User user)
        {
            return _db.SpecialPoints.Where(s => s.UserId == user.UserId && s.Expires >= DateTime.Now).ToList();
        }
        /// <summary>
        /// 获取用户的badgespecial
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<UserBadgeSpecial> GetUserBadgeSpecial(User user)
        {
            return _db.UserBadgeSpecials.Where(u => u.UserId == user.UserId && u.ReceivedDate.Value.Year == DateTime.Now.Year).ToList();
        }
        /// <summary>
        /// 获取用户的PermanentPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserPermanentPoint GetUserPermanentPoint(User user)
        {
            string sql = @"EXEC IBT_GetUserPermanentPoint {0}";
            UserPermanentPoint upp = _db.Database.SqlQuery<UserPermanentPoint>(sql, user.UserId).FirstOrDefault();
            if (upp == null)
                upp = new UserPermanentPoint() { UserId = user.UserId };
            return upp;
        }
        /// <summary>
        /// 获取当前登录用户的TravelBadge信息
        /// </summary>
        /// <returns></returns>
        public UserTravelBadge GetUserTravelbadge()
        {
            int userId = _profileService.GetUserId();
            User user=null;
            if (userId > 0)
                user = _profileService.GetUserInfo(userId);
            if (user != null)
                return this.GetUserTravelbadge(user);
            else
                return null;
        }
        /// <summary>
        /// 获取用户的TravelBadge信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserTravelBadge GetUserTravelbadge(User user)
        {
            string key = "GetUserTravelbadge_" + user.UserId.ToString();

            UserTravelBadge utb = _cache.GetOrInsert<UserTravelBadge>(key, 60, true, () => {
                UserTravelBadge utbCache = new UserTravelBadge();

                utbCache.User = user;
                utbCache.UserPermanentPoint = this.GetUserPermanentPoint(user);
                utbCache.UserBonusPoint = this.GetUserBonusPoint(user);
                utbCache.SpecialPoint = this.GetUserSpecialPoint(user);
                utbCache.UserBadgeSpecial = this.GetUserBadgeSpecial(user);

                return utbCache;
            });

            return utb;
        }
        /// <summary>
        /// 获取用户的PointSummary信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserPointSummaryByYear GetUserPointSummaryByYear(User user)
        {
            UserPointSummaryByYear ups = new UserPointSummaryByYear();

            ups.UserPermanentPointList = this.GetUserPermanentPointByYear(user);
            ups.UserBonusPointList = this.GetUserBonusPointByYear(user);
            ups.UserSpecialPointList = this.GetUserSpecialPointByYear(user);

            return ups;
        }
        /// <summary>
        /// 获取用户每年的PermanentPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<UserPermanentPoint> GetUserPermanentPointByYear(User user)
        {
            return _db.Database.SqlQuery<UserPermanentPoint>(@"EXEC IBT_GetUserPermanentPointByYear {0}", user.UserId).ToList();
        }
        /// <summary>
        /// 获取用户每年的BonusPoint信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<UserBonusPoint> GetUserBonusPointByYear(User user)
        {
            List<UserBonusPoint> result = new List<UserBonusPoint>();
            //判断是否Meeting Planners
            if (user.IsMeetingPlanner())
                result = _db.Database.SqlQuery<UserBonusPoint>(@"EXEC IBT_GetUserBonusPointByYear {0}", user.UserId).ToList();
            //else
            //    result = _db.Database.SqlQuery<UserBonusPoint>(@"EXEC IBT_GetUserBonusPointByYear_NMP {0}", user.UserId).ToList();
            return result;
        }
        /// <summary>
        /// 获取用户每年的SpecialPoint
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<UserSpecialPointByYear> GetUserSpecialPointByYear(User user)
        {
            return _db.Database.SqlQuery<UserSpecialPointByYear>(@"EXEC IBT_GetUserSpecialPointByYear {0}", user.UserId).ToList();
        }

        #region BadgeSpeclal
        /// <summary>
        /// 获取badge specials
        /// </summary>
        /// <returns></returns>
        public List<BadgeSpecial> GetBadgeSpecials()
        {
            List<BadgeSpecial> list = _db.BadgeSpecials.Where(bs => bs.StatusId == 0 && bs.StartDate <= DateTime.Now && bs.EndDate >= DateTime.Now && bs.Visible == (int)Identifier.BadgeSpecialVisible.Visible).OrderByDescending(bs => bs.Sortby).ToList();

            return list;
        }

        /// <summary>
        /// 获取用户的badgespecials 按分页
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="total"></param>
        /// <param name="orderby"></param>
        /// <param name="sortord"></param>
        /// <returns></returns>
        public List<BadgeSpecial> GetBadgeSpecialsPage(int page, int pagesize, out int total, string orderby = "", string sortord="desc")
        {
            int skip = (page - 1) * pagesize;

            IQueryable<BadgeSpecial> query = _db.BadgeSpecials.Where(bs => bs.StatusId == 0);

            if (sortord == "asc")
            {
                switch (orderby)
                {
                    case "StartDate":
                        query = query.OrderBy(bs => bs.StartDate);
                        break;
                    case "EndDate":
                        query = query.OrderBy(bs => bs.EndDate);
                        break;
                    case "Visible":
                        query = query.OrderBy(bs => bs.Visible);
                        break;
                    default:
                        query = query.OrderBy(bs => bs.Sortby);
                        break;
                }
            }
            else
            {
                switch (orderby)
                {
                    case "StartDate":
                        query = query.OrderByDescending(bs => bs.StartDate);
                        break;
                    case "EndDate":
                        query = query.OrderByDescending(bs => bs.EndDate);
                        break;
                    case "Visible":
                        query = query.OrderByDescending(bs => bs.Visible);
                        break;
                    default:
                        query = query.OrderByDescending(bs => bs.Sortby);
                        break;
                }
            }

            query = query.Skip(skip).Take(pagesize);

            List<BadgeSpecial> list = query.ToList();

            total = _db.BadgeSpecials.Where(bs => bs.StatusId == 0).Count();

            return list;
        }
        /// <summary>
        /// get user badge special
        /// </summary>
        /// <param name="badgeSpecialsId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="total"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public List<UserBadgeSpecial> GetUserBadgeSpecialsPage(int badgeSpecialsId, int page, int pagesize, out int total, string orderBy = "")
        {
            int skipIndex = (page - 1) * pagesize;
            total = _db.UserBadgeSpecials.Where(ubs => ubs.BadgeSpecialId == badgeSpecialsId && ubs.StatusId == 0).Count();
            List<UserBadgeSpecial> list = new List<UserBadgeSpecial>();

            switch (orderBy)
            {
                case "email":
                    list = _db.UserBadgeSpecials.Where(ubs => ubs.BadgeSpecialId == badgeSpecialsId && ubs.StatusId == 0).OrderBy(ubs => ubs.User.Email).Skip(skipIndex).Take(pagesize).ToList();
                    break;
                case "date":
                    list = _db.UserBadgeSpecials.Where(ubs => ubs.BadgeSpecialId == badgeSpecialsId && ubs.StatusId == 0).OrderByDescending(ubs => ubs.ReceivedDate).Skip(skipIndex).Take(pagesize).ToList();
                    break;
                case "point":
                    list = _db.UserBadgeSpecials.Where(ubs => ubs.BadgeSpecialId == badgeSpecialsId && ubs.StatusId == 0).OrderByDescending(ubs => ubs.point).Skip(skipIndex).Take(pagesize).ToList();
                    break;
                default:
                    list = _db.UserBadgeSpecials.Where(ubs => ubs.BadgeSpecialId == badgeSpecialsId && ubs.StatusId == 0).OrderBy(ubs => ubs.User.Profile.Firstname).ThenBy(ubs => ubs.User.Profile.Lastname).Skip(skipIndex).Take(pagesize).ToList();
                    break;
            }

            return list;
        }
        public List<UserBadgeSpecial> GetUserBadgeSpecials(int badgeSpecialsId)
        {
            return _db.UserBadgeSpecials.Where(ubs => ubs.BadgeSpecialId == badgeSpecialsId).ToList();
        }
        /// <summary>
        /// get the badge special
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BadgeSpecial GetBadgeSpecial(int id)
        {
            BadgeSpecial bs = _db.BadgeSpecials.FirstOrDefault(b => b.BadgeSpecialId == id);

            return bs;
        }

        /// <summary>
        /// add new BadgeSpecial
        /// </summary>
        /// <param name="badgeSpecial"></param>
        /// <returns></returns>
        public BadgeSpecial AddBadgeSpecial(BadgeSpecial badgeSpecial)
        {
            _db.BadgeSpecials.Add(badgeSpecial);
            _db.SaveChanges();
            badgeSpecial.Sortby = badgeSpecial.BadgeSpecialId;
            _db.SaveChanges();
            return badgeSpecial;
        }
        /// <summary>
        /// edit badgeSpecial
        /// </summary>
        /// <param name="bs"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public BadgeSpecial EditBadgeSpecial(BadgeSpecial bs, int id)
        {
            BadgeSpecial badgeSpecial = _db.BadgeSpecials.Find(id);

            badgeSpecial.Title = bs.Title;
            badgeSpecial.Type = bs.Type;
            badgeSpecial.ShortDescription = bs.ShortDescription;
            badgeSpecial.LongDescription = bs.LongDescription;
            badgeSpecial.ImageSrc = bs.ImageSrc;
            badgeSpecial.StartDate = bs.StartDate;
            badgeSpecial.EndDate = bs.EndDate;
            badgeSpecial.Link = bs.Link;
            badgeSpecial.Point = bs.Point;
            badgeSpecial.Visible = bs.Visible;
            if (bs.Type == (int)Identifier.BadgeSpecialType.UniqueIbt)
            {
                badgeSpecial.UIBTPV = bs.UIBTPV;
                badgeSpecial.UIBTPOF = bs.UIBTPOF;
            }
            if (bs.Type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity)
            {
                badgeSpecial.USAActivity = bs.USAActivity;
                badgeSpecial.USAPoint = bs.USAPoint;
            }

            _db.SaveChanges();

            return badgeSpecial;

        }

        /// <summary>
        /// deleted BadgeSpecial
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DelBadgeSpecial(int id)
        {
            try
            {
                BadgeSpecial bs = _db.BadgeSpecials.Find(id);
                if (bs != null)
                {
                    bs.StatusId = 1;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// set badge special sort
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SortbyBadgeSpecial(int pid, int id)
        {
            BadgeSpecial bs = _db.BadgeSpecials.Find(id);
            BadgeSpecial pbs = _db.BadgeSpecials.Find(pid);

            if (bs == null || pbs == null)
                return false;

            int sort = bs.Sortby;
            bs.Sortby = pbs.Sortby;
            pbs.Sortby = sort;

            _db.SaveChanges();

            return true;
        }

        /// <summary>
        /// add badgeSpecial point for user
        /// </summary>
        /// <param name="badgeSpecialId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool GetPoint(int badgeSpecialId, int userId)
        {
            this.RemoveUserTravelBadgeCache(userId);

            if (_db.UserBadgeSpecials.FirstOrDefault(ubs => ubs.UserId == userId && ubs.BadgeSpecialId == badgeSpecialId) == null)
            {
                BadgeSpecial bs = _db.BadgeSpecials.Find(badgeSpecialId);
                if (bs != null)
                {
                    UserBadgeSpecial ubs = new UserBadgeSpecial()
                    {
                        UserId = userId,
                        BadgeSpecialId = badgeSpecialId,
                        point = bs.Point,
                        ReceivedDate = DateTime.Now,
                        StatusId = 0,
                        UpdateDate = DateTime.Now
                    };
                    _db.UserBadgeSpecials.Add(ubs);
                    _db.SaveChanges();
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// get badgespecial email
        /// </summary>
        /// <param name="badgeSpecialId"></param>
        /// <returns></returns>
        public BadgeSpecialEmail GetBadgeSpecialEmailForBSId(int badgeSpecialId) 
        {
            return _db.BadgeSpecialEmails.Where(bse => bse.BadgeSpecialId == badgeSpecialId).OrderByDescending(bse => bse.BadgeSpecialEmailId).FirstOrDefault();
        }
        /// <summary>
        /// add a new badgespecialemail
        /// </summary>
        /// <param name="bse"></param>
        /// <returns></returns>
        public BadgeSpecialEmail AddBadgeSpecialEmail(BadgeSpecialEmail bse)
        {
            _db.BadgeSpecialEmails.Add(bse);
            _db.SaveChanges();
            return bse;
        }
        /// <summary>
        /// updated badgeSpecial email
        /// </summary>
        /// <param name="bse"></param>
        /// <param name="id"></param>
        public BadgeSpecialEmail UpdatedBadgeSpecialEmail(BadgeSpecialEmail bse, int id)
        {
            BadgeSpecialEmail badgeSpecialEmail = _db.BadgeSpecialEmails.Find(id);
            if (badgeSpecialEmail != null)
            {
                badgeSpecialEmail.Title = bse.Title;
                badgeSpecialEmail.EmailFrom = bse.EmailFrom;
                badgeSpecialEmail.Content = bse.Content;
                badgeSpecialEmail.UpdateDate = DateTime.Now;

                _db.SaveChanges();
            }
            return badgeSpecialEmail;
        }

        public BoolMessage ImportBadgeSpecialEmail(List<string> emails, int badgeSpecialId)
        {
            BadgeSpecial bs = _db.BadgeSpecials.Find(badgeSpecialId);
            string falseEmails = "";

            if (bs == null)
                return new BoolMessage(false, "Badge Special not exists!");

            emails = emails.Distinct().ToList();

            if (emails.Count() > 0)
            {
                List<User> users = _db.Users.Where(u => u.StatusId == 0).ToList();
                List<UserBadgeSpecial> ubs = new List<UserBadgeSpecial>();
                List<UserBadgeSpecial> hasUBS = _db.UserBadgeSpecials.Where(u => u.BadgeSpecialId == bs.BadgeSpecialId && u.StatusId == 0).ToList();

                emails.ForEach(email =>
                {
                    User user = users.Where(u => u.Email == email).FirstOrDefault();

                    if (user != null && !hasUBS.Exists(u => u.UserId == user.UserId))
                    {
                        string key = "GetUserTravelbadge_" + user.UserId.ToString();
                        if (_cache.Contains(key))
                            _cache.Remove(key);

                        UserBadgeSpecial ubsItem = new UserBadgeSpecial();

                        ubsItem.BadgeSpecialId = bs.BadgeSpecialId;
                        ubsItem.point = bs.Point;
                        ubsItem.ReceivedDate = DateTime.Now;
                        ubsItem.UserId = user.UserId;
                        ubsItem.StatusId = 0;
                        ubsItem.UpdateDate = DateTime.Now;

                        ubs.Add(ubsItem);
                    }
                    else {
                        if (falseEmails == "")
                            falseEmails = email;
                        else
                            falseEmails = falseEmails + "," + email;
                    }
                });

                if (ubs.Count() > 0)
                {
                    ubs.ForEach(userTravelBadge =>
                    {
                        _db.UserBadgeSpecials.Add(userTravelBadge);
                    });

                    _db.SaveChanges();
                }
            }

            return new BoolMessage(true,falseEmails);
        }

        #endregion

        /// <summary>
        /// 清除User Travel Badge 缓存
        /// </summary>
        /// <param name="userid"></param>
        private void RemoveUserTravelBadgeCache(int userid)
        {
            string key = "GetUserTravelbadge_" + userid.ToString();
            if (_cache.Contains(key))
                _cache.Remove(key);
        }
        /// <summary>
        /// insert meetPlan
        /// </summary>
        /// <param name="meetingPlan"></param>
        public void InsertMeetingPlan(MeetingPlan meetingPlan)
        {
            this.RemoveUserTravelBadgeCache(meetingPlan.UserId);
        
            _db.MeetingPlans.Add(meetingPlan);
            _db.SaveChanges();

            this.InsertPointForMeetingPlan(meetingPlan);

            //on site management 才能产生meeting planning comments
            if (meetingPlan.IsOnSiteManagement > 0)
            {
                this.InsertPointForMeetingPlanComment(meetingPlan);
            }
        }
        /// <summary>
        /// post visit 时添加积分(posting activity bonus)
        /// </summary>
        /// <param name="meetingPlan"></param>
        private void InsertPointForMeetingPlan(MeetingPlan meetingPlan)
        {
            int totalPoint = 0;

            DAL.IBT ibt = _db.IBTs.Where(i => i.CountryId == meetingPlan.CountryId && i.CityId == meetingPlan.CityId && i.SupplyId == meetingPlan.SupplyId && i.UserId == meetingPlan.UserId).FirstOrDefault();
            //第1次postvisit才计算该分数
            if (ibt != null && ibt.IbtCount == 1)
            {
                if (meetingPlan.IbtType == (int)Identifier.IBTType.Country)
                    totalPoint += 50;
                if (meetingPlan.IbtType == (int)Identifier.IBTType.City)
                    totalPoint += 30;
                if (meetingPlan.IbtType == (int)Identifier.IBTType.Supply)
                    totalPoint += 10;
            }

            totalPoint += meetingPlan.IsOnSiteManagement * 30;
            totalPoint += meetingPlan.IsSiteInsepection * 20;
            totalPoint += meetingPlan.IsGeneralTravel * 10;

            totalPoint += meetingPlan.IsPoloticalSocialCrisis * 20;
            totalPoint += meetingPlan.IsStrike * 20;
            totalPoint += meetingPlan.IsNaturalDisaster * 20;
            totalPoint += meetingPlan.IsGobalAudience * 20;
            totalPoint += meetingPlan.IsNonHotelVenue * 20;
            totalPoint += meetingPlan.IsGroupAttendees * 20;
            totalPoint += meetingPlan.IsVirtualComponent * 20;
            totalPoint += meetingPlan.IsCPLP * 20;

            List<BadgeSpecial> list = _db.BadgeSpecials.Where(b => b.Type == (int)Identifier.BadgeSpecialType.UniqueIbt && b.StatusId == 0 && b.StartDate <= DateTime.Now && b.EndDate >= DateTime.Now).ToList();
            list.ForEach(badgeSpecial => {
                if (badgeSpecial.UIBTPV > 0 && _db.UserBadgeSpecials.Count(u => u.BadgeSpecialId == badgeSpecial.BadgeSpecialId && u.UserId == meetingPlan.UserId) == 0)
                {
                    UserBadgeSpecial userBadgeSpecial = new UserBadgeSpecial();

                    userBadgeSpecial.UserId = meetingPlan.UserId;
                    userBadgeSpecial.BadgeSpecialId = badgeSpecial.BadgeSpecialId;
                    userBadgeSpecial.point = (int)(totalPoint * badgeSpecial.UIBTPOF * 0.01);
                    userBadgeSpecial.ReceivedDate = DateTime.Now;
                    userBadgeSpecial.UpdateDate = DateTime.Now;
                    userBadgeSpecial.PID = meetingPlan.MeetingPlanId;

                    badgeSpecial.UIBTPV--;

                    _db.UserBadgeSpecials.Add(userBadgeSpecial);
                }
            });

            _db.SaveChanges();

        }

        /// <summary>
        /// post visit 时如果有填写planning comments添加积分(comment/rate bonus[Meeting Comments on PostVisit])
        /// </summary>
        /// <param name="meetingPlan"></param>
        private void InsertPointForMeetingPlanComment(MeetingPlan meetingPlan)
        {
            if (!string.IsNullOrEmpty(meetingPlan.Comments))
            {
                string key = "GetUserTravelbadge_" + meetingPlan.UserId;
                if (_cache.Contains(key))
                    _cache.Remove(key);

                List<BadgeSpecial> list = _db.BadgeSpecials.Where(b => b.Type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity && b.USAActivity == "PostVisit" && b.StatusId == 0 && b.StartDate <= DateTime.Now && b.EndDate >= DateTime.Now).ToList();

                list.ForEach(badgeSpecial =>
                {
                    //if (_db.UserBadgeSpecials.Count(u => u.UserId == meetingPlan.UserId && u.BadgeSpecialId == badgeSpecial.BadgeSpecialId) == 0)
                    //{
                    UserBadgeSpecial userBadgeSpecial = new UserBadgeSpecial();
                    userBadgeSpecial.BadgeSpecialId = badgeSpecial.BadgeSpecialId;
                    userBadgeSpecial.UserId = meetingPlan.UserId;
                    userBadgeSpecial.point = badgeSpecial.USAPoint;
                    userBadgeSpecial.ReceivedDate = DateTime.Now;
                    userBadgeSpecial.UpdateDate = DateTime.Now;
                    userBadgeSpecial.PID = meetingPlan.MeetingPlanId;

                    _db.UserBadgeSpecials.Add(userBadgeSpecial);
                    //}
                });

                _db.SaveChanges();
            }
        }

        /// <summary>
        /// post visit时如果有填写Tell us your experience添加积分
        /// (comment/rate bonus[General Comments on Post Visit]
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="userId"></param>
        /// <param name="pvid"></param>
        public void InsertPointForGeneralComments(string comment,int userId,int pvid)
        {
            if (!string.IsNullOrEmpty(comment))
            {
                string key = "GetUserTravelbadge_" + userId.ToString();
                if (_cache.Contains(key))
                    _cache.Remove(key);

                List<BadgeSpecial> list = _db.BadgeSpecials.Where(b => b.Type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity && b.USAActivity == "PostVisitGeneralComments" && b.StatusId == 0 && b.StartDate <= DateTime.Now && b.EndDate >= DateTime.Now).ToList();

                list.ForEach(badgeSpecial => {
                    //if (_db.UserBadgeSpecials.Count(u => u.UserId == userId && u.BadgeSpecialId == badgeSpecial.BadgeSpecialId) == 0)
                    //{
                    UserBadgeSpecial userBadgeSpecial = new UserBadgeSpecial();
                    userBadgeSpecial.BadgeSpecialId = badgeSpecial.BadgeSpecialId;
                    userBadgeSpecial.UserId = userId;
                    userBadgeSpecial.point = badgeSpecial.USAPoint;
                    userBadgeSpecial.ReceivedDate = DateTime.Now;
                    userBadgeSpecial.UpdateDate = DateTime.Now;
                    userBadgeSpecial.PID = pvid;

                    _db.UserBadgeSpecials.Add(userBadgeSpecial);
                    //}
                });

                _db.SaveChanges();
            }
        }
    }
}
