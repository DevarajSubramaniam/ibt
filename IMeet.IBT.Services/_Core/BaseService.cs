﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Entity;

using StructureMap;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Caching;
using IMeet.IBT.Common.Logging;

using IMeet.IBT.DAL;

namespace IMeet.IBT.Services
{
   public class BaseService:IDisposable
    {
        public ICache _cache;
        public ILogger _logger;

        public IBTEntities _db;

        public BaseService()
        {
            _cache = ObjectFactory.GetInstance<ICache>();
            _logger = ObjectFactory.GetInstance<ILogger>();

            _db = ObjectFactory.GetInstance<IBTEntities>();
        }


        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
