﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Email;
using IMeet.IBT.DAL;
using System.Collections;
using System.Threading.Tasks;

namespace IMeet.IBT.Services
{
    public class PersonalProfileService : BaseService //, IProfileService
    {

        private bool createPersonalProfile(int userID)
        {
            PersonalProfile pp = new PersonalProfile();
            PersonalProfilePhoto ppp = new PersonalProfilePhoto();

            pp.UserID = userID;
            pp.CoverPageURL = "/Staticfile/PersonalProfile/SharedPhotos/Cover_default1.jpg";
            pp.DisplayLinks = true;
            pp.DisplayIndustryProfessional = true;
            pp.PublishMeetingExperiences = true;
            pp.DisplayMeetingSiteManagement = true;
            pp.MeetingSiteManagement = 1;
            pp.DisplayMeetingSiteInspection = true;
            pp.MeetingSiteInspection = 1;
            pp.DisplayMeetingBusinessTrips = true;
            pp.MeetingBusinessTrips = 1;
            pp.DisplayMeetingDestinationFamiliarization = true;
            pp.MeetingDestinationFamiliarization = 1;
            pp.DisplayMeetingSpecialties = true;
            pp.DisplayMeetingChallenges = true;
            pp.PublishBookingContracting = true;
            pp.DisplayBookingEventsSources = true;
            pp.BookingEventsSources = 1;
            pp.DisplayBookingSpecialties = true;
            pp.DisplayBookingVenuesContracted = true;

            _db.PersonalProfiles.Add(pp);
            if (_db.SaveChanges() == 1)
            {
                setDefaultPersonalProfile(userID);
                return true;
            }

            return false;
        }

        private bool setDefaultPersonalProfile(int userId)
        {
            //get User's PPID
            var PPID = _db.PersonalProfiles.Where(p => p.UserID == userId).FirstOrDefault().PPID;

            //set default gallery photos
            PersonalProfilePhoto ppp;
            for (int i = 1; i < 7; i++)
            {
                ppp = new PersonalProfilePhoto();
                ppp.PPID = PPID;
                ppp.LocationPath = "/Staticfile/PersonalProfile/SharedPhotos/Gal_default" + i + ".jpg";
                _db.PersonalProfilePhotos.Add(ppp);
            }
            return _db.SaveChanges() == 1 ? true : false; ;
        }

        public bool updatePersonalProfile(int PPID, string userFirstName, string userLastName, string JobTitle, string CompanyName, bool DisplayLinks, bool DisplayIndustryProfessional,
    bool PublishMeetingExperiences, bool DisplayMeetingSiteManagement, int MeetingSiteManagement,
    bool DisplayMeetingSiteInspection, int MeetingSiteInspection, bool DisplayMeetingBusinessTrips,
    int MeetingBusinessTrips, bool DisplayMeetingDestinationFamiliarization, int MeetingDestinationFamiliarization,
    bool DisplayMeetingSpecialties, string MeetingSpecialties, bool DisplayMeetingChallenges,
    bool IsMeetingPolitical, bool IsMeetingStrike, bool IsMeetingNatural, bool IsMeetingGlobalAudience,
    bool IsMeetingNonHotelVenue, bool IsMeetingGroup500Attendees, bool IsMeetingVirtualComponent,
    bool IsMeetingCelebrity, bool PublishBookingContracting, bool DisplayBookingEventsSources,
    int BookingEventsSources, bool DisplayBookingSpecialties, string BookingSpecialties,
    bool DisplayBookingVenuesContracted)
        {

            PersonalProfile pp = _db.PersonalProfiles.FirstOrDefault(x => x.PPID == PPID);
            if (pp != null)
            {
                pp.User.Profile.Firstname = userFirstName;
                pp.User.Profile.Lastname = userLastName;
                pp.User.Profile.JobTitle = JobTitle;

                pp.Company = CompanyName;
                pp.DisplayLinks = DisplayLinks;
                pp.DisplayIndustryProfessional = DisplayIndustryProfessional;
                pp.PublishMeetingExperiences = PublishMeetingExperiences;
                pp.DisplayMeetingSiteManagement = DisplayMeetingSiteManagement;
                pp.MeetingSiteManagement = MeetingSiteManagement;
                pp.DisplayMeetingSiteInspection = DisplayMeetingSiteInspection;
                pp.MeetingSiteInspection = MeetingSiteInspection;
                pp.DisplayMeetingBusinessTrips = DisplayMeetingBusinessTrips;
                pp.MeetingBusinessTrips = MeetingBusinessTrips;
                pp.DisplayMeetingDestinationFamiliarization = DisplayMeetingDestinationFamiliarization;
                pp.MeetingDestinationFamiliarization = MeetingDestinationFamiliarization;
                pp.DisplayMeetingSpecialties = DisplayMeetingSpecialties;
                pp.MeetingSpecialties = MeetingSpecialties;
                pp.DisplayMeetingChallenges = DisplayMeetingChallenges;
                pp.IsMeetingPolitical = IsMeetingPolitical;
                pp.IsMeetingStrike = IsMeetingStrike;
                pp.IsMeetingNatural = IsMeetingNatural;
                pp.IsMeetingGlobalAudience = IsMeetingGlobalAudience;
                pp.IsMeetingNonHotelVenue = IsMeetingNonHotelVenue;
                pp.IsMeetingGroup500Attendees = IsMeetingGroup500Attendees;
                pp.IsMeetingVirtualComponent = IsMeetingVirtualComponent;
                pp.IsMeetingCelebrity = IsMeetingCelebrity;
                pp.PublishBookingContracting = PublishBookingContracting;
                pp.DisplayBookingEventsSources = DisplayBookingEventsSources;
                pp.BookingEventsSources = BookingEventsSources;
                pp.BookingEventsSources = BookingEventsSources;
                pp.DisplayBookingSpecialties = DisplayBookingSpecialties;
                pp.BookingSpecialties = BookingSpecialties;
                pp.DisplayBookingVenuesContracted = DisplayBookingVenuesContracted;

                if (_db.SaveChanges() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public int getPersonalProfileID(int userID)
        {
            PersonalProfile pp = _db.PersonalProfiles.FirstOrDefault(x => x.UserID == userID);
            if (pp != null)
            {
                return pp.PPID;
            }
            return -1;
        }

        public PersonalProfile getPersonalProfile(int userID)
        {
            var pp = _db.PersonalProfiles.Where(p => p.UserID == userID).FirstOrDefault();
            //var er = _db.PersonalProfiles.Find(2323);

            if (pp == null)
            {
                var uID = _db.Users.Where(u => u.UserId == userID).FirstOrDefault();
                if (uID != null)
                {
                    var result = createPersonalProfile(userID);
                    pp = _db.PersonalProfiles.Where(p => p.UserID == userID).FirstOrDefault();
                }

            }
            return pp;
        }

        public List<String[]> getCountriesByOrder(int userID)
        {
            List<string[]> resultList = new List<string[]>();
            int PPID = _db.PersonalProfiles.Where(c => c.UserID == userID).FirstOrDefault().PPID;

            var arrOrdered = _db.PersonalProfileCountriesOrdereds.Where(p => p.PPID == PPID).ToArray();

            //get the list of vidited countroes
            var arrAllCoutries = getCountries(userID);

            //first adds the ordered countries to top of the resultList
            foreach (var item in arrOrdered)
            {
                var _country = arrAllCoutries.Where(c => c[0] == item.CountryID.ToString()).FirstOrDefault();
                // check for the removed countries form the ordered list
                if (_country != null)
                {
                    resultList.Add(_country);
                }
            }

            //Second adds the remaining visited countries to the resultList
            foreach (var item in arrAllCoutries)
            {
                var _country = resultList.Where(c => c[0] == item[0]).FirstOrDefault();
                if (_country == null)
                {
                    resultList.Add(item);
                }
            }
            return resultList;
        }

        public List<String[]> getCountries(int _userID)
        {
            List<string[]> listCountries = new List<string[]>();
            //IBTEntities _ibtEntity = new IBTEntities();

            var countryList = _db.Countries.ToList();
            List<DAL.IBT> ibtUserList = new SupplierService().GetUserIbt(_userID);

            var result = (from c in countryList join u in ibtUserList on c.CountryId equals u.CountryId select new { c.CountryId, c.CountryName, c.CoordsLatitude, c.CoordsLongitude }).ToList();

            ibtUserList = ibtUserList.Where(i => i.CountryId > 0 && i.CityId == 0 && i.SupplyId == 0).ToList();

            foreach (Country item in countryList)
            {
                if (ibtUserList.Exists(i => i.CountryId == item.CountryId))
                {
                    //string[] sList = {item.CountryId.ToString(), item.CountryName};
                    listCountries.Add(new string[] { item.CountryId.ToString(), item.CountryName, item.FlagSrc, item.CoordsLatitude.ToString(), item.CoordsLongitude.ToString() });
                }
            }
            return listCountries;
        }

        public List<City> getCities(int userId)
        {
            var cityList = new IBTEntities().Cities.SqlQuery(@"SELECT [Cities].[CityId], [Cities].[CountryId],[Cities].[CityName],
                                                                      [Cities].[Rating],[Cities].[RecommendedCount],[Cities].[WanttoGoCount],
	                                                                  [Cities].[BucketListCount], [Cities].[StatusId],[Cities].[Direction],
	                                                                  [Cities].[CreateDate],[Cities].[UpdateDate],[Cities].[CoordsLatitude],
	                                                                  [Cities].[CoordsLongitude], [Cities].[Sort], [Cities].[FlagSrc]
                                                               FROM   [Cities], [IBT] 
                                                               WHERE  [IBT].[UserId] = {0}  AND 
                                                                      [IBT].[CityId] > 0 AND 
                                                                      [IBT].[SupplyId] = 0 AND
	                                                                  [Cities].[CityID] = [IBT].[CityId]", userId).ToList();

            return cityList;
        }

        public List<City> getCities(int userId, int countryID)
        {
            var cityList = new IBTEntities().Cities.SqlQuery(@"SELECT [Cities].[CityId], [Cities].[CountryId],[Cities].[CityName],
                                                                      [Cities].[Rating],[Cities].[RecommendedCount],[Cities].[WanttoGoCount],
	                                                                  [Cities].[BucketListCount], [Cities].[StatusId],[Cities].[Direction],
	                                                                  [Cities].[CreateDate],[Cities].[UpdateDate],[Cities].[CoordsLatitude],
	                                                                  [Cities].[CoordsLongitude], [Cities].[Sort], [Cities].[FlagSrc]
                                                               FROM   [Cities], [IBT] 
                                                               WHERE  [IBT].[UserId] = {0}  AND 
                                                                      [Cities].[CountryId] = {1} AND
                                                                      [IBT].[CityId] > 0 AND 
                                                                      [IBT].[SupplyId] = 0 AND
	                                                                  [Cities].[CityID] = [IBT].[CityId]", userId, countryID).ToList();

            return cityList;
        }

        public List<PersonalProfileLink> getLinks(int PPID)
        {
            //List<PersonalProfileLink> ppl=new List<PersonalProfileLink>();
            //var linksList = _db.PersonalProfileLinks.Where(L => L.PPID == PPID).ToList();

            //foreach (var link in linksList)
            //{
            //    //url format checking
            //    var UriChecking = Uri.IsWellFormedUriString(link.Address, UriKind.RelativeOrAbsolute);
            //    if (UriChecking)
            //    {
            //        link.URI = new UriBuilder(link.Address).Uri.ToString();
            //        ppl.Add(link); 
            //    }
            //}

            //linksList = null;

            return _db.PersonalProfileLinks.Where(L => L.PPID == PPID).ToList();
            // return ppl;
        }

        public List<PersonalProfilePhoto> getPhotos(int PPID)
        {
            return _db.PersonalProfilePhotos.Where(P => P.PPID == PPID).ToList();
        }

        public void addLinksList(int PersonalProfileID, string strLinks)
        {
            PersonalProfileLink ppl = new PersonalProfileLink();
            //remove the brackets in string
            strLinks = strLinks.Substring(1);
            strLinks = strLinks.Substring(0, (strLinks.Length - 1));

            //split the strLinks to an array of links
            var arrLinks = strLinks.Split(',');

            //remove all the user's links from database
            var delObjList = _db.PersonalProfileLinks.Where(l => l.PPID == PersonalProfileID).ToList();
            foreach (var item in delObjList)
            {
                _db.PersonalProfileLinks.Remove(item);
            }
            var result = _db.SaveChanges();

            //add links for each user
            for (int i = 0; i < arrLinks.Count(); i++)
            {
                ppl.PPID = PersonalProfileID;
                ppl.Address = arrLinks[i];

                //url format checking
                var UriChecking = Uri.IsWellFormedUriString(arrLinks[i], UriKind.RelativeOrAbsolute);
                if (!string.IsNullOrEmpty(arrLinks[i]) && UriChecking)
                {
                    ppl.URI = new UriBuilder(arrLinks[i]).Uri.ToString();
                }
                else
                {
                    ppl.URI = "";
                }

                _db.PersonalProfileLinks.Add(ppl);

                result = _db.SaveChanges();
            }
        }

        public void addProfessionalsList(int PersonalProfileID, string strProfessionals)
        {
            if (!string.IsNullOrEmpty(strProfessionals))
            {
                PersonalProfileProfessional ppp = new PersonalProfileProfessional();

                //remove the brackets in string
                strProfessionals = strProfessionals.Substring(1);
                strProfessionals = strProfessionals.Substring(0, (strProfessionals.Length - 1));

                //split the strLinks to an array of links
                var arrProfessionals = strProfessionals.Split(',');

                //remove all the user's links from database
                var delObjList = _db.PersonalProfileProfessionals.
                    Where(l => l.PPID == PersonalProfileID);

                foreach (var item in delObjList)
                {
                    _db.PersonalProfileProfessionals.Remove(item);
                }

                var result = _db.SaveChanges();

                //add Professionals for user
                for (int i = 0; i < arrProfessionals.Count(); i++)
                {
                    ppp.PPID = PersonalProfileID;
                    ppp.Professional = arrProfessionals[i];
                    _db.PersonalProfileProfessionals.Add(ppp);

                    //  addLink(PersonalProfileID, arrLinks[i]);
                    result = _db.SaveChanges();
                }
            }
        }

        public void addPersonalProfileCountriesOrdered(int PersonalProfileID, String countriesOrdered)
        {
            //remove the brackets in string
            countriesOrdered = countriesOrdered.Substring(1);
            countriesOrdered = countriesOrdered.Substring(0, (countriesOrdered.Length - 1));

            //split the arrCountriesOrdered  to an array of links
            var arrCountriesOrdered = countriesOrdered.Split(',');

            //remove all the user's links from database
            var delObjList = _db.PersonalProfileCountriesOrdereds.Where(l => l.PPID == PersonalProfileID).ToList();
            foreach (var item in delObjList)
            {
                _db.PersonalProfileCountriesOrdereds.Remove(item);
            }
            var result = _db.SaveChanges();

            //add countries for each user
            for (int i = 0; i < arrCountriesOrdered.Count(); i++)
            {
                var dsdsse = arrCountriesOrdered[i];
                PersonalProfileCountriesOrdered ppco = new PersonalProfileCountriesOrdered();
                ppco.PPID = PersonalProfileID;
                ppco.CountryID = int.Parse(arrCountriesOrdered[i]);
                _db.PersonalProfileCountriesOrdereds.Add(ppco);

            }
            result = _db.SaveChanges();
        }

        public ArrayList suggestVenues(string Venue)
        {
            ArrayList arrList = new ArrayList();
            arrList.Add("Point 1");
            arrList.Add("Point 2");
            arrList.Add("Point 3");

            return arrList;
        }

        public Boolean addPhotoToGallery(int userId, String virtualPath)
        {
            var PPID = _db.PersonalProfiles.Where(p => p.UserID == userId).FirstOrDefault().PPID;
            PersonalProfilePhoto ppp = new PersonalProfilePhoto();
            ppp.LocationPath = virtualPath;
            ppp.PPID = PPID;

            _db.PersonalProfilePhotos.Add(ppp);
            return _db.SaveChanges() == 1 ? true : false;
        }

        public Boolean removePhotoFromGallery(int userId, String virtualPath)
        {
            var PPID = _db.PersonalProfiles.Where(p => p.UserID == userId).FirstOrDefault().PPID;

            ///Staticfile/PersonalProfile/SharedPhotos/Gal_default2.jpg

            var nodeToRemove = _db.PersonalProfilePhotos.Where(p => p.PPID == PPID && p.LocationPath == virtualPath).FirstOrDefault();
            _db.PersonalProfilePhotos.Remove(nodeToRemove);

            return _db.SaveChanges() == 1 ? true : false;
        }

        public Boolean addPersonalPhoto(int userId, String virtualPath)
        {
            PersonalProfile pp = _db.PersonalProfiles.Where(p => p.UserID == userId).FirstOrDefault();

            if (pp != null)
            {
                //HttpServerUtility hsu; = new HttpServerUtility();
                String oldProfilePhoto = pp.ProfilePhotoURL;
                var OldPhotoPhisicalPath = HttpContext.Current.Server.MapPath(oldProfilePhoto);
                if (System.IO.File.Exists(OldPhotoPhisicalPath))
                {
                    System.IO.File.Delete(OldPhotoPhisicalPath);
                }
            }
            pp.ProfilePhotoURL = virtualPath;
            return _db.SaveChanges() == 1 ? true : false;
        }

        public Boolean updatePersonalCoverPhoto(int userId, String virtualPath)
        {
            PersonalProfile pp = _db.PersonalProfiles.Where(p => p.UserID == userId).FirstOrDefault();
            if (pp != null)
            {
                //remove old photo
                String oldCoverPageURL = pp.CoverPageURL;
                if (!oldCoverPageURL.Contains("Cover_default"))
                {
                    var OldPhotoPhisicalPath = HttpContext.Current.Server.MapPath(oldCoverPageURL);
                    if (System.IO.File.Exists(OldPhotoPhisicalPath))
                    {
                        System.IO.File.Delete(OldPhotoPhisicalPath);
                    }
                }
                pp.CoverPageURL = virtualPath;
            }
            return _db.SaveChanges() == 1 ? true : false;
        }


    }
}
