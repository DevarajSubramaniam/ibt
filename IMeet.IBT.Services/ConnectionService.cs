﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Email;
using IMeet.IBT.DAL;
using StructureMap;

namespace IMeet.IBT.Services
{
    public class ConnectionService : BaseService, IConnectionService
    {
        private INewsFeedService _newsFeedService;
        public IProfileService _profileService;
        public ConnectionService()
        {
            _newsFeedService = ObjectFactory.GetInstance<INewsFeedService>();
            _profileService = ObjectFactory.GetInstance<IProfileService>();
        }

        #region Invite Friend
        public BoolMessageItem<int> InviteFriend(List<string> emailList, string msg, string firstname, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther)
        {
            Connection connection = new Connection();
            string newemail = "";
            foreach (var email in emailList)
            {
                newemail = email;
                var q = from o in _db.Users
                        where o.Email.Equals(newemail)
                        select o;
                if (q.Count() > 0)
                {
                    return new BoolMessageItem<int>(0, false, "Your friend is registered with I've Been There!");
                }
                else
                {
                    EmailService toemail = new EmailService("Email\\en-us");
                    //string strReceive = user.UserName;
                    toemail.SendInviteMail(newemail, msg, firstname, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther);

                }
            }
            return new BoolMessageItem<int>(1, true, "An email invitation has been successfully sent to your friend!");

        }
        #endregion

        #region Connection
        public int GetConnectionListCount(int userId, int status)
        {
            return 0;
        }

        /// <summary>
        /// Connection Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ConnectionListItem> GetConnectionList(int userId, int statusId, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total)
        {

            IList<ConnectionListItem> list = _db.Database.SqlQuery<ConnectionListItem>("EXEC IBT_ConnectionList {0},{1},{2},{3},{4},{5}", filterField, userId, statusId, filterKeyword, page, pageSize).ToList<ConnectionListItem>();
            total = list.Count();
            if (!string.IsNullOrWhiteSpace(filterLetter))
            {
                return (from c in list where c.FirstName.ToUpper().StartsWith(filterLetter) orderby c.ConnectionId descending select c).ToList<ConnectionListItem>();
            }
            else
            {
                return (from c in list orderby c.ConnectionId descending select c).ToList<ConnectionListItem>();
            }

            //todo 下面还需要返回好友数
        }

        /// <summary>
        /// Connection Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<ConnectionListItem> GetRequestList(int userId, int statusId, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total)
        {

            IList<ConnectionListItem> list = _db.Database.SqlQuery<ConnectionListItem>("EXEC IBT_RequestList {0},{1},{2},{3},{4}", userId, statusId, filterKeyword, page, pageSize).ToList<ConnectionListItem>();
            total = list.Count();

            //int startRecord = (page - 1) * pageSize;
            if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            {
                //todo:GetMyList filterField
                return new List<ConnectionListItem>();
            }
            else if (!string.IsNullOrWhiteSpace(filterLetter))
            {
                return (from c in list where c.FirstName.ToUpper().StartsWith(filterLetter) orderby c.ConnectionId descending select c).ToList<ConnectionListItem>();
            }
            else
            {
                return (from c in list orderby c.ConnectionId descending select c).ToList<ConnectionListItem>();
            }

            //todo 下面还需要返回好友数
        }

        /// <summary>
        /// 返回所有好友列表
        /// </summary>
        /// <returns></returns>
        public List<Profile> GetAllConnection(int userId)
        {
            var t = from p in _db.Connections
                    where p.UserId.Equals(userId) && p.StatusId == 1
                    select new
                    {
                        p.FriendId
                    };

            //var q = from n in _db.Connections
            //        join b in _db.Users on n.UserId equals b.UserId
            //        join s in _db.Profiles on n.UserId equals s.UserId
            //        where n.UserId.Equals(t)
            //        orderby n.ConnectionId descending
            //        select new
            //        {
            //            n.ConnectionId,
            //            n.UserId,
            //            b.UserName,
            //            b.Email,
            //            s.Firstname,
            //            s.Lastname,
            //            s.ProfileImage
            //        };
            //return q.ToList();
            //q.Count()//这个就是记录总数

            var connection = from c in _db.Profiles
                             join p in _db.Connections
                             on c.UserId equals p.FriendId
                             where c.UserId.Equals(t)
                             select c;
            return connection.ToList();
        }

        /// <summary>
        /// 返回下面的所有好友列表
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public IList<Connection> GetConList(int userId, int statusId)  //好友——>  添加确认1 请求0 删除3 拒绝忽略2
        {
            return (from c in _db.Connections
                    where (c.UserId.Equals(userId) && c.StatusId.Equals(statusId))
                    orderby c.ConnectionId ascending
                    select c).ToList<Connection>();
        }

        public BoolMessageItem<int> RemoveConnection(int userId, int friendId)
        {
            try
            {
                string key = "GetConnectionCount_" + userId.ToString();
                //Connection connection = _db.Connections.FirstOrDefault(x => (x.UserId.Equals(userId) && x.FriendId.Equals(friendId))||(x.UserId.Equals(friendId)&&x.FriendId.Equals(userId)));
                var query = from x in _db.Connections
                            where (x.UserId.Equals(userId) && x.FriendId.Equals(friendId)) || (x.UserId.Equals(friendId) && x.FriendId.Equals(userId)) //过滤集合
                            select x;
                foreach (var q in query)
                {
                    q.StatusId = 3;
                    q.UpdateDate = DateTime.Now;
                }
                _db.SaveChanges();
                _cache.Remove(key);  //清除缓存
                return new BoolMessageItem<int>(1, true, "Good friend relationship removes success！");
            }
            catch
            {
                return new BoolMessageItem<int>(0, false, "Good friend relationship removes failure！");
            }
        }

        public BoolMessageItem<int> SendRequest(int userId, int friendId)
        {
            try
            {
                //xg
                var connection = _db.Connections.Where(c => c.UserId.Equals(userId) && c.FriendId.Equals(friendId)).FirstOrDefault();
                ProfileService profileService = new ProfileService();
                if (connection != null)
                {
                    if (connection.StatusId == 1)
                    {
                        return new BoolMessageItem<int>(0, false, "The other is your friend!");
                    }
                    else
                    {
                        var query = from x in _db.Connections
                                    where (x.UserId.Equals(userId) && x.FriendId.Equals(friendId)) || (x.UserId.Equals(friendId) && x.FriendId.Equals(userId)) //过滤集合
                                    select x;
                        foreach (var q in query)
                        {
                            q.StatusId = 0;
                            q.UpdateDate = DateTime.Now;
                        }
                        _db.SaveChanges();
                        return new BoolMessageItem<int>(1, true, "You have successfully sent a connection request!");
                    }
                }
                else if (friendId == profileService.GetUserId())
                {
                    return new BoolMessageItem<int>(0, false, "Can not add to myself!");
                }
                else
                {
                    string key = "GetConnectionCount_" + userId.ToString();
                    //Connection[] connections = new Connection[2] { new Connection { UserId = userId, FriendId = friendId, StatusId = 0 }, new Connection { UserId = friendId, FriendId = userId, StatusId = 0 } };
                    //_db.Connections.Aggregate(connections);
                    _db.Connections.Add(new Connection { UserId = userId, FriendId = friendId, StatusId = 0, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });
                    _db.SaveChanges();
                    //_db.Connections.Add(new Connection { UserId = friendId, FriendId = userId, StatusId = 0 });
                    //_db.SaveChanges();
                    _cache.Remove(key);  //清除缓存
                    return new BoolMessageItem<int>(1, true, "You have successfully sent a connection request!");
                }

            }
            catch (Exception ex)
            {
                return new BoolMessageItem<int>(0, false, "You have not successfully sent a connection request. Please try again!");
            }

        }

        public BoolMessageItem<int> CreateConnection(int userId, int friendId) //同意确认
        {
            try
            {
                Connection connection = _db.Connections.Where(c => c.UserId.Equals(userId) && c.FriendId.Equals(friendId)).FirstOrDefault();
                if (connection == null)
                {
                    _db.Connections.Add(new Connection { UserId = userId, FriendId = friendId, StatusId = 0, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });
                }
                else
                {
                    connection.StatusId = 0;
                    connection.UpdateDate = DateTime.Now;
                }
                _db.SaveChanges();
                var query = from x in _db.Connections
                            where (x.UserId.Equals(userId) && x.FriendId.Equals(friendId)) || (x.UserId.Equals(friendId) && x.FriendId.Equals(userId)) //过滤集合
                            select x;
                foreach (var q in query)
                {
                    q.StatusId = 1;
                    q.UpdateDate = DateTime.Now;
                }
                _db.SaveChanges();
                string key = "GetRequestCount_" + userId.ToString();
                _cache.Remove(key);  //清除缓存

                //需要添加记录feed  —添加好友(修改)
                string yourName = _profileService.GetUserProfile(userId).Firstname;
                string friendName = _profileService.GetUserProfile(friendId).Firstname;
                string experiences = yourName + " is now friends with " + friendName.ToString() + ".";
                string experiences2 = friendName + " is now friends with " + yourName.ToString() + ".";
                _newsFeedService.AddNewsFeed(userId, 0, 0, 0, Identifier.IBTType.User, Identifier.FeedActiveType.AddNewFriend, experiences, friendId.ToString(), null);
                _db.NewsFeeds.Add(new NewsFeed { UserId = friendId, CountryId = 0, CityId = 0, SupplyId = 0, IBTType = (int)Identifier.IBTType.User, ActiveType = (int)Identifier.FeedActiveType.AddNewFriend, ObjVal = experiences2, ObjData = userId.ToString(), StatusId = 1, CreateDate = DateTime.Now, CommentCount = 0 });
                _db.SaveChanges();

                return new BoolMessageItem<int>(1, true, "You have successfully added a connection request!");

                //Connection connection = _db.Connections.FirstOrDefault(x => x.UserId == userId && x.FriendId == friendId);
                //if (connection != null && connection.StatusId == 0)
                //{
                //    connection.StatusId = 1;
                //    _db.SaveChanges();
                //    return new BoolMessageItem<int>(connection.ConnectionId, true, "好友添加成功！");
                //}
                //else
                //{
                //    return new BoolMessageItem<int>(0, false, "好友添加失败！");
                //}
            }
            catch
            {
                return new BoolMessageItem<int>(0, false, "You have not successfully added a connection request. Please try again!");
            }
        }
        public BoolMessageItem<int> IgnoreConnection(int userId, int friendId)  //StatusId=2 拒绝(忽略)
        {
            try
            {
                _db.Connections.Add(new Connection { UserId = userId, FriendId = friendId, StatusId = 0, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });
                _db.SaveChanges();

                var query = from x in _db.Connections
                            where (x.UserId.Equals(userId) && x.FriendId.Equals(friendId)) || (x.UserId.Equals(friendId) && x.FriendId.Equals(userId)) //过滤集合
                            select x;
                foreach (var q in query)
                {
                    q.StatusId = 2;
                    q.UpdateDate = DateTime.Now;
                }
                _db.SaveChanges();
                string key = "GetRequestCount_" + userId.ToString();
                _cache.Remove(key);  //清除缓存
                return new BoolMessageItem<int>(1, true, "Operation success！");
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<int>(0, false, ex.Message);
            }
            //Connection connection = _db.Connections.FirstOrDefault(x => x.UserId == userId && x.FriendId == friendId);
            //if (connection != null && connection.StatusId == 0)
            //{
            //    connection.StatusId = 2;
            //    _db.SaveChanges();
            //    return new BoolMessageItem<int>(connection.ConnectionId, true, "操作成功！");
            //}
            //else
            //{
            //    return new BoolMessageItem<int>(0, false, "操作失败！");
            //}
        }
        #endregion

        #region Message_Reply

        public int GetMessageListCount(int userId)
        {
            return 0;
        }

        /// <summary>
        /// 返回所有好友列表（Firstnmae,Lastname,UserId）
        /// </summary>
        /// <returns></returns>
        public IList<ConnectionListItem> GetAllConnectionProfile(int userId, string name)
        {
            string sql = @"SELECT  dbo.Profiles.Firstname, dbo.Profiles.Lastname,dbo.Connections.FriendId
                            FROM  dbo.Connections 
                            LEFT JOIN dbo.Profiles ON dbo.Connections.FriendId = dbo.Profiles.UserId 
                            LEFT JOIN dbo.Users ON dbo.Users.UserId = dbo.Profiles.UserId
                            WHERE dbo.Connections.StatusId=1 AND dbo.Connections.UserId=" + userId.ToString() + @" AND Profiles.UserId IN (SELECT dbo.Connections.FriendId FROM dbo.Connections WHERE UserId= " + userId.ToString() + @") 
                            AND ( dbo.Profiles.Firstname LIKE '%" + name.ToString() + @"%' OR dbo.Profiles.Lastname LIKE '%" + name.ToString() + @"%')";
            return _db.Database.SqlQuery<ConnectionListItem>(sql).ToList<ConnectionListItem>();
        }

        /// <summary>
        /// Message Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<MsgListItem> GetMessageList(int userId, bool IsDel, string strWhere, int page, int pageSize, string filterKeyword, string filterField, out int total)
        {

            IList<MsgListItem> list = _db.Database.SqlQuery<MsgListItem>("EXEC IBT_MessageList {0},{1},{2},{3},{4},{5}", userId, false, strWhere, filterKeyword, page, pageSize).ToList<MsgListItem>();
            total = list.Count();
            if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            {
                //todo:GetMyList filterField
                return new List<MsgListItem>();
            }
            else
            {
                return (from c in list orderby c.CreateDate descending select c).ToList<MsgListItem>();
            }

            //todo 下面还需要返回消息数
        }

        /// <summary>
        /// Reply Lists 的数据
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="IsDel"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<MsgListItem> GetReplyList(int msgId, bool IsDel, out int total)
        {

            IList<MsgListItem> list = _db.Database.SqlQuery<MsgListItem>("EXEC IBT_ReplyList {0},{1}", msgId, false).ToList<MsgListItem>();
            total = list.Count();
            return (from c in list orderby c.CreateDate ascending select c).ToList<MsgListItem>();
            //todo 下面还需要返回消息数
        }

        /// <summary>
        /// 返回用户ID下面的所有消息
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public IList<Messge> GetMsgList(int msgId, int toUserId, int fromUserId)
        {
            if (fromUserId.ToString() == "0")
            {
                return (from c in _db.Messges
                        where (c.MsgId.Equals(msgId) && (c.ToUserId.Equals(toUserId) || c.FormUserId.Equals(toUserId)) && c.ReplyId == 0 && c.IsDel == false)
                        orderby c.CreateDate ascending
                        select c).ToList<Messge>();
            }
            else
            {
                return (from c in _db.Messges
                        where (c.MsgId.Equals(msgId) && (c.ToUserId.Equals(toUserId) || c.FormUserId.Equals(toUserId)) && c.ReplyId == 0 && c.IsDel == false)
                        orderby c.CreateDate ascending
                        select c).ToList<Messge>();
            }
        }
        /// <summary>
        /// 根据用户ID返回用户的消息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DAL.Messge GetUserMsg(int toUserId, int fromUserId)
        {
            var msg = _db.Messges.Where(c => c.FormUserId.Equals(fromUserId) && c.ToUserId.Equals(toUserId)).FirstOrDefault();
            if (msg != null)
                return msg;
            else
            {
                return new DAL.Messge();
            }

        }
        //标记已读(点击触发)
        public BoolMessageItem<int> SetMsgRead(int msgId)
        {
            ProfileService profileService = new ProfileService();
            Messge msg = _db.Messges.FirstOrDefault(x => x.MsgId.Equals(msgId));
            if (msg != null)
            {
                msg.IsRead = true;
                _db.SaveChanges();
                string rating_sql = @"UPDATE [Messges]
                                           SET 
                                              [IsRead] =1 
                                         WHERE [ReplyId]={0}";
                _db.Database.ExecuteSqlCommand(rating_sql, msgId);
                string key = "GetMsgCount_" + profileService.GetUserId().ToString();
                _cache.Remove(key);
                return new BoolMessageItem<int>(msg.MsgId, true, "");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "");
            }
        }
        public BoolMessageItem<int> SetIsRead(int msgId)
        {
            ProfileService profileService = new ProfileService();
            Messge msg = _db.Messges.FirstOrDefault(x => x.MsgId.Equals(msgId));
            if (msg != null)
            {
                string tipMsg = "";
                if (msg.IsRead == false)
                {
                    msg.IsRead = true;
                    tipMsg = "Marked as Read.";
                }
                else
                {
                    msg.IsRead = false;
                    tipMsg = "Marked as Unread.";
                }
                _db.SaveChanges();
                string key = "GetMsgCount_" + profileService.GetUserId().ToString();
                _cache.Remove(key);
                return new BoolMessageItem<int>(msg.MsgId, true, tipMsg);
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Marked failure！");
            }
        }
        public BoolMessageItem<int> SetAllRead(List<int> MsgDataId)
        {
            try
            {
                int msgId = 0;
                ProfileService profileService = new ProfileService();
                Messge msg = new Messge();
                for (int i = 0; i < MsgDataId.Count(); i++)
                {
                    msgId = MsgDataId[i];
                    msg = _db.Messges.FirstOrDefault(x => x.MsgId.Equals(msgId));
                    if (msg != null)
                    {
                        if (msg.IsRead == false)
                            msg.IsRead = true;
                        else
                            msg.IsRead = false;
                        _db.SaveChanges();
                        string key = "GetMsgCount_" + profileService.GetUserId().ToString();
                        _cache.Remove(key);
                    }

                }
                return new BoolMessageItem<int>(1, true, "Marked success!");
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<int>(0, false, ex.Message);
            }
        }

        public BoolMessageItem<int> SetIsDel(int msgId)
        {
            ProfileService profileService = new ProfileService();
            Messge msg = _db.Messges.FirstOrDefault(x => x.MsgId.Equals(msgId));
            if (msg != null && msg.IsDel == false)
            {
                msg.IsDel = true;
                _db.SaveChanges();
                string key = "GetMsgCount_" + profileService.GetUserId().ToString();
                _cache.Remove(key);
                return new BoolMessageItem<int>(msg.MsgId, true, "Deleted successful！");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Deleted failure！");
            }
        }
        public BoolMessageItem<int> SetAllDel(List<int> MsgDataId)
        {
            try
            {
                int msgId = 0;
                ProfileService profileService = new ProfileService();
                Messge msg = new Messge();
                for (int i = 0; i < MsgDataId.Count(); i++)
                {
                    msgId = MsgDataId[i];
                    msg = _db.Messges.FirstOrDefault(x => x.MsgId.Equals(msgId));
                    if (msg != null && msg.IsDel == false)
                    {
                        msg.IsDel = true;
                        _db.SaveChanges();
                        string key = "GetMsgCount_" + profileService.GetUserId().ToString();
                        _cache.Remove(key);

                    }
                }
                return new BoolMessageItem<int>(1, true, "Deleted successful!");
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<int>(0, false, ex.Message);
            }
        }

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="fromUserId"></param>
        /// <param name="toUserId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public BoolMessage SendMessage(int fromUserId, int toUserId, string comments)
        {
            try
            {
                if (toUserId == fromUserId)
                {
                    return new BoolMessageItem<int>(0, false, "Can't give oneself to send message!");
                }
                else
                {
                    ProfileService profileService = new ProfileService();
                    Messge message = new Messge();
                    var msg = _db.Messges.Where(c => c.FormUserId.Equals(fromUserId) && c.ToUserId.Equals(toUserId)).FirstOrDefault();
                    if (msg != null)
                    {
                        message.ReplyId = msg.MsgId;
                    }
                    else
                    {
                        message.ReplyId = 0;
                    }
                    message.FormUserId = fromUserId;
                    message.ToUserId = toUserId;
                    message.Content = comments;
                    message.CreateDate = DateTime.Now;
                    message.UpdateDate = DateTime.Now;
                    message.IsDel = false;
                    message.IsFav = false;
                    message.IsRead = false;

                    _db.Messges.Add(message);
                    _db.SaveChanges();
                    string key = "GetMsgCount_" + profileService.GetUserId().ToString();
                    _cache.Remove(key);
                    return new BoolMessageItem<int>(message.MsgId, true, "Send successful!");
                }


            }
            catch (Exception ex)
            {
                _logger.Error("SendMessage,toUserId:" + toUserId.ToString(), ex);
                return new BoolMessage(false, "");
            }
        }

        /// <summary>
        /// Send Reply
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="fromUserId"></param>
        /// <param name="toUserId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public BoolMessage SendReply(int msgId, int fromUserId, int toUserId, string comments)
        {
            try
            {
                if (toUserId == fromUserId)
                {
                    return new BoolMessageItem<int>(0, false, "Can't give oneself to send message!");
                }
                else
                {
                    ProfileService profileService = new ProfileService();
                    Messge message = new Messge();
                    message.ReplyId = msgId;
                    message.FormUserId = fromUserId;
                    message.ToUserId = toUserId;
                    message.Content = comments;
                    message.CreateDate = DateTime.Now;
                    message.UpdateDate = DateTime.Now;
                    message.IsDel = false;
                    message.IsFav = false;
                    message.IsRead = false;

                    _db.Messges.Add(message);
                    _db.SaveChanges();
                    string key = "GetMsgCount_" + profileService.GetUserId().ToString();
                    _cache.Remove(key);
                    return new BoolMessageItem<int>(message.MsgId, true, "Send successful!");
                }


            }
            catch (Exception ex)
            {
                _logger.Error("SendMessage,toUserId:" + toUserId.ToString(), ex);
                return new BoolMessage(false, "");
            }
        }
        #endregion

    }
}
