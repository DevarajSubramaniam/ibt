﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.DAL.Models;
using System.Data.SqlClient;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using StructureMap;
using System.Text.RegularExpressions;

namespace IMeet.IBT.Services
{
    public class IBTService : BaseService, IIBTService
    {
        private ISupplierService _supplierService;
        private INewsFeedService _newsFeedService;
        public IBTService()
        {
            _supplierService = ObjectFactory.GetInstance<ISupplierService>();
            _newsFeedService = ObjectFactory.GetInstance<INewsFeedService>();
        }

        #region country and city

        /// <summary>
        /// get country by id
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public DAL.Country GetCountry(int countryId)
        {
            return _db.Countries.Find(countryId);
        }

        /// <summary>
        /// get country descript by countryId(cache)
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public CountriesDe GetCountryDes(int countryId)
        {
            string key = "IBTCountryDes_" + countryId.ToString();
            var countriesDe = _cache.GetOrInsert<CountriesDe>(key, 30, true, () =>
            {
                return _db.CountriesDes.FirstOrDefault(c => c.CountryId == countryId) ?? new CountriesDe();
            });
            if (countriesDe.CountryId == 0)
                return null;
            else
                return countriesDe;
        }
        /// <summary>
        /// get country descript by countryId
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public CountriesDe GetCountryDesInfo(int countryId)
        {
            return _db.CountriesDes.FirstOrDefault(c => c.CountryId == countryId);
        }

        /// <summary>
        /// 获取国家的城市数
        /// </summary>
        /// <param name="countryId">国家Id</param>
        /// <returns></returns>
        public int CityCount(int countryId)
        {
            string key = "IBTCountry_CityCount" + countryId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                return _db.Cities.Count(c => c.CountryId == countryId && c.StatusId == (int)Identifier.StatusType.Enabled);
            });
        }

        /// <summary>
        /// 获取国家的Supplies数量
        /// </summary>
        /// <param name="countryId">CountryId</param>
        /// <returns></returns>
        public int SuppliersCount(int countryId)
        {
            string key = "IBTCountry_SupplierCount" + countryId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                return _db.Supplies.Count(s => s.CountryId == countryId && s.StatusId == (int)Identifier.StatusType.Enabled);
            });
        }

        /// <summary>
        /// 获取国家的Members数量
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public int MembersCount(int countryId)
        {
            string key = "IBTCountry_MenbersCount" + countryId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                return _db.Profiles.Count(p => p.CountryId == countryId);
            });
        }

        /// <summary>
        /// ibt friends count
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public int IBTFriendsCount(int userId, int countryId, int cityId, int supplyId)
        {
            return (from i in _db.IBTs
                    where (from u in _db.Connections where u.UserId.Equals(userId) select u.FriendId).Contains(i.UserId)
                    && i.CountryId == countryId && i.CityId == cityId && i.SupplyId == supplyId
                    select i).Count();
        }

        /// <summary>
        /// get user ibtcount
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public int IBTCount(int userId, int countryId, int cityId, int supplyId)
        {
            var ibt = _db.IBTs.FirstOrDefault(i => i.UserId == userId && i.CountryId == countryId && i.CityId == cityId && i.SupplyId == supplyId);
            return ibt == null ? 0 : ibt.IbtCount;
        }

        /// <summary>
        /// get people ibt count
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public int IBTPeopleCount(int countryId, int cityId, int supplyId)
        {
            string key = "IBT_IBTPeopleCount_" + countryId.ToString() + "_" + cityId.ToString() + "_" + supplyId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                return _db.IBTs.Count(i => i.CountryId == countryId && i.CityId == cityId && i.SupplyId == supplyId && i.StatusId == (int)Identifier.StatusType.Enabled);
            });
        }

        /// <summary>
        /// 获取City
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public City GetCity(int cityId)
        {
            return _db.Cities.Find(cityId);
        }

        /// <summary>
        /// 获取城市的Supplier数量
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public int CountSupplierForCity(int cityId)
        {
            string key = "IBTCity_SupplierCount" + cityId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                return _db.Supplies.Count(s => s.CityId == cityId && s.StatusId == (int)Identifier.StatusType.Enabled);
            });
        }

        /// <summary>
        /// 获取城市的Menber数量
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public int CountMembersCountForCity(int cityId)
        {
            string key = "IBTCity_MemberCount" + cityId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                return _db.Profiles.Count(p => p.CityId == cityId);
            });
        }

        /// <summary>
        /// SpecialsOfCity
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<Media> GetSpecialsOfCity(int cityId, int pageNumber, int pageSize, out int total)
        {
            int startSize = (pageNumber - 1) * pageSize + 1;
            int endSize = startSize + pageSize - 1;
            string strSql = @"SELECT 	t1.* FROM (
                                  SELECT ROW_NUMBER() OVER (ORDER BY Title ASC) AS RowId, * FROM dbo.Medias 
                              WHERE CityId={0} AND Position=1 AND Convert(varchar(10),ExpiryDate,120) >= Convert(varchar(10),getdate(),120) AND StatusId=1
                            ) As t1
	                        WHERE RowId BETWEEN " + startSize.ToString() + " AND " + endSize.ToString() + "";
            var list = _db.Database.SqlQuery<Media>(strSql, cityId).ToList<Media>();
            total = list.Count();
            return list;
        }
        /// <summary>
        /// 获取用户been there次数
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int GetHistoryCount(int tabId, int ibtType, int objId, int userId)
        {
            string strWhere = "";
            string strActive = "";
            if (tabId == 2)
            {
                strActive = " AND ActiveType IN (5,6,7)";
            }
            if (ibtType == (int)Identifier.IBTType.Country)
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.CountryId=" + objId.ToString();
            }
            else if (ibtType == (int)Identifier.IBTType.City)
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.CityId=" + objId.ToString();
            }
            else
            {
                strWhere = strActive + " AND dbo.NewsFeed.IBTType=" + ibtType.ToString() + " AND dbo.NewsFeed.SupplyId=" + objId.ToString();
            }

            int total = 0;
            var list = _newsFeedService.GetFeedHistoryList(strWhere, userId, out total);

            return list.Count();
        }

        /// <summary>
        /// edit Country & CountryDes
        /// </summary>
        /// <param name="country">Country</param>
        /// <param name="description">CountriesDe</param>
        /// <returns></returns>
        public BoolMessageItem<int> EditCountry(Country country, CountriesDe description)
        {
            try
            {
                Country editCountry = null;
                Country findCountry = null;
                //编辑国家信息
                if (country.CountryId > 0)
                {
                    findCountry = _db.Countries.FirstOrDefault(c => c.CountryId != country.CountryId && c.CountryName == country.CountryName);
                    //判断编辑后的国家的名称是否已经存在，有的话把该国家状态设置为未删除
                    if (findCountry != null)
                    {
                        findCountry.StatusId = (int)Identifier.StatusType.Enabled;
                        _db.SaveChanges();
                        return new BoolMessageItem<int>(findCountry.CountryId, false, "False:the country is exist!");
                    }
                    editCountry = this.GetCountry(country.CountryId);
                }
                //新增国家信息
                else
                {
                    findCountry = _db.Countries.FirstOrDefault(c => c.CountryName == country.CountryName);
                    //判断是否有重名国家，有的话把该国家状态设置为未删除
                    if (findCountry != null)
                    {
                        findCountry.StatusId = (int)Identifier.StatusType.Enabled;
                        _db.SaveChanges();
                        return new BoolMessageItem<int>(findCountry.CountryId, false, "False:the country is exist!");
                    }
                    editCountry = new Country();
                    editCountry.CreateDate = DateTime.Now;
                    editCountry.Continent = " ";
                    editCountry.Sort = 0;
                    editCountry.FlagSrc = " ";
                    editCountry.StatusId = (int)Identifier.StatusType.Enabled;
                    _db.Countries.Add(editCountry);
                }


                if (!string.IsNullOrEmpty(country.FlagSrc))
                {
                    editCountry.FlagSrc = country.FlagSrc;
                }
                editCountry.CountryName = country.CountryName;
                editCountry.UpdateDate = DateTime.Now;
                editCountry.CoordsLongitude = country.CoordsLongitude;
                editCountry.CoordsLatitude = country.CoordsLatitude;

                //更新或创建国家描述
                CountriesDe des = _db.CountriesDes.FirstOrDefault(d => d.CountryId == country.CountryId);
                if (des == null)
                {
                    _db.SaveChanges();
                    des = new CountriesDe();
                    des.CountryId = editCountry.CountryId;
                    des.CountryName = editCountry.CountryName;
                    _db.CountriesDes.Add(des);
                }
                des.CountryTxt = description.CountryTxt;
                des.TTD = description.TTD;
                des.SN = description.SN;
                des.FAD = description.FAD;
                des.Events = description.Events;
                des.UpdateDate = DateTime.Now;

                _db.SaveChanges();
                return new BoolMessageItem<int>(0,true, "Success");
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<int>(0, false, ex.Message);
            }
        }

        /// <summary>
        /// edit city
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public BoolMessageItem<int> EditCity(City city)
        {
            try
            {
                City editCity = null;
                City findCity = null;
                if (city.CityId > 0)
                {
                    findCity = _db.Cities.FirstOrDefault(c => c.CityId != city.CityId && c.CityName == city.CityName && c.CountryId == city.CountryId && c.StatusId == (int)Identifier.StatusType.Enabled);
                    //（编辑）判断是否有重名城市，存在则把该城市的状态设为未删除
                    if (findCity != null)
                    {
                        findCity.StatusId = (int)Identifier.StatusType.Enabled;
                        _db.SaveChanges();
                        return new BoolMessageItem<int>(findCity.CityId, false, "False:city name is exist!");
                    }
                    editCity = this.GetCity(city.CityId);
                }
                else
                {
                    findCity = _db.Cities.FirstOrDefault(c => c.CityName == city.CityName && c.CountryId == city.CountryId);
                    //(新增)判断是否有重名城市，存在则把该城市的状态设为未删除
                    if (findCity != null)
                    {
                        findCity.StatusId = (int)Identifier.StatusType.Enabled;
                        _db.SaveChanges();
                        return new BoolMessageItem<int>(findCity.CityId, false, "False:city name is exist!");
                    }
                    else
                    {
                        editCity = new City();
                        editCity.Direction = " ";
                        editCity.Sort = 0;
                        editCity.StatusId = (int)Identifier.StatusType.Enabled;
                        editCity.CreateDate = DateTime.Now;
                        _db.Cities.Add(editCity);
                    }
                }

                if (!string.IsNullOrEmpty(city.FlagSrc))
                {
                    editCity.FlagSrc = city.FlagSrc;
                }
                editCity.CountryId = city.CountryId;
                editCity.CityName = city.CityName;
                editCity.CoordsLongitude = city.CoordsLongitude;
                editCity.CoordsLatitude = city.CoordsLatitude;
                editCity.UpdateDate = DateTime.Now;

                if (!string.IsNullOrEmpty(city.CityDescription.Txt) || !string.IsNullOrEmpty(city.CityDescription.Url) || !string.IsNullOrEmpty(city.CityDescription.PhotoUrl) || !string.IsNullOrEmpty(city.CityDescription.TTDUrl) || !string.IsNullOrEmpty(city.CityDescription.ShoppingUrl) || !string.IsNullOrEmpty(city.CityDescription.RestaurantsUrl) || !string.IsNullOrEmpty(city.CityDescription.HotelsUrl) || !string.IsNullOrEmpty(city.CityDescription.EventsUrl))
                {
                    if (editCity.CityDescription == null)
                    {
                        editCity.CityDescription = new CityDescription();
                    }
                    editCity.CityDescription.Txt = city.CityDescription.Txt;
                    editCity.CityDescription.Url = city.CityDescription.Url;
                    editCity.CityDescription.PhotoUrl = city.CityDescription.PhotoUrl;
                    editCity.CityDescription.TTDUrl = city.CityDescription.TTDUrl;
                    editCity.CityDescription.ShoppingUrl = city.CityDescription.ShoppingUrl;
                    editCity.CityDescription.RestaurantsUrl = city.CityDescription.RestaurantsUrl;
                    editCity.CityDescription.HotelsUrl = city.CityDescription.HotelsUrl;
                    editCity.CityDescription.EventsUrl = city.CityDescription.EventsUrl;
                    editCity.CityDescription.UpdateDate = DateTime.Now;
                }
                _db.SaveChanges();
                return new BoolMessageItem<int>(editCity.CityId,true, editCity.CityId.ToString());
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<int>(0, false, ex.Message);
            }
        }

        /// <summary>
        /// 导入城市description
        /// </summary>
        /// <param name="fs"></param>
        /// <returns></returns>
        public BoolMessage ImportCityDescript(FileStream fs)
        {
            try
            {
                List<City> cities = _db.Cities.ToList();
                IWorkbook wk = new HSSFWorkbook(fs);
                //获取excel中第一个表
                ISheet sheet = wk.GetSheetAt(0);
                for (int i = 1; i < sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    if (row.GetCell(0) != null)
                    {
                        int cityId = Convert.ToInt32(row.GetCell(0).ToString());
                        City city = cities.FirstOrDefault(c => c.CityId == cityId);
                        if (city != null)
                        {
                            string url = @"http://www.worldtravelguide.net";
                            city.FlagSrc = "/Staticfile/flag/cityflag/cityflag_170_170_"+city.CityId.ToString()+".jpg";
                            CityDescription des = new CityDescription();
                            if (city.CityDescription != null)
                                des = city.CityDescription;
                            des.UpdateDate = DateTime.Now;
                            if (row.GetCell(4) != null)
                            {//第i行第5个单元格
                                des.Txt = row.GetCell(4).ToString();
                            }
                            if (row.GetCell(5) != null)
                            {
                                des.Url = url + row.GetCell(5).ToString();
                            }
                            if (row.GetCell(6) != null)
                            {
                                des.PhotoUrl = row.GetCell(6).ToString();
                            }
                            if (row.GetCell(7) != null)
                            {
                                des.TTDUrl = url + row.GetCell(7).ToString();
                            }
                            if (row.GetCell(8) != null)
                            {
                                des.ShoppingUrl = url + row.GetCell(8).ToString();
                            }
                            if (row.GetCell(9) != null)
                            {
                                des.RestaurantsUrl = url + row.GetCell(9).ToString();
                            }
                            if (row.GetCell(10) != null)
                            {
                                des.HotelsUrl = url + row.GetCell(10).ToString();
                            }
                            if (row.GetCell(11) != null)
                            {
                                des.EventsUrl = url + row.GetCell(11).ToString();
                            }
                            city.CityDescription = des;
                        }
                    }
                }
                _db.SaveChanges();
                return new BoolMessage(true, "Success");
            }
            catch (Exception ex) 
            {
                return new BoolMessage(false, ex.Message);
            }
        }

        #endregion

        #region RRWB

        /// <summary>
        /// get RRWB
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="objId"></param>
        /// <param name="ibtType"></param>
        /// <returns></returns>
        public RRWB GetRRWB(int userId, int objId, int ibtType)
        {
            return _db.RRWBs.FirstOrDefault(r => r.UserId == userId && r.ObjId == objId && r.IBTType == ibtType);
        }

        #endregion

        #region Attachment(图片)

        /// <summary>
        /// 设置Attachment为删除状态
        /// </summary>
        /// <param name="attId">attId</param>
        /// <returns></returns>
        public BoolMessage AttachmentDel(int attId)
        {
            string sql = "UPDATE dbo.Attachment SET StatusId={0} WHERE AttId={1}";
            _db.Database.ExecuteSqlCommand(sql, (int)Identifier.StatusType.Disabled, attId);
            return new BoolMessage(true, string.Empty);
        }
        /// <summary>
        /// 根据post visit的附件删除newsfeed的附件
        /// </summary>
        /// <param name="pvAttId"></param>
        /// <returns></returns>
        public BoolMessage AttachmentDelForPVAttID(int pvAttId)
        {
            PostVistAttachment pvAtt = _db.PostVistAttachments.Find(pvAttId);
            if (pvAtt != null)
            {
                Attachment att = _db.Attachments.FirstOrDefault(a => a.Guid == pvAtt.Guid && a.SourceFileName == pvAtt.SourceFileName);
                if (att != null)
                {
                    att.StatusId = (int)Identifier.StatusType.Disabled;
                    _db.SaveChanges();
                }
            }
            return new BoolMessage(true, string.Empty);
        }

        /// <summary>
        /// change attachment statusid
        /// </summary>
        /// <param name="attId">attId</param>
        /// <param name="statusId">statusType</param>
        /// <returns></returns>
        public BoolMessage AttachmentChange(int attId, int statusId)
        {
            string sql = "UPDATE dbo.Attachment SET StatusId={0} WHERE AttId={1}";
            _db.Database.ExecuteSqlCommand(sql, statusId, attId);
            return new BoolMessage(true, string.Empty);
        }

        /// <summary>
        /// 根据guid更新attachment的objId
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="objId"></param>
        public void SetAttachmentObjId(string guid, int objId)
        {
            string sql = "UPDATE dbo.Attachment SET ObjId={0} WHERE Guid={1}";
            _db.Database.ExecuteSqlCommand(sql, objId, guid);
        }

        /// <summary>
        /// 根据attid更新attachment的objid
        /// </summary>
        /// <param name="attid"></param>
        /// <param name="objId"></param>
        public void SetAttachmentObjIdByAttId(string attid, int objId)
        {
            string sql = "UPDATE dbo.Attachment SET ObjId={0} WHERE AttId={1}";
            _db.Database.ExecuteSqlCommand(sql, objId, attid);
        }

        /// <summary>
        /// 根据用户ID获取附件
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="attType"></param>
        /// <returns></returns>
        public IList<Attachment> GetAttachmentList(int userId, int attType)
        {
            List<Attachment> list = null;
            switch (attType)
            {
                case (int)Identifier.AttachmentType.NewsFeed:
                    {
                        string sql = "SELECT * FROM dbo.Attachment WHERE ObjId IN(SELECT NewsFeedId FROM dbo.NewsFeed WHERE UserId={0}) AND AttType={1}";
                        list = _db.Database.SqlQuery<Attachment>(sql, userId, attType).ToList();
                        break;
                    }
                default:
                    break;
            }
            return list;
        }
        /// <summary>
        /// 根据id获取attachment
        /// </summary>
        /// <param name="attid">attid</param>
        /// <returns></returns>
        public Attachment GetAttachmentByAttid(int attid)
        {
            var att = (from a in _db.Attachments where a.AttId == attid select a).FirstOrDefault();
            return att;
        }
        /// <summary>
        /// 获取attachment list(分页)
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IList<Attachment> GetAttachmentListByPage(string strWhere, int page, int pageSize)
        {
            string sql = "EXEC IBT_GetAttachmentListByPage {0},{1},{2}";
            var list = _db.Database.SqlQuery<Attachment>(sql, strWhere, page, pageSize);

            //var list = (from a in _db.Attachments where ids.Contains(a.ObjId) && a.AttType == (int)type select a);
            //list = list.OrderByDescending(a => a.AttId).Skip((page - 1) * pageSize + 1).Take(pageSize);
            return list.ToList();

        }

        /// <summary>
        /// 添加attachment
        /// </summary>
        /// <param name="att"></param>
        public void AddAttachment(Attachment att)
        {
            _db.Attachments.Add(att);
            _db.SaveChanges();
        }

        /// <summary>
        /// newsFeed上传的图片处理
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="attType"></param>
        /// <param name="guid"></param>
        /// <param name="orgFilePath"></param>
        /// <returns></returns>
        public BoolMessageItem<int> AttachmentNewsFeedImage(int userId, int attType, string guid, string orgFilePath) 
        {
            int dir = userId / 500;
            string attPath = "/Attachment/comments/" + attType.ToString() + "/" + dir.ToString() + "/" + DateTime.Now.ToString("yyyyMM") + "/";
            string path = System.Web.HttpContext.Current.Server.MapPath("~" + attPath);
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);
            string fileName = Guid.NewGuid().ToString().Replace("-", "");
            try
            {
                var bms = this.ImageSave(orgFilePath, path, fileName);
                if (!bms.Success)
                    throw new Exception(bms.Message);

                Attachment att = new Attachment()
                {
                    Guid = guid,
                    AttType = attType,
                    SourceFileName = attPath + fileName,
                    FileName = fileName,
                    StatusId = (int)Identifier.StatusType.Enabled,
                    CreateData = DateTime.Now
                };
                this.AddAttachment(att);
                string url = attPath + fileName + "_tn.jpg";
                return new BoolMessageItem<int>(att.AttId, true, url);
            }
            catch (Exception ex)
            {
                _logger.Error("用户上传文件(Attachment)", ex);
                return new BoolMessageItem<int>(0, false, string.Empty);
            }
        }

        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="orgFilePath"></param>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public BoolMessageItem<string> ImageSave(string orgFilePath, string path, string fileName)
        {
            try {
                //生成原图
                System.IO.File.Copy(orgFilePath, path + fileName + "_ori.jpg", true);
                //Cut
                IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_tn.jpg", (int)Identifier.SuppilyPhotoSize._tn, (int)Identifier.SuppilyPhotoSize._tn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.Cut);
                Image o = Image.FromFile(path + fileName + "_ori.jpg");
                //big
                if (o.Width > (int)Identifier.SuppilyPhotoSize._big || o.Height > (int)Identifier.SuppilyPhotoSize._big)
                {
                    IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_big.jpg", (int)Identifier.SuppilyPhotoSize._big, (int)Identifier.SuppilyPhotoSize._big, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                }
                else
                {
                    if (System.IO.File.Exists(path + fileName + "_big.jpg"))
                        System.IO.File.Delete(path + fileName + "_big.jpg");
                    System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_big.jpg");
                }
                //_570x430 //用于前台页面相册的popul
                if (o.Width > 570 || o.Height > 430)
                {
                    IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_570x430.jpg", 570, 430, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                }
                else
                {
                    if (System.IO.File.Exists(path + fileName + "_570x430.jpg"))
                        System.IO.File.Delete(path + fileName + "_570x430.jpg");
                    System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_570x430.jpg");
                }
                //bn
                if (o.Width > (int)Identifier.SuppilyPhotoSize._bn || o.Height > (int)Identifier.SuppilyPhotoSize._bn)
                {
                    IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_bn.jpg", (int)Identifier.SuppilyPhotoSize._bn, (int)Identifier.SuppilyPhotoSize._bn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                }
                else
                {
                    if (System.IO.File.Exists(path + fileName + "_bn.jpg"))
                        System.IO.File.Delete(path + fileName + "_bn.jpg");
                    System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_bn.jpg");
                }
                //large
                if (o.Width > (int)Identifier.SuppilyPhotoSize._large || o.Height > (int)Identifier.SuppilyPhotoSize._large)
                {
                    IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_large.jpg", (int)Identifier.SuppilyPhotoSize._large, (int)Identifier.SuppilyPhotoSize._large, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                }
                else
                {
                    if (System.IO.File.Exists(path + fileName + "_large.jpg"))
                        System.IO.File.Delete(path + fileName + "_large.jpg");
                    System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_large.jpg");
                }
                //mn
                if (o.Width > (int)Identifier.SuppilyPhotoSize._mn || o.Height > (int)Identifier.SuppilyPhotoSize._mn)
                {
                    IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_mn.jpg", (int)Identifier.SuppilyPhotoSize._mn, (int)Identifier.SuppilyPhotoSize._mn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                }
                else
                {
                    if (System.IO.File.Exists(path + fileName + "_mn.jpg"))
                        System.IO.File.Delete(path + fileName + "_mn.jpg");
                    System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_mn.jpg");
                }
                //xtn
                if (o.Width > (int)Identifier.SuppilyPhotoSize._xtn || o.Height > (int)Identifier.SuppilyPhotoSize._xtn)
                {
                    IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(path + fileName + "_ori.jpg", path + fileName + "_xtn.jpg", (int)Identifier.SuppilyPhotoSize._xtn, (int)Identifier.SuppilyPhotoSize._xtn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
                }
                else
                {
                    if (System.IO.File.Exists(path + fileName + "_xtn.jpg"))
                        System.IO.File.Delete(path + fileName + "_xtn.jpg");
                    System.IO.File.Copy(path + fileName + "_ori.jpg", path + fileName + "_xtn.jpg");
                }
                return new BoolMessageItem<string>(fileName, true, "success");
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<string>(string.Empty, false, ex.Message);
            }
        }

        /// <summary>
        /// 图片翻转
        /// </summary>
        /// <param name="id">Attachment id</param>
        /// <returns></returns>
        public BoolMessageItem<string> AttachmentRotate(int userId, int id, int rotate)
        {
            try
            {
                Attachment attachment = _db.Attachments.Find(id);
                if (attachment == null)
                    return new BoolMessageItem<string>(string.Empty, false, "Attachment is null");
                int attType = attachment.AttType;
                //临时文件
                string fileName = attachment.SourceFileName + "_ori.jpg";
                string tempPath = HttpContext.Current.Server.MapPath("~/Staticfile/temp/");
                string tempFile = tempPath + Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(fileName);
                Bitmap image = new Bitmap(HttpContext.Current.Server.MapPath("~" + attachment.SourceFileName + "_ori.jpg"));
                Graphics graphics = Graphics.FromImage(image);
                switch (rotate)
                {
                    case 3:
                        {
                            image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            break;
                        }
                    case 2:
                        {
                            image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            break;
                        }
                    case 1:
                        {
                            image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
                graphics.DrawImage(image, 0, 0);
                image.Save(tempFile);

                int dir = userId / 500;
                string attPath = "/Attachment/comments/" + attType.ToString() + "/" + dir.ToString() + "/" + DateTime.Now.ToString("yyyyMM") + "/";
                string path = System.Web.HttpContext.Current.Server.MapPath("~" + attPath);
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                string saveFileName = Guid.NewGuid().ToString("N");

                var bms = this.ImageSave(tempFile, path, saveFileName);
                if (bms.Success)
                {
                    attachment.SourceFileName = attPath + saveFileName;
                    _db.SaveChanges();
                    return new BoolMessageItem<string>(attachment.SourceFileName, true, "success");
                }
                else
                    return bms;
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<string>(string.Empty, false, ex.Message);
            }
        }

        /// <summary>
        /// 对postvisit时上传的图片旋转
        /// </summary>
        /// <param name="attId"></param>
        /// <param name="rotate"></param>
        /// <returns></returns>
        public BoolMessageItem<string> PVImageRotate(int attId, int rotate, string basePath)
        {
            try
            {
                PostVistAttachment pva = _db.PostVistAttachments.Find(attId);

                if (pva == null)
                    throw new Exception("Attachment not exists!");

                Attachment attachment = _db.Attachments.FirstOrDefault(a => a.Guid == pva.Guid && a.SourceFileName == pva.SourceFileName);

                if (attachment == null)
                    throw new Exception("Attachment not exists!");

                if (attachment == null)
                {
                    return new BoolMessageItem<string>(string.Empty, false, "Attachment is null");
                }

                //Bitmap oriImage = new Bitmap(HttpContext.Current.Server.MapPath("~" + attachment.SourceFileName + "_ori.jpg"));

                string newFileName = System.Guid.NewGuid().ToString().Substring(0, 15).Replace("-", "");

                string oldFileName = Regex.Match(attachment.SourceFileName, @"\w*$").Value;

                string newSourceFileName = attachment.SourceFileName.Replace(oldFileName, newFileName);

                //图片旋转
                this.ImageRotate(basePath + attachment.SourceFileName + "_ori.jpg", basePath + newSourceFileName + "_ori.jpg", rotate);

                //生成不同大小的图片
                this.PVImageCut(basePath + newSourceFileName + "_ori.jpg", basePath + newSourceFileName);

                //删除旧图片
                this.DelOldImage(basePath + attachment.SourceFileName);

                pva.SourceFileName = newSourceFileName;
                attachment.SourceFileName = newSourceFileName;
                _db.SaveChanges();

                return new BoolMessageItem<string>("", true, newSourceFileName);
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<string>(string.Empty, false, ex.Message);
            }
        }

        /// <summary>
        /// 图片旋转后保存
        /// </summary>
        /// <param name="file"></param>
        /// <param name="saveFile"></param>
        /// <param name="rotate"></param>
        private void ImageRotate(string file,string saveFile,int rotate)
        {
            Bitmap oriImage = new Bitmap(file);

            Graphics graphics = Graphics.FromImage(oriImage);

            switch (rotate)
            {
                case 3:
                    {
                        oriImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;
                    }
                case 2:
                    {
                        oriImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        break;
                    }
                case 1:
                    {
                        oriImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            graphics.DrawImage(oriImage, 0, 0);
            oriImage.Save(saveFile);
        }

        /// <summary>
        /// 生成post visit切割的图片
        /// </summary>
        /// <param name="file"></param>
        /// <param name="saveName"></param>
        private void PVImageCut(string file, string saveName)
        {
            Image image = Image.FromFile(file);

            //cut
            IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(file, saveName + "_tn.jpg", (int)Identifier.SuppilyPostVisitPhotoSize._tn, (int)Identifier.SuppilyPostVisitPhotoSize._tn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.Cut);

            //big
            if (image.Width > (int)Identifier.SuppilyPostVisitPhotoSize._big || image.Height > (int)Identifier.SuppilyPostVisitPhotoSize._big)
                IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(file, saveName + "_big.jpg", (int)Identifier.SuppilyPostVisitPhotoSize._big, (int)Identifier.SuppilyPostVisitPhotoSize._big, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
            else
                System.IO.File.Copy(file, saveName + "_big.jpg", true);

            //_570x430 
            //用于前台页面相册的popul
            if (image.Width > 570 || image.Height > 430)
                IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(file, saveName + "_570x430.jpg", 570, 430, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
            else
                System.IO.File.Copy(file, saveName + "_570x430.jpg");

            //bn
            if (image.Width > (int)Identifier.SuppilyPhotoSize._bn || image.Height > (int)Identifier.SuppilyPhotoSize._bn)
                IMeet.IBT.Common.Utilities.MakeThumbImage.MakeThumbPhoto(file, saveName + "_bn.jpg", (int)Identifier.SuppilyPhotoSize._bn, (int)Identifier.SuppilyPhotoSize._bn, Common.Utilities.MakeThumbImage.ThumbPhotoModel.H_W);
            else
                System.IO.File.Copy(file, saveName + "_bn.jpg");

            image.Dispose();
        }

        /// <summary>
        /// 删除pv旋转前的旧图片
        /// </summary>
        /// <param name="fileInfo"></param>
        private void DelOldImage(string fileInfo)
        {
            string delFileName = Regex.Match(fileInfo, @"\w*$").Value;

            string dirtoryPath = fileInfo.Replace(delFileName, "");

            DirectoryInfo directory = new DirectoryInfo(dirtoryPath);

            var files = directory.GetFiles();

            files.ForEach(file => {
                if (file.Name.Contains(delFileName))
                {
                    file.Delete();
                }
            });

        }
        #endregion

        #region news feed

        /// <summary>
        /// get newsfeed infomation
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <returns></returns>
        public NewsFeed GetNewsFeedInfo(int newsFeedId)
        {
            return _db.NewsFeeds.Find(newsFeedId);
        }

        /// <summary>
        /// 加载feedlist的Attachment
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public IList<FeedList> GetFeedListAttachment(List<FeedList> list)
        {
            list.ForEach(f =>
            {
                f.AttachmentList = _db.Attachments.Where(a => a.AttType == (int)Identifier.AttachmentType.NewsFeed && a.StatusId == (int)Identifier.StatusType.Enabled && a.ObjId == f.NewsFeedId).ToList();
            });
            return list;
        }

        /// <summary>
        /// 获取Feedcomment
        /// </summary>
        /// <param name="newsFeedId"></param>
        /// <returns></returns>
        public IList<FeedcommentsEx> GetFeedcommentEx(int newsFeedId, int pageIndex, int pageSize)
        {
            pageIndex = pageIndex >= 0 ? pageIndex : 1;
            pageSize = pageSize >= 0 ? pageSize : 10;
            int startRow = (pageIndex - 1) * pageSize + 1;
            int endRow = pageIndex * pageSize;
            string sql = @"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY dbo.FeedComments.feedCommentId DESC) AS RowId,[feedCommentId],[NewsFeedId],dbo.Profiles.[UserId],[Comments],[StatusId],[CreateDate],dbo.Profiles.Firstname,dbo.Profiles.ProfileImage 
                            FROM dbo.FeedComments
                            LEFT JOIN dbo.Profiles on dbo.FeedComments.UserId=dbo.Profiles.UserId
                            WHERE NewsFeedId={0}) T1
                            WHERE RowId BETWEEN {1} AND {2} ORDER BY CreateDate DESC";
            var list = _db.Database.SqlQuery<FeedcommentsEx>(sql, newsFeedId, startRow, endRow).ToList<FeedcommentsEx>();
            list.Reverse();
            return list;
        }

        /// <summary>
        /// get feed list
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <param name="ibtType"></param>
        /// <param name="dt">查询时间点</param>
        /// <param name="pageIndex">页数</param>
        /// <param name="pageSize">大小</param>
        /// <returns></returns>
        public IList<FeedList> GetFeedList(int countryId, int cityId, int supplyId, int ibtType, DateTime dt, int pageIndex, int pageSize)
        {
            pageIndex = pageIndex < 1 ? 1 : pageIndex;
            pageSize = pageSize < 1 ? 10 : pageSize;
            int startSize = (pageIndex - 1) * pageSize + 1;
            int endSize = pageSize * pageIndex;
            string sql = @"exec IBT_GetFeedListPagerData {0},{1},{2},{3},{4},{5},{6}";
            var list = _db.Database.SqlQuery<FeedList>(sql, ibtType, countryId, cityId, supplyId, dt, startSize, endSize).ToList<FeedList>();
            if (list.Count > 0)
            {
                list = _supplierService.GetFeedRRWB(list);
                list = this.GetFeedListAttachment(list).ToList();
            }
            return list;
        }

        #endregion

    }
}
