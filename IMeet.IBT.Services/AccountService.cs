﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Encrypt;
using IMeet.IBT.Common.Email;
using IMeet.IBT.DAL;
using System.IO;
using StructureMap;

namespace IMeet.IBT.Services
{
    public class AccountService : BaseService, IAccountService
    {
        private INewsFeedService _newsFeedService;
        public IProfileService _profileService;
        public AccountService()
        {
            _newsFeedService = ObjectFactory.GetInstance<INewsFeedService>();
            _profileService = ObjectFactory.GetInstance<IProfileService>();
        }
        public void SignIn(int userId, bool createPersistentCookie)  //Remember me
        {
            FormsAuthentication.SetAuthCookie(userId.ToString(), createPersistentCookie);
        }

        public BoolMessageItem<int> CreateUser(string firstname, string lastname, string password, string email, int countryId, int cityId, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther, int userType, string source, bool isPleasure, bool isBusiness)
        {
            //string userName2 = "test";

            //EmailService toemail2 = new EmailService("Email\\en-us");
            //toemail2.SendRegistrationMail(userName2, "123", "luwei@techsailor.com", "luwei");
            //return new BoolMessageItem<int>(1, true, "");


            var q = from o in _db.Users
                    where o.Email.Equals(email)
                    select o;
            if (q.Count() > 0)
            {
                return new BoolMessageItem<int>(0, false, "This email is regstered with I've Been There. Please add in another email address!");
            }
            else
            {
                User user = new User();
                Profile profile = new Profile();
                user.UserName = "";
                //user.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5"); //加密
                user.Password = password;
                user.Email = email;
                user.IsApproved = 1;
                user.LastLoginDate = DateTime.Now;
                user.LastLoginIp = Utils.GetIP();
                user.LoginNum = 1;
                user.CreateDate = DateTime.Now;
                user.Comment = "";
                user.StatusId = 0;
                user.RoleId = 0;
                user.UserType = userType;
                user.Source = source;
                user.IsBusiness = isBusiness;
                user.IsPleasure = isPleasure;
                user.IsEmailSubscribe = true; //unsubscribe problem
                _db.Users.Add(user);
                _db.SaveChanges();

                profile.UserId = user.UserId;
                profile.Firstname = firstname;
                profile.Lastname = lastname;
                profile.AlternateEmail = "";
                profile.Email = email;
                profile.Gender = 2;
                profile.StatusUpdate = "";
                profile.Username = "";
                profile.CurrentLoginSnsId = 0; //xg
                profile.CountryId = countryId;
                profile.CityId = cityId;
                profile.Sort = 0;
                _db.Profiles.Add(profile);
                _db.SaveChanges();
                //pv
                int newUserId = user.UserId;
                DateTime? pvDate = null;
                SupplierService supplierService = new SupplierService();

                supplierService.SavePostVisit(1, newUserId, (int)Identifier.IBTType.Country, countryId, 0, 0, "", "", pvDate);
                supplierService.SavePostVisit(1, newUserId, (int)Identifier.IBTType.City, countryId, cityId, 0, "", "", pvDate);

                //send an email to the user's email
                EmailService toemail = new EmailService("Email\\en-us");
                toemail.SendRegistrationMail(email, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther);
                

                //if (user.IsApproved == 0)  //激活邮件
                //{
                //    EmailService toemail = new EmailService("Email\\en-us");
                //    //string strReceive = user.UserName;
                //    toemail.SendRegistrationMail(email);
                //}
                return new BoolMessageItem<int>(user.UserId, true, "You are now a member of IBT!");  //不用激活
            }
        }

        public BoolMessageItem<int> JudgeUser(string email, string password)
        {
            //判断是否email登录
            User user = new User();
            user = _db.Users.FirstOrDefault(c => c.Email.Equals(email) && c.Password.Equals(password));
            if (user != null)
            {
                if (user.Password != password.ToString())
                {
                    return new BoolMessageItem<int>(0, false, "Sorry, the password is incorrect.");
                }
                else if (user.IsApproved != 1)
                {
                    return new BoolMessageItem<int>(0, false, "Sorry, the account is not activate.");
                }
                else
                {
                    user.LoginNum++;
                    user.LastLoginDate = DateTime.Now;
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(user.UserId, true, "Welcome to I've Been There Home page!");
                }
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "The account is incorrect");
            }
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        /// <summary>
        /// 返回用户的UserId
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public BoolMessageItem<int> ValidateUser(string userName, string password)
        {
            //判断是否email登录
            User user = new User();
            if (userName.IndexOf("@") > -1)
            {
                user = _db.Users.FirstOrDefault(c => c.Email.Equals(userName));
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(userName))
                    user = _db.Users.FirstOrDefault(c => c.UserName.Equals(userName));
            }
            if (user != null)
            {
                if (user.Password != password.ToString())  //加密不需要
                {
                    //FormsAuthentication.SetAuthCookie(userName, true);
                    //Session["CurrentUser"] = name;
                    return new BoolMessageItem<int>((int)Identifier.FailureInfoType.PassowrdIncorrect, false, "Sorry, the password is incorrect.");
                }
                else if (user.IsApproved != 1)
                {
                    return new BoolMessageItem<int>((int)Identifier.FailureInfoType.AccountNotActivate, false, "Sorry, the account is not activate.");
                }
                else
                {
                    user.LoginNum++;
                    user.LastLoginIp = Utils.GetIP();
                    user.LastLoginDate = DateTime.Now;
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(user.UserId, true, "Welcome to I've Been There Home page!");
                }
            }
            else
            {
                return new BoolMessageItem<int>((int)Identifier.FailureInfoType.AccountIncorrect, false, "The account is incorrect");
            }


        }

        /// <summary>
        /// 返回用户的UserId(管理员)
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public BoolMessageItem<int> ValidateAdminUser(string userName, string password)
        {
            //判断是否email登录
            User user = new User();
            if (userName.IndexOf("@") > -1)
            {
                user = _db.Users.FirstOrDefault(c => c.Email.Equals(userName));
            }
            else
            {
                user = _db.Users.FirstOrDefault(c => c.UserName.Equals(userName));
            }
            if (user != null)
            {
                if (user.Password != password.ToString())  //加密不需要
                {
                    //FormsAuthentication.SetAuthCookie(userName, true);
                    //Session["CurrentUser"] = name;
                    return new BoolMessageItem<int>(0, false, "Sorry, the password is incorrect.");
                }
                else if (user.IsApproved != 1)
                {
                    return new BoolMessageItem<int>(0, false, "Sorry, the account is not activate.");
                }
                else if (user.RoleId != 1)
                {
                    return new BoolMessageItem<int>(0, false, "The account is not admin!");
                }
                else
                {
                    user.LoginNum++;
                    user.LastLoginDate = DateTime.Now;
                    _db.SaveChanges();
                    return new BoolMessageItem<int>(user.UserId, true, "Welcome to I've Been There Admin page!");
                }
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "The account is incorrect!");
            }


        }

        //更新用户个人信息资料
        public BoolMessageItem<int> UpdateProfile(int userId, string username, string firstname, string lastname, DateTime birth, string email, string newemail, string altemail, int gender, string statusUpdate, int countryId, int cityId, int usertype, bool isPleasure, bool isBusiness, bool isEmailSubscribe)
        {
            string key = "user_" + userId.ToString();
            string existingemail = string.Empty;
            string existingnewemail = string.Empty;

            Profile profile = _db.Profiles.FirstOrDefault(x => x.UserId == userId);
            if (profile != null)
            {
                User user = _db.Users.FirstOrDefault(u => u.UserId == userId);
                if (user != null)
                {

                    existingemail = user.Email;
                    //判断用户的usertype是否发生变化
                    if (user.UserType != usertype)
                    {
                        string key2 = "GetUserTravelbadge_" + userId.ToString();
                        if (_cache.Contains(key2))
                            _cache.Remove(key2);
                    }
                    user.UserType = usertype;
                    user.IsPleasure = isPleasure;
                    user.IsBusiness = isBusiness;
                    user.IsEmailSubscribe = isEmailSubscribe;
                    if (user.NewEmailId != null)
                        existingnewemail = user.NewEmailId;

                    try
                    {
                        if (newemail.ToLower().Trim() != string.Empty && email.ToLower().Trim() != newemail.ToLower().Trim() && existingnewemail.ToLower().Trim() != newemail.ToLower().Trim())
                        {
                            user.NewEmailId = newemail;
                            user.NewEmailIdVerfication = Guid.NewGuid().ToString().ToLower().Trim();
                            user.NewEmailIdVerficationActivate = 0;
                            EmailService toemail = new EmailService("Email\\en-us");

                            toemail.NewEmailAddressAdded(email, newemail, username, firstname);
                            toemail.NewEmailVerification(newemail, username, firstname, user.NewEmailIdVerfication);

                            User user2 = _db.Users.FirstOrDefault(u => u.Email == newemail);
                            User user3 = _db.Users.FirstOrDefault(u => u.NewEmailId == newemail);
                            if (user2 != null)
                            {
                                return new BoolMessageItem<int>(0, false, "Update failure! Mail id already in use.");
                            }
                            else if (user3 != null)
                            {
                                return new BoolMessageItem<int>(0, false, "Update failure! Mail id already in use.");
                            }
                        }
                        else if (newemail.ToLower().Trim() != string.Empty && email.ToLower().Trim() != newemail.ToLower().Trim() && existingnewemail.ToLower().Trim() == newemail.ToLower().Trim())
                        {
                            user.NewEmailId = newemail;
                            user.NewEmailIdVerfication = user.NewEmailIdVerfication;

                            User user2 = _db.Users.FirstOrDefault(u => u.Email == newemail);
                            User user3 = _db.Users.FirstOrDefault(u => u.NewEmailId == newemail);
                            if (user2 != null)
                            {
                                return new BoolMessageItem<int>(0, false, "Update failure! Mail id already in use.");
                            }
                            else if (user3 != null)
                            {
                                return new BoolMessageItem<int>(0, false, "Update failure! Mail id already in use.");
                            }
                        }
                        else
                        {
                            user.NewEmailId = user.NewEmailIdVerfication = string.Empty;
                        }

                        _db.SaveChanges();
                    }
                    catch { }
                }
                profile.Username = username;
                profile.Firstname = firstname;
                profile.Lastname = lastname;
                profile.Birth = birth;
                profile.Email = existingemail;
                profile.AlternateEmail = altemail;
                profile.Gender = Convert.ToByte(gender);
                //profile.ProfileImage = ProfileImage;
                profile.StatusUpdate = statusUpdate;
                profile.CountryId = countryId;
                profile.CityId = cityId;
                _db.SaveChanges();
                _cache.Remove(key);

                //需要添加记录feed  —更新了profile status
                string firstName = _profileService.GetUserProfile(userId).Firstname;
                string experiences = firstName + " updated profile status.";
                _newsFeedService.AddNewsFeed(userId, 0, 0, 0, Identifier.IBTType.User, Identifier.FeedActiveType.UpdateStatus, statusUpdate, profile.UserId.ToString(), null);
                return new BoolMessageItem<int>(profile.UserId, true, "Update Successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Update failure!");
            }

        }

        //更新用户头像
        public BoolMessageItem<int> UpdateAvatar(int userId, string ProfileImage, string sourceFile, string destinationFile, string objData)
        {
            Profile profile = _db.Profiles.FirstOrDefault(x => x.UserId == userId);
            if (profile != null)
            {
                profile.ProfileImage = ProfileImage;
                _db.SaveChanges();
                //需要添加记录feed  —更新了profile image
                string firstName = _profileService.GetUserProfile(userId).Firstname;
                string experiences = firstName + " has changed profile image.";
                _newsFeedService.AddNewsFeed(userId, 0, 0, 0, Identifier.IBTType.User, Identifier.FeedActiveType.ProImage, experiences, objData, DateTime.Now);
                //复制一份profile image到Newsfeed的图片目录下
                this.SetAvatarNewsfeed(sourceFile, destinationFile);
                return new BoolMessageItem<int>(profile.UserId, true, "Update Avatar Successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Update Avatar failure!");
            }

        }
        //更新Newsfeed的用户头像
        public void SetAvatarNewsfeed(string sourceFile, string destinationFile)
        {
            if (File.Exists(sourceFile))
            {
                // true is overwrite 
                File.Copy(sourceFile, destinationFile, true);
            } 
        }
        //发送密码(忘记密码？)
        public BoolMessageItem<int> SendPassword(string email, string statisticsCountry, string statisticsCity, string statisticsHotel, string statisticsVenue, string statisticsOther)
        {
            User user = _db.Users.FirstOrDefault(x => x.Email.Equals(email));
            Profile profile = _db.Profiles.FirstOrDefault(y => y.Email.Equals(email));
            if (user != null && profile != null)
            {
                EmailService sendemail = new EmailService("Email\\en-us");
                sendemail.RemindUserPassword(email, user.UserName, user.Password, statisticsCountry, statisticsCity, statisticsHotel, statisticsVenue, statisticsOther);
                return new BoolMessageItem<int>(user.UserId, true, "Send Successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Sorry, your current password is incorrect. Please try again.");
            }
        }

        //修改密码
        public BoolMessageItem<int> ChangePassword(int userId, string oldpassword, string newpassword)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserId.Equals(userId) && x.Password.Equals(oldpassword));
            if (user != null)
            {
                user.Password = newpassword;
                _db.SaveChanges();
                return new BoolMessageItem<int>(user.UserId, true, "Change Successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Sorry, your current password is incorrect. Please try again.");
            }
        }

        public BoolMessageItem<int> ChangeUserAndProfileEmail(int userId, string newemail)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserId.Equals(userId));
            Profile profile = _db.Profiles.FirstOrDefault(x => x.UserId == userId);

            if (user != null)
            {
                user.Email = newemail;
                user.NewEmailId = string.Empty;
                user.NewEmailIdVerfication = string.Empty;
                user.IsActivate = 1;
                user.ActivateDate = DateTime.UtcNow;
                user.NewEmailIdVerficationActivate = 0;
                _db.SaveChanges();

                profile.Email = newemail;
                _db.SaveChanges();

                EmailService toemail = new EmailService("Email\\en-us");
                toemail.NewEmailConfirmed(newemail, profile.Username, profile.Firstname);

                return new BoolMessageItem<int>(user.UserId, true, "Change Successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Sorry, please try again.");
            }
        }

        /// <summary>
        /// 根据emali返回Users
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public DAL.User GetUserByEmail(string email)
        {
            var res = (from c in _db.Users where c.Email.Equals(email) select c).FirstOrDefault();
            return res == null ? new DAL.User() : res;
        }

        public BoolMessageItem<int> EmailUnsubscribe(int userId)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserId.Equals(userId));
            if (user != null)
            {
                user.IsEmailSubscribe = false;
                _db.SaveChanges();
                return new BoolMessageItem<int>(user.UserId, true, "Unsubscribe Successful!");
            }
            return new BoolMessageItem<int>(0, false, "Sorry, there is a problem to unsubscribe. Please try again.");
        }


        /// <summary>
        /// update user see the word attribute
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public BoolMessageItem<int> UpdateAccountSeeWorld(int userid, int type,int value)
        {
            User user = _db.Users.Find(userid);

            if (user != null)
            {
                switch (type) { 
                    case 1:
                        user.IsPleasure = (value == 1);
                        break;
                    case 2:
                        user.IsBusiness = (value == 1);
                        break;
                    case 3:
                        user.UserType = value;
                        break;
                    default:
                        break;
                }
                _db.SaveChanges();
            }

            return new BoolMessageItem<int>(value, true, "success");
        }

        #region Member Management
        public BoolMessageItem<int> SetIsApproved(int userId)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserId.Equals(userId));
            if (user != null)
            {
                if (Convert.ToInt32(user.IsApproved) == 0)
                    user.IsApproved = 1;
                else
                    user.IsApproved = 0;
                _db.SaveChanges();
                return new BoolMessageItem<int>(user.UserId, true, "User activation has been successfully updated!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "User activation is not successfully updated. Please try again!");
            }
        }

        public BoolMessageItem<int> SetIsFeatured(int userId)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserId.Equals(userId));
            if (user != null)
            {
                if (Convert.ToInt32(user.StatusId) == 0)
                    user.StatusId = 1;
                else
                    user.StatusId = 0;
                _db.SaveChanges();
                return new BoolMessageItem<int>(user.UserId, true, "User Feature has been successfully updated!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "User Feature is not successfully updated. Please try again!");
            }
        }

        public BoolMessageItem<int> SetIsDel(int userId)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserId.Equals(userId));
            if (user != null && user.StatusId != 3)
            {
                user.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(user.UserId, true, "Member has been successfully deleted!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Member is not successfully deleted. Please try again!");
            }
        }
        #endregion

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
