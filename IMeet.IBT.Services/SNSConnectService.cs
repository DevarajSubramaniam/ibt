﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using StructureMap;
using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public class SNSConnectService : BaseService,ISNSConnectService
    {
        /// <summary>
        /// 根据用户ID获取用户绑定了那些sns
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<SnsConnect> GetSNSConnectList(int userId)
        {
            return (from c in _db.SnsConnects where c.UserId.Equals(userId) select c).ToList();
        }

        /// <summary>
        /// 根据userid,typeId返回相关信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        public SnsConnect GetSNSConnect(int userId, Identifier.SnsType snsTypeId)
        {
            int typeId = (int)snsTypeId;
            var res= (from c in _db.SnsConnects where c.UserId.Equals(userId) && c.SnsTypeId.Equals(typeId) select c).FirstOrDefault();
            return res == null ? new SnsConnect() : res;
        }

        /// <summary>
        /// 根据snsUid,typeId返回相关信息
        /// </summary>
        /// <param name="snsUid"></param>
        /// <param name="snsTypeId"></param>
        /// <returns></returns>
        public SnsConnect GetSNSConnect(string snsUid, Identifier.SnsType snsTypeId)
        {
            int typeId = (int)snsTypeId;
            var res = (from c in _db.SnsConnects where c.Sns_UId.Equals(snsUid) && c.SnsTypeId.Equals(typeId) select c).FirstOrDefault();
            return res == null ? new SnsConnect() : res;
        }

        /// <summary>
        /// 根据accessToken,typeId返回相关信息
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="snsTypeId"></param>
        /// <returns></returns>
        public SnsConnect GetSNSConnectByAccessTokenToken(string accessToken, Identifier.SnsType snsTypeId)
        {
            int snsTypId= (int)snsTypeId;
            var res = (from c in _db.SnsConnects where c.OauthToken.Equals(accessToken) && c.SnsTypeId.Equals(snsTypId) select c).FirstOrDefault();
            return res == null ? new SnsConnect() : res;
        }
        /// <summary>
        /// 新加/绑定
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="snsId"></param>
        /// <param name="snsTypeId"></param>
        /// <param name="token"></param>
        /// <param name="tokenSecret"></param>
        public void AddSNSConnect(int userId, string snsId, int snsTypeId, string token, string tokenSecret)
        {
            SnsConnect sns = new SnsConnect();
            sns.UserId = userId;
            sns.IsEnabled = true;
            sns.Sns_UId = snsId;
            sns.SnsTypeId = snsTypeId;
            sns.OauthToken = token;
            sns.OauthTokenSecret = tokenSecret;
            sns.IsSync = 0;
            sns.UserData = "";
            sns.Comment = DateTime.Now.ToString();

            _db.SnsConnects.Add(sns);
            _db.SaveChanges();
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="connect"></param>
        public void UpdateSNSConnect(SnsConnect connect)
        {
            var item = (from c in _db.SnsConnects where c.ConnectId.Equals(connect.ConnectId) select c).FirstOrDefault();
            if (item != null)
            {
                item.IsEnabled = connect.IsEnabled;
                item.OauthToken = connect.OauthToken;
                item.OauthTokenSecret = connect.OauthTokenSecret;
                item.IsSync = connect.IsSync;
                item.UserData = connect.UserData;
                item.Comment = connect.Comment;

                _db.SaveChanges();
            }

        }


        /// <summary>
        /// 取消绑定（删除）
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="snsTypeId"></param>
        public void DeleteSNSConnect(int userId, int snsTypeId)
        {
            string sql = "DELETE FROM [SnsConnects] WHERE  UserId={0} AND SnsTypeId={1}";
            _db.Database.ExecuteSqlCommand(sql, userId, snsTypeId);
        }

        /// <summary>
        /// 根据sns的ID返回ibt的用户ID
        /// </summary>
        /// <param name="snsIds"></param>
        /// <returns></returns>
        public Dictionary<string, int> GetUserIdBySnsId(List<string> snsIds,int snsTypeId)
        {
            var dic = (from c in _db.SnsConnects
                       where snsIds.Contains(c.Sns_UId) && c.SnsTypeId.Equals(snsTypeId)
                       select new { key = c.Sns_UId, value = c.UserId }).ToDictionary(kvp => kvp.key, kvp => kvp.value);
            return dic == null ? new Dictionary<string, int>() : dic;
        }
    }
}
