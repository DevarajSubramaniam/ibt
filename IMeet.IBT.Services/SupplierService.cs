﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using StructureMap;
using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;
using IMeet.IBT.DAL.Models;
using System.Data.SqlClient;

namespace IMeet.IBT.Services
{
    public class SupplierService : BaseService, ISupplierService
    {
        private INewsFeedService _newsFeedService;
        public IProfileService _profileService;
        private ITravelBadgeService _travelBadgeService;
        public SupplierService()
        {
            _newsFeedService = ObjectFactory.GetInstance<INewsFeedService>();
            _profileService = ObjectFactory.GetInstance<IProfileService>();
            _travelBadgeService = ObjectFactory.GetInstance<ITravelBadgeService>();
        }

        #region Country & City
        /// <summary>
        /// 返回所有国家列表
        /// </summary>
        /// <returns></returns>
        public IList<Country> GetAllCountry()
        {
            string cacheKey = "GetAllCountry";
            return _cache.GetOrInsert<IList<Country>>(cacheKey, 60, true, () =>
            {
                return (from o in _db.Countries orderby o.CountryName ascending select o).ToList<Country>();
            });
        }
        public IList<Country> GetAllCountryNew(string strKeyWord)
        {
            //return (from o in _db.Countries where o.CountryName.Contains(strKeyWord) && o.StatusId == (int)Identifier.StatusType.Enabled orderby o.CountryName ascending select o).ToList<Country>();
            return this.GetAllCountry().Where(c => c.StatusId == (int)Identifier.StatusType.Enabled && c.CountryName.ToLower().Contains(strKeyWord.ToLower())).OrderBy(c => c.CountryName).ToList();
        }
        public Dictionary<string, string> GetAllCountryItem()
        {
            return (from o in _db.Countries where o.StatusId == (int)Identifier.StatusType.Enabled orderby o.CountryName ascending select new { CountryId = o.CountryId, CountryName = o.CountryName }).ToDictionary(kvp => kvp.CountryId.ToString(), kvp => kvp.CountryName);
        }

        public List<CityList> GetAllCity(int page, int pageSize, out int total, out int itemTotal, string txtValue)
        {//返回所有城市和所属国家
            itemTotal = (from c in _db.WidgetIBTs where c.IBTType.Equals(2) && c.StatusId.Equals(1) select c).Count();
            if (itemTotal % 10 != 0)
                total = itemTotal / 10 + 1;
            else
                total = itemTotal / 10;
            int sitem = (page - 2) * pageSize;
            string sqlCmd = "select c.CityId,c.CityName,cc.CountryName,c.CreateDate,c.UpdateDate from Cities c join Countries cc on c.CountryId=cc.CountryId where c.StatusId=1 and c.CityName like {0}";
            //SqlParameter[] paras =  {
            //                        new SqlParameter("@cityName","%"+txtValue+"%")
            //                        };
            List<CityList> list = _db.Database.SqlQuery<CityList>(sqlCmd, "%" + txtValue + "%").ToList();
            return list;
        }


        /// <summary>
        /// 根据国家返回下面的所有城市
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public IList<City> GetCityList(int countryId)
        {
            return (from c in _db.Cities
                    where (c.CountryId.Equals(countryId) && c.StatusId.Equals(1))
                    orderby c.CityName ascending
                    select c).ToList<City>();
        }
        public IList<City> GetCityListByCityids(int[] cids)
        {
            return (from c in _db.Cities
                    where cids.Contains(c.CityId)
                    select c).ToList<City>();
        }
        public IList<City> GetCityListNew(int countryId, string keyword)
        {
            List<City> cityList = _db.Cities.Where(c => c.CountryId == countryId && c.StatusId == (int)IMeet.IBT.Services.Identifier.StatusType.Enabled).OrderBy(c => c.CityName).ToList();
            if (!string.IsNullOrEmpty(keyword))
            {
                cityList = cityList.Where(c => c.CityName.ToLower().Contains(keyword.ToLower())).OrderBy(c=>c.CityName).ToList();
            }
            return cityList;
            //return (from c in _db.Cities
            //        where (c.CountryId.Equals(countryId) && c.CityName.Contains(keyword) && c.StatusId.Equals(1))
            //        orderby c.CityName ascending
            //        select c).ToList<City>();
        }
        public IList<CityListItem> GetHistoryCityList(int userId, int countryId, out int total)
        {
            string sql = @"  SELECT dbo.PostVists.CityId, dbo.Cities.CityName
                            FROM dbo.PostVists
                            LEFT JOIN dbo.Cities ON dbo.PostVists.CityId = dbo.Cities.CityId
                            LEFT JOIN dbo.Countries ON dbo.Countries.CountryId =dbo.PostVists.CountryId                                                 
                            WHERE dbo.PostVists.UserId=" + userId.ToString() + @" AND dbo.PostVists.CountryId=" + countryId.ToString() + @" AND dbo.PostVists.CityId>0 AND dbo.PostVists.StatusId=1 GROUP BY dbo.PostVists.CityId,dbo.Cities.CityName";
            var list = _db.Database.SqlQuery<CityListItem>(sql).ToList();
            total = list.Count;
            return list.ToList();
        }
        public Dictionary<string, string> GetAllCityItem(int countryId)
        {
            return (from c in _db.Cities
                    where (c.CountryId.Equals(countryId) && c.StatusId.Equals(1))
                    orderby c.CityName ascending
                    select new { CityId = c.CityId, CityName = c.CityName }).ToDictionary(kvp => kvp.CityId.ToString(), kvp => kvp.CityName);
        }

        /// <summary>
        /// 根据城市IDs返回下面的所有城市
        /// </summary>
        /// <param name="Ids">多个以“，”分开的城市ID</param>
        /// <returns></returns>
        public IList<City> GetCityListByIds(string cityIds)
        {
            if (string.IsNullOrWhiteSpace(cityIds))
            {
                return new List<City>();
            }
            IList<int> ids = new List<int>();
            string[] cIds = cityIds.Split(',');
            foreach (string num in cIds)
            {
                if (Utils.IsNumeric(num))
                {
                    ids.Add(Convert.ToInt32(num));
                }
            }

            return (from c in _db.Cities where ids.Contains(c.CityId) select c).ToList<City>();
        }

        /// <summary>
        /// 根据国家ID获取国家信息
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public Country GetCountryInfo(int countryId)
        {
            return (from c in _db.Countries where c.CountryId.Equals(countryId) select c).FirstOrDefault();
        }
        public CountriesDe GetCountryDespInfo(int countryId)
        {
            return (from c in _db.CountriesDes where c.CountryId.Equals(countryId) select c).FirstOrDefault();
        }
        public Country GetAllCountryInfo(string countryName)
        {
            return (from c in _db.Countries where c.CountryName.Equals(countryName) select c).FirstOrDefault();
        }
        /// <summary>
        /// 根据城市ID获取城市信息
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public City GetCityInfo(int cityId)
        {
            return (from c in _db.Cities where c.CityId.Equals(cityId) select c).FirstOrDefault();
        }
        public CityDescription GetCityDespInfo(int cityId)
        {
            return (from c in _db.CityDescriptions where c.CityId.Equals(cityId) select c).FirstOrDefault();
        }
        public City GetAllCityInfo(string cityName)
        {
            return (from c in _db.Cities where c.CityName.Equals(cityName) select c).FirstOrDefault();
        }

        /// <summary>
        /// 根据城市ID返回国家名称与城市名称
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public CountryCityItem GetCountryCityInfo(int cityId)
        {
            string key = "GetCountryCityInfo" + cityId.ToString();

            return _cache.GetOrInsert<CountryCityItem>(key, 60, true, () =>
            {
                string sql = @"SELECT      dbo.Countries.CountryId, dbo.Countries.CountryName, dbo.Cities.CityId,dbo.Cities.CityName
                                FROM        dbo.Cities INNER JOIN
                                            dbo.Countries ON dbo.Cities.CountryId = dbo.Countries.CountryId
                                WHERE		dbo.Cities.CityId={0}";

                var res = _db.Database.SqlQuery<CountryCityItem>(sql, cityId).FirstOrDefault();
                return res == null ? new CountryCityItem() : res;
            });
        }



        #endregion Country & City

        #region I've RRWB#我对目的地的评分/推荐/想去（去过）/愿望单

        /// <summary>
        /// 页面的Rating/Recommended/Want to Go/Go Back/Bucket List 单个评论，就是一次只能更改一个值
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="rrwbType"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public BoolMessage RRWB(int userId, Identifier.PostTypes postType, Identifier.IBTType ibtType, int objId, Identifier.RRWBType rrwbType, int val)
        {
            int pid = 0;
            //接收的参数
            //# 类型1，类型1ID ,类型2，类型2ID
            //1-50-1-3 -->1 （国家），编号 50， 1 评分，3 (意思是给国家编号为50的评了3分)
            try
            {
                //如果是Supplier，则更新添加表WidgetMostRecommentdPlaces，在后台进行管理(逻辑错误，注释)
                //if ((int)ibtType == 3)  
                //{
                //    var item2 = (from c in _db.WidgetMostRecommentdPlaces where (c.UserId.Equals(userId) && c.SupplyId.Equals(objId)) select c).FirstOrDefault();
                //    if (item2 != null)
                //    {
                //        item2.SupplyId = objId;
                //        item2.UserId = userId;
                //        //取消Recommend
                //        if (rrwbType != Identifier.RRWBType.Rating && val == 0)
                //        {
                //            item2.StatusId = 3; //相当于remove Recommend
                //        }
                //        else
                //        {
                //            item2.StatusId = 1;
                //        }
                //        item2.Sort = 1;
                //        item2.UpdateDate = DateTime.Now;
                //        _db.SaveChanges();
                //    }
                //    else
                //    {
                //        WidgetMostRecommentdPlace wrp = new WidgetMostRecommentdPlace();
                //        wrp.SupplyId = objId;
                //        wrp.UserId = userId;
                //        wrp.UpdateDate = DateTime.Now;
                //        wrp.Sort = 1;
                //        wrp.CreateDate = DateTime.Now;
                //        wrp.StatusId = 1;
                //        _db.WidgetMostRecommentdPlaces.Add(wrp);
                //        _db.SaveChanges();
                //    }
                //}
                var item = (from c in _db.RRWBs where (c.UserId.Equals(userId) && c.ObjId.Equals(objId) && c.IBTType.Equals((int)ibtType)) select c).FirstOrDefault();
                //1.查找是否已经有了。如果有了就是更新
                if (item != null)
                {
                    switch (rrwbType)
                    {
                        case Identifier.RRWBType.Rating:
                            item.Rating = Utils.IntToByte(val);
                            break;
                        case Identifier.RRWBType.Recommended:
                            item.Recommended = Utils.IntToByte(val);
                            break;
                        case Identifier.RRWBType.Want_to_Go_Go_Back:
                            item.WanttoGo = Utils.IntToByte(val);
                            break;
                        case Identifier.RRWBType.BucketList:
                            item.BucketList = Utils.IntToByte(val);
                            break;
                    }
                    //item.CreateDate = DateTime.Now;
                    item.UpdateDate = DateTime.Now;
                    _db.SaveChanges();

                    pid = item.RRWBId;
                }
                else
                {
                    //2.没有就新添加
                    RRWB rrwb = new RRWB();
                    rrwb.UserId = userId;
                    rrwb.ObjId = objId;
                    rrwb.IBTType = (int)ibtType;

                    switch (rrwbType)
                    {
                        case Identifier.RRWBType.Rating:
                            rrwb.Rating = Utils.IntToByte(val);
                            break;
                        case Identifier.RRWBType.Recommended:
                            rrwb.Recommended = Utils.IntToByte(val);
                            break;
                        case Identifier.RRWBType.Want_to_Go_Go_Back:
                            rrwb.WanttoGo = Utils.IntToByte(val);
                            break;
                        case Identifier.RRWBType.BucketList:
                            rrwb.BucketList = Utils.IntToByte(val);
                            break;
                    }

                    rrwb.StatusId = BitConverter.GetBytes((int)Identifier.StatusType.Enabled)[0];
                    rrwb.CreateDate = DateTime.Now;
                    rrwb.UpdateDate = DateTime.Now;

                    _db.RRWBs.Add(rrwb);
                    _db.SaveChanges();

                    pid = rrwb.RRWBId;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("RRWB", ex);
                return new BoolMessage(false, "Save Error!");
            }

            //add feed (暂时不区分是第一次还是第N次操作)
            this._RRWBFeed(userId, postType, ibtType, objId, rrwbType, val);


            //rate时添加user badge special记录
            if (rrwbType == Identifier.RRWBType.Rating)
            {
                AddUserBadgeSpecialRatePoint(userId, pid);
            }

            //统计总数
            return RRWBStatistics(ibtType, objId, rrwbType);
        }

        /// <summary>
        /// comment on rate
        /// rate的时候产生 rate的point
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pid"></param>
        private void AddUserBadgeSpecialRatePoint(int userId, int pid)
        {
            string key = "GetUserTravelbadge_" + userId.ToString();
            if (_cache.Contains(key))
                _cache.Remove(key);

            string type = Enum.GetName(typeof(Identifier.BadgeSpecialCommentType), Identifier.BadgeSpecialCommentType.Rate);

            List<BadgeSpecial> list = _db.BadgeSpecials.Where(b => b.StatusId == 0 && b.Type == (int)Identifier.BadgeSpecialType.UniqueSpecificActivity && b.USAActivity == type && b.StartDate <= DateTime.Now && b.EndDate >= DateTime.Now && b.Visible == (int)Identifier.BadgeSpecialVisible.Visible).ToList();

            list.ForEach(b =>
            {
                UserBadgeSpecial userBadgeSpecial = new UserBadgeSpecial()
                {
                    UserId = userId,
                    BadgeSpecialId = b.BadgeSpecialId,
                    point = b.USAPoint,
                    ReceivedDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    PID = pid
                };

                _db.UserBadgeSpecials.Add(userBadgeSpecial);

                //if (_db.UserBadgeSpecials.Count(ubs => ubs.UserId == userId && ubs.BadgeSpecialId == b.BadgeSpecialId && ubs.StatusId == 0) == 0)
                //{
                //    UserBadgeSpecial userBadgeSpecial = new UserBadgeSpecial()
                //    {
                //        UserId=userId,
                //        BadgeSpecialId=b.BadgeSpecialId,
                //        point=b.USAPoint,
                //        ReceivedDate=DateTime.Now,
                //        UpdateDate=DateTime.Now,
                //        PID=pid
                //    };

                //    _db.UserBadgeSpecials.Add(userBadgeSpecial);
                //}
            });

            _db.SaveChanges();
        }

        /// <summary>
        /// 页面的Rating/Recommended/Want to Go/Go Back/Bucket List 一次改几个值
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="rating"></param>
        /// <param name="recommended"></param>
        /// <param name="wggb"></param>
        /// <param name="bucketList"></param>
        /// <returns></returns>
        /// <remarks>这方法暂时没用到</remarks>
        public BoolMessage RRWB(int userId, Identifier.IBTType ibtType, int objId, int rating, int recommended, int wggb, int bucketList)
        {
            //接收的参数
            //# 类型1，类型1ID ,类型2，类型2ID
            //1-50-1-3 -->1 （国家），编号 50， 1 评分，3 (意思是给国家编号为50的评了3分)
            try
            {
                var item = (from c in _db.RRWBs where (c.UserId.Equals(userId) && c.ObjId.Equals(objId) && c.IBTType.Equals((int)ibtType)) select c).FirstOrDefault();
                //1.查找是否已经有了。如果有了就是更新
                if (item != null)
                {
                    item.Rating = Utils.IntToByte(rating);
                    item.Recommended = Utils.IntToByte(recommended);
                    item.WanttoGo = Utils.IntToByte(wggb);
                    item.BucketList = Utils.IntToByte(bucketList);

                    _db.SaveChanges();
                }
                else
                {
                    //2.没有就新添加
                    RRWB rrwb = new RRWB();
                    rrwb.UserId = userId;
                    rrwb.ObjId = objId;
                    rrwb.IBTType = (int)ibtType;

                    rrwb.Rating = Utils.IntToByte(rating);
                    rrwb.Recommended = Utils.IntToByte(recommended);
                    rrwb.WanttoGo = Utils.IntToByte(wggb);
                    rrwb.BucketList = Utils.IntToByte(bucketList);

                    rrwb.StatusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
                    rrwb.CreateDate = DateTime.Now;
                    rrwb.UpdateDate = DateTime.Now;

                    _db.RRWBs.Add(rrwb);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("RRWB", ex);
                return new BoolMessage(false, "Save Error!");
            }

            //统计总数
            return RRWBStatistics(ibtType, objId, Identifier.RRWBType.All);
        }

        /// <summary>
        /// 更新国家/城市/Supply的RRWB的数量
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="rrwbType">如果是All,则要统计当前数据的RRWB4个字段</param>
        /// <returns></returns>
        private BoolMessage RRWBStatistics(Identifier.IBTType ibtType, int objId, Identifier.RRWBType rrwbType)
        {
            var bm = new BoolMessage(false, "RRWBStatistics rrwbType not found!");
            switch (ibtType)
            {
                case Identifier.IBTType.Country:
                    bm = this._RRWBStatisticsCountry(objId, ibtType, rrwbType);
                    break;
                case Identifier.IBTType.City:
                    bm = this._RRWBStatisticsCity(objId, ibtType, rrwbType);
                    break;
                case Identifier.IBTType.Supply:
                    bm = this._RRWBStatisticsSupply(objId, ibtType, rrwbType);
                    break;
            }
            return bm;
        }

        /// <summary>
        /// 统计国家的RRWB
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="rrwbType"></param>
        /// <returns></returns>
        public BoolMessage _RRWBStatisticsCountry(int countryId, Identifier.IBTType ibtType, Identifier.RRWBType rrwbType)
        {
            //todo:统计国家的RRWB 这页面暂时没显示，不统计
            try
            {
                //是统计的总的，不分用户的
                switch (rrwbType)
                {
                    case Identifier.RRWBType.Rating:
                        string rating_sql = @"UPDATE [Countries]
                                           SET 
                                              [Rating] = IsNull((SELECT TOP 1 SUM([Rating])/COUNT(1)
						                                        FROM [RRWB]
						                                        WHERE ObjId={0} AND IBTType={1})
						                                        ,0) 
                                         WHERE [CountryId]={0}";
                        _db.Database.ExecuteSqlCommand(rating_sql, countryId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.Recommended:
                        string recommended_sql = @"UPDATE [Countries]
                                                SET 
                                                    [RecommendedCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND Recommended=1)
						                                            ,0) 
                                                WHERE [CountryId]={0}";
                        _db.Database.ExecuteSqlCommand(recommended_sql, countryId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.Want_to_Go_Go_Back:
                        string sql3 = @"UPDATE [Countries]
                                                SET 
                                                    [WanttoGoCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND WanttoGo=1)
						                                            ,0) 
                                                WHERE [CountryId]={0}";
                        _db.Database.ExecuteSqlCommand(sql3, countryId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.BucketList:
                        string sql4 = @"UPDATE [Countries]
                                                SET 
                                                    [BucketListCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND BucketList=1)
						                                            ,0) 
                                                WHERE [CountryId]={0}";
                        _db.Database.ExecuteSqlCommand(sql4, countryId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.All:
                        string sql5 = @"UPDATE [Countries]
                                                SET 
                                                    [Rating] = IsNull((SELECT TOP 1 SUM([Rating])/COUNT(1)
						                                        FROM [RRWB]
						                                        WHERE ObjId={0} AND IBTType={1})
						                                        ,0),
                                                    [RecommendedCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND Recommended=1)
						                                            ,0),
                                                    [WanttoGoCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND WanttoGo=1)
						                                            ,0),
                                                    [BucketListCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND BucketList=1)
						                                            ,0) 
                                                WHERE [CountryId]={0}";
                        _db.Database.ExecuteSqlCommand(sql5, countryId, (int)ibtType);
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("_RRWBStatisticsCountry Error", ex);
                return new BoolMessage(false, "_RRWBStatisticsCountry Error！");
            }

            return new BoolMessage(true, "");
            //switch (rrwbType)
            //{
            //    case Identifier.RRWBType.Rating:
            //        break;
            //    case Identifier.RRWBType.Recommended:
            //        break;
            //    case Identifier.RRWBType.Want_to_Go_Go_Back:
            //        break;
            //    case Identifier.RRWBType.BucketList:
            //        break;
            //    case Identifier.RRWBType.All:
            //        break;
            //    default:
            //        break;
            //}
            //return new BoolMessage(true, "");
        }

        /// <summary>
        /// 统计城市的RRwb
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="rrwbType"></param>
        /// <returns></returns>
        public BoolMessage _RRWBStatisticsCity(int cityId, Identifier.IBTType ibtType, Identifier.RRWBType rrwbType)
        {
            //todo:统计城市的RRWB 这页面暂时没显示，不统计
            try
            {
                //是统计的总的，不分用户的
                switch (rrwbType)
                {
                    case Identifier.RRWBType.Rating:
                        string rating_sql = @"UPDATE [Cities]
                                           SET 
                                              [Rating] = IsNull((SELECT TOP 1 SUM([Rating])/COUNT(1)
						                                        FROM [RRWB]
						                                        WHERE ObjId={0} AND IBTType={1})
						                                        ,0) 
                                         WHERE [CityId]={0}";
                        _db.Database.ExecuteSqlCommand(rating_sql, cityId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.Recommended:
                        string recommended_sql = @"UPDATE [Cities]
                                                SET 
                                                    [RecommendedCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND Recommended=1)
						                                            ,0) 
                                                WHERE [CityId]={0}";
                        _db.Database.ExecuteSqlCommand(recommended_sql, cityId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.Want_to_Go_Go_Back:
                        string sql3 = @"UPDATE [Cities]
                                                SET 
                                                    [WanttoGoCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND WanttoGo=1)
						                                            ,0) 
                                                WHERE [CityId]={0}";
                        _db.Database.ExecuteSqlCommand(sql3, cityId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.BucketList:
                        string sql4 = @"UPDATE [Cities]
                                                SET 
                                                    [BucketListCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND BucketList=1)
						                                            ,0) 
                                                WHERE [CityId]={0}";
                        _db.Database.ExecuteSqlCommand(sql4, cityId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.All:
                        string sql5 = @"UPDATE [Cities]
                                                SET 
                                                    [Rating] = IsNull((SELECT TOP 1 SUM([Rating])/COUNT(1)
						                                        FROM [RRWB]
						                                        WHERE ObjId={0} AND IBTType={1})
						                                        ,0),
                                                    [RecommendedCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND Recommended=1)
						                                            ,0),
                                                    [WanttoGoCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND WanttoGo=1)
						                                            ,0),
                                                    [BucketListCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND BucketList=1)
						                                            ,0) 
                                                WHERE [CityId]={0}";
                        _db.Database.ExecuteSqlCommand(sql5, cityId, (int)ibtType);
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("_RRWBStatisticsCity Error", ex);
                return new BoolMessage(false, "_RRWBStatisticsCity Error！");
            }

            return new BoolMessage(true, "");
            //switch (rrwbType)
            //{
            //    case Identifier.RRWBType.Rating:
            //        break;
            //    case Identifier.RRWBType.Recommended:
            //        break;
            //    case Identifier.RRWBType.Want_to_Go_Go_Back:
            //        break;
            //    case Identifier.RRWBType.BucketList:
            //        break;
            //    case Identifier.RRWBType.All:
            //        break;
            //    default:
            //        break;
            //}
            //return new BoolMessage(true, "");
        }

        /// <summary>
        /// 统计Supply的RRWB
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="rrwbType"></param>
        /// <returns></returns>
        private BoolMessage _RRWBStatisticsSupply(int supplyId, Identifier.IBTType ibtType, Identifier.RRWBType rrwbType)
        {
            try
            {
                //是统计的总的，不分用户的
                switch (rrwbType)
                {
                    case Identifier.RRWBType.Rating:
                        string rating_sql = @"UPDATE [Supplies]
                                           SET 
                                              [Rating] = IsNull((SELECT TOP 1 SUM([Rating])/COUNT(1)
						                                        FROM [RRWB]
						                                        WHERE ObjId={0} AND IBTType={1})
						                                        ,0) 
                                         WHERE [SupplyId]={0}";
                        _db.Database.ExecuteSqlCommand(rating_sql, supplyId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.Recommended:
                        string recommended_sql = @"UPDATE [Supplies]
                                                SET 
                                                    [RecommendedCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND Recommended=1)
						                                            ,0) 
                                                WHERE [SupplyId]={0}";
                        _db.Database.ExecuteSqlCommand(recommended_sql, supplyId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.Want_to_Go_Go_Back:
                        string sql3 = @"UPDATE [Supplies]
                                                SET 
                                                    [WanttoGoCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND WanttoGo=1)
						                                            ,0) 
                                                WHERE [SupplyId]={0}";
                        _db.Database.ExecuteSqlCommand(sql3, supplyId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.BucketList:
                        string sql4 = @"UPDATE [Supplies]
                                                SET 
                                                    [BucketListCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND BucketList=1)
						                                            ,0) 
                                                WHERE [SupplyId]={0}";
                        _db.Database.ExecuteSqlCommand(sql4, supplyId, (int)ibtType);
                        break;
                    case Identifier.RRWBType.All:
                        string sql5 = @"UPDATE [Supplies]
                                                SET 
                                                    [Rating] = IsNull((SELECT TOP 1 SUM([Rating])/COUNT(1)
						                                        FROM [RRWB]
						                                        WHERE ObjId={0} AND IBTType={1})
						                                        ,0),
                                                    [RecommendedCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND Recommended=1)
						                                            ,0),
                                                    [WanttoGoCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND WanttoGo=1)
						                                            ,0),
                                                    [BucketListCount] = IsNull((SELECT TOP 1 COUNT(1)
						                                            FROM [RRWB]
						                                            WHERE ObjId={0} AND IBTType={1} AND BucketList=1)
						                                            ,0) 
                                                WHERE [SupplyId]={0}";
                        _db.Database.ExecuteSqlCommand(sql5, supplyId, (int)ibtType);
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("_RRWBStatisticsSupply Error", ex);
                return new BoolMessage(false, "_RRWBStatisticsSupply Error！");
            }

            return new BoolMessage(true, "");
        }

        /// <summary>
        /// RRWB的Feeds
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <param name="rrwbType"></param>
        private void _RRWBFeed(int userId, Identifier.PostTypes postType, Identifier.IBTType ibtType, int objId, Identifier.RRWBType rrwbType, int val)
        {
            string msg = "";
            string strmsg = "";
            int countryId = 0;
            int cityId = 0;
            int supplyId = 0;
            Identifier.FeedActiveType activeType = Identifier.FeedActiveType.Cancel;
            Profile profile = _profileService.GetUserProfile(userId);
            string firstName = profile.Firstname;
            string gender = "the";
            if (Convert.ToInt32(profile.Gender.ToString()) == 0)
            {
                gender = "his";
            }
            else if (Convert.ToInt32(profile.Gender.ToString()) == 1)
            {
                gender = "her";
            }
            //new
            #region msgType
            switch (postType)
            {
                case Identifier.PostTypes.PostVisit:
                    #region strmsg & get countryId,cityId,supplyId
                    switch (ibtType)
                    {
                        case Identifier.IBTType.Country:
                            string countryName = this.GetAllCountry().Where(c => c.CountryId.Equals(objId)).FirstOrDefault().CountryName;
                            countryId = objId;
                            switch (rrwbType)
                            {
                                case Identifier.RRWBType.Rating:
                                    strmsg = firstName + " rated " + countryName + ".";
                                    activeType = Identifier.FeedActiveType.Rating;
                                    break;
                                case Identifier.RRWBType.Recommended:
                                    strmsg = firstName + " recommended " + countryName + ".";
                                    activeType = Identifier.FeedActiveType.Recommended;
                                    break;
                                case Identifier.RRWBType.Want_to_Go_Go_Back:
                                    strmsg = firstName + " want to go / go back to " + countryName + ".";
                                    activeType = Identifier.FeedActiveType.WantGoBack;
                                    break;
                                case Identifier.RRWBType.BucketList:
                                    strmsg = firstName + " added " + countryName + " to " + gender + " bucket list.";
                                    activeType = Identifier.FeedActiveType.BucketList;
                                    break;
                            }
                            break;
                        case Identifier.IBTType.City:
                            var ccitem = this.GetCountryCityInfo(objId);
                            countryId = ccitem.CountryId;
                            cityId = objId;
                            switch (rrwbType)
                            {
                                case Identifier.RRWBType.Rating:
                                    strmsg = firstName + " rated " + ccitem.CityName + ".";
                                    activeType = Identifier.FeedActiveType.Rating;
                                    break;
                                case Identifier.RRWBType.Recommended:
                                    strmsg = firstName + " recommended " + ccitem.CityName + ".";
                                    activeType = Identifier.FeedActiveType.Recommended;
                                    break;
                                case Identifier.RRWBType.Want_to_Go_Go_Back:
                                    strmsg = firstName + " want to go / go back to " + ccitem.CityName + ".";
                                    activeType = Identifier.FeedActiveType.WantGoBack;
                                    break;
                                case Identifier.RRWBType.BucketList:
                                    strmsg = firstName + " added " + ccitem.CityName + " to " + gender + " bucket list.";
                                    activeType = Identifier.FeedActiveType.BucketList;
                                    break;
                            }
                            break;
                        case Identifier.IBTType.Supply:
                            var supplyPv = this.GetSupplyDetail(objId);
                            countryId = supplyPv.CountryId;
                            cityId = supplyPv.CityId;
                            supplyId = supplyPv.SupplyId;
                            switch (rrwbType)
                            {
                                case Identifier.RRWBType.Rating:
                                    strmsg = firstName + " rated " + supplyPv.Title + ".";
                                    activeType = Identifier.FeedActiveType.Rating;
                                    break;
                                case Identifier.RRWBType.Recommended:
                                    strmsg = firstName + " recommended " + supplyPv.Title + ".";
                                    activeType = Identifier.FeedActiveType.Recommended;
                                    break;
                                case Identifier.RRWBType.Want_to_Go_Go_Back:
                                    strmsg = firstName + " want to go / go back to " + supplyPv.Title + ".";
                                    activeType = Identifier.FeedActiveType.WantGoBack;
                                    break;
                                case Identifier.RRWBType.BucketList:
                                    strmsg = firstName + " added " + supplyPv.Title + " to " + gender + " bucket list.";
                                    activeType = Identifier.FeedActiveType.BucketList;
                                    break;
                            }
                            break;
                    }
                    #endregion
                    break;
                case Identifier.PostTypes.Detail:
                    #region strmsg & get countryId,cityId,supplyId
                    switch (ibtType)
                    {
                        case Identifier.IBTType.Country:
                            string countryName = this.GetAllCountry().Where(c => c.CountryId.Equals(objId)).FirstOrDefault().CountryName;
                            countryId = objId;
                            switch (rrwbType)
                            {
                                case Identifier.RRWBType.Rating:
                                    //strmsg = "<a href=\"/Profile/" + userId.ToString() + "\" >" + firstName + "</a><span class=\"f15  \"> rated <a href=\"#\" >" + countryName + "</a></span>";
                                    strmsg = firstName + " rated " + countryName + ".";
                                    activeType = Identifier.FeedActiveType.Rating;
                                    break;
                                case Identifier.RRWBType.Recommended:
                                    strmsg = firstName + " recommended " + countryName + ".";
                                    activeType = Identifier.FeedActiveType.Recommended;
                                    break;
                                case Identifier.RRWBType.Want_to_Go_Go_Back:
                                    strmsg = firstName + " want to go / go back to " + countryName + ".";
                                    activeType = Identifier.FeedActiveType.WantGoBack;
                                    break;
                                case Identifier.RRWBType.BucketList:
                                    strmsg = firstName + " added " + countryName + " to " + gender + " bucket list.";
                                    activeType = Identifier.FeedActiveType.BucketList;
                                    break;
                            }
                            break;
                        case Identifier.IBTType.City:
                            var ccitem = this.GetCountryCityInfo(objId);
                            countryId = ccitem.CountryId;
                            cityId = objId;
                            switch (rrwbType)
                            {
                                case Identifier.RRWBType.Rating:
                                    strmsg = firstName + " rated " + ccitem.CityName + ".";
                                    activeType = Identifier.FeedActiveType.Rating;
                                    break;
                                case Identifier.RRWBType.Recommended:
                                    strmsg = firstName + " recommended " + ccitem.CityName + ".";
                                    activeType = Identifier.FeedActiveType.Recommended;
                                    break;
                                case Identifier.RRWBType.Want_to_Go_Go_Back:
                                    strmsg = firstName + " want to go / go back to " + ccitem.CityName + ".";
                                    activeType = Identifier.FeedActiveType.WantGoBack;
                                    break;
                                case Identifier.RRWBType.BucketList:
                                    strmsg = firstName + " added " + ccitem.CityName + " to " + gender + " bucket list.";
                                    activeType = Identifier.FeedActiveType.BucketList;
                                    break;
                            }
                            break;
                        case Identifier.IBTType.Supply:
                            var supplyPv = this.GetSupplyDetail(objId);
                            countryId = supplyPv.CountryId;
                            cityId = supplyPv.CityId;
                            supplyId = supplyPv.SupplyId;
                            switch (rrwbType)
                            {
                                case Identifier.RRWBType.Rating:
                                    strmsg = firstName + " rated " + supplyPv.Title + ".";
                                    activeType = Identifier.FeedActiveType.Rating;
                                    break;
                                case Identifier.RRWBType.Recommended:
                                    strmsg = firstName + " recommended " + supplyPv.Title + ".";
                                    activeType = Identifier.FeedActiveType.Recommended;
                                    break;
                                case Identifier.RRWBType.Want_to_Go_Go_Back:
                                    strmsg = firstName + " want to go / go back to " + supplyPv.Title + ".";
                                    activeType = Identifier.FeedActiveType.WantGoBack;
                                    break;
                                case Identifier.RRWBType.BucketList:
                                    strmsg = firstName + " added " + supplyPv.Title + " to " + gender + " bucket list.";
                                    activeType = Identifier.FeedActiveType.BucketList;
                                    break;
                            }
                            break;
                    }
                    #endregion
                    break;
            }
            #endregion

            //old
            #region msg & get countryId,cityId,supplyId
            //switch (ibtType)
            //{
            //    case Identifier.IBTType.Country:
            //        string countryName = this.GetAllCountry().Where(c => c.CountryId.Equals(objId)).FirstOrDefault().CountryName;
            //        countryId = objId;
            //        switch (rrwbType)
            //        {
            //            case Identifier.RRWBType.Rating:
            //                msg = firstName + " rated " + countryName + ".";
            //                activeType = Identifier.FeedActiveType.Rating;
            //                break;
            //            case Identifier.RRWBType.Recommended:
            //                msg = firstName + " recommended " + countryName + ".";
            //                activeType = Identifier.FeedActiveType.Recommended;
            //                break;
            //            case Identifier.RRWBType.Want_to_Go_Go_Back:
            //                msg = firstName + " want to go / go back to " + countryName + ".";
            //                activeType = Identifier.FeedActiveType.WantGoBack;
            //                break;
            //            case Identifier.RRWBType.BucketList:
            //                msg = firstName + " added " + countryName + " to his bucket list.";
            //                activeType = Identifier.FeedActiveType.BucketList;
            //                break;
            //        }
            //        break;
            //    case Identifier.IBTType.City:
            //        var ccitem = this.GetCountryCityInfo(objId);
            //        countryId = ccitem.CountryId;
            //        cityId = objId;
            //        switch (rrwbType)
            //        {
            //            case Identifier.RRWBType.Rating:
            //                msg = firstName + " rated " + ccitem.CityName + ".";
            //                activeType = Identifier.FeedActiveType.Rating;
            //                break;
            //            case Identifier.RRWBType.Recommended:
            //                msg = firstName + " recommended " + ccitem.CityName + ".";
            //                activeType = Identifier.FeedActiveType.Recommended;
            //                break;
            //            case Identifier.RRWBType.Want_to_Go_Go_Back:
            //                msg = firstName + " want to go / go back to " + ccitem.CityName + ".";
            //                activeType = Identifier.FeedActiveType.WantGoBack;
            //                break;
            //            case Identifier.RRWBType.BucketList:
            //                msg = firstName + " added " + ccitem.CityName + " to his bucket list.";
            //                activeType = Identifier.FeedActiveType.BucketList;
            //                break;
            //        }
            //        break;
            //    case Identifier.IBTType.Supply:
            //        var supply = this.GetSupplyDetail(objId);
            //        countryId = supply.CountryId;
            //        cityId = supply.CityId;
            //        supplyId = supply.SupplyId;
            //        switch (rrwbType)
            //        {
            //            case Identifier.RRWBType.Rating:
            //                msg = firstName + " rated " + supply.Title + ".";
            //                activeType = Identifier.FeedActiveType.Rating;
            //                break;
            //            case Identifier.RRWBType.Recommended:
            //                msg = firstName + " recommended " + supply.Title + ".";
            //                activeType = Identifier.FeedActiveType.Recommended;
            //                break;
            //            case Identifier.RRWBType.Want_to_Go_Go_Back:
            //                msg = firstName + " want to go / go back to " + supply.Title + ".";
            //                activeType = Identifier.FeedActiveType.WantGoBack;
            //                break;
            //            case Identifier.RRWBType.BucketList:
            //                msg = firstName + " added " + supply.Title + " to his bucket list.";
            //                activeType = Identifier.FeedActiveType.BucketList;
            //                break;
            //        }
            //        break;
            //}
            #endregion

            //如果val是0则是取消
            if (rrwbType != Identifier.RRWBType.Rating && val == 0)
            {
                activeType = Identifier.FeedActiveType.Cancel;//-1
            }

            _newsFeedService.AddNewsFeed(userId, countryId, cityId, supplyId, ibtType, activeType, strmsg, "", DateTime.Now);

        }

        /// <summary>
        /// 根据用户ID，objId返回RRWB(个人用户页面根据用户ID统计个人的Recommended, Want to Go, Bucket List，Ratings)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="objId"></param>
        /// <param name="ibtType"></param>
        /// <returns></returns>
        public RRWB GetUserOnlyRRWB(int userId, int objId, Identifier.IBTType ibtType)
        {
            //1:Country 2:City 3:Supplier
            int _ibtType = (int)ibtType;
            var res = (from c in _db.RRWBs
                       where c.UserId.Equals(userId) && c.ObjId.Equals(objId) && c.IBTType.Equals(_ibtType)
                       select c).FirstOrDefault();
            return res == null ? new RRWB() : res;

        }

        #endregion

        #region The Recent Places that We've Been
        /// <summary>
        /// The Recent Places that We've Been list
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public IList<RecentPlacesItem> GetHomeRecentPlacesList(int limit)
        {
            string sql = @"SELECT TOP " + limit.ToString() + @"   dbo.Profiles.Firstname, dbo.Countries.CountryName, dbo.Cities.CityName, dbo.Supplies.Title, dbo.IBT.*, 
                                             dbo.Supplies.CoverPhotoSrc, dbo.Countries.FlagSrc,dbo.Cities.FlagSrc as CFlagSrc
                            FROM dbo.IBT
                              LEFT JOIN dbo.Cities ON dbo.IBT.CityId = dbo.Cities.CityId
                              LEFT JOIN dbo.Countries ON dbo.Countries.CountryId =dbo.IBT.CountryId
                              LEFT JOIN dbo.Supplies ON dbo.IBT.SupplyId = dbo.Supplies.SupplyId
                              LEFT JOIN dbo.Profiles ON dbo.Profiles.UserId=dbo.IBT.UserId
                              WHERE dbo.Profiles.ProfileImage<>'' AND dbo.IBT.IBTId IN (select Max(IBTId) from dbo.IBT group by UserId)
                              ORDER BY ibt.UpdateDate DESC,ibt.IBTId DESC ";

            return _db.Database.SqlQuery<RecentPlacesItem>(sql).ToList();
            //string key = "GetHomeRecentPlacesList" + limit.ToString();

            //return _cache.GetOrInsert<List<RecentPlacesItem>>(key, 60, true, () =>
            //{

            //});
        }


        #endregion

        #region I've Been There

        /// <summary>
        /// 首页，Prolie的ibt统计
        /// </summary>
        /// <param name="userId">用户ID，如果是0就是所有（首页的）</param>
        /// <param name="tabId">1 Countries 2 Cities 3 Hotels 4 LIFESTYLE 5 ATTRACTIONS</param>
        /// <returns></returns>
        public int IbtStatistics(int userId, int tabId)
        {
            //todo:首页，Prolie的ibt统计
            string key = "IbtStatistics_" + userId.ToString() + "_" + tabId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                int total = -1;
                switch (tabId)
                {
                    case 1: //Countries
                        string strSql1 = "";
                        if (userId > 0)
                        {
                            /*20130228裕冲修改*/
                            strSql1 = @"SELECT count(*) FROM dbo.IBT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND  dbo.IBT.UserId = dbo.NewsFeed.UserId JOIN dbo.Countries ON dbo.NewsFeed.CountryId = dbo.Countries.CountryId JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId WHERE dbo.NewsFeed.StatusId=1 AND dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE  CountryId>0 AND CityId=0 AND SupplyId=0 AND StatusId=1 AND dbo.IBT.UserId=" + userId + @" GROUP BY dbo.IBT.UserId,dbo.IBT.CountryId)  AND dbo.NewsFeed.NewsFeedId IN (SELECT MAX(NewsFeedId) FROM dbo.NewsFeed WHERE UserId=" + userId + @" AND dbo.NewsFeed.IBTType=1 AND dbo.NewsFeed.ActiveType IN (5,6,7)  GROUP BY CountryId) ";
                            //strSql1 = @"SELECT Count(temp.CountryId) FROM (SELECT UserId, CountryId  FROM dbo.IBT WHERE  CountryId>0 AND UserId=" + userId.ToString() + @" GROUP BY UserId,CountryId) AS temp";
                            /*修改结束*/
                            //total = _db.Database.SqlQuery<int>(strSql1).ToList().Count;
                            //total = (from c in _db.IBTs group c by new { Counter = c.CountryId > 0 && c.UserId.Equals(userId) } into g select g).Count();
                            User user = _profileService.GetUserInfo(userId);
                            if (user != null)
                            {
                                total = _travelBadgeService.GetUserTravelbadge(user).UserPermanentPoint.CountryNumber;
                                break;
                            }
                        }
                        else
                        {
                            /*20130228裕冲修改*/
                            //strSql1 = @"SELECT count(*) FROM dbo.IBT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND  dbo.IBT.UserId = dbo.NewsFeed.UserId JOIN dbo.Countries ON dbo.NewsFeed.CountryId = dbo.Countries.CountryId JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId WHERE  dbo.IBT.IBTId IN  ( SELECT MAX(IBTId) as id FROM dbo.IBT WHERE  CountryId>0  GROUP BY dbo.IBT.UserId,dbo.IBT.CountryId) AND dbo.NewsFeed.IBTType=1 ";
                            strSql1 = @"SELECT Count(temp.CountryId) FROM (SELECT DISTINCT CountryId FROM dbo.IBT WHERE  CountryId>0 AND CityId=0 AND SupplyId=0 AND StatusId=1 GROUP BY CountryId) AS temp";
                            /*修改结束*/
                            //total = _db.Database.SqlQuery<int>(strSql1).ToList().Count;
                            //total = (from c in _db.IBTs where c.CountryId > 0 select c).Count();
                        }
                        var res_country = _db.Database.SqlQuery<int>(strSql1).ToList();
                        total = res_country[0];
                        break;
                    case 2: //Cities
                        string strSql2 = "";
                        if (userId > 0)
                        {
                            /*20130228裕冲修改*/
                            strSql2 = @"SELECT COUNT(*) FROM dbo.IBT LEFT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND dbo.IBT.UserId = dbo.NewsFeed.UserId LEFT JOIN dbo.Cities ON dbo.NewsFeed.CityId = dbo.Cities.CityId LEFT JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId WHERE dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE  CityId>0 AND SupplyId=0 AND StatusId=1 AND dbo.IBT.UserId=" + userId + @" GROUP BY dbo.IBT.UserId,dbo.IBT.CityId) AND dbo.NewsFeed.NewsFeedId IN (SELECT MAX(NewsFeedId) FROM dbo.NewsFeed WHERE UserId=" + userId + @"  AND dbo.NewsFeed.IBTType=2 AND dbo.NewsFeed.ActiveType IN (5,6,7) GROUP BY CityId)";
                            //strSql2 = @"SELECT Count(temp.CityId) FROM (SELECT UserId, CityId  FROM dbo.IBT WHERE  CityId>0 AND UserId=" + userId.ToString() + @" GROUP BY UserId,CityId) AS temp";
                            /*修改结束*/
                            //total = (from c in _db.IBTs where (c.CityId > 0 && c.UserId.Equals(userId)) select c).Count();
                            //total = (from c in _db.IBTs group c by new { Counter = c.CityId > 0 && c.UserId.Equals(userId) } into g select g).Count();
                            User user = _profileService.GetUserInfo(userId);
                            if (user != null)
                            {
                                total = _travelBadgeService.GetUserTravelbadge(user).UserPermanentPoint.CityNumber;
                                break;
                            }
                        }
                        else
                        {
                            /*20130228裕冲修改*/
                            //strSql2 = @"SELECT COUNT(*) FROM dbo.IBT LEFT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND dbo.IBT.UserId = dbo.NewsFeed.UserId LEFT JOIN dbo.Cities ON dbo.NewsFeed.CityId = dbo.Cities.CityId LEFT JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId WHERE dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE  CityId>0 GROUP BY dbo.IBT.UserId,dbo.IBT.CityId) AND dbo.NewsFeed.IBTType=2 ";
                            strSql2 = @"SELECT Count(temp.CityId) FROM (SELECT DISTINCT CityId FROM dbo.IBT WHERE  CityId>0 AND SupplyId=0 AND StatusId=1 GROUP BY CityId) AS temp";
                            /*20130228裕冲修改*/
                            //total = _db.Database.SqlQuery<int>(strSql2).ToList().Count();
                            //total = (from c in _db.IBTs where c.CityId > 0 select c).Count();
                            //total = (from c in _db.IBTs group c by new { Counter = c.CityId > 0  } into g select g).Count();
                        }
                        var res_city = _db.Database.SqlQuery<int>(strSql2).ToList();
                        total = res_city[0];
                        break;
                    case 3: //Hotels
                        string hotelCategoryId = System.Configuration.ConfigurationManager.AppSettings["hotelCategoryId"] ?? "1";
//                        string strSql3 = @"SELECT    COUNT(1)
//                                            FROM       dbo.AttributeData INNER JOIN
//                                                      dbo.SupplyAttributeData ON dbo.AttributeData.AttrDataId = dbo.SupplyAttributeData.ArrtDataId INNER JOIN
//                                                      dbo.IBT ON dbo.SupplyAttributeData.SupplyId = dbo.IBT.SupplyId
//                                            WHERE dbo.AttributeData.ParentId=" + hotelCategoryId;
                        string strSql3 = "EXEC IBT_GetSupplyIBTCount " + hotelCategoryId;
                        if (userId > 0)
                        {
                            //strSql3 = strSql3 + " AND ibt.UserId=" + userId.ToString();
                            strSql3 = "EXEC IBT_GetSupplyIBTCountByUser " + userId.ToString() + "," + hotelCategoryId;
                        }

                        var res_hotel = _db.Database.SqlQuery<int>(strSql3).ToList();
                        total = res_hotel[0];
                        break;
                    case 4: //LIFESTYLE
                        string lifestyleCategoryId = System.Configuration.ConfigurationManager.AppSettings["lifestyleCategoryId"] ?? "1";
//                        string strSql4 = @"SELECT    COUNT(1)
//                                            FROM       dbo.AttributeData INNER JOIN
//                                                      dbo.SupplyAttributeData ON dbo.AttributeData.AttrDataId = dbo.SupplyAttributeData.ArrtDataId INNER JOIN
//                                                      dbo.IBT ON dbo.SupplyAttributeData.SupplyId = dbo.IBT.SupplyId
//                                            WHERE dbo.AttributeData.ParentId=" + lifestyleCategoryId;
                        string strSql4 = "EXEC IBT_GetSupplyIBTCount " + lifestyleCategoryId;
                        if (userId > 0)
                        {
                            //strSql4 = strSql4 + " AND ibt.UserId=" + userId.ToString();
                            strSql4 = "EXEC IBT_GetSupplyIBTCountByUser " + userId.ToString() + "," + lifestyleCategoryId;
                        }

                        var res_lifestyle = _db.Database.SqlQuery<int>(strSql4).ToList();
                        total = res_lifestyle[0];
                        break;
                    case 5: //ATTRACTIONS
                        string attractionsCategoryId = System.Configuration.ConfigurationManager.AppSettings["attractionsCategoryId"] ?? "1";
//                        string strSql5 = @"SELECT    COUNT(1)
//                                            FROM       dbo.AttributeData INNER JOIN
//                                                      dbo.SupplyAttributeData ON dbo.AttributeData.AttrDataId = dbo.SupplyAttributeData.ArrtDataId INNER JOIN
//                                                      dbo.IBT ON dbo.SupplyAttributeData.SupplyId = dbo.IBT.SupplyId
//                                            WHERE dbo.AttributeData.ParentId=" + attractionsCategoryId;
                        string strSql5 = "EXEC IBT_GetSupplyIBTCount " + attractionsCategoryId;
                        if (userId > 0)
                        {
                            //strSql5 = strSql5 + " AND ibt.UserId=" + userId.ToString();
                            strSql5 = "EXEC IBT_GetSupplyIBTCountByUser " + userId.ToString() + "," + attractionsCategoryId;
                        }

                        var res_attractions = _db.Database.SqlQuery<int>(strSql5).ToList();
                        total = res_attractions[0];
                        break;
                }
                return total;
            });
        }

        /// <summary>
        /// Step4 Confrim的时候保存的
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <returns></returns>
        public BoolMessage IBeenThere(int userId, long epoch)
        {
            try
            {
                //获取用户选择的
                var AddList = (from c in _db.AddTravelsTemps where (c.UserId.Equals(userId) && c.Epoch.Equals(epoch)) select c).ToList<AddTravelsTemp>();

                //国家
                var countryList = AddList.Where(c => c.TypeId.Equals((int)Identifier.IBTType.Country)).ToList<AddTravelsTemp>();
                foreach (AddTravelsTemp country in countryList)
                {
                    var selCountryIds = country.Ids; //2,3,221
                    var arr_countryId = selCountryIds.Split(',');
                    foreach (string countryId in arr_countryId)
                    {
                        if (!Utils.IsNumeric(countryId)) //如果不是数字就下一个
                        {
                            continue;
                        }
                        int int_countryId = Convert.ToInt32(countryId);
                        //查看国家下面是否有选择城市
                        var cityList = AddList.Where(c => c.ParentId.Equals(int_countryId) && c.TypeId.Equals((int)Identifier.IBTType.City)).ToList<AddTravelsTemp>();
                        //如果cityList是空的话。那国家下面就没有数据了
                        if (cityList.Count == 0 || (cityList.Count == 1 && string.IsNullOrWhiteSpace(cityList[0].Ids))) //就保存数据（国家的）
                        {
                            //add newfeed(pv)
                            DateTime? pvDate = null;
                            string firstName = _profileService.GetUserProfile(userId).Firstname;
                            string strmsg = firstName + " has posted a visit here.";
                            //PostVisted(userId, int_countryId, 0, 0, pvDate);
                            this.PostVisted(userId, (int)Identifier.IBTType.Country, int_countryId, 0, 0, pvDate);
                            _newsFeedService.AddNewsFeed(userId, int_countryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                            IBeenThere(userId, int_countryId, 0, 0, strmsg);
                        }

                        #region City
                        foreach (var itemCity in cityList)
                        {
                            var selCityIds = itemCity.Ids; //419,423,746
                            var arr_cityId = selCityIds.Split(',');
                            foreach (var cityId in arr_cityId)
                            {
                                if (!Utils.IsNumeric(cityId)) //如果不是数字就下一个
                                {
                                    continue;
                                }
                                int int_cityId = Convert.ToInt32(cityId);
                                //查看城市下面是否选择了supply
                                var venueList = AddList.Where(c => c.ParentId.Equals(int_cityId) && c.TypeId.Equals((int)Identifier.IBTType.Supply)).ToList<AddTravelsTemp>();
                                if (venueList.Count == 0 || (venueList.Count == 1 && string.IsNullOrWhiteSpace(venueList[0].Ids)))
                                {
                                    //add newfeed(pv)
                                    DateTime? pvDate = null;
                                    string firstName = _profileService.GetUserProfile(userId).Firstname;
                                    string strmsg = firstName + " has posted a visit here.";
                                    //PostVisted(userId, int_countryId, 0, 0, pvDate);
                                    //PostVisted(userId, int_countryId, int_cityId, 0, pvDate);
                                    this.PostVisted(userId, (int)Identifier.IBTType.Country, int_countryId, 0, 0, pvDate);
                                    this.PostVisted(userId, (int)Identifier.IBTType.City, int_countryId, int_cityId, 0, pvDate);
                                    _newsFeedService.AddNewsFeed(userId, int_countryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                                    _newsFeedService.AddNewsFeed(userId, int_countryId, int_cityId, 0, Identifier.IBTType.City, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                                    //add city and country
                                    IBeenThere(userId, int_countryId, 0, 0, strmsg);
                                    IBeenThere(userId, int_countryId, int_cityId, 0, strmsg);
                                }
                                #region Supply
                                //Supply
                                foreach (var itemVenue in venueList)
                                {
                                    var selVenues = itemVenue.Ids; //2086,2097
                                    var arr_venueId = selVenues.Split(',');
                                    foreach (var venueId in arr_venueId)
                                    {
                                        if (!Utils.IsNumeric(venueId)) //如果不是数字就下一个
                                        {
                                            continue;
                                        }
                                        int int_venueId = Convert.ToInt32(venueId);
                                        //add newfeed(pv)
                                        DateTime? pvDate = null;
                                        string firstName = _profileService.GetUserProfile(userId).Firstname;
                                        string strmsg = firstName + " has posted a visit here.";
                                        //PostVisted(userId, int_countryId, 0, 0, pvDate);
                                        //PostVisted(userId, int_countryId, int_cityId, 0, pvDate);
                                        //PostVisted(userId, int_countryId, int_cityId, int_venueId, pvDate);
                                        this.PostVisted(userId, (int)Identifier.IBTType.Country, int_countryId, 0, 0, pvDate);
                                        this.PostVisted(userId, (int)Identifier.IBTType.City, int_countryId, int_cityId, 0, pvDate);
                                        this.PostVisted(userId, (int)Identifier.IBTType.Supply, int_countryId, int_cityId, int_venueId, pvDate);
                                        _newsFeedService.AddNewsFeed(userId, int_countryId, 0, 0, Identifier.IBTType.Country, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                                        _newsFeedService.AddNewsFeed(userId, int_countryId, int_cityId, 0, Identifier.IBTType.City, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);
                                        _newsFeedService.AddNewsFeed(userId, int_countryId, int_cityId, int_venueId, Identifier.IBTType.Supply, Identifier.FeedActiveType.AddTravel, strmsg, "", DateTime.Now);

                                        IBeenThere(userId, int_countryId, 0, 0, strmsg);
                                        IBeenThere(userId, int_countryId, int_cityId, 0, strmsg);
                                        IBeenThere(userId, int_countryId, int_cityId, int_venueId, strmsg);
                                    }

                                }
                                #endregion

                            }
                        }
                        #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("IBeenThere;userId=" + userId.ToString(), ex);
                return new BoolMessage(false, "");
            }

            return new BoolMessage(true, "");
        }

        /// <summary>
        /// I've Been There(FeedActiveType---PostVisit = 5)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public BoolMessage IBeenThere(int postType, int userId, int countryId, int cityId, int supplyId)
        {
            //var item = (from c in _db.IBTs
            //            where (c.UserId.Equals(userId) && c.CountryId.Equals(countryId) && c.CityId.Equals(cityId)
            //                && c.SupplyId.Equals(supplyId))
            //            select c).FirstOrDefault();
            DAL.IBT item = null;

            if (supplyId > 0)
                item = _db.IBTs.Where(i => i.StatusId == (int)Identifier.StatusType.Enabled && i.UserId == userId && i.SupplyId == supplyId).FirstOrDefault();
            else if (cityId > 0)
                item = _db.IBTs.Where(i => i.StatusId == (int)Identifier.StatusType.Enabled && i.UserId == userId && i.CityId == cityId && i.SupplyId == 0).FirstOrDefault();
            else if (countryId > 0)
                item = _db.IBTs.Where(i => i.StatusId == (int)Identifier.StatusType.Enabled && i.UserId == userId && i.CountryId == countryId && i.CityId == 0 && i.SupplyId == 0).FirstOrDefault();

            if (item == null)
            {
                DAL.IBT ibt = new DAL.IBT();
                ibt.UserId = userId;
                ibt.CountryId = countryId;
                ibt.CityId = cityId;
                ibt.SupplyId = supplyId;
                ibt.StatusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
                ibt.IbtCount = 1;
                ibt.CreateDate = DateTime.Now;
                ibt.UpdateDate = DateTime.Now;

                _db.IBTs.Add(ibt);
                _db.SaveChanges();


                if (postType == (int)Identifier.PostTypes.Detail)   //产生newfeed
                {
                    //news feed
                    Identifier.IBTType ibtType = Identifier.IBTType.Supply;
                    if (supplyId == 0 && cityId > 0)
                        ibtType = Identifier.IBTType.City;
                    else if (supplyId == 0 && cityId == 0)
                        ibtType = Identifier.IBTType.Country;
                    else
                        ibtType = Identifier.IBTType.Supply;
                    _newsFeedService.AddNewsFeed(userId, countryId, cityId, supplyId, ibtType, Identifier.FeedActiveType.IBT, "", "", DateTime.Now);
                }
            }
            else
            {
                item.IbtCount = item.IbtCount + 1;
                item.SupplyId = supplyId;
                item.CityId = cityId;
                item.CountryId = countryId;
                item.UpdateDate = DateTime.Now;
                _db.SaveChanges();
            }

            return new BoolMessage(true, "");
        }

        /// <summary>
        /// I've Been There(FeedActiveType--- AddTravel = 7)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public BoolMessage IBeenThere(int userId, int countryId, int cityId, int supplyId, string strMsg)
        {
            var item = (from c in _db.IBTs
                        where (c.UserId.Equals(userId) && c.CountryId.Equals(countryId) && c.CityId.Equals(cityId)
                            && c.SupplyId.Equals(supplyId))
                        select c).FirstOrDefault();
            if (item == null)
            {
                DAL.IBT ibt = new DAL.IBT();
                ibt.UserId = userId;
                ibt.CountryId = countryId;
                ibt.CityId = cityId;
                ibt.SupplyId = supplyId;
                ibt.StatusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
                ibt.IbtCount = 1;
                ibt.CreateDate = DateTime.Now;
                ibt.UpdateDate = DateTime.Now;

                _db.IBTs.Add(ibt);
                _db.SaveChanges();
            }
            else
            {
                item.IbtCount = item.IbtCount + 1;
                item.UpdateDate = DateTime.Now;
                _db.SaveChanges();
            }
            return new BoolMessage(true, "");
        }

        //删除post相应ibtcount减一
        public void ReIBTCount(int ibtId)
        {
            var item = (from c in _db.IBTs
                        where (c.UserId.Equals(ibtId))
                        select c).FirstOrDefault();
            if (item != null)
            {
                item.IbtCount = item.IbtCount - 1;
                item.UpdateDate = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public BoolMessage PostVisted(int userId, int int_countryId, int int_cityId, int int_venueId, DateTime? pvDate)
        {
            //var item = (from c in _db.PostVists
            //            where (c.UserId.Equals(userId) && c.CountryId.Equals(int_countryId) && c.CityId.Equals(int_cityId)
            //                && c.SupplyId.Equals(int_venueId))
            //            select c).FirstOrDefault();
            //if (item == null)
            //{
            //    PostVist pv = new PostVist();
            //    pv.UserId = userId;
            //    pv.CountryId = int_countryId;
            //    pv.CityId = int_cityId;
            //    pv.SupplyId = int_venueId;
            //    pv.Experiences = "";
            //    pv.SharingData = "";
            //    pv.PVDate = pvDate;
            //    pv.CreateDate = DateTime.Now;
            //    pv.StatusId = (int)Identifier.StatusType.Enabled;
            //    pv.IBTType = (int)Identifier.IBTType.Supply;
            //    _db.PostVists.Add(pv);
            //    _db.SaveChanges();
            //}
            //else
            //{
            //    item.PVDate = pvDate;
            //    item.CreateDate = DateTime.Now;
            //    _db.SaveChanges();
            //}
            PostVist pv = new PostVist();
            pv.UserId = userId;
            pv.CountryId = int_countryId;
            pv.CityId = int_cityId;
            pv.SupplyId = int_venueId;
            pv.Experiences = "";
            pv.SharingData = "";
            pv.PVDate = pvDate;
            pv.CreateDate = DateTime.Now;
            pv.StatusId = (int)Identifier.StatusType.Enabled;
            pv.IBTType = (int)Identifier.IBTType.Supply;
            _db.PostVists.Add(pv);
            _db.SaveChanges();
            return new BoolMessage(true, "");
        }

        public BoolMessage PostVisted(int userId, int ibtType, int int_countryId, int int_cityId, int int_venueId, DateTime? pvDate)
        {
            try
            {
                PostVist pv = new PostVist();
                pv.UserId = userId;
                pv.CountryId = int_countryId;
                pv.CityId = int_cityId;
                pv.SupplyId = int_venueId;
                pv.Experiences = "";
                pv.SharingData = "";
                pv.PVDate = pvDate;
                pv.CreateDate = DateTime.Now;
                pv.StatusId = (int)Identifier.StatusType.Enabled;
                pv.IBTType = ibtType;
                _db.PostVists.Add(pv);
                _db.SaveChanges();

                string key = "GetUserTravelbadge_" + userId.ToString();
                if (_cache.Contains(key))
                    _cache.Remove(key);

                return new BoolMessage(true, "");
            }
            catch (Exception ex)
            {
                return new BoolMessage(false, ex.Message);
            }
        }

        /// <summary>
        /// 返回用户Last been to : city,country
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string LastBeenToCityCountryName(int userId)
        {
            string sql = @"SELECT TOP 1 ISNULL(dbo.Cities.CityId,0) AS CityId,dbo.Cities.CityName,dbo.Countries.CountryName,dbo.Countries.CountryId
                            FROM         dbo.IBT 
			                             INNER JOIN dbo.Countries ON dbo.IBT.CountryId = dbo.Countries.CountryId 
                                         LEFT OUTER JOIN dbo.Cities ON dbo.IBT.CityId = dbo.Cities.CityId
                            WHERE IBT.UserId={0}
                            ORDER BY dbo.Cities.UpdateDate DESC";
            var res = _db.Database.SqlQuery<CountryCityItem>(sql, userId).ToList();
            if (res.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(res[0].CityName))
                    return res[0].CityName + ", " + res[0].CountryName;
                else
                    return res[0].CountryName;
            }
            else
                return "";
        }
        #endregion

        #region Post visit

        /// <summary>
        /// 返回首页（index）的PostedVisitsCount
        /// </summary>
        /// <returns></returns>
        public int GetPostedVisitsCount()
        {
            return (from c in _db.PostVists select c).Count();
        }

        /// <summary>
        /// 返回用户的PostVisit (Countries)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public int HasUserPVCountries(int userId, int countryId)
        {
            //            string strSql2 = @"SELECT count(*) FROM dbo.IBT 
            //                              LEFT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND  dbo.IBT.UserId = dbo.NewsFeed.UserId 
            //                              LEFT JOIN dbo.Countries ON dbo.NewsFeed.CountryId = dbo.Countries.CountryId 
            //                              LEFT JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId 
            //                              WHERE dbo.NewsFeed.ActiveType IN (5,6,7) AND dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE  CountryId={1} AND CityId=0 AND SupplyId=0 AND dbo.IBT.UserId={0} GROUP BY dbo.IBT.UserId,dbo.IBT.CountryId) AND dbo.NewsFeed.IBTType=1 ";
            string strSql = @"SELECT count(*) FROM dbo.IBT 
                               JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND  dbo.IBT.UserId = dbo.NewsFeed.UserId 
                               JOIN dbo.Countries ON dbo.NewsFeed.CountryId = dbo.Countries.CountryId 
                               JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId 
                               WHERE dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE StatusId=1 AND  CountryId={1} AND CityId=0 AND SupplyId=0 AND dbo.IBT.UserId={0} GROUP BY dbo.IBT.UserId,dbo.IBT.CountryId) 
                               AND dbo.NewsFeed.NewsFeedId IN (SELECT MAX(NewsFeedId) FROM dbo.NewsFeed WHERE UserId={0} AND dbo.NewsFeed.IBTType=1 AND dbo.NewsFeed.ActiveType IN (5,6,7)  GROUP BY CountryId) ";
            var list = _db.Database.SqlQuery<int>(strSql, userId, countryId).ToList();
            return list[0];
        }
        public int HasUserPVCities(int userId, int cityId)
        {
            //            string strSql2 = @"SELECT COUNT(*) FROM dbo.IBT 
            //                              LEFT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND dbo.IBT.UserId = dbo.NewsFeed.UserId 
            //                              LEFT JOIN dbo.Cities ON dbo.NewsFeed.CityId = dbo.Cities.CityId 
            //                              LEFT JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId 
            //                              LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
            //                              WHERE dbo.NewsFeed.ActiveType IN (5,6,7) and dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE CityId={1} AND SupplyId=0 AND dbo.IBT.UserId={0} GROUP BY dbo.IBT.UserId,dbo.IBT.CityId) AND dbo.NewsFeed.IBTType=2 ";

            string strSql = @"SELECT COUNT(*) FROM dbo.IBT 
                               LEFT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND dbo.IBT.UserId = dbo.NewsFeed.UserId 
                               LEFT JOIN dbo.Cities ON dbo.NewsFeed.CityId = dbo.Cities.CityId 
                               LEFT JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId 
                               LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
                               WHERE dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE StatusId=1 AND CityId={1} AND SupplyId=0 AND dbo.IBT.UserId={0} GROUP BY dbo.IBT.UserId,dbo.IBT.CityId) 
                               AND dbo.NewsFeed.NewsFeedId IN (SELECT MAX(NewsFeedId) FROM dbo.NewsFeed WHERE UserId={0}  AND dbo.NewsFeed.IBTType=2 AND dbo.NewsFeed.ActiveType IN (5,6,7) GROUP BY CityId)";
            var list = _db.Database.SqlQuery<int>(strSql, userId, cityId).ToList();
            return list[0];
        }
        public int HasUserPVSupply(int userId, int supplyId)
        {
            string strSql = @"SELECT count(*) FROM dbo.IBT 
                              LEFT JOIN dbo.NewsFeed ON dbo.IBT.SupplyId = dbo.NewsFeed.SupplyId AND dbo.IBT.CityId = dbo.NewsFeed.CityId AND dbo.IBT.CountryId = dbo.NewsFeed.CountryId AND  dbo.IBT.UserId = dbo.NewsFeed.UserId 
                              LEFT JOIN dbo.Cities ON dbo.NewsFeed.CityId = dbo.Cities.CityId 
                              LEFT JOIN dbo.Countries ON dbo.NewsFeed.CountryId = dbo.Countries.CountryId 
                              LEFT JOIN dbo.Profiles ON dbo.NewsFeed.UserId = dbo.Profiles.UserId WHERE dbo.NewsFeed.ActiveType IN (5,6,7) AND dbo.IBT.IBTId IN  (SELECT MAX(IBTId) as id FROM dbo.IBT WHERE SupplyId={1} AND dbo.IBT.UserId={0} GROUP BY dbo.IBT.UserId,dbo.IBT.SupplyId) AND dbo.NewsFeed.IBTType=3 ";
            var list = _db.Database.SqlQuery<int>(strSql, userId, supplyId).ToList();
            return list[0];
        }
        /// <summary>
        /// 获取用户ibt过的city的ibt列表
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<DAL.IBT> GetUserIBTCities(int userId)
        {
            return _db.IBTs.Where(ibt => ibt.UserId == userId && ibt.CityId > 0 && ibt.SupplyId == 0 && ibt.StatusId == 1).ToList();
        }

        /// <summary>
        /// get user ibt list
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<DAL.IBT> GetUserIbt(int userId)
        {
            return _db.IBTs.Where(i => i.UserId == userId && i.StatusId == (int)Identifier.StatusType.Enabled).ToList();
        }


        /// <summary>
        /// PostVisit 的Country的信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public PostVisitItem GetPostVisitCountryInfo(int userId, int countryId)
        {
            string sql = @"SELECT  
                                Countries.CountryId AS ObjId,Countries.CountryName AS Title, dbo.Countries.FlagSrc AS CoverPhotoSrc,
                                0 AS CommentsCount,
                                0 AS IbtCount,
                                dbo.Countries.CountryName, ISNULL(Countries.CountryId,0) AS CountryId,
                                '' AS CityName,0 AS CityId,ISNULL(dbo.IBT.SupplyId,0) AS SupplyId,
                                ISNULL(RRWB.Rating,0) AS Rating,
                                ISNULL(RRWB.Recommended,0) AS Recommended,
                                ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
                                ISNULL(RRWB.BucketList,0) AS BucketList
                        FROM    Countries
		                          LEFT JOIN IBT ON Countries.CountryId = IBT.CountryId AND IBT.CityId=0 AND ibt.SupplyId=0 AND IBT.UserId={0}
		                          LEFT JOIN RRWB ON Countries.CountryId = RRWB.[ObjId] AND RRWB.IBTType=1 AND RRWB.UserId={0}
                        WHERE Countries.CountryId={1}";
            var res = _db.Database.SqlQuery<PostVisitItem>(sql, userId, countryId).FirstOrDefault();
            return res == null ? new PostVisitItem() : res;
        }
        /// <summary>
        /// PostVisit 的City的信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public PostVisitItem GetPostVisitCityInfo(int userId, int cityId)
        {
            string sql = @"SELECT  
                                Cities.CityId AS ObjId,Cities.CityName AS Title,  dbo.Countries.FlagSrc AS CoverPhotoSrc,
                                0 AS CommentsCount,
                                0 AS IbtCount,
                                dbo.Countries.CountryName, ISNULL(Countries.CountryId,0) AS CountryId,
                                dbo.Cities.CityName,ISNULL(dbo.Cities.CityId,0) AS CityId,
                                ISNULL(RRWB.Rating,0) AS Rating, ISNULL(dbo.IBT.SupplyId,0) AS SupplyId,
                                ISNULL(RRWB.Recommended,0) AS Recommended,
                                ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
                                ISNULL(RRWB.BucketList,0) AS BucketList
                        FROM    Cities
                                  LEFT JOIN Countries ON Cities.CountryId = Countries.CountryId 
                                  LEFT JOIN IBT ON Cities.CityId = IBT.CityId AND ibt.SupplyId=0 AND IBT.UserId={0}
                                  LEFT JOIN RRWB ON Cities.CityId = RRWB.[ObjId] AND RRWB.IBTType=2 AND RRWB.UserId={0}
                        WHERE Cities.CityId={1}";

            var res = _db.Database.SqlQuery<PostVisitItem>(sql, userId, cityId).FirstOrDefault();
            return res == null ? new PostVisitItem() : res;
        }

        /// <summary>
        /// PostVisit 的Supply的信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public PostVisitItem GetPostVisitSupplyInfo(int userId, int supplyId)
        {
            string sql = @"SELECT  
                                Supplies.SupplyId,Supplies.Title,dbo.Supplies.CoverPhotoSrc,Supplies.InfoState,Supplies.ServiceType,
                                ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
                                ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
                                dbo.Countries.CountryName, ISNULL(Countries.CountryId,0) AS CountryId,
                                dbo.Cities.CityName,ISNULL(dbo.Cities.CityId,0) AS CityId,
                                ISNULL(RRWB.Rating,0) AS Rating,
                                ISNULL(RRWB.Recommended,0) AS Recommended,
                                ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
                                ISNULL(RRWB.BucketList,0) AS BucketList
                        FROM      Supplies 
                                  LEFT JOIN Cities ON Cities.CityId=Supplies.CityId
                                  LEFT JOIN Countries ON Countries.CountryId = Supplies.CountryId 
                                  LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId AND IBT.UserId={0}
                                  LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND RRWB.IBTType=3 AND RRWB.UserId={0}
                        WHERE Supplies.StatusId=1 AND Supplies.SupplyId={1}";
            var res = _db.Database.SqlQuery<PostVisitItem>(sql, userId, supplyId).FirstOrDefault();

            return res == null ? new PostVisitItem() : res;
        }

        /// <summary>
        /// SavePostVisit
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="CountryId"></param>
        /// <param name="cityId"></param>
        /// <param name="SupplyId"></param>
        /// <param name="experiences"></param>
        /// <param name="sharingData"></param>
        /// <param name="pvDate"></param>
        /// <returns></returns>
        public BoolMessageItem<int> SavePostVisit(int postType, int userId, int ibtType, int countryId, int cityId, int supplyId, string experiences, string sharingData, DateTime? pvDate)
        {
            try
            {
                PostVist pv = new PostVist();
                pv.UserId = userId;
                pv.CountryId = countryId;
                pv.CityId = cityId;
                pv.SupplyId = supplyId;
                pv.Experiences = experiences;
                pv.SharingData = sharingData;
                pv.PVDate = pvDate;
                pv.CreateDate = DateTime.Now;
                pv.StatusId = (int)Identifier.StatusType.Enabled;
                pv.IBTType = ibtType;

                _db.PostVists.Add(pv);
                _db.SaveChanges();
                string key = "GetUserTravelbadge_" + userId.ToString();
                if (_cache.Contains(key))
                    _cache.Remove(key);
                //更新pv数 （不需要统计在supply detail的评论数里 PV的）
                //                string sql = @"UPDATE [Supplies]
                //                               SET [CommentsCount] = (SELECT COUNT(1) FROM PostVists WHERE SupplyId={0} AND StatusId=1)
                //                               WHERE  Supplies.SupplyId={0}";
                //                _db.Database.ExecuteSqlCommand(sql, supplyId);

                //这里需要记录ibt
                this.IBeenThere(postType, userId, countryId, cityId, supplyId);

                //需要记录feed(需要)
                var bmi = new BoolMessageItem<int>(0, false, "");
                bmi = _newsFeedService.AddNewsFeed(userId, countryId, cityId, supplyId, ibtType, Identifier.FeedActiveType.PostVisit, experiences, pv.PVId.ToString(), pvDate);

                return new BoolMessageItem<int>(pv.PVId, true, bmi.Item.ToString());
            }
            catch (Exception ex)
            {
                _logger.Error("SavePostVisit Error!", ex);
                return new BoolMessageItem<int>(-1, false, "");
            }
        }

        //Edit
        public BoolMessageItem<int> EditPostVisit(int pvId, int newsfeedId, string experiences, DateTime? pvDate)
        {
            try
            {
                PostVist pv = _db.PostVists.FirstOrDefault(x => x.PVId.Equals(pvId));
                if (pv != null)
                {
                    pv.Experiences = experiences;
                    pv.PVDate = pvDate;
                    pv.CreateDate = DateTime.Now;
                    _db.SaveChanges();

                    //这里不需要修改记录ibt
                    //this.IBeenThere(userId, countryId, cityId, supplyId, "");

                    //需要修改记录feed
                    _newsFeedService.EditNewsFeed(newsfeedId, experiences, pvId.ToString(), pvDate);

                    return new BoolMessageItem<int>(pv.PVId, true, "Edit successful!");

                }
                else
                {
                    return new BoolMessageItem<int>(0, false, "Edit failure!");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("EditPostVisit Error!", ex);
                return new BoolMessageItem<int>(-1, false, "");
            }
        }


        /// <summary>
        /// 保存用户上传的图片
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="fileName">图形名称</param>
        /// <param name="sourceFileName">路径（/0/0/tesst/guid.jpg）</param>
        /// <returns></returns>
        public BoolMessageItem<int> SavePostvisitFile(string guid, string fileName, string sourceFileName)
        {
            try
            {
                PostVistAttachment pva = new PostVistAttachment();
                pva.PvId = 0;
                pva.Guid = guid;
                pva.FileName = fileName;
                pva.SourceFileName = sourceFileName;
                pva.StatusId = Utils.IntToByte((int)Identifier.FileStatusType.Enabled);
                pva.CreateDate = DateTime.Now;

                _db.PostVistAttachments.Add(pva);
                _db.SaveChanges();

                //return new BoolMessage(true, "");
                return new BoolMessageItem<int>(pva.AttId, true, "");
            }
            catch (Exception ex)
            {
                _logger.Error("SavePostvisitFile", ex);
                return new BoolMessageItem<int>(0, false, "");
            }
        }

        /// <summary>
        /// 更改PvId
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="pvId"></param>
        public void SetPostVistAttachmentPvId(string guid, int pvId)
        {
            string sql = @"UPDATE [PostVistAttachment]
                               SET [PvId] = {0} WHERE [Guid] = {1}";
            _db.Database.ExecuteSqlCommand(sql, pvId, guid);
        }

        /// <summary>
        /// 返回用户LAST POSTED VISIT: supplierName,cityName,countryName
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string LastPostedVistPlaceName(int userId)
        {
            string sql = @"SELECT    TOP 1 ISNULL(dbo.Cities.CityId,0) AS CityId,dbo.Cities.CityName,dbo.Countries.CountryName,dbo.Countries.CountryId
                            FROM         dbo.PostVists INNER JOIN            
                                        dbo.Countries ON dbo.PostVists.CountryId = dbo.Countries.CountryId LEFT OUTER JOIN
                                        dbo.Cities ON dbo.PostVists.CityId = dbo.Cities.CityId LEFT OUTER JOIN
                                        dbo.Supplies ON dbo.PostVists.SupplyId = dbo.Supplies.SupplyId AND dbo.Countries.CountryId = dbo.Supplies.CountryId 
                            WHERE dbo.PostVists.UserId={0}
                            ORDER BY dbo.PostVists.PVId DESC";
            var res = _db.Database.SqlQuery<LastPVItem>(sql, userId).ToList();
            if (res.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(res[0].Title))
                {
                    return "<a  href=\"" + res[0].ExTitleUrl + "\"><strong>" + res[0].Title.ToString() + "</strong></a>";
                }
                else if (!string.IsNullOrWhiteSpace(res[0].CityName))
                {
                    return "<a  href=\"" + res[0].ExTitleUrl + "\"><strong>" + res[0].CityName.ToString() + "</strong></a>" + ",<a  href=\"/Country/About/1/" + res[0].CountryId.ToString() + "\"><strong>" + res[0].CountryName + "</strong></a>";
                }
                else
                {
                    return "<a  href=\"" + res[0].ExTitleUrl + "\"><strong>" + res[0].CountryName.ToString() + "</strong></a>";
                }
            }
            else
                return "";

        }

        /// <summary>
        /// 返回用户的Last supply
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public BoolMessage LastPostedVistSupplyName(int userId)
        {
            var res = (from c in _db.Supplies
                       join p in _db.PostVists on c.SupplyId equals p.SupplyId
                       where p.UserId.Equals(userId)
                       orderby p.PVId descending
                       select new { Title = c.Title, SupplyId = c.SupplyId }).Take(1).ToList();

            if (res.Count > 0)
            {
                string href = "<a href=\"/Supplier/v/" + res[0].SupplyId + "-" + UrlSeoUtils.BuildValidUrl(res[0].Title) + "\">" + res[0].Title + "</a>";
                return new BoolMessage(true, href);
            }
            else
                return new BoolMessage(false, "");
        }
        /// <summary>
        /// 返回用户的last postvisit
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public BoolMessage LastPosteVistName(int userId)
        {
            BoolMessage bm = new BoolMessage(false, string.Empty);

            PostVist pv = _db.PostVists.Where(p => p.UserId == userId && p.StatusId == (int)Identifier.StatusType.Enabled).OrderByDescending(p=>p.PVId).FirstOrDefault();
            if (pv != null)
            {
                string href = "";
                if (pv.IBTType == (int)Identifier.IBTType.Country)
                {
                    Country country = _db.Countries.Find(pv.CountryId);
                    if (country != null)
                        href = "<a href='/country/newsfeed/1/" + country.CountryId.ToString() + "'>" + country.CountryName + "</a>";
                }
                else if (pv.IBTType == (int)Identifier.IBTType.City)
                {
                    City city = _db.Cities.Find(pv.CityId);
                    if (city != null)
                        href = "<a href='/city/newsfeed/2/" + city.CityId.ToString() + "'>" + city.CityName + "</a>";
                }
                else if (pv.IBTType == (int)Identifier.IBTType.Supply)
                {
                    Supply supply = _db.Supplies.Find(pv.SupplyId);
                    if (supply != null)
                        href = "<a href=\"/supplier/v/" + supply.SupplyId.ToString() + "-" + UrlSeoUtils.BuildValidUrl(supply.Title) + "\">" + supply.Title + "</a>";
                }
                if (!string.IsNullOrEmpty(href))
                    bm = new BoolMessage(true, href);
            }

            return bm;
        }

        /// <summary>
        /// 根据Guid获取国家信息
        /// </summary>
        /// <param name="Guid"></param>
        /// <returns></returns>
        public PostVistAttachment GetPostVisitInfo(string Guid)
        {
            return (from c in _db.PostVistAttachments where c.Guid.Equals(Guid) select c).FirstOrDefault();
        }
        public PostVistAttachment GetPostVisitImageInfo(string SourceFileName)
        {
            return (from c in _db.PostVistAttachments where c.SourceFileName.Equals(SourceFileName) select c).FirstOrDefault();
        }
        public BoolMessageItem<int> PostVisitIsDel(string AttId)
        {
            string strsql = @"Update [PostVistAttachment] SET [StatusId]=3
                             WHERE [AttId] = {0}";
            _db.Database.ExecuteSqlCommand(strsql, AttId);
            return new BoolMessageItem<int>(1, true, "Delete Successful!");
        }
        /// <summary>
        /// 判断是否有postvisit过
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ibtType"></param>
        /// <param name="objId"></param>
        /// <returns></returns>
        public bool HasUserPostVisited(int userId, int ibtType, int objId)
        {
            bool isPosted = false;
            switch (ibtType)
            {
                case (int)Identifier.IBTType.Country:
                    isPosted = _db.PostVists.IsTrueForAny(p => p.UserId == userId && p.IBTType == ibtType && p.CountryId == objId);
                    break;
                case (int)Identifier.IBTType.City:
                    isPosted = _db.PostVists.IsTrueForAny(p => p.UserId == userId && p.IBTType == ibtType && p.CityId == objId);
                    break;
                case (int)Identifier.IBTType.Supply:
                    isPosted = _db.PostVists.IsTrueForAny(p => p.UserId == userId && p.IBTType == ibtType && p.SupplyId == objId);
                    break;
                default:
                    break;
            }
            return isPosted;
        }

        #endregion

        #region AddTravels(需要修改)
        /// <summary>
        /// 获取用户选择的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="Epoch"></param>
        /// <param name="travelType"></param>
        /// <param name="objId"></param>
        /// <returns></returns>
        public AddTravelsTemp GetAddTravels(int userId, long epoch, Identifier.AddTravelsType travelType, int parentId)
        {
            var item = (from c in _db.AddTravelsTemps
                        where (c.UserId.Equals(userId) && c.Epoch.Equals(epoch)
                               && c.TypeId.Equals((int)travelType) && c.ParentId.Equals(parentId))
                        select c).FirstOrDefault();
            if (item == null)
                return new AddTravelsTemp();
            else
                return item;
        }

        /// <summary>
        /// AddTravels
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <param name="travelType"></param>
        /// <param name="parentId"></param>
        /// <param name="vals"></param>
        public void AddTravels(int userId, long epoch, Identifier.AddTravelsType travelType, int parentId, string vals)
        {
            //先删除下面的数据然后再添加
            string delSql = "DELETE FROM [AddTravelsTemp] WHERE UserId={0} AND Epoch={1} AND TypeId={2} AND ParentId={3}";
            _db.Database.ExecuteSqlCommand(delSql, userId, epoch, (int)travelType, parentId);

            AddTravelsTemp att = new AddTravelsTemp();
            att.UserId = userId;
            att.Epoch = epoch;
            att.TypeId = (int)travelType;
            att.ParentId = parentId;
            att.Ids = vals;
            att.CreateDate = DateTime.Now;

            //取消选择国家时删除相关国家下的city和venue记录
            if (travelType == Identifier.AddTravelsType.Country)
            {
                List<AddTravelsTemp> userAttAllList = _db.AddTravelsTemps.Where(a => a.UserId == userId && a.Epoch == epoch).ToList();
                string[] countryIds = vals.Split(',');
                //查找取消选择的国家
                List<AddTravelsTemp> userAttcountryList = userAttAllList.Where(a => a.TypeId == (int)Identifier.AddTravelsType.City && !countryIds.Contains(a.ParentId.ToString())).ToList();
                userAttcountryList.ForEach(attCountry => {
                    if (!string.IsNullOrEmpty(attCountry.Ids))
                    {
                        string[] cityIds=attCountry.Ids.Split(',');
                        List<AddTravelsTemp> userAttCityList = userAttAllList.Where(a => a.TypeId == (int)Identifier.AddTravelsType.Venue && !cityIds.Contains(a.ParentId.ToString())).ToList();
                        userAttCityList.ForEach(a => { _db.AddTravelsTemps.Remove(a); });
                    }
                    _db.AddTravelsTemps.Remove(attCountry);
                });
            }

            if (travelType == Identifier.AddTravelsType.City)
            {
                List<AddTravelsTemp> userAttAllList = _db.AddTravelsTemps.Where(a => a.UserId == userId && a.Epoch == epoch).ToList();
                List<AddTravelsTemp> attCityList = userAttAllList.Where(a => a.ParentId != parentId && a.TypeId == (int)Identifier.AddTravelsType.City).ToList();
                string venueIdsStr = vals;
                attCityList.ForEach(attCity =>
                {
                    if (!string.IsNullOrEmpty(attCity.Ids))
                        venueIdsStr = venueIdsStr + "," + attCity.Ids;
                });
                string[] venueIdsStrArray = venueIdsStr.Split(',');
                List<AddTravelsTemp> attVenueList = userAttAllList.Where(a => a.TypeId == (int)Identifier.AddTravelsType.Venue && !venueIdsStrArray.Contains(a.ParentId.ToString())).ToList();
                attVenueList.ForEach(a => { _db.AddTravelsTemps.Remove(a); });
            }

            _db.AddTravelsTemps.Add(att);
            _db.SaveChanges();
        }

        /// <summary>
        /// 删除用户选择的临时数据
        /// </summary>
        /// <param name="userId"></param>
        public void DelAddTravels(int userId)
        {
            string sql = "DELETE FROM [AddTravelsTemp] WHERE UserId={0}";
            _db.Database.ExecuteSqlCommand(sql, userId);
        }

        /// <summary>
        /// AddTravels
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public IList<SupplyListShort> AddTravelsGetSupplyListAll(string keyWord, string filterLetter, int userId, int cityId, int pageSize = 10, int pageNumber = 1)
        {
            int startSize = (pageNumber - 1) * pageSize + 1;
            int endSize = startSize + pageSize - 1;
            /*
            string sql = @"SELECT t1.* FROM (
                                   SELECT  
                                    Supplies.SupplyId,Supplies.Title,dbo.Supplies.CoverPhotoSrc,
		                            ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
		                            ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
		                            dbo.Countries.CountryName, Countries.CountryId,
		                            dbo.Cities.CityName,dbo.Cities.CityId,
	 	                            ISNULL(RRWB.Rating,0) AS Rating,
		                            ISNULL(RRWB.Recommended,0) AS Recommended,
		                            ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
		                            ISNULL(RRWB.BucketList,0) AS BucketList,
			                        ROW_NUMBER() OVER (ORDER BY Supplies.Title ASC) AS RowId
                            FROM      Supplies 
		                              INNER JOIN Cities ON Cities.CityId=Supplies.CityId
                                      INNER JOIN Countries ON Cities.CountryId = Countries.CountryId 
                                      LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId AND IBT.UserId={0}
                                      LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND IBTType=3 AND RRWB.UserId={0}
                            WHERE Supplies.StatusId=1 AND Supplies.CityId={1} AND Supplies.Title LIKE '"+ filterLetter +@"%' AND Supplies.Title  LIKE '%" + keyWord + @"%'  
                            ) As t1
	                        WHERE RowId BETWEEN " + startSize.ToString() + " AND " + endSize.ToString() + "";
             * */
            string _sqlwhere = "";
            string[] keywords = keyWord.Split(' ').ToArray();

            foreach (var item in keywords)
            {
                if (!string.IsNullOrEmpty(item.Trim()))
                {
                    _sqlwhere = _sqlwhere + " AND Supplies.Title LIKE '%" + item + "%' ";
                }
            }

            if (string.IsNullOrEmpty(keyWord))
            {
                _sqlwhere = " or 1=1 ";
            }

            string sql = @"SELECT t1.*
                FROM 
                (
	                SELECT ROW_NUMBER() OVER (ORDER BY temp_rowid ASC, Title ASC) AS RowId,* 
	                FROM(
			                SELECT 0 AS temp_rowid,
			                    Supplies.SupplyId,Supplies.Title,dbo.Supplies.CoverPhotoSrc,
		                        ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
		                        ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
		                        dbo.Countries.CountryName, Countries.CountryId,
		                        dbo.Cities.CityName,dbo.Cities.CityId,
	 	                        ISNULL(RRWB.Rating,0) AS Rating,
		                        ISNULL(RRWB.Recommended,0) AS Recommended,
		                        ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
		                        ISNULL(RRWB.BucketList,0) AS BucketList
			                FROM Supplies 
		                        INNER JOIN Cities ON Cities.CityId=Supplies.CityId
                                INNER JOIN Countries ON Cities.CountryId = Countries.CountryId 
                                LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId AND IBT.UserId=" + userId + @"
                                LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND IBTType=3 AND RRWB.UserId=" + userId + @"
                            WHERE Supplies.StatusId=1 
                                AND Supplies.CityId=" + cityId + @"
                                AND Supplies.Title LIKE '" + filterLetter + @"%' 
                                AND Supplies.Title  LIKE '%" + keyWord + @"%' 
	                UNION
			                SELECT 1 AS temp_rowid,
			                    Supplies.SupplyId,Supplies.Title,dbo.Supplies.CoverPhotoSrc,
		                        ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
		                        ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
		                        dbo.Countries.CountryName, Countries.CountryId,
		                        dbo.Cities.CityName,dbo.Cities.CityId,
	 	                        ISNULL(RRWB.Rating,0) AS Rating,
		                        ISNULL(RRWB.Recommended,0) AS Recommended,
		                        ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
		                        ISNULL(RRWB.BucketList,0) AS BucketList
			                FROM Supplies 
		                        INNER JOIN Cities ON Cities.CityId=Supplies.CityId
                                INNER JOIN Countries ON Cities.CountryId = Countries.CountryId 
                                LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId AND IBT.UserId=" + userId + @"
                                LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND IBTType=3 AND RRWB.UserId=" + userId + @"
                            WHERE Supplies.StatusId=1 
                                AND Supplies.CityId=" + cityId + @"
                                AND Supplies.Title LIKE '" + filterLetter + @"%' 
				                AND Supplies.Title LIKE '%" + keywords[0] + @"%'
				                AND Supplies.SupplyId not in(
					                SELECT SupplyId FROM dbo.Supplies 
						                WHERE Supplies.Title LIKE '" + filterLetter + @"%' 
							                AND Supplies.Title LIKE '%" + keyWord + @"%'
						                )
	                UNION
			                SELECT 2 AS temp_rowid,
			                    Supplies.SupplyId,Supplies.Title,dbo.Supplies.CoverPhotoSrc,
		                        ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
		                        ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
		                        dbo.Countries.CountryName, Countries.CountryId,
		                        dbo.Cities.CityName,dbo.Cities.CityId,
	 	                        ISNULL(RRWB.Rating,0) AS Rating,
		                        ISNULL(RRWB.Recommended,0) AS Recommended,
		                        ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
		                        ISNULL(RRWB.BucketList,0) AS BucketList
			                FROM Supplies 
		                        INNER JOIN Cities ON Cities.CityId=Supplies.CityId
                                INNER JOIN Countries ON Cities.CountryId = Countries.CountryId 
                                LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId AND IBT.UserId=" + userId + @"
                                LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND IBTType=3 AND RRWB.UserId=" + userId + @"
                            WHERE (1=2 " + _sqlwhere + @") 
				                AND Supplies.StatusId=1 
                                AND Supplies.CityId=" + cityId + @"
                                AND Supplies.Title LIKE '" + filterLetter + @"%' 
				                AND Supplies.SupplyId not in(
					                SELECT SupplyId FROM dbo.Supplies 
						                WHERE Supplies.Title LIKE '" + filterLetter + @"%' 
							                AND (Supplies.Title LIKE '%" + keyWord + @"%'
							                OR Supplies.Title LIKE '%" + keywords[0] + @"%')
													                )
		                ) AS temp
	                ) AS t1 
	             WHERE RowId BETWEEN " + startSize.ToString() + " AND " + endSize.ToString() + "";


            //ORDER BY Supplies.Title DESC
            return _db.Database.SqlQuery<SupplyListShort>(sql).ToList();
        }

        public List<SupplyListShort> AddTravelsGetSupplyListSortBy(string keyWord, string filterLetter, int userId, int cityId, int pageSize = 10, int pageNumber = 1, string sortBy = "")
        {
            int endPoint = pageSize * pageNumber;
            int startPoint = (pageNumber - 1) * pageSize;

            City city = _db.Cities.Find(cityId);
            Country country = this.GetAllCountry().First(c => c.CountryId == city.CountryId);
            List<DAL.IBT> ibtList = _db.IBTs.Where(i => i.UserId == userId && i.CityId == cityId && i.SupplyId > 0).ToList();
            List<RRWB> rrwbList = _db.RRWBs.Where(r => r.UserId == userId && r.StatusId == (int)Identifier.StatusType.Enabled && r.IBTType == (int)Identifier.IBTType.Supply).ToList();

            List<SupplyListShort> supplyShortList = _db.Supplies.Where(s => s.CityId == cityId && s.StatusId == (int)Identifier.StatusType.Enabled).OrderBy(s => s.Title).Select(s => new SupplyListShort() { SupplyId = s.SupplyId, Title = s.Title, CoverPhotoSrc = s.CoverPhotoSrc, CommentsCount = s.CommentsCount }).ToList();

            if (!string.IsNullOrEmpty(filterLetter))
                supplyShortList = supplyShortList.Where(s => s.Title.StartsWith(filterLetter)).ToList();

            if (!string.IsNullOrEmpty(keyWord))
                supplyShortList = supplyShortList.Where(s => s.Title.ToLower().Contains(keyWord.ToLower())).ToList();

            string adName=@"'Hotels','Lifestyle','Attractions'";

            if (sortBy == "Hotels")
            {
                adName = @"'Hotels'";
            }

            if (sortBy == "Others")
            {
                adName = @"'Lifestyle','Attractions'";
            }

            string sqlStr = @"SELECT dbo.Supplies.SupplyId FROM dbo.Supplies 
                                LEFT JOIN dbo.SupplyAttributeData ON dbo.Supplies.SupplyId = dbo.SupplyAttributeData.SupplyId
                                LEFT JOIN dbo.AttributeData ON dbo.AttributeData.AttrDataId=dbo.SupplyAttributeData.ArrtDataId
                                LEFT JOIN dbo.AttributeData sub ON sub.AttrDataId=dbo.AttributeData.ParentId
                                WHERE dbo.Supplies.StatusId=1 AND CityId=" + city.CityId.ToString() + " AND sub.AttrDataName IN (" + adName + ")";
            List<int> supplyIds = _db.Database.SqlQuery<int>(sqlStr).ToList();

            supplyShortList = supplyShortList.Where(ssl => supplyIds.Contains(ssl.SupplyId)).ToList();

            supplyShortList = supplyShortList.Take(endPoint).Skip(startPoint).ToList();

            supplyShortList.ForEach(ssl => {
                ssl.CountryId = country.CountryId;
                ssl.CountryName = country.CountryName;
                ssl.CityId = city.CityId;
                ssl.CityName = city.CityName;

                DAL.IBT ibt = ibtList.FirstOrDefault(i => i.SupplyId == ssl.SupplyId);
                if (ibt != null)
                {
                    ssl.IbtCount = ibt.IbtCount;
                }

                RRWB rrwb = rrwbList.FirstOrDefault(i => i.ObjId == ssl.SupplyId);
                if (rrwb != null)
                {
                    ssl.Rating = rrwb.Rating;
                    ssl.Recommended = rrwb.Rating;
                    ssl.WanttoGo = rrwb.WanttoGo;
                    ssl.BucketList = rrwb.BucketList;
                }
            });

            return supplyShortList;
        }

        /// <summary>
        /// AddTravelsGetSupplyListSortByActivity
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <param name="categoryName">Hotels,Lifestyle,Attractions</param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public IList<SupplyListShort> AddTravelsGetSupplyListSortByCategory(string keyWord, string filterLetter, int userId, int cityId, string categoryName, int pageSize = 10, int pageNumber = 1)
        {
            int startSize = (pageNumber - 1) * pageSize + 1;
            int endSize = startSize + pageSize - 1;
            string sql = @"SELECT 	t2.* FROM (
	                    SELECT t1.*,ROW_NUMBER() OVER (ORDER BY Title) AS RowId FROM (
		                    SELECT  
				                    Supplies.SupplyId,Supplies.Title,dbo.Supplies.CoverPhotoSrc,
				                    ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
				                    ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
				                    dbo.Countries.CountryName, Countries.CountryId,
				                    dbo.Cities.CityName,dbo.Cities.CityId,
				                    ISNULL(RRWB.Rating,0) AS Rating,
				                    ISNULL(RRWB.Recommended,0) AS Recommended,
				                    ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
				                    ISNULL(RRWB.BucketList,0) AS BucketList,
				                    dbo.SupplyCategory(Supplies.SupplyId,{0}) AS sort
		                    FROM    Supplies 
				                      INNER JOIN Cities ON Cities.CityId=Supplies.CityId 
				                      INNER JOIN Countries ON Cities.CountryId = Countries.CountryId  
				                      LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId 
				                      LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND IBTType=3 
		                    WHERE Supplies.StatusId=1 AND Supplies.CityId={2} AND Supplies.Title LIKE '" + filterLetter + @"%' AND Supplies.Title  LIKE '%" + keyWord + @"%'
	                    ) AS t1 WHERE t1.sort=1 
                    )AS t2 	
                    WHERE RowId BETWEEN " + startSize.ToString() + " AND " + endSize.ToString() + "";
            /*20130305裕冲修改
            string sql = @"SELECT 	t2.* FROM (
	                    SELECT t1.*,ROW_NUMBER() OVER (ORDER BY t1.sort DESC) AS RowId FROM (
		                    SELECT  
				                    Supplies.SupplyId,Supplies.Title,dbo.Countries.FlagSrc AS CoverPhotoSrc,
				                    ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
				                    ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
				                    dbo.Countries.CountryName, Countries.CountryId,
				                    dbo.Cities.CityName,dbo.Cities.CityId,
				                    ISNULL(RRWB.Rating,0) AS Rating,
				                    ISNULL(RRWB.Recommended,0) AS Recommended,
				                    ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
				                    ISNULL(RRWB.BucketList,0) AS BucketList,
				                    dbo.SupplyCategory(Supplies.SupplyId,'{0}') AS sort
		                    FROM    Supplies 
				                      INNER JOIN Cities ON Cities.CityId=Supplies.CityId
				                      INNER JOIN Countries ON Cities.CountryId = Countries.CountryId 
				                      LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId AND IBT.UserId={1}
				                      LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND IBTType=3 AND RRWB.UserId={1}
		                    WHERE Supplies.StatusId=1 AND Supplies.CityId={2}
	                    ) AS t1
                    )AS t2 	
                    WHERE RowId BETWEEN " + startSize.ToString() + " AND " + endSize.ToString() + "";
             * */
            return _db.Database.SqlQuery<SupplyListShort>(sql, categoryName, userId, cityId).ToList();
        }

        /// <summary>
        /// 获取用户添加的国家，城市与venues的名称列表 (step4)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <param name="travelType"></param>
        /// <returns></returns>
        public IList<string> AddTravelsGetSelectItem(int userId, long epoch, Identifier.AddTravelsType travelType)
        {
            int typeId = (int)travelType;
            List<string> result = new List<string>();

            var list = (from c in _db.AddTravelsTemps where (c.UserId.Equals(userId) && c.Epoch.Equals(epoch) && c.TypeId.Equals(typeId)) select new { Ids = c.Ids }).ToList();
            string ids = "";
            foreach (var item in list)
            {
                if (!string.IsNullOrWhiteSpace(item.Ids))
                {
                    ids = ids + item.Ids + ",";
                }
            }
            ids = ids.Length > 0 ? ids.Substring(0, ids.Length - 1) : ids;
            if (ids.Length < 1)
            {
                return result;
            }

            List<int> list_ids = new List<int>();
            foreach (string str in ids.Split(','))
            {
                if (Utils.IsNumeric(str))
                {
                    list_ids.Add(Convert.ToInt32(str));
                }
            }

            switch (travelType)
            {
                case Identifier.AddTravelsType.Country:
                    var resCountry = (from c in _db.Countries where list_ids.Contains(c.CountryId) select new { CountryName = c.CountryName }).ToList();
                    foreach (var item in resCountry)
                    {
                        result.Add(item.CountryName);
                    }
                    break;
                case Identifier.AddTravelsType.City:
                    var resCity = (from c in _db.Cities where list_ids.Contains(c.CityId) select new { CityName = c.CityName }).ToList();
                    foreach (var item in resCity)
                    {
                        result.Add(item.CityName);
                    }
                    break;
                case Identifier.AddTravelsType.Venue:
                    var resVenue = (from c in _db.Supplies where list_ids.Contains(c.SupplyId) select new { Title = c.Title }).ToList();
                    foreach (var item in resVenue)
                    {
                        result.Add(item.Title);
                    }
                    break;
            }
            return result;
        }

        public List<Country> AddTravelsGetSelectCountry(int userId, long epoch)
        {
            List<Country> countryList = new List<Country>();

            var atpList = _db.AddTravelsTemps.Where(a => a.UserId == userId && a.Epoch == epoch && a.TypeId == (int)Identifier.AddTravelsType.Country).ToList();
            string ids = "";
            foreach (var item in atpList)
            {
                if (!string.IsNullOrWhiteSpace(item.Ids))
                {
                    ids = ids + item.Ids + ",";
                }
            }
            ids = ids.Length > 0 ? ids.Substring(0, ids.Length - 1) : ids;
            if (ids.Length < 1)
            {
                return countryList;
            }

            List<int> list_ids = new List<int>();
            foreach (string str in ids.Split(','))
            {
                if (Utils.IsNumeric(str))
                {
                    list_ids.Add(Convert.ToInt32(str));
                }
            }

            countryList = _db.Countries.Where(c => list_ids.Contains(c.CountryId) && c.StatusId == (int)Identifier.StatusType.Enabled).OrderBy(c => c.CountryName).ToList();

            return countryList;
        }

        public List<City> AddTravelsGetSelectCity(int userId, long epoch) 
        {
            List<City> cityList = new List<City>();

            var atpList = _db.AddTravelsTemps.Where(a => a.UserId == userId && a.Epoch == epoch && a.TypeId == (int)Identifier.AddTravelsType.City).ToList();
            string ids = "";
            foreach (var item in atpList)
            {
                if (!string.IsNullOrWhiteSpace(item.Ids))
                {
                    ids = ids + item.Ids + ",";
                }
            }
            ids = ids.Length > 0 ? ids.Substring(0, ids.Length - 1) : ids;
            if (ids.Length < 1)
            {
                return cityList;
            }

            List<int> list_ids = new List<int>();
            foreach (string str in ids.Split(','))
            {
                if (Utils.IsNumeric(str))
                {
                    list_ids.Add(Convert.ToInt32(str));
                }
            }

            cityList = _db.Cities.Where(c => list_ids.Contains(c.CityId) && c.StatusId == (int)Identifier.StatusType.Enabled).OrderBy(c => c.CityName).ToList();

            return cityList;
        }

        public List<Supply> AddTravelsGetSelectSupply(int userId, long epoch) 
        {
            List<Supply> supplyList = new List<Supply>();

            var atpList = _db.AddTravelsTemps.Where(a => a.UserId == userId && a.Epoch == epoch && a.TypeId == (int)Identifier.AddTravelsType.Venue).ToList();
            string ids = "";
            foreach (var item in atpList)
            {
                if (!string.IsNullOrWhiteSpace(item.Ids))
                {
                    ids = ids + item.Ids + ",";
                }
            }
            ids = ids.Length > 0 ? ids.Substring(0, ids.Length - 1) : ids;
            if (ids.Length < 1)
            {
                return supplyList;
            }

            List<int> list_ids = new List<int>();
            foreach (string str in ids.Split(','))
            {
                if (Utils.IsNumeric(str))
                {
                    list_ids.Add(Convert.ToInt32(str));
                }
            }

            supplyList = _db.Supplies.Where(s => list_ids.Contains(s.SupplyId) && s.StatusId == (int)Identifier.StatusType.Enabled).OrderBy(s => s.Title).ToList();

            return supplyList;
        }

        /// <summary>
        /// 获取Ids
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="epoch"></param>
        /// <param name="travelType"></param>
        /// <returns></returns>
        public IList<int> AddTravelsGetSelectIds(int userId, long epoch, Identifier.AddTravelsType travelType)
        {
            int typeId = (int)travelType;
            List<int> result = new List<int>();

            var list = (from c in _db.AddTravelsTemps where (c.UserId.Equals(userId) && c.Epoch.Equals(epoch) && c.TypeId.Equals(typeId)) select new { Ids = c.Ids }).ToList();
            string ids = "";
            foreach (var item in list)
            {
                if (!string.IsNullOrWhiteSpace(item.Ids))
                {
                    ids = ids + item.Ids + ",";
                }
            }
            ids = ids.Length > 0 ? ids.Substring(0, ids.Length - 1) : ids;
            if (ids.Length < 1)
            {
                return result;
            }

            foreach (string str in ids.Split(','))
            {
                if (Utils.IsNumeric(str))
                {
                    result.Add(Convert.ToInt32(str));
                }
            }

            return result;
        }
        #endregion

        #region Travels History

        public void SaveToHistory(List<City> cityList, int userId, long epoch)
        {
            if (cityList.Count > 0)
            {
                List<AddTravelsTemp> attList = new List<AddTravelsTemp>();

                cityList.ForEach(c =>
                {
                    AddTravelsTemp att = attList.FirstOrDefault(a => a.ParentId == c.CountryId);
                    if (att == null)
                    {
                        att = new AddTravelsTemp();
                        att.ParentId = c.CountryId;
                        att.Ids = c.CityId.ToString();
                        att.TypeId = 2;
                        att.UserId = userId;
                        att.Epoch = epoch;
                        att.CreateDate = DateTime.Now;

                        attList.Add(att);
                    }
                    else
                    {
                        att.Ids = att.Ids + "," + c.CityId;
                    }
                });

                string ids = "";
                attList.ForEach(a =>
                {
                    if (string.IsNullOrEmpty(ids))
                        ids = a.ParentId.ToString();
                    else
                        ids = ids + "," + a.ParentId.ToString();
                });

                attList.Add(new AddTravelsTemp()
                {
                    ParentId = 0,
                    Ids = ids,
                    UserId = userId,
                    Epoch = epoch,
                    TypeId = 1,
                    CreateDate = DateTime.Now
                });

                if (attList.Count > 0)
                {
                    attList.ForEach(a => {
                        _db.AddTravelsTemps.Add(a);
                    });
                    _db.SaveChanges();
                }
            }
        }

        public int HasPostVisitAllCount(int userId)
        {
            string strSql = @"SELECT Count(temp.PVId) FROM 
(SELECT PVId  FROM dbo.PostVists WHERE  CountryId>0 AND UserId={0}) AS temp";
            var list = _db.Database.SqlQuery<int>(strSql, userId).ToList();
            return list[0];
        }

        public int HasCountriesPostVisitCount(int userId, int countryId)
        {
            string strSql = @"SELECT Count(temp.CountryId) FROM 
(SELECT UserId, CountryId  FROM dbo.PostVists WHERE  CountryId>0 AND UserId={0}) AS temp WHERE temp.CountryId={1}";
            var list = _db.Database.SqlQuery<int>(strSql, userId, countryId).ToList();
            return list[0];
        }

        public int HasCountriesPostVisitOnlyCount(int userId, int countryId)
        {
            string strSql = @"SELECT Count(temp.CountryId) FROM 
(SELECT UserId, CountryId  FROM dbo.PostVists WHERE  CountryId>0 AND CityId=0 AND SupplyId=0 AND UserId={0}) AS temp WHERE temp.CountryId={1}";
            var list = _db.Database.SqlQuery<int>(strSql, userId, countryId).ToList();
            return list[0];
        }

        public int HasCitiesPostVisitCount(int userId, int cityId)
        {
            string strSql = @"SELECT Count(temp.CityId) FROM 
(SELECT UserId, CityId  FROM dbo.PostVists WHERE  CityId>0 AND UserId={0}) AS temp WHERE temp.CityId={1}";
            var list = _db.Database.SqlQuery<int>(strSql, userId, cityId).ToList();
            return list[0];
        }

        public int HasCitiesPostVisitOnlyCount(int userId, int cityId)
        {
            string strSql = @"SELECT Count(temp.CityId) FROM 
(SELECT UserId, CityId  FROM dbo.PostVists WHERE  CityId>0 AND SupplyId=0 AND UserId={0}) AS temp WHERE temp.CityId={1}";
            var list = _db.Database.SqlQuery<int>(strSql, userId, cityId).ToList();
            return list[0];
        }

        public int HasVenuesPostVisitCount(int userId, int supplyId)
        {
            string strSql = @"SELECT Count(temp.SupplyId) FROM 
(SELECT UserId, SupplyId  FROM dbo.PostVists WHERE  SupplyId>0 AND UserId={0}) AS temp WHERE temp.SupplyId={1}";
            var list = _db.Database.SqlQuery<int>(strSql, userId, supplyId).ToList();
            return list[0];
        }

        public IList<SupplierListItem> GetHistoryVenuesList(int userId, int countryId, int cityId, out int total)
        {
            string sql = @" SELECT dbo.PostVists.SupplyId, dbo.Supplies.Title, dbo.Cities.CityName, ISNULL(dbo.supply_type(dbo.PostVists.SupplyId),'') AS TypesOfSupplier
                            FROM dbo.PostVists
                            LEFT JOIN dbo.Supplies ON dbo.PostVists.SupplyId = dbo.Supplies.SupplyId   
                            LEFT JOIN dbo.Cities ON dbo.PostVists.CityId = dbo.Cities.CityId
                            LEFT JOIN dbo.Countries ON dbo.Countries.CountryId =dbo.Cities.CountryId                         
                            WHERE dbo.PostVists.UserId=" + userId.ToString() + @" AND dbo.PostVists.CountryId=" + countryId.ToString() + @" AND dbo.PostVists.CityId=" + cityId.ToString() + @" AND dbo.PostVists.IBTType=3 AND dbo.PostVists.StatusId=1 GROUP BY dbo.PostVists.SupplyId, dbo.Supplies.Title, dbo.Cities.CityName";
            var list = _db.Database.SqlQuery<SupplierListItem>(sql).ToList();
            total = list.Count;
            return list.ToList();
        }
        #endregion

        #region Supplier

        /// <summary>
        /// 根据supplyId获取下面的信息
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public Supply GetSupplyDetail(int supplyId)
        {
            var item = (from c in _db.Supplies where c.SupplyId.Equals(supplyId) select c).FirstOrDefault();
            if (item == null)
                return new Supply();
            else
                return item;
        }
        /// <summary>
        /// 根据countryId,cityId,cateId获取下面的信息
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="cateId"></param>
        /// <returns></returns>
        public IList<Supply> GetSupplyItemDetail(string strWhere)
        {
            string sql = @"SELECT  ROW_NUMBER() OVER (ORDER BY sp.Score DESC, sp.SupplyId ASC) AS RowId,
				sp.*,cn.CountryName,cy.CityName                          
                   FROM  dbo.Supplies sp
                            LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId 
                            LEFT JOIN SupplyAttributeData sa ON sp.SupplyId = sa.SupplyId
                            LEFT JOIN AttributeData ad ON sa.ArrtDataId = ad.AttrDataId 
                            WHERE 1=1 " + strWhere.ToString();
            IList<Supply> list = _db.Database.SqlQuery<Supply>(sql).ToList<Supply>();
            return (from c in list orderby c.Score descending select c).ToList<Supply>();
        }
        /// <summary>
        /// 根据城市ID返回下面所有的Supply
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public IList<Supply> GetSupplyList(int cityId)
        {
            byte statusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
            return (from c in _db.Supplies
                    where (c.CityId.Equals(cityId) && c.StatusId.Equals(statusId))
                    orderby c.Score descending
                    select c).ToList<Supply>();
        }

        /// <summary>
        /// Add Travels Step3的列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cityId"></param>
        /// <param name="sortBy"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public IList<SupplyListShort> GetSupplyList(int userId, int cityId, int sortBy, int pageSize, int pageNumber)
        {
            /*
             SELECT     dbo.Countries.CountryName, dbo.Cities.CityName, dbo.IBT.IbtCount
FROM         dbo.Supplies INNER JOIN
                      dbo.Cities INNER JOIN
                      dbo.Countries ON dbo.Cities.CountryId = dbo.Countries.CountryId INNER JOIN
                      dbo.IBT ON dbo.Cities.CityId = dbo.IBT.CityId ON dbo.Supplies.CityId = dbo.Cities.CityId AND dbo.Supplies.CountryId = dbo.Countries.CountryId AND 
                      dbo.Supplies.SupplyId = dbo.IBT.SupplyId INNER JOIN
                      dbo.RRWB ON dbo.Supplies.SupplyId = dbo.RRWB.ObjId
             */
            return new List<SupplyListShort>();
        }

        /// <summary>
        /// 根据supplyId获取简要信息
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public SupplyListShort GetSupplyShortInfo(int userId, int supplyId)
        {
            string sql = @"SELECT  
                                Supplies.SupplyId,Supplies.Title,dbo.Supplies.CoverPhotoSrc,
                                ISNULL(Supplies.CommentsCount,0) AS CommentsCount,
                                ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
                                dbo.Countries.CountryName, Countries.CountryId,
                                dbo.Cities.CityName,dbo.Cities.CityId,
                                ISNULL(RRWB.Rating,0) AS Rating,
                                ISNULL(RRWB.Recommended,0) AS Recommended,
                                ISNULL(RRWB.WanttoGo,0) AS WanttoGo,
                                ISNULL(RRWB.BucketList,0) AS BucketList
                        FROM      Supplies 
                                  INNER JOIN Cities ON Cities.CityId=Supplies.CityId
                                  INNER JOIN Countries ON Cities.CountryId = Countries.CountryId 
                                  LEFT JOIN IBT ON Supplies.SupplyId = IBT.SupplyId AND IBT.UserId={0}
                                  LEFT JOIN RRWB ON Supplies.SupplyId = RRWB.[ObjId] AND RRWB.UserId={0}
                        WHERE Supplies.StatusId=1 AND Supplies.SupplyId={1}";
            IEnumerable<SupplyListShort> list = _db.Database.SqlQuery<SupplyListShort>(sql, userId, supplyId);

            if (list != null)
            {
                return list.ToList<SupplyListShort>()[0];
            }
            else
                return new SupplyListShort();
        }

        /// <summary>
        /// GetStaffsContactList
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public IList<StaffsContact> GetStaffsContactList(int supplyId)
        {
            byte statusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
            return (from c in _db.StaffsContacts
                    where (c.SupplyId.Equals(supplyId) && c.StatusId.Equals(statusId))
                    orderby c.Sort descending
                    select c).ToList<StaffsContact>();
        }

        /// <summary>
        /// 获取supply的封面图片（单张）
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="coverPhotoSrc"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public string GetSupplyPhoto(int supplyId, string coverPhotoSrc, Identifier.SuppilyPhotoSize size)
        {
            //已经有了
            if (!string.IsNullOrWhiteSpace(coverPhotoSrc))
            {
                string absPath = coverPhotoSrc + size.ToString() + ".jpg";
                //path
                string path = System.Web.HttpContext.Current.Server.MapPath("~" + absPath);

                if (System.IO.File.Exists(path))
                {
                    return absPath;
                }
            }

            //没有,获取国家图片
            var country = (from s in _db.Supplies
                           join c in _db.Countries on s.CountryId equals c.CountryId
                           where s.SupplyId.Equals(supplyId)
                           select new { IconSrc = c.FlagSrc }).FirstOrDefault();
            if (country != null && !string.IsNullOrWhiteSpace(country.IconSrc))
            {
                return country.IconSrc;
            }

            //没有，获取默认分类的
            var icon = (from a in _db.AttributeDatas
                        join s in _db.SupplyAttributeDatas on a.AttrDataId equals s.ArrtDataId
                        where s.SupplyId.Equals(supplyId)
                        orderby s.Score ascending
                        select new { IconSrc = a.IconSrc }).FirstOrDefault();
            if (icon != null && !string.IsNullOrWhiteSpace(icon.IconSrc))
                return icon.IconSrc + size.ToString() + ".jpg";
            else
            {
                string defaultIcon = System.Configuration.ConfigurationManager.AppSettings["SupplyDefaultIcon"] ?? "";
                return defaultIcon + size.ToString() + ".jpg";
            }
        }

        /// <summary>
        /// 获取Interactive supply的封面图片（单张）
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="coverPhotoSrc"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public string GetInterSupplyPhoto(int supplyId, string coverPhotoSrc, Identifier.SuppilyPhotoSize size)
        {
            //已经有了
            if (!string.IsNullOrWhiteSpace(coverPhotoSrc))
            {
                string absPath = coverPhotoSrc + size.ToString() + ".jpg";
                //path
                string path = System.Web.HttpContext.Current.Server.MapPath("~" + absPath);

                if (System.IO.File.Exists(path))
                {
                    return absPath;
                }
            }

            //没有,获取国家图片
            var country = (from s in _db.Supplies
                           join c in _db.Countries on s.CountryId equals c.CountryId
                           where s.SupplyId.Equals(supplyId)
                           select new { IconSrc = c.FlagSrc }).FirstOrDefault();
            if (country != null && !string.IsNullOrWhiteSpace(country.IconSrc))
            {
                string flagSrc = country.IconSrc;
                if (flagSrc.IndexOf("PNG") > 0 && flagSrc.IndexOf("flag") > 0)
                {
                    flagSrc = flagSrc.Replace("PNG", "gif").Replace("flag", "flag_170");
                    return flagSrc.ToString();
                }
                else
                {
                    return country.IconSrc.ToString();
                }
            }

            //没有，获取默认分类的
            var icon = (from a in _db.AttributeDatas
                        join s in _db.SupplyAttributeDatas on a.AttrDataId equals s.ArrtDataId
                        where s.SupplyId.Equals(supplyId)
                        orderby s.Score ascending
                        select new { IconSrc = a.IconSrc }).FirstOrDefault();
            if (icon != null && !string.IsNullOrWhiteSpace(icon.IconSrc))
                return icon.IconSrc + size.ToString() + ".jpg";
            else
            {
                string defaultIcon = System.Configuration.ConfigurationManager.AppSettings["SupplyDefaultIcon"] ?? "";
                return defaultIcon + size.ToString() + ".jpg";
            }
        }
        /// <summary>
        /// 根据SupplyId返回Supplier所属的分类
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public AttributeData GetSupplyTypes(int supplyId)
        {
            var res = (from c in _db.AttributeDatas
                       join s in _db.SupplyAttributeDatas on c.AttrDataId equals s.ArrtDataId
                       where s.SupplyId.Equals(supplyId)
                       orderby s.Score ascending
                       select c).FirstOrDefault();

            return res == null ? new AttributeData() : res;
        }
        #endregion

        #region Home#Most Recommended Places/Most Visited Places/We've Been/Destination of the Week
        /// <summary>
        /// 首页的Most Recommended Places(修改-)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public IEnumerable<SupplyHomeListShort> GetMostRecommendedPlaces(int userId, int limit)
        {
            string sql = @"SELECT  TOP " + limit.ToString() + @"   dbo.WidgetMostRecommentdPlaces.ObjId, dbo.Cities.CountryId, dbo.Countries.CountryName,dbo.Supplies.CityId,dbo.Cities.CityName, 
				                        dbo.Supplies.Title, dbo.Supplies.InfoAddress,
                                        dbo.Supplies.CoverPhotoSrc,
                                        ISNULL(dbo.Supplies.CommentsCount,0) AS CommentsCount,
                                        ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
                                        ISNULL(dbo.Supplies.Rating,0) AS Rating, 
                                        ISNULL(dbo.Supplies.RecommendedCount, 0) AS RecommendedCount,
                                        ISNULL(dbo.Supplies.WanttoGoCount,0) AS WanttoGoCount, 
                                        ISNULL(dbo.Supplies.BucketListCount,0) AS BucketListCount
                        FROM         dbo.Cities INNER JOIN
                                              dbo.WidgetMostRecommentdPlaces INNER JOIN
                                              dbo.Supplies ON dbo.WidgetMostRecommentdPlaces.ObjId = dbo.Supplies.SupplyId INNER JOIN
                                              dbo.Countries ON dbo.Supplies.CountryId = dbo.Countries.CountryId ON dbo.Cities.CityId = dbo.Supplies.CityId 
                                              LEFT OUTER JOIN dbo.RRWB ON dbo.Supplies.SupplyId = dbo.RRWB.ObjId AND RRWB.IBTType=3 AND dbo.RRWB.UserId={0}
                                              LEFT OUTER JOIN dbo.IBT ON dbo.Supplies.SupplyId = dbo.IBT.SupplyId AND ibt.UserId={0}
                        WHERE WidgetMostRecommentdPlaces.StatusId=1
                        ORDER BY WidgetMostRecommentdPlaces.Sort ASC";

            return _db.Database.SqlQuery<SupplyHomeListShort>(sql, userId);
        }


        /// <summary>
        /// 个人首页的Most Visited Places
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<SupplyHomeListShort> GetMostVisitedPlaces(int userId, int limit)
        {
            //            string sql = @"SELECT  TOP " + limit.ToString() + @"   dbo.WidgetMostVisitedPlaces.ObjId AS SupplyId, dbo.Cities.CountryId, dbo.Countries.CountryName,dbo.Supplies.CityId,dbo.Cities.CityName, 
            //				                        dbo.Supplies.Title, dbo.Supplies.InfoAddress,
            //                                        dbo.Supplies.CoverPhotoSrc,
            //                                        ISNULL(dbo.Supplies.CommentsCount,0) AS CommentsCount,
            //                                        ISNULL(dbo.IBT.IbtCount, 0) AS IbtCount,
            //                                        ISNULL(dbo.Supplies.Rating,0) AS Rating, 
            //                                        ISNULL(dbo.Supplies.RecommendedCount, 0) AS RecommendedCount,
            //                                        ISNULL(dbo.Supplies.WanttoGoCount,0) AS WanttoGoCount, 
            //                                        ISNULL(dbo.Supplies.BucketListCount,0) AS BucketListCount
            //                        FROM         dbo.Cities INNER JOIN
            //                                              dbo.WidgetMostVisitedPlaces INNER JOIN
            //                                              dbo.Supplies ON dbo.WidgetMostVisitedPlaces.ObjId = dbo.Supplies.SupplyId INNER JOIN
            //                                              dbo.Countries ON dbo.Supplies.CountryId = dbo.Countries.CountryId ON dbo.Cities.CityId = dbo.Supplies.CityId 
            //                                              LEFT OUTER JOIN dbo.RRWB ON dbo.Supplies.SupplyId = dbo.RRWB.ObjId AND RRWB.IBTType=3 AND dbo.RRWB.UserId={0}
            //                                              LEFT OUTER JOIN dbo.IBT ON dbo.Supplies.SupplyId = dbo.IBT.SupplyId AND ibt.UserId={0}
            //                        WHERE WidgetMostVisitedPlaces.StatusId=1
            //                        ORDER BY WidgetMostVisitedPlaces.Sort ASC";
            string sql = @"
		SELECT * FROM (
		SELECT  ROW_NUMBER() OVER (ORDER BY Sort ASC, temp_rowid ASC) AS RowId,* 
		FROM(
		SELECT 0 AS temp_rowid,
		0 AS SupplyId,cn.CountryName AS Title,1 AS objType,'' AS Desp,
        cn.FlagSrc AS CoverPhotoSrc,
        cn.CountryId,cn.CountryName,
        0 AS CityId,'' AS CityName,
        ISNULL(ibt.IbtCount, 0) AS IbtCount,
        ISNULL(cn.Rating,0) AS Rating,
        wvp.Sort                           
                   FROM  dbo.WidgetMostVisitedPlaces wvp
                   LEFT JOIN dbo.Countries cn ON wvp.ObjId=cn.CountryId 
                   LEFT JOIN dbo.CountriesDes cd ON cn.CountryId = cd.CountryId 
                   LEFT OUTER JOIN dbo.IBT ibt ON wvp.ObjId = ibt.CountryId AND ibt.CityId=0 AND ibt.SupplyId=0 AND ibt.UserId={0}
                   WHERE wvp.StatusId=1 AND wvp.MVPType=1
        UNION
        SELECT 1 AS temp_rowid,
		0 AS SupplyId,cy.CityName AS Title,2 AS objType,'' AS Desp,
        cn.FlagSrc AS CoverPhotoSrc,
        cn.CountryId AS CountryId,cn.CountryName AS CountryName,
        cy.CityId,cy.CityName,
        ISNULL(ibt.IbtCount, 0) AS IbtCount,
        ISNULL(cy.Rating,0) AS Rating,
        wvp.Sort                          
                    FROM  dbo.WidgetMostVisitedPlaces wvp
                    LEFT JOIN  dbo.Cities cy ON wvp.ObjId=cy.CityId 
                    LEFT JOIN  dbo.Countries cn ON cy.CountryId = cn.CountryId  
                    LEFT JOIN dbo.CitiesDescription cd ON cy.CityId = cd.CityId
                    LEFT OUTER JOIN dbo.IBT ibt ON wvp.ObjId = ibt.CityId AND ibt.SupplyId=0 AND ibt.UserId={0}
                    WHERE wvp.StatusId=1 AND wvp.MVPType=2    
        UNION
        SELECT 2 AS temp_rowid,
		sp.SupplyId,sp.Title,3 AS objType,sp.Desp,
        sp.CoverPhotoSrc,sp.CountryId,cn.CountryName,
        sp.CityId,cy.CityName,
        ISNULL(ibt.IbtCount, 0) AS IbtCount,
        ISNULL(sp.Rating,0) AS Rating,
        wvp.Sort                          
                   FROM  dbo.WidgetMostVisitedPlaces wvp
                            LEFT JOIN dbo.Supplies sp ON wvp.ObjId=sp.SupplyId 
                            LEFT JOIN dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN dbo.Cities cy ON sp.CityId = cy.CityId 
                            LEFT OUTER JOIN dbo.IBT ibt ON wvp.ObjId = ibt.SupplyId AND ibt.UserId={0}
                            WHERE wvp.StatusId=1 AND wvp.MVPType=3
        )AS temp  
	) As t1
	WHERE RowId BETWEEN 1 AND 5";

            return _db.Database.SqlQuery<SupplyHomeListShort>(sql, userId);
        }

        #endregion

        #region MyList
        /// <summary>
        /// 这个暂时不用
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        public int GetMyListCount(int userId, string listType)
        {
            return 0;
        }

        /// <summary>
        /// My Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="listType">Recommended,WanttoGo,BucketList</param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        /// <remarks>这里暂时是一下获取出来数据分页的。在数据量不是很大的情况下没问题。如果上千的话可能还是需要修改sp来分页</remarks>
        public IList<MyListItem> GetMyList(int userId, string listType, int page, int pageSize, string filterLetter, string filterKeyword, string filterField, out int total)
        {
            IList<MyListItem> list = _db.Database.SqlQuery<MyListItem>("EXEC IBT_MyList {0},{1},{2},{3}", userId, listType, filterKeyword, filterField).ToList<MyListItem>();
            total = list.Count;

            int startRecord = (page - 1) * pageSize;
            if (!string.IsNullOrWhiteSpace(filterLetter))
            {
                return (from c in list where c.Title.StartsWith(filterLetter) orderby c.RRWBId descending select c).Skip(startRecord).Take(pageSize).ToList<MyListItem>();
            }
            else
            {
                return (from c in list orderby c.RRWBId descending select c).Skip(startRecord).Take(pageSize).ToList<MyListItem>();
            }

            //todo 下面还需要返回好友数
        }
        #endregion

        #region Profile Activties List &  My Travel
        /// <summary>
        /// Profile Activties: my （user ）Activties
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <remarks>
        /// 1. 从数据获取主要的数据
        /// 2. 获取RRWB的数据
        /// 3. 如果是PV的。看是否是否上传有图片
        /// </remarks>
        public IList<FeedList> FeedUserActivtieLists(int userId, int page, int pageSize)
        {
            //1:从数据获取主要的数据
            List<FeedList> list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserNewsFeeds {0},{1},{2}", userId, page, pageSize).ToList<FeedList>();

            //2:根据IBTType去获取RRWB
            list = this.GetFeedRRWB(list);

            //如果是pv，看是否有上传有图片
            list = this.GetFeedPostVisitPhotos(list);

            return list;
        }

        /// <summary>
        /// Profile Activties: All Activities
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <remarks>
        /// 1. 从数据获取主要的数据
        /// 2. 获取RRWB的数据
        /// 3. 如果是PV的。看是否是否上传有图片
        /// </remarks>
        public IList<FeedList> FeedAllActivitiesLists(int userId, int page, int pageSize)
        {
            //1:从数据获取主要的数据
            List<FeedList> list = _db.Database.SqlQuery<FeedList>("EXEC IBT_AllActivitiesFeeds {0},{1},{2}", userId, page, pageSize).ToList<FeedList>();

            //2:根据IBTType去获取RRWB
            list = this.GetFeedRRWB(list);

            //如果是pv，看是否有上传有图片
            list = this.GetFeedPostVisitPhotos(list);

            return list;
        }

        /// <summary>
        /// Profile Activties: Friend Activties
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <remarks>
        /// 1. 从数据获取主要的数据
        /// 2. 获取RRWB的数据
        /// 3. 如果是PV的。看是否是否上传有图片
        /// </remarks>
        public IList<FeedList> FeedFriendActivtieLists(int userId, int page, int pageSize)
        {
            //1:从数据获取主要的数据
            List<FeedList> list = _db.Database.SqlQuery<FeedList>("EXEC IBT_FriendNewsFeeds {0},{1},{2}", userId, page, pageSize).ToList<FeedList>();

            //2:根据IBTType去获取RRWB
            list = this.GetFeedRRWB(list);

            //如果是pv，看是否有上传有图片
            list = this.GetFeedPostVisitPhotos(list);

            return list;
        }

        /// <summary>
        /// userId's Travel List
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IList<FeedList> FeedTravelList(int userId, int categoryId, int page, int pageSize, string filterLetter, string sortByRating)
        {
            //1:从数据获取主要的数据
            List<FeedList> list = new List<FeedList>();
            if (categoryId == -1)                               //countries
            {
                list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserTravelCountriesFeeds {0},{1},{2}", userId, page, pageSize).ToList<FeedList>();
            }
            else if (categoryId == -2)                          //cities
            {
                list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserTravelCitiesFeeds {0},{1},{2}", userId, page, pageSize).ToList<FeedList>();
            }
            else
            {
                list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserTravelFeeds {0},{1},{2},{3}", userId, page, pageSize, categoryId).ToList<FeedList>();
            }
            //2:根据IBTType去获取RRWB
            list = this.GetFeedRRWB(list);

            //如果是pv，看是否有上传有图片
            list = this.GetFeedPostVisitPhotos(list);
            if (!string.IsNullOrWhiteSpace(filterLetter))
            {
                return (from c in list where c.ExTitle.ToUpper().StartsWith(filterLetter) orderby c.CreateDate descending select c).ToList<FeedList>();
            }
            else
            {
                if (sortByRating == "Yes")
                {
                    return list.OrderByDescending(a => a.Rating).ToList();
                }
                else
                {
                    return list;
                }
            }
        }
        public IList<FeedList> FeedDateTravelList(int userId, string year, string month, int categoryId, int page, int pageSize, string filterLetter, string sortByRating)
        {
            //1:从数据获取主要的数据
            List<FeedList> list = new List<FeedList>();
            if (categoryId == -1)                               //countries
            {
                list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserTravelCountriesDateFeeds {0},{1},{2},{3},{4}", userId, year, month, page, pageSize).ToList<FeedList>();
            }
            else if (categoryId == -2)                          //cities
            {
                list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserTravelCitiesDateFeeds {0},{1},{2},{3},{4}", userId, year, month, page, pageSize).ToList<FeedList>();
            }
            else
            {
                list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserTravelDateFeeds {0},{1},{2},{3},{4},{5}", userId, year, month, page, pageSize, categoryId).ToList<FeedList>();
            }
            //2:根据IBTType去获取RRWB
            list = this.GetFeedRRWB(list);

            //如果是pv，看是否有上传有图片
            list = this.GetFeedPostVisitPhotos(list);
            if (!string.IsNullOrWhiteSpace(filterLetter))
            {
                return (from c in list where c.ExTitle.ToUpper().StartsWith(filterLetter) orderby c.CreateDate descending select c).ToList<FeedList>();
            }
            else
            {
                if (sortByRating == "Yes")
                {
                    return list.OrderByDescending(a => a.Rating).ToList();
                }
                else
                {
                    return list;
                }
            }
        }

        public IList<FeedList> FeedActivtieListsBySupplyId(int supplyId, int page, int pageSize)
        {
            //1:从数据获取主要的数据
            List<FeedList> list = _db.Database.SqlQuery<FeedList>("EXEC IBT_AllActivitiesFeedsBySupplyId {0},{1},{2}", supplyId, page, pageSize).ToList<FeedList>();

            //2:根据IBTType去获取RRWB
            list = this.GetFeedRRWB(list);

            //如果是pv，看是否有上传有图片
            list = this.GetFeedPostVisitPhotos(list);

            return list;
        }

        /// <summary>
        /// 统计userId的好友去过objId的次数 （ 9 Friends has Been There）
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="countryId"></param>
        /// <param name="cityId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public int HasBeenThereFriendsCount(int userId, int countryId, int cityId, int supplyId)
        {
            string key = "hbtfc" + userId.ToString() + "_" + countryId.ToString() + "_" + cityId.ToString() + "_" + supplyId.ToString();
            return _cache.GetOrInsert<int>(key, 30, true, () =>
            {
                string strSql = @"SELECT  COUNT(0) As Total
                                FROM    dbo.Connections INNER JOIN
                                        dbo.IBT ON dbo.Connections.FriendId = dbo.IBT.UserId
                                WHERE IBT.CountryId={1} AND IBT.CityId={2} AND IBT.SupplyId={3} AND Connections.ConnectionId IN (SELECT MAX(dbo.Connections.ConnectionId) ConnectionId FROM dbo.Connections WHERE dbo.Connections.UserId={0} GROUP BY UserId,FriendId)";

                var list = _db.Database.SqlQuery<int>(strSql, userId, countryId, cityId, supplyId).ToList();
                return list[0];
            });
        }
        public DAL.IBT GetIBTUser(int userId, int supplyId, int countryId, int cityId)
        {
            var ibt = _db.IBTs.Where(c => c.UserId.Equals(userId) && c.SupplyId.Equals(supplyId) && c.CountryId.Equals(countryId) && c.CityId.Equals(cityId) && c.StatusId.Equals(1)).FirstOrDefault();
            if (ibt != null)
                return ibt;
            else
            {
                return new DAL.IBT();
            }
        }
        public IList<NewsFeed> getNewsFeedAll(int userId = 0, int countryId = 0, int cityId = 0, int supplyId = 0)
        {
            var nf = (from a in _db.NewsFeeds where a.UserId > 0 select a);
            if (userId != 0)
            {
                nf = nf.Where(a => a.UserId == userId);
            }
            if (countryId != 0)
            {
                nf = nf.Where(a => a.CountryId == countryId);
            }
            if (cityId != 0)
            {
                nf = nf.Where(a => a.CityId == cityId);
            }
            if (supplyId != 0)
            {
                nf = nf.Where(a => a.SupplyId == supplyId);
            }

            return nf.ToList();
        }

        public IList<NewsFeed> getNewsFeed(int page, int pageSize, int userId = 0, int countryId = 0, int cityId = 0, int supplyId = 0)
        {
            var nf = (from a in _db.NewsFeeds where a.UserId > 0 select a);
            if (userId != 0)
            {
                nf = nf.Where(a => a.UserId == userId);
            }
            if (countryId != 0)
            {
                nf = nf.Where(a => a.CountryId == countryId);
            }
            if (cityId != 0)
            {
                nf = nf.Where(a => a.CityId == cityId);
            }
            if (supplyId != 0)
            {
                nf = nf.Where(a => a.SupplyId == supplyId);
            }

            return nf.OrderBy(a => a.NewsFeedId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
        }
        /// <summary>
        /// 返回所有好友列表
        /// </summary>
        /// <returns></returns>
        public IList<ConnectionListItem> GetAllFriend(int userId, int countryId, int cityId, int supplyId)
        {
            string strSql = @"SELECT  dbo.Connections.FriendId AS UserId,dbo.Profiles.Firstname,dbo.Profiles.Lastname,
                                dbo.Profiles.UserName,dbo.Profiles.CountryId,dbo.Countries.CountryName,dbo.Profiles.CityId,dbo.Cities.CityName
                                FROM    dbo.Connections 
                                LEFT JOIN dbo.IBT ON dbo.Connections.FriendId = dbo.IBT.UserId
                                LEFT JOIN dbo.Profiles ON dbo.IBT.UserId = dbo.Profiles.UserId
                                LEFT JOIN dbo.Countries ON dbo.Profiles.CountryId = dbo.Countries.CountryId
                                LEFT JOIN dbo.Cities ON dbo.Profiles.CityId = dbo.Cities.CityId
                                WHERE Connections.UserId={0} AND IBT.CountryId={1} AND IBT.CityId={2} AND IBT.SupplyId={3} AND Connections.ConnectionId IN (SELECT MAX(dbo.Connections.ConnectionId) ConnectionId FROM dbo.Connections  WHERE dbo.Connections.UserId={0}  GROUP BY UserId,FriendId)";
            return _db.Database.SqlQuery<ConnectionListItem>(strSql, userId, countryId, cityId, supplyId).ToList<ConnectionListItem>();
        }

        /// <summary>
        /// 有可能是国家，城市，supply
        /// </summary>
        /// <param name="feedList"></param>
        /// <returns></returns>
        public List<FeedList> GetFeedRRWB(List<FeedList> feedList)
        {
            foreach (var item in feedList)
            {
                int ibtType = item.IBTType;
                int objId = 0;
                if (ibtType == (int)Identifier.IBTType.Country)
                {
                    objId = item.CountryId;
                }
                else if (ibtType == (int)Identifier.IBTType.City)
                {
                    objId = item.CityId;
                }
                else
                {
                    objId = item.SupplyId;
                }

                var res = (from c in _db.RRWBs
                           where (c.IBTType.Equals(ibtType) && c.ObjId.Equals(objId) && c.UserId.Equals(item.UserId))
                           select new { Rating = c.Rating, Recommended = c.Recommended, WanttoGo = c.WanttoGo, BucketList = c.BucketList }
                           ).FirstOrDefault();
                if (res != null)
                {
                    item.Rating = res.Rating;
                    item.Recommended = res.Recommended;
                    item.WanttoGo = res.WanttoGo;
                    item.BucketList = res.BucketList;
                }
            }

            return feedList;
        }

        /// <summary>
        /// 获取相片
        /// </summary>
        /// <param name="feedList"></param>
        /// <returns></returns>
        public List<FeedList> GetFeedPostVisitPhotos(List<FeedList> feedList)
        {
            foreach (var item in feedList)
            {
                //如果ActiveType==pv && objData != ""
                if (item.ActiveType == (int)Identifier.FeedActiveType.PostVisit && Utils.IsNumeric(item.ObjData))
                {
                    int pvid = Convert.ToInt32(item.ObjData);
                    byte statusId = Utils.IntToByte(1);
                    item.PostVistAttachmentList = (from c in _db.PostVistAttachments where (c.PvId.Equals(pvid) && c.StatusId.Equals(statusId)) select c).ToList();
                }
            }

            return feedList;
        }
        #endregion

        #region Supply Type Management
        /// <summary>
        /// 根据类型ID返回类型的资料
        /// </summary>
        /// <param name="attrId"></param>
        /// <returns></returns>
        public DAL.AttributeData GetAttrInfo(int attrId)
        {
            var attr = _db.AttributeDatas.Where(c => c.AttrDataId.Equals(attrId)).FirstOrDefault();
            if (attr != null)
                return attr;
            else
            {
                return new DAL.AttributeData();
            }
        }

        public BoolMessageItem<int> AddNewType(int attrDataId, int attrCateId, string attrDataName, string imgsrc, string mapsrc, string maptsrc)
        {
            var item = (from o in _db.AttributeDatas
                        where o.AttrDataName.Equals(attrDataName)
                        select o).FirstOrDefault(); ;
            if (item != null)
            {
                return new BoolMessageItem<int>(0, false, "Cannot repeat add!");
            }
            else
            {
                AttributeData attr = new AttributeData();
                attr.ParentId = attrCateId;
                attr.AttrDataName = attrDataName;
                attr.StatusId = 1;
                attr.IconSrc = imgsrc;
                attr.IconMap = mapsrc;
                attr.IconMap2 = maptsrc;
                _db.AttributeDatas.Add(attr);
                _db.SaveChanges();
                return new BoolMessageItem<int>(attr.AttrDataId, true, "Add Successful!");  //不用激活
            }
        }

        public BoolMessageItem<int> UpdateAttributeData(int attrDataId, int attrCateId, string attrDataName, string imgsrc, string mapsrc, string maptsrc)
        {

            AttributeData attr = _db.AttributeDatas.FirstOrDefault(x => x.AttrDataId.Equals(attrDataId));
            if (attr != null)
            {
                attr.ParentId = attrCateId;
                attr.AttrDataName = attrDataName;
                attr.IconSrc = imgsrc;
                attr.IconMap = mapsrc;
                attr.IconMap2 = maptsrc;
                _db.SaveChanges();
                return new BoolMessageItem<int>(attr.AttrDataId, true, "Edit Successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Edit failure!");
            }

        }

        public BoolMessageItem<int> SetTypeSort(List<int> SupplyAttr)
        {
            int attrId = 0;
            SupplyAttributeData supplyattr;
            for (int i = 0; i < SupplyAttr.Count(); i++)
            {
                attrId = SupplyAttr[i];
                supplyattr = _db.SupplyAttributeDatas.FirstOrDefault(x => x.SupplyAttr.Equals(attrId));
                supplyattr.Score = i + 1;
                _db.SaveChanges();

            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }

        public BoolMessageItem<int> EditType(int supplyId, List<int> ArrtDataId)
        {
            int attrId = 0;
            SupplyAttributeData supplyattr = new SupplyAttributeData();
            for (int i = 0; i < ArrtDataId.Count(); i++)
            {
                attrId = ArrtDataId[i];
                //先删除
                supplyattr = _db.SupplyAttributeDatas.FirstOrDefault(x => x.SupplyId.Equals(supplyId));
                if (supplyattr != null)
                {
                    _db.SupplyAttributeDatas.Remove(supplyattr);
                    _db.SaveChanges();
                }
            }
            SupplyAttributeData newattr = new SupplyAttributeData();
            for (int i = 0; i < ArrtDataId.Count(); i++)
            {
                attrId = ArrtDataId[i];
                //再添加
                newattr.SupplyId = supplyId;
                newattr.ArrtDataId = attrId;
                newattr.Score = i + 1;
                _db.SupplyAttributeDatas.Add(newattr);
                _db.SaveChanges();
            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }
        /// <summary>
        /// 返回SupplyType Sort列表（SupplyId,AttrDataName,Score）
        /// </summary>
        /// <returns></returns>
        public IList<SupplyTypeInfo> GetAllSupplyTypeSort(int supplyId)
        {

            string sql = @"SELECT  ROW_NUMBER() OVER (ORDER BY SupplyAttributeData.Score ASC) AS RowId,
                                Supplies.SupplyId,Supplies.Title,SupplyAttributeData.SupplyAttr,SupplyAttributeData.ArrtDataId,                          
                                AttributeData.ParentId, AttributeData.AttrDataName,
                                AttributeData.StatusId,AttributeData.IconSrc, ISNULL(SupplyAttributeData.Score,0) AS Score
                        FROM      SupplyAttributeData 
                                  LEFT JOIN Supplies ON SupplyAttributeData.SupplyId = Supplies.SupplyId 
                                  LEFT JOIN AttributeData ON SupplyAttributeData.ArrtDataId = AttributeData.AttrDataId
                        WHERE AttributeData.StatusId=1 AND Supplies.SupplyId={0} ORDER BY SupplyAttributeData.Score ASC";
            return _db.Database.SqlQuery<SupplyTypeInfo>(sql, supplyId).ToList<SupplyTypeInfo>();
        }

        public AttributeData GetAttributeData(int supplyId)
        {
            //var user = _db.Users.Where(c => c.UserId.Equals(userId)).FirstOrDefault();
            //if (user != null)
            //    return user;
            //else
            //{
            //    return new DAL.User();
            //}
            var q = from n in _db.SupplyAttributeDatas
                    join b in _db.Supplies on n.SupplyId equals b.SupplyId
                    where n.SupplyId.Equals(supplyId)
                    orderby n.Score ascending
                    select new
                    {
                        n.ArrtDataId
                    };
            var attrData = _db.AttributeDatas.Where(c => c.AttrDataId.Equals(q.FirstOrDefault().ArrtDataId)).FirstOrDefault();
            if (attrData != null)
                return attrData;
            else
            {
                return new DAL.AttributeData();
            }
        }
        /// <summary>
        /// GetAttributeDataList
        /// </summary>
        /// <param name="ParentId"></param>
        /// <returns></returns>
        public IList<AttributeData> GetAttributeDataList(int ParentId)
        {
            if (ParentId == -1)
            {
                return (from c in _db.AttributeDatas orderby c.AttrDataId ascending select c).ToList<AttributeData>();
            }
            else
            {
                byte statusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
                return (from c in _db.AttributeDatas
                        where (c.ParentId.Equals(ParentId) && c.StatusId.Equals(statusId))
                        orderby c.AttrDataId ascending
                        select c).ToList<AttributeData>();
            }
        }
        /// <summary>
        /// GetAttributeDataImg
        /// </summary>
        /// <param name="AttrDataId"></param>
        /// <returns></returns>
        public IList<AttributeData> GetAttributeDataImg(int AttrDataId)
        {

            byte statusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
            return (from c in _db.AttributeDatas
                    where (c.AttrDataId.Equals(AttrDataId) && c.StatusId.Equals(statusId))
                    orderby c.AttrDataId ascending
                    select c).ToList<AttributeData>();

        }
        #endregion

        #region Supplier List &  Management
        public int GetSupplierListCount(string strWhere)
        {
            return 0;
        }



        /// <summary>
        /// Supplier Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<SupplierListItem> GetSupplierList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string filterKeyword, string filterField, out int total, string Where = "")
        {

            //--IBT_IMeetGetAllSupplierList 0,'ASC','',1,1,10,0,1,-1
            IList<SupplierListItem> list = _db.Database.SqlQuery<SupplierListItem>("EXEC IBT_IMeetGetAllSupplierList {0},{1},{2},{3},{4},{5},{6}", OrderBy, DescOrAsc, filterKeyword, userId, page, pageSize, Where).ToList<SupplierListItem>();
            total = list.Count();

            if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            {
                //todo:GetMyList filterField
                return new List<SupplierListItem>();
            }
            else
            {
                return (from c in list select c).ToList<SupplierListItem>();
            }

            //todo 下面还需要返回成员数
        }

        public BoolMessageItem<int> SetSortby(int supplyId, int newText)
        {
            Supply supply = _db.Supplies.FirstOrDefault(x => x.SupplyId.Equals(supplyId));
            if (supply != null)
            {
                supply.Score = newText;
                _db.SaveChanges();
                return new BoolMessageItem<int>(supply.SupplyId, true, "Search Score has been successfully updated!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Search Score is not successfully updated. Please try again!");
            }
        }


        public BoolMessageItem<int> SetSupplyIsDel(int supplyId)
        {
            Supply supply = _db.Supplies.FirstOrDefault(x => x.SupplyId.Equals(supplyId));
            if (supply != null && supply.StatusId != 3)
            {
                supply.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(supply.SupplyId, true, "Deleted successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Delete failure!");
            }
        }

        /// <summary>
        /// 返回所有Supplier列表
        /// </summary>
        /// <returns></returns>
        public IList<DestinationWeekItem> GetAllSupplier(string title)
        {
            //var query = from sp in _db.Supplies
            //            join cn in _db.Countries on sp.CountryId equals cn.CountryId
            //            join cy in _db.Cities on sp.CityId equals cy.CityId
            //            where sp.Title.Contains(title) || cn.CountryName.Contains(title) || cy.CityName.Contains(title)
            //            select new { title = sp.Title, objId = sp.SupplyId };
            string strSql = @"SELECT * FROM (
		SELECT  ROW_NUMBER() OVER (ORDER BY Title ASC, temp_rowid ASC) AS row_number,* 
		FROM(
		SELECT 0 AS temp_rowid,
		       cn.CountryId AS ObjId,
		       cn.CountryName AS Title,
               ISNULL(cn.FlagSrc, '') AS ImgSrc,
               ISNULL(SUBSTRING(cd.CountryTxt,1,datalength(cd.CountryTxt)), '') AS Desp,
		       1 AS DWType                 
                   FROM  dbo.Countries cn
                   LEFT JOIN dbo.CountriesDes cd ON cn.CountryId = cd.CountryId  
                   WHERE cn.CountryName LIKE '%" + title + @"%'
        UNION
        SELECT 1 AS temp_rowid,
               cy.CityId AS ObjId,
               cy.CityName AS Title,
               ISNULL(cn.FlagSrc, '') AS ImgSrc,
               ISNULL(SUBSTRING(cd.Txt,1,datalength(cd.Txt)), '') AS Desp,
               2 AS DWType              
                    FROM  dbo.Cities cy 
                    LEFT JOIN  dbo.Countries cn ON cy.CountryId = cn.CountryId  
                    LEFT JOIN dbo.CitiesDescription cd ON cy.CityId = cd.CityId
                    WHERE cy.CityName LIKE '%" + title + @"%'   
        UNION
        SELECT 2 AS temp_rowid,
               sp.SupplyId AS ObjId,
               sp.Title,
               '' AS ImgSrc,
               sp.Desp,
               3 AS DWType               
                   FROM  dbo.Supplies sp  
                            LEFT JOIN dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN dbo.Cities cy ON sp.CityId = cy.CityId 
                            WHERE sp.Title LIKE '%" + title + @"%'
        )AS temp  
	) As t1";
            return _db.Database.SqlQuery<DestinationWeekItem>(strSql).ToList<DestinationWeekItem>();
            //return (from c in _db.Supplies where c.Title.Contains(title) select c).ToList<Supply>();

        }
        public Dictionary<string, string> GetSupplierItem(string title)
        {
            return (from c in _db.Supplies where c.Title.Contains(title) select new { SupplyId = c.SupplyId, Title = c.Title }).ToDictionary(kvp => kvp.SupplyId.ToString(), kvp => kvp.Title);
        }

        public BoolMessageItem<int> UpdateSupplier(Supply supply, List<SupplyAttributeData> datalist)
        {
            try
            {
                int fun = 0;
                var s = (from a in _db.Supplies where a.SupplyId.Equals(supply.SupplyId) select a).FirstOrDefault();
                if (s == null)
                {
                    supply.CreateDate = supply.UpdateDate = DateTime.Now;
                    _db.Supplies.Add(supply);
                    _db.SaveChanges();
                    fun = 0;
                }
                else
                {
                    supply.UpdateDate = DateTime.Now;
                    _db.SaveChanges();
                    fun = 1;
                }
                /*删除*/
                string dataids = string.Empty;
                try
                {
                    dataids = string.Join(",", (from a in datalist select a.ArrtDataId).ToArray());
                }
                catch { }
                if (!string.IsNullOrEmpty(dataids))
                {
                    string sqlCmd = "delete SupplyAttributeData where SupplyId=" + supply.SupplyId;
                    _db.Database.ExecuteSqlCommand(sqlCmd);
                }
                /*添加*/
                foreach (var item in datalist)
                {
                    SupplyAttributeData sad = new SupplyAttributeData();
                    sad.SupplyId = supply.SupplyId;
                    sad.Score = supply.Score ?? 0;
                    sad.ArrtDataId = item.ArrtDataId;
                    _db.SupplyAttributeDatas.Add(sad);
                    _db.SaveChanges();
                }

                return new BoolMessageItem<int>(supply.SupplyId, true, "You have successfully saved the information for this supplier!");
            }
            catch (Exception ex)
            {
                return new BoolMessageItem<int>(0, false, ex.Message);
            }
        }
        /// <summary>
        /// 添加supplies的附件
        /// </summary>
        /// <param name="SuppliesAttachment"></param>
        /// <returns></returns>
        public BoolMessageItem<string> AddSupplyAttachment(SuppliesAttachment SuppliesAttachment)
        {
            int fun = 0;
            var item = (from a in _db.SuppliesAttachments where a.Guid == SuppliesAttachment.Guid select a).FirstOrDefault();
            if (item == null)
            {
                _db.SuppliesAttachments.Add(SuppliesAttachment);
                _db.SaveChanges();
                fun = 0;
            }
            else
            {
                _db.SaveChanges();
                fun = 1;
            }

            return new BoolMessageItem<string>(SuppliesAttachment.Guid, true, fun == 0 ? "Add Successful!" : "Update Successful!");
        }

        /// <summary>
        /// 添加supplies与supplies附件的绑定
        /// </summary>
        /// <param name="sab"></param>
        /// <returns></returns>
        public BoolMessageItem<int> AddSupplyAttachmentBind(SuppliesAttachmentBind sab)
        {
            int fun = 0;
            var item = (from a in _db.SuppliesAttachmentBinds where a.AttGuidId == sab.AttGuidId select a).FirstOrDefault();
            if (item == null)
            {
                _db.SuppliesAttachmentBinds.Add(sab);
                _db.SaveChanges();
                fun = 0;
            }
            else
            {
                _db.SaveChanges();
                fun = 1;
            }

            return new BoolMessageItem<int>(sab.BindId, true, fun == 0 ? "Add Successful!" : "Update Successful!");
        }

        /// <summary>
        /// 根据guid集合来获取附件
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public IList<SuppliesAttachment> GetSuppliesAttachment(string[] guid)
        {
            var list = (from a in _db.SuppliesAttachments where guid.Contains(a.Guid) select a).ToList();
            return list;
        }

        /// <summary>
        /// 根据supplyid来获取附件关系表数据
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public IList<SuppliesAttachmentBind> GetSuppliesAttachmentBind(int supplyId)
        {
            var list = (from a in _db.SuppliesAttachmentBinds where a.SupplyId == supplyId select a).ToList();
            return list;
        }
        /// <summary>
        /// 根据guid来删除附件绑定数据
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public BoolMessageItem<int> DeleteSupplyAttachmentBind(int supplyid)
        {

            string sqlCmd = "delete SuppliesAttachmentBind where SupplyId=" + supplyid.ToString() + " ";
            int i = _db.Database.ExecuteSqlCommand(sqlCmd);
            return new BoolMessageItem<int>(i, true, "Delete Successful!");
        }


        public IList<SupplyAttributeData> GetSupplyAttributeData(int supplyid)
        {
            string strSql = @"SELECT [SupplyAttr],[SupplyId],[ArrtDataId],[Score] FROM [dbo].[SupplyAttributeData] WHERE [SupplyId]=" + supplyid + " ";
            IList<SupplyAttributeData> list = _db.Database.SqlQuery<SupplyAttributeData>(strSql).ToList<SupplyAttributeData>();
            return list;
        }

        #endregion

        #region Feedbacks
        /// <summary>
        /// Supply的评论
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public BoolMessageItem SupplyFeedbacks(int supplyId, int userId, string comments)
        {
            try
            {
                Feedback feedback = new Feedback();
                feedback.SupplyId = supplyId;
                feedback.UserId = userId;
                feedback.Comments = comments;
                feedback.CreateDate = DateTime.Now;
                feedback.StatusId = Utils.IntToByte(1);

                _db.Feedbacks.Add(feedback);
                _db.SaveChanges();

                //需要添加记录feed  —发表评论
                SupplierService supplierService = new SupplierService();
                var supplyPv = supplierService.GetSupplyDetail(supplyId);
                int countryId = supplyPv.CountryId;
                int cityId = supplyPv.CityId;
                string supplierName = supplyPv.Title;
                supplyId = supplyPv.SupplyId;
                string firstName = _profileService.GetUserProfile(userId).Firstname;
                string experiences = firstName + " has written a post to" + supplierName + ".";
                BoolMessageItem<int> newsfeedid = _newsFeedService.AddNewsFeed(userId, countryId, cityId, supplyId, Identifier.IBTType.Supply, Identifier.FeedActiveType.PostComments, comments, feedback.FeedbackId.ToString(), DateTime.Now);

                return newsfeedid;
            }
            catch (Exception ex)
            {
                _logger.Error("SupplyFeedbacks,supplyId:" + supplyId.ToString(), ex);
                return new BoolMessageItem(null, false, "");
            }
        }

        /// <summary>
        /// 评论的列表
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public IList<SupplyFeedbackItem> FeedbackLists(int supplyId, int page, int pageSize)
        {
            return _db.Database.SqlQuery<SupplyFeedbackItem>("EXEC IBT_SupplyFeedbackLists {0},{1},{2}", supplyId, page, pageSize).ToList();
        }
        #endregion

        #region Landing Management

        #region Edit
        public BoolMessageItem<int> SetIndexSupplySort(List<int> IndexIds)
        {
            int indexId = 0;
            IndexTop10Supplies indexsupply;
            for (int i = 0; i < IndexIds.Count(); i++)
            {
                indexId = IndexIds[i];
                indexsupply = _db.IndexTop10Supplies.FirstOrDefault(x => x.ITSId.Equals(indexId));
                indexsupply.Sort = i + 1;
                _db.SaveChanges();

            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }
        public BoolMessageItem<int> EditIndexSupply(int indexId, string imgsrc, string txtDesc)
        {
            IndexTop10Supplies indexsupply = _db.IndexTop10Supplies.FirstOrDefault(x => x.ITSId.Equals(indexId));
            if (indexsupply != null && indexsupply.StatusId == 1)
            {
                indexsupply.ImgSrc = imgsrc;
                indexsupply.Desp = txtDesc;
                _db.SaveChanges();
                return new BoolMessageItem<int>(indexsupply.ITSId, true, "Edit Successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Edit failure!");
            }

        }
        public BoolMessageItem<int> SetIndexSupplyIsDel(int indexId)
        {
            IndexTop10Supplies indexItem = _db.IndexTop10Supplies.FirstOrDefault(x => x.ITSId.Equals(indexId));
            if (indexItem != null && indexItem.StatusId != 3)
            {
                indexItem.StatusId = 3;
                _db.SaveChanges();
                return new BoolMessageItem<int>(indexItem.ITSId, true, "Deleted successful!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Delete failure!");
            }
        }

        public BoolMessageItem<int> AddIndexSupply(int objId, int objType, int scroe)
        {
            //var item = (from o in _db.IndexTop10Supplies
            //            where (o.ObjId.Equals(objId) && o.ITType.Equals(objType))
            //            select o).FirstOrDefault();
            IndexTop10Supplies indexItem = _db.IndexTop10Supplies.FirstOrDefault(x => x.ObjId.Equals(objId) && x.ITType.Equals(objType));
            if (indexItem != null && indexItem.StatusId != 1)
            {
                indexItem.StatusId = 1;
                indexItem.Sort = 0;
                _db.SaveChanges();
                return new BoolMessageItem<int>(indexItem.ITSId, true, "Add successful!");
            }
            else
            {
                string itemName = "";
                string strDesp="";
                if (objType == (int)Identifier.IBTType.Supply)
                {
                    itemName = GetSupplyDetail(objId).Title;
                }
                else if (objType == (int)Identifier.IBTType.Country)
                {
                    itemName = GetCountryInfo(objId).CountryName;
                    //strDesp = GetCountryDespInfo(objId).CountryTxt.ToString();
                }
                else
                {
                    itemName = GetCityInfo(objId).CityName;
                    //strDesp = GetCityDespInfo(objId).Txt.ToString();
                }
                IndexTop10Supplies indexSupply = new IndexTop10Supplies();
                indexSupply.ObjId = objId;
                indexSupply.ITType = objType;
                indexSupply.Title = "Top " + scroe.ToString() + " Hotels in ADELAIDE, AUS";
                indexSupply.SubTitle = itemName;
                //indexSupply.Desp = strDesp;
                indexSupply.Sort = 0;
                indexSupply.CreateDate = DateTime.Now;
                indexSupply.StatusId = 1;
                _db.IndexTop10Supplies.Add(indexSupply);
                _db.SaveChanges();
                return new BoolMessageItem<int>(indexSupply.ITSId, true, "Add success!");
            }

        }
        /// <summary>
        /// 根据ID获取信息
        /// </summary>
        /// <param name="indexId"></param>
        /// <returns></returns>
        public IndexTop10Supplies GetIndexSupplyInfo(int indexId)
        {
            return (from c in _db.IndexTop10Supplies where c.ITSId.Equals(indexId) select c).FirstOrDefault();
        }
        #endregion

        #region HomeTopList
        public IList<SupplierListItem> GetHomeTopList(string strWhere, int page, int pageSize, out int total)
        {
            //            string strSql = @"SELECT   ROW_NUMBER() OVER (ORDER BY dbo.IndexTop10Supplies.Sort ASC, dbo.IndexTop10Supplies.ObjId ASC) AS row_number,
            //                dbo.IndexTop10Supplies.ITSId AS Id,dbo.IndexTop10Supplies.Title AS indexTitle,dbo.IndexTop10Supplies.ImgSrc AS icon, dbo.IndexTop10Supplies.ObjId AS SupplyId, dbo.Supplies.Title,dbo.IndexTop10Supplies.Sort AS Score,
            //                dbo.Supplies.CoordsLatitude, dbo.Supplies.CoordsLongitude,dbo.supply_type(dbo.Supplies.SupplyId) AS TypesOfSupplier,3 AS typ,
            //                dbo.Supplies.CoverPhotoSrc,dbo.Supplies.InfoAddress,dbo.Supplies.InfoZip,dbo.Supplies.InfoPhone,
            //                dbo.Supplies.InfoFax,dbo.Supplies.InfoState,dbo.Supplies.InfoWebsite,dbo.IndexTop10Supplies.CreateDate,dbo.Supplies.CountryId,dbo.Countries.CountryName,dbo.Supplies.CityId,dbo.Cities.CityName
            //                                FROM    dbo.IndexTop10Supplies 
            //                                LEFT JOIN dbo.Supplies ON dbo.IndexTop10Supplies.ObjId = dbo.Supplies.SupplyId
            //                                LEFT JOIN  dbo.Countries ON dbo.Supplies.CountryId = dbo.Countries.CountryId 
            //                                LEFT JOIN  dbo.Cities ON dbo.Supplies.CityId = dbo.Cities.CityId 
            //                                WHERE 1=1 " + strWhere.ToString() + @" AND dbo.IndexTop10Supplies.StatusId=1";
            string strSql = @"SELECT * FROM (
		SELECT  ROW_NUMBER() OVER (ORDER BY Score ASC, temp_rowid ASC) AS row_number,* 
		FROM(
		SELECT 0 AS temp_rowid,it.ITSId AS Id,it.Title AS indexTitle,it.ObjId,it.ITType,
		0 AS SupplyId,cn.CountryName AS Title,'' AS InfoState,
        it.ImgSrc AS icon,
        cn.CountryId,cn.CountryName,
        0 AS CityId,'' AS CityName,
        cn.FlagSrc AS CoverPhotoSrc,it.CreateDate,
        it.Sort AS Score                          
                   FROM  dbo.IndexTop10Supplies it
                   LEFT JOIN dbo.Countries cn ON it.ObjId=cn.CountryId 
                   LEFT JOIN dbo.CountriesDes cd ON cn.CountryId = cd.CountryId 
                   WHERE it.StatusId=1 AND it.ITType=1
        UNION
        SELECT 1 AS temp_rowid,it.ITSId AS Id,it.Title AS indexTitle,it.ObjId,it.ITType,
		0 AS SupplyId,cy.CityName AS Title,'' AS InfoState,
        it.ImgSrc AS icon,
        cn.CountryId AS CountryId,cn.CountryName AS CountryName,
        cy.CityId,cy.CityName,
        cn.FlagSrc AS CoverPhotoSrc,it.CreateDate,
        it.Sort AS Score                         
                    FROM  dbo.IndexTop10Supplies it
                    LEFT JOIN  dbo.Cities cy ON it.ObjId=cy.CityId 
                    LEFT JOIN  dbo.Countries cn ON cy.CountryId = cn.CountryId  
                    LEFT JOIN dbo.CitiesDescription cd ON cy.CityId = cd.CityId
                    WHERE it.StatusId=1 AND it.ITType=2    
        UNION
        SELECT 2 AS temp_rowid,it.ITSId AS Id,it.Title AS indexTitle,it.ObjId,it.ITType,
		sp.SupplyId,sp.Title,sp.InfoState,
        it.ImgSrc AS icon,sp.CountryId,cn.CountryName,
        sp.CityId,ISNULL(cy.CityName, '') AS CityName,
        sp.CoverPhotoSrc,it.CreateDate,
        it.Sort AS Score                          
                   FROM  dbo.IndexTop10Supplies it
                            LEFT JOIN dbo.Supplies sp ON it.ObjId=sp.SupplyId 
                            LEFT JOIN dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN dbo.Cities cy ON sp.CityId = cy.CityId 
                            WHERE it.StatusId=1 AND it.ITType=3
        )AS temp  
	) As t1";
            IList<SupplierListItem> list = _db.Database.SqlQuery<SupplierListItem>(strSql).ToList<SupplierListItem>();
            total = list.Count();
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.Score ascending select c).Skip(startRecord).Take(pageSize).ToList<SupplierListItem>();
        }

        public IList<IndexTop10Supplies> GetHomeTopListCheck()
        {
            var list = (from a in _db.IndexTop10Supplies where a.StatusId == 1 select a);
            return list.ToList();
        }

        #endregion

        #endregion

        #region IBT(ibtcount)
        public DAL.IBT GetIBTInfo(int userId, int supplyId, int countryId, int cityId)
        {
            cityId = cityId < 0 ? 0 : cityId;
            countryId = countryId < 0 ? 0 : countryId;
            supplyId = supplyId < 0 ? 0 : supplyId;
            if (userId == 0)
            {
                var ibt = _db.IBTs.Where(c => c.SupplyId.Equals(supplyId) && c.CountryId.Equals(countryId) && c.CityId.Equals(cityId) && c.StatusId.Equals(1)).FirstOrDefault();

                if (ibt != null)
                {
                    return ibt;
                }
                else
                {
                    return new DAL.IBT();
                }
            }
            else
            {
                var ibt = _db.IBTs.Where(c => c.UserId.Equals(userId) && c.SupplyId.Equals(supplyId) && c.CountryId.Equals(countryId) && c.CityId.Equals(cityId) && c.StatusId.Equals(1)).FirstOrDefault();
                if (ibt != null)
                {
                    return ibt;
                }
                else
                {
                    return new DAL.IBT();
                }
            }

        }
        #endregion

        #region Widget Country/City

        #region 新增国家 Widget Country
        public BoolMessageItem<int> AddWidgetCountry(int userId, int countryId, int scroe)
        {
            var item = (from c in _db.WidgetIBTs where (c.ObjId.Equals(countryId) && c.IBTType.Equals(1)) select c).FirstOrDefault();
            //1.查找是否已经有了。如果有了就是更新
            if (item != null)
            {
                item.UserId = userId;
                item.Sort = scroe;
                item.StatusId = 1;
                item.UpdateDate = DateTime.Now;
                _db.SaveChanges();
                return new BoolMessageItem<int>(item.ibtId, true, "Add success!");
            }
            else
            {
                string countryName = GetCountryInfo(countryId).CountryName;
                DAL.WidgetIBT ibt = new WidgetIBT();
                ibt.ObjId = countryId;
                ibt.ObjName = countryName;
                ibt.IBTType = 1;
                ibt.Sort = scroe;
                ibt.UserId = userId;
                ibt.CreateDate = DateTime.Now;
                ibt.UpdateDate = DateTime.Now;
                ibt.StatusId = 1;
                _db.WidgetIBTs.Add(ibt);
                _db.SaveChanges();
                return new BoolMessageItem<int>(ibt.ibtId, true, "Add success!");
            }

        }
        #endregion

        #region 新增城市 Widget City
        public BoolMessageItem<int> AddWidgetCity(int userId, int cityId, int scroe)
        {
            var item = (from c in _db.WidgetIBTs where (c.ObjId.Equals(cityId) && c.IBTType.Equals(2)) select c).FirstOrDefault();
            //1.查找是否已经有了。如果有了就是更新
            if (item != null)
            {
                item.UserId = userId;
                item.Sort = scroe;
                item.StatusId = 1;
                item.UpdateDate = DateTime.Now;
                _db.SaveChanges();
                return new BoolMessageItem<int>(item.ibtId, true, "Add success!");
            }
            else
            {
                string cityName = GetCityInfo(cityId).CityName;
                DAL.WidgetIBT ibt = new WidgetIBT();
                ibt.ObjId = cityId;
                ibt.ObjName = cityName;
                ibt.IBTType = 2;
                ibt.Sort = scroe;
                ibt.UserId = userId;
                ibt.CreateDate = DateTime.Now;
                ibt.UpdateDate = DateTime.Now;
                ibt.StatusId = 1;
                _db.WidgetIBTs.Add(ibt);
                _db.SaveChanges();
                return new BoolMessageItem<int>(ibt.ibtId, true, "Add success!");
            }

        }

        #endregion

        #region 返回所有添加的国家
        /// <summary>
        /// 返回所有添加的国家
        /// </summary>
        /// <returns></returns>
        public IList<WidgetIBT> GetAllWidgetIBTCountry(string strWhere, int page, int pageSize, out int total)
        {
            string strSql = @"select * from dbo.WidgetIBT  where 1=1 and dbo.WidgetIBT.IBTType=1 and dbo.WidgetIBT.StatusId=1 " + strWhere + "    ORDER BY sort ASC";
            IList<WidgetIBT> list = _db.Database.SqlQuery<WidgetIBT>(strSql).ToList<WidgetIBT>();
            total = list.Count();
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.Sort ascending select c).Skip(startRecord).Take(pageSize).ToList<WidgetIBT>();

        }

        public IList<Country> GetAllSearchCountry(string keyWord, int page, int pageSize, out int total)
        {
            /* 20130219裕冲修改 */
            List<Country> list = new List<Country>();
            list = _db.Database.SqlQuery<Country>("EXEC IBT_GetAllSearchCountry {0},{1},{2}", keyWord, page, pageSize).ToList<Country>();
            total = list.Count;
            return list;
            /*
            //string strSql = "select * from dbo.Countries  where dbo.Countries.CountryName LIKE '%" + keyWord + @"%' ORDER BY sort DESC";
            //IList<Country> list = _db.Database.SqlQuery<Country>(strSql).ToList<Country>();
            //total = list.Count();
            //int startRecord = (page - 1) * pageSize;
            //return (from c in list orderby c.CountryId ascending select c).ToList<Country>();
            //return (from c in list orderby c.Sort ascending select c).Skip(startRecord).Take(pageSize).ToList<Country>();
             *修改结束*/
        }
        #endregion

        #region 返回所有添加的城市
        /// <summary>
        /// 返回所有添加的城市
        /// </summary>
        /// <returns></returns>
        public List<WidgetIBTCity> GetAllWidgetIBTCity(string strWhere, int page, int pageSize, out int total)
        {
            string sqlCmd = @"SELECT   bt.ibtId,bt.ObjId,bt.ObjName,cn.CountryId,cn.CountryName,bt.CreateDate,bt.Sort
                                FROM    dbo.WidgetIBT bt
                                LEFT JOIN  dbo.Cities cy ON bt.ObjId = cy.CityId
                                LEFT JOIN  dbo.Countries cn ON cy.CountryId = cn.CountryId 
          WHERE 1=1 AND bt.IBTType=2 AND bt.StatusId=1 " + strWhere;
            IList<WidgetIBTCity> list = _db.Database.SqlQuery<WidgetIBTCity>(sqlCmd).ToList<WidgetIBTCity>();
            total = list.Count();
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.Sort ascending select c).Skip(startRecord).Take(pageSize).ToList<WidgetIBTCity>();
        }

        public IList<CountryCityItem> GetAllSearchCity(string strWhere, int page, int pageSize, out int total)
        {
            /* 20130219裕冲修改 */
            List<CountryCityItem> list = new List<CountryCityItem>();
            list = _db.Database.SqlQuery<CountryCityItem>("EXEC IBT_GetAllSearchCity {0},{1},{2}", strWhere, page, pageSize).ToList<CountryCityItem>();
            total = list.Count;

            return list;
            /*
            string sqlCmd = "select cy.CityId,cy.CityName,cn.CountryName,cy.Sort from dbo.Cities cy LEFT JOIN  dbo.Countries cn ON cy.CountryId = cn.CountryId WHERE  cy.CityName LIKE '%" + keyWord + @"%'";
            IList<CountryCityItem> list = _db.Database.SqlQuery<CountryCityItem>(sqlCmd).ToList<CountryCityItem>();
            total = list.Count();
            //int startRecord = (page - 1) * pageSize;
            //return (from c in list orderby c.Sort ascending select c).Skip(startRecord).Take(pageSize).ToList<CountryCityItem>();
            return (from c in list orderby c.CountryId ascending select c).ToList<CountryCityItem>();
             * 修改结束*/
        }
        #endregion

        #region 查询城市列表
        /// <summary>
        /// 查询城市列表
        /// </summary>
        /// <returns></returns>
        public List<WidgetIBTCity> GetAllWidgetIBTCityAfterClick(int page, int pageSize, out int total, out int itemTotal)
        {
            itemTotal = (from c in _db.WidgetIBTs where c.IBTType.Equals(2) && c.StatusId.Equals(1) select c).Count();
            if (itemTotal % 10 != 0)
                total = itemTotal / 10 + 1;
            else
                total = itemTotal / 10;
            int sitem = (page - 2) * pageSize;
            string sqlCmd = "select w.ObjName,c.cityName,cc.CountryName,w.CreateDate,w.UpdateDate,w.Sort,w.ibtId from WidgetIBT w join Cities c on w.ObjId=c.CityId join Countries cc on c.CountryId=cc.CountryId where w.IBTType=2 and w.StatusId=1";
            List<WidgetIBTCity> list = _db.Database.SqlQuery<WidgetIBTCity>(sqlCmd).ToList();
            return list;
        }
        #endregion

        #region 根据id删除国家
        /// <summary>
        /// 根据id删除国家
        /// </summary>
        /// <returns></returns>
        public BoolMessageItem<int> DeleteWidgetIBTCountry(int objId)
        {
            string sqlCmd = "update WidgetIBT set StatusId=0 where ibtId=@objId and IBTType=1";
            SqlParameter[] paras = { 
                                   new SqlParameter("@objId",objId)
                                   };
            _db.Database.ExecuteSqlCommand(sqlCmd, paras);
            return new BoolMessageItem<int>(1, true, "Deleted successful!");

        }
        #endregion

        #region  根据id删除城市
        /// <summary>
        /// 根据id删除城市
        /// </summary>
        /// <returns></returns>
        public BoolMessageItem<int> DeleteWidgetIBTCity(int objId)
        {
            string sqlCmd = "update WidgetIBT set StatusId=0 where ibtId=@objId and IBTType=2";
            SqlParameter[] paras = { 
                                   new SqlParameter("@objId",objId)
                                   };
            _db.Database.ExecuteSqlCommand(sqlCmd, paras);
            return new BoolMessageItem<int>(1, true, "Deleted successful!");
        }
        #endregion

        /// <summary>
        /// Country Sort
        /// </summary>
        /// <param name="ObjIds"></param>
        /// <returns></returns>
        public BoolMessageItem<int> SetWidgetCountrySort(List<int> ObjIds)
        {
            int objId = 0;
            WidgetIBT widgetIbt;
            for (int i = 0; i < ObjIds.Count(); i++)
            {
                objId = ObjIds[i];
                widgetIbt = _db.WidgetIBTs.FirstOrDefault(x => x.ibtId.Equals(objId) && x.IBTType.Equals(1));
                widgetIbt.Sort = i + 1;
                _db.SaveChanges();

            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }

        /// <summary>
        /// City Sort
        /// </summary>
        /// <param name="ObjIds"></param>
        /// <returns></returns>
        public BoolMessageItem<int> SetWidgetCitySort(List<int> ObjIds)
        {
            int objId = 0;
            WidgetIBT widgetIbt;
            for (int i = 0; i < ObjIds.Count(); i++)
            {
                objId = ObjIds[i];
                widgetIbt = _db.WidgetIBTs.FirstOrDefault(x => x.ibtId.Equals(objId) && x.IBTType.Equals(2));
                widgetIbt.Sort = i + 1;
                _db.SaveChanges();

            }
            return new BoolMessageItem<int>(1, true, "Edit Successful!");
        }

        #endregion

        #region Ads and Promotions Management

        #region 新增Media
        public BoolMessageItem<int> AddWidgetMedia(int ibtType, int supplyId, int countryId, int cityId, int position, int mediaType, string title, string desc, string startDate, string endTime, string imgUrl, string htmlCode, bool isLink, string targetUrl)
        {
            //string targetUrl = txtTargetUrl.Text.Trim();
            //string parttern = @"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?";
            //Regex regex = new Regex(parttern);
            //if (targetUrl.Equals(string.Empty) || !regex.IsMatch(targetUrl))
            //{
            //    if (this.cbIsUrl.Checked)
            //    {
            //        this.litMessage.Text = "<div class=\"error \">Please enter an URL for the media.</div>";
            //        return;
            //    }
            //}
            //else
            //{
            //    objAdvertisement.TargetUrl = targetUrl;
            //}
            //var q = from o in _db.Medias
            //        where o.SupplyId.Equals(supplyId) && o.Position.Equals(position) && o.MediaType.Equals(mediaType) 
            //        select o;
            try
            {
                //Ads、Special offers表
                DAL.Media media = new Media();
                media.IBTType = ibtType;
                media.SupplyId = supplyId;
                media.CountryId = countryId;
                media.CityId = cityId;
                media.Position = position;
                media.MediaType = mediaType;
                media.Title = title;
                media.Desp = desc;
                media.StartDate = DateTime.Parse(startDate);
                media.ExpiryDate = DateTime.Parse(endTime);
                if (mediaType != (int)Identifier.MediaTypes.picture)//2 picture
                {
                    media.Code = htmlCode;
                }
                else
                {
                    media.FileSrc = imgUrl;
                }
                media.IsLink = isLink;
                media.TargetUrl = targetUrl;
                media.CreateDate = DateTime.Now;
                media.UpdateDate = DateTime.Now;
                media.StatusId = 1;
                _db.Medias.Add(media);
                _db.SaveChanges();
                //统计表
                DAL.StatisticsReportInfo report = new StatisticsReportInfo();
                int Max_mediaId = media.MediaId;
                report.MediaId = Max_mediaId;
                report.LocalUrl = "";
                report.AriseCount = 0;
                report.BrowseCount = 0;
                report.ClickCount = 0;
                report.CreateDate = DateTime.Now;
                report.UpdateDate = DateTime.Now;
                _db.StatisticsReportInfoes.Add(report);
                _db.SaveChanges();
                return new BoolMessageItem<int>(media.MediaId, true, "Create successful!");
            }
            catch (Exception ex)
            {
                _logger.Error("Media", ex);
                return new BoolMessageItem<int>(-1, false, "Save Error!");
            }

        }
        #endregion

        #region Edit Media
        public BoolMessageItem<int> EditWidgetMedia(int mediaId, int ibtType, int supplyId, int countryId, int cityId, int position, int mediaType, string title, string desc, string startDate, string endTime, string imgUrl, string htmlCode, bool isLink, string targetUrl)
        {
            Media media = _db.Medias.FirstOrDefault(x => x.MediaId.Equals(mediaId));
            if (media != null)
            {
                media.SupplyId = supplyId;
                media.CountryId = countryId;
                media.CityId = cityId;
                media.IBTType = ibtType;
                media.Position = position;
                media.MediaType = mediaType;
                media.Title = title;
                media.Desp = desc;
                media.StartDate = Convert.ToDateTime(startDate);
                media.ExpiryDate = Convert.ToDateTime(endTime);
                if (mediaType != 2)//2 picture
                {
                    media.Code = htmlCode;
                }
                else
                {
                    media.FileSrc = imgUrl;
                }
                media.IsLink = isLink;
                media.TargetUrl = targetUrl;
                media.UpdateDate = DateTime.Now;
                media.StatusId = 1;
                _db.SaveChanges();
                return new BoolMessageItem<int>(media.MediaId, true, "Edit successful!");

            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Edit failure!");
            }
        }
        #endregion

        /// <summary>
        /// 返回所有添加的Ads
        /// </summary>
        /// <returns></returns>
        public IList<WidgetMedia> GetAllWidgetAdsInfo(string strWhere, int page, int pageSize, out int total)
        {
            string strSql = @"select ROW_NUMBER() OVER (ORDER BY dbo.Medias.CreateDate DESC) AS RowId,dbo.Supplies.Title AS SupplyName, dbo.Medias.* from dbo.Medias LEFT JOIN dbo.Supplies ON dbo.Medias.SupplyId = dbo.Supplies.SupplyId where 1=1 and dbo.Medias.StatusId=1 " + strWhere;
            IList<WidgetMedia> list = _db.Database.SqlQuery<WidgetMedia>(strSql).ToList<WidgetMedia>();
            total = list.Count();
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.CreateDate descending select c).Skip(startRecord).Take(pageSize).ToList<WidgetMedia>();

        }

        #region 根据id删除Ads
        /// <summary>
        /// 根据id删除Ads
        /// </summary>
        /// <returns></returns>
        public BoolMessageItem<int> DeleteWidgetAds(int objId)
        {
            string sqlCmd = "update Medias set StatusId=0 where MediaId=@objId and StatusId=1";
            SqlParameter[] paras = { 
                                   new SqlParameter("@objId",objId)
                                   };
            _db.Database.ExecuteSqlCommand(sqlCmd, paras);
            return new BoolMessageItem<int>(1, true, "Deleted successful!");

        }
        #endregion

        public DAL.Media GetWidgetMediaInfo(int mediaId)
        {
            var media = _db.Medias.Where(c => c.MediaId.Equals(mediaId)).FirstOrDefault();
            if (media != null)
                return media;
            else
            {
                return new DAL.Media();
            }
        }

        /// <summary>
        /// GetSpecialAdsList
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public IList<Media> GetSpecialAdsList(int supplyId)
        {
            byte statusId = Utils.IntToByte((int)Identifier.StatusType.Enabled);
            return (from c in _db.Medias
                    where (c.SupplyId.Equals(supplyId) && c.StatusId.Equals(statusId) && c.Position.Equals(1) && c.ExpiryDate > DateTime.Now)
                    orderby c.UpdateDate descending
                    select c).ToList<Media>();
        }
        public IList<Media> GetSpecialsOfCountry(int countryId, int pageNumber, int pageSize, out int total)
        {
            int startSize = (pageNumber - 1) * pageSize + 1;
            int endSize = startSize + pageSize - 1;
            string strSql = @"SELECT 	t1.* FROM (
                                  SELECT ROW_NUMBER() OVER (ORDER BY Title ASC) AS RowId, * FROM dbo.Medias 
                              WHERE CountryId={0} AND Position=1  AND Convert(varchar(10),ExpiryDate,120) >= Convert(varchar(10),getdate(),120) AND StatusId=1
                            ) As t1
	                        WHERE RowId BETWEEN " + startSize.ToString() + " AND " + endSize.ToString() + "";
            var list = _db.Database.SqlQuery<Media>(strSql, countryId).ToList<Media>();
            total = list.Count();
            return list;
        }
        #endregion

        #region supplies attachment
        /// <summary>
        /// 根据supplierid来读取相关的图片
        /// </summary>
        /// <param name="supplyId">supplyId</param>
        /// <param name="limit">读取的数量，0表示全部</param>
        /// <param name="type">是否包含logo</param>
        /// <returns></returns>
        public IList<SuppliesAttachment> GetSuppliesAttachment(int supplyId, int limit = 0, bool haslogo = false)
        {
            IList<SuppliesAttachmentBind> bindlist = null;
            IList<SuppliesAttachment> list = null;
            if (haslogo)
            {
                bindlist = (from b in _db.SuppliesAttachmentBinds where b.SupplyId == supplyId select b).ToList();
            }
            else
            {
                bindlist = (from b in _db.SuppliesAttachmentBinds where b.SupplyId == supplyId && b.Type == 0 select b).ToList();
            }

            if (limit != 0)
            {
                bindlist = bindlist.Take(limit).ToList();
            }

            if (bindlist != null)
            {
                var guids = (from a in bindlist select a.AttGuidId).ToArray();
                list = (from a in _db.SuppliesAttachments where guids.Contains(a.Guid) select a).ToList();
            }

            return list;

        }
        #endregion

        #region Supplier Specials
        /// <summary>
        /// Specials Lists 的数据
        /// </summary>
        /// <param name="filterSort"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<Media> GetSpecialsList(string filterSort, int page, int pageSize, string filterKeyword, string filterField, out int total)
        {

            IList<Media> list = _db.Database.SqlQuery<Media>("EXEC IBT_SpecialsList {0},{1},{2},{3},{4}", filterField, filterSort, filterKeyword, page, pageSize).ToList<Media>();
            total = list.Count();
            if (filterSort == "Date")
            {
                return list.OrderByDescending(a => a.StartDate).ToList();
            }
            else if (filterSort == "Alphabet")
            {
                return list.OrderBy(a => a.Title).ToList();
            }
            else
            {
                return list;
            }
        }
        #endregion

        #region supplier Attachment--附件 and AttachmentComments--附件评论
        public IList<Attachment> GetAttachment(int objId, Services.Identifier.AttachmentType type)
        {
            IList<Attachment> attachmentList = (from a in _db.Attachments where a.ObjId == objId && a.AttType == (int)type && a.StatusId == 1 select a).ToList();

            return attachmentList;
        }
        public IList<AttachmentComment> GetAttachmentComments(int attid, Services.Identifier.AttachmentType type)
        {
            IList<AttachmentComment> attachmentCommentList = (from a in _db.AttachmentComments where a.AttId == attid select a).ToList();

            return attachmentCommentList;
        }
        #endregion

        #region Newsfeed好友列表
        /// <summary>
        /// 返回最新7天好友列表
        /// </summary>
        /// <returns></returns>
        public IList<ConnectionListItem> GetNewsfeedFriend(int userId, int newsfeedId)
        {
            //            string strSql = @"   SELECT TOP 5 * FROM dbo.Connections 
            //                                WHERE 
            //                                FriendId IN
            //		(
            //			SELECT MAX(ObjData) FROM dbo.NewsFeed WHERE UserId={0} AND StatusId=1 AND ActiveType=9 AND datediff(dd,CreateDate,getdate()) <= 7 GROUP BY UserId,ObjData
            //		) AND dbo.Connections.UserId={0} AND StatusId=1 AND datediff(dd,UpdateDate,getdate()) <= 7 ORDER BY UpdateDate DESC";
            string strSql = @"SELECT * FROM dbo.Connections
WHERE CONVERT(NVARCHAR(100),DATEADD(DAY,-7,(SELECT CreateDate FROM dbo.NewsFeed WHERE NewsFeedId={1})),23) <= UpdateDate and  
UpdateDate <= (SELECT CreateDate FROM dbo.NewsFeed WHERE NewsFeedId={1})
AND dbo.Connections.UserId={0} AND StatusId=1 
ORDER BY ConnectionId DESC";

            return _db.Database.SqlQuery<ConnectionListItem>(strSql, userId, newsfeedId).ToList<ConnectionListItem>();
        }
        #endregion

        #region Services列表
        /// <summary>
        /// userId's Services List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IList<FeedList> ServicesSPList(int userId, int categoryId, int page, int pageSize, string filterLetter, string sortByRating)
        {
            //1:从数据获取主要的数据
            List<FeedList> list = new List<FeedList>();
            list = _db.Database.SqlQuery<FeedList>("EXEC IBT_UserTravelFeeds {0},{1},{2},{3}", userId, page, pageSize, categoryId).ToList<FeedList>();        
            //2:根据IBTType去获取RRWB
            list = this.GetFeedRRWB(list);

            //如果是pv，看是否有上传有图片
            list = this.GetFeedPostVisitPhotos(list);
            if (!string.IsNullOrWhiteSpace(filterLetter))
            {
                return (from c in list where c.ExTitle.ToUpper().StartsWith(filterLetter) orderby c.CreateDate descending select c).ToList<FeedList>();
            }
            else
            {
                if (sortByRating == "Yes")
                {
                    return list.OrderByDescending(a => a.Rating).ToList();
                }
                else
                {
                    return list;
                }
            }
        }
        #endregion
    }
}
