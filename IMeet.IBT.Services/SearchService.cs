﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public class SearchService : BaseService, ISearchService
    {
        #region SearchResult_List

        public int GetSearchResultListCount(string strWhere)
        {

            string[] strspl = strWhere.Trim().Split(' ');
            string strq = string.Empty;
            string strw = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR n.Title LIKE '%" + item.Trim() + "%' OR n.Desp LIKE '%" + item.Trim() + "%' OR b.CountryName LIKE '%" + item.Trim() + "%' OR s.CityName LIKE '%" + item.Trim() + "%' ";
                strw = strw + " OR n.Firstname LIKE '%" + item.Trim() + "%' OR n.Lastname LIKE '%" + item.Trim() + "%' OR b.CountryName LIKE '%" + item.Trim() + "%' OR s.CityName LIKE '%" + item.Trim() + "%' ";
            }

            //如果关键词为空的话，查询所有
            strq = string.IsNullOrEmpty(strWhere) ? " or 1=1 " : strq;
            strw = string.IsNullOrEmpty(strWhere) ? " or 1=1 " : strw;

            string q = "SELECT COUNT(n.SupplyId) FROM dbo.Supplies AS n JOIN dbo.Countries AS b ON n.CountryId = b.CountryId JOIN dbo.Cities AS s ON n.CityId = s.CityId WHERE 1=2 " + strq;
            var q_count = _db.Database.SqlQuery<int>(q).ToList();

            string w = "SELECT COUNT(n.UserId) FROM dbo.Profiles AS n JOIN dbo.Countries AS b ON n.CountryId=b.CountryId JOIN dbo.Cities AS s ON n.CityId=s.CityId WHERE 1=2 " + strw;
            var m_count = _db.Database.SqlQuery<int>(w).ToList();


            //var q = from n in _db.Supplies
            //        join b in _db.Countries on n.CountryId equals b.CountryId
            //        join s in _db.Cities on n.CityId equals s.CityId
            //        where (n.Title.Contains(strWhere) || n.Desp.Contains(strWhere) || b.CountryName.Contains(strWhere) || s.CityName.Contains(strWhere))
            //        orderby n.SupplyId descending
            //        select new
            //        {
            //            n.SupplyId,
            //            n.Title,
            //            n.Desp,
            //            b.CountryId,
            //            b.CountryName,
            //            s.CityId,
            //            s.CityName
            //        };
            //var m = from n in _db.Profiles
            //        join b in _db.Countries on n.CountryId equals b.CountryId
            //        join s in _db.Cities on n.CityId equals s.CityId
            //        where (n.Firstname.Contains(strWhere) || n.Lastname.Contains(strWhere) || b.CountryName.Contains(strWhere) || s.CityName.Contains(strWhere))
            //        orderby n.UserId descending
            //        select new
            //        {
            //            n.UserId,
            //            n.Firstname,
            //            n.Lastname,
            //            b.CountryId,
            //            b.CountryName,
            //            s.CityId,
            //            s.CityName
            //        };
            return q_count[0] + m_count[0];
        }

        #region Member
        public int GetSearchMemberResultCount(string strWhere)
        {
            if (strWhere == "")
            {
                return (from c in _db.Profiles select c).Count();
            }
            else
            {
                string[] strspl = strWhere.Trim().Split(' ');
                string strw = string.Empty;
                foreach (var item in strspl)
                {
                    strw = strw + " OR n.Firstname LIKE '%" + item.Trim() + "%' OR n.Lastname LIKE '%" + item.Trim() + "%' OR b.CountryName LIKE '%" + item.Trim() + "%' OR s.CityName LIKE '%" + item.Trim() + "%' ";
                }

                strw = string.IsNullOrEmpty(strWhere) ? " OR 1=1 " : strw;

                string w = "SELECT COUNT(n.UserId) FROM dbo.Profiles AS n JOIN dbo.Countries AS b ON n.CountryId=b.CountryId JOIN dbo.Cities AS s ON n.CityId=s.CityId WHERE 1=2 " + strw;
                var w_count = _db.Database.SqlQuery<int>(w).ToList();

                return w_count[0];

                /*
                var m = from n in _db.Profiles
                        join b in _db.Countries on n.CountryId equals b.CountryId
                        join s in _db.Cities on n.CityId equals s.CityId
                        where (n.Firstname.Contains(strWhere) || n.Lastname.Contains(strWhere) || b.CountryName.Contains(strWhere) || s.CityName.Contains(strWhere))
                        orderby n.UserId descending
                        select new
                        {
                            n.UserId,
                            n.Firstname,
                            n.Lastname,
                            b.CountryId,
                            b.CountryName,
                            s.CityId,
                            s.CityName
                        };
                return m.Count();
                 * */
            }
        }
        /// <summary>
        /// 返回Member列表Count
        /// </summary>
        /// <returns></returns>
        public int GetAllMemberCount(string keyword)
        {
            string[] strspl = keyword.Trim().Split(' ');
            string strw = string.Empty;
            foreach (var item in strspl)
            {
                strw = strw + " OR mp.Firstname LIKE '%" + item.Trim() + "%' OR mp.Lastname LIKE '%" + item.Trim() + "%' OR cn.CountryName LIKE '%" + item.Trim() + "%' OR cy.CityName LIKE '%" + item.Trim() + "%' ";
            }

            strw = string.IsNullOrEmpty(keyword) ? " OR 1=1 " : strw;

            string w = "SELECT COUNT(mp.UserId) FROM  dbo.Profiles mp LEFT JOIN  dbo.Users u ON u.UserId=mp.UserId LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId WHERE 1=2 " + strw;
            var w_count = _db.Database.SqlQuery<int>(w).ToList();

            return w_count[0];

            /*
            string sql = @"SELECT 
                ROW_NUMBER() OVER (ORDER BY mp.Sort DESC, mp.UserId ASC) AS RowId,
				mp.UserId,mp.Username,mp.Birth,mp.FirstName,mp.LastName,mp.Email,mp.AlternateEmail,mp.Gender,mp.Location,mp.ProfileImage,
				mp.CurrentLoginSnsId,mp.Sort,mp.CountryId,cn.CountryName,mp.CityId,cy.CityName
                            FROM  dbo.Profiles mp
                            LEFT JOIN  dbo.Users u ON u.UserId=mp.UserId 
                            LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId
                            WHERE mp.FirstName LIKE '%" + keyword.ToString() + @"%' OR mp.LastName LIKE '%" + keyword.ToString() + @"%' OR cn.CountryName LIKE '%" + keyword.ToString() + @"%' OR cy.CityName LIKE '%" + keyword.ToString() + @"%'";
            return _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>().Count;
             * */
        }
        public int GetAllMemberCount(string keyword, string filterLetter)
        {
            string[] strspl = keyword.Trim().Split(' ');
            string strw = string.Empty;
            foreach (var item in strspl)
            {
                strw = strw + " OR mp.Firstname LIKE '%" + item.Trim() + "%' OR mp.Lastname LIKE '%" + item.Trim() + "%' OR cn.CountryName LIKE '%" + item.Trim() + "%' OR cy.CityName LIKE '%" + item.Trim() + "%' ";
            }

            strw = string.IsNullOrEmpty(keyword) ? " OR 1=1 " : strw;

            string w = "SELECT COUNT(mp.UserId) FROM  dbo.Profiles mp LEFT JOIN  dbo.Users u ON u.UserId=mp.UserId LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId WHERE (1=2 " + strw + ") AND mp.Firstname LIKE '" + filterLetter + "%' ";
            var w_count = _db.Database.SqlQuery<int>(w).ToList();

            return w_count[0];

            /*
            string sql = @"SELECT 
                ROW_NUMBER() OVER (ORDER BY mp.Sort DESC, mp.UserId ASC) AS RowId,
				mp.UserId,mp.Username,mp.Birth,mp.FirstName,mp.LastName,mp.Email,mp.AlternateEmail,mp.Gender,mp.Location,mp.ProfileImage,
				mp.CurrentLoginSnsId,mp.Sort,mp.CountryId,cn.CountryName,mp.CityId,cy.CityName
                            FROM  dbo.Profiles mp
                            LEFT JOIN  dbo.Users u ON u.UserId=mp.UserId 
                            LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId
                            WHERE mp.FirstName LIKE '%" + keyword.ToString() + @"%' OR mp.LastName LIKE '%" + keyword.ToString() + @"%' OR cn.CountryName LIKE '%" + keyword.ToString() + @"%' OR cy.CityName LIKE '%" + keyword.ToString() + @"%'";
            return _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>().Count;
             * */
        }

        public IList<MemberListItem> GetMemberSearchList(int topN, string basekeyWord)
        {
            //string[] strspl = baseSearch.Trim().Split(' ');
            //string strw = string.Empty;
            //foreach (var item in strspl)
            //{
            //    strw = strw + " OR mp.Firstname LIKE '%" + item.Trim() + "%' OR mp.Lastname LIKE '%" + item.Trim() + "%' OR cn.CountryName LIKE '%" + item.Trim() + "%' OR cy.CityName LIKE '%" + item.Trim() + "%' ";
            //}

            string strw = string.Empty;
            strw = strw + " OR mp.Firstname + ' ' + mp.Lastname  LIKE '%" + basekeyWord.ToString() + "%' OR cn.CountryName LIKE '%" + basekeyWord.ToString() + "%' OR cy.CityName LIKE '%" + basekeyWord.ToString() + "%' ";

            string sql = "SELECT  TOP " + topN.ToString() + @"   ROW_NUMBER() OVER (ORDER BY mp.Sort DESC, mp.UserId ASC) AS RowId,mp.UserId,mp.Username,mp.Birth,mp.FirstName,mp.LastName,mp.Email,mp.AlternateEmail,mp.Gender,mp.Location,mp.ProfileImage,mp.CurrentLoginSnsId,mp.Sort,mp.CountryId,cn.CountryName,mp.CityId,cy.CityName FROM  dbo.Profiles mp LEFT JOIN  dbo.Users u on u.UserId=mp.UserId  LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId WHERE 1=2 " + strw;

            return _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>();

            /*
            string sql = @"SELECT TOP " + topN.ToString() + @"   ROW_NUMBER() OVER (ORDER BY mp.Sort DESC, mp.UserId ASC) AS RowId,
				mp.UserId,mp.Username,mp.Birth,mp.FirstName,mp.LastName,mp.Email,mp.AlternateEmail,mp.Gender,mp.Location,mp.ProfileImage,
				mp.CurrentLoginSnsId,mp.Sort,mp.CountryId,cn.CountryName,mp.CityId,cy.CityName
                            FROM  dbo.Profiles mp
                            LEFT JOIN  dbo.Users u on u.UserId=mp.UserId 
                            LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId
                            WHERE mp.FirstName LIKE '%" + baseSearch.ToString() + @"%' OR mp.LastName LIKE '%" + baseSearch.ToString() + @"%' OR cn.CountryName LIKE '%" + baseSearch.ToString() + @"%' OR cy.CityName LIKE '%" + baseSearch.ToString() + @"%'";
            return _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>();
             * */
        }
        /// <summary>
        /// Search Member All Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="SearchKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<MemberListItem> GetSearchMemberAllList(int page, int pageSize, string SearchKeyword, string filterLetter, string filterField, out int total)
        {
            //--IBT_MemberBaseSearch '',1,10,0,1
            IList<MemberListItem> list = _db.Database.SqlQuery<MemberListItem>("EXEC IBT_MemberBaseSearch {0},{1},{2},{3}", SearchKeyword, filterLetter, page, pageSize).ToList<MemberListItem>();
            total = GetAllMemberCount(SearchKeyword, filterLetter);

            if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            {
                //todo:GetMyList filterField
                return new List<MemberListItem>();
            }
            else
            {
                return list;
            }

            //todo 下面还需要返回成员数
        }
        #endregion

        #region Supply
        public int GetSearchSupplyResultCount(string strWhere)
        {
            if (strWhere == "")
            {
                return (from c in _db.Supplies select c).Count();
            }
            else
            {
                string[] strspl = strWhere.Trim().Split(' ');
                string strq = string.Empty;
                foreach (var item in strspl)
                {
                    strq = strq + " OR n.Title LIKE '%" + item.Trim() + "%' OR b.CountryName LIKE '%" + item.Trim() + "%' OR s.CityName LIKE '%" + item.Trim() + "%' ";
                }
                //如果关键词为空的话，查询所有
                strq = string.IsNullOrEmpty(strWhere) ? " or 1=1 " : strq;

                string q = "SELECT COUNT(n.SupplyId) FROM dbo.Supplies AS n JOIN dbo.Countries AS b ON n.CountryId = b.CountryId JOIN dbo.Cities AS s ON n.CityId = s.CityId WHERE 1=2 " + strq;
                var q_count = _db.Database.SqlQuery<int>(q).ToList();

                //var q = from n in _db.Supplies
                //        join b in _db.Countries on n.CountryId equals b.CountryId
                //        join s in _db.Cities on n.CityId equals s.CityId
                //        where (n.Title.Contains(strWhere) || n.Desp.Contains(strWhere) || b.CountryName.Contains(strWhere) || s.CityName.Contains(strWhere))
                //        orderby n.SupplyId descending
                //        select new
                //        {
                //            n.SupplyId,
                //            n.Title,
                //            n.Desp,
                //            b.CountryId,
                //            b.CountryName,
                //            s.CityId,
                //            s.CityName
                //        };
                return q_count[0];
            }

        }

        /// <summary>
        /// 返回Supplier列表Count
        /// </summary>
        /// <returns></returns>
        public int GetAllSupplierCount(int userId, string keyword)
        {
            string[] strspl = keyword.Trim().Split(' ');
            string strq = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR sp.Title LIKE '%" + item.Trim() + "%' OR cn.CountryName LIKE '%" + item.Trim() + "%' OR cy.CityName LIKE '%" + item.Trim() + "%' ";
                //去除简介进入检索条件
                // OR sp.Desp LIKE '%" + item.Trim() + "%'
            }

            strq = string.IsNullOrEmpty(keyword) ? " OR 1=1 " : strq;

            string q = "SELECT COUNT(sp.supplyid) FROM  dbo.Supplies sp LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId LEFT JOIN dbo.IBT bt ON sp.CountryId = bt.CountryId AND sp.CityId = bt.CityId  AND bt.SupplyId=sp.SupplyId AND bt.UserId=" + userId.ToString() + " LEFT JOIN dbo.RRWB rw ON sp.SupplyId = rw.ObjId and rw.IBTType=3 AND rw.UserId=" + userId.ToString() + " WHERE (1=2 " + strq + ") AND sp.StatusId=1";
            var q_count = _db.Database.SqlQuery<int>(q).ToList();
            return q_count[0];

            /**
            string sql = @"SELECT 
                ROW_NUMBER() OVER (ORDER BY sp.Score DESC, sp.SupplyId ASC) AS RowId,
			    sp.SupplyId,sp.Title,sp.SupplierTypes,sp.Score,sp.Desp,
                sp.CoverPhotoSrc,sp.StatusId,sp.InfoAddress,sp.InfoState,sp.CreateDate,sp.UpdateDate,
                sp.CountryId,cn.CountryName,sp.CityId,cy.CityName,
                ISNULL(rw.Rating,0) AS Rating ,
                ISNULL(rw.Recommended,0) AS Recommended ,
                ISNULL(rw.WanttoGo,0) AS WanttoGo ,
                ISNULL(rw.BucketList,0) AS BucketList ,
                ISNULL(bt.IbtCount,0) AS IbtCount
                            FROM  dbo.Supplies sp
                            LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId
                            LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId
                            LEFT JOIN dbo.IBT bt ON sp.CountryId = bt.CountryId AND sp.CityId = bt.CityId  AND bt.SupplyId=sp.SupplyId AND bt.UserId=" + userId.ToString() + @"
                            LEFT JOIN dbo.RRWB rw ON sp.SupplyId = rw.ObjId and rw.IBTType=3 AND rw.UserId=" + userId.ToString() + @"
                            WHERE sp.Title LIKE '%" + keyword.ToString() + @"%' OR sp.Desp LIKE '%" + keyword.ToString() + @"%' OR cn.CountryName LIKE '%" + keyword.ToString() + @"%' OR cy.CityName LIKE '%" + keyword.ToString() + @"%'";
            return _db.Database.SqlQuery<SupplyListShort>(sql).ToList<SupplyListShort>().Count;
             * */
        }
        public int GetAllSupplierCount(int userId, string keyword, string filterLetter)
        {
            string[] strspl = keyword.Trim().Split(' ');
            string strq = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR sp.Title LIKE '%" + item.Trim() + "%' OR sp.Desp LIKE '%" + item.Trim() + "%' OR cn.CountryName LIKE '%" + item.Trim() + "%' OR cy.CityName LIKE '%" + item.Trim() + "%' ";
            }

            strq = string.IsNullOrEmpty(keyword) ? " OR 1=1 " : strq;

            string q = "SELECT COUNT(sp.supplyid) FROM  dbo.Supplies sp LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId LEFT JOIN dbo.IBT bt ON sp.CountryId = bt.CountryId AND sp.CityId = bt.CityId  AND bt.SupplyId=sp.SupplyId AND bt.UserId=" + userId.ToString() + " LEFT JOIN dbo.RRWB rw ON sp.SupplyId = rw.ObjId and rw.IBTType=3 AND rw.UserId=" + userId.ToString() + " WHERE (1=2 " + strq + ") AND sp.Title LIKE '" + filterLetter + "%' AND sp.StatusId=1 "; ;
            var q_count = _db.Database.SqlQuery<int>(q).ToList();
            return q_count[0];
        }

        public IList<SupplierListItem> GetSupplierSearchList(int topN, string baseSearch)
        {
            string[] strspl = baseSearch.Trim().Split(' ');
            string strq = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR sp.Title LIKE '%" + item.Trim() + "%' OR cn.CountryName LIKE '%" + item.Trim() + "%' OR cy.CityName LIKE '%" + item.Trim() + "%' ";
            }

            string sql = "SELECT TOP " + topN.ToString() + @"   ROW_NUMBER() OVER (ORDER BY sp.Score DESC, sp.SupplyId ASC) AS RowId,
				sp.SupplyId,sp.Title,sp.SupplierTypes,sp.ParentSupplyId,sp.Score,sp.Desp,sp.CoverPhotoSrc,sp.CoordsLatitude,sp.CoordsLongitude,
				sp.Rating, sp.StatusId,sp.InfoAddress,sp.InfoZip,sp.InfoPhone,sp.InfoFax,sp.InfoState,sp.InfoWebsite,sp.CreateDate,sp.UpdateDate,
				sp.CountryId,cn.CountryName,sp.CityId,cy.CityName
                            FROM  dbo.Supplies sp
                            LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId
                            LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId
                            WHERE 1=2 " + strq;

            return _db.Database.SqlQuery<SupplierListItem>(sql).ToList<SupplierListItem>();

            /*
            string sql = @"SELECT TOP " + topN.ToString() + @"   ROW_NUMBER() OVER (ORDER BY sp.Score DESC, sp.SupplyId ASC) AS RowId,
				sp.SupplyId,sp.Title,sp.SupplierTypes,sp.ParentSupplyId,sp.Score,sp.Desp,sp.CoverPhotoSrc,sp.CoordsLatitude,sp.CoordsLongitude,
				sp.Rating, sp.StatusId,sp.InfoAddress,sp.InfoZip,sp.InfoPhone,sp.InfoFax,sp.InfoState,sp.InfoWebsite,sp.CreateDate,sp.UpdateDate,
				sp.CountryId,cn.CountryName,sp.CityId,cy.CityName
                            FROM  dbo.Supplies sp
                            LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId
                            LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId
                            WHERE sp.Title LIKE '%" + baseSearch.ToString() + @"%' OR sp.Desp LIKE '%" + baseSearch.ToString() + @"%' OR cn.CountryName LIKE '%" + baseSearch.ToString() + @"%' OR cy.CityName LIKE '%" + baseSearch.ToString() + @"%'";
            return _db.Database.SqlQuery<SupplierListItem>(sql).ToList<SupplierListItem>();
             * */
        }

        

        /// <summary>
        /// Search Supplier All Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="SearchKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<SupplyListShort> GetSearchSupplierAllList(int userId, int page, int pageSize, string SearchKeyword, string filterLetter, out int total)
        {
            //--IBT_SupplyBaseSearch '',1,10,0,1
            IList<SupplyListShort> list = _db.Database.SqlQuery<SupplyListShort>("EXEC IBT_SupplyBaseSearch {0},{1},{2},{3},{4}", userId, filterLetter, SearchKeyword, page, pageSize).ToList<SupplyListShort>();

            if (!string.IsNullOrWhiteSpace(filterLetter)) //搜索
            {
                total = GetAllSupplierCount(userId, SearchKeyword, filterLetter);
            }
            else
            {
                total = GetAllSupplierCount(userId, SearchKeyword);
            }
            return list;
            //todo 下面还需要返回成员数
        }

        /// <summary>
        /// get the SupplyListShort for search listing
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="attDataName"></param>
        /// <param name="filter"></param>
        /// <param name="keyWord"></param>
        /// <param name="outTotal"></param>
        /// <returns></returns>
        public List<SupplyListShort> GetSupplySearchListing(int userid,int pageIndex,int pageSize,string attDataName,string filter,string keyWord,out int outTotal)
        {
            int start = (pageIndex - 1) * pageSize;
            int top = pageIndex * pageSize;

            string strWhere = "";
            keyWord=keyWord.Replace('\'',' ');

            if (!string.IsNullOrEmpty(keyWord)) {
                string[] keyWordArray = keyWord.Split(' ');
                keyWordArray.ForEach(s => {
                    strWhere += "AND sp.Title LIKE '%" + s + "%' ";
                });
            }

            List<SupplyListShort> list = _db.Database.SqlQuery<SupplyListShort>("EXEC IBT_SupplySearchListing {0},{1},{2},{3}", top.ToString(), attDataName, filter, strWhere).ToList();
            list = list.Skip(start).ToList();

            List<DAL.IBT> ibtList = _db.IBTs.Where(i => i.StatusId == (int)Identifier.StatusType.Enabled && i.UserId == userid && i.SupplyId > 0).ToList();
            List<RRWB> rrwbList = _db.RRWBs.Where(r => r.StatusId == (int)Identifier.StatusType.Enabled && r.UserId == userid && r.IBTType == (int)Identifier.IBTType.Supply).ToList();

            list.ForEach(sls => {
                DAL.IBT ibt = ibtList.FirstOrDefault(i => i.SupplyId == sls.SupplyId);

                if (ibt != null)
                    sls.IbtCount = ibt.IbtCount;

                RRWB rrwb = rrwbList.FirstOrDefault(r => r.ObjId == sls.SupplyId);

                if (rrwb != null)
                {
                    sls.Rating = rrwb.Rating;
                    sls.Recommended = rrwb.Recommended;
                    sls.WanttoGo = rrwb.WanttoGo;
                    sls.BucketList = rrwb.BucketList;
                }
            });

            outTotal = _db.Database.SqlQuery<int>("EXEC IBT_SupplySearchListingCount {0},{1},{2}", attDataName, filter, strWhere).First();

            return list;
        }
        #endregion

        #region Country
        public IList<CountryListItem> GetCountrySearchList(int topN, int userId, string baseSearch)
        {
            string[] strspl = baseSearch.Trim().Split(' ');
            string strq = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR dbo.Countries.CountryName LIKE '%" + item.Trim() + "%' ";
            }

            string sql = @"SELECT TOP " + topN.ToString() + @"   
				dbo.Countries.CountryId,dbo.Countries.CountryName, 
                dbo.Countries.FlagSrc,
                ISNULL(RRWB.IBTType,0) AS IBTType ,
                ISNULL(RRWB.Rating,0) AS Rating ,
                ISNULL(RRWB.Recommended,0) AS Recommended ,
                ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
                ISNULL(RRWB.BucketList,0) AS BucketList ,
                ISNULL(dbo.IBT.IbtCount,0) AS IbtCount
                FROM  dbo.Countries 
                LEFT JOIN dbo.IBT ON dbo.Countries.CountryId = dbo.IBT.CountryId AND  dbo.IBT.CityId=0 AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() +
              @"LEFT JOIN dbo.RRWB ON dbo.Countries.CountryId = dbo.RRWB.ObjId and RRWB.IBTType=1 AND RRWB.UserId=" + userId.ToString() + @"
                            WHERE 1=2 " + strq;

            return _db.Database.SqlQuery<CountryListItem>(sql).ToList<CountryListItem>();

            /*
            string sql = @"SELECT TOP " + topN.ToString() + @"   
				dbo.Countries.CountryId,dbo.Countries.CountryName, 
                dbo.Countries.FlagSrc,
                ISNULL(RRWB.IBTType,0) AS IBTType ,
                ISNULL(RRWB.Rating,0) AS Rating ,
                ISNULL(RRWB.Recommended,0) AS Recommended ,
                ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
                ISNULL(RRWB.BucketList,0) AS BucketList ,
                ISNULL(dbo.IBT.IbtCount,0) AS IbtCount
                FROM  dbo.Countries 
                LEFT JOIN dbo.IBT ON dbo.Countries.CountryId = dbo.IBT.CountryId AND  dbo.IBT.CityId=0 AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() +
              @"LEFT JOIN dbo.RRWB ON dbo.Countries.CountryId = dbo.RRWB.ObjId and RRWB.IBTType=1 AND RRWB.UserId=" + userId.ToString() +
                            @"WHERE  dbo.Countries.CountryName LIKE '%" + baseSearch.ToString() + @"%'";
            return _db.Database.SqlQuery<CountryListItem>(sql).ToList<CountryListItem>();
             * */
        }
        /// <summary>
        /// get countryListItem for search listing
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="index"></param>
        /// <param name="pageSize"></param>
        /// <param name="filter"></param>
        /// <param name="keyWork"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public List<CountryListItem> GetCountrySearchListing(int userid, int index, int pageSize, string filter, string keyWork, out int total)
        {
            int start = (index - 1) * pageSize;
            ISupplierService iss=new SupplierService();

            List<CountryListItem> list = iss.GetAllCountry().OrderBy(c=>c.CountryName).Select(c => new CountryListItem { 
                CountryId=c.CountryId,
                CountryName=c.CountryName,
                FlagSrc=c.FlagSrc
            }).ToList();

            if (!string.IsNullOrEmpty(filter))
                list = list.Where(c => c.CountryName.ToUpper().StartsWith(filter)).ToList();

            if (!string.IsNullOrEmpty(keyWork))
            {
                string[] strWhere = keyWork.Split(' ');
                strWhere.ForEach(s =>
                {
                    list = list.Where(c => c.CountryName.ToUpper().Contains(s.ToUpper())).ToList();
                });
            }

            total = list.Count;

            list = list.Skip(start).Take(pageSize).ToList();

            List<DAL.IBT> ibtList = _db.IBTs.Where(i => i.StatusId == (int)Identifier.StatusType.Enabled && i.CityId == 0 && i.SupplyId == 0 && i.CountryId > 0&&i.UserId==userid).ToList();
            List<RRWB> rrwbList = _db.RRWBs.Where(r => r.StatusId == (int)Identifier.StatusType.Enabled && r.IBTType == (int)Identifier.IBTType.Country && r.UserId == userid).ToList();

            list.ForEach(c => {
                var ibt = ibtList.FirstOrDefault(i => i.CountryId == c.CountryId);
                if (ibt != null)
                    c.IbtCount = ibt.IbtCount;

                var rrwb = rrwbList.FirstOrDefault(r => r.ObjId == c.CountryId);
                if (rrwb != null) {
                    c.Rating = rrwb.Rating;
                    c.Recommended = rrwb.Recommended;
                    c.WanttoGo = rrwb.WanttoGo;
                    c.BucketList = rrwb.BucketList;
                }
            });

            return list;
        }
        #endregion

        #region City
        public IList<CityListItem> GetCitySearchList(int topN, int userId, string baseSearch)
        {
            string[] strspl = baseSearch.Trim().Split(' ');
            string strq = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR dbo.Cities.CityName LIKE '%" + item.Trim() + "%' ";
            }

            strq = string.IsNullOrEmpty(baseSearch) ? " or 1=1 " : strq;

            string sql = @"SELECT TOP " + topN.ToString() + @"   
	                          dbo.Cities.CityId,dbo.Cities.CityName, 
                              dbo.Countries.CountryId,dbo.Countries.CountryName,
                              dbo.Countries.FlagSrc,
                              ISNULL(RRWB.Rating,0) AS Rating ,
                              ISNULL(RRWB.Recommended,0) AS Recommended ,
                              ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
                              ISNULL(RRWB.BucketList,0) AS BucketList ,
                              ISNULL(dbo.IBT.IbtCount,0) AS IbtCount
                                                    FROM  dbo.Cities 
                                                    LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
                        LEFT JOIN dbo.IBT ON dbo.Cities.CityId = dbo.IBT.CityId  AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() + @"
                        LEFT JOIN dbo.RRWB ON dbo.Cities.CityId = dbo.RRWB.ObjId and RRWB.IBTType=2 AND RRWB.UserId=" + userId.ToString() + @"
                            WHERE 1=2 " + strq;

            return _db.Database.SqlQuery<CityListItem>(sql).ToList<CityListItem>();

            /*
            string sql = @"SELECT TOP " + topN.ToString() + @"   
	  dbo.Cities.CityId,dbo.Cities.CityName, 
      dbo.Countries.CountryId,dbo.Countries.CountryName,
      ISNULL(RRWB.Rating,0) AS Rating ,
      ISNULL(RRWB.Recommended,0) AS Recommended ,
      ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
      ISNULL(RRWB.BucketList,0) AS BucketList ,
      ISNULL(dbo.IBT.IbtCount,0) AS IbtCount
                            FROM  dbo.Cities 
                            LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
LEFT JOIN dbo.IBT ON dbo.Cities.CityId = dbo.IBT.CityId  AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() + @"
LEFT JOIN dbo.RRWB ON dbo.Cities.CityId = dbo.RRWB.ObjId and RRWB.IBTType=2 AND RRWB.UserId=" + userId.ToString() + @"
                            WHERE  dbo.Cities.CityName LIKE '%" + baseSearch.ToString() + @"%'";
            return _db.Database.SqlQuery<CityListItem>(sql).ToList<CityListItem>();
             * */
        }

        public int GetCityfilterCount(int userId, string filterLetter, string keyWord)
        {
            string[] strspl = keyWord.Trim().Split(' ');
            string strq = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR dbo.Cities.CityName LIKE '%" + item + "%'  ";
            }

            strq = string.IsNullOrEmpty(keyWord) ? " OR 1=1 " : strq;

            string sql = @"SELECT count(*) FROM  dbo.Cities 
                                                    LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
                        LEFT JOIN dbo.IBT ON dbo.Cities.CityId = dbo.IBT.CityId  AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() + " LEFT JOIN dbo.RRWB ON dbo.Cities.CityId = dbo.RRWB.ObjId and RRWB.IBTType=2 AND RRWB.UserId=" + userId.ToString() + " WHERE (1=2 " + strq + ") AND dbo.Cities.CityName LIKE '" + filterLetter + "%' ";
            var q_count = _db.Database.SqlQuery<int>(sql).ToList();
            return q_count[0];

            /*
            string sql = @"SELECT      
                  dbo.Cities.CityId,dbo.Cities.CityName, 
                   ISNULL(dbo.Countries.CountryId,0) AS CountryId,
                  ISNULL(dbo.Countries.CountryName,'''') AS CountryName,
                  ISNULL(RRWB.Rating,0) AS Rating ,
                  ISNULL(RRWB.Recommended,0) AS Recommended ,
                  ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
                  ISNULL(RRWB.BucketList,0) AS BucketList ,
                  ISNULL(dbo.IBT.IbtCount,0) AS IbtCount
                                        FROM  dbo.Cities 
                                        LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
            LEFT JOIN dbo.IBT ON dbo.Cities.CityId = dbo.IBT.CityId  AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() + @"
            LEFT JOIN dbo.RRWB ON dbo.Cities.CityId = dbo.RRWB.ObjId and RRWB.IBTType=2 AND RRWB.UserId=" + userId.ToString() +
            @"WHERE  dbo.Cities.CityName LIKE '%" + keyWord.ToString() + @"%' AND dbo.Cities.CityName LIKE '" + filterLetter.ToString() + @"%'";
            return _db.Database.SqlQuery<CityListItem>(sql).ToList<CityListItem>().Count();
             * */
        }

        /// <summary>
        /// City Result Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<CityListItem> GetCityResultList(int userId, int page, int pageSize, string filterLetter, string filterKeyword, out int total)
        {

            IList<CityListItem> list = _db.Database.SqlQuery<CityListItem>("EXEC IBT_CityBaseSearch {0},{1},{2},{3},{4}", userId, filterLetter, filterKeyword, page, pageSize).ToList<CityListItem>();
            total = GetCityfilterCount(userId, filterLetter, filterKeyword);
            return (from c in list select c).ToList<CityListItem>();
            //todo 下面还需要返回好友数
        }

        /// <summary>
        /// get cityListItem from search listing
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="index"></param>
        /// <param name="pageSize"></param>
        /// <param name="filter"></param>
        /// <param name="keyWork"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public List<CityListItem> GetCitySearchListing(int userid, int index, int pageSize, string filter, string keyWork, out int total)
        {
            List<CityListItem> list = new List<CityListItem>();

            int start = (index - 1) * pageSize;

            IQueryable<CityListItem> query = _db.Cities.Where(c => c.StatusId == (int)Identifier.StatusType.Enabled).Join(_db.Countries, c => c.CountryId, cc => cc.CountryId, (c, cc) => new CityListItem()
            {
                CityId = c.CityId,
                CityName = c.CityName,
                CountryId = c.CountryId,
                CountryName = cc.CountryName,
                FlagSrc = cc.FlagSrc
            }).OrderBy(c => c.CityName);

            if (!string.IsNullOrEmpty(filter))
                query = query.Where(c => c.CityName.StartsWith(filter));

            if (!string.IsNullOrEmpty(keyWork))
            {
                string[] strWhere = keyWork.Split(' ');
                strWhere.ForEach(s =>
                {
                    query = query.Where(c => c.CityName.Contains(s));
                });
            }

            total = query.Count();
            list = query.Skip(start).Take(pageSize).ToList();

            List<DAL.IBT> ibtList = _db.IBTs.Where(i => i.StatusId == (int)Identifier.StatusType.Enabled && i.UserId == userid && i.SupplyId == 0 && i.CityId > 0).ToList();
            List<RRWB> rrwbList = _db.RRWBs.Where(r => r.StatusId == (int)Identifier.StatusType.Enabled && r.IBTType == (int)Identifier.IBTType.City && r.UserId == userid).ToList();

            list.ForEach(c => {
                var ibt = ibtList.FirstOrDefault(i => i.CityId == c.CityId);
                if (ibt != null)
                    c.IbtCount = ibt.IbtCount;

                var rrwb = rrwbList.FirstOrDefault(r => r.ObjId == c.CityId);
                if (rrwb != null) 
                {
                    c.Rating = rrwb.Rating;
                    c.Recommended = rrwb.Recommended;
                    c.WanttoGo = rrwb.WanttoGo;
                    c.BucketList = rrwb.BucketList;
                }
            });

            return list;
        }

        #endregion

        #region Advanced Search

        #region Country
        public int GetCountryAdvancedSearchCount(int userId, string strWhere, string filterLetter)
        {
            string sql = @"SELECT      
      dbo.Countries.CountryId,dbo.Countries.CountryName, 
      ISNULL(RRWB.Rating,0) AS Rating ,
      ISNULL(RRWB.Recommended,0) AS Recommended ,
      ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
      ISNULL(RRWB.BucketList,0) AS BucketList ,
      ISNULL(dbo.IBT.IbtCount,0) AS IbtCount    
                FROM  dbo.Countries 
                            LEFT JOIN dbo.IBT ON dbo.Countries.CountryId = dbo.IBT.CountryId AND  dbo.IBT.CityId=0 AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() +
              @"LEFT JOIN dbo.RRWB ON dbo.Countries.CountryId = dbo.RRWB.ObjId and RRWB.IBTType=1 AND RRWB.UserId=" + userId.ToString() +
                            @"WHERE 1=1 " + strWhere.ToString() + @" AND dbo.Countries.CountryName LIKE '" + filterLetter.ToString() + @"%' AND dbo.Cities.StatusId=1";
            return _db.Database.SqlQuery<CountryListItem>(sql).ToList<CountryListItem>().Count;
        }
        #endregion

        #region City(Cities)
        public int GetCityAdvancedSearchCount(int userId, string strWhere, string filterLetter)
        {
            string sql = @"SELECT    
	  dbo.Cities.CityId,dbo.Cities.CityName, 
      dbo.Countries.CountryId,dbo.Countries.CountryName,
      ISNULL(RRWB.Rating,0) AS Rating ,
      ISNULL(RRWB.Recommended,0) AS Recommended ,
      ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
      ISNULL(RRWB.BucketList,0) AS BucketList ,
      ISNULL(dbo.IBT.IbtCount,0) AS IbtCount
                            FROM  dbo.Cities 
                            LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
LEFT JOIN dbo.IBT ON dbo.Cities.CityId = dbo.IBT.CityId  AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() + @"
LEFT JOIN dbo.RRWB ON dbo.Cities.CityId = dbo.RRWB.ObjId and RRWB.IBTType=2 AND RRWB.UserId=" + userId.ToString() + @"
                            WHERE 1=1 " + strWhere.ToString() + @" AND dbo.Cities.CityName LIKE '" + filterLetter.ToString() + @"%' AND dbo.Cities.StatusId=1";
            return _db.Database.SqlQuery<CityListItem>(sql).ToList<CityListItem>().Count;
        }
        public IList<CityListItem> GetCityAdvancedSearch(int userId, string strWhere, string filterLetter, int page, int pageSize, out int total)
        {
            string sql = @"SELECT    
	  dbo.Cities.CityId,dbo.Cities.CityName, 
      dbo.Countries.CountryId,dbo.Countries.CountryName,dbo.Countries.FlagSrc,
      ISNULL(RRWB.Rating,0) AS Rating ,
      ISNULL(RRWB.Recommended,0) AS Recommended ,
      ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
      ISNULL(RRWB.BucketList,0) AS BucketList ,
      ISNULL(dbo.IBT.IbtCount,0) AS IbtCount
                            FROM  dbo.Cities 
                            LEFT JOIN dbo.Countries ON dbo.Countries.CountryId = dbo.Cities.CountryId 
LEFT JOIN dbo.IBT ON dbo.Cities.CityId = dbo.IBT.CityId  AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() + @"
LEFT JOIN dbo.RRWB ON dbo.Cities.CityId = dbo.RRWB.ObjId and RRWB.IBTType=2 AND RRWB.UserId=" + userId.ToString() + @"
                            WHERE 1=1 " + strWhere.ToString() + @" AND dbo.Cities.CityName LIKE '" + filterLetter.ToString() + @"%'";
            IList<CityListItem> list = _db.Database.SqlQuery<CityListItem>(sql).ToList<CityListItem>();
            total = GetCityAdvancedSearchCount(userId, strWhere, filterLetter);
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.CityName ascending select c).Skip(startRecord).Take(pageSize).ToList<CityListItem>();

        }
        /// <summary>
        /// City Search Lists 的数据(未用)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="strWhere"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<CityListItem> GetCitySearchtList(int userId, int page, int pageSize, string filterLetter, string strWhere, out int total)
        {

            IList<CityListItem> list = _db.Database.SqlQuery<CityListItem>("EXEC IBT_CityAdvancedSearch {0},{1},{2},{3},{4},{5}", userId, strWhere, filterLetter, page, pageSize, DBNull.Value).ToList<CityListItem>();
            total = GetCityAdvancedSearchCount(userId, strWhere, filterLetter);
            return list;
            //todo 下面还需要返回好友数
        }
        #endregion

        #region Suppliers
        /// <summary>
        /// GetAttributeDataList
        /// </summary>
        /// <returns></returns>
        public IList<AttributeData> GetAttributeDataList()
        {
            return (from c in _db.AttributeDatas where c.ParentId != 0 orderby c.AttrDataId ascending select c).ToList<AttributeData>();
        }
        public Dictionary<string, string> GetAttributeDataItem()
        {
            return (from c in _db.AttributeDatas
                    where c.ParentId != 0
                    orderby c.AttrDataId ascending
                    select new { AttrDataId = c.AttrDataId, AttrDataName = c.AttrDataName }).ToDictionary(kvp => kvp.AttrDataId.ToString(), kvp => kvp.AttrDataName);
        }

        public IList<SupplyListShort> GetSupplyAdvancedSearch(int userId, string strWhere, int page, int pageSize, out int total)
        {   //优化修改
            string sql = "";

            sql = @"SELECT  ROW_NUMBER() OVER (ORDER BY sp.Score DESC, sp.SupplyId ASC) AS RowId,
				sp.SupplyId,sp.Title,sp.SupplierTypes,sp.ServiceType,sp.Score,sp.Desp,
                sp.CoverPhotoSrc,sp.StatusId,sp.InfoAddress,sp.InfoState,sp.CreateDate,sp.UpdateDate,sp.CountryId,cn.CountryName,sp.CityId,cy.CityName,
      ISNULL(rw.Rating,0) AS Rating ,
      ISNULL(rw.Recommended,0) AS Recommended ,
      ISNULL(rw.WanttoGo,0) AS WanttoGo ,
      ISNULL(rw.BucketList,0) AS BucketList ,
      ISNULL(bt.IbtCount,0) AS IbtCount                            
                   FROM  dbo.Supplies sp
                            LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId 
                            LEFT JOIN dbo.IBT bt ON sp.CountryId = bt.CountryId AND sp.CityId = bt.CityId  AND bt.SupplyId=sp.SupplyId and bt.UserId=" + userId.ToString() + @"
                            LEFT JOIN dbo.RRWB rw ON sp.SupplyId = rw.ObjId and rw.IBTType=3 and rw.UserId=" + userId.ToString() + @"
                            LEFT JOIN SupplyAttributeData sa ON sp.SupplyId = sa.SupplyId
                            LEFT JOIN AttributeData ad ON sa.ArrtDataId = ad.AttrDataId 
                            WHERE 1=1 AND sp.StatusId=1 " + strWhere.ToString();

            IList<SupplyListShort> list = _db.Database.SqlQuery<SupplyListShort>(sql).ToList<SupplyListShort>();
            total = GetSupplyAdvancedSearchCount(userId, strWhere);
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.Title ascending select c).Skip(startRecord).Take(pageSize).ToList<SupplyListShort>();

        }

        #region suarez 优化查询20131028
        public List<SupplyListShort> GetSupplyAdvancedSearch(int userId, int countryId, int cityId, int sortby, string filterLetter, string cateIds, int page, int pageSize, out int total)
        {
            List<SupplyListShort> list = new List<SupplyListShort>();
            int startRecord = (page - 1) * pageSize;
            total = 0;

            string strWhere = "";
            string strOrderBy = " ORDER BY rw.Rating DESC,sp.Title ASC";

            if (countryId > 0)
            {
                strWhere += " AND sp.CountryId=" + countryId.ToString();
            }
            if (cityId > 0)
            {
                strWhere += " AND sp.CityId=" + cityId.ToString();
            }
            if (!string.IsNullOrEmpty(filterLetter))
            {
                strWhere += " AND sp.Title like '" + filterLetter + @"%'";
            }

            if (sortby == 1)
                strOrderBy = " ORDER BY rw.Recommended DESC,sp.Title ASC";
            else if (sortby == 2)
                strOrderBy = " ORDER BY bt.IbtCount DESC,sp.Title ASC";

            if (string.IsNullOrEmpty(cateIds))
            {
                string sql = @"SELECT TOP " + (page * pageSize).ToString() + @" 
                                sp.SupplyId,sp.Title,sp.SupplierTypes,sp.ServiceType,sp.Score,sp.Desp,
                                sp.CoverPhotoSrc,sp.StatusId,sp.InfoAddress,sp.InfoState,sp.CreateDate,sp.UpdateDate,sp.CountryId,cn.CountryName,sp.CityId,cy.CityName,
                                ISNULL(rw.Rating,0) AS Rating ,
                                ISNULL(rw.Recommended,0) AS Recommended ,
                                ISNULL(rw.WanttoGo,0) AS WanttoGo ,
                                ISNULL(rw.BucketList,0) AS BucketList ,
                                ISNULL(bt.IbtCount,0) AS IbtCount
                                FROM  dbo.Supplies sp
                                LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId 
                                LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId 
                                LEFT JOIN dbo.IBT bt ON sp.CountryId = bt.CountryId AND sp.CityId = bt.CityId  AND bt.SupplyId=sp.SupplyId and bt.UserId={0}
                                LEFT JOIN dbo.RRWB rw ON sp.SupplyId = rw.ObjId and rw.IBTType=3 and rw.UserId={0}
                                WHERE 1=1 AND sp.StatusId=1" + strWhere + strOrderBy;

                string sqlCount = @"SELECT COUNT(*) FROM Supplies sp WHERE sp.StatusId=1 " + strWhere;

                list = _db.Database.SqlQuery<SupplyListShort>(sql, userId.ToString()).ToList();
                list = list.Skip(startRecord).Take(pageSize).ToList();

                total = _db.Database.SqlQuery<int>(sqlCount).FirstOrDefault<int>();
            }
            else
            {
                string sql = @"WITH aTable AS
                                (
                                SELECT DISTINCT SupplyId
                                FROM dbo.SupplyAttributeData
                                WHERE ArrtDataId IN(" + cateIds + @")
                                ),
                                sp AS
                                (
                                SELECT dbo.Supplies.* FROM aTable LEFT JOIN dbo.Supplies ON aTable.SupplyId = dbo.Supplies.SupplyId
                                WHERE dbo.Supplies.StatusId=1
                                )
                                SELECT TOP " + (page * pageSize).ToString() + @"
                                sp.SupplyId,sp.Title,sp.SupplierTypes,sp.ServiceType,sp.Score,sp.Desp,
                                sp.CoverPhotoSrc,sp.StatusId,sp.InfoAddress,sp.InfoState,sp.CreateDate,sp.UpdateDate,sp.CountryId,cn.CountryName,sp.CityId,cy.CityName,
                                ISNULL(rw.Rating,0) AS Rating ,
                                ISNULL(rw.Recommended,0) AS Recommended ,
                                ISNULL(rw.WanttoGo,0) AS WanttoGo ,
                                ISNULL(rw.BucketList,0) AS BucketList ,
                                ISNULL(bt.IbtCount,0) AS IbtCount
                                FROM sp
                                LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId 
                                LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId 
                                LEFT JOIN dbo.IBT bt ON sp.CountryId = bt.CountryId AND sp.CityId = bt.CityId  AND bt.SupplyId=sp.SupplyId and bt.UserId={0}
                                LEFT JOIN dbo.RRWB rw ON sp.SupplyId = rw.ObjId and rw.IBTType=3 and rw.UserId={0}
                                WHERE sp.StatusId=1 " + strWhere + strOrderBy;

                string sqlCount = @"WITH aTable AS
                                    (
                                    SELECT DISTINCT SupplyId
                                    FROM dbo.SupplyAttributeData
                                    WHERE ArrtDataId IN(" + cateIds + @")
                                    )
                                    SELECT COUNT(aTable.SupplyId) FROM aTable LEFT JOIN dbo.Supplies sp ON aTable.SupplyId = sp.SupplyId WHERE StatusId=1 " + strWhere;

                list = _db.Database.SqlQuery<SupplyListShort>(sql, userId.ToString()).ToList();
                list = list.Skip(startRecord).Take(pageSize).ToList();

                total = _db.Database.SqlQuery<int>(sqlCount).FirstOrDefault<int>();
            }

            return list;
        }
        #endregion

        /*20130205裕冲修改:在存储过程中分页后得到结果*/
        public IList<SupplyListShort> GetSupplyAdvancedSearchByPage(int userId, string strWhere, string strOderby, int page, int pageSize, out int total)
        {
            IList<SupplyListShort> list = _db.Database.SqlQuery<SupplyListShort>("EXEC IBT_SupplyAdvancedSearch {0},{1},{2},{3},{4}", userId, page, pageSize, strWhere.ToString(), strOderby.ToString()).ToList<SupplyListShort>();
            total = GetSupplyAdvancedSearchCount(userId, strWhere);
            total = list.Count();
            return list;
        }
        public IList<SupplyListShort> GetRecommendAdvancedSearchByPage(int userId, string strWhere, string strOderby, int page, int pageSize, out int total)
        {
            IList<SupplyListShort> list = _db.Database.SqlQuery<SupplyListShort>("EXEC IBT_RecommendSearch {0},{1},{2},{3},{4}", userId, page, pageSize, strWhere.ToString(), strOderby.ToString()).ToList<SupplyListShort>();
            total = list.Count();
            return list;
        }
        /*修改结束*/
        public int GetSupplyAdvancedSearchCount(int userId, string strWhere)
        {
            if (strWhere.ToLower().IndexOf("order by") > 0)
            {
                strWhere = strWhere.Substring(0, strWhere.ToLower().LastIndexOf("order by")).ToString();
            }
            string sql = "";
            sql = @"SELECT Count(*)                           
                   FROM  dbo.Supplies sp
                            LEFT JOIN  dbo.Countries cn ON sp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON sp.CityId = cy.CityId 
                            LEFT JOIN dbo.IBT bt ON sp.CountryId = bt.CountryId AND sp.CityId = bt.CityId  AND bt.SupplyId=sp.SupplyId and bt.UserId=" + userId.ToString() + @"
                            LEFT JOIN dbo.RRWB rw ON sp.SupplyId = rw.ObjId and rw.IBTType=3 and rw.UserId=" + userId.ToString() + @"
                            LEFT JOIN SupplyAttributeData sa ON sp.SupplyId = sa.SupplyId
                            LEFT JOIN AttributeData ad ON sa.ArrtDataId = ad.AttrDataId 
                            WHERE 1=1 AND sp.StatusId=1 " + strWhere.ToString();
            //strWhere.Substring(0, strWhere.ToLower().LastIndexOf("order by")).ToString();

            return _db.Database.SqlQuery<int>(sql).FirstOrDefault();

        }
        #endregion

        #region member
        public int GetMemberAdvancedSearchCount(string strWhere)
        {
            string sql = @"SELECT 
                ROW_NUMBER() OVER (ORDER BY mp.Sort DESC, mp.UserId ASC) AS RowId,
				mp.UserId,mp.Username,mp.Birth,mp.FirstName,mp.LastName,mp.Email,mp.AlternateEmail,mp.Gender,mp.Location,mp.ProfileImage,
				mp.CurrentLoginSnsId,mp.Sort,mp.CountryId,cn.CountryName,mp.CityId,cy.CityName
                            FROM  dbo.Profiles mp
                            LEFT JOIN  dbo.Users u ON u.UserId=mp.UserId 
                            LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId
                            WHERE 1=1 " + strWhere.ToString();
            return _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>().Count;
        }
        public IList<MemberListItem> GetMemberAdvancedSearch(string strWhere, int page, int pageSize, out int total)
        {
            string sql = @"SELECT 
                ROW_NUMBER() OVER (ORDER BY mp.FirstName ASC) AS RowId,
				mp.UserId,mp.Username,mp.Birth,mp.FirstName,mp.LastName,mp.Email,mp.AlternateEmail,mp.Gender,mp.Location,mp.ProfileImage,
				mp.CurrentLoginSnsId,mp.Sort,mp.CountryId,cn.CountryName,mp.CityId,cy.CityName
                            FROM  dbo.Profiles mp
                            LEFT JOIN  dbo.Users u ON u.UserId=mp.UserId 
                            LEFT JOIN  dbo.Countries cn ON mp.CountryId = cn.CountryId 
                            LEFT JOIN  dbo.Cities cy ON mp.CityId = cy.CityId
                            WHERE 1=1 " + strWhere.ToString();
            IList<MemberListItem> list = _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>();
            total = GetMemberAdvancedSearchCount(strWhere);
            int startRecord = (page - 1) * pageSize;
            return (from c in list orderby c.FirstName ascending select c).Skip(startRecord).Take(pageSize).ToList<MemberListItem>();

        }
        #endregion

        #endregion

        #region Browse by Country
        /// <summary>
        /// Browse Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<CountryListItem> GetBrowseList(int userId, int page, int pageSize, string strWhere, string filterLetter, string filterKeyword, out int total)
        {

            IList<CountryListItem> list = _db.Database.SqlQuery<CountryListItem>("EXEC IBT_BrowseList {0},{1},{2},{3},{4},{5}", userId, strWhere, filterLetter, filterKeyword, page, pageSize).ToList<CountryListItem>();
            total = GetCountryfilterCount(userId, strWhere, filterLetter, filterKeyword);
            //total = GetCountryAdvancedSearchCount(userId, filterLetter, filterKeyword);
            //return (from c in list orderby c.CountryName ascending select c).ToList<CountryListItem>();
            return list.ToList<CountryListItem>();

            //todo 下面还需要返回好友数
        }
        public int GetCountryfilterCount(int userId, string strWhere, string filterLetter, string keyWord)
        {
            string[] strspl = keyWord.Trim().Split(' ');
            string strq = string.Empty;
            foreach (var item in strspl)
            {
                strq = strq + " OR dbo.Countries.CountryName LIKE '%" + item.Trim() + "%' ";
            }

            strq = string.IsNullOrEmpty(keyWord) ? " or 1=1 " : strq;
            string sql = @"SELECT      
      dbo.Countries.CountryId,dbo.Countries.CountryName, 
      ISNULL(RRWB.Rating,0) AS Rating ,
      ISNULL(RRWB.Recommended,0) AS Recommended ,
      ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
      ISNULL(RRWB.BucketList,0) AS BucketList ,
      ISNULL(dbo.IBT.IbtCount,0) AS IbtCount    
                FROM  dbo.Countries 
                LEFT JOIN dbo.IBT ON dbo.Countries.CountryId = dbo.IBT.CountryId AND  dbo.IBT.CityId=0 AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() +
              @"LEFT JOIN dbo.RRWB ON dbo.Countries.CountryId = dbo.RRWB.ObjId and RRWB.IBTType=1 AND RRWB.UserId=" + userId.ToString() +
                            @"WHERE (1=2 " + strq + ") AND dbo.Countries.CountryName LIKE '" + filterLetter.ToString() + "%' " + strWhere;

            return _db.Database.SqlQuery<CountryListItem>(sql).ToList<CountryListItem>().Count();

            /*
            string sql = @"SELECT      
      dbo.Countries.CountryId,dbo.Countries.CountryName, 
      ISNULL(RRWB.Rating,0) AS Rating ,
      ISNULL(RRWB.Recommended,0) AS Recommended ,
      ISNULL(RRWB.WanttoGo,0) AS WanttoGo ,
      ISNULL(RRWB.BucketList,0) AS BucketList ,
      ISNULL(dbo.IBT.IbtCount,0) AS IbtCount    
                FROM  dbo.Countries 
                LEFT JOIN dbo.IBT ON dbo.Countries.CountryId = dbo.IBT.CountryId AND  dbo.IBT.CityId=0 AND dbo.IBT.SupplyId=0 AND dbo.IBT.UserId=" + userId.ToString() +
              @"LEFT JOIN dbo.RRWB ON dbo.Countries.CountryId = dbo.RRWB.ObjId and RRWB.IBTType=1 AND RRWB.UserId=" + userId.ToString() +
                            @"WHERE 1=1 " + strWhere.ToString() + @" AND dbo.Countries.CountryName LIKE '%" + keyWord.ToString() + @"%' AND dbo.Countries.CountryName LIKE '" + filterLetter.ToString() + @"%'";
            return _db.Database.SqlQuery<CountryListItem>(sql).ToList<CountryListItem>().Count();
             * */
        }
        #endregion

        #endregion
    }
}
