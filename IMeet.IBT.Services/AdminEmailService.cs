﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using StructureMap;
using IMeet.IBT.DAL;
using IMeet.IBT.DAL.Models;
using IMeet.IBT.Common;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web;
using System.IO;

namespace IMeet.IBT.Services
{
    /// <summary>
    /// AdminEmailService
    /// </summary>
    public class AdminEmailService : BaseService, IAdminEmailService
    {
        private IProfileService _profileService;
        private ITravelBadgeService _travelBadgeService;

        public AdminEmailService()
        {
            _profileService = ObjectFactory.GetInstance<IProfileService>();
            _travelBadgeService = ObjectFactory.GetInstance<ITravelBadgeService>();
        }

        /// <summary>
        /// 添加新Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Email Add(Email email)
        {
            email.StatusId = (int)Identifier.StatusType.Enabled;
            email.CreateDate = DateTime.Now;

            _db.Emails.Add(email);
            _db.SaveChanges();
            return email;
        }
        /// <summary>
        /// 更改email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Email Update(Email email, int id)
        {
            Email item = _db.Emails.Find(id);
            if (item != null)
            {
                item.SenderName = email.SenderName;
                item.SenderEmail = email.SenderEmail;
                item.Title = email.Title;
                item.SubTitle = email.SubTitle;
                item.Logo = email.Logo;
                item.Subject = email.Subject;
                item.Recipients = email.Recipients;
                item.RecipientsEmail = email.RecipientsEmail;
                item.Content = email.Content;
                item.ScheduleTime = email.ScheduleTime;
                item.UserId = email.UserId;
                item.SendStatus = email.SendStatus;
                item.ShowMPB = email.ShowMPB;

                if (email.ScheduleStatus != null)
                {
                    item.ScheduleStatus = email.ScheduleStatus;
                    item.ScheduleTime = email.ScheduleTime;
                }

                _db.SaveChanges();
            }
            return item;
        }
        /// <summary>
        /// 根据id获取email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Email Find(int id)
        {
            return _db.Emails.Find(id);
        }
        /// <summary>
        /// 删除email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Del(int id)
        {
            Email email = _db.Emails.Find(id);
            email.StatusId = (int)Identifier.StatusType.Disabled;
            _db.SaveChanges();
            return true;
        }
        /// <summary>
        /// 获取email列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<Email> GetEmailList(int page, int pageSize, out int total)
        {
            List<Email> list = new List<Email>();
            int start = (page - 1) * pageSize;

            list = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.email).OrderByDescending(e => e.EmailId).Skip(start).Take(pageSize).ToList();
            total = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.email).Count();

            return list;
        }
        /// <summary>
        /// New-Email
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public List<Email> GetNewEmailList(int page, int pageSize, out int total)//EDM
        {
            List<Email> list = new List<Email>();
            int start = (page - 1) * pageSize;

            list = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.newemail).OrderByDescending(e => e.EmailId).Skip(start).Take(pageSize).ToList();
            total = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.newemail).Count();

            return list;
        }
        
        public List<Email> GetSurveyList(int page, int pageSize, out int total)
        {
            List<Email> list = new List<Email>();
            int start = (page - 1) * pageSize;

            list = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.survey).OrderByDescending(e => e.EmailId).Skip(start).Take(pageSize).ToList();
            total = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.survey).Count();

            return list;
        }
        /// <summary>
        /// Get New Surveylist
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public List<Email> GetNewSurveyList(int page, int pageSize, out int total)
        {
            List<Email> list = new List<Email>();
            int start = (page - 1) * pageSize;

            list = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.newsurvey).OrderByDescending(e => e.EmailId).Skip(start).Take(pageSize).ToList();
            total = _db.Emails.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.TypeId == (int)Identifier.EmailType.newsurvey).Count();

            return list;
        }

        /// <summary>
        /// add email sendlog
        /// </summary>
        /// <param name="emailId"></param>
        public bool AddEmailSendLog(int emailId)
        {
            try
            {
                Email email = _db.Emails.Find(emailId);

                if (email != null)
                {
                    DateTime scheduleTime = email.ScheduleTime == null ? DateTime.Now : email.ScheduleTime.Value;
                    List<User> userList = null;

                    switch (email.Recipients)
                    {
                        case (int)Identifier.EmailRecipients.AllMembers:
                            userList = _db.Users.Where(u => u.StatusId == 0).ToList();
                            break;
                        case (int)Identifier.EmailRecipients.BadgesMembers:
                            userList = _db.Users.Where(u => u.StatusId == 0 && u.IsActivate == (int)Identifier.IsActivate.True).ToList();
                            break;
                        case (int)Identifier.EmailRecipients.NotActivatedMembers:
                            userList = _db.Users.Where(u => u.StatusId == 0 && u.IsActivate == (int)Identifier.IsActivate.False).ToList();
                            break;
                        case (int)Identifier.EmailRecipients.Custom:
                            if (string.IsNullOrEmpty(email.RecipientsEmail))
                            {
                                List<string> emailList = email.RecipientsEmail.Split(',').ToList();
                                emailList.ForEach(e =>
                                {
                                    EmailSendLog sendLog = new EmailSendLog();
                                    sendLog.ToEmail = e;
                                    sendLog.EmailId = email.EmailId;
                                    sendLog.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                                    sendLog.ScheduleStatus = email.ScheduleStatus.Value;
                                    sendLog.ScheduleTime = scheduleTime;
                                    sendLog.StatusId = (int)Identifier.StatusType.Enabled;
                                    sendLog.CreateDate = DateTime.Now;

                                    _db.EmailSendLogs.Add(sendLog);
                                });

                                _db.SaveChanges();
                            }
                            break;
                        default:
                            break;
                    }

                    if (userList != null)
                    {
                        userList.ForEach(u =>
                        {
                            EmailSendLog sendLog = new EmailSendLog();
                            sendLog.ToEmail = u.Email;
                            sendLog.EmailId = email.EmailId;
                            sendLog.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                            sendLog.ScheduleStatus = email.ScheduleStatus.Value;
                            sendLog.ScheduleTime = scheduleTime;
                            sendLog.ScheduleStatus = (int)Identifier.StatusType.Enabled;
                            sendLog.CreateDate = DateTime.Now;

                            _db.EmailSendLogs.Add(sendLog);
                        });

                        _db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// 开启多线程添加EmailSendLog
        /// </summary>
        /// <param name="emailId"></param>
        public void AddEmailSendLogTreading(int emailId)
        {
            Thread t = new Thread(AdminEmailService.AddEmailSendLogProc);
            t.Start(emailId);
        }

        /// <summary>
        /// 用于开启多线程加email send log
        /// </summary>
        /// <param name="id"></param>
        public static void AddEmailSendLogProc(object id)
        {
            int emailId = (int)id;

            ThreadingService _threadingService = new ThreadingService();
            _threadingService.AddEmailSendLog(emailId);
        }

        /// <summary>
        /// 邮件发送
        /// </summary>
        public void SendEmail(int digit)
        {
            IBT.Common.Email.EmailService _emailService = new Common.Email.EmailService();

            List<EmailSendLog> list = _db.EmailSendLogs.Where(e => e.StatusId == (int)Identifier.StatusType.Enabled && e.ScheduleTime <= DateTime.Now && e.SendStatus == (int)Identifier.EmailSendStatus.HaventSent && !String.IsNullOrEmpty(e.ToEmail)).Take(4000).ToList();
            //WriteLog("listCount before digit removal:" + list.Count.ToString());

            //error in here
            list.RemoveAll(e => getMostRightDigit(e.EmailSendLogId) != digit);
            //WriteLog("list Count after digit removal:" + list.Count.ToString());


            Console.WriteLine("Thread No:" + digit + ", sending " + list.Count + " emails");
            //WriteLog("Thread No:" + digit + ", sending " + list.Count + " emails")
            foreach (var sendLogs in list)
            {
                //WriteLog("Mail Starts to user :" + sendLogs.ToEmail.ToString()); //TestCode
                User user = _db.Users.Where(u => u.Email == sendLogs.ToEmail && u.IsEmailSubscribe == true &&u.StatusId==0).FirstOrDefault();
                if (sendLogs.Email.SendStatus.HasValue && sendLogs.Email.SendStatus.Value == (int)Identifier.EmailSendStatus.HaventSent)
                {
                    sendLogs.Email.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                    //WriteLog("Send Status Email :" + sendLogs.Email.EmailId + "Send status :" + sendLogs.Email.SendStatus); //TestCode
                    _db.SaveChanges();
                }

                if (user!=null)
                {
                    Console.WriteLine("Thread No:" + digit + ", sending to : " + user.Email.ToString());
                    //WriteLog("Thread No:" + digit + ", sending to " + user.Email.ToString());

                    UserTravelBadge utb = _travelBadgeService.GetUserTravelbadge(user);
                    string userName = user.Profile.Firstname;
                    string userPic = this.GetUserAvatar(user.UserId, 100, user.Profile.ProfileImage);

                    //加密
                    string key = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt(user.UserId.ToString()));

                    //sedning email campagine

                    switch (sendLogs.Email.TypeId)
                    {
                        case (int)Identifier.EmailType.email:

                            if (user.IsActivate == (int)Identifier.IsActivate.True)
                            {
                                _emailService.SendActiveMemberEmail(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), sendLogs.Email.Content, key, utb.TotalPoints.ToString(), sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                                //WriteLog("SendActiveMemberEmail : " + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }
                            else if (user.IsActivate == (int)Identifier.IsActivate.False)
                            {
                                _emailService.SendNotActiveMemberEmail(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), sendLogs.Email.Content, key, sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                                //WriteLog("SendNotActiveMemberEmail:" + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }
                            sendLogs.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                            sendLogs.SendTime = DateTime.Now;
                            _db.SaveChanges();
                           // WriteLog("Mail Sent to:" + user.Email);

                            break;

                        case (int)Identifier.EmailType.newemail:

                            string emailContent = string.Empty;
                            if (!String.IsNullOrWhiteSpace(sendLogs.Email.Content))
                            {
                                emailContent = sendLogs.Email.Content.Replace(" (Le+|-(((w4+---====r-GQ ", "");
                            }
                            else
                            {
                                emailContent = string.Empty;
                            }
                            if (user.IsActivate == (int)Identifier.IsActivate.True)
                            {
                                _emailService.NewSendActiveMemberEmail(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), emailContent, key, utb.TotalPoints.ToString(), sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                                //WriteLog("NewSendActiveMemberEmail : " + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }
                            else if (user.IsActivate == (int)Identifier.IsActivate.False)
                            {
                                _emailService.NewSendNotActiveMemberEmail(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), emailContent, key, sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                               // WriteLog("NewSendNotActiveMemberEmail:" + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }

                            sendLogs.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                            sendLogs.SendTime = DateTime.Now;
                            _db.SaveChanges();
                            //WriteLog("Mail Sent to:" + user.Email);

                            break;

                        case (int)Identifier.EmailType.survey:

                            string[] SurveyContent = new string[3];
                            if (!String.IsNullOrWhiteSpace(sendLogs.Email.Content))
                            {
                                SurveyContent = sendLogs.Email.Content.Split(new string[] { " (Le+|-(((w4+---====r-GQ " }, StringSplitOptions.None);
                            }
                            else
                            {
                                SurveyContent = new string[] { null, null, null };
                            }

                            var placesList = SugestedList2EmailHtmlTemplate(SurveyContent[2]);

                            if (user.IsActivate == (int)Identifier.IsActivate.True)
                            {
                                _emailService.SendActiveMemberSurvey(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), SurveyContent[0], SurveyContent[1], placesList, key, utb.TotalPoints.ToString(), sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                                //WriteLog("SendActiveMemberSurvey:" + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }
                            else if (user.IsActivate == (int)Identifier.IsActivate.False)
                            {
                                _emailService.SendNotActiveMemberSurvey(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), SurveyContent[0], SurveyContent[1], placesList, key, sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                                //WriteLog("SendNotActiveMemberSurvey:" + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }
                            sendLogs.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                            sendLogs.SendTime = DateTime.Now;
                            _db.SaveChanges();
                            //WriteLog("Mail Sent to:" + user.Email);

                            break;

                        case (int)Identifier.EmailType.newsurvey:

                            string[] newSurveyContent = new string[4];
                            if (!String.IsNullOrWhiteSpace(sendLogs.Email.Content))
                            {
                                newSurveyContent = sendLogs.Email.Content.Split(new string[] { " (Le+|-(((w4+---====r-GQ " }, StringSplitOptions.None);
                            }
                            else
                            {
                                newSurveyContent = new string[] { null, null, null, null };
                            }

                            var newplacesList = NewSugestedList2EmailHtmlTemplate(newSurveyContent[3]);
                            var contentdata = newSurveyContent[0] + newSurveyContent[1];

                            if (user.IsActivate == (int)Identifier.IsActivate.True)
                            {
                                _emailService.NewSendActiveMemberSurvey(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), contentdata, newSurveyContent[2], newplacesList, key, utb.TotalPoints.ToString(), sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                                //WriteLog("NewSendActiveMemberSurvey:" + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }
                            else if (user.IsActivate == (int)Identifier.IsActivate.False)
                            {
                                _emailService.NewSendNotActiveMemberSurvey(sendLogs.Email.SenderName, sendLogs.Email.SenderEmail, user.Email, userName, sendLogs.Email.Subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), contentdata, newSurveyContent[2], newplacesList, key, sendLogs.Email.EmailId.ToString(), sendLogs.Email.Title, sendLogs.Email.Logo, (int)sendLogs.Email.ShowMPB);
                                //WriteLog("NewSendNotActiveMemberSurvey:" + " Sender Name : " + sendLogs.Email.SenderName + " Sender Email : " + sendLogs.Email.SenderEmail + " User Email : " + user.Email + " User Name : " + userName); //TestCode
                            }

                            sendLogs.SendStatus = (int)Identifier.EmailSendStatus.Sent;
                            sendLogs.SendTime = DateTime.Now;
                            _db.SaveChanges();
                            //WriteLog("Mail Sent to:" + user.Email);

                            break;
                    }
                }
                else
                {
                    sendLogs.StatusId = (int)Identifier.StatusType.Disabled;
                    _db.SaveChanges();
                }
            }
        }

       
        private int getMostRightDigit(int number)
        {
            string strNo = number.ToString();
            int digit = int.Parse(strNo.Substring(strNo.Length - 1));
            return digit;
        }

        /// <summary>
        /// get the email content and extract the the place names
        /// </summary>
        /// <param name="SuggestedHtmlList"></param>
        /// <returns></returns>
        public string SugestedList2EmailHtmlTemplate(string SuggestedHtmlList)
        {
            //SuggestedHtmlList
            var rowsArray = SuggestedHtmlList.Split(new string[] { "cmp-edmRatingSlctdWrap" }, StringSplitOptions.None);
            StringBuilder sb = new StringBuilder("");
            foreach (var item in rowsArray)
            {
                //check for wrong data
                if (item.Contains("data-placeid"))
                {
                    var placeId = extractDataVale(item, "data-placeid");
                    var cityId = extractDataVale(item, "data-cityid");
                    var countryId = extractDataVale(item, "data-countryid");
                    sb.Append(getRowHtml(countryId, cityId, placeId));
                }
            }
            return sb.ToString();
        }

        public string NewSugestedList2EmailHtmlTemplate(string SuggestedHtmlList)
        {
            //SuggestedHtmlList
            var rowsArray = SuggestedHtmlList.Split(new string[] { "cmp-edmRatingSlctdWrap" }, StringSplitOptions.None);
            StringBuilder sb = new StringBuilder("");
            foreach (var item in rowsArray)
            {
                //check for wrong data
                if (item.Contains("data-placeid"))
                {
                    var placeId = extractDataVale(item, "data-placeid");
                    var cityId = extractDataVale(item, "data-cityid");
                    var countryId = extractDataVale(item, "data-countryid");
                    sb.Append(newGetRowHtml(countryId, cityId, placeId));
                }
            }
            return sb.ToString();
        }

        private string newGetRowHtml(int CountryId, int CityId, int PlaceId)
        {
            StringBuilder sb = new StringBuilder("");
            if (CountryId > 0 && CityId > 0 && PlaceId > 0)
            {
                var countryObj = _db.Countries.Find(CountryId);
                var cityObj = _db.Cities.Find(CityId);
                var placeObj = _db.Supplies.Find(PlaceId);

                var imgsrcObj = !string.IsNullOrEmpty(placeObj.CoverPhotoSrc) ? placeObj.CoverPhotoSrc+"_bn.jpg" : countryObj.FlagSrc;

                sb.Append(@"<tr height='100px' style='background-color:#ffffff;width:160px;height:auto;padding:10px;display:block;text-decoration:none;outline:none;border:none; float:left;'>");
                sb.Append(@"<td width='150px' style='width:150px;height:60px;padding:0;display:block;text-decoration:none;border:none;outline:none;'><span style='width:auto;height:auto;padding:6px 0 0px 0px;display:block;text-decoration:none;border:none;outline:none;font:11px/12px Arial, Helvetica, sans-serif;text-align:left;color:#1c95c9;'><span>" + countryObj.CountryName + "</span> <br /><span>" + cityObj.CityName + "</span> <br /><span>" + placeObj.Title + "</span> </span></td>");
                sb.Append(@"<td width='150px' style='width:28px;height:auto;padding:0;display:block;text-decoration:none;border:none;outline:none;'><span style='width:150px;height:85px;padding:0 0 6px;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com" + imgsrcObj + "' width='150px' height='auto' alt='" + countryObj.CountryName + "' Flag' style='width:50px;height:50px;margin-left: auto;margin-right: auto;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='130px' style='width:60px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:60px;height:auto;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com/app/images/survey/star.jpg' width='120' height='auto' alt='' style='width:50px;height:auto;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='30px' style='width:30px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;text-align:left;'><span style='width:auto;height:auto;text-decoration:none;border:none;outline:none;'><input type='checkbox' name='option' value='pl" + placeObj.SupplyId.ToString() + "' width='20px' height='20px' style='background-color:#a9a9a9;height:15px;width:15px;padding:0;text-decoration:none;outline:none;border-color:#f7f7f7;' /></span></td></tr>");
            }
            else if (CountryId > 0 && CityId > 0)
            {
                var countryObj = _db.Countries.Find(CountryId);
                var cityObj = _db.Cities.Find(CityId);
                var imgsrcObj = !string.IsNullOrEmpty(cityObj.FlagSrc) ? cityObj.FlagSrc : countryObj.FlagSrc;

                sb.Append(@"<tr height='100px' style='background-color:#ffffff;width:160px;height:auto;padding:10px;display:block;text-decoration:none;outline:none;border:none; float:left;'>");
                sb.Append(@"<td width='150px' style='width:150px;height:60px;padding:0;display:block;text-decoration:none;border:none;outline:none;'><span style='width:auto;height:auto;padding:6px 0 0px 0px;display:block;text-decoration:none;border:none;outline:none;font:11px/12px Arial, Helvetica, sans-serif;text-align:left;color:#1c95c9;'><span>" + countryObj.CountryName + "</span> <br /><span>" + cityObj.CityName + "</span> </span></td>");
                sb.Append(@"<td width='150px' style='width:28px;height:auto;padding:0;display:block;text-decoration:none;border:none;outline:none;'><span style='width:150px;height:85px;padding:0 0 6px;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com" + imgsrcObj + "' width='150px' height='auto' alt='" + countryObj.CountryName + "' Flag' style='width:50px;height:50px;margin-left: auto;margin-right: auto;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='130px' style='width:60px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:60px;height:auto;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com/app/images/survey/star.jpg' width='120' height='auto' alt='' style='width:50px;height:auto;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='30px' style='width:30px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;text-align:left;'><span style='width:auto;height:auto;text-decoration:none;border:none;outline:none;'><input type='checkbox' name='option' value='ci" + cityObj.CityId.ToString() + "' width='20px' height='20px' style='background-color:#a9a9a9;height:15px;width:15px;padding:0;text-decoration:none;outline:none;border-color:#f7f7f7;' /></span></td></tr>");
            }
            else if (CountryId > 0)
            {
                var countryObj = _db.Countries.Find(CountryId);

                sb.Append(@"<tr height='100px' style='background-color:#ffffff;width:160px;height:auto;padding:10px;display:block;text-decoration:none;outline:none;border:none; float:left;'>");
                sb.Append(@"<td width='150px' style='width:150px;height:60px;padding:0;display:block;text-decoration:none;border:none;outline:none;'><span style='width:auto;height:auto;padding:6px 0 0px 0px;display:block;text-decoration:none;border:none;outline:none;font:11px/12px Arial, Helvetica, sans-serif;text-align:left;color:#1c95c9;'><span>" + countryObj.CountryName + "</span>  </span></td>");
                sb.Append(@"<td width='150px' style='width:28px;height:auto;padding:0;display:block;text-decoration:none;border:none;outline:none;'><span style='width:150px;height:85px;padding:0 0 6px;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com" + countryObj.FlagSrc + "' width='150px' height='auto' alt='" + countryObj.CountryName + "' Flag' style='width:50px;height:50px;margin-left: auto;margin-right: auto;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='130px' style='width:60px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:60px;height:auto;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com/app/images/survey/star.jpg' width='120' height='auto' alt='' style='width:50px;height:auto;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='30px' style='width:30px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;text-align:left;'><span style='width:auto;height:auto;text-decoration:none;border:none;outline:none;'><input type='checkbox' name='option' value='co" + countryObj.CountryId.ToString() + "' width='20px' height='20px' style='background-color:#a9a9a9;height:15px;width:15px;padding:0;text-decoration:none;outline:none;border-color:#f7f7f7;' /></span></td></tr>");
            }      
            
            
            return sb.ToString();

        }
        private string getRowHtml(int CountryId, int CityId, int PlaceId)
        {
            StringBuilder sb = new StringBuilder("");
            if (CountryId > 0 && CityId > 0 && PlaceId > 0)
            {
                var countryObj = _db.Countries.Find(CountryId);
                var cityObj = _db.Cities.Find(CityId);
                var placeObj = _db.Supplies.Find(PlaceId);
                //var EncryptedPlaceId = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt("pl"+placeObj.SupplyId.ToString()));
                //placeObj.SupplyId.ToString() 
                //HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt());

                sb.Append(@"<tr height='74px' style='background-color:#efefef;width:570px;height:auto;padding:0 0 0 30px;display:block;text-decoration:none;outline:none;border:none;border-bottom:1px solid #f7f7f7;'>");
                sb.Append(@"<td width='28px' style='width:28px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:28px;height:28px;padding:6px 0;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com" + countryObj.FlagSrc + "' width='26px' height='auto' alt='" + countryObj.CountryName + "' Flag' style='width:26px;height:auto;padding:1px;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='236px' style='width:236px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:auto;height:auto;padding:6px 0 8px 18px;display:block;text-decoration:none;border:none;outline:none;font:14px/16px Arial, Helvetica, sans-serif;text-align:left;color:#a1a1a1;'><span>" + countryObj.CountryName + "</span> <br /><span style='font-weight:bold;'>" + cityObj.CityName + "</span> / City <br /><span style='font-size:13px;line-height:20px;'><span style='font-weight:bold;'>" + placeObj.Title + "</span> / Places</span></span></td>");
                sb.Append(@"<td width='174px' style='width:174px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:134px;height:22px;padding:9px 6px 9px 24px;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com/app/images/survey/dashed_02.jpg' width='134' height='auto' alt='' style='width:134px;height:auto;padding:0;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='132px' style='width:132px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;text-align:left;'><span style='width:auto;height:auto;padding:12px 0 0 42px;text-decoration:none;border:none;outline:none;'><input type='checkbox' name='option' value='pl" + placeObj.SupplyId.ToString() + "' width='20px' height='20px' style='background-color:#a9a9a9;width:20px;height:20px;padding:0;text-decoration:none;outline:none;border-color:#f7f7f7;' /></span></td></tr>");
            }
            else if (CountryId > 0 && CityId > 0)
            {
                var countryObj = _db.Countries.Find(CountryId);
                var cityObj = _db.Cities.Find(CityId);
                // var EncryptedCityId = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt("ci" + cityObj.CityId.ToString()));

                sb.Append(@"<tr height='74px' style='background-color:#efefef;width:570px;height:74px;padding:0 0 0 30px;display:block;text-decoration:none;outline:none;border:none;border-bottom:1px solid #f7f7f7;'>");
                sb.Append(@"<td width='28px' style='width:28px;height:74px;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:28px;height:28px;padding:6px 0;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com" + countryObj.FlagSrc + "' width='26px' height='auto' alt='" + countryObj.CountryName + "' Flag' style='width:26px;height:auto;padding:1px;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='236px' style='width:236px;height:74px;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:auto;height:auto;padding:4px 0 0 18px;display:block;text-decoration:none;border:none;outline:none;font:14px/16px Arial, Helvetica, sans-serif;text-align:left;color:#a1a1a1;'><span>" + countryObj.CountryName + "</span> <br /><span style='font-weight:bold;'>" + cityObj.CityName + "</span> / City</span></td>");
                sb.Append(@"<td width='174px' style='width:174px;height:74px;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:134px;height:22px;padding:9px 6px 9px 24px;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com/app/images/survey/dashed_02.jpg' width='134' height='auto' alt='' style='width:134px;height:auto;padding:0;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='132px' style='width:132px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;text-align:left;'><span style='width:auto;height:auto;padding:12px 0 0 42px;text-decoration:none;border:none;outline:none;'><input type='checkbox' name='option' value='ci" + cityObj.CityId.ToString() + "' width='20px' height='20px' style='background-color:#a9a9a9;width:20px;height:20px;padding:0;text-decoration:none;outline:none;border-color:#f7f7f7;' /></span></td></tr>");

            }
            else if (CountryId > 0)
            {
                var countryObj = _db.Countries.Find(CountryId);
                //var EncryptedCountryId = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt("co" + countryObj.CountryId.ToString()));

                sb.Append(@"<tr height='74px' style='background-color:#efefef;width:570px;height:74px;padding:0 0 0 30px;display:block;text-decoration:none;outline:none;border:none;border-bottom:1px solid #f7f7f7;'>");
                sb.Append(@"<td width='28px' style='width:28px;height:74px;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:28px;height:28px;padding:6px 0;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com" + countryObj.FlagSrc + "' width='26px' height='auto' alt='" + countryObj.CountryName + "' Flag' style='width:26px;height:auto;padding:1px;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='236px' style='width:236px;height:74px;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:auto;height:auto;padding:4px 0 0 18px;display:block;text-decoration:none;border:none;outline:none;font:14px/16px Arial, Helvetica, sans-serif;text-align:left;color:#a1a1a1;'><span>" + countryObj.CountryName + "</span></td>");
                sb.Append(@"<td width='174px' style='width:174px;height:74px;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;'><span style='width:134px;height:22px;padding:9px 6px 9px 24px;display:block;text-decoration:none;border:none;outline:none;overflow:hidden;'><img src='http://ive-been-there.com/app/images/survey/dashed_02.jpg' width='134' height='auto' alt='' style='width:134px;height:auto;padding:0;display:block;text-decoration:none;border:none;outline:none;' /></span></td>");
                sb.Append(@"<td width='132px' style='width:132px;height:auto;padding:0;display:table-cell;text-decoration:none;border:none;outline:none;text-align:left;'><span style='width:auto;height:auto;padding:12px 0 0 42px;text-decoration:none;border:none;outline:none;'><input type='checkbox' name='option' value='co" + countryObj.CountryId.ToString() + "' width='20px' height='20px' style='background-color:#a9a9a9;width:20px;height:20px;padding:0;text-decoration:none;outline:none;border-color:#f7f7f7;' /></span></td></tr>");
            }
            return sb.ToString();
        }

        private int extractDataVale(String rawData, string dataField)
        {
            int returnVal = 0;
            var pointStart = rawData.IndexOf(dataField) + dataField.Length + 2;
            var pointEnd = rawData.IndexOf('"', pointStart);
            var strValue = rawData.Substring(pointStart, pointEnd - pointStart);

            if (strValue.Length > 0 && int.TryParse(strValue, out returnVal))
            {
                return returnVal;
            }
            return returnVal;
        }

        /// <summary>
        /// admin tools发送测试邮件
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="emailId"></param>
        public bool AdminEmailSendForTest(int userId, string senderEmail, string subject, string content, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB)
        {
            //User user = _db.Users.Find(userId);
            User user = _db.Users.Where(u => u.Email == testEmail).FirstOrDefault();
            if (user != null)
            {
                IBT.Common.Email.EmailService _emailService = new Common.Email.EmailService();
                UserTravelBadge utb = _travelBadgeService.GetUserTravelbadge(user);
                string userName = user.Profile.Firstname;
                string userPic = this.GetUserAvatar(user.UserId, 100, user.Profile.ProfileImage);

                string key = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt(user.UserId.ToString()));

                if (user.IsActivate == (int)Identifier.IsActivate.True)
                    _emailService.SendActiveMemberEmail(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content, key, utb.TotalPoints.ToString(), emailId, title, logo, ShowMPB);
                else if (user.IsActivate == (int)Identifier.IsActivate.False)
                    _emailService.SendNotActiveMemberEmail(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content, key, emailId, title, logo, ShowMPB);

                return true;
            }
            return false;
        }

        /// <summary>
        /// admin tools发送测试邮件
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="emailId"></param>
        public bool NewAdminEmailSendForTest(int userId, string senderEmail, string subject, string content, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB)
        {
            //User user = _db.Users.Find(userId);
            User user = _db.Users.Where(u => u.Email == testEmail).FirstOrDefault();
            if (user != null)
            {
                IBT.Common.Email.EmailService _emailService = new Common.Email.EmailService();
                UserTravelBadge utb = _travelBadgeService.GetUserTravelbadge(user);
                string userName = user.Profile.Firstname;
                string userPic = this.GetUserAvatar(user.UserId, 100, user.Profile.ProfileImage);

                string key = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt(user.UserId.ToString()));

                if (user.IsActivate == (int)Identifier.IsActivate.True)
                    _emailService.NewSendActiveMemberEmail(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content, key, utb.TotalPoints.ToString(), emailId, title, logo, ShowMPB);
                else if (user.IsActivate == (int)Identifier.IsActivate.False)
                    _emailService.NewSendNotActiveMemberEmail(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content, key, emailId, title, logo, ShowMPB);

                return true;
            }
            return false;
        }

        public bool AdminSurveySendForTest(int userId, string senderEmail, string subject, string content1, string suggestedPlacesList, string content2, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB)
        {
            //User user = _db.Users.Find(userId);
            User user = _db.Users.Where(u => u.Email == testEmail).FirstOrDefault();
            if (user != null)
            {
                IBT.Common.Email.EmailService _emailService = new Common.Email.EmailService();
                UserTravelBadge utb = _travelBadgeService.GetUserTravelbadge(user);
                string userName = user.Profile.Firstname;
                string userPic = this.GetUserAvatar(user.UserId, 100, user.Profile.ProfileImage);

                string key = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt(user.UserId.ToString()));

                var suggestedList = SugestedList2EmailHtmlTemplate(suggestedPlacesList);

                if (user.IsActivate == (int)Identifier.IsActivate.True)
                    _emailService.SendActiveMemberSurvey(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content1, content2, suggestedList, key, utb.TotalPoints.ToString(), emailId, title, logo, ShowMPB);
                else if (user.IsActivate == (int)Identifier.IsActivate.False)
                    _emailService.SendNotActiveMemberSurvey(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content1, content2, suggestedList, key, emailId, title, logo, ShowMPB);
               
                return true;
            }
            return false;
        }

        public bool NewAdminSurveySendForTest(int userId, string senderEmail, string subject, string content1, string suggestedPlacesList, string content2, string testEmail, string emailId, string title, string logo, string senderName, int ShowMPB)
        {
            //User user = _db.Users.Find(userId);
            User user = _db.Users.Where(u => u.Email == testEmail).FirstOrDefault();
            if (user != null)
            {
                IBT.Common.Email.EmailService _emailService = new Common.Email.EmailService();
                UserTravelBadge utb = _travelBadgeService.GetUserTravelbadge(user);
                string userName = user.Profile.Firstname;
                string userPic = this.GetUserAvatar(user.UserId, 100, user.Profile.ProfileImage);

                string key = HttpUtility.UrlEncode(Common.Encrypt.EncodeHelper.AES_Encrypt(user.UserId.ToString()));

                var suggestedList = NewSugestedList2EmailHtmlTemplate(suggestedPlacesList);

                if (user.IsActivate == (int)Identifier.IsActivate.True)
                    _emailService.NewSendActiveMemberSurvey(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content1, content2, suggestedList, key, utb.TotalPoints.ToString(), emailId, title, logo, ShowMPB);
                else if (user.IsActivate == (int)Identifier.IsActivate.False)
                    _emailService.NewSendNotActiveMemberSurvey(senderName, senderEmail, testEmail, userName, subject, userPic, utb.UserPermanentPoint.CountryNumber.ToString(), utb.UserPermanentPoint.CityNumber.ToString(), utb.UserPermanentPoint.PlacesNumber.ToString(), content1, content2, suggestedList, key, emailId, title, logo, ShowMPB);

                return true;
            }
            return false;
        }




        /// <summary>
        /// 获取用户头像
        /// </summary>
        /// <returns></returns>
        private string GetUserAvatar(int userId, int avatarSize, string profileImage)
        {
            string savename = userId.ToString() + "_" + avatarSize.ToString() + ".jpg";
            string path = "/Staticfile/Avatar/" + (userId / 1000) + "/" + userId.ToString() + "/";
            string avatarpath = path + savename;
            if (string.IsNullOrEmpty(profileImage))
            {
                avatarpath = "/Staticfile/Avatar/icon/9ab2285b_" + avatarSize.ToString() + ".jpg"; //默认图片
            }
            return avatarpath;

        }
        /// <summary>
        /// Creating Log to find the issue in email sening
        /// </summary>
        /// <param name="message"></param>
        //static void WriteLog(string message)
        //{
        //    try
        //    {
        //        string logFileName = "D:\\log\\ErrorLog(" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ").txt";

        //        if (!Directory.Exists("D:\\log"))
        //        {
        //            Directory.CreateDirectory("D:\\log");
        //        }

        //        if (!File.Exists(logFileName))
        //        {
        //            using (StreamWriter sw = File.CreateText(logFileName))
        //            {
        //                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + message);
        //            }
        //        }
        //        else
        //        {
        //            using (StreamWriter sw = File.AppendText(logFileName))
        //            {
        //                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + message);
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}
    }

    /// <summary>
    /// 用于多线程调用添加EmailSendLog
    /// </summary>
    public class ThreadingService
    {
        IBTEntities _db = new IBTEntities();
        public bool AddEmailSendLog(int emailId)
        {
            try
            {
                Email email = _db.Emails.Find(emailId);

                if (email != null)
                {
                    DateTime scheduleTime = email.ScheduleTime == null ? DateTime.Now : email.ScheduleTime.Value;
                    List<User> userList = null;

                    switch (email.Recipients)
                    {
                        case (int)Identifier.EmailRecipients.AllMembers:
                            {
                                string sqlQuery = string.Format("CREATE TABLE #EmailSendLog (EmailId INT DEFAULT " + email.EmailId
                                    + ",ToEmail NVARCHAR(255), ScheduleStatus INT DEFAULT " + email.ScheduleStatus.Value
                                    + ", ScheduleTime DATETIME DEFAULT '" + DateTime.Now
                                    + "',SendStatus INT DEFAULT " + (int)Identifier.EmailSendStatus.HaventSent
                                    + ", SendTime DATETIME DEFAULT '" + DateTime.Now
                                    + "', StatusId INT DEFAULT " + (int)Identifier.StatusType.Enabled
                                    + ", CreateDate DATETIME DEFAULT '" + DateTime.Now
                                    + "'); INSERT INTO #EmailSendLog (ToEmail) SELECT Distinct email FROM Users WHERE StatusId = 0;"
                                    + "INSERT INTO EmailSendLog SELECT * FROM #EmailSendLog; DROP TABLE #EmailSendLog;");

                                _db.Database.ExecuteSqlCommand(sqlQuery);
                            }
                            break;
                        case (int)Identifier.EmailRecipients.BadgesMembers:
                            {
                                //userList = _db.Users.Where(u => u.StatusId == 0 && u.IsActivate == (int)Identifier.IsActivate.True).ToList();
                                //AddEmailSendLogFast(userList, emailId);
                                string sqlQuery = string.Format("CREATE TABLE #EmailSendLog (EmailId INT DEFAULT " + email.EmailId
                                  + ",ToEmail NVARCHAR(255), ScheduleStatus INT DEFAULT " + email.ScheduleStatus.Value
                                  + ", ScheduleTime DATETIME DEFAULT '" + DateTime.Now
                                  + "',SendStatus INT DEFAULT " + (int)Identifier.EmailSendStatus.HaventSent
                                  + ", SendTime DATETIME DEFAULT '" + DateTime.Now
                                  + "', StatusId INT DEFAULT " + (int)Identifier.StatusType.Enabled
                                  + ", CreateDate DATETIME DEFAULT '" + DateTime.Now
                                  + "'); INSERT INTO #EmailSendLog (ToEmail) SELECT Distinct email FROM Users WHERE StatusId = 0 AND [IsActivate]=1"
                                  + "INSERT INTO EmailSendLog SELECT * FROM #EmailSendLog; DROP TABLE #EmailSendLog;");
                                _db.Database.ExecuteSqlCommand(sqlQuery);
                            }
                            break;
                        case (int)Identifier.EmailRecipients.NotActivatedMembers:
                            {
                                //userList = _db.Users.Where(u => u.StatusId == 0 && u.IsActivate == (int)Identifier.IsActivate.False).ToList();
                                //AddEmailSendLogFast(userList, emailId);
                                string sqlQuery = string.Format("CREATE TABLE #EmailSendLog (EmailId INT DEFAULT " + email.EmailId
                                    + ",ToEmail NVARCHAR(255), ScheduleStatus INT DEFAULT " + email.ScheduleStatus.Value
                                    + ", ScheduleTime DATETIME DEFAULT '" + DateTime.Now
                                    + "',SendStatus INT DEFAULT " + (int)Identifier.EmailSendStatus.HaventSent
                                    + ", SendTime DATETIME DEFAULT '" + DateTime.Now
                                    + "', StatusId INT DEFAULT " + (int)Identifier.StatusType.Enabled
                                    + ", CreateDate DATETIME DEFAULT '" + DateTime.Now
                                    + "'); INSERT INTO #EmailSendLog (ToEmail) SELECT Distinct email FROM Users WHERE StatusId = 0 AND [IsActivate]=0"
                                    + "INSERT INTO EmailSendLog SELECT * FROM #EmailSendLog; DROP TABLE #EmailSendLog;");
                                _db.Database.ExecuteSqlCommand(sqlQuery);

                            } break;
                        case (int)Identifier.EmailRecipients.Custom:
                            if (!string.IsNullOrEmpty(email.RecipientsEmail))
                            {
                                List<string> emailList = email.RecipientsEmail.Split(',').ToList();
                                emailList.ForEach(e =>
                                {
                                    EmailSendLog sendLog = new EmailSendLog();
                                    sendLog.ToEmail = e;
                                    sendLog.EmailId = email.EmailId;
                                    sendLog.ScheduleStatus = email.ScheduleStatus.Value;
                                    sendLog.ScheduleTime = scheduleTime;
                                    sendLog.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                                    sendLog.SendTime = DateTime.Now;
                                    sendLog.StatusId = (int)Identifier.StatusType.Enabled;
                                    sendLog.CreateDate = DateTime.Now;

                                    _db.EmailSendLogs.Add(sendLog);
                                });

                                _db.SaveChanges();
                            }
                            break;
                        default:
                            break;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public Boolean AddEmailSendLogFast(List<User> userList, int emailId)
        {
            try
            {
                Email email = _db.Emails.Find(emailId);
                DateTime scheduleTime = email.ScheduleTime == null ? DateTime.Now : email.ScheduleTime.Value;
                if (userList != null)
                {
                    EmailSendLog sendLog;
                    DateTime _dateTimeNow = DateTime.Now;
                    int j = 0;
                    for (int i = 0; i < userList.Count; i++)
                    {
                        sendLog = new EmailSendLog();
                        sendLog.ToEmail = userList[i].Email;
                        sendLog.EmailId = email.EmailId;
                        sendLog.ScheduleStatus = email.ScheduleStatus.Value;
                        sendLog.ScheduleTime = scheduleTime;
                        sendLog.SendStatus = (int)Identifier.EmailSendStatus.HaventSent;
                        sendLog.SendTime = _dateTimeNow;
                        sendLog.StatusId = (int)Identifier.StatusType.Enabled;
                        sendLog.CreateDate = _dateTimeNow;

                        _db.EmailSendLogs.Add(sendLog);
                        //used to reduce server memory usage
                        if (j > 50)
                        {
                            _db.SaveChanges();
                            j = 0;
                        }
                        j++;
                    }
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }       
    }
}