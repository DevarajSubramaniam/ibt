﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using IMeet.IBT.Common;
using IMeet.IBT.Common.Email;
using IMeet.IBT.DAL;

namespace IMeet.IBT.Services
{
    public class ProfileService : BaseService, IProfileService
    {
        #region 获取当前登录的ID
        /// <summary>
        /// 获取当前登录的ID
        /// </summary>
        /// <returns></returns>
        public int GetUserId()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return Convert.ToInt32(HttpContext.Current.User.Identity.Name);
            }
            else
            {
                return -1;
            }
        }
        #endregion

        #region 获取总的会员数
        /// <summary>
        /// 获取总的会员数
        /// </summary>
        /// <returns></returns>
        public int GetAllMemberCount()
        {
            return _cache.GetOrInsert<int>("GetAllMemberCount", 30, true, () =>
            {
                byte approved = Utils.IntToByte(1);
                return (from c in _db.Users select c).Count();
            });
        }
        #endregion

        #region 根据条件返回用户的资料
        /// <summary>
        /// 根据用户ID返回用户的资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DAL.Profile GetUserProfile(int userId)
        {
            string key = "user_" + userId.ToString();
            return _cache.GetOrInsert<Profile>(key, 60, true, () =>
            {
                var profile = _db.Profiles.Where(c => c.UserId.Equals(userId)).FirstOrDefault();
                if (profile != null)
                    return profile;
                else
                {
                    return new DAL.Profile();
                }
            });
        }
        /// <summary>
        /// 根据用户ID返回用户的资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DAL.User GetUserInfo(int userId)
        {
            var user = _db.Users.Where(c => c.UserId.Equals(userId)).FirstOrDefault();
            if (user != null)
                return user;
            else
            {
                return new DAL.User();
            }
        }
        /// <summary>
        /// 根据email和password返回用户的资料
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public DAL.User GetBindUser(string email, string password)
        {
            var user = _db.Users.Where(c => c.Email.Equals(email) && c.Password.Equals(password)).FirstOrDefault();
            if (user != null)
                return user;
            else
            {
                return new DAL.User();
            }
        }

        public DAL.User GetUserNewEMailVerification(string token)
        {
            var user = _db.Users.Where(c => c.NewEmailIdVerfication.Equals(token)).FirstOrDefault();
            if (user != null)
                return user;
            else
            {
                return new DAL.User();
            }
        }

        public int GetUserCount(string email)
        {
            var q = from o in _db.Users
                    where o.Email.Equals(email)
                    select o;
            return q.Count();
        }

        /// <summary>
        /// 根据用户ID返回用户的好友资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DAL.Connection GetUserConnection(int userId, int friendId)
        {
            var connection = _db.Connections.Where(c => c.UserId.Equals(userId) && c.FriendId.Equals(friendId)).FirstOrDefault();
            if (connection != null)
                return connection;
            else
            {
                return new DAL.Connection();
            }

        }
        #endregion

        #region 根据用户ID获取信息通知数量
        /// <summary>
        /// 根据用户ID获取信息通知数量
        /// </summary>
        /// <returns></returns>
        public int GetMsgCount(int userId)
        {
            string key = "GetMsgCount_" + userId.ToString();
            return _cache.GetOrInsert<int>(key, 5, true, () =>
           {
               string sql = @"SELECT  ROW_NUMBER() OVER (ORDER BY dbo.Messges.MsgId DESC) AS RowId,dbo.Messges.MsgId,dbo.Messges.ReplyId, dbo.Messges.ToUserId,dbo.Messges.FormUserId,dbo.Profiles.Firstname, dbo.Profiles.Lastname,dbo.Profiles.Username,
	 	           dbo.Profiles.ProfileImage AS ImgSrc,dbo.Messges.IsRead, dbo.Messges.IsFav, dbo.Messges.Content,dbo.Messges.CreateDate, dbo.Messges.IsDel			
			FROM dbo.Messges
			LEFT JOIN dbo.Profiles ON dbo.Messges.FormUserId = dbo.Profiles.UserId			
            LEFT JOIN dbo.Users ON dbo.Users.UserId = dbo.Profiles.UserId
            WHERE  dbo.Messges.IsDel=0 AND dbo.Messges.IsRead=0 AND dbo.Messges.ToUserId=" + userId.ToString();
               return _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>().Count;
           });
        }
        public int GetReplyCount(int msgId, int userId)
        {
            string sql = @"SELECT  ROW_NUMBER() OVER (ORDER BY dbo.Messges.MsgId DESC) AS RowId,dbo.Messges.MsgId,dbo.Messges.ReplyId, dbo.Messges.ToUserId,dbo.Messges.FormUserId,dbo.Profiles.Firstname, dbo.Profiles.Lastname,dbo.Profiles.Username,
	 	           dbo.Profiles.ProfileImage AS ImgSrc,dbo.Messges.IsRead, dbo.Messges.IsFav, dbo.Messges.Content,dbo.Messges.CreateDate, dbo.Messges.IsDel			
			FROM dbo.Messges
			LEFT JOIN dbo.Profiles ON dbo.Messges.FormUserId = dbo.Profiles.UserId			
            LEFT JOIN dbo.Users ON dbo.Users.UserId = dbo.Profiles.UserId
            WHERE  dbo.Messges.IsDel=0 AND dbo.Messges.IsRead=0 AND dbo.Messges.ToUserId=" + userId.ToString() + "AND dbo.Messges.ReplyId=" + msgId.ToString();
            return _db.Database.SqlQuery<MemberListItem>(sql).ToList<MemberListItem>().Count;
        }
        #endregion

        #region 根据用户ID获取好友数量
        /// <summary>
        /// 根据用户ID获取好友数量
        /// </summary>
        /// <returns></returns>
        public int GetConnectionCount(int userId)
        {
            string key = "GetConnectionCount_" + userId.ToString();
            return _cache.GetOrInsert<int>(key, 10, true, () =>
            {
                var q = from o in _db.Connections
                        where o.UserId.Equals(userId) && o.StatusId == 1
                        select o;
                return q.Count();
            });
            //int userId = GetUserId();
            //var q = from o in _db.Connections
            //        where o.UserId.Equals(userId) && o.StatusId == 1
            //        select o;
            //if (q.Count() > 0)
            //{
            //    return q.Count();
            //}
            //else
            //{
            //    return 0;
            //}
        }
        #endregion

        #region 获取用户头像
        /// <summary>
        /// 获取用户头像
        /// </summary>
        /// <returns></returns>
        public string GetUserAvatar(int userId, int avatarSize)
        {
            string savename = userId.ToString() + "_" + avatarSize.ToString() + ".jpg";
            string path = "/Staticfile/Avatar/" + (userId / 1000) + "/" + userId.ToString() + "/";
            string avatarpath = path + savename;
            if (!File.Exists(HttpContext.Current.Server.MapPath("~" + avatarpath)))
            {
                avatarpath = "/Staticfile/Avatar/icon/9ab2285b_" + avatarSize.ToString() + ".jpg"; //默认图片
            }
            return avatarpath;

        }
        /// <summary>
        /// 获取用户Newsfeed头像
        /// </summary>
        /// <returns></returns>
        public string GetUserFeedAvatar(int userId, int avatarSize)
        {
            string savename = userId.ToString() + "_" + avatarSize.ToString() + ".jpg";
            string path = "/Staticfile/Newsfeed/" + (userId / 1000) + "/" + userId.ToString() + "/";
            string avatarpath = path + savename;
            if (!File.Exists(HttpContext.Current.Server.MapPath("~" + avatarpath)))
            {
                avatarpath = "/Staticfile/Avatar/icon/9ab2285b_" + avatarSize.ToString() + ".jpg"; //默认图片
            }
            return avatarpath;

        }
        

        public string GetUserCutAvatar(int userId, int avatarSize)
        {
            string savename = userId.ToString() + "_cut.jpg";
            string path = "/Staticfile/Avatar/" + (userId / 1000) + "/" + userId.ToString() + "/";
            string avatarpath = path + savename;
            if (!File.Exists(HttpContext.Current.Server.MapPath("~" + avatarpath)))
            {
                avatarpath = "/Staticfile/Avatar/icon/9ab2285b_" + avatarSize.ToString() + ".jpg"; //默认图片
            }
            return avatarpath;

        }
        #endregion

        #region 根据条件获取数量
        /// <summary>
        /// 根据用户ID获取好友请求数量
        /// </summary>
        /// <returns></returns>
        public int GetRequestCount(int userId)
        {
            string key = "GetRequestCount_" + userId.ToString();
            return _cache.GetOrInsert<int>(key, 10, true, () =>
            {
                var q = from o in _db.Connections
                        where o.FriendId.Equals(userId) && o.StatusId == 0
                        select o;
                return q.Count();
            });
            //int userId = GetUserId();
            //var q = from o in _db.Connections
            //        where o.UserId.Equals(userId) && o.StatusId == 0
            //        select o;
            //if (q.Count() > 0)
            //{
            //    return q.Count();
            //}
            //else
            //{
            //    return 0;
            //}
        }
        public int GetFriendCount(int userId, int friendId)
        {
            string sql = @"SELECT * FROM dbo.Connections
            WHERE  UserId=" + userId.ToString() + "AND FriendId=" + friendId.ToString() + "AND StatusId=0";
            return _db.Database.SqlQuery<Connection>(sql).ToList<Connection>().Count;
        }

        public int GetTotalCount(int userId)
        {
            return GetMsgCount(userId) + GetRequestCount(userId);
        }
        #endregion

        #region 根据条件返回国家、城市的资料
        /// <summary>
        /// 根据国家ID返回国家的资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserCountry(int userId, int countryId)
        {
            string key = "GetUserCountry" + userId.ToString() + "_" + countryId.ToString();
            //countryId = Convert.ToInt32(GetUserProfile(userId).CountryId);
            return _cache.GetOrInsert<string>(key, 60, true, () =>
            {
                var country = _db.Countries.Where(c => c.CountryId.Equals(countryId)).FirstOrDefault();
                if (country != null)
                    return "<a href=\"/Country/About/1/" + country.CountryId.ToString() + "\">" + country.CountryName + "</a>";
                else
                {
                    return "";
                }
            });

            /*
            countryId = Convert.ToInt32(GetUserProfile(userId).CountryId);
            var country = _db.Countries.Where(c => c.CountryId.Equals(countryId)).FirstOrDefault();
            if (country != null)
                return country;
            else
            {
                return new DAL.Country();
            }
            */
        }

        /// <summary>
        /// 根据城市ID返回城市的资料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserCity(int userId, int cityId)
        {
            string key = "GetUserCity" + userId.ToString() + "_" + cityId.ToString();
            return _cache.GetOrInsert<string>(key, 60, true, () =>
            {
                //cityId = Convert.ToInt32(GetUserProfile(userId).CityId);
                var city = _db.Cities.Where(c => c.CityId.Equals(cityId)).FirstOrDefault();
                if (city != null)
                    return "<a href=\"/City/NewsFeed/2/" + city.CityId.ToString() + "\">" + city.CityName +"</a>";
                else
                {
                    return "";
                }
            });
        }
        #endregion

        #region MemberManage_List

        public int GetMemberListCount(string strWhere)
        {
            return 0;
        }

        /// <summary>
        /// Member Lists 的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterLetter"></param>
        /// <param name="filterKeyword"></param>
        /// <param name="filterField"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public IList<MemberListItem> GetMemberList(int OrderBy, int userId, string DescOrAsc, int page, int pageSize, string searchKeyword, string filterField, out int total)
        {
            //int skipSize = 0;
            //int IsCountRow = 1;
            //int NotNeedID = -1;
            //--IBT_IMeetGetAllMemberList 0,'DESC','',1,1,10
            //IList<MemberListItem> list = _db.Database.SqlQuery<MemberListItem>("EXEC IBT_IMeetGetAllMemberList {0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", OrderBy, DescOrAsc, searchKeyword, filterField, userId, page, pageSize, skipSize, IsCountRow, NotNeedID).ToList<MemberListItem>();
            IList<MemberListItem> list = _db.Database.SqlQuery<MemberListItem>("EXEC IBT_IMeetGetAllMemberList {0},{1},{2},{3},{4},{5},{6}", OrderBy, DescOrAsc, searchKeyword, filterField, userId, page, pageSize).ToList<MemberListItem>();
            total = list.Count();

            //if (!string.IsNullOrWhiteSpace(filterField)) //搜索
            //{
            //    //todo:GetMyList filterField
            //    return new List<MemberListItem>();
            //}
            //else
            //{
            return list.ToList<MemberListItem>();
            //}

            //todo 下面还需要返回成员数
        }

        public BoolMessageItem<int> SetMemberSort(int userId, int newText)
        {
            Profile profile = _db.Profiles.FirstOrDefault(x => x.UserId.Equals(userId));
            if (profile != null)
            {
                profile.Sort = newText;
                _db.SaveChanges();
                return new BoolMessageItem<int>(profile.UserId, true, "Search Score has been successfully updated!");
            }
            else
            {
                return new BoolMessageItem<int>(0, false, "Search Score is not successfully updated. Please try again!");
            }
        }
        #endregion

        #region 更新用户信息
        /// <summary>
        /// 激活用户TravelBadge
        /// </summary>
        /// <param name="userid">userid</param>
        /// <returns></returns>
        public BoolMessage SetUserActivateTravelBadge(int userid)
        {
            try
            {
                DAL.User user = _db.Users.Find(userid);
                user.IsActivate = (int)Identifier.IsActivate.True;
                user.ActivateDate = DateTime.Now;
                _db.SaveChanges();

                //send email
                EmailService toEmail = new EmailService("Email\\en-us");
                toEmail.SendActivedTeavelBadge(user.Email);

                return new BoolMessage(true, "");
            }
            catch (Exception ex)
            {
                return new BoolMessage(false, ex.Message);
            }
        }

        /// <summary>
        /// send email to user who isactive TravelBadge
        /// </summary>
        /// <returns></returns>
        public BoolMessage SendEmailToIsActive() 
        {
            try 
            {
                EmailService toEmail = new EmailService("Email\\en-us");
                var userList = _db.Users.Where(u => u.IsActivate == 1).ToList();

                userList.ForEach(u => {
                    toEmail.SendActivedTeavelBadge(u.Email);
                });

                return new BoolMessage(true, "");
            }
            catch (Exception ex) 
            {
                return new BoolMessage(false, ex.Message);
            }
        }

        /// <summary>
        /// send emai to auto active
        /// </summary>
        public void SendEmailToAutoActive()
        {
            List<UserAutoActivate> uaa = _db.UserAutoActivates.Where(u => u.StatusId == 0).ToList();
            EmailService toEmail = new EmailService("Email\\en-us");

            uaa.ForEach(u => {
                User user = _db.Users.Where(us => us.Email == u.Email).FirstOrDefault();
                if (user != null && user.IsActivate == (int)Identifier.IsActivate.False)
                {
                    toEmail.SendActivateMail(user.Email);
                    user.IsActivate = (int)Identifier.IsActivate.True;
                    user.ActivateDate = DateTime.Now;
                    u.StatusId = 1;
                }
            });

            _db.SaveChanges();
        }
        #endregion

        #region 查找用户信息

        /// <summary>
        /// 查找profiles
        /// </summary>
        /// <param name="total"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public List<Profile> GetProfileList(out int total, int page = 0, int pageSize = 0, string keyword = "")
        {
            List<Profile> list = new List<Profile>();

            if (keyword == "")
            {
                total = _db.Profiles.Count();
                if (page > 0 && pageSize > 0)
                {
                    int skip = (page - 1) * pageSize;
                    list = _db.Profiles.OrderBy(p => p.UserId).Skip(skip).Take(pageSize).ToList();
                }
                else
                {
                    list = _db.Profiles.ToList();
                }
            }
            else
            {
                total = _db.Profiles.Where(p => p.Firstname.Contains(keyword) || p.Lastname.Contains(keyword) || p.Email.Contains(keyword)).Count();

                if (page > 0 && pageSize > 0)
                {
                    int skip = (page - 1) * pageSize;
                    list = _db.Profiles.Where(p => p.Firstname.Contains(keyword) || p.Lastname.Contains(keyword) || p.Email.Contains(keyword)).OrderBy(p => p.UserId).Skip(skip).Take(pageSize).ToList();
                }
                else
                {
                    list = _db.Profiles.Where(p => p.Firstname.Contains(keyword) || p.Lastname.Contains(keyword) || p.Email.Contains(keyword)).ToList();
                }
            }

            return list;
        }

        #endregion
    }
}
