﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.Services
{
    public class Identifier
    {
        /// <summary>
        /// SnsType
        /// </summary>
        public enum SnsType
        {
            /// <summary>
            /// 0
            /// </summary>
            IBT = 0,
            /// <summary>
            /// 1
            /// </summary>
            Facebook = 1,
            /// <summary>
            /// 2
            /// </summary>
            Twitter = 2,
            /// <summary>
            /// 3
            /// </summary>
            LinkedIn = 3,
            /// <summary>
            /// 4
            /// </summary>
            IMeet = 4
        }

        /// <summary>
        /// 网站默认的状态类型
        /// </summary>
        public enum StatusType
        {
            /// <summary>
            /// 1
            /// </summary>
            Enabled = 1,
            /// <summary>
            /// 0
            /// </summary>
            Disabled = 0
        }

        /// <summary>
        /// 
        /// </summary>
        public enum IBTType
        {
            /// <summary>
            /// 1
            /// </summary>
            Country = 1,
            /// <summary>
            /// 2
            /// </summary>
            City = 2,
            /// <summary>
            /// 3
            /// </summary>
            Supply = 3,
            /// <summary>
            /// 4
            /// </summary>
            User = 4,
            /// <summary>
            /// 5
            /// </summary>
            Admin = 5
        }

        /// <summary>
        /// AddTravels 的类型
        /// </summary>
        public enum AddTravelsType
        {
            Country = 1,
            City = 2,
            Venue = 3
        }

        /// <summary>
        /// 评分类型
        /// </summary>
        public enum RRWBType
        {
            /// <summary>
            /// 1
            /// </summary>
            Rating = 1,
            /// <summary>
            /// 2
            /// </summary>
            Recommended = 2,
            /// <summary>
            /// 3
            /// </summary>
            Want_to_Go_Go_Back = 3,
            /// <summary>
            /// 4
            /// </summary>
            BucketList = 4,
            /// <summary>
            /// 此只在统计时候用5
            /// </summary>
            All = 5
        }

        public enum SuppilyPostVisitPhotoSize
        {
            /// <summary>
            /// 1024
            /// </summary>
            _big = 1024,
            /// <summary>
            /// 80
            /// </summary>
            _tn = 80
        }

        /// <summary>
        /// Suppily 的图片大小的
        /// </summary>
        public enum SuppilyPhotoSize
        {
            /// <summary>
            /// 1024
            /// </summary>
            _big = 1024,
            /// <summary>
            /// 466x270 (不一起生产此图，需要Cut，放最后来改这问题吧)
            /// </summary>
            _large = 466,
            /// <summary>
            /// 156
            /// </summary>
            _bn = 156,
            /// <summary>
            /// 100
            /// </summary>
            _mn = 100,
            /// <summary>
            /// 80
            /// </summary>
            _tn = 80,
            /// <summary>
            /// 50
            /// </summary>
            _xtn = 50
        }

        public enum FileStatusType
        {
            /// <summary>
            /// 1
            /// </summary>
            Enabled = 1,
            /// <summary>
            /// 0
            /// </summary>
            Disabled = 0,
            /// <summary>
            /// 2 del
            /// </summary>
            Delete = 2
        }

        public enum AccountSettingAvatarSize
        {
            /// <summary>
            /// 130
            /// </summary>
            _mm = 130,
            /// <summary>
            /// 100
            /// </summary>
            _mn = 100,
            /// <summary>
            /// 75
            /// </summary>
            _lw = 75,
            /// <summary>
            /// 55
            /// </summary>
            _at = 55,
            /// <summary>
            /// 50
            /// </summary>
            _xat = 50,
            /// <summary>
            /// 36
            /// </summary>
            _xlat = 36,
            /// <summary>
            /// 26
            /// </summary>
            _xxlat = 26
        }

        public enum MemberSearchOrderBy
        {
            SortNum = 0,
            FirstName = 1,
            UserID = 2,
            Birth = 3,
            Email = 4,
            MemberFeatured = 5,
            Gender = 6,
            Location = 7,
            Method = 8,
            Activate = 9,
            Password = 10

        }
        public enum SupplySearchOrderBy
        {
            Score = 0,
            Title = 1
        }
        public enum SupplyDestinationOrderBy
        {
            Sort = 0,
            Title = 1
        }
        public enum OrderBy
        {
            DESC = 1,
            ASC = 2
        }

        /// <summary>
        /// Feed的操作类型
        /// </summary>
        public enum FeedActiveType
        {
            Rating = 1,
            Recommended = 2,
            WantGoBack = 3,
            BucketList = 4,
            /// <summary>
            /// PostVisit 也包含ibt 5
            /// </summary>
            PostVisit = 5,
            /// <summary>
            /// 是只有ibt没评论内容。在Add Travels的批量添加的 6
            /// </summary>
            IBT = 6,
            /// <summary>
            /// Add Travels 7
            /// </summary>
            AddTravel = 7,
            /// <summary>
            /// ProfileImg 8
            /// </summary>
            ProImage = 8,
            /// <summary>
            /// 添加新好友（时间段：最新7天） 9
            /// </summary>
            AddNewFriend = 9,
            /// <summary>
            /// post 新的评论内容 10
            /// </summary>
            PostComments = 10,
            /// <summary>
            /// 用户更新状态 11
            /// </summary>
            UpdateStatus = 11,
            /// <summary>
            /// 对象 post(暂时没这个功能，但有这个type) 12
            /// </summary>
            ObjPost = 12,
            /// <summary>
            /// 取消 Recommended /WantGoBack / BucketList 也记录下来，不过在feed里不显示出来 -1
            /// </summary>
            Cancel = -1
        }

        /// <summary>
        /// SupplierTypes
        /// </summary>
        public enum SupplierTypes
        {
            /// <summary>
            /// 1
            /// </summary>
            Basic = 1,
            /// <summary>
            /// 2
            /// </summary>
            Sponsored = 2,
            /// <summary>
            /// 3
            /// </summary>
            Interactive = 3
        }
        /// <summary>
        /// ServiceType for supplier
        /// </summary>
        public enum ServiceType
        {
            /// <summary>
            /// 0
            /// </summary>
            None = 0,
            /// <summary>
            /// 1
            /// </summary>
            ServiceProvider = 1
        }
        /// <summary>
        /// SourceType for supplier
        /// </summary>
        public enum SourceType
        {
            Default = 0,
            Lanyon = 1
        }
        /// <summary>
        /// PostTypes
        /// </summary>
        public enum PostTypes
        {
            /// <summary>
            /// 0
            /// </summary>
            PostVisit = 0,
            /// <summary>
            /// 1
            /// </summary>
            Detail = 1
        }

        /// <summary>
        /// MediaTypes
        /// </summary>
        public enum MediaTypes
        {
            /// <summary>
            /// 1
            /// </summary>
            Video = 1,
            /// <summary>
            /// 2
            /// </summary>
            picture = 2,
            /// <summary>
            /// 3
            /// </summary>
            HtmlCode = 3
        }

        /// <summary>
        /// PositionTypes
        /// </summary>
        public enum PositionTypes
        {
            /// <summary>
            /// 1
            /// </summary>
            Promotions = 1,
            /// <summary>
            /// 2
            /// </summary>
            Advertisement = 2
        }

        /// <summary>
        /// Media 的图片大小的 Video (116X85)  Ads (205X169) Special Offer (304X198)
        /// </summary>
        public enum MediaPhotoSize
        {
            /// <summary>
            /// 304X198
            /// </summary>
            _spec = 166,
            /// <summary>
            /// 205X169 (不一起生产此图，需要Cut，放最后来改这问题吧)
            /// </summary>
            _ads = 205,
            /// <summary>
            /// 116X85
            /// </summary>
            _vd = 116,
            /// <summary>
            /// 110X80
            /// </summary>
            _def = 110,
            /// <summary>
            /// 86X86
            /// </summary>
            _sw = 86
        }

        /// <summary>
        /// 登录失败原因
        /// </summary>
        public enum FailureInfoType
        {
            /// <summary>
            /// 用户不存在
            /// </summary>
            AccountIncorrect = 0,
            /// <summary>
            /// 密码错误
            /// </summary>
            PassowrdIncorrect = 1,
            /// <summary>
            /// 用户未激活
            /// </summary>
            AccountNotActivate = 2
        }

        /// <summary>
        /// 登录状态
        /// </summary>
        public enum AccountStatusType
        {
            /// <summary>
            /// 1
            /// </summary>
            Success = 1,
            /// <summary>
            /// 0
            /// </summary>
            Fail = 0
        }

        /// <summary>
        /// 附件类型
        /// </summary>
        public enum AttachmentType
        {
            /// <summary>
            /// 所有的附件类型 0
            /// </summary>
            All = 0,
            /// <summary>
            /// PV的附件类型 1
            /// </summary>
            PostVisit = 1,
            /// <summary>
            /// 评论的附件 2
            /// </summary>
            NewsFeed = 2
          
        }
        /// <summary>
        /// User表的IsActive
        /// </summary>
        public enum IsActivate
        {
            /// <summary>
            /// 未激活
            /// </summary>
            False = 0,
            /// <summary>
            /// 已激活
            /// </summary>
            True = 1
        }
        /// <summary>
        /// Travel badge level of achievement
        /// </summary>
        public enum Levels
        {
            /// <summary>
            /// 0 start 0-149 point
            /// </summary>
            Practitioners = 0,
            /// <summary>
            /// 1 start 150-299 point
            /// </summary>
            Professional = 1,
            /// <summary>
            /// 2 start 300-599 point
            /// </summary>
            Expert = 2,
            /// <summary>
            /// 3 start 600 more point
            /// </summary>
            Elite = 3,
            /// <summary>
            /// 4 start
            /// 40 Countries Minimum
            /// 200 Cities Minimum
            /// 300 Hotels and Other Venues Minimum
            /// </summary>
            WorldClass = 4
        }

        /// <summary>
        /// badge special 可见属性
        /// </summary>
        public enum BadgeSpecialVisible
        {
            /// <summary>
            /// 不可见
            /// </summary>
            Invisible = 0,
            /// <summary>
            /// 可见
            /// </summary>
            Visible = 1
        }

        /// <summary>
        /// badge special type
        /// </summary>
        public enum BadgeSpecialType
        {
            /// <summary>
            /// CLICK FOR POINTS
            /// </summary>
            IbtDriven = 1,
            /// <summary>
            /// EXTERNAL EVENT
            /// </summary>
            EmailSignup = 2,
            /// <summary>
            /// POSTING ACTIVITY BONUS
            /// </summary>
            UniqueIbt = 3,
            /// <summary>
            /// COMMENT/RATE BONUS
            /// </summary>
            UniqueSpecificActivity = 4
        }

        /// <summary>
        /// badge special comment type
        /// badge special的评论类型
        /// </summary>
        public enum BadgeSpecialCommentType
        {
            /// <summary>
            /// Comment on Post Visit
            /// </summary>
            PostVisit,
            /// <summary>
            /// Comment on Country Listing
            /// </summary>
            Country,
            /// <summary>
            /// Comment on City Listing
            /// </summary>
            City,
            /// <summary>
            /// Comment on Supplier Listing
            /// </summary>
            Supply,
            /// <summary>
            /// Rate
            /// </summary>
            Rate,
            /// <summary>
            /// General Comments on Post Visit
            /// </summary>
            PostVisitGeneralComments
        }

        /// <summary>
        /// Email Recipients
        /// </summary>
        public enum EmailRecipients
        {
            /// <summary>
            /// All Members
            /// </summary>
            AllMembers=1,
            /// <summary>
            /// Badges Members
            /// </summary>
            BadgesMembers=2,
            /// <summary>
            /// Not Activated Members
            /// </summary>
            NotActivatedMembers=3,
            /// <summary>
            /// Custom
            /// </summary>
            Custom=4
        }

        /// <summary>
        /// Email send status
        /// </summary>
        public enum EmailSendStatus
        {
            /// <summary>
            /// sent 1
            /// </summary>
            Sent = 1,
            /// <summary>
            /// haven't sent 2
            /// </summary>
            HaventSent = 2,
            /// <summary>
            /// achive 3
            /// </summary>
            Achive = 3
        }

        /// <summary>
        /// Email ScheduleStatus
        /// </summary>
        public enum EmailScheduleStatus
        {
            /// <summary>
            /// Immediate(0)
            /// </summary>
            Immediate=0,
            /// <summary>
            /// Auto(1)
            /// </summary>
            Auto=1
        }

        //added by Alireza 07-July-15 for email Survey
        /// <summary>
        /// Email Type template
        /// </summary>
        public enum EmailType
        {
            /// <summary>
            /// email ads template
            /// email= (1)
            /// </summary>
            email = 1,

            /// <summary>
            /// Survey email for countries/Cities/Places
            /// survey = (2)
            /// </summary>
            survey = 2,

            /// <summary>
            /// newemail ads template
            /// newemail= (3)
            /// </summary>
            newemail = 3,

            /// <summary>
            /// new Survey email for countries/Cities/Places
            /// newsurvey = (4)
            /// </summary>
            newsurvey = 4
        }
    }
}
