﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using StructureMap;
using System.Data;
using System.Data.Entity;
using IMeet.IBT.DAL;
using IMeet.IBT.Common;

namespace IMeet.IBT.Services
{
    public class MapsService : BaseService,IMapsService
    {
        #region 获取坐标的
        /// <summary>
        /// 国家de坐标
        /// </summary>
        /// <param name="countryId">如果是0就是给所有没坐标设定坐标，如果大于0则给指定的更新坐标</param>
        public void GeoCountry(int countryId = 0)
        {
            if (countryId > 0)
            {
                var item = (from c in _db.Countries
                            where c.CountryId.Equals(countryId)
                            select c).FirstOrDefault();
                if (item != null)
                {
                    var bmi = getGeo(item.CountryName);
                    if (bmi.Success)
                    {
                        item.CoordsLatitude = bmi.Item["lat"];
                        item.CoordsLongitude = bmi.Item["lon"];

                        _db.SaveChanges();
                    }
                }
            }
            else
            {
                var list = (from c in _db.Countries where c.CoordsLongitude.Value.Equals(0) && c.CoordsLatitude.Value.Equals(0) select c).ToList();
                foreach (var item in list)
                {
                    var bmi = getGeo(item.CountryName);
                    if (bmi.Success)
                    {
                        item.CoordsLatitude = bmi.Item["lat"];
                        item.CoordsLongitude = bmi.Item["lon"];

                        _db.SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// 城市de坐标
        /// </summary>
        /// <param name="cityId">如果是0就是给所有没坐标设定坐标，如果大于0则给指定的更新坐标</param>
        public void GeoCity(int cityId)
        {
            if (cityId > 0)
            {
                var item = (from c in _db.Cities
                            join t in _db.Countries on c.CountryId equals t.CountryId
                            where c.CityId.Equals(cityId)
                            select new { CityName = c.CityName, CountryName = t.CountryName }).FirstOrDefault();
                if (item != null)
                {
                    var bmi = getGeo(item.CityName + "," + item.CountryName);
                    if (bmi.Success)
                    {
                        var city = (from c in _db.Cities where c.CityId.Equals(cityId) select c).FirstOrDefault();
                        if (city != null)
                        {
                            city.CoordsLatitude = bmi.Item["lat"];
                            city.CoordsLongitude = bmi.Item["lon"];

                            _db.SaveChanges();
                        }
                    }
                }
            }
            else
            {
                cityGeo(1);
            }
        }

        private void cityGeo(int page)
        {
            int pageSize = 50;
            int startRecord = (page - 1) * pageSize;

            var list = (from c in _db.Cities
                        join t in _db.Countries on c.CountryId equals t.CountryId
                        where c.CoordsLatitude.Value.Equals(0) && c.CoordsLongitude.Value.Equals(0)
                        orderby c.CityId ascending
                        select new { CityId = c.CityId, CityName = c.CityName, CountryName = t.CountryName })
                        .Skip(startRecord).Take(pageSize).ToList();
            foreach (var item in list)
            {
                var bmi = getGeo(item.CityName + "," + item.CountryName);
                if (bmi.Success)
                {
                    var city = (from c in _db.Cities where c.CityId.Equals(item.CityId) select c).FirstOrDefault();
                    if (city != null)
                    {
                        city.CoordsLatitude = bmi.Item["lat"];
                        city.CoordsLongitude = bmi.Item["lon"];

                        _db.SaveChanges();
                    }
                }
            }

            if (list.Count > 0)
            {
                cityGeo(page + 1);
            }
        }

        /// <summary>
        /// supply de 坐标
        /// </summary>
        /// <param name="supplyId">如果是0就是给所有没坐标设定坐标，如果大于0则给指定的更新坐标</param>
        public void GeoSupply(int supplyId)
        {
            if (supplyId > 0)
            {
                var item = (from s in _db.Supplies
                            join t in _db.Countries on s.CountryId equals t.CountryId
                            join c in _db.Cities on s.CityId equals c.CityId into c1 from c2 in c1.DefaultIfEmpty()
                            where s.SupplyId.Equals(supplyId)
                            select new { SupplyId = s.SupplyId, InfoAddress = s.InfoAddress, CityName = c2.CityName, CountryName = t.CountryName }).FirstOrDefault();
                if (item != null)
                {
                    var bmi = getGeo(item.InfoAddress + "," + item.CityName + "," + item.CountryName);
                    if (bmi.Success)
                    {
                        var supply = (from c in _db.Supplies where c.SupplyId.Equals(supplyId) select c).FirstOrDefault();
                        if (supply != null)
                        {
                            supply.CoordsLatitude = bmi.Item["lat"];
                            supply.CoordsLongitude = bmi.Item["lon"];

                            _db.SaveChanges();
                        }
                    }
                }
            }
            else
            {
                supplyGeo(1);
            }
        }

        private void supplyGeo(int page)
        {
            int pageSize = 50;
            int startRecord = (page - 1) * pageSize;

            var list = (from s in _db.Supplies
                        join t in _db.Countries on s.CountryId equals t.CountryId
                        join c in _db.Cities on s.CityId equals c.CityId into c1 from c2 in c1.DefaultIfEmpty()
                        where s.CoordsLatitude.Value.Equals(0) && s.CoordsLongitude.Value.Equals(0)
                        orderby s.SupplyId ascending
                        select new { SupplyId = s.SupplyId, InfoAddress = s.InfoAddress, CityName = c2.CityName, CountryName = t.CountryName })
                        .Skip(startRecord).Take(pageSize).ToList();

            foreach (var item in list)
            {
                //string cityName = string.IsNullOrWhiteSpace(item.CityName) ? "" : item.CityName + ",";
                var bmi = getGeo(item.InfoAddress + "," + item.CityName + item.CountryName);
                if (bmi.Success)
                {
                    var supply = (from c in _db.Supplies where c.SupplyId.Equals(item.SupplyId) select c).FirstOrDefault();
                    if (supply != null)
                    {
                        supply.CoordsLatitude = bmi.Item["lat"];
                        supply.CoordsLongitude = bmi.Item["lon"];

                        _db.SaveChanges();
                    }
                }
            }

            if (list.Count > 0)
            {
                supplyGeo(page + 1);
            }
        }

        /// <summary>
        /// 根据地址返回相关的坐标
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private BoolMessageItem<Dictionary<string, decimal>> getGeo(string address)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                string url = "http://maps.google.com/maps/geo?output=csv&key=key&q=" + System.Web.HttpContext.Current.Server.UrlEncode(address);
                string geo = client.DownloadString(url); //200,6,45.3101717,-89.0166135

                //todo:每次请求完先暂停0.3s
                System.Threading.Thread.Sleep(300);

                string[] _geo = geo.Split(',');
                if (_geo.Length == 4 && Utils.IsNumeric(_geo[2]) && Utils.IsNumeric(_geo[3]))
                {
                    decimal lat = decimal.TryParse(_geo[2], out lat) ? lat : 0;
                    decimal lon = decimal.TryParse(_geo[3], out lon) ? lon : 0;

                    Dictionary<string, decimal> dic = new Dictionary<string, decimal>();
                    dic.Add("lat", lat);
                    dic.Add("lon", lon);

                    return new BoolMessageItem<Dictionary<string, decimal>>(dic, true, "");
                }
                else
                    return new BoolMessageItem<Dictionary<string, decimal>>(null, false, "");
            }
            catch
            {
                return new BoolMessageItem<Dictionary<string, decimal>>(null, false, "");
            }
        }
        #endregion

        #region Profile Maps
        /// <summary>
        /// 获取用户的最近一条记录的地图信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MapsItem ProfileFirstItem(int userId)
        {
            var res = _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsProfileFirst {0}", userId).FirstOrDefault();
            return res == null ? new MapsItem() : res;
        }
        /// <summary>
        /// 根据坐标获取用户的相关数据（profile页面的地图）
        /// </summary>
        /// <param name="ibtType">1:country,2:ctiy,3:supply</param>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <returns></returns>
        public IList<MapsItem> GetProfileMapsList(Identifier.IBTType ibtType, int userId, decimal coordsLatitude, decimal coordsLongitude)
        {
            IList<MapsItem> list = new List<MapsItem>();
            switch (ibtType)
            {
                case Identifier.IBTType.Country:
                    list = _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsProfileCountryList {0},{1},{2}", userId, coordsLatitude, coordsLongitude).ToList();
                    break;
                case Identifier.IBTType.City:
                    list = _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsProfileCityList {0},{1},{2}", userId, coordsLatitude, coordsLongitude).ToList();
                    break;
                default: //todo:supply需要从PV表获取
                    list = _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsProfileSupplyList {0},{1},{2}", userId, coordsLatitude, coordsLongitude).ToList();
                    break;
            }

            return list;
        }


        /// <summary>
        /// Profile 的map infobox 的内容
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public MapsProfileViewItem GetProfileSupplyInfo(int userId, int supplyId)
        {
            var item= _db.Database.SqlQuery<MapsProfileViewItem>("EXEC IBT_MapsProfileSupplyInfo {0},{1}", userId, supplyId).FirstOrDefault();
            return item == null ? new MapsProfileViewItem() : item;
        }

        #endregion

        #region  myList Maps

        /// <summary>
        /// 获取用户的最近一条记录的地图信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MapsItem FirstMyListItem(int userId)
        {
            var res =  _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsMyListFirst {0}", userId).FirstOrDefault();
            return res == null ? new MapsItem() : res;
        }

        /// <summary>
        /// 根据坐标获取用户的相关数据(Mylist page)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <returns></returns>
        public IList<MapsItem> GetMyListMapsList(int userId, decimal coordsLatitude, decimal coordsLongitude)
        {
            IList<MapsItem> list = new List<MapsItem>();
            for (int i = 1; i < 4; i++)
            {
                var list1 = _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsMyListList {0},{1},{2},{3}", userId, coordsLatitude, coordsLongitude, i).ToList();
                foreach (var item in list1)
                {
                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// 根据坐标获取用户的相关数据(Mylist page)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <param name="ibttype"></param>
        /// <returns></returns>
        public IList<MapsItem> GetMyListMapsList(int userId, decimal coordsLatitude, decimal coordsLongitude, Identifier.IBTType ibttype)
        {
            IList<MapsItem> list = new List<MapsItem>();
            var list1 = _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsMyListList {0},{1},{2},{3}", userId, coordsLatitude, coordsLongitude, (int)ibttype).ToList();
            foreach (var item in list1)
            {
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// mylist maps supply info
        /// </summary>
        /// <param name="supplyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MapsMyListViewItem GetMyListSupplyInfo(int supplyId, int userId)
        {
            string sql = @"SELECT   dbo.Supplies.SupplyId, dbo.Supplies.Title, dbo.Supplies.InfoAddress, dbo.Supplies.Desp, dbo.Supplies.CoverPhotoSrc,
		                            ISNULL(dbo.RRWB.Rating,0) AS Rating 
                            FROM     dbo.Supplies LEFT	JOIN
                                      dbo.RRWB ON dbo.RRWB.ObjId = dbo.Supplies.SupplyId AND dbo.RRWB.IBTType=3 AND dbo.RRWB.UserId={0}
                            WHERE dbo.Supplies.SupplyId={1}";
            var res = _db.Database.SqlQuery<MapsMyListViewItem>(sql, userId, supplyId).FirstOrDefault();
            return res == null ? new MapsMyListViewItem() : res;
        }

        #endregion


        #region index maps
        /// <summary>
        /// 获取首页的列表
        /// </summary>
        /// <returns></returns>
        public IList<MapsItem> GetIndexTopList()
        {
            string key = "indexMapsTops10";
            return _cache.GetOrInsert<List<MapsItem>>(key, 30, true, () =>
            {
//                string strSql = @"SELECT Top 10  dbo.IndexTop10Supplies.ObjId AS Id, dbo.IndexTop10Supplies.Title, dbo.Supplies.CoordsLatitude AS lat, dbo.Supplies.CoordsLongitude AS lng,3 AS typ
//                                FROM    dbo.IndexTop10Supplies INNER JOIN
//                                        dbo.Supplies ON dbo.IndexTop10Supplies.ObjId = dbo.Supplies.SupplyId
//                                WHERE dbo.IndexTop10Supplies.StatusId=1
//                                ORDER BY dbo.IndexTop10Supplies.Sort ASC";
                string strSql = @"SELECT * FROM (
		SELECT  ROW_NUMBER() OVER (ORDER BY Sort ASC, temp_rowid ASC) AS RowId,* 
		FROM(
		SELECT 0 AS temp_rowid,
               its.ObjId AS Id,
               its.Title, 
               cn.CoordsLatitude AS lat, 
               cn.CoordsLongitude AS lng,
               its.ITType AS typ,
               its.Sort                
                   FROM  dbo.IndexTop10Supplies its
                   LEFT JOIN dbo.Countries cn ON its.ObjId=cn.CountryId 
                   WHERE its.StatusId=1 AND its.ITType=1
        UNION
        SELECT 1 AS temp_rowid,
               its.ObjId AS Id,
               its.Title, 
               cy.CoordsLatitude AS lat, 
               cy.CoordsLongitude AS lng,
               its.ITType AS typ,
               its.Sort                       
                    FROM  dbo.IndexTop10Supplies its
                    INNER JOIN  dbo.Cities cy ON its.ObjId=cy.CityId 
                    WHERE its.StatusId=1 AND its.ITType=2    
        UNION
        SELECT 2 AS temp_rowid,
               its.ObjId AS Id,
               its.Title, 
               sp.CoordsLatitude AS lat, 
               sp.CoordsLongitude AS lng,
               its.ITType AS typ,
               its.Sort                     
                   FROM  dbo.IndexTop10Supplies its
                   INNER JOIN dbo.Supplies sp ON its.ObjId=sp.SupplyId 
                   WHERE its.StatusId=1 AND its.ITType=3
        )AS temp  
	) As t1
	WHERE RowId BETWEEN 1 AND 10";

                return _db.Database.SqlQuery<MapsItem>(strSql).ToList();
            });
        }

        /// <summary>
        /// 首页地图box的信息
        /// </summary>
        /// <param name="supplyId"></param>
        /// <returns></returns>
        public MapsIndexViewItem GetIndexSupplyInfo(int objId, int objType)
        {
            string strSql = "";
            if (objType == (int)Identifier.IBTType.Supply)
            {
                 strSql = @"SELECT    dbo.Supplies.Rating, 
                                dbo.IndexTop10Supplies.ITSId,
                                dbo.IndexTop10Supplies.ObjId,
                                dbo.IndexTop10Supplies.ITType,
                                dbo.IndexTop10Supplies.Title,
                                dbo.IndexTop10Supplies.SubTitle,
                                dbo.IndexTop10Supplies.ImgSrc,
                                ISNULL(dbo.IndexTop10Supplies.Desp,dbo.Supplies.Desp) AS desp, 
                                dbo.IndexTop10Supplies.StatusId,
                                dbo.IndexTop10Supplies.Sort,
                                dbo.IndexTop10Supplies.CreateDate, 
                                dbo.Supplies.InfoAddress,
                                dbo.Countries.CountryName, 
                                dbo.Cities.CityName
                            FROM  dbo.Supplies 
                            INNER JOIN dbo.Countries ON dbo.Supplies.CountryId = dbo.Countries.CountryId 
                            LEFT JOIN dbo.Cities ON dbo.Supplies.CityId = dbo.Cities.CityId 
                            INNER JOIN dbo.IndexTop10Supplies ON dbo.Supplies.SupplyId = dbo.IndexTop10Supplies.ObjId
                            WHERE dbo.IndexTop10Supplies.StatusId=1 AND dbo.IndexTop10Supplies.ITType=3 AND dbo.IndexTop10Supplies.ObjId={0}";
            }
            else if (objType == (int)Identifier.IBTType.Country)
            {
                strSql = @"SELECT     dbo.Countries.Rating, 
                                dbo.IndexTop10Supplies.ITSId,
                                dbo.IndexTop10Supplies.ObjId,
                                dbo.IndexTop10Supplies.ITType,
                                dbo.IndexTop10Supplies.Title,
                                dbo.IndexTop10Supplies.SubTitle,
                                dbo.IndexTop10Supplies.ImgSrc,
                                ISNULL(dbo.IndexTop10Supplies.Desp,SUBSTRING(dbo.CountriesDes.CountryTxt,1,datalength(dbo.CountriesDes.CountryTxt))) AS desp, 
                                dbo.IndexTop10Supplies.StatusId,
                                dbo.IndexTop10Supplies.Sort,
                                dbo.IndexTop10Supplies.CreateDate, 
                                '' AS InfoAddress,
                                dbo.Countries.CountryName, 
                                '' AS CityName
                            FROM         dbo.Countries 
                            INNER JOIN dbo.CountriesDes ON dbo.Countries.CountryId = dbo.CountriesDes.CountryId
                            INNER JOIN dbo.IndexTop10Supplies ON dbo.Countries.CountryId = dbo.IndexTop10Supplies.ObjId
                            WHERE dbo.IndexTop10Supplies.StatusId=1 AND dbo.IndexTop10Supplies.ITType=1 AND dbo.IndexTop10Supplies.ObjId={0}";
            }
            else
            {
                strSql = @"  SELECT     dbo.Cities.Rating, 
                                dbo.IndexTop10Supplies.ITSId,
                                dbo.IndexTop10Supplies.ObjId,
                                dbo.IndexTop10Supplies.ITType,
                                dbo.IndexTop10Supplies.Title,
                                dbo.IndexTop10Supplies.SubTitle,
                                dbo.IndexTop10Supplies.ImgSrc,
                                ISNULL(dbo.IndexTop10Supplies.Desp,SUBSTRING(dbo.CitiesDescription.Txt,1,datalength(dbo.CitiesDescription.Txt))) AS desp, 
                                dbo.IndexTop10Supplies.StatusId,
                                dbo.IndexTop10Supplies.Sort,
                                dbo.IndexTop10Supplies.CreateDate, 
                                '' AS InfoAddress,
                                dbo.Countries.CountryName, 
                                dbo.Cities.CityName
                            FROM         dbo.Cities 
                            LEFT JOIN dbo.Countries ON dbo.Cities.CountryId = dbo.Countries.CountryId
                            LEFT JOIN dbo.CitiesDescription ON dbo.Cities.CityId = dbo.CitiesDescription.CityId
                            LEFT JOIN dbo.IndexTop10Supplies ON dbo.Cities.CityId = dbo.IndexTop10Supplies.ObjId
                            WHERE dbo.IndexTop10Supplies.StatusId=1 AND dbo.IndexTop10Supplies.ITType=2 AND dbo.IndexTop10Supplies.ObjId={0}";
            }
            var o = _db.Database.SqlQuery<MapsIndexViewItem>(strSql, objId);
            var item = o.FirstOrDefault();
            if (item == null)
                item = new MapsIndexViewItem();
            return item;
        }
        #endregion


        #region Add Travels
        /// <summary>
        /// 获取用户的最近一条记录的地图信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MapsItem FirstAddTravelsItem(int userId)
        {
            var res = _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsAddTravelsFirst {0}", userId).FirstOrDefault();
            return res == null ? new MapsItem() : res;
        }

        /// <summary>
        /// 根据坐标获取用户的相关数据(add travels step 4 page) ---IBT table
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="userId"></param>
        /// <param name="coordsLatitude"></param>
        /// <param name="coordsLongitude"></param>
        /// <returns></returns>
        public IList<MapsItem> GetAddTravelsMapsList(Identifier.IBTType ibtType, int userId, decimal coordsLatitude, decimal coordsLongitude)
        {
            return _db.Database.SqlQuery<MapsItem>("EXEC IBT_MapsAddTravelsList {0},{1},{2},{3}", userId, coordsLatitude, coordsLongitude, (int)ibtType).ToList();
        }

        /// <summary>
        /// 根据Ids 返回相关相关的MapsItems
        /// </summary>
        /// <param name="ibtType"></param>
        /// <param name="ids">1,3,4</param>
        /// <returns></returns>
        public IList<MapsItem> GetAddTravelsNewList(Identifier.IBTType ibtType, string ids)
        {
            List<MapsItem> list = new List<MapsItem>();

            switch (ibtType)
            {
                case Identifier.IBTType.Country:
                    string sql_1 = @"SELECT [CountryId] AS Id
                                      ,[CountryName] AS Title
                                      ,[CoordsLatitude] AS lat
                                      ,[CoordsLongitude] AS lng
                                      ,1 AS typ
                                  FROM [Countries] WHERE [CountryId] IN (" + ids + ")";
                    list = _db.Database.SqlQuery<MapsItem>(sql_1).ToList();
                    break;
                case Identifier.IBTType.City:
                    string sql_2 = @"SELECT [CityId] AS Id
                                      ,[CityName] AS Title
                                      ,[CoordsLatitude] AS lat
                                      ,[CoordsLongitude] AS lng
                                      ,2 AS typ
                                  FROM [Cities] WHERE CityId IN (" + ids + ")";
                    list = _db.Database.SqlQuery<MapsItem>(sql_2).ToList();
                    break;
                case Identifier.IBTType.Supply:
                    string sql_3 = @"SELECT SupplyId AS Id
                                      ,Title
                                      ,[CoordsLatitude] AS lat
                                      ,[CoordsLongitude] AS lng
                                      ,3 AS typ
                                  FROM [Supplies] WHERE SupplyId IN (" + ids + ")";
                    list = _db.Database.SqlQuery<MapsItem>(sql_3).ToList();
                    break;
                default:
                    break;
            }
            return list;
        }
        #endregion
    }
}
