﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public class WidgetCountryManageService
    {
        public string CountryName { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
