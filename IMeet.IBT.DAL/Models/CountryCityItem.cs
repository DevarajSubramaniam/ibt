﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
   public class CountryCityItem
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string FlagSrc { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int Sort { get; set; }
        /// <summary>
        /// 160x160 ImgSrc_City
        /// </summary>
        public string ImgSrc_City
        {
            get
            {
                if (FlagSrc != "")
                    return FlagSrc;
                else
                    return "/App/images/flag/supplier_city_icon.jpg";
            }
        }
    }
}
