﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class FeedcommentsEx:FeedComment
    {
        public string FirstName { get; set; }
        /// <summary>
        /// 数据保存的地址
        /// </summary>
        public string ProfileImage { get; set; }
        /// <summary>
        /// 处理好的地址
        /// </summary>
        public string AvatarPath
        {
            get
            {
                //todo:FeedcommentsEx 处理好的地址
                return ProfileImage;
            }
        }
    }
}
