﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class MemberListItem
    {
       public long row_number { get; set; }
       public int UserId { get; set; }
       public int CurrentLoginSnsId { get; set; }
       public string UserName { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string Password { get; set; }
       //public string ImgSrc { get; set; }
       public Nullable<System.DateTime> Birth { get; set; }
       public string Email { get; set; }
       public string AlternateEmail { get; set; }
       public Nullable<byte> Gender { get; set; }
       public string Location { get; set; }
       public Nullable<byte> IsApproved { get; set; }
       public Nullable<byte> StatusId { get; set; }
       public Nullable<int> CountryId { get; set; }  
       public string CountryName { get; set; }
       public Nullable<int> CityId { get; set; }
       public string CityName { get; set; }
       public Nullable<System.DateTime> CreateDate { get; set; }
       public Nullable<int> Sort { get; set; }
       public Nullable<int> RoleId { get; set; }
       public Nullable<int> UserType { get; set; }
       public string Source { get; set; }

       /// <summary>
       /// n Member has Been There,,这在最后的显示的时候再计算
       /// </summary>
       public int MemberCount { get; set; }

       public string TitleUrl
       {
           get
           {
               //todo:返回Member List 的连接需要处理
               return "/admin/home/v/" + UserId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(UserName);
           }
       }

       //public string ShowImgSrc
       //{
       //    get
       //    {
       //        //todo:返回Member List 的图片需要设置默认的
       //        return "/images/my_connections_list_box.jpg";//
       //    }
       //}
    }
}
