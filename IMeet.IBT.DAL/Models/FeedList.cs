﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    /// <summary>
    /// Proflie feed list 与 my Travel的列表
    /// </summary>
    public class FeedList:NewsFeed
    {
        public string CountryName { get; set; }
        public string FlagSrc { get; set; }
        public string CoverPhotoSrc { get; set; }
        public string CityName { get; set; }
        public string SupplyName { get; set; }
        public byte Rating { get; set; }
        public byte Recommended { get; set; }
        public byte WanttoGo { get; set; }
        public byte BucketList { get; set; }
        public int IBTId { get; set; }
        public int IbtCount { get; set; }
        public int RoleId { get; set; }
        public Nullable<byte> Gender { get; set; }
        public string FirstName { get; set; }
        public List<PostVistAttachment> PostVistAttachmentList { get; set; }
        public List<Attachment> AttachmentList { get; set; }
        /// <summary>
        /// Supplies.ServiceType
        /// </summary>
        public int ServiceType { get; set; }

        public string ExTitleUrl
        {
            get {
                if (SupplyId > 0)
                {
                    return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(SupplyName);
                }
                else if (CountryId > 0 && CityId == 0 && SupplyId == 0)
                {
                    return "/Country/NewsFeed/" + IBTType.ToString() + "/" + CountryId.ToString();
                }
                else
                {
                    return "/City/NewsFeed/" + IBTType.ToString() + "/" + CityId.ToString();
                }
            }
        }

        //todo:feedlist的图片地址处理
        public string ExShowImg
        {
            get {
                if (SupplyId > 0)
                {
                    return CoverPhotoSrc;
                }
                else if (SupplyId == 0 && CityId == 0)
                {
                    return FlagSrc;
                }
                else
                {
                    if (FlagSrc != "")
                        return FlagSrc;
                    else
                        return "/App/images/flag/supplier_city_icon.jpg";
                }
            }
        }

        public int ExObjId
        {
            get {
                if (SupplyId > 0)
                    return SupplyId;
                else if (CityId > 0)
                    return CityId;
                else
                    return CountryId;
            }
        }

        public string ExTitle
        {
            get
            {
                if (SupplyId > 0)
                    return SupplyName;
                else if (CityId > 0)
                    return CityName;
                else
                    return CountryName;
            }

        }

        public string ExGender
        {
            get
            {
                if (Convert.ToInt32(Gender.ToString()) == 0)
                    return "his";
                else if (Convert.ToInt32(Gender.ToString()) == 1)
                    return "her";
                else
                    return "the";
            }
        }
    }
}
