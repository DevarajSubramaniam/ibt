﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class MapsItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public string icon { get; set; }
        public int typ { get; set; }
    }
}
