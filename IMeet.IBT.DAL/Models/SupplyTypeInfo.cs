using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{

    public class SupplyTypeInfo
    {
        public long RowId { get; set; }
        public int SupplyId { get; set; }
        public int ArrtDataId { get; set; }
        public string Title { get; set; }

        public int SupplyAttr { get; set; }
        public int Score { get; set; }

        public int ParentId { get; set; }
        public string AttrDataName { get; set; }
        public byte StatusId { get; set; }
        public string IconSrc { get; set; }
    }
}
