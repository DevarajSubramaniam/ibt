﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class RecommendWeekItem
    {
        public long row_number { get; set; }
        public int MRPId { get; set; }
        public int ObjId { get; set; }
        public int MRPType { get; set; }
        public int SupplyId { get; set; }
        public int StatusId { get; set; }
        public int Sort { get; set; }
        public int UserId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }


         
        public string Title { get; set; }
        public string Desp { get; set; }    
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public decimal Rating { get; set; }     
        public string InfoAddress { get; set; }
        public string InfoState { get; set; }
        public string Types { get; set; }

        public string TitleUrl
        {
            get
            {
                if (MRPType == 3)
                    return "/Supplier/v/" + ObjId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                else if (MRPType == 2)
                    return "/City/NewsFeed/" + MRPType.ToString() + "/" + ObjId.ToString();
                else
                    return "/Country/NewsFeed/" + MRPType.ToString() + "/" + ObjId.ToString();
            }
        }
        //public string TitleUrl
        //{
        //    get
        //    {
        //        return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
        //    }
        //}
    }
}
