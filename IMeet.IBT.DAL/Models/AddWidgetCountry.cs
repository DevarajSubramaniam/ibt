﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public class AddWidgetCountry
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int UserId { get; set; }
    }
}
