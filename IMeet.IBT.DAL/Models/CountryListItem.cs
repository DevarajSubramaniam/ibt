﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class CountryListItem
    {
        public long RowId { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public byte StatusId { get; set; }
        public string FlagSrc { get; set; }
        public Byte Rating { get; set; }
        public Byte Recommended { get; set; }
        public Byte WanttoGo { get; set; }
        public Byte BucketList { get; set; }
        public Nullable<int> Sort { get; set; }
        public int IBTType { get; set; }
        /// <summary>
        /// 去过次数（按钮上会显示的）
        /// </summary>
        public int IbtCount { get; set; }
       /// <summary>
       /// n Friends has Been There,,这在最后的显示的时候再计算
       /// </summary>
       public int FriendCount { get; set; }


       public string ShowImgSrc
       {
           get
           {
               //todo:返回My List 的图片需要设置默认的
               return "/images/national_flag/united_kingdom.gif";//
           }
       }

       public int ObjId
       {
           get
           {
               return CountryId;
           }
       }
    }
}
