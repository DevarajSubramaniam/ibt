﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class MsgListItem
    {
       public int MsgId { get; set; }
       public int ReplyId { get; set; }
       public int FormUserId { get; set; }
       public int ToUserId { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string UserName { get; set; }
       public string ImgSrc { get; set; }
       public bool IsRead { get; set; }
       public bool IsDel { get; set; }
       public bool IsFav { get; set; }
       public string Content { get; set; }
       public DateTime CreateDate { get; set; }

       /// <summary>
       /// n Messages has Been There,,这在最后的现实的时候再计算
       /// </summary>
       public int MsgCount { get; set; }

       public string TitleUrl
       {
           get
           {
               //todo:返回Connection List 的连接需要处理
               return "/Message/v/" + MsgId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Content);
           }
       }

       public string ShowImgSrc
       {
           get
           {
               //todo:返回Connection List 的图片需要设置默认的
               return "/images/avatar/w55_01.gif";//
           }
       }
    }
}
