﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public class UserPointSummaryByYear
    {
        public List<UserPermanentPoint> UserPermanentPointList { get; set; }
        public List<UserBonusPoint> UserBonusPointList { get; set; }
        public List<UserSpecialPointByYear> UserSpecialPointList { get; set; }
    }
}
