﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class VisitedWeekItem
    {
        public long row_number { get; set; }
        public int MVPId { get; set; }
        public int ObjId { get; set; }
        public int StatusId { get; set; }
        public int Sort { get; set; }
        public int UserId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }


         
        public string Title { get; set; }
        public string Desp { get; set; }    
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public decimal Rating { get; set; }     
        public string InfoAddress { get; set; }
        public string InfoState { get; set; }
        public string Types { get; set; }
        public int MVPType { get; set; }
        public string TitleUrl
        {
            get
            {
                if (MVPType == 3)
                    return "/Supplier/v/" + ObjId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                else if (MVPType == 2)
                    return "/City/NewsFeed/" + MVPType.ToString() + "/" + ObjId.ToString();
                else
                    return "/Country/NewsFeed/" + MVPType.ToString() + "/" + ObjId.ToString();
            }
        }
    }
}
