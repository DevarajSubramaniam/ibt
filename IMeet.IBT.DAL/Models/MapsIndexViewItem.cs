﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class MapsIndexViewItem:IndexTop10Supplies
    {
        public decimal Rating { get; set; }
        public string InfoAddress { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }

        public string TitleUrl
        {
            get
            {
                if (ITType == 3)
                    return "/Supplier/v/" + ObjId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                else if (ITType == 2)
                    return "/City/NewsFeed/" + ITType.ToString() + "/" + ObjId.ToString();
                else
                    return "/Country/NewsFeed/" + ITType.ToString() + "/" + ObjId.ToString();
            }
        }
    }
}
