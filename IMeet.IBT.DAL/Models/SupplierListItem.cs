using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{

    public class SupplierListItem
    {
        public long row_number { get; set; }
        public int Id { get; set; }
        public int ObjId { get; set; }
        public int ITType { get; set; }
        public string indexTitle { get; set; }
        public string icon { get; set; }
        public int typ { get; set; }
        public int SupplyId { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }    
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string Title { get; set; }
        public string Desp { get; set; }
        public string CoverPhotoSrc { get; set; }
        public decimal Rating { get; set; }
        public byte StatusId { get; set; }
        public string InfoAddress { get; set; }
        public string InfoZip { get; set; }
        public string InfoPhone { get; set; }
        public string InfoFax { get; set; }
        public string InfoState { get; set; }
        public string InfoWebsite { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public Nullable<int> Score { get; set; }  //����
        public Nullable<decimal> CoordsLatitude { get; set; }
        public Nullable<decimal> CoordsLongitude { get; set; }
        public string TypesOfSupplier { get; set; }
        public Nullable<int> SupplierTypes { get; set; }
        public Nullable<int> ParentSupplyId { get; set; }
        public Nullable<int> CommentsCount { get; set; }

        public string SupplyUrl
        {
            get
            {
                return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
            }
        }
        public string TitleUrl
        {
            get
            {
                if (ITType == 3)
                    return "/Supplier/v/" + ObjId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                else if (ITType == 2)
                    return "/City/NewsFeed/" + ITType.ToString() + "/" + ObjId.ToString();
                else
                    return "/Country/NewsFeed/" + ITType.ToString() + "/" + ObjId.ToString();
            }
        }
    }
}
