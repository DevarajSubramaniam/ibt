﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    /// <summary>
    /// User Extend
    /// </summary>
    public static class UserExtend
    {
        /// <summary>
        /// 判断user 是否meeting planner
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool IsMeetingPlanner(this User user)
        {
            if (user.UserType.HasValue)
                return user.UserType != 0;
            else
                return false;
        }
    }
}
