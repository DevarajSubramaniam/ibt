﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    /// <summary>
    /// Admin City list 的列表
    /// </summary>
    public class CityAllListItem
    {
        public long RowId { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string FlagSrc { get; set; }
        public byte StatusId { get; set; }
        public decimal Rating { get; set; }
        public int RecommendedCount { get; set; }
        public int WanttoGoCount { get; set; }
        public int BucketListCount { get; set; }
        public int IbtCount { get; set; }
        public Nullable<int> Sort { get; set; }
        public string FirstName { get; set; }
        public List<PostVistAttachment> PostVistAttachmentList { get; set; }
   

        //todo:feedlist的图片地址处理
        public string ExShowImg
        {
            get
            {
                if (FlagSrc != "")
                    return FlagSrc;
                else
                    return "/images/national_flag/supplier_city_icon.jpg";
            }
        }

        public int ExObjId
        {
            get
            {
               return CityId;
            }
        }

        public string ExTitle
        {
            get
            {
               return CityName;
            }

        }
    }
}
