﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public class UserBonusPoint
    {
        public int UserId { get; set; }

        public int OSMNumber { get; set; }
        public int OSMPoint { get { return this.OSMNumber * 30; } }

        public int SINumber { get; set; }
        public int SIPonit { get { return this.SINumber * 20; } }

        public int GTNumber { get; set; }
        public int GTPoint { get { return this.GTNumber * 10; } }

        public int PSCNumber { get; set; }
        public int PSCPoint { get { return this.PSCNumber * 20; } }

        public int StrikeNumber { get; set; }
        public int StrikePoint { get { return this.StrikeNumber * 20; } }

        public int NDNumber { get; set; }
        public int NDPoint { get { return this.NDNumber * 20; } }

        public int GANumber { get; set; }
        public int GAPoint { get { return this.GANumber * 20; } }

        public int NHVNumber { get; set; }
        public int NHVPoint { get { return this.NHVNumber * 20; } }

        public int GroupANumber { get; set; }
        public int GroupAPoint { get { return this.GroupANumber * 20; } }

        public int VCNumber { get; set; }
        public int VCPoint { get { return this.VCNumber * 20; } }

        public int CPLPNumber { get; set; }
        public int CPLPPoint { get { return CPLPNumber * 20; } }

        public int OthersNumber { get; set; }
        public int OthersPoint { get { return this.OthersNumber * 20; } }

        public int BonusPointTotal { get { return this.OSMPoint + this.SIPonit + this.GTPoint + this.PSCPoint + this.StrikePoint + this.NDPoint + this.GAPoint + this.NHVPoint + this.GroupAPoint + this.VCPoint + this.CPLPPoint + this.OthersPoint; } }

        public int Year { get; set; }
    }
}
