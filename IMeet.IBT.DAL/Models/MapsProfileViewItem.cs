﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class MapsProfileViewItem
    {
        public int PVId { get; set; }
        public int SupplyId { get; set; }
        public string Firstname { get; set; }
        public DateTime CreateDate { get; set; }
        public string Experiences { get; set; }
        public string CoverPhotoSrc { get; set; }
        public string InfoAddress { get; set; }
        public string Title { get; set; }
        public byte Rating { get; set; }

        public string TitleUrl
        {
            get
            {
                return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
            }
        }
    }
}
