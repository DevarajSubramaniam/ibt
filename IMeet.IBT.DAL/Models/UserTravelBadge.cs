﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public class UserTravelBadge
    {
        public UserPermanentPoint UserPermanentPoint { get; set; }
        public UserBonusPoint UserBonusPoint { get; set; }
        public List<SpecialPoint> SpecialPoint { get; set; }
        public List<UserBadgeSpecial> UserBadgeSpecial { get; set; }

        public User User { get; set; }

        public int BadgeSpecialTotal
        {
            get
            {
                int total = 0;
                if (this.UserBadgeSpecial.Count > 0)
                {
                    this.UserBadgeSpecial.ForEach(b => {
                        total += b.point.Value;
                    });
                }
                return total;
            }
        }

        public int SpecialPointTotal
        {
            get
            {
                int total = 0;
                if (this.SpecialPoint.Count > 0)
                {
                    this.SpecialPoint.ForEach(s =>
                    {
                        total += s.Point;
                    });
                }
                return total;
            }
        }
        /// <summary>
        /// 总分数
        /// </summary>
        public int TotalPoints
        {
            get
            {
                return this.UserPermanentPoint.PermanentPointTotal + this.UserBonusPoint.BonusPointTotal + this.BadgeSpecialTotal;
            }
        }
        /// <summary>
        /// 勋章等级
        /// </summary>
        public int Level
        {
            get
            {
                int l = 0;
                if (this.TotalPoints < 1500)
                    l = 0;
                else if (this.TotalPoints >= 1500 && this.TotalPoints < 3000)
                    l = 1;
                else if (this.TotalPoints >= 3000 && this.TotalPoints < 6000)
                    l = 2;
                else if (this.TotalPoints >= 6000 && this.TotalPoints < 12000)
                    l = 3;
                else if (this.TotalPoints >= 12000)
                {
                    l = 4;
                    //if (this.UserPermanentPoint.CountryNumber >= 40 && this.UserPermanentPoint.CityNumber >= 200 && this.UserPermanentPoint.PlacesNumber >= 300)
                    //    l = 5;
                }
                return l;
            }
        }
        /// <summary>
        /// 勋章图标
        /// </summary>
        public string TravelBadgeLogo
        {
            get {
                string imgSrc = "";

                if (this.User.IsMeetingPlanner())
                {
                    switch (this.Level)
                    {
                        case 1:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start1.png";
                            break;
                        case 2:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start2.png";
                            break;
                        case 3:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start3.png";
                            break;
                        case 4:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start4.png";
                            break;
                        default:
                            imgSrc = "/images/logo/travel_badge/travel_badge.png";
                            break;
                    }
                }
                else
                {
                    switch (this.Level)
                    {
                        case 1:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start1.png";
                            break;
                        case 2:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start2.png";
                            break;
                        case 3:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start3.png";
                            break;
                        case 4:
                            imgSrc = "/images/logo/travel_badge/travel_badge_start4.png";
                            break;
                        default:
                            imgSrc = "/images/logo/travel_badge/travel_badge.png";
                            break;
                    }
                }

                return imgSrc;
            }
        }
        /// <summary>
        /// 勋章图标
        /// </summary>
        public string TravelBadgeProfileLogo
        {
            get
            {
                string imgSrc = "";

                if (this.User.IsMeetingPlanner())
                {
                    switch (this.Level)
                    {
                        case 1:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start1.jpg";
                            break;
                        case 2:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start2.jpg";
                            break;
                        case 3:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start3.jpg";
                            break;
                        case 4:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start4.jpg";
                            break;
                        default:
                            imgSrc = "/images/logo/travel_badge/logo_trveler.jpg";
                            break;
                    }
                }
                else
                {
                    switch (this.Level)
                    {
                        case 1:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start1.jpg";
                            break;
                        case 2:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start2.jpg";
                            break;
                        case 3:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start3.jpg";
                            break;
                        case 4:
                            imgSrc = "/images/logo/travel_badge/logo_trveler_start4.jpg";
                            break;
                        default:
                            imgSrc = "/images/logo/travel_badge/logo_trveler.jpg";
                            break;
                    }
                }

                return imgSrc;
            }
        }
    }
}
