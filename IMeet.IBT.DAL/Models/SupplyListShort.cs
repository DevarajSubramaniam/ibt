﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class SupplyListShort
    {
        public long RowId { get; set; }
        public int SupplyId { get; set; }
        public string Title { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public Byte Rating { get; set; }
        public Byte Recommended { get; set; }
        public Byte WanttoGo { get; set; }
        public Byte BucketList { get; set; }
        public int? CommentsCount { get; set; }
        public string CoverPhotoSrc { get; set; }
        //public string TypesOfVenues { get; set; }
        public byte StatusId { get; set; }
        public string InfoAddress { get; set; }
        public string InfoState { get; set; }
        public Nullable<int> Score { get; set; }
        /// <summary>
        /// 去过次数（按钮上会显示的）
        /// </summary>
        public int IbtCount { get; set; }
        public int Type { get; set; }
        public int ServiceType { get; set; }
        public string TitleUrl
        {
            get
            {
                if (SupplyId > 0)
                    return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                else if (CountryId > 0 && CityId == 0 && SupplyId == 0)
                    return "/Country/NewsFeed/1/" + CountryId.ToString();
                else
                    return "/City/NewsFeed/2/" + CityId.ToString();
            }
        }

        /// <summary>
        /// 100x100 POst visit
        /// </summary>
        public string ImgSrc_S100
        {
            get
            {
                return "/images/photos/imgW100_01.jpg";
            }
        }

        /// <summary>
        /// 98x98 The Recent Places that We've Bee
        /// </summary>
        public string ImgSrc_S98
        {
            get
            {
                return "/images/photos/imgW98_01.jpg";
            }
        }

        /// <summary>
        /// 50x50 Most Visited Places
        /// </summary>
        public string ImgSrc_S50
        {
            get
            {
                return "/images/photos/imgW50_01.gif";
            }
        }

        public int ObjId
        {
            get
            {
                if (SupplyId > 0)
                    return SupplyId;
                else if (CityId > 0)
                    return CityId;
                else
                    return CountryId;
            }
        }
    }
}
