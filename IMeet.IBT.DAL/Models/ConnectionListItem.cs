﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class ConnectionListItem
    {
       public int ConnectionId { get; set; }
       public int UserId { get; set; }
       public int FriendId { get; set; }
       public string UserName { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string ImgSrc { get; set; }
       public int CountryId { get; set; }
       public string CountryName { get; set; }
       public int CityId { get; set; }
       public string CityName { get; set; }
       public int StatusId { get; set; }

       /// <summary>
       /// n Friends has Been There,,这在最后的现实的时候再计算
       /// </summary>
       public int FriendCount { get; set; }

       public string TitleUrl
       {
           get
           {
               //todo:返回Connection List 的连接需要处理
               return "/Connection/v/" + FriendId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(FirstName + LastName);
           }
       }

       public string ShowImgSrc
       {
           get
           {
               //todo:返回Connection List 的图片需要设置默认的
               return "/images/my_connections_list_box.jpg";//
           }
       }
    }
}
