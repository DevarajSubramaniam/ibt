﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class LastPVItem
    {
        public int SupplyId { get; set; }
        public string Title { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string FlagSrc { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int Sort { get; set; }
        /// <summary>
        /// 160x160 ImgSrc_City
        /// </summary>
        public string ImgSrc_City
        {
            get
            {
                if (FlagSrc != "")
                    return FlagSrc;
                else
                    return "/App/images/flag/supplier_city_icon.jpg";
            }
        }
        public string ExTitleUrl
        {
            get
            {
                if (SupplyId > 0)
                {
                    return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                }
                else if (CountryId > 0 && CityId == 0 && SupplyId == 0)
                {
                    return "/Country/About/1/" + CountryId.ToString();
                }
                else
                {
                    return "/City/NewsFeed/2/" + CityId.ToString();
                }
            }
        }
        public int ExObjId
        {
            get
            {
                if (SupplyId > 0)
                    return SupplyId;
                else if (CityId > 0)
                    return CityId;
                else
                    return CountryId;
            }
        }
    }
}
