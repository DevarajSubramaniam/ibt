﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public class UserPermanentPoint
    {
        public int UserId { get; set; }

        public int CountryNumber { get; set; }
        public int CountryPoint { get { return this.CountryNumber * 50; } }

        public int CityNumber { get; set; }
        public int CityPoint { get { return this.CityNumber * 30; } }

        public int PlacesNumber { get; set; }
        public int PlacesPoint { get { return this.PlacesNumber * 10; } }

        public int PermanentPointTotal { get { return this.CountryPoint + this.CityPoint + this.PlacesPoint; } }

        public int Year { get; set; }
    }
}
