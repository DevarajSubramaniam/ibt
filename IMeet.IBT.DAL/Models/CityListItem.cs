﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    /// <summary>
    /// Search list 的列表
    /// </summary>
    public class CityListItem
    {
        public long RowId { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string FlagSrc { get; set; }
        public byte StatusId { get; set; }
        public byte Rating { get; set; }
        public byte Recommended { get; set; }
        public byte WanttoGo { get; set; }
        public byte BucketList { get; set; }
        public int IbtCount { get; set; }
        public Nullable<int> Sort { get; set; }
        public string FirstName { get; set; }
        public List<PostVistAttachment> PostVistAttachmentList { get; set; }
   

        //todo:feedlist的图片地址处理
        public string ExShowImg
        {
            get
            {
                if (FlagSrc != "")
                    return FlagSrc;
                else
                    return "/images/national_flag/supplier_city_icon.jpg";
            }
        }

        public int ExObjId
        {
            get
            {
               return CityId;
            }
        }

        public string ExTitle
        {
            get
            {
               return CityName;
            }

        }
    }
}
