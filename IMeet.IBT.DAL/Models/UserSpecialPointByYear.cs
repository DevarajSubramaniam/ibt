﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public class UserSpecialPointByYear
    {
        public int UserId { get; set; }
        public int Point { get; set; }
        public int Year { get; set; }
    }
}
