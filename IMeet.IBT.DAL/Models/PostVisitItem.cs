﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class PostVisitItem
    {
        public int SupplyId { get; set; }
        public string Title { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string InfoState { get; set; }
        public Byte Rating { get; set; }
        public Byte Recommended { get; set; }
        public Byte WanttoGo { get; set; }
        public Byte BucketList { get; set; }
        public int? CommentsCount { get; set; }
        public string CoverPhotoSrc { get; set; }
        public string TypesOfVenues { get; set; }
        public int ServiceType { get; set; }
        /// <summary>
        /// 去过次数（按钮上会显示的）
        /// </summary>
        public int IbtCount { get; set; }

        public string ShowImgSrc
        {
            get
            {
                //todo: 这里需要根据TypesOfVenues返回不同的图片
                return CoverPhotoSrc;
            }
        }

        public string TitleUrl
        {
            get
            {
                //todo:这里需要返回链接地址
                if (SupplyId > 0)
                    return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                else if (this.CityId > 0)
                    return "/City/NewsFeed/2/" + this.CityId.ToString();
                else if (this.CountryId > 0)
                    return "/Country/NewsFeed/1/"+this.CountryId.ToString();
                else
                    return "#";
            }
        }

        public int ObjId
        {
            get
            {
                if (SupplyId > 0)
                    return SupplyId;
                else if (CityId > 0)
                    return CityId;
                else
                    return CountryId;
            }
        }
    }
}
