﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
    public class DestinationWeekItem
    {
        public long row_number { get; set; }
        public int? DWeekId { get; set; }
        public int? SupplyId { get; set; }
        public byte StatusId { get; set; }
        public int? Sort { get; set; }
        public int? UserId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public string ImgSrc { get; set; }
        public string Desp { get; set; }
       
       
        public string Title { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public decimal Rating { get; set; }     
        public string InfoAddress { get; set; }
        public string InfoState { get; set; }
       // public Nullable<int> SupplierTypes { get; set; }
        public string SupplierTypes { get; set; }
        public int? ObjId { get; set; }
        public int? DWType { get; set; }
        /// <summary>
        /// 去过次数（按钮上会显示的）
        /// </summary>
        public int? IbtCount { get; set; }
        public string TitleUrl
        {
            get
            {
                if (DWType == 3)
                    return "/Supplier/v/" + ObjId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
                else if (DWType == 2)
                    return "/City/NewsFeed/" + DWType.ToString() + "/" + ObjId.ToString();
                else
                    return "/Country/NewsFeed/" + DWType.ToString() + "/" + ObjId.ToString();
            }
        }

        public string ShowImgSrc
        {
            get
            {
                    //todo:返回的DestinationWeekItem图片需要设置
                    return ImgSrc.ToString() != "" ? ImgSrc.ToString() : "/images/photos/imgW430_01.jpg";//
            }
        }
    }
}
