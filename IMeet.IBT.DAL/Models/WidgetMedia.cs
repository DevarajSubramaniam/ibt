﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public partial class WidgetMedia
    {
        public long RowId { get; set; }
        public int MediaId { get; set; }
        public int SupplyId { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public int IbtType { get; set; }
        public string SupplyName { get; set; }
        public int Position { get; set; }
        public int MediaType { get; set; }
        public string Title { get; set; }
        public string Desp { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime ExpiryDate { get; set; }
        public string FileSrc { get; set; }
        public string Code { get; set; }
        public bool IsLink { get; set; }
        public string TargetUrl { get; set; }
        public byte StatusId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }
    }
}
