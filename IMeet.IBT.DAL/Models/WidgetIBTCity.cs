﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL.Models
{
    public partial class WidgetIBTCity
    {
        public int ibtId { get; set; }
        public int ObjId { get; set; }
        public string ObjName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int Sort { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }
    }
}
