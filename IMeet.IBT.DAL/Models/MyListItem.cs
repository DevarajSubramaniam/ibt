﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMeet.IBT.DAL
{
   public class MyListItem
    {
       public int RRWBId { get; set; }
       public int SupplyId { get; set; }
       public string Title { get; set; }
       public string ImgSrc { get; set; }
       public int CountryId { get; set; }
       public string CountryName { get; set; }
       public int CityId { get; set; }
       public string CityName { get; set; }
       public Byte Rating { get; set; }
       public Byte Recommended { get; set; }
       public Byte WanttoGo { get; set; }
       public Byte BucketList { get; set; }
       public int IBTType { get; set; }
       /// <summary>
       /// 去过次数（按钮上会显示的）
       /// </summary>
       public int IbtCount { get; set; }
       /// <summary>
       /// n Friends has Been There,,这在最后的现实的时候再计算
       /// </summary>
       public int FriendCount { get; set; }

       public string TitleUrl
       {
           get
           {
               if (IBTType == 3)
               {
                   //todo:返回My List 的连接需要处理
                   return "/Supplier/v/" + SupplyId.ToString() + "-" + IMeet.IBT.Common.UrlSeoUtils.BuildValidUrl(Title);
               }
               else if (IBTType == 2)
               {
                   return "/City/NewsFeed/" + IBTType.ToString() + "/" + CityId.ToString();
               }
               else
               {
                   return "/Country/About/" + IBTType.ToString() + "/" + CountryId.ToString();
               }

           }
       }

       public string ShowImgSrc
       {
           get
           {
               if (IBTType == 1)
               {
                   return ImgSrc;

               }
               else if (IBTType == 2)
               {
                   if (ImgSrc != "")
                       return ImgSrc;
                   else
                       return "/images/national_flag/supplier_city_icon.jpg";
               }
               else
               {
                   //todo:返回My List 的图片需要设置默认的
                   return ImgSrc;
               }
            
           }
       }

       public int ObjId
       {
           get
           {
               if (SupplyId > 0)
                   return SupplyId;
               else if (CityId > 0)
                   return CityId;
               else
                   return CountryId;
           }
       }
    }
}
