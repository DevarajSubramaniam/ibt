//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IMeet.IBT.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class AttributeData
    {
        public int AttrDataId { get; set; }
        public int ParentId { get; set; }
        public string AttrDataName { get; set; }
        public byte StatusId { get; set; }
        public string IconSrc { get; set; }
        public string IconMap { get; set; }
        public string IconMap2 { get; set; }
    }
}
