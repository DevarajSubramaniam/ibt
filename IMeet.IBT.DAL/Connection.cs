//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IMeet.IBT.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Connection
    {
        public int ConnectionId { get; set; }
        public int UserId { get; set; }
        public int FriendId { get; set; }
        public int StatusId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }
    }
}
